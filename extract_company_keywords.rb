require 'typhoeus'
require 'nokogiri'
require 'open-uri'
require 'sanitize'
require 'redis'
require 'json'
require 'term-extract'

URL = "http://localhost:9200/tradegenie_titan/stock_details/_search"

def self.make_typhoeus_request(url)

  header = {"Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Cache-Control" => "no-cache", "Pragma" => "no-cache", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19"}
  response = Typhoeus::Request.get(url, headers: header, verbose: true, follow_location: true)
  page = Nokogiri::HTML(response.body)
  return page

end

def self.make_request(request_body)
    request = Typhoeus::Request.new(
      URL,
      method: :post,
      body: request_body
    )

    response = request.run
    if response.code!=200
      puts JSON.parse(response.body)
    end
    return response

end

def update_hash_with_new_scores(hash_name,hash_key,keywords)

  ##puts "hash name is: #{hash_name} and hash key is #{hash_key}"

  if hash_name[hash_key].nil?
        hash_name[hash_key] = Hash[keywords.map{|c| c = [c[0],1]}]
  else
    hash_name[hash_key] = Hash[hash_name[hash_key].map{|c| 
      #puts "hash key is-: ->>>> #{hash_key}"
      #puts "keywords already existing are: #{hash_name[hash_key].to_s}"
      #puts "current keyword is:--> #{c[0]}"
      if keywords[c[0]].nil?
        #puts "this keyword was nil. #{c[0]}"
        c = [c[0],c[1]]
      else
        c = [c[0],keywords[c[0]] + c[1]]
      end

    }]

    keywords.each do |key,value|

      if hash_name[hash_key][key].nil?
        hash_name[hash_key][key] = value
      end

    end

    #puts "after adding it becomes: #{hash_name[hash_key]}"
    #gets.chomp
  end
  return hash_name

end

def filter_hash_for_more_than_two(hash_in)

  hash_in = Hash[hash_in.map{|q|

          q[1] = Hash[q[1].map{|c|

              puts "this is the c[0] -- #{c[0]}"
              
              if c[0] =~ /^[A-Z]/ 
                c = nil
              elsif c[1] >= 2
                c = [c[0],c[1]]
              else
                c = nil
              end
          }] 

          q = [q[0],q[1]]        
      }]


  return hash_in

end


def scrape_wikipedia_for_company(company_full_name) 
  company_full_name =  company_full_name.gsub(/[[:punct:]]/){|match|}.gsub(/\s+(plc)$|\s+(Inc)$/){|match|}.gsub(/\s+/){|match| match = "_"}
  puts company_full_name
  gets.chomp
  page = make_typhoeus_request("http://www.wikipedia.com/" + company_full_name)
  puts page.text
  gets.chomp
end

def create_pooled_cross_hash_words(hash)

  pooled_words = {}
  hash.each do |key,value|

    value.keys.each do |word|
      if pooled_words[word].nil?
        pooled_words[word] = [1,[key]]
      else
        pooled_words[word] = [pooled_words[word][0] + 1,pooled_words[word][1] + [key]]
      end
    end

  end

  puts JSON.pretty_generate(pooled_words)
  gets.chomp

  pooled_words = Hash[pooled_words.map{|c|

      if c[1][0] >= 2
          c = [c[0],c[1]]
      else
          false
      end  

    }]

  puts JSON.pretty_generate(pooled_words)
  gets.chomp


end

query_string = 
'
  {
      "size":400,
      "query": {
          "match_all": {
              
          }
      }    
  }

'
 
response = make_request(query_string)    

company_name_and_and_pooled_keywords = {}
sector_and_pooled_keywords = {}
industry_and_pooled_keywords = {}


if response.code == 200
  body = JSON.parse(response.body)
  body['hits']['hits'].each do |doc|

    source = doc['_source']
    

    keywords = TermExtract.extract(source['company_description'])
    
    wiki_page = scrape_wikipedia_for_company(source['stockFullName'])



    if keywords.nil?
      puts "no keywords got for #{source['stockFullName']}"
    else   
      
      ##processing on a per company basis
     ## company_name_and_pooled_keywords = update_hash_with_new_scores(company_name_and_and_pooled_keywords,source['stockFullName'],keywords)

      ##processing for sector
      sector_and_pooled_keywords = update_hash_with_new_scores(sector_and_pooled_keywords,source['sector'],keywords)


      ##processing for industry
      industry_and_pooled_keywords = update_hash_with_new_scores(industry_and_pooled_keywords,source['industry'],keywords)



    end

  end
end





sector_and_pooled_keywords = filter_hash_for_more_than_two(sector_and_pooled_keywords)
industry_and_pooled_keywords =  filter_hash_for_more_than_two(industry_and_pooled_keywords)




#create_pooled_cross_hash_words(sector_and_pooled_keywords)



##algorithm to pursue

##get wikipedia company page.
##add to existing sector / industry of company from its sector listed on wikipedia
##then pool 



#IO.write("industry.json",JSON.pretty_generate(industry_and_pooled_keywords))
#IO.write("sector.json",JSON.pretty_generate(sector_and_pooled_keywords))



###getting wikipedia pages containing the components of each of the indices that we use.
