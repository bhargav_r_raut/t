import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;


if (ctx._source.buy == null || ctx._source.buy.empty ){
	//println "buy or sell were null";
	ctx._source.decompressed_length = sparse_arr[sparse_arr.length - 1];
	//println "decompressed length is:" + ctx._source.decompressed_length;
	ctx._source.buy = get_compressed_int_array(get_arr(sparse_arr,0,null));
	ctx._source.sell = get_compressed_int_array(get_arr(sparse_arr,1,null));
}
else{
	
	ctx._source.decompressed_length = sparse_arr[sparse_arr.length - 1];

	int[] barr = get_decompressed_array_from_compressed_array(
			ctx._source.buy, ctx._source.decompressed_length);
	barr = get_arr(sparse_arr,0,barr);
	ctx._source.buy = get_compressed_int_array(barr);

	int[] sarr = get_decompressed_array_from_compressed_array(ctx._source.sell,ctx._source.decompressed_length);
	sarr = get_arr(sparse_arr,1,sarr);
	ctx._source.sell = get_compressed_int_array(sarr);


}


/****
	 * TESTED TAKES THE GIVEN UNCOMPRESSED ARRAY AND COMPRESSES IT DEPENDING ON
	 * ITS TYPE.
	 * 
	 * 
	 * @param data
	 * @param type
	 *            : 0 uncompressed, 1 compressed
	 * @return
	 */
	public static int[] get_compressed_int_array(int[] data) {
		
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());

		int[] compressed = new int[data.length + 1024];

		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed,
				outputoffset);

		// System.out.println("compressed from " + data.length * 4 / 1024
		// + "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");

		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		// System.out.println("invoked compression of sorted array:");
		return compressed;
		
	}

/****
	 * given a compressed array, and its type,(whether unsorted or sorted)
	 * returns a decompressed array.
	 * 
	 * 
	 * @param compressed_arr
	 * @param expected_length_of_decompressed_array
	 * @param type
	 * @return
	 */
	public static int[] get_decompressed_array_from_compressed_array(
			ArrayList<Integer> compressed_list, int expected_length_of_decompressed_array) {

		int[] compressed_arr = new int[compressed_list.size()];
		
		for(int i=0; i < compressed_list.size(); i++){
			compressed_arr[i] = compressed_list.get(i);
		}

		int[] recovered = new int[expected_length_of_decompressed_array + 1];

		
		// sorted
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		recovered = new int[expected_length_of_decompressed_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed_arr, new IntWrapper(0),
				compressed_arr.length, recovered, recoffset);
		// System.out
		// .println("invoking decompression of sorted array and returned \n");
		
		return recovered;

	}





public static int[] get_arr(int[] sparse_arr, Integer buy_or_sell,
			int[] sorted_compressible_arr) {

		int max_day_id = sparse_arr[sparse_arr.length - 1];
		int init_fill = 0;
		if (sorted_compressible_arr == null) {
			sorted_compressible_arr = new int[max_day_id + 1];
		} else {
			init_fill = sorted_compressible_arr[sorted_compressible_arr.length - 1];
			sorted_compressible_arr = Arrays.copyOf(sorted_compressible_arr,
					sparse_arr[sparse_arr.length - 1] + 1);
		}

		int index = 0;

		for (int day_id : sparse_arr) {
			
			int prev_day_id = index == 0 ? init_fill : sparse_arr[index - 1];
			int fill_with = buy_or_sell.intValue() == 0 ? prev_day_id : day_id;
			int s = (index == 0) ? prev_day_id : prev_day_id + 1;

			for (int i = s; i < day_id; i++) {
				
				sorted_compressible_arr[i] = sorted_compressible_arr[i] != 0 ? sorted_compressible_arr[i]
						: fill_with;
			}

			sorted_compressible_arr[day_id] = day_id;
			index++;
		}


		return sorted_compressible_arr;
	}