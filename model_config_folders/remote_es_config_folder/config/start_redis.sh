#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "dir is $DIR"
if pgrep "redis" > /dev/null
then
    echo "Redis is Running"
else
    echo "Redis does nto exist...starting it now..."
    redis-server $DIR/redis.conf
    sleep 1
    redis-cli PING
fi
