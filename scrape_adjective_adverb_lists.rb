require 'typhoeus'
require 'nokogiri'
require 'open-uri'
require 'sanitize'
require 'redis'
require 'json'
require 'term-extract'


def self.make_request(request_body)
    request = Typhoeus::Request.new(
      URL,
      method: :post,
      body: request_body
    )

    response = request.run
    if response.code!=200
      puts JSON.parse(response.body)
    end
    return response

end


$normal_redis = Redis.new

header = {"Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Cache-Control" => "no-cache", "Pragma" => "no-cache", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19"}

=begin
BASE_URL = "http://www.enchantedlearning.com/wordlist/adverbs.shtml"

response = Typhoeus::Request.get(BASE_URL, headers: header, verbose: true, follow_location: true)
page = Nokogiri::HTML(response.body)
adjectives = []
page.xpath("//tr/td").each do |cell|
	if cell.text.strip=="zestily"
		adjectives << cell.text.strip
		break
	end
	adjectives = adjectives + cell.text.strip.split(/\n/)
end

$normal_redis.pipelined do 
	adjectives[11..-1].each do |adjective|
		$normal_redis.sadd("adjectives",adjective)
	end
end
=end
=begin
BASE_URL = "http://www.enchantedlearning.com/wordlist/adjectives.shtml"
response = Typhoeus::Request.get(BASE_URL, headers: header, verbose: true, follow_location: true)
page = Nokogiri::HTML(response.body)
adverbs = []
crossed = false

page.xpath("//tr/td").each do |cell|
	if cell.text.strip=="zigzag"
		adverbs << cell.text.strip
		break
	end

	adverbs = adverbs + cell.text.strip.split(/\n/)
	
end


$normal_redis.pipelined do 
	adverbs[34..-1].each do |adjective|
		$normal_redis.sadd("adjectives",adjective)
	end
end
=end


#a = IO.write("/home/aditya/Downloads/elasticsearch_in_use/elasticsearch-1.4.4/config/stopwords/english.txt",$normal_redis.smembers("adjectives").join("\n"))





content = <<DOC

Sanofi researches, develops, and markets various therapeutic solutions. Its products comprise diabetes solutions, including Lantus, Apidra, and Insuman that are human insulin analogs; Amaryl, an oral sulfonylurea; Lyxumia, a glucagon-like peptide-1 receptor agonist; and Afrezza, an inhaled insulin to improve glycemic control, as well as Toujeo, an insulin glargine. The company also offers Cerezyme to treat gaucher disease; Myozyme/Lumizyme for the pompe disease treatment; Fabrazyme for fabry disease treatment; and Aldurazyme to treat mucopolysaccharidosis type I, as well as provides Aubagio and Lemtrada for multiple sclerosis. In addition, it provides Jevtana for prostate cancer; Taxotere for the treatment of breast, non-small cell lung, prostate, gastric, and head and neck cancers; Thymoglobulin to treat acute rejection in organ transplantation, aplastic anemia, and graft-versus-host diseases; Eloxatin for colorectal cancer; Mozobil for treating hematologic malignancies; and Zaltrap for oxaliplatin resistant metastatic colorectal cancer. Further, the company offers other prescription drugs, such as Plavix for atherothrombosis and acute coronary syndrome; Lovenox for the treatment and prevention of deep vein thrombosis and acute coronary syndromes; Aprovel/CoAprovel for hypertension; Renagel/Renvela to treat chronic kidney disease; Depakine for epilepsy; Synvisc/Synvisc-One for the treatment of pain associated with osteoarthritis of the knee; Stilnox/Ambien/Myslee to treat sleep disorders; Multaq for atrial fibrillation; Allegra for the treatment of allergic rhinitis and urticarial; Actonel for osteoporosis and paget's disease; and Auvi-Q/Allerject for the treatment of severe allergic reactions. Additionally, it offers consumer health care products and generic medicines; and vaccine and animal health products. The company was formerly known as Sanofi-Aventis and changed its name to Sanofi in May 2011. Sanofi was founded in 1973 and is headquartered in Paris, France.
DOC

terms = TermExtract.extract(content)
puts terms.to_s





