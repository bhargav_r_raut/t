import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import subindicators.TImeBasedSubindicators;
import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.dataPointIndicator;
import Indicators.RedisManager;

public class BenchMarks {

	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		//PairArray.pair_differences_index_to_end_day_id(48601);
		//System.out.println(PairArray.get_int_array_index_given_start_day_id_and_difference(162, 1).getPair_difference_array_index());
		TImeBasedSubindicators.generate_expected_subindicators();
	}

	public static boolean[] get_boolean_array() {
		return new boolean[6];
	}

	public void compress_300_unsorted_integers() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		/**
		 * try(Jedis jedis = RedisManager.getInstance().getJedis()){ Pipeline p
		 * = jedis.pipelined(); for(int i=0; i < 40000; i++){ int[]
		 * compressed_arr = PairArray.get_random_test_array(300, 0, 0, 10000,
		 * -10000); String s = mapper.writeValueAsString(compressed_arr);
		 * p.hset("test", String.valueOf(i), s); } p.sync(); }
		 **/

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			GregorianCalendar cal = new GregorianCalendar();
			Pipeline p = jedis.pipelined();
			for (int i = 0; i < 400; i++) {
				p.hget("test", String.valueOf(i));
			}
			p.syncAndReturnAll();
			GregorianCalendar end_cal = new GregorianCalendar();
			System.out.println(end_cal.getTimeInMillis() - cal.getTimeInMillis());
		}

	}

	public void time_taken_to_decompress_1000_compressed_unsorted_arrays() {
		ArrayList<int[]> comp_arr_list = new ArrayList<int[]>();
		for (int i = 0; i < 1000; i++) {
			comp_arr_list.add(PairArray.get_random_test_array(1000, 0, 1, 10000, -10000));
		}
		GregorianCalendar start_cal = new GregorianCalendar();

		for (int[] carr : comp_arr_list) {
			int[] dec = PairArray.get_decompressed_array_from_compressed_array(carr, 1000, 0);
		}

		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println(
				"time taken to decompress 1000 days ids:" + (end_cal.getTimeInMillis() - start_cal.getTimeInMillis()));
	}

	public void run() throws JsonGenerationException, JsonMappingException, IOException {
		HashMap<String, int[]> hm = new LinkedHashMap<String, int[]>();
		/**
		 * test_2_unsorted_arrays_for_each_stock(hm); System.out.println(
		 * "after the big arrays."); PairArray.print_heap_stats();
		 * test_lookup_and_addition_speed(hm);
		 **/
		// test_entity_memory_requirements();
		// evaluate_bigdecimal_comparison_speed();
		compress_300_unsorted_integers();
		// time_taken_to_decompress_1000_compressed_unsorted_arrays();
	}

	/***
	 * 100 entities, required 2.5 gigs to load into ram. for all entities will
	 * require a jvm of at least 10 gigs, just for holding all the values for
	 * the calculations.
	 * 
	 * 
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void test_entity_memory_requirements() throws JsonGenerationException, JsonMappingException, IOException {
		// instantiate 300 new entities.
		// on each entity instantieate 3000 datapoints in the price map
		// internal.
		// on each datapoint instantiate, 20 indicators, with random values.
		// and then see the heap size
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		mapper.configure(Feature.USE_ANNOTATIONS, true);
		HashMap<String, Entity> en_map = new LinkedHashMap<String, Entity>();
		Random r = new Random();
		for (int i = 0; i < 1; i++) {
			Entity e = new Entity("test" + String.valueOf(i), 1);
			e.setPrice_map_internal(new LinkedList<Map<Integer, DataPoint>>());
			for (int d = 0; d < 3000; d++) {
				DataPoint da = new DataPoint();
				da.setClose(String.valueOf(r.nextDouble()));
				da.setHigh(String.valueOf(r.nextDouble()));
				da.setLow(String.valueOf(r.nextDouble()));
				da.setOpen(String.valueOf(r.nextDouble()));
				da.setVolume(String.valueOf(r.nextDouble()));
				for (int in = 0; in < 20; in++) {
					dataPointIndicator dpi = new dataPointIndicator();
					dpi.setIndicator_big_decimal_value(new BigDecimal(r.nextDouble()));
					// dpi.setIndicator_big_decimal_mean(new
					// BigDecimal(r.nextDouble()));
					// dpi.setIndicator_big_decimal_standard_deviation(new
					// BigDecimal(r.nextDouble()));
					da.getIndicator_values_on_datapoint().put(String.valueOf(in), dpi);
				}
				e.getPrice_map_internal().put(d, da);
			}
			en_map.put("test" + i, e);
			System.out.println("after the entity:" + i);
			System.out.println("map size is: " + en_map.size());
			PairArray.print_heap_stats();
		}

		for (Map.Entry<String, Entity> entry : en_map.entrySet()) {
			System.out.println(entry.getKey());
			Integer random_day = r.nextInt(3000);
			System.out.println(
					entry.getValue().getPrice_map_internal().get(random_day).getIndicator_values_on_datapoint());
			System.out.println(mapper.defaultPrettyPrintingWriter()
					.writeValueAsString(entry.getValue().getPrice_map_internal().get(random_day)));
		}
	}

	public void test_lookup_and_addition_speed(HashMap<String, int[]> hm) {
		// we want to take
		// 1500 days,
		// then take 7 days after each of them
		// then add and average.
		String dummy_big_array_name = "101";
		// basically just lookup and add integers.
		int[] compressed_arr = hm.get(dummy_big_array_name);
		System.out.println("compressed array length is:" + compressed_arr.length);

		int[] decom = PairArray.get_decompressed_array_from_compressed_array(compressed_arr, 3000 * 300, 0);
		int[] l = new int[7];
		for (int i = 0; i < 7; i++) {
			l[i] = -90000;
		}
		// so you want to get and add everything after each day for 1 week.
		GregorianCalendar cal = new GregorianCalendar();
		for (int a = 0; a < 5000; a++) {

			for (int i = 0; i < 1500; i++) {
				Random r = new Random();
				int ki = r.nextInt(300 * 3000 - 20);
				for (int g = 0; g < 7; g++) {
					if (l[g] == -90000) {
						l[g] = decom[ki + g];
					} else {
						l[g] += decom[ki + g];
					}
				}
			}

		}
		GregorianCalendar cal_end = new GregorianCalendar();
		System.out.println(cal_end.getTimeInMillis() - cal.getTimeInMillis());
	}

	/***
	 * these arrays hold the open open and close close price change differences
	 * for each stock for 300 days per day for a total of 3000 days.
	 * 
	 * @param hm
	 */
	public void test_2_unsorted_arrays_for_each_stock(HashMap<String, int[]> hm) {
		for (int i = 0; i < 11; i++) {
			for (int k = 0; k < 2; k++) {
				int[] tarr = PairArray.get_random_test_array(3000 * 300, 0, 1, 10000, -10000);
				System.out.println("array name is:" + String.valueOf(i) + String.valueOf(k));
				hm.put(String.valueOf(i) + String.valueOf(k), tarr);
				System.out.println("after stock:" + String.valueOf(i));
				PairArray.print_heap_stats();
			}
		}
	}

	public void test_bsl_and_tsl_arrays_total_18_each_length_3000_sorted(HashMap<String, int[]> hm) {
		for (int i = 0; i < 300; i++) {
			for (int j = 0; j < 18; j++) {
				int[] tarr = PairArray.get_random_test_array(3000, 1, 1, 300 * 3000, 0);
				hm.put(String.valueOf(i) + String.valueOf(j), tarr);
			}
		}
	}

	public void test_1000_arrays_of_length_3000_for_1_day_1_week_1_month_unsorted_changes(HashMap<String, int[]> hm) {

		HashMap<String, int[]> arrays_map = new LinkedHashMap<String, int[]>();

		for (int i = 0; i < 5000; i++) {
			arrays_map.put(String.valueOf(i), PairArray.get_random_test_array(3000, 0, 1, 1000, -1000));
		}

	}

	public void store_for_each_day_of_each_stock_3_arrays_each_unsorted_of_length_30(HashMap<String, int[]> hm) {
		for (int stock = 0; stock < 1; stock++) {

			for (int d = 0; d < 3000; d++) {
				for (int i = 0; i < 3; i++) {
					// get an unsorted array of

					int[] x = PairArray.get_random_test_array(30, 0, 1, 300, -300);
					hm.put(String.valueOf(stock) + String.valueOf(i) + String.valueOf(d), x);
				}
				System.out.println("did day:" + d + " of stock: " + stock);
				PairArray.print_heap_stats();
			}

		}
	}

	public void evaluate_bigdecimal_comparison_speed() {
		BigDecimal bd = new BigDecimal("10.3422").setScale(4, RoundingMode.HALF_DOWN);
		Random r = new Random();
		for (int i = 0; i < 1000000; i++) {
			Double d = r.nextDouble();

			BigDecimal db = new BigDecimal(d.toString());
			int res = bd.compareTo(db);
			if (i % 1000 == 0) {
				System.out.println("did:" + (i) + " : " + res);
				System.out.println(d);
			}
		}
	}

	public void test_fst_size() {

	}

	public BenchMarks() {

	}

}
