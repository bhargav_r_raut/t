import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;

import elasticSearchNative.LocalEs;

public class BulkProcessorTestClasses {
	private BulkProcessor bulkprocessor;

	public BulkProcessorTestClasses(Client client) {
		bulkprocessor = BulkProcessor
				.builder(client, new BulkProcessor.Listener() {

					public void afterBulk(long execution_id, BulkRequest req,
							BulkResponse resp) {
						// TODO Auto-generated method stub
						System.out.println("execution id:" + execution_id);
						System.out.println(req.toString());
						System.out.println(req.getContext().toString());
						BulkItemResponse[] resp_items_arr = resp.getItems();
						for (int i = 0; i < resp_items_arr.length; i++) {
							// System.out.println(resp_items_arr[i].get);
							System.out.println(resp_items_arr[i].getItemId());

						}
						System.out.println(resp.hasFailures());
					}

					public void afterBulk(long arg0, BulkRequest arg1,
							Throwable arg2) {
						// TODO Auto-generated method stub
						System.out.println("the whole bulk failed.--->"
								+ arg2.getLocalizedMessage());
					}

					public void beforeBulk(long arg0, BulkRequest arg1) {
						// TODO Auto-generated method stub
						System.out
								.println("these were the number of requests sent to es. bulk");
						System.out.println(arg1.numberOfActions());

					}

				}).setBulkActions(10)
				.setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
				.setFlushInterval(TimeValue.timeValueSeconds(30))
				.setConcurrentRequests(1).build();
	}

	public void testBulkRequest() throws InterruptedException {
		for (int i = 0; i < 10; i++) {
			bulkprocessor.add(new IndexRequest("test_index", "test_mapping")
					.source("test_name", "name_" + String.valueOf(i)));
		}
		bulkprocessor.awaitClose(10, TimeUnit.SECONDS);
		
		for (int i = 0; i < 10; i++) {
			bulkprocessor.add(new IndexRequest("test_index", "test_mapping")
					.source("test_name", "name_" + String.valueOf(i)));
		}
		bulkprocessor.awaitClose(10, TimeUnit.SECONDS);
	}

	public static void main(String[] args) throws InterruptedException {
		 Client client = LocalEs.getInstance();
		 BulkProcessorTestClasses bulkProcessorTestClasses = new
		 BulkProcessorTestClasses(client);
		 bulkProcessorTestClasses.testBulkRequest();

		
	}

}