package CommonUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;
import yahoo_finance_historical_scraper.StockConstants;

public class CompressUncompress {
	
	public static IntegratedIntegerCODEC codec = new IntegratedComposition(
			new IntegratedBinaryPacking(), new IntegratedVariableByte());
	
	/**
	 * function is used to convert the arraylist of integers(the new day ids) to
	 * an int array, awaiting its further conversion to a byte array to be
	 * stored into redis.
	 * 
	 * 
	 */
	public static int[] convert_integer_list_to_int_array(List<Integer> integers) {
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next().intValue();
		}
		return ret;
	}
	
	/**
	 * function is used to convert the arraylist of integers(the new day ids) to
	 * an int array, awaiting its further conversion to a byte array to be
	 * stored into redis.
	 * 
	 * 
	 */
	public static double[] convertDoubles(List<Double> doubles) {
		double[] ret = new double[doubles.size()];
		Iterator<Double> iterator = doubles.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next();
		}
		return ret;
	}
	
	public static int[] byte_array_to_int_array(byte[] arr) {
		ByteArrayInputStream bais = new ByteArrayInputStream(arr); // byte[] arr
		DataInputStream dis = new DataInputStream(bais);
		int[] tab = null;
		int lenght = 0;
		try {
			lenght = dis.available() / 4;
			tab = new int[lenght];
			for (int i = 0; i < lenght; i++) {
				tab[i] = dis.readInt();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return tab;
	}
	
	public static byte[] int_array_to_byte_array(int[] con) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			for (int i : con)
				dos.writeInt(i);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		byte[] arr = baos.toByteArray();
		return arr;
	}
	
	/**
	 * given an array of bytes, 
	 * <br>
	 * first convert the byte[] to int[]
	 * <br>
	 * then call <a>decompress_int_array</a>
	 * 
	 * @param redis_bytes
	 * @return uncompressed integer array
	 */
	public static int[] decompress_byte_array_to_integer_array(byte[] byte_array){
		 
		int[] compressed_int_array = 
				byte_array_to_int_array(byte_array);
		return decompress_int_array(compressed_int_array);
		
	}
	
	/**
	 * 
	 * 
	 * 
	 * @param decompressed_int_array
	 * @return decompressed arraylist 
	 */
	public static ArrayList<Integer> decompress_byte_array_to_integer_list(byte[] byte_array){
		int[] decompressed_int_array = decompress_byte_array_to_integer_array(byte_array);
		ArrayList<Integer> decompressed_int_arraylist = new ArrayList<Integer>();
		for(int i: decompressed_int_array){
			decompressed_int_arraylist.add(i);
		}
		return decompressed_int_arraylist;
	}
	
	/**
	 * given a compressed int[] , decompress it using the codec and return.
	 * 
	 * @param compressed_int_array
	 * @return uncompressed int array
	 */
	public static int[] decompress_int_array(int[] compressed_int_array){
		 int[] recovered = new int[StockConstants.max_size_of_redis_complex_values_arrays]; 
		 IntWrapper recoffset = new IntWrapper(0);
		 codec.uncompress(compressed_int_array, new IntWrapper(0),
		 compressed_int_array.length, recovered, recoffset);
		 return Arrays.copyOf(recovered, recoffset.intValue());
	}
	
	/**
	 * given an int[]<br>
	 * first compress it using the codec<br>
	 * then return
	 * 
	 * 
	 * @param array_to_compress
	 * @return compressed int[]
	 */
	public static int[] compress_integer_array(int[] array_to_compress){
		
		int[] compressed = new int[array_to_compress.length + 1024];
		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(array_to_compress, inputoffset, array_to_compress.length, compressed, outputoffset);
		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		
		return compressed;
		
	}
	
	
	public static byte[] compress_integer_array_to_byte_array(int[] array_to_compress){
		
		int[] compressed = compress_integer_array(array_to_compress);
		return int_array_to_byte_array(compressed);
		
	}
	
	public static Double[] integer_array_to_double_array(Integer[] arr){
		Double[] d = new Double[arr.length];
		for(int i = 0; i < arr.length; i++){
			d[i] = Double.parseDouble(arr[i].toString());
		}
		return d;
	}
	
	
	public static Double find_nearest(Double search, Double[] arr) {

		Double closest = null;
		for (Double item : arr) {
			if ((closest == null)
					|| (Math.abs(search - closest) > Math.abs(item - search))) {
				closest = item;
			}
		}

		return closest;
	}
	
	public static Integer get_total_number_of_actual_sessions_between_two_day_ids(
			Integer day_id_greater, Integer day_id_lesser, 
			TreeMap<Integer,Double> all_available_indicator_values) {
		
		System.out.println("day id greater is:" + day_id_greater);
		System.out.println("day id lesser is:" + day_id_lesser);
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Integer size =  all_available_indicator_values.subMap(day_id_lesser, true,
				day_id_greater, false).size();
		
		return size;

	}
	
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	

	
}
