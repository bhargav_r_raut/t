package CommonUtils;

import yahoo_finance_historical_scraper.StockConstants;

public class FloatGlitch {

	/***
	 * takes the price of any stock multiplies it by the constant , which is
	 * usually 10000 then converts to int to get rid of the decimals, then
	 * converts back to double and returns.
	 * 
	 * a)1.22356
	 * 
	 * b)12235.6
	 * 
	 * c)12235
	 * 
	 * d)12235.0
	 * 
	 * 
	 * @param price_incoming
	 * @return
	 */
	private static Double readjust_value(Double incoming_value,
			Integer multiplication_factor) {

		double new_price = incoming_value * multiplication_factor;

		long adjusted_value = (long) new_price;

		return (double) adjusted_value;
	}

	/***
	 * name can be complex name or indicator name
	 * 
	 * a) check if the indicator should be readjusted or not. b) then readjust
	 * it. c) use the same readjustment policy as above.
	 * 
	 * @param incoming_indicator
	 * @param name
	 * @return
	 */
	public static Double process_indicator(Double incoming_indicator_value,
			String indicator_name) {

		Integer multiplication_factor_for_indicator = StockConstants.value_adjustment_rules
				.get(indicator_name);

		return readjust_value(incoming_indicator_value,
				multiplication_factor_for_indicator);

	}

	/***
	 * 
	 * should only be used when the incoming double has already been adjusted
	 * previously
	 * 
	 * cast incoming double as integer, and cast back as double.
	 * 
	 * 
	 * @param incoming_double
	 * @return
	 */
	public static Double drop_decimals(Double incoming_double) {
		long i = (long) incoming_double.doubleValue();
		return (double) i;
	}

}
