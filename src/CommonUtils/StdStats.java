package CommonUtils;

/******************************************************************************
 *  Compilation:  javac StdStats.java
 *  Execution:    java StdStats < input.txt
 *  Dependencies: StdOut.java
 *
 *  Library of statistical functions.
 *
 *  The test client reads an array of real numbers from standard
 *  input, and computes the minimum, mean, maximum, and
 *  standard deviation.
 *
 *  The functions all throw a NullPointerException if the array
 *  passed in is null.
 *
 *  The floating-point functions all return NaN if any input is NaN.
 *
 *  Unlike Math.min() and Math.max(), the min() and max() functions
 *  do not differentiate between -0.0 and 0.0.
 *
 *  % more tiny.txt
 *  5
 *  3.0 1.0 2.0 5.0 4.0
 *
 *  % java StdStats < tiny.txt
 *         min   1.000
 *        mean   3.000
 *         max   5.000
 *     std dev   1.581
 *
 *  Should these funtions use varargs instead of array arguments?
 *
 ******************************************************************************/

/**
 * <i>Standard statistics</i>. This class provides methods for computing
 * statistics such as min, max, mean, sample standard deviation, and sample
 * variance.
 * <p>
 * For additional documentation, see <a
 * href="http://introcs.cs.princeton.edu/22library">Section 2.2</a> of
 * <i>Introduction to Programming in Java: An Interdisciplinary Approach</i> by
 * Robert Sedgewick and Kevin Wayne.
 * 
 * @author Robert Sedgewick
 * @author Kevin Wayne
 */
public final class StdStats {

	private StdStats() {
	}

	/**
	 * Returns the average value in the array.
	 * 
	 * @param a
	 *            the array
	 * @return the average value in the array <tt>a[]</tt>, <tt>Double.NaN</tt>
	 *         if no such value
	 */
	public static double mean(double[] a) {
		if (a.length == 0)
			return Double.NaN;
		double sum = sum(a);
		double mean = sum / a.length;
		return FloatGlitch.drop_decimals(mean);
	}

	/**
	 * Returns the population variance in the array.
	 * 
	 * @param a
	 *            the array
	 * @return the population variance in the array <tt>a[]</tt>,
	 *         <tt>Double.NaN</tt> if no such value
	 */
	public static double varp(double[] a) {
		if (a.length == 0)
			return Double.NaN;
		double avg = mean(a);
		double sum = 0.0;
		for (int i = 0; i < a.length; i++) {
			sum += (a[i] - avg) * (a[i] - avg);
		}
		return sum / a.length;
	}

	/**
	 * Returns the population standard deviation in the array.
	 * 
	 * @param a
	 *            the array
	 * @return the population standard deviation in the subarray <tt>a[lo]</tt>,
	 *         <tt>Double.NaN</tt> if no such value
	 */
	public static double stddevp(double[] a) {
		return Math.sqrt(varp(a));
	}

	/**
	 * Returns the sum of all values in the array.
	 * 
	 * @param a
	 *            the array
	 * @return the sum of all values in the array <tt>a[]</tt>, <tt>0.0</tt> if
	 *         no such value
	 */
	public static double sum(double[] a) {
		double sum = 0.0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}

}
