import java.util.HashMap;
import java.util.List;

import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.StockOperations;
import Indicators.ComplexCalculationRequest;
import Titan.GroovyStatements;


public class ComplexCalc {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GroovyStatements.getInstance().initializeGroovyStatements(null);
		List<HashMap<String,String>> results = 
				GroovyStatements.getInstance().searchElasticExchange("CAC");
		
		for(HashMap<String,String> result : results){
			LoggingDailyOperations.get_instance()
			.individual_stock_operations.put((String)result.get("symbol") + "_" + (String)result.get("fullName") + "_CAC" ,
					new StockOperations((String)result.get("symbol"),
							(String)result.get("fullName"), "CAC"));
		}
	
		
		ComplexCalculationRequest cer = 
				new ComplexCalculationRequest("AC.PA",
						"Accor S.A.",
						"CAC",
						"AC.PA_Accor S.A._CAC_stochastic_oscillator" +
						"_k_indicator_period_start_35_3_period_end_" +
						"subindicator_rises_by_1.5_percent_in_1_session", 
						"stochastic_oscillator_k_indicator",
						"subindicator_rises_by_1.5_percent_in_1_session", 
						null,
						null,
						16504,
						16517);
		
	}

}
