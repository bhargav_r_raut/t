package ComplexProcessor;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import eu.verdelhan.ta4j.Decimal;

public class Complex {

	public static BigDecimal TADecimal_to_BigDecimal(Decimal t) {
		
		/**
		System.out.println("ta tostring was;" + t.toString());
		System.out.println("we returned.");
		System.out.println(new BigDecimal(t.toString()).setScale(
				StockConstants.DataPoint_bd_scale,
				StockConstants.DataPoint_bd_rounding_mode));
		**/
		
		return new BigDecimal(t.toString()).setScale(
				StockConstants.DataPoint_bd_scale,
				StockConstants.DataPoint_bd_rounding_mode);

	}
	/**
	 * check whether the price map internal has the start day id that is desired.
	 * if not then the lower key is taken and returned.
	 * 
	 * @param e
	 * @return
	 */
	public static Integer get_corrected_start_day_id(Entity e,Integer start_day_id){
		if(!e.getPrice_map_internal().containsKey(start_day_id)){
			return e.getPrice_map_internal().lowerKey(start_day_id);
		}
		return start_day_id;
	}

	/****
	 * calls generate newly downloaded datapoints on the entity and then returns
	 * the firstkey.
	 * 
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public static Integer get_calculate_complex_from(Entity e) throws Exception {
		return e.generate_newly_downloaded_datapoints().firstKey();
	}
	
	public static String left_pad(String input_string) {

		input_string = StringUtils.leftPad(input_string, 2, "0");

		return input_string;
	}

	public static String get_date(DateTime d) {

		String date = "";

		date = String.valueOf(d.getYear()) + "-"
				+ left_pad(String.valueOf(d.getMonthOfYear())) + "-"
				+ left_pad(String.valueOf(d.getDayOfMonth()));

		return date;

	}
	
	/***
	 * special function for consecutive 
	 * 
	 * first calls {@link #get_calculate_complex_from(Entity)}
	 * 
	 * then takes the lower key to that key from the price_map_internal
	 * 
	 * if there is no lower key, (this will happen for the first time, then returns whatever the first call would)
	 * 
	 * @param e
	 * @param indicator_stub_name
	 * @param indicator_name
	 * @return
	 * @throws Exception 
	 */
	public static Integer get_calculate_complex_from_for_consecutive(Entity e) throws Exception{
		Integer default_key = get_calculate_complex_from(e);
		Integer lower_key = e.getPrice_map_internal().lowerKey(default_key);
		return lower_key == null ? default_key : lower_key;
	}
	
	
	public static Integer get_first_available_day_id_on_entity_with_valid_value_for_indicator(
			Entity e, String indicator_stub_name, String indicator_name)
			{
		
		
		for (Integer k : e.getPrice_map_internal().keySet()) {
			 

			if (e.getPrice_map_internal().get(k).getIndicator_values_on_datapoint()
					.get(indicator_stub_name) != null) {
				
				return k;
			}
		}

		LogObject loge = new LogObject(Complex.class, e);
		loge.add_to_info_packet("stub name", indicator_stub_name);
		loge.add_to_info_packet("indicator name", indicator_name);
		loge.commit_error_log(
				"first available day id with indicator calcualtion",
				new Exception(
						"could not find any day ids with a value for this indicator stub"));
		return null;

	}
}
