package ComplexProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.javatuples.Pair;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.PairArray;
import elasticSearchNative.LocalEs;

/****
 * this class is passed in an entity.
 * 
 * @1 before doing anything it first does a search query to get the names of all
 *    entities, as well as their last day ids, from elasticsearch. for this the
 *    function {@link #get_all_entity_unique_names()} is used. inside the
 *    function using the last day id of the entity, we also get its last valid
 *    pair index.
 * 
 *    then each of the indicator names registered on teh entity int the
 *    {@link Entity#getEntity_initial_stub_names()} is used .
 * 
 *    each indicator is passed to each of the individual complex_calculators.
 *    the complexcalculator after it calculates the required complexes, puts
 *    them into a {@link ResultHolder} the resultholder, contains a hashmap of
 *    {@link Result} objects, one for each individual stub for that particular
 *    indicator.
 * 
 *    the stubs are basically when we calculate the same indicator using
 *    different periods, if you use only one period then the resultholder will
 *    have only one result object.
 * 
 *    when the resultholder is instatiated(from inside the complex_calculators)
 *    it is passed in the entity, its es_id, the indicator name, the
 *    subindicator_name inside the constrcutor for the result_object we use the
 *    {@link CentralSingleton#indicators_jsonfile} to get and set the id of the
 *    indicator. ##note: it is currently not possible to support more than one
 *    stub for any indicator. this is because we currently only have one
 *    indicator id per indicator. this is used inside the {@link Result} object
 *    while commiting complexes to es, and also while committing to the various
 *    fst indices. In order to support more than one stub, please refer to the
 *    documentation inside {@link Result} where to the existing indicator id, a
 *    further thing will have to be appended depending upon the name of the
 *    complex.
 * 
 * 
 *    after the resultholder has been created and populated , then we run the
 *    {@link ResultHolder#commit(HashMap)} function on it. while doing this we
 *    pass in the unique_names and last day id and last valid pairs calculated
 *    in @1 to it. these are used inside the {@link Result} functions(commit)
 *    for doing whatever is necessary.
 * 
 * 
 * 
 * @author aditya
 * 
 */
public class ComplexCalculate implements Callable<ArrayList<ResultHolder>>,
		IComplexCalculate {

	private static final ArrayList<String> indicators_applicable_to_zero_line = new ArrayList<String>(
			Arrays.asList("awesome_oscillator_indicator",
					"moving_average_convergence_divergence",
					"acceleration_deceleration_indicator", "cci_indicator"));

	private static final ArrayList<String> indicators_applicable_to_rise_and_fall = new ArrayList<String>(
			Arrays.asList("open", "high", "low", "close"));

	private Entity e;

	public Entity getE() {
		return e;
	}

	public void setE(Entity e) {
		this.e = e;
	}

	public ComplexCalculate(Entity e) {
		this.e = e;
	}

	/*****
	 * how it works now is that complexes are calculated for
	 * each entity for all its indicators in one shot, 
	 * and results hashmap is generated.
	 * the hashmap is seperate for each subindicator type
	 * for each entity.
	 * but it will be for all the indicators.
	 * 
	 * so we will have a results map for a given subindicator -> for a given
	 * entity.
	 * so that hashmap will be like this:
	 * 
	 * day_id => [complex_1,complex_2,complex_3]
	 * 
	 * 
	 * now in order to generate the buy sell arrays, we first need to convert
	 * this to 
	 * complex_1 => on day ids
	 * complex_2 => on day ids
	 * complex_3 => on day ids.
	 * 
	 * 
	 * then we can send the update requests.
	 * this will land up holding a large number of these maps.
	 * so what to do for complex calculate?
	 * 
	 * do we make one thread per entity ?
	 * 
	 * this will land up processing one index at a time.
	 * ok so how to actually proces ??
	 * 
	 * complex calculate can be used.
	 * let us create a different complex calculate for each subindicator of each entity
	 * so that we can have things being sent to the bulk processor.
	 * 
	 * so entity and subindicators, can be sent like that into a bulk processor.
	 * and the "call" will have to be what?
	 * 
	 * basically it will call the calculate on the entity for that particular 
	 * subindicator class that is passed in.
	 * but how to pass in that class?
	 * so we will make them all implement an interface.
	 * and then call that interface, we will give the interface to subindicator 
	 * calculation.
	 * what will it return
	 * results hashmap.
	 * that will be reorganized on the caller thread
	 * and then it will do the update requests.
	 * 
	 * 
	 * what about subindicator applicability ?
	 * 
	 * first we are going to just 
	 * 
	 * 
	 * 35 complexes detected for a given indicator for all its subindicators.
	 * total 20 indicators. so about 700 complexes per entity per day. total 300
	 * entities. so total 2.1 lakh new complexes per day.
	 * 
	 * existing complexes: new complexes:
	 * 
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	@Override
	public ArrayList<ResultHolder> call() throws Exception {
		LogObject lg = new LogObject(getClass(), getE());
		lg.commit_info_log("init: calc complex");
		// HashMap<String, Pair<Integer, Integer>>
		// entity_unique_name_and_last_valid_pair_index =
		// get_all_entity_unique_names();

		Integer total_complexes_on_last_day = 0;

		ArrayList<ResultHolder> res_holder_arraylist = new ArrayList<ResultHolder>();

		for (String indicator_name : getE().getEntity_initial_stub_names()
				.keySet()) {
			System.out.println("now doing indicator name:" + indicator_name);

			if (indicators_applicable_to_rise_and_fall.contains(indicator_name)) {

				ResultHolder rise_or_fall_amount = IndicatorRiseFallAmount
						.calculate(e, indicator_name);

				res_holder_arraylist.add(rise_or_fall_amount);
			} else {

				//ResultHolder standard_deviation = IndicatorStandardDeviation
				//		.calculate(e, indicator_name);
				//res_holder_arraylist.add(standard_deviation);

			}
			
			/**
			if (indicators_applicable_to_zero_line.contains(indicator_name)) {
				// process it.
				System.out.println("entered zero line");
				ResultHolder zero_line = IndicatorZeroLine.calculate(e,
						indicator_name);
				res_holder_arraylist.add(zero_line);

			}

			ResultHolder patterns_holder = IndicatorPatterns.calculate(e,
					indicator_name);
			res_holder_arraylist.add(patterns_holder);

			ResultHolder high_low_holder = indicatorNDayHighLow
					.calculate_complex(e, indicator_name);
			res_holder_arraylist.add(high_low_holder);

			ResultHolder consecutive_holder = ConsecutiveAndPrevious.calculate(
					getE(), indicator_name);
			res_holder_arraylist.add(consecutive_holder);

			ResultHolder sma_cross = IndicatorSmaCross.calculate(e,
					indicator_name);
			res_holder_arraylist.add(sma_cross);
			**/
		}

		return res_holder_arraylist;
	}

	public void calculate_complexes(String initial_stub, String indicator_name,
			ArrayList<Integer> required_day_ids, ArrayList<Integer> all_day_ids) {

		/**
		 * IndicatorRiseAndFall iraf = new IndicatorRiseAndFall(getE(),
		 * required_day_ids, all_day_ids, initial_stub, indicator_name);
		 * 
		 * LinkedHashMap<String, Double> pair_differences_map = iraf
		 * .getPair_differences_map();
		 **/
		/**
		 * indicator rises for n sessions indicator patterns.
		 * 
		 * 
		 * IndicatorRisesFallsForNDays irfnd = new IndicatorRisesFallsForNDays(
		 * pair_differences_map, required_day_ids, all_day_ids, initial_stub);
		 * for (Map.Entry<String, ComplexChange> entry : irfnd
		 * .getComplex_changes_map().entrySet()) {
		 * 
		 * }
		 **/
		/**
		 * indicator shows n session high low.
		 * 
		 */
		/**
		 * IndicatorNDayHighLow indh = new IndicatorNDayHighLow(
		 * indicator_values_from_beginning_of_time, required_day_ids,
		 * all_day_ids, initial_stub);
		 * 
		 * for (Map.Entry<String, ComplexChange> entry : indh
		 * .getComplex_changes_map().entrySet()) {
		 * 
		 * }
		 **/
		/***
		 * indicator signal line and zero line complexes calculation.
		 * 
		 * 
		 */
		/**
		 * try { IndicatorSignalLineAndSmaCalculations sma_zero_complexes = new
		 * IndicatorSignalLineAndSmaCalculations(
		 * indicator_values_from_beginning_of_time, required_day_ids,
		 * all_day_ids, initial_stub,
		 * StockConstants.periods_for_sma_calculations); for (Map.Entry<String,
		 * ComplexChange> entry : sma_zero_complexes
		 * .getComplex_changes_map().entrySet()) {
		 * 
		 * } } catch (JSONException | IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 **/
		// finally send all calculated complexes to jedis.
		// send_calculated_complexes_to_jedis_popper();
	}

	/****
	 * return the name of each entity in the entire database, and its last valid
	 * pair the last valid pair is calculated using the last day id of the
	 * entity.
	 * 
	 */
	@Override
	public HashMap<String, Pair<Integer, Integer>> get_all_entity_unique_names() {

		HashMap<String, Pair<Integer, Integer>> hm = new LinkedHashMap<String, Pair<Integer, Integer>>();

		SearchResponse response = LocalEs.getInstance()
				.prepareSearch("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.matchAllQuery())
				.addFields("unique_name", "es_linked_list.day_id").setSize(500)
				.execute().actionGet();

		for (SearchHit hit : response.getHits().getHits()) {

			ArrayList<Object> day_ids = (ArrayList<Object>) hit.getFields()
					.get("es_linked_list.day_id").values();

			String unique_name = (String) hit.getFields().get("unique_name")
					.value();
			Integer last_day_id = (Integer) day_ids.get(day_ids.size() - 1);
			Integer index_of_last_valid_pair_in_close_close_price_changes_int_arr = get_last_valid_pair(last_day_id);
			LogObject lgd = new LogObject(ComplexCalculate.class);
			lgd.commit_debug_log("unique name is:"
					+ unique_name
					+ " and last day id is:"
					+ day_ids.get(day_ids.size() - 1)
					+ " and last valid pair is :"
					+ index_of_last_valid_pair_in_close_close_price_changes_int_arr);

			hm.put(unique_name,
					new Pair<Integer, Integer>(last_day_id,
							index_of_last_valid_pair_in_close_close_price_changes_int_arr));

		}

		return hm;
	}

	/***
	 * the last key in the price map internal defines the last day id for which
	 * pairs can be calculated. any pair whose day id, ends after the last day
	 * id, cannot be considered valid. it will return 0 from the close_close /
	 * open_open unsorted pairs arrays, and this will be wrong.
	 * 
	 * so this method to calculate what is the last valid index in the unsorted
	 * pairs map that returns an actual valid pair. it is just the index for
	 * (last_key - 1)->last_key. it doesnt mean that we have something on
	 * last_key - 1, but only defines the upper limit of valid indices. in the
	 * entire program we only query for valid pairs.
	 * 
	 * {@link #PairArray#update_t}
	 * 
	 */
	@Override
	public Integer get_last_valid_pair(Integer last_day_id) {
		// TODO Auto-generated method stub
		return PairArray.get_int_array_index_given_start_day_id_and_difference(
				last_day_id - 1, 1).getPair_difference_array_index();
	}
}
