package ComplexProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.javatuples.Pair;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;

public class ConsecutiveAndPrevious {

	public static final String subindicator_name = "consecutive_periods";
	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			Arrays.asList(1, 2, 3, 4, 5, 6));

	public static final ArrayList<Integer> weeks_or_months_or_years = new ArrayList<Integer>(
			Arrays.asList(0, 1, 2));

	public static final LinkedHashMap<Integer, String> weeks_or_months_or_years_correlation = new LinkedHashMap<Integer, String>() {
		{
			put(0, "weeks");
			put(1, "months");
			put(2, "years");
		}
	};

	public static final Integer default_period_for_all_logs = 1;

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_processed_because_they_were_last_days";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					for (String weeks_months_or_years : weeks_or_months_or_years_correlation
							.values()) {
						put(initial_stub + "_" + weeks_months_or_years,
								new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
									{

										put(default_period_for_all_logs,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());

														put(day_ids_actually_processed,
																new ArrayList<Object>());
													}
												});

									}
								});
					}

				}
			}
		};

		return report_log;
	}

	/****
	 * 
	 * in this calculation the calculate complexes from proceeds from one day
	 * before the actual calculate complexes from Reason:
	 * 
	 * the only way to know if something is the last week,month, or year is to
	 * have a later day, which belongs to a new week etc.
	 * 
	 * So for whatever is the last downloaded day, it may be the last day of any
	 * of the above, but we wont know about that till we get the next value.
	 * 
	 * so in essence we only find out that something is the last day, one day
	 * later, and hence need to start the calculate complexes from one day
	 * earlier than the actual calculated complexes from.
	 * 
	 * so we use the special calculate complexes from function which returns one
	 * key lower than the actual calculated complex from.
	 * 
	 * basically datapoints record last_day_of_week/month/year only upto one day
	 * before the latest downloaded day. the latest downloaded day does not have
	 * this attribute set to either zero or one.
	 * 
	 * a detailed explanation is available in
	 * {@link Entity#update_last_day_of_week_month_and_year()}
	 * 
	 * 
	 * --how to read the report log
	 * 
	 * the log has a default period of 1, because there is no concept of periods
	 * here. it however has a seperate stub name for weeks , months and years(by
	 * appending weeks_month_or_years to the initial stub name)
	 * 
	 * so there will be three times the number of initial stubs in the final
	 * report.
	 * 
	 * 
	 * 
	 * - in this method, first we check whether whatever id is being processed
	 * is greater or equal than the first_avialable_day_id this is because, we
	 * need the indicator value, for that particular day.
	 * 
	 * after that we only process the day if it is a last day of
	 * week/month/year, because all we care about is the difference between
	 * successive last days. otherwise it is rejected
	 * 
	 * finally after processing it, we check if it is greater than the calculate
	 * complex from. otherwise we reject it.
	 * 
	 * so in this case in the log:
	 * 
	 * 
	 * the "day_ids_actually_processed" will be more than the day ids accepted
	 * due to >= calculated complexes from.
	 * 
	 * because we process everythin, that is a last day, but only generate the
	 * names for those ones which are greater than the calculated complexes
	 * from.
	 * 
	 * 
	 * @throws Exception
	 * 
	 */

	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		// print_all_last_days_with_datestrings(e);

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from_for_consecutive(e);

		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		/****
		 * here this is the lower key than the last key in the price map
		 * internal, because we can only calculate consecutive previous for one
		 * day before the last available day.
		 * 
		 * in all other subindicators, the last applicable day for complex
		 * calculation is the last day in the price map internal.
		 * 
		 */
		

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name, e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);
		
		Integer last_applicable_day_for_complex_calculation = e
				.getPrice_map_internal().lowerKey(
						e.getPrice_map_internal().lastKey());
		
		lg_report.add_to_info_packet(
				"last available day for complex calculation : (SPECIAL)",
				last_applicable_day_for_complex_calculation);

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());

		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));

		Integer stub_counter = 0;

		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(initial_stub_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity, res_holder.getIndicator_id(),
					stub_counter, e.getEntity_es_id(),last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			/***
			 * store the
			 * 
			 * @key -> day id value ->
			 * @value -> pair
			 * @value0 ->indicator_integer_value
			 * @value1 -> 1 or -1, if greater or less than earlier value.
			 * 
			 * 
			 */
			TreeMap<Integer, Pair<Integer, Integer>> last_day_of_weeks = new TreeMap<Integer, Pair<Integer, Integer>>();
			TreeMap<Integer, Pair<Integer, Integer>> last_day_of_months = new TreeMap<Integer, Pair<Integer, Integer>>();
			TreeMap<Integer, Pair<Integer, Integer>> last_day_of_years = new TreeMap<Integer, Pair<Integer, Integer>>();

			for (Map.Entry<Integer, DataPoint> entry : e
					.getPrice_map_internal().entrySet()) {
				// System.out.println("doing day id:" + entry.getKey());
				// the last key does not have any value for either
				// last_day_of_week or month or year, neither 0 or 1
				// so it will throw a null pointer if it is allowed to be
				// processsed. so we ensure that its not processed
				// by setting the second condition.

				if (entry.getKey().intValue() >= first_available_day_id_on_entity_with_value_for_this_indicator
						.intValue()
						&& entry.getKey() != e.getPrice_map_internal()
								.lastKey()) {

					lg_report.report_log_map
							.get(initial_stub_name + "_weeks")
							.get(default_period_for_all_logs)
							.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

					lg_report.report_log_map
							.get(initial_stub_name + "_months")
							.get(default_period_for_all_logs)
							.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

					lg_report.report_log_map
							.get(initial_stub_name + "_years")
							.get(default_period_for_all_logs)
							.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

					Integer previous_day_id;

					if (entry.getValue().getLast_day_of_week().equals(1)) {

						previous_day_id = last_day_of_weeks.isEmpty() ? null
								: last_day_of_weeks.lastKey();

						last_day_of_weeks.put(entry.getKey(),
								new Pair<Integer, Integer>(entry.getValue()
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value(), null));

						// here provided that the day id is greater than or
						// equal to one day before the
						// required calculate complexes from , then proceed.

						Integer consecutive_weeks = get_consecutive_number(
								last_day_of_weeks, entry.getKey(),
								previous_day_id);

						lg_report.report_log_map
								.get(initial_stub_name + "_weeks")
								.get(default_period_for_all_logs)
								.get(day_ids_actually_processed)
								.add(entry.getKey().intValue());

						if (entry.getKey().intValue() >= calculate_complexes_from
								&& consecutive_weeks != null) {
							// send for getting the name

							res.add_results(
									IdAndShardResolver
											.get_names_for_consecutive_weeks_months_years(
													weeks_or_months_or_years
															.get(0),
													consecutive_weeks), entry
											.getKey().intValue());
							// System.out.println("the printed map looks like:");
							// print_custom_treemap_for_test(last_day_of_weeks);
							// System.out.println("got successive change:"
							// + consecutive_weeks + " and the day id is:"
							// + entry.getKey().intValue());
							// System.out.println(e.getPrice_map_internal()
							// .get(entry.getKey()).getDateString());
							// System.in.read();

							lg_report.report_log_map
									.get(initial_stub_name + "_weeks")
									.get(default_period_for_all_logs)
									.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
									.add(entry.getKey().intValue());
						} else {

							lg_report.report_log_map
									.get(initial_stub_name + "_weeks")
									.get(default_period_for_all_logs)
									.get(day_ids_rejected_dt_lt_calculate_complexes_from)
									.add(entry.getKey().intValue());

						}

					}

					if (entry.getValue().getLast_day_of_month().equals(1)) {

						previous_day_id = last_day_of_months.isEmpty() ? null
								: last_day_of_months.lastKey();

						last_day_of_months.put(entry.getKey(),
								new Pair<Integer, Integer>(entry.getValue()
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value(), null));
						Integer consecutive_months = get_consecutive_number(
								last_day_of_months, entry.getKey(),
								previous_day_id);

						lg_report.report_log_map
								.get(initial_stub_name + "_months")
								.get(default_period_for_all_logs)
								.get(day_ids_actually_processed)
								.add(entry.getKey().intValue());

						if (entry.getKey().intValue() >= calculate_complexes_from
								&& consecutive_months != null) {
							lg_report.report_log_map
									.get(initial_stub_name + "_months")
									.get(default_period_for_all_logs)
									.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
									.add(entry.getKey().intValue());
							res.add_results(
									IdAndShardResolver
											.get_names_for_consecutive_weeks_months_years(
													weeks_or_months_or_years
															.get(1),
													consecutive_months), entry
											.getKey().intValue());
						} else {

							lg_report.report_log_map
									.get(initial_stub_name + "_months")
									.get(default_period_for_all_logs)
									.get(day_ids_rejected_dt_lt_calculate_complexes_from)
									.add(entry.getKey().intValue());

						}
					}

					if (entry.getValue().getLast_day_of_year().equals(1)) {

						previous_day_id = last_day_of_years.isEmpty() ? null
								: last_day_of_years.lastKey();

						last_day_of_years.put(entry.getKey(),
								new Pair<Integer, Integer>(entry.getValue()
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value(), null));
						Integer consecutive_years = get_consecutive_number(
								last_day_of_years, entry.getKey(),
								previous_day_id);

						lg_report.report_log_map
								.get(initial_stub_name + "_years")
								.get(default_period_for_all_logs)
								.get(day_ids_actually_processed)
								.add(entry.getKey().intValue());

						if (entry.getKey().intValue() >= calculate_complexes_from
								&& consecutive_years != null) {
							// send for getting the name
							lg_report.report_log_map
									.get(initial_stub_name + "_years")
									.get(default_period_for_all_logs)
									.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
									.add(entry.getKey().intValue());

							res.add_results(
									IdAndShardResolver
											.get_names_for_consecutive_weeks_months_years(
													weeks_or_months_or_years
															.get(2),
													consecutive_years), entry
											.getKey().intValue());
						} else {
							lg_report.report_log_map
									.get(initial_stub_name + "_years")
									.get(default_period_for_all_logs)
									.get(day_ids_rejected_dt_lt_calculate_complexes_from)
									.add(entry.getKey().intValue());

						}
					}

				} else {

					lg_report.report_log_map
							.get(initial_stub_name + "_weeks")
							.get(default_period_for_all_logs)
							.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

					lg_report.report_log_map
							.get(initial_stub_name + "_months")
							.get(default_period_for_all_logs)
							.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

					lg_report.report_log_map
							.get(initial_stub_name + "_years")
							.get(default_period_for_all_logs)
							.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
							.add(entry.getKey().intValue());

				}
			}

			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			res.report_result();
			stub_counter++;
		}

		lg_report.commit_report_log("completed");
		// System.in.read();
		return res_holder;

	}

	/***
	 * how this works
	 * 
	 * @first: get the previous day id, i.e last key in the relevant treemap. if
	 *         the previous day id is null, then it means that we are processing
	 *         the first day id ever, in that case just return null.
	 * 
	 * @second: get the 'difference' i.e the difference between the current and
	 *          the previous indicator integer values. this returns 1 if greater
	 *          than or equal and -1 if less. the current day ids indicator
	 *          value is put into the relevant map, before entering this
	 *          function but its difference from the previous value is put
	 *          inside this function, just before building the descending map.
	 *          so when the relevant map enters, for this day id, the pair has
	 *          null for the difference(value1). so after getting the
	 *          difference, set it at value1 for this day id in the map.
	 * 
	 * @third : make a map of the relevant treemap starting from the first
	 *        key,but exclusive because the first key will always have null for
	 *        the difference ,and upto the last key but inclusive.(we want to
	 *        start the consecutive counter as zero and so start counting from
	 *        the day id being processed itself). Take a descending treemap of
	 *        this map, because we want to go back from the most recent day id.
	 * 
	 * @finally:just increment the consecutive counter till the difference sign
	 *               is the same, otherwise break.
	 * 
	 * @return: the consecutive weeks * difference -> to indicate rising or
	 *          falling for those many consecutive weeks.
	 * 
	 * @param relevant_treemap
	 *            : the last_days_weeks | last_days_months | last_days_years
	 * @param current_day_id
	 *            : the day id of the day being processed.
	 * @return Integer : consecutive counter
	 */
	private static Integer get_consecutive_number(
			TreeMap<Integer, Pair<Integer, Integer>> relevant_treemap,
			Integer current_day_id, Integer previous_day_id) {

		// ObjectMapper mapper = new ObjectMapper();

		// System.out.println("entered get consecutive");

		// System.out.println("the treemap looks like:");
		// print_custom_treemap_for_test(relevant_treemap);

		// System.out.println("previous day id is:" + previous_day_id);

		if (previous_day_id == null) {
			// System.out.println("it is null so returning null");
			return null;
		}
		Integer current_indicator_value = relevant_treemap.get(current_day_id)
				.getValue0();

		// System.out.println("current indicator value: "
		// + current_indicator_value);

		Integer previous_indicator_Value = relevant_treemap
				.get(previous_day_id).getValue0();
		// System.out.println("previous indicator value:"
		// + previous_indicator_Value);

		Integer difference = current_indicator_value.intValue() >= previous_indicator_Value
				.intValue() ? 1 : -1;

		// System.out.println("difference is:" + difference);

		Pair<Integer, Integer> p = relevant_treemap.get(current_day_id).setAt1(
				difference);
		relevant_treemap.put(current_day_id, p);

		// System.out.println("relevant treemap after setting difference:");
		// print_custom_treemap_for_test(relevant_treemap);

		NavigableMap<Integer, Pair<Integer, Integer>> map_upto_and_inclusive_current_day_id = relevant_treemap
				.subMap(relevant_treemap.firstKey(), false, current_day_id,
						true).descendingMap();

		// System.out.println("the submap becomes:");
		// print_custom_treemap_for_test(map_upto_and_inclusive_current_day_id);

		Integer consecutive_counter = 0;

		for (Map.Entry<Integer, Pair<Integer, Integer>> entry : map_upto_and_inclusive_current_day_id
				.entrySet()) {
			// System.out.println("now iterating day id:" + entry.getKey()
			// + " of the submap:");
			// System.out.println("the differnce at this point is: "
			// + entry.getValue().getValue1());
			if (entry.getValue().getValue1().intValue() == difference
					.intValue()) {
				// System.out.println("same as our difference so increment.");
				consecutive_counter++;
			} else {
				// System.out.println("different from our diference, so break");
				break;
			}
		}

		// testing.

		// suppose the counter is more than 6, it defaults to six.
		consecutive_counter = consecutive_counter > 6 ? 6 : consecutive_counter;

		return consecutive_counter * difference;
	}

	private static void print_custom_treemap_for_test(
			Map<Integer, Pair<Integer, Integer>> tm) {
		for (Map.Entry<Integer, Pair<Integer, Integer>> entry : tm.entrySet()) {
			System.out.println("day id:" + entry.getKey() + " and value:"
					+ entry.getValue().getValue0() + "-"
					+ entry.getValue().getValue1());
		}
	}

	private static void print_all_last_days_with_datestrings(Entity e) {
		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal()
				.entrySet()) {
			String report_string = "";
			report_string += "day_id: " + entry.getKey() + " datestring: "
					+ entry.getValue().getDateString();

			if (entry.getValue().last_day_of_week != null) {
				if (entry.getValue().last_day_of_week == 1) {
					report_string += " week: yes";
				} else {
					report_string += " week:no";
				}
			}

			if (entry.getValue().last_day_of_month != null) {
				if (entry.getValue().last_day_of_month == 1) {
					report_string += " month: yes";

				} else {
					report_string += " month: no";
				}
			}

			if (entry.getValue().last_day_of_year != null) {
				if (entry.getValue().last_day_of_year == 1) {
					report_string += " year: yes";
				} else {
					report_string += " year: no";
				}
			}
			System.out.println(report_string);
		}
		try {
			System.in.read();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
