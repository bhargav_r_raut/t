package ComplexProcessor;

import java.util.HashMap;

import org.javatuples.Pair;

public interface IComplexCalculate {

	public HashMap<String, Pair<Integer, Integer>> get_all_entity_unique_names();
	
	public abstract Integer get_last_valid_pair(Integer last_day_id);
	
}
