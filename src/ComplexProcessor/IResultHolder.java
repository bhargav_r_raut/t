package ComplexProcessor;

import java.util.ArrayList;
import java.util.HashMap;

import org.javatuples.Pair;

public interface IResultHolder {

	public abstract Integer latest_day_id_has_n_results(
			HashMap<String, Pair<Integer, Integer>> entities_unique_names_and_last_day_ids_and_last_valid_indexes);

	public abstract void before_commit()
			throws Exception;
	
	public abstract HashMap<String, Pair<ArrayList<Integer>,Boolean>> build_complex_came_on_which_day_ids_hashmap() throws Exception;
	
	public abstract void build_fst(
			HashMap<String, Pair<Integer, Integer>> entities_unique_names_and_last_day_ids_and_last_valid_indexes)
			throws Exception;

	public abstract void after_commit() throws Exception;
}
