package ComplexProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;

public class IndicatorPatterns {

	private static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10));

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String subindicator_name = "patterns";

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());

														put(day_ids_actually_processed,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from(e);

		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name,e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);
		
		
		Integer last_applicable_day_for_complex_calculation =
				e.getPrice_map_internal().lastKey();
		

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());
		
		
		/////////////////////////////////////////
		
		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));
		
		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(initial_stub_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity,res_holder.getIndicator_id(),stub_counter,e.getEntity_es_id(),
					last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);
			// System.out.println("first available day id with entry for stub:"
			// +
			// first_available_day_id_on_entity_with_value_for_this_indicator);
			// System.in.read();
			// System.out.println("now doing for period: " + period);
			for (Integer end_day_id : e.getPrice_map_internal().keySet()) {
				// System.out.println("now oding for day id:" + end_day_id);
				// if the day id is greater than the
				// LogObject lgd = new
				// LogObject(IndicatorRiseFallAmount.class,
				// e);
				// lgd.commit_debug_log("doing end day id:" + end_day_id);
				// System.out.println("doing end day id:" + end_day_id);
				if (end_day_id >= calculate_complexes_from) {
					// System.out.println("this day id is gt than calc from");
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
								.add(end_day_id);
						// now for each period , we need to get the shit.

						// System.out.println("end day id is greater than calc complexes from:"
						// + end_day_id);

						// lgd1.add_to_info_packet("period", period);

						Integer start_day_id = e
								.generate_day_id_n_days_before_given_day_id(
										end_day_id, period);
						/**
						 * System.out .println("doing period:" + period +
						 * " for end day id:" + end_day_id +
						 * " with start day id as:" + start_day_id +
						 * " and first avail day id:" +
						 * first_available_day_id_on_entity_with_value_for_this_indicator
						 * );
						 **/
						// System.out.println("start day id becomes:" +
						// start_day_id);

						// lgd1.add_to_info_packet("start day id becomes",
						// start_day_id);
						// System.out.println("start day id becomes:" +
						// start_day_id);
						if (start_day_id != null
								&& start_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

							// System.out.println("this day id is greater than first day in entity");

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
									.add(end_day_id);

							lg_report.report_log_map.get(initial_stub_name)
									.get(period)
									.get(day_ids_actually_processed)
									.add(end_day_id);

							// go from start day id to end day id and provided
							// convert it to 1-2 format
							String pattern = "";

							ArrayList<Integer> day_ids = new ArrayList<Integer>(
									e.getPrice_map_internal()
											.subMap(start_day_id, true,
													end_day_id, true).keySet());

							// System.out
							// .println("the day ids got in between the start and end days are:");
							// System.out.println(day_ids);

							// System.in.read();
							// HashMap<String, Integer> differences_for_debug =
							// new LinkedHashMap<String, Integer>();

							for (int i = 0; i < day_ids.size() - 1; i++) {
								// pattern difference is the
								Integer st_day_id = day_ids.get(i);
								Integer en_day_id = day_ids.get(i + 1);
								/**
								 * differences_for_debug.put("start day id: " +
								 * st_day_id + " => end_day_id:" + en_day_id,
								 * en_day_id - st_day_id); differences_for_debug
								 * .put("start day indicator: " +
								 * e.getPrice_map_internal() .get(st_day_id)
								 * .getIndicator_values_on_datapoint()
								 * .get(initial_stub_name)
								 * .getIndicator_integer_value() +
								 * " => end_day_indicator:" +
								 * e.getPrice_map_internal() .get(en_day_id)
								 * .getIndicator_values_on_datapoint()
								 * .get(initial_stub_name)
								 * .getIndicator_integer_value(),
								 * e.getPrice_map_internal() .get(en_day_id)
								 * .getIndicator_values_on_datapoint()
								 * .get(initial_stub_name)
								 * .getIndicator_integer_value() -
								 * e.getPrice_map_internal() .get(st_day_id)
								 * .getIndicator_values_on_datapoint()
								 * .get(initial_stub_name)
								 * .getIndicator_integer_value());
								 * 
								 * ObjectMapper mapper = new ObjectMapper();
								 * System.out.println(mapper
								 * .defaultPrettyPrintingWriter()
								 * .writeValueAsString(differences_for_debug) );
								 **/
								if (e.getPrice_map_internal().get(en_day_id)
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value()

								>=

								e.getPrice_map_internal().get(st_day_id)
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value()) {

									// System.out.println("the indicator value at the end day is:"
									// +
									// e.getPrice_map_internal().get(en_day_id)
									// .getIndicator_values_on_datapoint()
									// .get(initial_stub_name)
									// .getIndicator_integer_value());
									// System.out.println("the indicator value at the start day is"
									// +
									// e.getPrice_map_internal().get(st_day_id)
									// .getIndicator_values_on_datapoint()
									// .get(initial_stub_name)
									// .getIndicator_integer_value());

									pattern += "1";

									// System.out.println("pattern was made 1");
									// System.out.println("pattern now is:" +
									// pattern);
									// System.in.read();
								} else {
									pattern += "2";
									// System.out.println("pattern was made 2");
									// System.out.println("pattern now is:" +
									// pattern);
									// System.in.read();
								}

							}

							// now send the pattern in and get the result.
							// System.out.println("full pattern:" + pattern);

							res.add_results(
									IdAndShardResolver
											.get_subindicator_name_for_permutations(pattern),
									end_day_id);

							// System.in.read();

						} else {

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
									.add(end_day_id);
						}
					}
				} else {
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_rejected_dt_lt_calculate_complexes_from)
								.add(end_day_id);
					}

				}
			}

			res.report_result();
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			// System.in.read();
			stub_counter++;
		}

		lg_report.commit_report_log("completed");
		System.in.read();
		return res_holder;
	}
}
