package ComplexProcessor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import org.hibernate.validator.internal.util.privilegedactions.GetClassLoader;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.PercentageIntegerCorrelate;
import IdAndShardResolver.IdAndShardResolver;

public class IndicatorRiseFallAmount {

	public static final String subindicator_name = "Indicator Rises by or Falls by Amounts";

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());

														put(day_ids_actually_processed,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		String decompressed_array_name = e.getUnique_name() + "_"
				+ StockConstants.Entity_close_close_map_suffix;

		if (indicator_name.equals("open")) {
			decompressed_array_name = e.getUnique_name() + "_"
					+ StockConstants.Entity_open_open_map_suffix;
		}

		// System.out.println("determined the name of the arr as:"
		// + decompressed_array_name);
		int[] decompressed_arr = PairArray.get_decompressed_arr(
				decompressed_array_name, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);

		// System.in.read();

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from(e);
		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name, e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);
		
		Integer last_applicable_day_for_complex_calculation = e.getPrice_map_internal().lastKey();
		

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());
		
		
		//////////////////////////////////////////////////////////////////////////////////
		
		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));

		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(indicator_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity, res_holder.getIndicator_id(),
					stub_counter, e.getEntity_es_id(),last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			// System.out
			// .println("first available day id on entity with value for stub is:"
			// +
			// first_available_day_id_on_entity_with_value_for_this_indicator);

			// System.out.println("the stub name is: " + initial_stub_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			// System.out.println("now doing for period: " + period);
			for (Integer end_day_id : e.getPrice_map_internal().keySet()) {
				// System.out.println("now doing for day id:" + end_day_id);
				// if the day id is greater than the
				// LogObject lgd = new
				// LogObject(IndicatorRiseFallAmount.class,
				// e);
				// lgd.commit_debug_log("doing end day id:" + end_day_id);
				// System.out.println("doing end day id:" + end_day_id);
				if (end_day_id >= calculate_complexes_from) {
					// System.out.println("this day id is gt than calc from");
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
								.add(end_day_id);
						// now for each period , we need to get the shit.

						// System.out.println("end day id is greater than calc complexes from:"
						// + end_day_id);

						// lgd1.add_to_info_packet("period", period);
						// System.out.println("period is:" + period);
						Integer start_day_id = e
								.generate_day_id_n_days_before_given_day_id(
										end_day_id, period);

						// System.out.println("start day id becomes:"
						// + start_day_id);

						// lgd1.add_to_info_packet("start day id becomes",
						// start_day_id);
						// System.out.println("start day id becomes:" +
						// start_day_id);
						if (start_day_id != null
								&& start_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

							// System.out
							// .println("this day id is greater than first day in entity");

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
									.add(end_day_id);

							lg_report.report_log_map.get(initial_stub_name)
									.get(period)
									.get(day_ids_actually_processed)
									.add(end_day_id);
							// lgd1.add_to_info_packet("start day id is greater than or equal to first day id",
							// start_day_id);
							// lgd1.commit_debug_log("c");
							// System.out.println("the start day id is greater than or equal to the first day id:"
							// + start_day_id);
							Integer diff = null;

							if (indicator_name.equals("close")) {

								Integer index_of_pair = PairArray
										.get_int_array_index_given_start_and_end_day_id(
												start_day_id, end_day_id, e)
										.getPair_difference_array_index();
								// GregorianCalendar start_cal = new
								// GregorianCalendar();
								diff = decompressed_arr[index_of_pair];
								// System.out.println("first diff for close was:"
								// + diff);

							} else if (indicator_name.equals("open")) {

								Integer index_of_pair = PairArray
										.get_int_array_index_given_start_and_end_day_id(
												start_day_id, end_day_id, e)
										.getPair_difference_array_index();
								// System.out.println("index of pair is:" +
								// index_of_pair);
								diff = decompressed_arr[index_of_pair];
								// /System.out.println("first diff for open was:"
								// + diff);
								// System.out.println("the index of pair is:" +
								// index_of_pair);
								// System.out.println("the diff is:" + diff);

								// if(period == 2){
								// System.in.read();
								// }

							} else {

								DataPoint end_day_id_datapoint = e
										.getPrice_map_internal()
										.get(end_day_id);
								DataPoint start_day_id_datapoint = e
										.getPrice_map_internal().get(
												start_day_id);
								PercentageIntegerCorrelate pchl = start_day_id_datapoint
										.calculate_differences_from_end_day_datapoint(
												end_day_id_datapoint, true);
								if (indicator_name.equals("high")) {
									// System.out.println("came to calculate high pc");
									// System.out.println("pchl is:" +
									// pchl.toString());
									diff = pchl
											.getHigh_high_price_change_correlate();
									// System.in.read();
								} else if (indicator_name.equals("low")) {
									// System.out.println("came to calculate low pc");
									diff = pchl
											.getLow_low_price_change_correlate();
								}

							}

							// System.out.println("diff was: " + diff);

							res.add_results(
									IdAndShardResolver
											.get_rise_and_fall_amount_subindicators_names(
													period, diff, diff >= 0 ? 0
															: 1), end_day_id);

							// System.in.read();

						} else {
							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
									.add(end_day_id);
						}
					}
				} else {
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_rejected_dt_lt_calculate_complexes_from)
								.add(end_day_id);
					}

				}
			}

			res.report_result();
			LogObject lgd = new LogObject(IndicatorRiseFallAmount.class);
			lgd.add_to_info_packet("put into res holder with id",
					res.getComplex_id_suffix());
			lgd.add_to_info_packet("put in total results", res
					.getNames_and_aliases().size());
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			lgd.add_to_info_packet("checking back from res holder", res_holder
					.getHm().get(res.getComplex_id_suffix())
					.getNames_and_aliases().size());
			stub_counter++;
			
		}

		lg_report.commit_report_log("completed");
		// /System.in.read();
		return res_holder;
	}

}
