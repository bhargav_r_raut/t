package ComplexProcessor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.joda.time.DateTime;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;

public class IndicatorSmaCross {

	private static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String subindicator_name = "sma_cross";

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	private static final Integer default_period_for_logs = 10000;

	private static final String day_ids_where_sma_could_not_be_generated_due_to_less_days = "day_ids_where_smas_could_not_be_generated_due_to_less_days";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									put(default_period_for_logs,
											new LinkedHashMap<String, ArrayList<Object>>() {
												{
													put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
															new ArrayList<Object>());
													put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
															new ArrayList<Object>());
													put(day_ids_rejected_dt_lt_calculate_complexes_from,
															new ArrayList<Object>());
													put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
															new ArrayList<Object>());

													put(day_ids_actually_processed,
															new ArrayList<Object>());
												}
											});

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_where_sma_could_not_be_generated_due_to_less_days,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	private static void compare_two_periods(int curr_day_period,
			int prev_day_period, HashMap<Integer, Decimal> prev_day_smas,
			HashMap<Integer, Decimal> curr_day_smas, Result r,
			Integer end_day_id) throws IOException {

		if (prev_day_period != curr_day_period) {
			// first check if both periods are present in both hashmaps.
			Decimal pd_cp = prev_day_smas.get(curr_day_period);
			Decimal pd_pp = prev_day_smas.get(prev_day_period);
			Decimal cd_cp = curr_day_smas.get(curr_day_period);
			Decimal cd_pp = curr_day_smas.get(prev_day_period);

			LogObject lg = new LogObject(IndicatorSmaCross.class, r);
			lg.add_to_info_packet("curr day period(can also call period A)",
					curr_day_period);
			lg.add_to_info_packet("prev day period(can also call period B):",
					prev_day_period);
			lg.add_to_info_packet("prev hm value for curr period:", pd_cp);
			lg.add_to_info_packet("prev hm value for prev period:", pd_pp);
			lg.add_to_info_packet("curr hm value for curr period:", cd_cp);
			lg.add_to_info_packet("curr hm value for prev period:", cd_pp);

			if (pd_cp != null && pd_pp != null && cd_cp != null
					&& cd_pp != null) {

				if (cd_cp.compareTo(cd_pp) > 0 && pd_cp.compareTo(pd_pp) <= 0) {
					// crosser is the curr day period.
					// crossed is the prev day period.
					lg.add_to_info_packet("crosser", curr_day_period);
					lg.add_to_info_packet("crossed is", prev_day_period);
					lg.commit_debug_log("current day-current period("
							+ curr_day_period + "):" + cd_cp.toDouble() + " > "
							+ " current day-previous period(" + prev_day_period
							+ "):" + cd_pp.toDouble() + " and " + " "
							+ "previous day-current period(" + curr_day_period
							+ "):" + pd_cp.toDouble() + " <= "
							+ " previous day-preiovus period("
							+ prev_day_period + "):" + pd_pp.toDouble());

					r.add_results(IdAndShardResolver
							.get_names_of_sma_subindicators(curr_day_period,
									prev_day_period), end_day_id);

				} else if (cd_cp.compareTo(cd_pp) < 0
						&& pd_cp.compareTo(pd_pp) >= 0) {
					// crossed is the curr day period.
					// crosser is the prev day period.
					lg.add_to_info_packet("crosser", prev_day_period);
					lg.add_to_info_packet("crossed is", curr_day_period);
					lg.commit_debug_log("current day-current period("
							+ curr_day_period + "):" + cd_cp.toDouble() + " < "
							+ " current day-previous period(" + prev_day_period
							+ "):" + cd_pp.toDouble() + " and " + " "
							+ "previous day-current period(" + curr_day_period
							+ "):" + pd_cp.toDouble() + " >= "
							+ " previous day-preiovus period("
							+ prev_day_period + "):" + pd_pp.toDouble());

					r.add_results(IdAndShardResolver
							.get_names_of_sma_subindicators(prev_day_period,
									curr_day_period), end_day_id);

				}

			} else {
				lg.commit_debug_log("cross conditions not satisfied");
			}
			//System.in.read();
		}

	}

	/****
	 * first generate all the sma's for the said indicator. use the indicator
	 * integer value for that.
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from_for_consecutive(e);

		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name,e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);
		
		
		Integer last_applicable_day_for_complex_calculation = e.getPrice_map_internal().lastKey();
		

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());
		
		///////////////////////////////////////////////////////////////
		
		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));
		
		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(indicator_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity,res_holder.getIndicator_id(),stub_counter,e.getEntity_es_id(),
					last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			/************
			 * 
			 * BUILD A TICK LIST FOR ALL THE DAYS THAT WE HAVE TILL TODAY AND
			 * INCLUSIVE.
			 * 
			 * 
			 */
			ArrayList<Tick> ticks = new ArrayList<Tick>();

			for (Map.Entry<Integer, DataPoint> entry : e
					.getPrice_map_internal().entrySet()) {
				if (entry.getKey() >= first_available_day_id_on_entity_with_value_for_this_indicator) {

					lg_report.report_log_map
							.get(initial_stub_name)
							.get(default_period_for_logs)
							.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
							.add(entry.getKey());

					DateTime dt = entry.getValue().dateString_to_dateTime();
					// if there is a value on this day for the initial stub.
					BigDecimal existing_value = entry.getValue()
							.getIndicator_values_on_datapoint()
							.get(initial_stub_name)
							.getIndicator_big_decimal_value();

					ticks.add(new Tick(dt, Decimal.ZERO, Decimal.ZERO,
							Decimal.ZERO, Decimal.valueOf(existing_value
									.toString()), Decimal.ZERO));
				} else {

					lg_report.report_log_map
							.get(initial_stub_name)
							.get(default_period_for_logs)
							.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
							.add(entry.getKey());

				}

			}

			/***
			 * 
			 * BUILD A TIMESERIES, OUT OF THE TICKS AND MAKE A HASHMAP TO HOLD
			 * ONE SMAINDICATOR FOR EACH PERIOD.
			 * 
			 */
			TimeSeries ts = new TimeSeries(ticks);
			ClosePriceIndicator clp = new ClosePriceIndicator(ts);
			HashMap<Integer, SMAIndicator> smas_map = new LinkedHashMap<Integer, SMAIndicator>();
			for (Integer period : IdAndShardResolver.periods_map.keySet()) {
				smas_map.put(period, new SMAIndicator(clp, period));
			}

			/*****
			 * 
			 * end
			 * 
			 * 
			 */

			/****
			 * ITERATE THE TIME SERIES.
			 * 
			 * IF THE DAY_ID IS GREATER THAN OR EQUAL TO THE CALCULATE COMPLEXES
			 * FROM THEN WE PROCEED
			 * 
			 * 
			 * 
			 */

			HashMap<Integer, Decimal> prev_period_to_sma_hashmap = new LinkedHashMap<Integer, Decimal>();

			for (int i = ts.getBegin(); i < ts.getEnd(); i++) {

				Integer end_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id
						.get(Complex.get_date(ts.getTick(i).getEndTime()));

				// suppose you want to calculate complexes from day id 10.
				// then you need to have one previous day id, to generate the
				// previous day map.
				// so we use the modified generate calculate complexes from
				// method.
				if (end_day_id >= calculate_complexes_from) {
					lg_report.report_log_map
							.get(initial_stub_name)
							.get(default_period_for_logs)
							.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
							.add(end_day_id);
					/*****
					 * 
					 * CHECK THAT THERE HAVE BEEN ENOUGH DAYS IN THE TIME SERIES
					 * TO CALCULATE THE SMA IF YES, THEN ADD THE SMA AT THIS DAY
					 * ID TO THE curr_period_to_sma_map
					 * 
					 * 
					 */
					//System.out
					//		.println("starting new end day id:------------------------------------------------"
					//				+ end_day_id);
					HashMap<Integer, Decimal> curr_period_to_sma_hashmap = new LinkedHashMap<Integer, Decimal>();
					for (Integer period : IdAndShardResolver.periods_map
							.keySet()) {
						// now get the value of this particular sma.
						// if there were enough days to calculate this sma or
						// not?
						// if the period is > (i + 1)
						// basically if the i is 10, it means that we have 11
						// points.
						// so provided that the period is either less than or
						// equal to (i + 1), we can do it.

						if (period > (i + 1)) {
							//System.out.println("skipped period:" + period);
							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_where_sma_could_not_be_generated_due_to_less_days)
									.add(end_day_id);
						} else {
							Decimal sma = smas_map.get(period).getValue(i);

							curr_period_to_sma_hashmap.put(period, sma);
						}
					}

					/*****
					 * 
					 * now if you have a previous day_id smas_map, then call the
					 * function {@link #compare_two_periods} to compare each
					 * period
					 * 
					 * 
					 */
					//System.out.println("curr period to sma map is:"
					//		+ curr_period_to_sma_hashmap);
					//System.out.println("prev period to sma map is:"
					//		+ prev_period_to_sma_hashmap);

					for (Integer curr_period : curr_period_to_sma_hashmap
							.keySet()) {
						//System.out.println("now doing curr period:"
						//		+ curr_period);
						for (Integer prev_period : prev_period_to_sma_hashmap
								.keySet()) {
							//System.out.println("with prev period:"
							//		+ prev_period);
							compare_two_periods(curr_period, prev_period,
									prev_period_to_sma_hashmap,
									curr_period_to_sma_hashmap, res, end_day_id);
						}
					}

					prev_period_to_sma_hashmap = curr_period_to_sma_hashmap;
				} else {
					lg_report.report_log_map
							.get(initial_stub_name)
							.get(default_period_for_logs)
							.get(day_ids_rejected_dt_lt_calculate_complexes_from)
							.add(end_day_id);
				}
			}

			res.report_result();
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			stub_counter++;
		}

		lg_report.commit_report_log("completed");
		//System.in.read();
		return res_holder;

	}
}
