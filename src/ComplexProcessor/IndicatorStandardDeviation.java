package ComplexProcessor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.joda.time.DateTime;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.helpers.StandardDeviationIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;

public class IndicatorStandardDeviation {

	private static final String subindicator_name = "standard_deviation_indicator";

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_rejected_dt_lt_300_ticks_for_stddev_calc = "day_ids_rejected_dt_lt_300_ticks_for_stddev_calc";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_300_ticks_for_stddev_calc,
																new ArrayList<Object>());
														put(day_ids_actually_processed,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	/****
	 * expected to put onto the entity , at the datapointIndicator. it should
	 * put the names of the complexes that are calculated.
	 * 
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from(e);
		Integer first_available_day_in_entity = e.getPrice_map_internal()
				.firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name,e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorStandardDeviation.class,
				res_holder);
		
		Integer last_applicable_day_for_complex_calculation = e.getPrice_map_internal().lastKey();
		

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());
		
		
		/////////////////////////////////////////
		
		lg_report = build_report_map(lg_report, new ArrayList<String>(e
				.getEntity_initial_stub_names().get(indicator_name)));
		
		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {
			Result res = new Result(initial_stub_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_available_day_in_entity,res_holder.getIndicator_id(),stub_counter,e.getEntity_es_id(),
					last_applicable_day_for_complex_calculation);

			LogObject lg = new LogObject(IndicatorStandardDeviation.class, res);
			lg.commit_info_log("init:process stub:" + initial_stub_name);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			lg_report.add_to_info_packet("calculate complexes from",
					calculate_complexes_from);

			if (first_available_day_id_on_entity_with_value_for_this_indicator == null) {
				throw new Exception(
						"no day id with required stub name found, prev log will have details");
			}

			/***
			 * instantiate hashmap to hold ticklist for each period in the
			 * idandshardresolver.
			 * 
			 * 
			 * structure of this is as follows:
			 * 
			 * key -> period from idand shard resolver value -> value ->
			 * arraylist<tick>
			 * 
			 * the report log holds also one entry for each period. it contains
			 * a hashmap, as the value. and in the report is written:
			 * 
			 * 
			 * period -> {"day_ids_added_to_tick_array": []} {"day_ids_processed
			 * for standard deviation calculation:[]}
			 * 
			 * 
			 * 
			 */

			HashMap<Integer, ArrayList<Tick>> period_wise_tick_map = new LinkedHashMap<Integer, ArrayList<Tick>>();
			for (Integer period : custom_periods) {
				period_wise_tick_map.put(period, new ArrayList<Tick>());

			}

			/***
			 * @iterate all the days in the map_for_pairs.
			 * @provided that the (present_day_id - period) is greater than the
			 *           first day id that we have in the map for
			 *           pairs.(basically we want to build the difference
			 *           between the current day and (current_day - period) days
			 *           ago so we have to be certain that we have a value for n
			 *           days ago.
			 * 
			 * @now the change in the indicator value is the difference between
			 *      the integer indicator vlaues on the two days.
			 * 
			 * @then a new tick is made and this value is added to it as the
			 *       close price.
			 * 
			 * 
			 * 
			 */
			HashMap<Integer, ArrayList<Integer>> raw_ticks = new LinkedHashMap<Integer, ArrayList<Integer>>();

			for (Integer end_day_id : e.getPrice_map_internal().keySet()) {
				// now for each period from idandshardresolver

				for (Integer period : custom_periods) {

					Integer start_day_id = e
							.generate_day_id_n_days_before_given_day_id(
									end_day_id, period);

					if (start_day_id != null
							&& start_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
								.add(end_day_id);

						// here if the start day id does not exist in the price
						// map internal
						// instead of using day_id - period, use,
						// price_map_internal.lower_key(day_id = period),

						Integer change_in_indicator_value = e
								.getPrice_map_internal().get(end_day_id)
								.getIndicator_values_on_datapoint()
								.get(initial_stub_name)
								.getIndicator_integer_value()
								- e.getPrice_map_internal().get(start_day_id)
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value();

						Tick tick = new Tick(e.getPrice_map_internal()
								.get(end_day_id).dateString_to_dateTime(),
								Decimal.ZERO, Decimal.ZERO, Decimal.ZERO,
								Decimal.valueOf(change_in_indicator_value),
								Decimal.ZERO);
						/**
						 * if (raw_ticks.containsKey(period)) {
						 * 
						 * raw_ticks.get(period).add(
						 * change_in_indicator_value);
						 * 
						 * } else { raw_ticks .put(period, new
						 * ArrayList<Integer>(
						 * Arrays.asList(change_in_indicator_value))); }
						 **/

						period_wise_tick_map.get(period).add(tick);
						// System.in.read();

					} else {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
								.add(end_day_id);

						// System.in.read();
					}
				}
			}

			for (Map.Entry<Integer, ArrayList<Tick>> entry : period_wise_tick_map
					.entrySet()) {
				// now create a standard deviation indicator.
				

				Integer period = entry.getKey();

				TimeSeries ts = new TimeSeries(entry.getValue());

				ClosePriceIndicator clp = new ClosePriceIndicator(ts);

				StandardDeviationIndicator stddev = new StandardDeviationIndicator(
						clp, StockConstants.total_pairs_for_any_given_days);

				SMAIndicator average = new SMAIndicator(clp,
						StockConstants.total_pairs_for_any_given_days);

				for (int i = ts.getBegin(); i <= ts.getEnd(); i++) {

					DateTime d = ts.getTick(i).getEndTime();

					Integer end_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id
							.get(CalculateIndicators.datetime_to_datestring(d));

					if (end_day_id >= calculate_complexes_from
							&& i >= StockConstants.total_pairs_for_any_given_days) {

						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
								.add(end_day_id);

						lg_report.report_log_map.get(initial_stub_name)
								.get(period).get(day_ids_actually_processed)
								.add(end_day_id);

						BigDecimal std_dev = Complex
								.TADecimal_to_BigDecimal(stddev.getValue(i));
						BigDecimal mean = Complex
								.TADecimal_to_BigDecimal(average.getValue(i));
						BigDecimal indicator_value = Complex
								.TADecimal_to_BigDecimal(ts.getTick(i)
										.getClosePrice());

						BigDecimal diff = indicator_value.subtract(mean).abs();

						// now you need the ratio of this to the standard
						// deviation
						// we will get a percentage

						BigDecimal ratio = diff.divide(std_dev,
								StockConstants.DataPoint_bd_scale,
								StockConstants.DataPoint_bd_rounding_mode);

						ratio = ratio
								.multiply(new BigDecimal(
										StockConstants.DataPoint_multiplicand_for_deriving_percentage_correlate));

						ratio = ratio
								.setScale(
										StockConstants.DataPoint_percentage_correlate_scale,
										StockConstants.DataPoint_percentage_correlate_rounding_mode);

						Integer percentage_change = ratio.intValueExact();

						LogObject lgd5 = new LogObject(
								IndicatorStandardDeviation.class, res);
						lgd5.add_to_info_packet("standard devation",
								std_dev.toString());
						lgd5.add_to_info_packet("mean", mean.toString());
						lgd5.add_to_info_packet("indidcator_value",
								indicator_value);

						lgd5.add_to_info_packet("diff", diff);
						lgd5.add_to_info_packet("ratio", ratio.toString());
						lgd5.add_to_info_packet("ratio integer value",
								percentage_change);
						lgd5.commit_debug_log("debug");

						// System.out.println(calculated_values.get(String
						// .valueOf(day_id) + "_" + period));

						// get_val(i, clp, raw_ticks, period, ts,std_dev);

						res.add_results(
								IdAndShardResolver
										.get_standard_deviation_subindicator_names(
												indicator_value
														.compareTo(StockConstants.bd_0) >= 0 ? 0
														: 1, period,
												percentage_change), end_day_id);

					} else {

						if (end_day_id < calculate_complexes_from) {
							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_lt_calculate_complexes_from)
									.add(end_day_id);
						} else {

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
									.add(end_day_id);

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_lt_300_ticks_for_stddev_calc)
									.add(end_day_id);
						}
					}

				}
			}

			res.report_result();
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			// System.in.read();
			stub_counter++;
		}

		lg_report.commit_report_log("complete");
		// System.in.read();
		return res_holder;
	}

	public static void get_val(Integer index, ClosePriceIndicator indicator,
			HashMap<Integer, ArrayList<Integer>> ticks, Integer period,
			TimeSeries ts, BigDecimal standard_deviation_as_calculated)
			throws IOException {

		SMAIndicator sma = new SMAIndicator(indicator,
				StockConstants.Entity_total_pairs_for_a_day);

		final int startIndex = Math.max(0, index
				- StockConstants.Entity_total_pairs_for_a_day + 1);
		final int numberOfObservations = index - startIndex + 1;

		System.out.println("start index was:" + startIndex);
		System.out.println("total obs:" + numberOfObservations);

		System.out.println("ta average is:" + sma.getValue(index));

		ArrayList<Double> test_list = new ArrayList<Double>();

		for (int i = Math.max(0, index
				- StockConstants.Entity_total_pairs_for_a_day + 1); i <= index; i++) {
			test_list.add(ticks.get(period).get(i).doubleValue());
		}

		double[] d = new double[test_list.size()];
		for (int i = 0; i < test_list.size(); i++) {
			d[i] = test_list.get(i);
		}

		double stddev_s = new StandardDeviation().evaluate(d);
		double m_s = new Mean().evaluate(d);

		Decimal standardDeviation = Decimal.ZERO;
		Decimal average = sma.getValue(index);

		Double simple_sum = 0.0;
		Decimal ta_sum = Decimal.ZERO;

		for (int i = 0; i < test_list.size(); i++) {
			System.out.println("simple value is:" + d[i]);
			System.out.println("ta value is:"
					+ indicator.getValue(i + startIndex));
			System.out.println("simple - average:" + (m_s - d[i]));
			System.out.println("ta - avg:"
					+ indicator.getValue(i + startIndex).minus(average));
			System.out.println("simple square:" + (m_s - d[i]) * (m_s - d[i]));
			System.out.println("ta square:"
					+ indicator.getValue(i + startIndex).minus(average).pow(2));
			simple_sum = simple_sum + (m_s - d[i]) * (m_s - d[i]);
			System.out.println("new sum simple:" + simple_sum);
			ta_sum = ta_sum.plus(indicator.getValue(i + startIndex)
					.minus(average).pow(2));
			System.out.println("new sum ta:" + ta_sum);
			System.out.println("difference in sums:"
					+ ((ta_sum).toDouble() - simple_sum));

		}

		System.out.println("final sum ta:" + ta_sum);
		System.out.println("final sum simple:" + simple_sum);
		ta_sum = ta_sum.dividedBy(Decimal.valueOf(numberOfObservations));
		simple_sum = simple_sum / (d.length);
		System.out.println("after dividing ta by 300 ta:" + ta_sum);
		System.out.println("after dividing by 300 simple:" + simple_sum);
		ta_sum = ta_sum.sqrt();
		simple_sum = Math.sqrt(simple_sum);
		System.out.println("square root of ta:" + ta_sum);
		System.out.println("simple sum square root:" + simple_sum);

		System.out.println("standard devaition calculated by simple:"
				+ stddev_s);
		System.out.println("standard deviation calculated by ta main:"
				+ standard_deviation_as_calculated);

		System.in.read();

		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
