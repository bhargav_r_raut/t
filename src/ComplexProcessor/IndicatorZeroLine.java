package ComplexProcessor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;

public class IndicatorZeroLine {

	public static final String subindicator_name = "Indicator Zero Line";

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.headMap(2).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {
		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());

														put(day_ids_actually_processed,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	public static ResultHolder calculate(Entity e, String indicator_name)
			throws Exception {

		// System.in.read();

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from(e);
		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name, e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);

		Integer last_applicable_day_for_complex_calculation = e
				.getPrice_map_internal().lastKey();

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());

		// //////////////////////////////////////////////////////////////
		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));

		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(indicator_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity, res_holder.getIndicator_id(),
					stub_counter, e.getEntity_es_id(),
					last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			// System.out
			// .println("first available day id on entity with value for stub is:"
			// +
			// first_available_day_id_on_entity_with_value_for_this_indicator);

			// System.out.println("the stub name is: " + initial_stub_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			// System.out.println("now doing for period: " + period);
			for (Integer end_day_id : e.getPrice_map_internal().keySet()) {
				// System.out.println("now doing for day id:" + end_day_id);
				// if the day id is greater than the
				// LogObject lgd = new
				// LogObject(IndicatorRiseFallAmount.class,
				// e);
				// lgd.commit_debug_log("doing end day id:" + end_day_id);
				// System.out.println("doing end day id:" + end_day_id);
				if (end_day_id >= calculate_complexes_from) {
					// System.out.println("this day id is gt than calc from");
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
								.add(end_day_id);
						// now for each period , we need to get the shit.

						// System.out.println("end day id is greater than calc complexes from:"
						// + end_day_id);

						// lgd1.add_to_info_packet("period", period);
						// System.out.println("period is:" + period);
						Integer start_day_id = e
								.generate_day_id_n_days_before_given_day_id(
										end_day_id, period);

						// System.out.println("start day id becomes:"
						// + start_day_id);

						// lgd1.add_to_info_packet("start day id becomes",
						// start_day_id);
						// System.out.println("start day id becomes:" +
						// start_day_id);
						if (start_day_id != null
								&& start_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

							// System.out
							// .println("this day id is greater than first day in entity");

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
									.add(end_day_id);

							lg_report.report_log_map.get(initial_stub_name)
									.get(period)
									.get(day_ids_actually_processed)
									.add(end_day_id);

							Integer indicator_on_start_day_id = e
									.getPrice_map_internal().get(start_day_id)
									.getIndicator_values_on_datapoint()
									.get(initial_stub_name)
									.getIndicator_integer_value();

							Integer indicator_on_end_day_id = e
									.getPrice_map_internal().get(end_day_id)
									.getIndicator_values_on_datapoint()
									.get(initial_stub_name)
									.getIndicator_integer_value();

							LogObject lgd = new LogObject(
									IndicatorZeroLine.class, res);
							lgd.add_to_info_packet("status",
									"indicator on end day id:"
											+ indicator_on_end_day_id
											+ " and on start day id:"
											+ indicator_on_start_day_id);

							if (indicator_on_end_day_id.intValue() > 0
									&& indicator_on_start_day_id.intValue() <= 0) {
								// crosses above zero line.

								res.add_results(
										IdAndShardResolver
												.get_subindicator_names_for_zero_line(0),
										end_day_id);

								lgd.commit_debug_log("indicator crosses above");

							} else if (indicator_on_end_day_id.intValue() < 0
									&& indicator_on_start_day_id.intValue() >= 0) {

								res.add_results(
										IdAndShardResolver
												.get_subindicator_names_for_zero_line(1),
										end_day_id);
								lgd.commit_debug_log("indicator crosses below");

							} else {
								lgd.commit_debug_log("did not do any cross");
							}

							// System.in.read();

						} else {
							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
									.add(end_day_id);
						}
					}
				} else {
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_rejected_dt_lt_calculate_complexes_from)
								.add(end_day_id);
					}

				}
			}

			res.report_result();
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			// System.in.read();
			stub_counter++;
		}

		lg_report.commit_report_log("completed");
		System.in.read();
		return res_holder;
	}

}
