package ComplexProcessor;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Question {
	private String question;
	private HashMap<String,Object> suggest;
	
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public HashMap<String, Object> getSuggest() {
		return suggest;
	}

	public void setSuggest(HashMap<String, Object> suggest) {
		this.suggest = suggest;
	}

	public Question(String question, HashMap<String,Object> suggest){
		this.question = question;
		this.suggest = suggest;
	}
	
	//for jackson
	public Question(){
		
	}
	
	public Question(String question){
		this.question = question;
		this.suggest = new LinkedHashMap<String, Object>();
	}
	
	
}
