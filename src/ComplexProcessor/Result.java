package ComplexProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

import org.javatuples.Pair;

import yahoo_finance_historical_scraper.StockConstants;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.PairArray;
import IdAndShardResolver.IdAndShardResolver;

public class Result implements IResult {
	
	
	
	
	// i need later on to be able to get all the day ids on which a complex was
	// detected.
	// so the result object has a simple hashmap.
	// name of complex -> {int[]days_detected,string[]aliases}

	// you can put a result into the result object
	
	/***
	 * the last day id in the entity, this is used to ascertain whether the result 
	 * contains a complex on the last day or not.
	 * 
	 */
	private Integer last_day_id;
	
	public Integer getLast_day_id() {
		return last_day_id;
	}

	public void setLast_day_id(Integer last_day_id) {
		this.last_day_id = last_day_id;
	}

	private String entity_es_id;

	public String getEntity_es_id() {
		return entity_es_id;
	}

	public void setEntity_es_id(String entity_es_id) {
		this.entity_es_id = entity_es_id;
	}

	private String indicator_stub_name;

	public String getIndicator_stub_name() {
		return indicator_stub_name;
	}

	public void setIndicator_stub_name(String indicator_stub_name) {
		this.indicator_stub_name = indicator_stub_name;
	}

	private String complex_id_suffix;

	public String getComplex_id_suffix() {
		return complex_id_suffix;
	}

	public void setComplex_id_suffix(String complex_id_suffix) {
		this.complex_id_suffix = complex_id_suffix;
	}

	private String subindicator_name;

	public String getSubindicator_name() {
		return subindicator_name;
	}

	public void setSubindicator_name(String subindicator_name) {
		this.subindicator_name = subindicator_name;
	}

	private String entity_unique_name;

	public String getEntity_unique_name() {
		return entity_unique_name;
	}

	public void setEntity_unique_name(String entity_unique_name) {
		this.entity_unique_name = entity_unique_name;
	}

	public HashMap<String, ArrayList<Integer>> getResults() {
		return results;
	}

	public void setResults(HashMap<String, ArrayList<Integer>> results) {
		this.results = results;
	}

	

	private String indicator_id;

	public String getIndicator_id() {
		return indicator_id;
	}

	public void setIndicator_id(String indicator_id) {
		this.indicator_id = indicator_id;
	}

	private String initial_stub_id;

	public void setInitial_stub_id(String initial_stub_id) {
		this.initial_stub_id = initial_stub_id;
	}

	public String getInitial_stub_id() {
		return initial_stub_id;
	}

	private Integer stub_index_in_indicator_stubs;

	public Integer getStub_index_in_indicator_stubs() {
		return stub_index_in_indicator_stubs;
	}

	public void setStub_index_in_indicator_stubs(
			Integer stub_index_in_indicator_stubs) {
		this.stub_index_in_indicator_stubs = stub_index_in_indicator_stubs;
	}

	/***
	 * initially this contains the subindicator id and the unsorted, possibly
	 * duplicated day ids on which they occcrued.
	 * 
	 * modified in {@link #before_commit()} after the function
	 * {@link #uniq_and_sort_day_ids()} , this contains the sorted, and uniqued
	 * day ids.
	 * 
	 * 
	 */
	private HashMap<String, ArrayList<Integer>> results = new LinkedHashMap<String, ArrayList<Integer>>();

	/***
	 * contains the subindicator id as the key and the names and aliases as the
	 * string[] value. the name is always the first element in the string[]
	 * value.
	 * 
	 * 
	 */
	private HashMap<String, String[]> names_and_aliases = new LinkedHashMap<String, String[]>();

	public HashMap<String, String[]> getNames_and_aliases() {
		return names_and_aliases;
	}

	public void setNames_and_aliases(HashMap<String, String[]> names_and_aliases) {
		this.names_and_aliases = names_and_aliases;
	}

	/**
	 * private ArrayList<String[]> names_and_aliases = new
	 * ArrayList<String[]>();
	 * 
	 * public ArrayList<String[]> getNames_and_aliases() { return
	 * names_and_aliases; }
	 * 
	 * public void setNames_and_aliases(ArrayList<String[]> names_and_aliases) {
	 * this.names_and_aliases = names_and_aliases; }
	 **/

	public Result(String indicator_stub_name, String entity_unique_name,
			String subindicator_name, Integer calculate_complexes_from,
			Integer first_available_day_in_entity, String indicator_id,
			Integer stub_index_in_indicator_stubs, String entity_id,Integer last_day_id) {

		this.entity_unique_name = entity_unique_name;
		this.indicator_stub_name = indicator_stub_name;
		this.names_and_aliases = new LinkedHashMap<String, String[]>();
		this.results = new LinkedHashMap<String, ArrayList<Integer>>();
		this.subindicator_name = subindicator_name;
		this.entity_es_id = entity_id;
		this.indicator_id = indicator_id;
		this.stub_index_in_indicator_stubs = stub_index_in_indicator_stubs;
		this.initial_stub_id = this.indicator_id
				+ this.stub_index_in_indicator_stubs;
		this.last_day_id = last_day_id;
		// /this is the entity_es_id + - + indicator_id +
		// initial_stub_numeric_id
		// /for eg: => e12-ia5
		this.complex_id_suffix = this.entity_es_id
				+ StockConstants.Result_Hyphen + this.initial_stub_id;
		LogObject lg = new LogObject(getClass(), this);
		lg.add_to_info_packet("calculate from", calculate_complexes_from);
		lg.add_to_info_packet("first day in entity",
				first_available_day_in_entity);
		lg.add_to_info_packet("stub index in indicator stubs AKA stub_counter",
				stub_index_in_indicator_stubs);
		lg.add_to_info_packet("complex_id_suffix", complex_id_suffix);
		lg.add_to_info_packet("entity es id", entity_es_id);
		lg.add_to_info_packet("indicator id", indicator_id);
		lg.add_to_info_packet("initial stub id", initial_stub_id);
		lg.commit_info_log("instantiated result object");
		
	}

	public void add_results(HashMap<String, String[]> hm, Integer day_id) {
		for (Map.Entry<String, String[]> entry : hm.entrySet()) {
			add_result(day_id, entry.getKey(), entry.getValue());
		}
	}

	public void add_result(Integer day_id, String result_id,
			String[] names_and_aliases) {

		if (getResults().containsKey(result_id)) {
			LogObject lgd = new LogObject(getClass(), this);
			lgd.add_to_info_packet("names and aliases array length before:",
					getNames_and_aliases().size());
			getResults().get(result_id).add(day_id);

			lgd.add_to_info_packet("added day id :" + day_id + " to ",
					result_id);
			lgd.add_to_info_packet("entry in map", getResults().get(result_id));
			lgd.add_to_info_packet("names and aliases array length after:",
					getNames_and_aliases().size());
			lgd.commit_debug_log("result");

		} else {
			getResults().put(result_id,
					new ArrayList<Integer>(Arrays.asList(day_id)));
			getNames_and_aliases().put(result_id, names_and_aliases);
			LogObject lgd = new LogObject(getClass(), this);

			lgd.add_to_info_packet("added new result id:", result_id);
			lgd.add_to_info_packet("day id at result id",
					getResults().get(result_id));
			String added_names_and_aliases = "";
			for (int i = 0; i < getNames_and_aliases().get(result_id).length; i++) {

				added_names_and_aliases += " "
						+ getNames_and_aliases().get(result_id)[i];

			}

			lgd.add_to_info_packet("added names and aliases",
					added_names_and_aliases);

			lgd.commit_debug_log("result");
		}

	}

	public void report_result() {
		LogObject lgdi = new LogObject(getClass(), this);
		lgdi.commit_info_log("total result ids:" + getResults().size());
		LogObject lgd = new LogObject(getClass(), this);
		for (Map.Entry<String, ArrayList<Integer>> entry : getResults()
				.entrySet()) {
			lgd.add_to_info_packet(entry.getKey(), entry.getValue().get(0)
					+ "..." + entry.getValue().get(entry.getValue().size() - 1)
					+ "(total :" + entry.getValue().size() + ")");
			if (!getNames_and_aliases().containsKey(entry.getKey())) {
				lgd.add_to_info_packet("failed to find name", entry.getKey());
				lgd.commit_error_log("no name for result", new Throwable(""));
			}
		}

		lgd.commit_info_log("completed");
	}

	

	/****
	 * 
	 * 
	 * @param sparse_list
	 *            -> the sorted arraylist of day ids on which the subindicator
	 *            occurred.
	 * @param buy_or_sell
	 *            -> 0 -> if you want to generate a buy list, 1 -> if you want
	 *            to generate a sell list.
	 * 
	 */
	public int[] build_array_for_compression(ArrayList<Integer> sparse_list,
			Integer buy_or_sell) {
		int[] sorted_compressible_arr = new int[CentralSingleton.getInstance().gmt_day_id_to_dataPoint
				.size()];
		Integer index = 0;
		for (Integer day_id : sparse_list) {
			// get the previous day id.
			Integer prev_day_id = index - 1 < 0 ? 0 : sparse_list
					.get(index - 1);
			Integer fill_with = buy_or_sell.intValue() == 0 ? prev_day_id
					: day_id;

			if (prev_day_id != null) {
				for (int i = prev_day_id + 1; i < day_id; i++) {
					sorted_compressible_arr[i] = fill_with;
				}
			}

			index++;
		}

		return sorted_compressible_arr;

	}

	/******
	 * 
	 * END BEFORE COMMIT METHODS. ----------------------------------
	 * 
	 * 
	 */

		// //////////////////////TEST METHOD TO GET NUMBER OF COMPLEXES DETECTED ON
	// THE LAST DAY IN THE PRICE MPA.

	@Override
	public Integer test_get_total_results_on_last_day_id(
			Integer entity_latest_day_id) {
		// TODO Auto-generated method stub

		Integer total_on_last_day_id = 0;
		for (Map.Entry<String, ArrayList<Integer>> entry : getResults()
				.entrySet()) {
			// System.out.println("the complex id detected was:" +
			// entry.getKey());
			// System.out.println("days on which it occurred was:" +
			// entry.getValue().toString());
			// System.out.println("before starting the total is:" +
			// total_on_last_day_id);
			if (entry.getValue().get(entry.getValue().size() - 1).intValue() == entity_latest_day_id
					.intValue()) {
				// System.out.println("detected true on last day id");
				total_on_last_day_id++;
				// System.out.println("after incrmenting total is:" +
				// total_on_last_day_id);
				// try {
				// System.in.read();
				// } catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// }
			}
		}

		return total_on_last_day_id;
	}

}
