package ComplexProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.javatuples.Pair;
import org.json.JSONException;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.PairArray;

public class ResultHolder implements IResultHolder {

	private HashMap<String, Result> hm;
	private String entity_unique_name;
	private String entity_es_id;
	private String indicator_name;
	private String subindicator_name;
	private String indicator_id;

	public String getIndicator_id() {
		return indicator_id;
	}

	public void setIndicator_id(String indicator_id) {
		this.indicator_id = indicator_id;
	}

	public String getEntity_es_id() {
		return entity_es_id;
	}

	public void setEntity_es_id(String entity_es_id) {
		this.entity_es_id = entity_es_id;
	}

	public String getSubindicator_name() {
		return subindicator_name;
	}

	public void setSubindicator_name(String subindicator_name) {
		this.subindicator_name = subindicator_name;
	}

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	public HashMap<String, Result> getHm() {
		return hm;
	}

	public void setHm(HashMap<String, Result> hm) {
		this.hm = hm;
	}

	public String getEntity_unique_name() {
		return entity_unique_name;
	}

	public void setEntity_unique_name(String entity_unique_name) {
		this.entity_unique_name = entity_unique_name;
	}

	/****
	 * when result holder is instantiated, it is done so from one of the various
	 * indicator_calculation_classes. the entity es id, entity unique name,
	 * indicator name and subindicator name are all passed in, the indicator id,
	 * is got within the constructor from the centralsingleton.
	 * 
	 * Bascially the complex_calculate class, is passed in an for the
	 * {@link #commit(HashMap)} function,
	 * 
	 * 
	 * @param entity_unique_name
	 * @param indicator_name
	 * @param subindicator_name
	 * @param entity_es_id
	 * @throws JSONException
	 */
	public ResultHolder(String entity_unique_name, String indicator_name,
			String subindicator_name, String entity_es_id) throws JSONException {
		this.entity_unique_name = entity_unique_name;
		this.indicator_name = indicator_name;
		this.indicator_id = CentralSingleton.getInstance().indicators_jsonfile
				.getJSONObject(indicator_name).getString("id");
		this.hm = new LinkedHashMap<String, Result>();
		this.subindicator_name = subindicator_name;
		this.entity_es_id = entity_es_id;
		LogObject lgd = new LogObject(getClass());
		lgd.add_to_info_packet("entity unique name", entity_unique_name);
		lgd.add_to_info_packet("indicator name", indicator_name);
		lgd.add_to_info_packet("indicator id", indicator_id);
		lgd.add_to_info_packet("subindicator name", subindicator_name);
		lgd.add_to_info_packet("entity es id", entity_es_id);
		lgd.commit_info_log("instantiated result holder");

	}

	/****
	 * 
	 * 
	 * @param all_entity_unique_names
	 * 
	 *            public void process_results(ArrayList<String>
	 *            all_entity_unique_names) {
	 * 
	 *            // for the particular subindicator -> which stock is most
	 *            profitable. // how many do we have already for this today. //
	 *            if less, then inlude otherwise leave.
	 * 
	 *            for (String entity_unique_name : all_entity_unique_names) {
	 * 
	 *            int[] entity_close_price_changes =
	 *            PairArray.get_decompressed_arr( entity_unique_name + "_" +
	 *            StockConstants.Entity_close_close_map_suffix, 0,
	 *            CentralSingleton.getInstance().gmt_day_id_to_dataPoint .size()
	 *            StockConstants.Entity_total_pairs_for_a_day); HashMap<String,
	 *            double[]> t_test_maps = new LinkedHashMap<String, double[]>();
	 *            for (Integer period :
	 *            IdAndShardResolver.periods_map_for_t_tests .keySet()) {
	 *            t_test_maps.put(entity_unique_name + "_" + period,
	 *            int_arr_to_double_arr(PairArray.get_decompressed_arr(
	 *            entity_unique_name + "_" + period, 0,
	 *            getEntity_price_map_internal_size() - period))); }
	 * 
	 *            for (Map.Entry<String, Result> result_entry :
	 *            getHm().entrySet()) {
	 *            result_entry.getValue().do_t_test(entity_close_price_changes,
	 *            t_test_maps, getEntity_last_valid_pair()); }
	 * 
	 *            }
	 * 
	 *            }
	 **/

	public double[] int_arr_to_double_arr(int[] arr) {
		double[] d = new double[arr.length];
		for (int i = 0; i < arr.length; i++) {
			d[i] = new Double(arr[i]).doubleValue();
		}
		return d;
	}

	@Override
	public Integer latest_day_id_has_n_results(
			HashMap<String, Pair<Integer, Integer>> entities_unique_names_and_last_day_ids_and_last_valid_indexes) {
		Integer total_across_all_results = 0;
		for (Map.Entry<String, Result> result : getHm().entrySet()) {
			// /result.getValue().before_commit();
			total_across_all_results += result.getValue()
					.test_get_total_results_on_last_day_id(
							entities_unique_names_and_last_day_ids_and_last_valid_indexes
									.get(getEntity_unique_name()).getValue0());
		}
		setHm(null);

		return total_across_all_results;

	}

	/*****
	 * just does uniq and sorts the day ids in each of the results.
	 * 
	 * 
	 */
	@Override
	public void before_commit() throws Exception {
		// TODO Auto-generated method stub
		for (Map.Entry<String, Result> entry : getHm().entrySet()) {
			// entry.getValue().before_commit();
		}

	}

	/***
	 * commits each of the subindicators to the local db and collects the
	 * responses. then updates the day ids of all the subindicators from the
	 * returned responses
	 * 
	 */
	@Override
	public HashMap<String, Pair<ArrayList<Integer>, Boolean>> build_complex_came_on_which_day_ids_hashmap()
			throws Exception {
		HashMap<String, Pair<ArrayList<Integer>, Boolean>> ret_hm = new LinkedHashMap<String, Pair<ArrayList<Integer>, Boolean>>();
		// should return a hashmap of id->arraylist.
		// if the arraylist contains the last day id, then it should be marked
		// as such.

		for (Result r : getHm().values()) {
			// take each result.
			for (String subindicator_id : r.getResults().keySet()) {
				Boolean occurred_on_last_day_id = false;
				if (r.getResults().get(subindicator_id)
						.contains(r.getLast_day_id())) {
					occurred_on_last_day_id = true;
				}
				ret_hm.put(r.getComplex_id_suffix()
						+ StockConstants.Result_Hyphen + subindicator_id,
						new Pair<ArrayList<Integer>, Boolean>(r.getResults()
								.get(subindicator_id), occurred_on_last_day_id));
			}
		}

		return ret_hm;
	}

	@Override
	public void build_fst(
			HashMap<String, Pair<Integer, Integer>> entities_unique_names_and_last_day_ids_and_last_valid_indexes)
			throws Exception {
		// TODO Auto-generated method stub
		HashMap<String, Pair<int[], Integer>> all_decompressed_close_close_price_arrs = new HashMap<String, Pair<int[], Integer>>();

		for (String entity_unique_name : entities_unique_names_and_last_day_ids_and_last_valid_indexes
				.keySet()) {

			int[] entity_close_price_changes = PairArray.get_decompressed_arr(
					entity_unique_name + "_"
							+ StockConstants.Entity_close_close_map_suffix,
					0,
					CentralSingleton.getInstance().gmt_day_id_to_dataPoint
							.size()
							* StockConstants.Entity_total_pairs_for_a_day);

		}

	}

	@Override
	public void after_commit() throws Exception {
		// TODO Auto-generated method stub

	}

}
