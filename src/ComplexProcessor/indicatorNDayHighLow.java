package ComplexProcessor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import IdAndShardResolver.IdAndShardResolver;

public class indicatorNDayHighLow {

	public static final String subindicator_name = "indicator_n_day_high_low";

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.tailMap(1, false).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	private static final String day_ids_rejected_dt_before_first_day_with_indicator_stub_value = "day_ids_rejected_dt_before_first_day_with_indicator_stub_value";

	private static final String day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value = "day_ids_acepted_dt_after_first_day_with_indicator_stub_value";

	private static final String day_ids_rejected_dt_lt_calculate_complexes_from = "day_ids_rejected_dt_lt_calculated_complexes_from";

	private static final String day_ids_accepted_dt_eq_gt_calculate_complexes_from = "day_ids_accepted_dt_eq_gt_calculate_complexes_from";

	private static final String day_ids_actually_processed = "day_ids_actually_processed";

	private static final String day_ids_neither_high_nor_low = "day_ids_neither_high_now_low";

	private static final String day_ids_high = "day_ids_high";
	private static final String day_ids_low = "day_ids_low";

	public static LogObject build_report_map(LogObject report_log,
			final ArrayList<String> initial_stubs) {

		report_log.report_log_map = new LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>>() {
			{
				for (String initial_stub : initial_stubs) {
					put(initial_stub,
							new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>() {
								{

									for (Integer period : custom_periods) {
										put(period,
												new LinkedHashMap<String, ArrayList<Object>>() {
													{
														put(day_ids_rejected_dt_before_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value,
																new ArrayList<Object>());
														put(day_ids_rejected_dt_lt_calculate_complexes_from,
																new ArrayList<Object>());
														put(day_ids_accepted_dt_eq_gt_calculate_complexes_from,
																new ArrayList<Object>());

														put(day_ids_actually_processed,
																new ArrayList<Object>());

														put(day_ids_neither_high_nor_low,
																new ArrayList<Object>());

														put(day_ids_high,
																new ArrayList<Object>());

														put(day_ids_low,
																new ArrayList<Object>());
													}
												});
									}

								}
							});

				}
			}
		};

		return report_log;
	}

	/****
	 * alternative logic
	 * 
	 * sort teh whole price map internal once.
	 * build another map with the ranks of the keys.
	 * 
	 * then take each end day id,
	 *	-> then take each period
	 *		-> see whether all of the day ids in that period window are less than it.
	 *		-> then it is the highest for that period
	 * 		-> if opposite is true, then it is the highest for that period.
	 * 
	 * 
	 * logic:
	 * 
	 * for each end_day_id:
	 * 
	 * build a treemap.
	 * 
	 * for each period(starting from the smallest period), get the relevant day
	 * id n days ago.
	 * 
	 * add it to the treemap, if the value already exists, dont add it. if the
	 * value of the end_Day_id is the hightest, then that is the n_day_high.
	 * suppose that you dont get enough values upto that day, i.e the returned
	 * value is null, then cannot process.
	 * 
	 * 
	 * 
	 * @param e
	 * @param indicator_name
	 * @return
	 * @throws Exception
	 */
	public static ResultHolder calculate_complex(Entity e, String indicator_name)
			throws Exception {

		// ObjectMapper mapper = new ObjectMapper();

		Integer calculate_complexes_from = Complex
				.get_calculate_complex_from(e);

		Integer first_day_in_entity = e.getPrice_map_internal().firstKey();

		ResultHolder res_holder = new ResultHolder(e.getUnique_name(),
				indicator_name, subindicator_name,e.getEntity_es_id());

		LogObject lg_report = new LogObject(IndicatorRiseFallAmount.class,
				res_holder);
		lg_report.add_to_info_packet("first day id in entity",
				first_day_in_entity);

		lg_report.add_to_info_packet("calculate complexes from",
				calculate_complexes_from);
		
		Integer last_applicable_day_for_complex_calculation = e.getPrice_map_internal().lastKey();
	

		lg_report.add_to_info_packet(
				"actual last day in the price map internal", e
						.getPrice_map_internal().lastKey());
		
		
		
		
		
		/////////////////////////////////////////////////////////////

		lg_report = build_report_map(lg_report, e
				.getEntity_initial_stub_names().get(indicator_name));
		
		Integer stub_counter = 0;
		for (String initial_stub_name : e.getEntity_initial_stub_names().get(
				indicator_name)) {

			Result res = new Result(indicator_name, e.getUnique_name(),
					subindicator_name, calculate_complexes_from,
					first_day_in_entity,res_holder.getIndicator_id(),stub_counter,e.getEntity_es_id()
					,last_applicable_day_for_complex_calculation);

			Integer first_available_day_id_on_entity_with_value_for_this_indicator = Complex
					.get_first_available_day_id_on_entity_with_valid_value_for_indicator(
							e, initial_stub_name, indicator_name);
			lg_report
					.add_to_info_packet("first day id with stub:"
							+ initial_stub_name + " : ",
							first_available_day_id_on_entity_with_value_for_this_indicator);

			for (Integer end_day_id : e.getPrice_map_internal().keySet()) {

				if (end_day_id >= calculate_complexes_from
						&& end_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

					/***
					 * make the treemap and add the value at the end day id as
					 * the key and the end day id as the value
					 * 
					 */
					TreeMap<Integer, Integer> high_low_treemap = new TreeMap<Integer, Integer>();
					high_low_treemap.put(
							e.getPrice_map_internal().get(end_day_id)
									.getIndicator_values_on_datapoint()
									.get(initial_stub_name)
									.getIndicator_integer_value(), end_day_id);
					
					
					// System.out
					// .println("doing end day id:"
					// + end_day_id
					// + " and the treemap looks like before doing any periods:"
					// + high_low_treemap.toString());
					// System.in.read();

					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_accepted_dt_eq_gt_calculate_complexes_from)
								.add(end_day_id);

						Integer start_day_id = e
								.generate_day_id_n_days_before_given_day_id(
										end_day_id, period);

						if (start_day_id != null
								&& start_day_id >= first_available_day_id_on_entity_with_value_for_this_indicator) {

							// System.out.println("period:" + period
							// + " and start day id  qualifies:"
							// + start_day_id);

							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_accepted_dt_eq_or_after_first_day_with_indicator_stub_value)
									.add(end_day_id);

							lg_report.report_log_map.get(initial_stub_name)
									.get(period)
									.get(day_ids_actually_processed)
									.add(end_day_id);

							// get all the values starting from the start day
							// id, upto the end day id, but not including it.
							// then add them to the treemap.

							NavigableMap<Integer, DataPoint> nm = e
									.getPrice_map_internal().subMap(
											start_day_id, true, end_day_id,
											false);

							// System.out
							// .println("all day ids with their values in between these two");
							// mapper.defaultPrettyPrintingWriter()
							// .writeValueAsString(nm);

							for (Map.Entry<Integer, DataPoint> entry : nm
									.entrySet()) {
								if (!high_low_treemap.containsKey(entry
										.getValue()
										.getIndicator_values_on_datapoint()
										.get(initial_stub_name)
										.getIndicator_integer_value())) {

									high_low_treemap.put(entry.getValue()
											.getIndicator_values_on_datapoint()
											.get(initial_stub_name)
											.getIndicator_integer_value(),
											entry.getKey());
								}
							}

							// System.out
							// .println("the high low treemap after adding all this into it:"
							// + mapper.defaultPrettyPrintingWriter()
							// .writeValueAsString(
							// high_low_treemap));

							// now check if the indicator value on the end day
							// id is the highest value or the lowest value.

							// if yes then send it for getting the name and id.

							if (high_low_treemap.higherKey(e
									.getPrice_map_internal().get(end_day_id)
									.getIndicator_values_on_datapoint()
									.get(initial_stub_name)
									.getIndicator_integer_value()) == null) {

								// System.out.println("the end day id:"
								// + end_day_id
								// + " valu is the highest key:");
								res.add_results(IdAndShardResolver
										.get_n_day_high_low_subindicator_names(
												period, 0), end_day_id);
								lg_report.report_log_map.get(initial_stub_name)
										.get(period).get(day_ids_high)
										.add(end_day_id);
								// this is the highest value.
							} else if (high_low_treemap.lowerKey(e
									.getPrice_map_internal().get(end_day_id)
									.getIndicator_values_on_datapoint()
									.get(initial_stub_name)
									.getIndicator_integer_value()) == null) {
								// this is the lowest value.
								// System.out.println("end day id:" + end_day_id
								// + " is the lowest key");
								res.add_results(IdAndShardResolver
										.get_n_day_high_low_subindicator_names(
												period, 1), end_day_id);
								lg_report.report_log_map.get(initial_stub_name)
										.get(period).get(day_ids_low)
										.add(end_day_id);
							} else {
								// System.out.println("end day id:" + end_day_id
								// + "  is neither");
								// System.out
								// .println("this is the higher entry:"
								// + high_low_treemap
								// .higherEntry(e
								// .getPrice_map_internal()
								// .get(end_day_id)
								// .getIndicator_values_on_datapoint()
								// .get(initial_stub_name)
								// .getIndicator_integer_value()));
								// System.out
								// .println("this is the lower entry:"
								// + high_low_treemap
								// .lowerEntry(e
								// .getPrice_map_internal()
								// .get(end_day_id)
								// .getIndicator_values_on_datapoint()
								// .get(initial_stub_name)
								// .getIndicator_integer_value()));
								lg_report.report_log_map.get(initial_stub_name)
										.get(period)
										.get(day_ids_neither_high_nor_low)
										.add(end_day_id);
							}

						}

						else {
							// System.out
							// .println("could not be processed for period dt before first valid day:"
							// + period);
							lg_report.report_log_map
									.get(initial_stub_name)
									.get(period)
									.get(day_ids_rejected_dt_before_first_day_with_indicator_stub_value)
									.add(end_day_id);
						}

					}

				} else {
					for (Integer period : custom_periods) {
						lg_report.report_log_map
								.get(initial_stub_name)
								.get(period)
								.get(day_ids_rejected_dt_lt_calculate_complexes_from)
								.add(end_day_id);
					}
				}

			}

			res.report_result();
			res_holder.getHm().put(res.getComplex_id_suffix(), res);
			stub_counter++;
		}

		lg_report.commit_report_log("completed");

		return res_holder;
	}
}
