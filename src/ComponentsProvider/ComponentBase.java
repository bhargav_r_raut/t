package ComponentsProvider;

import java.util.HashMap;
import java.util.LinkedHashMap;

import ExchangeBuilder.Entity;

public class ComponentBase {

	private String index;
	private Integer index_id;
	private HashMap<String,Entity> exchange_entity_map;
	
	
	
	public String getIndex() {
		return index;
	}



	public void setIndex(String index) {
		this.index = index;
	}



	public Integer getIndex_id() {
		return index_id;
	}



	public void setIndex_id(Integer index_id) {
		this.index_id = index_id;
	}



	public HashMap<String, Entity> getExchange_entity_map() {
		return exchange_entity_map;
	}



	public void setExchange_entity_map(HashMap<String, Entity> exchange_entity_map) {
		this.exchange_entity_map = exchange_entity_map;
	}



	public ComponentBase(String index, Integer index_id){
		setIndex(index);
		setIndex_id(index_id);
		setExchange_entity_map(new LinkedHashMap<String, Entity>());
	}
	
	public void get_components(){
		
	}

}
