package ComponentsProvider;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequestBuilder;
import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.index.query.QueryBuilders;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.LogObject;
import Titan.ReadWriteTextToFile;

import com.tradegenie.tests.BaseTest;
import com.tradegenie.tests.FlushIndicesAndRedis;
import com.tradegenie.tests.ShScript;
import com.tradegenie.tests.TestSingleton;

import elasticSearchNative.RemoteEs;

public class ComponentsSavedWikiPageBuilder {

	public ComponentsSavedWikiPageBuilder() {

	}

	public static void main(String[] args) throws Exception {

		try {
			StockConstants.force_download_and_store_wikipedia_company_info_pages = true;
			if (ShScript.start_all_servers()) {
				TestSingleton.initialize_test_singleton();
				ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
				FlushIndicesAndRedis flush = new FlushIndicesAndRedis();
				flush.flush();
				
				//run an sh script to clear the component pages.
				
				if (!LogObject.get_errors()) {
					// download_and_write_pages_to_disk(erp);
					// save wikipedia company info pages
					FileUtils.cleanDirectory(new File(StockConstants.path_for_component_lists));
					FileUtils.cleanDirectory(new File(StockConstants.path_for_component_pages));
					download_and_write_component_lists_to_disk(erp);
					save_wikipedia_company_pages(erp);
					
				}
			}
		} finally {
			StockConstants.force_download_and_store_wikipedia_company_info_pages = false;
		}

	}

	public static void save_wikipedia_company_pages(
			ExchangeBuilderPackageCoordinator erp) throws Exception {
		// now actually run the build entity exchange
		
		HashMap<String, Integer> all_indices = BaseTest
				.iterate_and_return_indices_to_test(new String[] {
						StockConstants.name_for_nasdaq,
						StockConstants.name_for_dax,
						StockConstants.name_for_cac,
						StockConstants.name_for_ftse,
						StockConstants.name_for_nifty });
		// that will store the pages.
		for (Map.Entry<String, Integer> entry : all_indices.entrySet()) {
			//delete all the errors. from the error log.
			//THIS WILL IGNORE EXCEPTIONS, BY DELETING ALL THE EXCEPTIONS AFTER EACH ITERATION.
			DeleteByQueryResponse drr = new DeleteByQueryRequestBuilder(
					RemoteEs.getInstance())
					.setQuery(QueryBuilders.matchAllQuery())
					.setIndices("tradegenie_titan").setTypes("error_log")
					.execute().actionGet();
			erp.build_entityExchange(entry.getValue(), entry.getKey());
		}
	}

	public static void download_and_write_component_lists_to_disk(
			ExchangeBuilderPackageCoordinator erp) throws Exception {

		try {
			/***
			 * nasdaq
			 */
			
			String nasdaq_wiki_page = ReadWriteTextToFile
					.getTextFromUrl(StockConstants.wikipedia_nasdaq_components_page)
					.body().toString();

			ReadWriteTextToFile.getInstance().writeFile(
					StockConstants.path_for_component_lists
							+ StockConstants.name_for_nasdaq, nasdaq_wiki_page);

			/***
			 * cac
			 * 
			 */
			String cac_wiki_page = ReadWriteTextToFile
					.getTextFromUrl(StockConstants.wikipedia_cac_components_page)
					.body().toString();
			ReadWriteTextToFile.getInstance().writeFile(
					StockConstants.path_for_component_lists
							+ StockConstants.name_for_cac, cac_wiki_page);

			/***
			 * 
			 * dax
			 * 
			 */
			String dax_wiki_page = ReadWriteTextToFile
					.getTextFromUrl(StockConstants.wikipedia_dax_components_page)
					.body().toString();
			ReadWriteTextToFile.getInstance().writeFile(
					StockConstants.path_for_component_lists
							+ StockConstants.name_for_dax, dax_wiki_page);

			/****
			 * ftse
			 * 
			 * 
			 */
			String ftse_wiki_page = ReadWriteTextToFile
					.getTextFromUrl(StockConstants.wikipedia_ftse_components_page)
					.body().toString();
			ReadWriteTextToFile.getInstance().writeFile(
					StockConstants.path_for_component_lists
							+ StockConstants.name_for_ftse, ftse_wiki_page);

			/***
			 * 
			 * nifty
			 */
			String nifty_wiki_page = ReadWriteTextToFile
					.getTextFromUrl(StockConstants.url_for_nifty_component_csv)
					.body().toString();
			ReadWriteTextToFile.getInstance().writeFile(
					StockConstants.path_for_component_lists
							+ StockConstants.name_for_nifty, nifty_wiki_page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
