package ComponentsProvider;

import java.util.HashMap;
import java.util.LinkedHashMap;

import ExchangeBuilder.Entity;

public class ForexComponents extends ComponentBase {

	public ForexComponents(String index, Integer index_id) {
		super(index, index_id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void get_components() {
		// TODO Auto-generated method stub
		String[] forex_pair_names = { 
				"USD/CAD",
				"AUD/CAD",
				"EUR/CAD",
				"EUR/JPY",
				"GBP/JPY",
				"USD/JPY",
				"CHF/JPY",
				"AUD/JPY",
				"EUR/USD",
				"GBP/USD",
				"NZD/USD",
				"AUD/USD",
				"EUR/CHF",
				"USD/CHF", 
				"GBP/CHF",
				"EUR/GBP",
				"EUR/AUD",
				"AUD/NZD" };

		HashMap<String, String> full_names_of_currencies = new LinkedHashMap<String, String>();
		full_names_of_currencies.put("USD", "US Dollar");
		full_names_of_currencies.put("CAD", "Canadian Dollar");
		full_names_of_currencies.put("AUD", "Australian Dollar");
		full_names_of_currencies.put("NZD", "New Zealand Dollar");
		full_names_of_currencies.put("JPY", "Japanese Yen");
		full_names_of_currencies.put("CHF", "Swiss Franc");
		full_names_of_currencies.put("GBP", "British Pound");
		full_names_of_currencies.put("EUR", "European Union Euro");
		
		for (String forex_pair : forex_pair_names) {
			Entity e = new Entity(getIndex(),
					getIndex_id());
			e.setsymbol(forex_pair);
			String[] pair_components = forex_pair.split("/");
			String fullName = forex_pair;
			
			Integer counter = 0;
			for (String currency : pair_components) {
				fullName = fullName.replace(currency, full_names_of_currencies.get(currency));
			}
			e.setFullName(fullName);
			e.setIndice(getIndex());
			e.setIndice_stockConstants_code(getIndex_id());
			e.setOhlcv_type("c1");
			e.setUnique_name();
			getExchange_entity_map().put(e.getUnique_name(), e);
		}
	}
}
