package ComponentsProvider;

import ExchangeBuilder.Entity;

public class OilComponents extends ComponentBase{
	
	//TO CHANGE COMPONENT, CHANGE HERE, AND ALSO IN ENTITY, ADD THE CODE IN 
	//SET_INDICE_CODE
	
	public OilComponents(String index, Integer index_id) {
		super(index, index_id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void get_components() {
		// TODO Auto-generated method stub
		String[] oil = {"oil"};
		Entity e = new Entity(getIndex(), getIndex_id());
		e.setFullName("OPEC");
		e.setsymbol("oil");
		e.setIndice(getIndex());
		e.setIndice_stockConstants_code(getIndex_id());
		e.setUnique_name();
		e.setOhlcv_type("c1");
		getExchange_entity_map().put(e.getUnique_name(), e);
	}


}
