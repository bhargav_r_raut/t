package ComponentsProvider;

import static jodd.jerry.Jerry.jerry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tradegenie.tests.TestSingleton;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.LogObject;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import au.com.bytecode.opencsv.CSVReader;

/**
 * add the zones mapping in appendcompanyinformation
 * 
 * see whats wrong with array shit
 * 
 * do the thing for all the indices.
 * 
 */

public class WikipediaScraper extends ComponentBase {

	public WikipediaScraper(String index, Integer index_id) {
		super(index, index_id);
		// TODO Auto-generated constructor stub
	}

	public static String prepare_string(String input_string) {

		input_string = input_string.toLowerCase().trim()
				.replaceAll("\\s([Pp][Ll][Cc]|[Gg]roup|[Cc]o|[Ii]nc|[Aa][Gg]|[dD][Ee]|[Ss][eE]|[Pp][Aa])", " ")
				.replaceAll("\\.([Aa][Gg]|[Dd][eE]|[Ss][Ee]|[lL])", "").replaceAll("^\\s|^\\n|^\\t|^\\r", "")
				.replaceAll("\\s$|\\n$|\\t$|\\r$", "").replaceAll("\\.|\\,", "");
		// /System.out.println("this is input string:" + input_string);
		return input_string;
	}

	@SuppressWarnings("static-access")
	protected Entity scrape_wikipedia_page(String url, final Entity wso) {
		String page = null;
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			String file_name = StockConstants.path_for_component_pages
					+ ReadWriteTextToFile.getInstance().get_base64_representation(url);

			if (!StockConstants.force_download_and_store_wikipedia_company_info_pages) {
				// read the file.
				page = ReadWriteTextToFile.getInstance().readFile(file_name, TitanConstants.ENCODING);

			} else {
				// write the file to the location.
				// System.out.println("the file name is:" + file_name);
				page = ReadWriteTextToFile.getInstance().getTextFromUrl(url).body().toString();
				ReadWriteTextToFile.getInstance().writeFile(file_name, page);
				TestSingleton.getTsingle().getTest_expected_values_map().put("url_called", null);
			}

			Jerry doc = jerry(page);

			doc.$("table.infobox.vcard").each(new JerryFunction() {
				@Override
				public boolean onNode(Jerry $this, int index) {
					final ArrayList<Integer> indices = new ArrayList<Integer>();
					final HashMap<Integer, String> values = new HashMap<Integer, String>();
					// TODO Auto-generated method stub
					$this.$("tr").each(new JerryFunction() {
						@Override
						public boolean onNode(Jerry $this, final int row_index) {
							// TODO Auto-generated method stub

							$this.$("th").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $this, int index) {
									if ($this.text().contains("Industry")) {
										indices.add(row_index);
									} else if ($this.text().contains("Products")) {
										indices.add(row_index);
									}
									return true;
								}
							});

							$this.$("td").each(new JerryFunction() {

								@Override
								public boolean onNode(Jerry $this, int index) {
									// TODO Auto-generated method stub
									String stripped_text = $this.text().trim().replaceAll("\\n|\\r|\\t", ",");
									// stripped_text =
									// stripped_text.replaceAll("\\n$|\\r$|\\t$|\\s$",
									// "");
									values.put(row_index, stripped_text);
									return true;
								}
							});

							return true;
						}
					});

					for (int i = 0; i < indices.size(); i++) {
						if (values.get(indices.get(i)) != null) {
							if (i == 0) {
								wso.addIndustry(values.get(indices.get(i)));

							} else {
								wso.addProducts(values.get(indices.get(i)));

							}
						}
					}

					return false;
				}
			});
		} catch (Exception e) {
			LogObject lg = new LogObject(getClass(), wso);
			lg.add_to_info_packet(StockConstants.EntityIndex_name, getIndex());
			lg.commit_error_log("scraping wiki page", e);
		}

		return wso;
	}

	/**
	 * 
	 * scrape nasdaq components.
	 * 
	 */
	protected void scrape_nasdaq_components() throws IOException {

		// String url = StockConstants.wikipedia_nasdaq_components_page;
		// String response =
		// ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		LogObject lgd = new LogObject(WikipediaScraper.class);
		lgd.commit_debug_log("init: scrape nasdaq components");
		String response = ReadWriteTextToFile.getInstance().readFile(
				StockConstants.path_for_component_lists + StockConstants.name_for_nasdaq, TitanConstants.ENCODING);
		Jerry jerry_doc = jerry(response);

		LogObject lgd1 = new LogObject(WikipediaScraper.class);
		lgd1.commit_debug_log(response);
		final Pattern p = Pattern.compile("(.*)\\(([A-Z]+)\\)");

		jerry_doc.$("ol").each(new JerryFunction() {

			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				$this.$("li").each(new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						// TODO Auto-generated method stub
						final Entity wso = new Entity(getIndex(), getIndex_id());
						Matcher m = p.matcher($this.text().trim());
						if (m.find()) {
							wso.setFullName(m.group(1));
							wso.setsymbol(m.group(2));
							wso.setUnique_name();

						}
						wso.setLink_to_wikipedia_page("http://www.wikipedia.com" + $this.$("a").first().attr("href"));
						// String stocksymbol = prepare_string(wso.getsymbol());
						wso.setOhlcv_type("c3");
						getExchange_entity_map().put(wso.getUnique_name(),
								scrape_wikipedia_page(wso.getLink_to_wikipedia_page(), wso));

						return true;
					}
				});
				return false;
			}
		});

	}

	/**
	 * 
	 * scrape ftse components.
	 * 
	 */
	protected void scrape_ftse_components() throws IOException {

		// String url = StockConstants.wikipedia_ftse_components_page;
		// String response =
		// ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		String response = ReadWriteTextToFile.getInstance().readFile(
				StockConstants.path_for_component_lists + StockConstants.name_for_ftse, TitanConstants.ENCODING);
		Jerry jerry_doc = jerry(response);
		jerry_doc.$("table#constituents").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				$this.$("tr").each(new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						// System.out.println("this is the row text:"
						// + $this.text());
						// TODO Auto-generated method stub

						if (index > 0) {
							final Entity wso = new Entity(getIndex(), getIndex_id());
							$this.$("td").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $this, int index) {
									// TODO Auto-generated method stub
									if (index == 0) {
										wso.setFullName($this.text().trim());
										wso.setLink_to_wikipedia_page(
												"http://www.wikipedia.com" + $this.$("a").first().attr("href"));
									} else if (index == 1) {
										wso.setsymbol($this.text().trim());
									}

									return true;
								}
							});
							// String stocksymbol =
							// prepare_string(wso.getsymbol());

							// scrape_wikipedia_page(wso.getLink(), wso);
							wso.setOhlcv_type("c3");
							wso.setUnique_name();
							getExchange_entity_map().put(wso.getUnique_name(),
									scrape_wikipedia_page(wso.getLink_to_wikipedia_page(), wso));

						}
						return true;
					}
				});
				return false;
			}
		});

	}

	protected void scrape_cac_components() throws IOException {
		// String url = StockConstants.wikipedia_components_pages[2];
		// String response =
		// ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		String response = ReadWriteTextToFile.getInstance().readFile(
				StockConstants.path_for_component_lists + StockConstants.name_for_cac, TitanConstants.ENCODING);
		Jerry jerry_doc = jerry(response);

		jerry_doc.$("table.wikitable").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				// index o-> nothing
				// index 1-> fullname
				// index 2-> sector

				// index 3-> symbol

				$this.$("tr").each(new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						if (index > 0) {
							final Entity wso = new Entity(getIndex(), getIndex_id());
							$this.$("td").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $this, int index) {

									// TODO Auto-generated method stub
									if (index == 0) {
										wso.setsymbol($this.text().trim());
									} else if (index == 1) {
										// System.out
										// .println("this is the stockfullname:"
										// + $this.text());
										wso.setFullName($this.text().trim());
										$this.$("a").each(new JerryFunction() {

											@Override
											public boolean onNode(Jerry $this, int index) {
												// TODO Auto-generated method
												// stub
												wso.setLink_to_wikipedia_page(
														"http://www.wikipedia.com" + $this.attr("href"));
												return false;
											}
										});
									} else if (index == 2) {
										wso.addIndustry($this.text().trim());
									} else if (index == 3) {

									} else {

									}

									return true;
								}
							});

							// String stocksymbol =
							// prepare_string(wso.getsymbol());
							wso.setOhlcv_type("c3");
							wso.setUnique_name();
							getExchange_entity_map().put(wso.getUnique_name(),
									scrape_wikipedia_page(wso.getLink_to_wikipedia_page(), wso));

						}
						return true;
					}

				});

				return false;
			}
		});
	}

	/**
	 * scrape dax components.
	 * 
	 * @throws IOException
	 */
	protected void scrape_dax_components() throws IOException {

		// String url = StockConstants.wikipedia_components_pages[1];
		// String response =
		// ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		String response = ReadWriteTextToFile.getInstance().readFile(
				StockConstants.path_for_component_lists + StockConstants.name_for_dax, TitanConstants.ENCODING);
		Jerry jerry_doc = jerry(response);

		jerry_doc.$("table.wikitable").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				// index o-> nothing
				// index 1-> fullname
				// index 2-> sector

				// index 3-> symbol

				$this.$("tr").each(new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						if (index > 0) {
							final Entity wso = new Entity(getIndex(), getIndex_id());
							$this.$("td").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $this, int index) {

									// TODO Auto-generated method stub
									if (index == 0) {

									} else if (index == 1) {
										// System.out
										// .println("this is the stockfullname:"
										// + $this.text());
										wso.setFullName($this.text().trim());
										$this.$("a").each(new JerryFunction() {

											@Override
											public boolean onNode(Jerry $this, int index) {
												// TODO Auto-generated method
												// stub
												wso.setLink_to_wikipedia_page(
														"http://www.wikipedia.com" + $this.attr("href"));
												return false;
											}
										});
									} else if (index == 2) {
										wso.addIndustry($this.text().trim());
									} else if (index == 3) {
										wso.setsymbol($this.text().trim());

									} else {

									}

									return true;
								}
							});

							// String stocksymbol =
							// prepare_string(wso.getsymbol());
							wso.setOhlcv_type("c3");
							wso.setUnique_name();
							getExchange_entity_map().put(wso.getUnique_name(),
									scrape_wikipedia_page(wso.getLink_to_wikipedia_page(), wso));

						}
						return true;
					}

				});

				return false;
			}
		});
	}

	protected void scrape_nifty_components() throws IOException {
		String nifty_csv_string = ReadWriteTextToFile.getInstance().readFile(
				StockConstants.path_for_component_lists + StockConstants.name_for_nifty, TitanConstants.ENCODING);
		StringReader reader = new StringReader(nifty_csv_string);
		BufferedReader br = new BufferedReader(reader);
		CSVReader nifty_components_csv = new CSVReader(br);
		List<String[]> myEntries = nifty_components_csv.readAll();
		Integer counter = 0;
		for (String[] entry : myEntries) {
			if (counter > 0) {
				Entity entity = new Entity(getIndex(), getIndex_id());
				entity.setsymbol(entry[2]);
				entity.setFullName(entry[0]);
				entity.setOhlcv_type("c3");
				entity.setUnique_name();
				entity.addIndustry(entry[1]);
				getExchange_entity_map().put(entity.getUnique_name(), entity);
			}
			counter++;
		}

	}

	@Override
	public void get_components() {
		// TODO Auto-generated method stub
		try {
			// now depeding upon the index, scrape it.
			if (getIndex().equals(StockConstants.name_for_nasdaq)) {
				scrape_nasdaq_components();
			} else if (getIndex().equals(StockConstants.name_for_dax)) {
				scrape_dax_components();
			} else if (getIndex().equals(StockConstants.name_for_cac)) {
				scrape_cac_components();
			} else if (getIndex().equals(StockConstants.name_for_ftse)) {
				scrape_ftse_components();
			} else if (getIndex().equals(StockConstants.name_for_nifty)) {
				scrape_nifty_components();
			} else if (getIndex().equals(StockConstants.name_for_forex)) {

			} else if (getIndex().equals(StockConstants.name_for_metals)) {

			} else if (getIndex().equals(StockConstants.name_for_oil)) {

			}
		} catch (Exception e) {
			LogObject lg = new LogObject(getClass());
			lg.add_to_info_packet(StockConstants.EntityIndex_name, getIndex());
			lg.commit_error_log("get_components", e);
		}

	}

}
