import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;


public class CouchTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Cluster cluster = CouchbaseCluster.create();
		Bucket bucket = cluster.openBucket();
		JsonArray arr = JsonArray.create();
		for(int i=0; i < 1000; i++){
			arr.add(i);
		}
		for(int i=0; i < 1000000; i++){
			JsonObject object = JsonObject.empty()
					.put("test" + String.valueOf(i)	, arr);
			JsonDocument doc = JsonDocument.create("abc" + i, object);
			bucket.upsert(doc);
			System.out.println("doing :" + i);
		}
				
		
	}

}
