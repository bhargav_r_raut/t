package DailyOperationsLogging;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import Titan.GroovyStatements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import elasticSearchNative.LocalEs;

public class BaseError {

	private String stockName;
	private String stockFullName;
	private String indice;
	private String indicator_name;
	private String complex_name;
	private Integer day_id;
	private transient Exception exception;
	private Long complex_vertex_id;
	private String exception_trace;
	private String failed_where;
	private Integer actual_day_id_start;
	private Integer actual_day_id_end;
	private Integer extended_day_id_start;
	private Long error_time;
	private String failed_object;
	private transient Gson gson;

	public Long getError_time() {
		return error_time;
	}

	public void setError_time(Long error_time) {
		this.error_time = error_time;
	}

	public String getFailed_object() {
		return failed_object;
	}

	public void setFailed_object(String failed_object) {
		this.failed_object = failed_object;
	}

	public Integer getDay_id_inclusive() {
		return extended_day_id_start;
	}

	public void setDay_id_inclusive(Integer day_id_inclusive) {
		this.extended_day_id_start = day_id_inclusive;
	}

	public Integer getactual_Day_id_start() {
		return actual_day_id_start;
	}

	public void setactual_Day_id_start(Integer actual_day_id_start) {
		this.actual_day_id_start = actual_day_id_start;
	}

	public Integer getDay_id_end() {
		return actual_day_id_end;
	}

	public void setDay_id_end(Integer day_id_end) {
		this.actual_day_id_end = day_id_end;
	}

	public String getFailed_where() {
		return failed_where;
	}

	public void setFailed_where(String failed_where) {
		this.failed_where = failed_where;
	}

	public Long getComplex_vertex_id() {
		return complex_vertex_id;
	}

	public void setComplex_vertex_id(Long complex_vertex_id) {
		this.complex_vertex_id = complex_vertex_id;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockFullName() {
		return stockFullName;
	}

	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	public String getComplex_name() {
		return complex_name;
	}

	public void setComplex_name(String complex_name) {
		this.complex_name = complex_name;
	}

	public Integer getDay_id() {
		return day_id;
	}

	public void setDay_id(Integer day_id) {
		this.day_id = day_id;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		exception.printStackTrace();
		this.exception = exception;
		StackTraceElement[] traces = exception.getStackTrace();
		LinkedHashMap<String, String> exception_trace = new LinkedHashMap<String, String>();
		exception_trace.put("message", exception.getLocalizedMessage());
		exception_trace.put("localized_message", exception.getMessage());
		for (int i = 0; i < traces.length; i++) {
			exception_trace.put(String.valueOf(i), traces[i].toString());
		}
		try {
			this.exception_trace = GroovyStatements.getInstance().objmapper
					.writeValueAsString(exception_trace);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public BaseError() {

	}

	/*
	 * public BaseError(String stockName, String stockFullName, String indice,
	 * String indicator_name, String complex_name, Exception e, Integer day_id,
	 * Long complex_vertex_id,String failed_where) {
	 * 
	 * setComplex_name(complex_name); setComplex_vertex_id(complex_vertex_id);
	 * setDay_id(day_id); setException(e); setIndicator_name(indicator_name);
	 * setIndice(indice); setStockName(stockName);
	 * setStockFullName(stockFullName); setFailed_where(failed_where);
	 * add_to_es(); }
	 */

	public BaseError(String stockName, String stockFullName, String indice,
			String indicator_name, String complex_name, Exception e,
			Integer day_id_inclusive, Integer actual_day_id_start,
			Integer day_id_end, String failed_where) {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		setComplex_name(complex_name);
		setDay_id_inclusive(day_id_inclusive);
		setactual_Day_id_start(actual_day_id_start);
		setDay_id_end(day_id_end);
		setException(e);
		setIndicator_name(indicator_name);
		setIndice(indice);
		setStockName(stockName);
		setStockFullName(stockFullName);
		setFailed_where(failed_where);
		setError_time(new GregorianCalendar().getTimeInMillis());

		add_to_es();
		// add_to_redis();
	}

	public BaseError(String failed_where, String failed_object,
			String complex_name) {
		setFailed_where(failed_where);
		setFailed_object(failed_object);
		setComplex_name(complex_name);
		setError_time(new GregorianCalendar().getTimeInMillis());
		add_to_es();
	}

	public BaseError(Exception e, String failed_where) {
		Long error_time = new GregorianCalendar().getTimeInMillis();
		setFailed_where(failed_where);
		setError_time(error_time);
		setException(e);
		setError_time(new GregorianCalendar().getTimeInMillis());
		add_to_es();
	}

	public BaseError(Exception exc, EntityIndex ei, Entity e) {

		setIndice(ei.getIndex_name());
		setStockName(e.getsymbol());
		setStockFullName(e.getFullName());
		setException(exc);
		add_to_es();

	}

	public BaseError(Exception exc, Entity e) {
		setIndice(e.getIndice());
		setStockName(e.getsymbol());
		setStockFullName(e.getFullName());
		setException(exc);
		add_to_es();
	}

	public static Long get_latest_base_error_time() {

		Long last_base_error_at = 0l;

		SearchResponse resp = LocalEs.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("complex_calculation_error")
				.setQuery(QueryBuilders.matchAllQuery()).setSize(1)
				.addSort("error_time", SortOrder.DESC).addField("error_time")
				.execute().actionGet();

		for (SearchHit hit : resp.getHits().getHits()) {
			last_base_error_at = (Long) hit.field("error_time").getValue();
		}

		return last_base_error_at;

	}

	public void add_to_es() {
		/**
		 * Map<String,Object> complex_calculation_error_es_object = new
		 * HashMap<String,Object>();
		 * complex_calculation_error_es_object.put("complex_name",
		 * getComplex_name());
		 * complex_calculation_error_es_object.put("log_unique_identifier",
		 * LoggingDailyOperations.log_unique_identifier);
		 * complex_calculation_error_es_object.put("stockName", stockName);
		 * complex_calculation_error_es_object.put("stockFullName",
		 * stockFullName); complex_calculation_error_es_object.put("indice",
		 * indice);
		 * complex_calculation_error_es_object.put("actual_day_id_start",
		 * actual_day_id_start);
		 * complex_calculation_error_es_object.put("actual_day_id_end"
		 * ,actual_day_id_end);
		 * complex_calculation_error_es_object.put("extended_day_id_start",
		 * getDay_id_inclusive());
		 * complex_calculation_error_es_object.put("error_description",
		 * exception_trace.toString());
		 * complex_calculation_error_es_object.put("failed_where",
		 * failed_where);
		 **/
		try {
			LocalEs
					.getInstance()
					.prepareIndex("tradegenie_titan",
							"complex_calculation_error")
					.setSource(
							GroovyStatements.getInstance().objmapper
									.writeValueAsString(this)).execute()
					.actionGet();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ElasticsearchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * public void add_to_redis() { Gson gson = new Gson(); String
	 * stock_operations_keys_name = getStockName() + "_" + getStockFullName() +
	 * "_" + getIndice() + "_error_log_" +
	 * LoggingDailyOperations.get_instance().log_unique_identifier; try (Jedis
	 * jedis = RedisManager.getInstance().getJedis()) {
	 * jedis.lpush(stock_operations_keys_name, gson.toJson(this)); } String
	 * stock_operations_name = getStockName() + "_" + getStockFullName() + "_" +
	 * getIndice();
	 * if(LoggingDailyOperations.get_instance().getIndividual_stock_operations
	 * ().containsKey(stock_operations_name)){ LoggingDailyOperations
	 * .get_instance() .getIndividual_stock_operations()
	 * .get(stock_operations_name).getFailed_complexes().add(this); } else{
	 * LoggingDailyOperations .get_instance() .getIndividual_stock_operations()
	 * .putIfAbsent(stock_operations_name, new StockOperations(getStockName(),
	 * getStockFullName(), getIndice())); LoggingDailyOperations .get_instance()
	 * .getIndividual_stock_operations()
	 * .get(stock_operations_name).getFailed_complexes().add(this); } }
	 */

}
