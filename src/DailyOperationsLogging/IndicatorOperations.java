package DailyOperationsLogging;

import java.util.ArrayList;

import genericIndicatorFunctions.Indicator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class IndicatorOperations {
	private transient Gson gson;
	private transient Indicator indicator;
	private String indicator_name;
	private Boolean failed_in_any_respect;
	private ArrayList<String> failed_to_add_what;
	private Boolean already_present_in_graph_and_same;
	private Boolean different_because_either_absent_or_different;
	private Boolean not_present_in_graph;
	private Boolean successfully_added_vertex_and_vertex_properties_to_graph;
	private Boolean successfully_added_monitor_with_and_cross_with_edges;
	private ArrayList<String> failed_to_create_complexes_list;
	private ArrayList<ArrayList<String>> exceptions;
	private ArrayList<String> complexes_already_present;
	private ArrayList<String> stocks_processed_for_complex_creation_without_error;
	private ArrayList<String> stocks_having_same_ohlcv_as_this_indicator;
	private Boolean started_processing_for_this_indicator;
	private Boolean finished_processing_for_this_indicator_without_error;
	private static final String identifier_for_log = "indicator_operations";
	private Boolean failed_to_find_the_stock_details_while_complex_generation;
	private Boolean failed_to_get_any_stock_details_for_while_complex_generation;
	private Boolean failed_to_generate_the_indicator_subindicator_pairs;
	private Boolean failed_in_processing_indicator_to_generate_pairs;
	
	

	public Boolean getFailed_in_processing_indicator_to_generate_pairs() {
		return failed_in_processing_indicator_to_generate_pairs;
	}

	public void setFailed_in_processing_indicator_to_generate_pairs(
			Boolean failed_in_processing_indicator_to_generate_pairs) {
		this.failed_in_processing_indicator_to_generate_pairs = failed_in_processing_indicator_to_generate_pairs;
	}

	public Boolean getFailed_to_generate_the_indicator_subindicator_pairs() {
		return failed_to_generate_the_indicator_subindicator_pairs;
	}

	public void setFailed_to_generate_the_indicator_subindicator_pairs(
			Boolean failed_to_generate_the_indicator_subindicator_pairs) {
		this.failed_to_generate_the_indicator_subindicator_pairs = failed_to_generate_the_indicator_subindicator_pairs;
	}

	public Boolean getFailed_to_find_the_stock_details_while_complex_generation() {
		return failed_to_find_the_stock_details_while_complex_generation;
	}

	public void setFailed_to_find_the_stock_details_while_complex_generation(
			Boolean failed_to_find_the_stock_details_while_complex_generation) {
		this.failed_to_find_the_stock_details_while_complex_generation = failed_to_find_the_stock_details_while_complex_generation;
	}

	public Boolean getFailed_to_get_any_stock_details_for_while_complex_generation() {
		return failed_to_get_any_stock_details_for_while_complex_generation;
	}

	public void setFailed_to_get_any_stock_details_for_while_complex_generation(
			Boolean failed_to_get_any_stock_details_for_while_complex_generation) {
		this.failed_to_get_any_stock_details_for_while_complex_generation = failed_to_get_any_stock_details_for_while_complex_generation;
	}

	public Boolean getStarted_processing_for_this_indicator() {
		return started_processing_for_this_indicator;
	}

	public void setStarted_processing_for_this_indicator(
			Boolean started_processing_for_this_indicator) {
		LoggingDailyOperations.get_instance().add_to_log("info"
				,null,"indicator_ops","started processing for indicator:" + getIndicator_name());
		this.started_processing_for_this_indicator = started_processing_for_this_indicator;
	}

	public Boolean getFinished_processing_for_this_indicator_without_error() {
		LoggingDailyOperations.get_instance().add_to_log("info"
				,null,"indicator_ops","finished processing for indicator:" + getIndicator_name());
		return finished_processing_for_this_indicator_without_error;
	}

	public void setFinished_processing_for_this_indicator_without_error(
			Boolean finished_processing_for_this_indicator_without_error) {
		this.finished_processing_for_this_indicator_without_error = finished_processing_for_this_indicator_without_error;
	}

	public ArrayList<String> getStocks_having_same_ohlcv_as_this_indicator() {
		return stocks_having_same_ohlcv_as_this_indicator;
	}

	public void setStocks_having_same_ohlcv_as_this_indicator(
			ArrayList<String> stocks_having_same_ohlcv_as_this_indicator) {
		this.stocks_having_same_ohlcv_as_this_indicator = stocks_having_same_ohlcv_as_this_indicator;
	}

	public ArrayList<String> getStocks_processed_for_complex_creation_without_error() {
		return stocks_processed_for_complex_creation_without_error;
	}

	public void setStocks_processed_for_complex_creation_without_error(
			ArrayList<String> stocks_processed_for_complex_creation_without_error) {
		this.stocks_processed_for_complex_creation_without_error = stocks_processed_for_complex_creation_without_error;
	}
	
	public void addStocks_processed_for_complex_creation_without_error(String stockName){
		LoggingDailyOperations.get_instance().add_to_log("info",
				null,
				identifier_for_log, "finished stock:" + stockName + "for indicator:" + getIndicator_name() );
		
		this.stocks_processed_for_complex_creation_without_error.add(stockName);
	}

	public ArrayList<String> getComplexes_already_present() {
		return complexes_already_present;
	}

	public void setComplexes_already_present(
			ArrayList<String> complexes_already_present) {
		this.complexes_already_present = complexes_already_present;
	}
	
	public void addComplexes_already_present(String complex_name){
		getComplexes_already_present().add(complex_name);
	}

	public ArrayList<ArrayList<String>> getExceptions() {
		return exceptions;
	}

	public void setExceptions(ArrayList<ArrayList<String>> exceptions) {
		this.exceptions = exceptions;
	}

	public void addExceptions(Exception e) {
		ArrayList<String> exception_trace = new ArrayList<String>();
		LoggingDailyOperations.get_instance()
		.add_to_log("exception", e, "create indicator subindicator pairs for each stock",
				null);
		exception_trace.add(e.getLocalizedMessage());
		for (StackTraceElement ste : e.getStackTrace()) {
			exception_trace.add(ste.toString());
		}
		getExceptions().add(exception_trace);
	}

	public ArrayList<String> getFailed_to_create_complexes_list() {
		return failed_to_create_complexes_list;
	}

	public void setFailed_to_create_complexes_list(
			ArrayList<String> failed_to_create_complexes_list) {
		this.failed_to_create_complexes_list = failed_to_create_complexes_list;
	}

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	public Boolean getSuccessfully_added_monitor_with_and_cross_with_edges() {
		return successfully_added_monitor_with_and_cross_with_edges;
	}

	public void setSuccessfully_added_monitor_with_and_cross_with_edges(
			Boolean successfully_added_monitor_with_and_cross_with_edges) {
		this.successfully_added_monitor_with_and_cross_with_edges = successfully_added_monitor_with_and_cross_with_edges;
	}

	public Boolean getSuccessfully_added_vertex_and_vertex_properties_to_graph() {
		return successfully_added_vertex_and_vertex_properties_to_graph;
	}

	public void setSuccessfully_added_vertex_and_vertex_properties_to_graph(
			Boolean successfully_added_vertex_and_vertex_properties_to_graph) {
		this.successfully_added_vertex_and_vertex_properties_to_graph = successfully_added_vertex_and_vertex_properties_to_graph;
	}

	public Boolean getAlready_present_in_graph_and_same() {
		return already_present_in_graph_and_same;
	}

	public void setAlready_present_in_graph_and_same(
			Boolean already_present_in_graph_and_same) {
		this.already_present_in_graph_and_same = already_present_in_graph_and_same;
	}

	public Boolean getdifferent_because_either_absent_or_changed() {
		return different_because_either_absent_or_different;
	}

	public void setdifferent_because_either_absent_or_changed(Boolean different) {
		this.different_because_either_absent_or_different = different;
	}

	public Boolean getNot_present_in_graph() {
		return not_present_in_graph;
	}

	public void setNot_present_in_graph(Boolean not_present_in_graph) {
		this.not_present_in_graph = not_present_in_graph;
	}

	public Indicator getIndicator() {
		return indicator;
	}

	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
	}

	public Boolean getFailed_in_any_respect() {
		return failed_in_any_respect;
	}

	public void setFailed_in_any_respect(Boolean failed_in_any_respect) {
		this.failed_in_any_respect = failed_in_any_respect;
	}

	public ArrayList<String> getFailed_to_add_what() {
		return failed_to_add_what;
	}

	public void setFailed_to_add_what(ArrayList<String> failed_to_add_what) {
		this.failed_to_add_what = failed_to_add_what;
	}

	public void addFailed_to_add_what(String description) {
		getFailed_to_add_what().add(description);
	}

	public IndicatorOperations(Indicator indicator) {
		setIndicator(indicator);
		setIndicator_name(getIndicator().getIndicator_name());
		setFailed_in_any_respect(false);
		setFailed_to_add_what(new ArrayList<String>());
		setAlready_present_in_graph_and_same(false);
		setdifferent_because_either_absent_or_changed(false);
		setSuccessfully_added_monitor_with_and_cross_with_edges(false);
		setSuccessfully_added_vertex_and_vertex_properties_to_graph(false);
		setFailed_to_create_complexes_list(new ArrayList<String>());
		setExceptions(new ArrayList<ArrayList<String>>());
		setStocks_having_same_ohlcv_as_this_indicator(new ArrayList<String>());
		setStocks_processed_for_complex_creation_without_error(new ArrayList<String>());
		
	}

	public String prepare_report_for_log() {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		String string_representation_of_json = this.gson.toJson(this);
		return string_representation_of_json;
	}

}
