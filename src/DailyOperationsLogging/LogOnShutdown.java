package DailyOperationsLogging;

public class LogOnShutdown {

	public void attachShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("writing logs to file, due to program exit.");
				LoggingDailyOperations.get_instance().log_on_exit();
			}
		});
		
	}

}
