package DailyOperationsLogging;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.simple.JSONObject;

import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.StockConstants;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Graph;

/***
 * the stocks are added to the loggingoperations in the
 * 
 * -CreateQueryStringFromDbFields.
 * 
 * -the dates before adjusting are added in the historical stock object. -the
 * dates after adding are also added in the historical stock object. -if no
 * dates were found for the stock then that is also added in the -historical
 * stock object.
 * 
 * 
 * 
 * 
 * @author aditya
 * 
 */
public class LoggingDailyOperations {

	public static ConcurrentHashMap<String, SubIndicatorOperations> subindicator_operations;
	public static ConcurrentHashMap<String, IndicatorOperations> indicator_operations;
	public static LoggingDailyOperations instance;
	public static ConcurrentHashMap<String, StockOperations> individual_stock_operations;
	public static TreeMap<Integer, JSONObject> logs;
	public static HashMap<String, ArrayList<String>> indices_and_components;
	public static TitanGraph graph;
	public static AtomicInteger log_counter;
	public static String log_unique_identifier;
	public static ArrayList<String> indicators_which_changed;
	public static ArrayList<String> indicators_which_stayed_same;
	public static ArrayList<String> subindicators_which_stayed_same;
	public static ArrayList<String> subindicators_which_changed;
	public static AtomicInteger total_complexes_added;
	public static List<String> complexes_which_failed_to_add_into_es;
	public static List<String> complexes_which_failed_to_add_into_graph;
	public static List<LinkedHashMap<String, Object>> day_id_and_complex_name_and_exception_which_failed_for_drawing_edge;
	public static List<HashMap<String, Object>> pairs_failed_to_add_to_es;

	public void add_day_id_and_complex_name_and_exception_which_failed_for_drawing_edge(
			Integer day_id, String complex_name, Exception e) {
		LinkedHashMap exception_trace = new LinkedHashMap<String, String>();
		StackTraceElement[] stack_trace = e.getStackTrace();
		exception_trace.put("localized_messge", e.getLocalizedMessage());
		exception_trace.put("message", e.getMessage());
		for (int i = 0; i < stack_trace.length; i++) {
			exception_trace.put(String.valueOf(i), stack_trace[i].toString());
		}
		LinkedHashMap<String, Object> element_to_add = new LinkedHashMap<String, Object>();
		element_to_add.put("day_id", day_id);
		element_to_add.put("complex_name", complex_name);
		element_to_add.put("exception", exception_trace);
		Gson gson = new Gson();
		LoggingDailyOperations.get_instance().add_to_log("exception",
				new Exception(gson.toJson(element_to_add)),
				"logging_daily_operations - referred from ", null);
		getDay_id_and_complex_name_and_exception_which_failed_for_drawing_edge()
				.add(element_to_add);

	}

	public static List<LinkedHashMap<String, Object>> getDay_id_and_complex_name_and_exception_which_failed_for_drawing_edge() {
		return day_id_and_complex_name_and_exception_which_failed_for_drawing_edge;
	}

	public static List<String> getComplexes_which_failed_to_add_into_es() {
		return complexes_which_failed_to_add_into_es;
	}

	public static void addComplex_which_failed_to_add_into_es(String complex) {
		getComplexes_which_failed_to_add_into_es().add(complex);
	}

	public static List<String> getComplexes_which_failed_to_add_into_graph() {
		return complexes_which_failed_to_add_into_graph;
	}

	public static void addComplex_which_failed_to_add_into_graph(String complex) {
		getComplexes_which_failed_to_add_into_graph().add(complex);
	}

	public static ConcurrentHashMap<String, IndicatorOperations> getIndicator_operations() {
		return indicator_operations;
	}

	public static void setIndicator_operations(
			ConcurrentHashMap<String, IndicatorOperations> indicator_operations) {
		LoggingDailyOperations.indicator_operations = indicator_operations;
	}

	public static ConcurrentHashMap<String, SubIndicatorOperations> getSubindicator_operations() {
		return subindicator_operations;
	}

	public static void setSubindicator_operations(
			ConcurrentHashMap<String, SubIndicatorOperations> subindicator_operations) {
		LoggingDailyOperations.subindicator_operations = subindicator_operations;
	}

	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	/**
	 * here the string is the unique identifier composed of the stock name,
	 * stock full name and index name. the stock operations is a stock
	 * operations object.
	 * 
	 * @return
	 */
	public ConcurrentHashMap<String, StockOperations> getIndividual_stock_operations() {
		return individual_stock_operations;
	}

	public static HashMap<String, ArrayList<String>> getIndices_and_components() {
		return indices_and_components;
	}

	public static void setIndices_and_components(
			HashMap<String, ArrayList<String>> indices_and_components) {
		LoggingDailyOperations.indices_and_components = indices_and_components;
	}

	public static void initialize_logging(TitanGraph g) {
		graph = g;
		get_instance();
	}

	public static LoggingDailyOperations get_instance() {

		if (instance == null) {

			individual_stock_operations = new ConcurrentHashMap<String, StockOperations>();
			instance = new LoggingDailyOperations();
			logs = new TreeMap<Integer, JSONObject>();
			indices_and_components = new HashMap<String, ArrayList<String>>();
			log_counter = new AtomicInteger(0);
			log_unique_identifier = new CalendarFunctions(
					new GregorianCalendar()).getFullDateWithoutTimeZone();
			subindicator_operations = new ConcurrentHashMap<String, SubIndicatorOperations>();
			indicator_operations = new ConcurrentHashMap<String, IndicatorOperations>();
			complexes_which_failed_to_add_into_es = Collections
					.synchronizedList(new ArrayList<String>());
			complexes_which_failed_to_add_into_graph = Collections
					.synchronizedList(new ArrayList<String>());
			day_id_and_complex_name_and_exception_which_failed_for_drawing_edge = Collections
					.synchronizedList(new ArrayList<LinkedHashMap<String, Object>>());
			total_complexes_added = new AtomicInteger(0);
			pairs_failed_to_add_to_es = new ArrayList<HashMap<String, Object>>();
		}

		return instance;

	}

	/**
	 * indice components are the newly scraped components.
	 * 
	 * but first we check what components we already have and add them
	 * 
	 * they are represented by the 'results' variable.
	 * 
	 * 
	 * @param indice
	 * @param indice_components
	 */
	public void add_indice_components(String indice,
			ArrayList<String> indice_components) {

		 System.out.println("this is the indice");
		 System.out.println(indice);

		 System.out.println("these are the indice components.");
		 System.out.println(indice_components.toString());

		String indice_to_use = null;
		for (int i = 0; i < StockConstants.indices.length; i++) {
			if (StockConstants.indices[i].equals(indice)) {
				
				indice_to_use = StockConstants.indices_main[i];
			}
		}

		// System.out.println("indice:" + indice);
		// System.out.println("indice to use:" + indice_to_use);

		Integer counter = 0;
		String stockName = null;
		String stockFullName = null;

		/**
		 * first we add all the existing components into the stockoperations, so
		 * if we find something afterwards that is no longer in the components,
		 * we can stop.
		 * 
		 * 
		 */
		add_to_log(
				"info",
				null,
				"logging_daily_operations",
				"inside logging_daily_operations ,getting the existing components for the index-->"
						+ indice);

		ArrayList<HashMap<String, String>> results = (ArrayList) GroovyStatements
				.getInstance().searchElasticExchange(indice_to_use);

		for (HashMap<String, String> stock : results) {

			List<String> name_fullName = new ArrayList<String>(stock.values());
			StockOperations operations = new StockOperations(
					name_fullName.get(0), name_fullName.get(1), indice_to_use);
			operations.setLog_batch_identifier(log_unique_identifier);
			individual_stock_operations.putIfAbsent(
					operations.get_unique_name(), operations);

		}

		add_to_log("info", null, "logging_daily_operations",
				"got " + results.size() + " existing results");

		// /System.out.println("the result were---" + results.toString());

		add_to_log("info", null, "logging_daily_operations",
				"now going to add scraped components.");
		/**
		 * now we add the newly scraped components.
		 * 
		 * 
		 * 
		 */
		for (String name : indice_components) {

			if (counter == 0) {
				stockName = name;
			} else {
				stockFullName = name;
				String stock_unique_name = stockName + "_" + stockFullName
						+ "_" + indice_to_use;
				StockOperations operations = new StockOperations(stockName,
						stockFullName, indice_to_use);
				operations.setLog_batch_identifier(log_unique_identifier);
				individual_stock_operations.putIfAbsent(stock_unique_name,
						operations);

				/**
				 * now set its found to true.
				 * 
				 */
				individual_stock_operations.get(stock_unique_name)
						.setFound_in_component_scrape(true);
			}

			counter++;

			if (counter > 1) {
				counter = 0;
			}
		}

		add_to_log("info", null, "logging_daily_operations",
				"finished adding newly found components.");

		/**
		 * now what we do is check whether there were any components already
		 * existing in our database that were not found in the component scrape.
		 * 
		 * in that case exception is logged otherwise, we add it to the
		 * components of the present index.
		 * 
		 */
		List<String> list_of_components_for_this_index = new ArrayList<String>();

		for (StockOperations ops : individual_stock_operations.values()) {
			if (!ops.getFound_in_component_scrape()) {
				add_to_log(
						"exception",
						new Exception("did not find--->"
								+ ops.get_unique_name()
								+ "in the component scrape"),
						"logging_daily_operations", null);
			} else {
				if (ops.getIndice().equals(indice_to_use)) {
					list_of_components_for_this_index
							.add(ops.get_unique_name());
				}
			}
		}

		add_to_log("info", null, "logging_daily_operations",
				"list of final components for this index:"
						+ list_of_components_for_this_index.toString());

	}

	/**
	 * 
	 * @param log_data
	 *            -> for logging stock operations, pass as null, for error_log
	 *            or info_log pass the actual gsonified object
	 * @param only_errors
	 *            -> boolean true for the logging of only todays exceptions.
	 * @param op
	 *            -> pass the stock operation of each stock , otherwise null if
	 *            just doing normal log.
	 */
	public void write_to_logfile(String log_data, Boolean only_errors,
			StockOperations op, SubIndicatorOperations sop,
			IndicatorOperations iop,
			HashMap<String, ArrayList<String>> ind_subind_creation_summary,
			List<String> failed_complexes_in_es,
			List<String> failed_complexes_in_graph) {
		try {

			String todays_directory_name = new CalendarFunctions(
					new GregorianCalendar())
					.getFullDateWithoutTimeZoneOnlyDate();

			File todays_directory = new File("DailyOperationsLogs/"
					+ todays_directory_name);
			boolean directory_create_result = false;
			if (!todays_directory.exists()) {

				try {
					todays_directory.mkdir();
					directory_create_result = true;
				} catch (SecurityException se) {
					// handle it
					se.printStackTrace();
				}

			} else {
				directory_create_result = true;
			}

			File file = null;
			if (directory_create_result) {
				// now check if the unique identifier directory exists?
				File log_batch_directory = new File("DailyOperationsLogs/"
						+ todays_directory_name + "/" + log_unique_identifier);
				boolean log_batch_directory_create_result = false;
				if (!log_batch_directory.exists()) {

					try {
						log_batch_directory.mkdir();
						directory_create_result = true;
					} catch (SecurityException se) {
						// handle it
						directory_create_result = false;
						se.printStackTrace();
					}

				} else {
					directory_create_result = true;
				}

				if (directory_create_result) {

					String directory_prefix = log_batch_directory
							.getAbsolutePath() + "/";
					if (op != null) {
						file = new File(directory_prefix
								+ op.getStockName().replace("/", "") + ".json");
						log_data = op.prepare_report_for_log();

					} else if (sop != null) {
						file = new File(directory_prefix
								+ sop.getSubindicator().getSubIndicator_name()
										.replace("/", "") + ".json");
						log_data = sop.prepare_report_for_log();
					} else if (iop != null) {
						file = new File(directory_prefix
								+ iop.getIndicator().getIndicator_name()
										.replace("/", "") + ".json");
						log_data = iop.prepare_report_for_log();
					} else if (ind_subind_creation_summary != null) {

						Gson gson_l = new GsonBuilder().setPrettyPrinting()
								.create();
						file = new File(directory_prefix
								+ "subind_ind_creation_summary.json");
						log_data = gson_l.toJson(ind_subind_creation_summary);
					} else {

						if (only_errors) {
							file = new File(directory_prefix
									+ "_error_log.json");
						} else {
							if (failed_complexes_in_es != null) {
								file = new File(directory_prefix
										+ "_failed_complexes_in_es.json");
							} else if (failed_complexes_in_graph != null) {
								file = new File(directory_prefix
										+ "_failed_complexes_in_graph.json");
							} else {

								file = new File(directory_prefix
										+ "_info_log.json");
							}

						}
					}
					// if file doesnt exists, then create it
					if (file != null) {
						// if (!file.exists()) {

						file.createNewFile();
						// }

						// FileWriter fileWritter = new
						// FileWriter(file.getName(),
						// false);

						// System.out
						// .println("came to write the file and the name is :"
						// + file.getName());

						ReadWriteTextToFile.getInstance().writeFilefile(file,
								log_data);
					}
				}

			}

		} catch (IOException logger_exception) {
			logger_exception.printStackTrace();
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		for (Map.Entry<String, StockOperations> entry : getIndividual_stock_operations()
				.entrySet()) {
			builder.append("name of stock-->" + entry.getKey());
			builder.append("stockoperations--->" + entry.getValue().toString());
			builder.append("\n");
		}
		return builder.toString();
	}

	public void add_to_log(String log_type, Exception e, String identifier,
			String message) {

		GregorianCalendar cal = new GregorianCalendar();
		String date = new CalendarFunctions(cal).gettime_for_logger();
		JSONObject object = new JSONObject();
		// Gson gson = new GsonBuilder().setPrettyPrinting().create();
		object.put("log_batch_id", log_unique_identifier);
		object.put("time", date);
		object.put("type", log_type);
		if (e != null) {
			if (log_type.equals("exception")) {
				BaseError err = new BaseError(e, identifier);
			}
			JSONObject exception_info = new JSONObject();
			exception_info.put("message", e.getMessage());
			exception_info.put("localized_message", e.getLocalizedMessage());
			LinkedHashMap<String, String> exception_trace = new LinkedHashMap<>();

			for (int i = 0; i < e.getStackTrace().length; i++) {
				exception_trace.put(String.valueOf(i),
						e.getStackTrace()[i].toString());
			}
			exception_info.put("stack_trace", exception_trace);
			object.put(identifier, exception_info);
		} else {

			object.put(identifier, message);
		}

		/**
		 * this part is only for putting out while runnin the program all the
		 * logs.
		 * 
		 */
		String final_key_for_logs = log_type + "_" + date;
		StringBuilder sb = new StringBuilder();
		sb.append(final_key_for_logs + ":");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		sb.append("\r\n");
		sb.append(gson.toJson(object));
		sb.append("\r\n");
		System.out.println(sb.toString());
		/**
		 * ends.
		 * 
		 */

		logs.put(log_counter.incrementAndGet(), object);

		// if (log_type.equals("exception")) {
		// write_to_logfile(logs.get(final_key_for_logs).toJSONString(),false);
		// }

	}

	public static Boolean has_exception() {
		for (Map.Entry<Integer, JSONObject> entry : logs.entrySet()) {
			if (entry.getValue().get("type").equals("exception")) {
				// System.out
				// .println("log contained an exception and we should not proceed");
				return true;
			}
		}
		return false;
	}

	public void log_on_exit() {
		LinkedHashMap<Integer, JSONObject> exceptions_only = new LinkedHashMap<Integer, JSONObject>();

		for (Map.Entry<Integer, JSONObject> entry : logs.entrySet()) {
			if (entry.getValue().get("type").equals("exception")) {
				exceptions_only.put(entry.getKey(), entry.getValue());
			}
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		// add the entire log as the info log.
		write_to_logfile(gson.toJson(logs), false, null, null, null, null,
				null, null);
		// write only the errors.

		write_to_logfile(gson.toJson(exceptions_only), true, null, null, null,
				null, null, null);
		// write each individual operation.

		for (Map.Entry<String, StockOperations> entry : individual_stock_operations
				.entrySet()) {
			write_to_logfile(null, false, entry.getValue(), null, null, null,
					null, null);
		}

		for (Map.Entry<String, SubIndicatorOperations> entry : subindicator_operations
				.entrySet()) {
			write_to_logfile(null, false, null, entry.getValue(), null, null,
					null, null);
		}

		for (Map.Entry<String, IndicatorOperations> entry : indicator_operations
				.entrySet()) {
			write_to_logfile(null, false, null, null, entry.getValue(), null,
					null, null);
		}

		HashMap<String, ArrayList<String>> ind_subind_creation_summary = new HashMap<String, ArrayList<String>>();

		ind_subind_creation_summary.put("indicators_which_changed",
				indicators_which_changed);
		ind_subind_creation_summary.put("indicators_which_stayed_same",
				indicators_which_stayed_same);
		ind_subind_creation_summary.put("subindicators_which_changed",
				subindicators_which_changed);
		ind_subind_creation_summary.put("subindicators_which_stayed_same",
				subindicators_which_stayed_same);
		write_to_logfile(null, false, null, null, null,
				ind_subind_creation_summary, null, null);
		write_to_logfile(gson.toJson(complexes_which_failed_to_add_into_es),
				false, null, null, null, null, null,
				complexes_which_failed_to_add_into_graph);
	}

	public void exit_and_log_before_exiting(TitanGraph g) {
		log_on_exit();
		// g.commit();
		// g.shutdown();
		System.exit(1);

	}

	public void exit_without_graph_and_log_before_exiting() {
		log_on_exit();
		System.exit(1);
	}

}
