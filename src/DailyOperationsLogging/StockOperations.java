package DailyOperationsLogging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.json.simple.JSONObject;

import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.HistoricalStock;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class StockOperations {
	
	private Boolean all_things_in_scrape_less_than_start_date;
	
	
	public Boolean getAll_things_in_scrape_less_than_start_date() {
		return all_things_in_scrape_less_than_start_date;
	}

	public void setAll_things_in_scrape_less_than_start_date(
			Boolean all_things_in_scrape_less_than_start_date) {
		this.all_things_in_scrape_less_than_start_date = all_things_in_scrape_less_than_start_date;
	}

	/**
	 * was there an error anywhere while calculating the complexes?
	 * 
	 * true -> yes
	 * 
	 */
	private Boolean complex_calculation_error_encountered;
	
	
	public Boolean getComplex_calculation_error_encountered() {
		return complex_calculation_error_encountered;
	}

	public void setComplex_calculation_error_encountered(
			Boolean complex_calculation_error_encountered) {
		this.complex_calculation_error_encountered = complex_calculation_error_encountered;
	}

	/**
	 * was it that zero days were found when we requested from redis.
	 * 
	 * false => zero days were found.
	 * 
	 */
	private Boolean days_found_of_requested_from_redis;
	
	
	public Boolean getDays_found_of_requested_from_redis() {
		return days_found_of_requested_from_redis;
	}

	public void setDays_found_of_requested_from_redis(
			Boolean days_found_of_requested_from_redis) {
		this.days_found_of_requested_from_redis = days_found_of_requested_from_redis;
	}

	/**
	 * what is the range of day ids requested for this stock in the complex calculation request.
	 * this refers to :
	 * the actual requested days and not the 300 days minused.
	 * 
	 */
	private ArrayList<Integer> day_ids_requested_for_this_stock_in_complex_calculation_request;
	
	public ArrayList<Integer> getDay_ids_requested_for_this_stock_in_complex_calculation_request() {
		return day_ids_requested_for_this_stock_in_complex_calculation_request;
	}

	public void setDay_ids_requested_for_this_stock_in_complex_calculation_request(
			ArrayList<Integer> day_ids_requested_for_this_stock_in_complex_calculation_request) {
		this.day_ids_requested_for_this_stock_in_complex_calculation_request = day_ids_requested_for_this_stock_in_complex_calculation_request;
	}

	/**
	 * which days were found in the redis ohlcv sorted set.
	 * given the range that was requested, in the complex calculation request.
	 * 
	 */
	private ArrayList<Integer> days_got_from_redis_of_requested_from_complex_calculation_request;
	
	public ArrayList<Integer> getDays_got_from_redis_of_requested_from_complex_calculation_request() {
		return days_got_from_redis_of_requested_from_complex_calculation_request;
	}

	public void setDays_got_from_redis_of_requested_from_complex_calculation_request(
			ArrayList<Integer> days_got_from_redis_of_requested_from_complex_calculation_request) {
		this.days_got_from_redis_of_requested_from_complex_calculation_request = days_got_from_redis_of_requested_from_complex_calculation_request;
	}

	/**
	 * the batch identifier of this log group.
	 * 
	 */
	private String log_batch_identifier;
	private String stockName;
	private String stockFullName;
	private String indice;
	private GregorianCalendar query_start_date;
	private GregorianCalendar query_end_date;
	private HistoricalStock stock_object_got_from_api_before_insertion;
	
	/**
	 * the pairs failed to update to es, during each day download.
	 * 
	 */
	private List<String> pair_change_failed_to_update_to_es;
	/**
	 * the pairs succesfully updated to es during each daily download.
	 * 
	 */
	private List<String> pairs_successfully_updated_to_es;
	
	public List<String> getPairs_successfully_updated_to_es() {
		return pairs_successfully_updated_to_es;
	}

	public void setPairs_successfully_updated_to_es(
			List<String> pairs_successfully_updated_to_es) {
		this.pairs_successfully_updated_to_es = pairs_successfully_updated_to_es;
	}

	public List<String> getPair_change_failed_to_update_to_es() {
		return pair_change_failed_to_update_to_es;
	}

	public void setPair_change_failed_to_update_to_es(
			List<String> pair_change_failed_to_update_to_es) {
		this.pair_change_failed_to_update_to_es = pair_change_failed_to_update_to_es;
	}

	private Boolean failed_to_add_company_information;
	
	public Boolean getFailed_to_add_company_information() {
		return failed_to_add_company_information;
	}

	public void setFailed_to_add_company_information(
			Boolean failed_to_add_company_information) {
		this.failed_to_add_company_information = failed_to_add_company_information;
	}

	

	/**
	 * this refers to dates + date -> common day id edge successfully added.
	 */
	
	private ArrayList<String> dates_successfully_added_to_db;
	
	/**
	 * 
	 * date is added here if there is exception in either basic stock vertex
	 * or stock vertex to common day id vertex edge.
	 * 
	 */
	private ConcurrentHashMap<String,JSONObject> dates_failed_in_database_adding;
	
	/**
	 * stock vertex (historical data) already present in graph.
	 * 
	 */
	private ArrayList<String> dates_already_present_in_db;
	

	/**
	 * 
	 * stock vertices(dates) for this stock who already had a edge to a common day id vertex.
	 * 
	 */
	private ArrayList<String> dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db;
	

	private ArrayList<String> complexes_expected_to_be_touched;
	private HashMap<String, JSONObject> complexes_touched;
	private Boolean no_data_got_from_api_for_dates;
	private ArrayList<Calendar> dates_before_adjusting;
	
	/**
	 * these are the day connector edges - which were successfully drwan.
	 * 
	 */
	private ConcurrentHashMap<String, ArrayList<String>> edges_drawn_for_each_date;
	
	/**
	 * these are the day connector edges which failed to draw.
	 * 
	 * 
	 */
	private ConcurrentHashMap<String,ArrayList<String>> edges_failed_to_draw;
	
	private Boolean found_in_component_scrape;
	private ConcurrentHashMap<String,Triplet<Integer,Double,Double>> aberration_type_and_index;
	private transient Gson gson;

	
	
	
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	//GETTERS AND SETTERS START HERE.
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
	public String getLog_batch_identifier() {
		return log_batch_identifier;
	}

	public void setLog_batch_identifier(String log_batch_identifier) {
		this.log_batch_identifier = log_batch_identifier;
	}


	public ArrayList<String> getDates_of_stock_vertex_to_common_day_id_edges_already_present_in_db() {
		return dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db;
	}

	public void setDates_of_stock_vertex_to_common_day_id_edges_already_present_in_db(
			ArrayList<String> dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db) {
		this.dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db = dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db;
	}
	
	
	public ArrayList<String> getDates_already_present_in_db() {
		return dates_already_present_in_db;
	}

	public void setDates_already_present_in_db(
			ArrayList<String> dates_already_present_in_db) {
		this.dates_already_present_in_db = dates_already_present_in_db;
	}
	
	
	public ConcurrentHashMap<String,Triplet<Integer,Double,Double>> getAberration_type_and_index() {
		return aberration_type_and_index;
	}

	public void setAberration_type_and_index(
			ConcurrentHashMap<String, Triplet<Integer,Double,Double>> aberration_type_and_index) {
		this.aberration_type_and_index = aberration_type_and_index;
	}

	public Boolean getFound_in_component_scrape() {
		return found_in_component_scrape;
	}

	public void setFound_in_component_scrape(Boolean found_in_component_scrape) {
		this.found_in_component_scrape = found_in_component_scrape;
	}

	public ConcurrentHashMap<String, ArrayList<String>> getEdges_failed_to_draw() {
		return edges_failed_to_draw;
	}

	public void setEdges_failed_to_draw(
			ConcurrentHashMap<String, ArrayList<String>> edges_failed_to_draw) {
		this.edges_failed_to_draw = edges_failed_to_draw;
	}

	/**
	 * private Pair<GregorianCalendar,GregorianCalendar>
	 * oldest_and_latest_dates_before_starting; private Integer
	 * total_dates_for_this_stock_before_starting; private
	 * Pair<GregorianCalendar,GregorianCalendar> oldest_and_latest_dates_at_end;
	 * private Integer total_dates_for_this_stock_at_end;
	 **/

	public ConcurrentHashMap<String, ArrayList<String>> getEdges_drawn_for_each_date() {
		return edges_drawn_for_each_date;
	}

	public void setEdges_drawn_for_each_date(
			ConcurrentHashMap<String, ArrayList<String>> edges_drawn_for_each_date) {
		this.edges_drawn_for_each_date = edges_drawn_for_each_date;
	}

	public ArrayList<Calendar> getDates_before_adjusting() {
		return dates_before_adjusting;
	}

	public void setDates_before_adjusting(
			ArrayList<Calendar> dates_before_adjusting) {
		this.dates_before_adjusting = dates_before_adjusting;
	}

	public Boolean getNo_data_got_from_api_for_dates() {
		return no_data_got_from_api_for_dates;
	}

	public void setNo_data_got_from_api_for_dates(
			Boolean no_data_got_from_api_for_dates) {
		this.no_data_got_from_api_for_dates = no_data_got_from_api_for_dates;
	}

	public HistoricalStock getStock_object_got_from_api_before_insertion() {
		return stock_object_got_from_api_before_insertion;
	}

	public void setStock_object_got_from_api_before_insertion(
			HistoricalStock stock_object_got_from_api_before_insertion) {
		this.stock_object_got_from_api_before_insertion = stock_object_got_from_api_before_insertion;
	}

	public ConcurrentHashMap<String,JSONObject> getDates_failed_in_database_adding() {
		return dates_failed_in_database_adding;
	}

	

	/*
	 * public Pair<GregorianCalendar, GregorianCalendar>
	 * getOldest_and_latest_dates_before_starting() { return
	 * oldest_and_latest_dates_before_starting; }
	 * 
	 * public void setOldest_and_latest_dates_before_starting(
	 * Pair<GregorianCalendar, GregorianCalendar>
	 * oldest_and_latest_dates_before_starting) {
	 * this.oldest_and_latest_dates_before_starting =
	 * oldest_and_latest_dates_before_starting; }
	 * 
	 * public Integer getTotal_dates_for_this_stock_before_starting() { return
	 * total_dates_for_this_stock_before_starting; }
	 * 
	 * public void setTotal_dates_for_this_stock_before_starting( Integer
	 * total_dates_for_this_stock_before_starting) {
	 * this.total_dates_for_this_stock_before_starting =
	 * total_dates_for_this_stock_before_starting; }
	 * 
	 * public Pair<GregorianCalendar, GregorianCalendar>
	 * getOldest_and_latest_dates_at_end() { return
	 * oldest_and_latest_dates_at_end; }
	 * 
	 * public void setOldest_and_latest_dates_at_end( Pair<GregorianCalendar,
	 * GregorianCalendar> oldest_and_latest_dates_at_end) {
	 * this.oldest_and_latest_dates_at_end = oldest_and_latest_dates_at_end; }
	 * 
	 * public Integer getTotal_dates_for_this_stock_at_end() { return
	 * total_dates_for_this_stock_at_end; }
	 * 
	 * public void setTotal_dates_for_this_stock_at_end( Integer
	 * total_dates_for_this_stock_at_end) {
	 * this.total_dates_for_this_stock_at_end =
	 * total_dates_for_this_stock_at_end; }
	 */

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockFullName() {
		return stockFullName;
	}

	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public GregorianCalendar getQuery_start_date() {
		return query_start_date;
	}

	public void setQuery_start_date(GregorianCalendar query_start_date) {
		this.query_start_date = query_start_date;
	}

	public GregorianCalendar getQuery_end_date() {
		return query_end_date;
	}

	public void setQuery_end_date(GregorianCalendar query_end_date) {
		this.query_end_date = query_end_date;
	}

	public ArrayList<String> getDates_successfully_added_to_db() {
		if (this.dates_successfully_added_to_db == null) {
			this.dates_successfully_added_to_db = new ArrayList<String>();
		}
		return dates_successfully_added_to_db;
	}

	public void addDate_successfully_added_to_db(String date) {
		getDates_successfully_added_to_db().add(date);
	}

	public ArrayList<String> getComplexes_expected_to_be_touched() {
		if (this.complexes_expected_to_be_touched == null) {
			this.complexes_expected_to_be_touched = new ArrayList<String>();
		}
		if (this.complexes_touched == null) {
			this.complexes_touched = new HashMap<String, JSONObject>();
		}
		return complexes_expected_to_be_touched;
	}

	public void addComplex_expected_to_be_touched(
			String complex_expected_to_be_touched) {
		getComplexes_expected_to_be_touched().add(
				complex_expected_to_be_touched);
		getComplexes_touched().put(complex_expected_to_be_touched,
				new JSONObject());
	}

	public HashMap<String, JSONObject> getComplexes_touched() {
		return this.complexes_touched;
	}

	/**
	 * expects following any of following options:
	 * 
	 * es - 1/0 edge - 1/0 vertex - 1/0
	 * 
	 * 
	 * @param key_value_pair
	 */
	public void add_complex_touched_status(Pair<String, String> key_value_pair,
			String complex_name) {
		getComplexes_touched().get(complex_name).put(
				key_value_pair.getValue0(), key_value_pair.getValue1());
	}

	public void add_date_edge(String date, String edge_drawn_to_date) {
		this.edges_drawn_for_each_date.putIfAbsent(date,
				new ArrayList<String>());
		this.edges_drawn_for_each_date.get(date).add(edge_drawn_to_date);
	}
	
	public void add_date_edge_failed_to_draw(String date, String edge_drawn_to_date){
		this.edges_failed_to_draw.putIfAbsent(date, new ArrayList<String>());
		this.edges_failed_to_draw.get(date).add(edge_drawn_to_date);
	}
	
	/***
	 * add historical stock date vertex already exists in database to array.
	 * 
	 * 
	 * 
	 * @return
	 */
	public void add_dates_already_existing_in_database(String date){
		getDates_already_present_in_db().add(date);
	}
	
	/***
	 * add dates of this stock which already had edges drawn to common day id verteices. 
	 * 
	 * 
	 * @return
	 */
	public void add_dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db(String date){
		getDates_of_stock_vertex_to_common_day_id_edges_already_present_in_db().add(date);
	}
	
	public Boolean assess_for_exception(){
		
		return false;
	}
	
	/**
	 * initialized at the time of the component scraper.
	 * 
	 * @param stockName
	 * @param stockFullName
	 * @param indice
	 */
	public StockOperations(String stockName, String stockFullName, String indice){
		setStockName(stockName);
		setStockFullName(stockFullName);
		setIndice(indice);
		this.complexes_expected_to_be_touched = new ArrayList<String>();
		this.complexes_touched = new HashMap<String, JSONObject>();
		this.dates_before_adjusting = new ArrayList<Calendar>();
		this.dates_failed_in_database_adding = new ConcurrentHashMap<String,JSONObject>();
		this.dates_successfully_added_to_db = new ArrayList<String>();
		this.edges_drawn_for_each_date = new ConcurrentHashMap<String, ArrayList<String>>();
		this.edges_failed_to_draw = new ConcurrentHashMap<String, ArrayList<String>>();
		this.aberration_type_and_index = new ConcurrentHashMap<String,Triplet<Integer,Double,Double>>();
		this.dates_already_present_in_db = new ArrayList<String>();
		this.dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db = new ArrayList<String>();
		setFound_in_component_scrape(false);
		
		this.pair_change_failed_to_update_to_es = new ArrayList<String>();
		this.pairs_successfully_updated_to_es = new ArrayList<String>();
		this.day_ids_requested_for_this_stock_in_complex_calculation_request = new ArrayList<Integer>();
		this.days_got_from_redis_of_requested_from_complex_calculation_request = new ArrayList<Integer>();
		 
	}
	
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(new CalendarFunctions(getQuery_start_date())
				.getFullDateWithoutTimeZone()
				+ "---->"
				+ new CalendarFunctions(getQuery_end_date())
						.getFullDateWithoutTimeZone());
		return builder.toString();
	}

	public String get_unique_name() {
		return getStockName() + "_" + getStockFullName() + "_" + getIndice();
	}
	
	public String prepare_report_for_log(){
		 this.gson = new GsonBuilder().setPrettyPrinting().create();
		 String string_representation_of_json = this.gson.toJson(this);
		 return string_representation_of_json;
	}

}
