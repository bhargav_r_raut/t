package DailyOperationsLogging;

import genericIndicatorFunctions.SubIndicator;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SubIndicatorOperations {
	private transient Gson gson;
	private transient SubIndicator subindicator;
	private String subindicator_name;
	private Boolean failed_in_any_respect;
	private ArrayList<String> failed_to_add_what;
	private Boolean already_present_in_graph_and_same;
	private Boolean different_either_absent_before_or_changed_now;
	private Boolean not_present_in_graph;
	private Boolean added_vertex_to_graph;
	private Boolean added_parameters_to_graph;
	private HashMap<String,ArrayList<String>> edge_types_and_ids;
	private ArrayList<ArrayList<String>> exceptions;
	
	

	public ArrayList<ArrayList<String>> getExceptions() {
		return exceptions;
	}

	public void setExceptions(ArrayList<ArrayList<String>> exceptions) {
		this.exceptions = exceptions;
	}
	
	public void addExceptions(Exception e){
		ArrayList<String> exception_trace = new ArrayList<String>();
		exception_trace.add(e.getLocalizedMessage());
		for(StackTraceElement ste : e.getStackTrace()){
			exception_trace.add(ste.toString());
		}
		getExceptions().add(exception_trace);
	}
	
	public String getSubindicator_name() {
		return subindicator_name;
	}

	public void setSubindicator_name(String subindicator_name) {
		this.subindicator_name = subindicator_name;
	}

	public HashMap<String, ArrayList<String>> getEdge_types_and_ids() {
		return edge_types_and_ids;
	}

	public void setEdge_types_and_ids(
			HashMap<String, ArrayList<String>> edge_types_and_ids) {
		this.edge_types_and_ids = edge_types_and_ids;
	}
	
	
	public void put_edge_type_id(String edge_type, String edge_id){
		if(!getEdge_types_and_ids().containsKey(edge_type)){
			getEdge_types_and_ids().put(edge_type, new ArrayList<String>());
		}
		
		getEdge_types_and_ids().get(edge_type).add(edge_id);
		
		
	}

	public Boolean getAdded_vertex_to_graph() {
		return added_vertex_to_graph;
	}

	public void setAdded_vertex_to_graph(Boolean added_vertex_to_graph) {
		this.added_vertex_to_graph = added_vertex_to_graph;
	}

	public Boolean getAdded_parameters_to_graph() {
		return added_parameters_to_graph;
	}

	public void setAdded_parameters_to_graph(Boolean added_parameters_to_graph) {
		this.added_parameters_to_graph = added_parameters_to_graph;
	}

	public Boolean getAlready_present_in_graph_and_same() {
		return already_present_in_graph_and_same;
	}

	public void setAlready_present_in_graph_and_same(
			Boolean already_present_in_graph_and_same) {
		this.already_present_in_graph_and_same = already_present_in_graph_and_same;
	}

	public Boolean getdifferent() {
		return different_either_absent_before_or_changed_now;
	}

	public void setdifferent(
			Boolean already_present_in_graph_but_different) {
		this.different_either_absent_before_or_changed_now = already_present_in_graph_but_different;
	}

	public Boolean getNot_present_in_graph() {
		return not_present_in_graph;
	}

	public void setNot_present_in_graph(Boolean not_present_in_graph) {
		this.not_present_in_graph = not_present_in_graph;
	}

	public SubIndicator getSubindicator() {
		return subindicator;
	}

	public void setSubindicator(SubIndicator subindicator) {
		this.subindicator = subindicator;
	}

	public Boolean getFailed_in_any_respect() {
		return failed_in_any_respect;
	}

	public void setFailed_in_any_respect(Boolean failed_in_any_respect) {
		this.failed_in_any_respect = failed_in_any_respect;
	}

	public ArrayList<String> getFailed_to_add_what() {
		return failed_to_add_what;
	}

	public void setFailed_to_add_what(ArrayList<String> failed_to_add_what) {
		this.failed_to_add_what = failed_to_add_what;
	}

	public void addFailed_to_add_what(String description){
		getFailed_to_add_what().add(description);
	}
	
	public SubIndicatorOperations(SubIndicator subindicator) {
		setSubindicator(subindicator);
		setSubindicator_name(getSubindicator().getSubIndicator_name());
		setFailed_in_any_respect(false);
		setFailed_to_add_what(new ArrayList<String>());
		setExceptions(new ArrayList<ArrayList<String>>());
		setEdge_types_and_ids(new HashMap<String,ArrayList<String>>());
		setAlready_present_in_graph_and_same(false);
		setAdded_vertex_to_graph(false);
		setAdded_parameters_to_graph(false);
		setdifferent(false);
	}

	public String prepare_report_for_log() {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		String string_representation_of_json = this.gson.toJson(this);
		return string_representation_of_json;
	}

}
