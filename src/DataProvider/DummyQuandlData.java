package DataProvider;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;

public class DummyQuandlData {

	private static JSONObject response_object;

	public static JSONObject getResponse_object() {
		return response_object;
	}

	public static void setResponse_object(JSONObject response_object) {
		DummyQuandlData.response_object = response_object;
	}

	private static String response_string;

	public static String getResponse_string() {
		return response_object.toString();
	}

	public static void generate_dummy_response_in_range(String start_date, String end_date) throws JSONException {
		// first get the start day and end day id
		Integer start_day_id = CentralSingleton.gmt_datestring_to_day_id.get(start_date);
		Integer end_day_id = CentralSingleton.gmt_datestring_to_day_id.get(end_date);
		response_object = new JSONObject();
		Random r = new Random();
		JSONArray all_days_prices = new JSONArray();
		
		Integer counter = 0;
		for (Map.Entry<Integer, DataPoint> entry : CentralSingleton.getInstance().gmt_day_id_to_dataPoint.entrySet()) {
			if (entry.getKey() >= start_day_id && entry.getKey() <= end_day_id) {
				String dateString = entry.getValue().getDateString();
				Double open = 1.0;
				Double high = 2.0;
				Double low = 3.0;
				Double close = 4.0;
				Double volume = 10.0;
				Double adj_close = 4.0;
				JSONArray day_prices = new JSONArray();
				day_prices.put(dateString);
				day_prices.put(open);
				day_prices.put(high);
				day_prices.put(low);
				day_prices.put(close);
				day_prices.put(volume);
				day_prices.put(adj_close);
				all_days_prices.put(counter, day_prices);
				counter++;
			}
		}
		JSONObject all_day_prices_holder = new JSONObject();
		all_day_prices_holder.put("data", all_days_prices);
		response_object.put("dataset", all_day_prices_holder);

	}

	public static void generate_dummy_response() throws JSONException {
		response_object = new JSONObject();
		// we will be generating 1500 datapoints.
		// datestring, open , high, low, close, volume, adjusted close.
		Random r = new Random();
		JSONArray all_days_prices = new JSONArray();
		Integer counter = 0;
		for (Map.Entry<Integer, DataPoint> entry : CentralSingleton.getInstance().gmt_day_id_to_dataPoint.entrySet()) {

			String dateString = entry.getValue().getDateString();
			Double open = 1.0 + r.nextDouble();
			Double high = 1.0 + r.nextDouble();
			Double low = 1.0 + r.nextDouble();
			Double close = 1.0 + r.nextDouble();
			Double volume = 1.0 + r.nextDouble();
			Double adj_close = 1.0 + r.nextDouble();
			JSONArray day_prices = new JSONArray();
			day_prices.put(dateString);
			day_prices.put(open);
			day_prices.put(high);
			day_prices.put(low);
			day_prices.put(close);
			day_prices.put(volume);
			day_prices.put(adj_close);
			all_days_prices.put(counter, day_prices);
			counter++;
		}
		JSONObject all_day_prices_holder = new JSONObject();
		all_day_prices_holder.put("data", all_days_prices);
		response_object.put("dataset", all_day_prices_holder);
	}

	public static void main(String[] args) throws Exception {
		CentralSingleton.initializeSingleton();
		generate_dummy_response();
		System.out.println(response_object.toString(5));
	}

}
