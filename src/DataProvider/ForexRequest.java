package DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.tradegenie.tests.DownloadEntityExchangeTests;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.TestMethodExecutorArgs;
import ExchangeBuilder.TestMethodsExecutor;
import Titan.ReadWriteTextToFile;
import yahoo_finance_historical_scraper.StockConstants;

public class ForexRequest extends RequestBase {

	private String date;
	private ArrayList<String> dates;
	private String symbol;
	private String base;
	private JSONObject result_object;

	public ForexRequest(String symbol, String fullName, LinkedHashMap<String, Object> other_data, Entity e) {
		super(symbol, fullName, other_data, e);
		// TODO Auto-generated constructor stub
		this.dates = (ArrayList<String>) other_data.get(StockConstants.Entity_forex_request_dates);
		this.base = symbol.substring(symbol.lastIndexOf("/") + 1);

	}

	/*****
	 * possible exceptions: 1. internet failure 2. remote server responds with
	 * error(url invalid / no date in response) 3. remote response is not valid
	 * json. 4. date not yet available.
	 */
	@Override
	public Boolean make_request() {
		LogObject lg_d = new LogObject(getClass());
		lg_d.commit_debug_log("init: make forex request");

		LogObject lg_d2 = new LogObject(getClass());

		try {
			HashMap<String, Object> boo_res = TestMethodsExecutor
					.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.throw_exception_from_make_request));
			if (boo_res.keySet().size() == 1 && boo_res.containsValue(true)) {
				throw new Exception(DownloadEntityExchangeTests.throw_exception_from_make_request);
			}

			JSONObject built_result_object = new JSONObject();

			List<String> dates_stubbed;

			if (StockConstants.test) {
				// sometimes in some tests we have fewer than five dates that we
				// test, so in those cases
				// we take the dates size as the end point.
				Integer end_point = (dates.size() < StockConstants.number_of_requests_to_send_per_forex_entity)
						? dates.size() : StockConstants.number_of_requests_to_send_per_forex_entity;

				dates_stubbed = dates.subList(0, end_point);
			} else {
				dates_stubbed = dates.subList(0, dates.size());
			}

			for (String date : dates_stubbed) {

				String request = StockConstants.forex_request_template.replace(StockConstants.date_placeholder, date)
						.replace(StockConstants.symbol_placeholder,
								getSymbol().substring(0, getSymbol().lastIndexOf("/")))
						.replace(StockConstants.currency_base_placeholder, base);

				setRequest_url(request);

				/****
				 * test code
				 * 
				 */
				TestMethodExecutorArgs args = new TestMethodExecutorArgs(
						DownloadEntityExchangeTests.WHEN_ES_EMPTY_check_request_made_and_start_and_end_dates);
				args.getArguments().put("entity", getE());
				args.getArguments().put("request_made", true);
				args.getArguments().put("request_start_date", this.dates.get(0));
				args.getArguments().put("request_end_date", this.dates.get(this.dates.size() - 1));
				TestMethodsExecutor.exec(args);
				/***
				 * test code ends
				 * 
				 */

				setResponse(ReadWriteTextToFile.getInstance().forex_request(request));
				setResponse_string(getResponse().body().string());
				setResponse_code(getResponse().code());

				/****
				 * test code
				 * 
				 */
				HashMap<String, Object> boo_res2 = TestMethodsExecutor
						.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_non_200_response_code));
				if (boo_res2.keySet().size() == 1 && boo_res2.containsValue(true)) {
					setResponse_code(5);

				}
				/***
				 * test code ends
				 * 
				 */

				lg_d2.add_to_info_packet("request_url", getRequest_url());
				lg_d2.add_to_info_packet("response_string", getResponse_string());
				lg_d2.add_to_info_packet("response_code", getResponse_code());

				if (getResponse_code().intValue() != 200) {

					// if the built_result_object already has some dates, then
					// we
					// return false, otherwise we let it go forwards as we may
					// have been asking
					// for too old dates.

					commit_info_exception_log(StockConstants.response_code_not_200, lg_d2,
							new Exception(StockConstants.response_code_not_200));
					return false;

				} else {

					try {
						HashMap<String, Object> boo_res3 = TestMethodsExecutor.exec(
								new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_invalid_json_response));
						if (boo_res3.keySet().size() == 1 && boo_res3.containsValue(true)) {
							throw new Exception(DownloadEntityExchangeTests.simulate_invalid_json_response);
						}
						JSONObject resp_obj = new JSONObject(getResponse_string());
						// check if the simulate forex date exception return
						// true, and if yes
						// then delete date from the
						HashMap<String, Object> boo_res4 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
								DownloadEntityExchangeTests.simulate_forex_date_not_in_response));
						if (boo_res4.keySet().size() == 1 && boo_res4.containsValue(true)) {
							resp_obj.remove("date");
						}

						if (resp_obj.isNull("date")) {
							// here log an info level exception.
							// this is still a info level thing.
							commit_info_exception_log(StockConstants.forex_date_not_found_in_response, lg_d2,
									new Exception(StockConstants.forex_date_not_found_in_response));

							return false;
						} else {
							if (resp_obj.getString("date").equals(date)) {

								// test what happens if rates not in response.
								HashMap<String, Object> boo_res5 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
										DownloadEntityExchangeTests.simulate_forex_rates_key_not_in_response));
								if (boo_res5.keySet().size() == 1 && boo_res5.containsValue(true)) {
									resp_obj.remove("rates");
								}

								// end test.

								// check if this rates is null or not.
								if (!resp_obj.isNull("rates")) {
									JSONObject resp_obj_rates = resp_obj.getJSONObject("rates");

									// check if the symbol is not found.
									String symbol_stub = getSymbol().substring(0, getSymbol().lastIndexOf("/"));

									// test case to see what to do if symbol is
									// not found in the response
									HashMap<String, Object> boo_res6 = TestMethodsExecutor
											.exec(new TestMethodExecutorArgs(
													DownloadEntityExchangeTests.simulate_forex_symbol_not_in_response));
									if (boo_res6.keySet().size() == 1 && boo_res6.containsValue(true)) {
										resp_obj_rates.remove(symbol_stub);
									}

									if (!resp_obj_rates.isNull(symbol_stub)) {
										built_result_object.put(resp_obj.getString("date"),
												resp_obj_rates.getString(symbol_stub));
									} else {
										commit_info_exception_log(StockConstants.forex_symbol_not_found_in_response,
												lg_d2,
												new Exception(StockConstants.forex_symbol_not_found_in_response));
										return false;
									}

								} else {
									// should this return false? and raise an
									// exception?
									// yes.
									commit_info_exception_log(StockConstants.forex_rates_not_found_in_response, lg_d2,
											new Exception(StockConstants.forex_rates_not_found_in_response));
									return false;

								}

							}

						}
					} catch (Exception e) {
						commit_info_exception_log(StockConstants.response_json_parse_exception, lg_d2, e);

						return false;
					}

				}

			}

			setResults_object(built_result_object);
			setRequest_response_successfull(true);
			return true;
		} catch (Exception e) {
			commit_info_exception_log(StockConstants.request_failed, lg_d2, e);
			return false;
		}
	}

	@Override
	public void prepare_datapoint_for_entity() throws Exception {
		// TODO Auto-generated method stub
		TestMethodExecutorArgs t_args = new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.add_datapoint_to_entity_method_triggered);
		t_args.getArguments().put("entity", getE());
		TestMethodsExecutor.exec(t_args);

		Iterator<String> dates_iterator = getResults_object().keys();

		while (dates_iterator.hasNext()) {
			String date = dates_iterator.next();
			Integer day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id.get(date);
			DataPoint dp = new DataPoint(date,getResults_object().getString(date));
			add_data_point_to_entity(dp, day_id);
		}
	}

}
