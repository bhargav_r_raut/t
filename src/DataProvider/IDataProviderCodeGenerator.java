package DataProvider;

public interface IDataProviderCodeGenerator {
	public String generate_data_provider_code(String component_scraper_symbol,
			String index, Integer index_code_in_stockConstants);
}
