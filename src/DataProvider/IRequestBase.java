package DataProvider;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.LogObject;

public interface IRequestBase {
	
	/***
	 * implemented by the data provider when it gets new data
	 * this method adds the data in the form of a data point to the entity.
	 */
	public abstract void add_data_point_to_entity(DataPoint dp, Integer day_id) throws Exception;
	
	public abstract void prepare_datapoint_for_entity() throws Exception;
	
	public abstract void commit_info_exception_log(String exception_classification,
			LogObject lg,Exception e) ;
	
	
	
	/****
	 * after the request is done, we carry out certain actions, on the entity
	 * which are defined here.
	 * @throws Exception
	 */
	public abstract void after_request() throws Exception;
}
