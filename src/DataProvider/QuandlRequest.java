package DataProvider;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tradegenie.tests.DownloadEntityExchangeTests;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.TestMethodExecutorArgs;
import ExchangeBuilder.TestMethodsExecutor;
import Titan.ReadWriteTextToFile;
import yahoo_finance_historical_scraper.StockConstants;

public class QuandlRequest extends RequestBase {

	private String dataset_code;
	private String start_date;
	private String end_date;

	public QuandlRequest(String symbol, String fullName, LinkedHashMap<String, Object> other_data, Entity e) {
		super(symbol, fullName, other_data, e);
		LogObject lg = new LogObject(getClass(), e);

		this.dataset_code = (String) getOther_data().get(StockConstants.dataProvider_dataset_code);
		System.out.println("the dataset code is:" + dataset_code);
		this.start_date = (String) getOther_data().get(StockConstants.dataProvider_start_date);
		this.end_date = (String) getOther_data().get(StockConstants.dataProvider_end_date);

		lg.commit_info_log("quandl request object was intiialized");

	}

	/*****
	 * following exceptions are covered and logged as info level exceptions
	 * allowing the program to proceed.
	 * 
	 * 1.failure to make request 2.response code is not 200
	 * 
	 * 
	 * a third exception is possible, i.e the json is not parsable into a
	 * jsonobject that is logged as an error_exception and then thrown all the
	 * way back up to the caller to halt the program execution.
	 * 
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	@Override
	protected Boolean make_request() {
		// TODO Auto-generated method stub
		LogObject lg_1 = new LogObject(getClass(), getE());

		try {

			/****
			 * test code
			 * 
			 */
			HashMap<String, Object> boo_res = TestMethodsExecutor
					.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.throw_exception_from_make_request));
			if (boo_res.keySet().size() == 1 && boo_res.containsValue(true)) {
				throw new Exception(DownloadEntityExchangeTests.throw_exception_from_make_request);
			}
			/***
			 * test code ends.
			 * 
			 */

			String request = StockConstants.quandl_request_template
					.replace(StockConstants.start_date_placeholder, this.start_date)
					.replace(StockConstants.end_date_placeholder, this.end_date)
					.replace(StockConstants.dataset_code_placeholder, this.dataset_code);
			setRequest_url(request);
			
			LogObject lg = new LogObject(getClass(), getE());
			lg.add_to_info_packet("request string", request);
			lg.commit_info_log("making url request to quandl for new data");

			/******
			 * 
			 * TEST METHODs 1.check if the request was even made. 2.check that
			 * the start and end dates are correct.
			 * 
			 */

			TestMethodExecutorArgs args = new TestMethodExecutorArgs(
					DownloadEntityExchangeTests.WHEN_ES_EMPTY_check_request_made_and_start_and_end_dates);
			args.getArguments().put("entity", getE());
			args.getArguments().put("request_made", true);
			args.getArguments().put("request_start_date", this.start_date);
			args.getArguments().put("request_end_date", this.end_date);
			TestMethodsExecutor.exec(args);

			/***
			 * test code ends.
			 * 
			 * 
			 */
			
			
			if(StockConstants.make_dummy_request){
				//DummyQuandlData.generate_dummy_response();
				DummyQuandlData.generate_dummy_response_in_range(this.start_date, this.end_date);
				setResponse_string(DummyQuandlData.getResponse_string());
				setResponse_code(200);
				LogObject lg1 = new LogObject(getClass(), getE());
				lg1.add_to_info_packet("status:", "generating dummy response");
				lg1.commit_info_log("dummy request");
			}
			else{
				setResponse(ReadWriteTextToFile.getInstance().getTextFromUrl(request));
				setResponse_string(getResponse().body().string());
				setResponse_code(getResponse().code());
			}
			/****
			 * test code begins
			 * 
			 */

			HashMap<String, Object> boo_res2 = TestMethodsExecutor
					.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_non_200_response_code));
			if (boo_res2.keySet().size() == 1 && boo_res2.containsValue(true)) {
				// throw new
				// Exception(DownloadEntityExchangeTests.simulate_non_200_response_code);
				setResponse_code(5);
			}
			/***
			 * test code ends.
			 * 
			 * 
			 */

			lg_1.add_to_info_packet("response_string", getResponse_string().substring(0, 10));
			lg_1.add_to_info_packet("response_code", getResponse_code());

			if (getResponse_code() != 200) {
				commit_info_exception_log(StockConstants.response_code_not_200, lg_1,
						new Exception(StockConstants.response_code_not_200));

				return false;

			} else {

				try {
					HashMap<String, Object> boo_res3 = TestMethodsExecutor.exec(
							new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_invalid_json_response));
					if (boo_res3.keySet().size() == 1 && boo_res3.containsValue(true)) {
						throw new Exception(DownloadEntityExchangeTests.simulate_invalid_json_response);
					}

					setResults_object(new JSONObject(getResponse_string()));

					// SIMULATING THREE TEST CONDITIONS.
					// 1
					HashMap<String, Object> boo_res4 = TestMethodsExecutor.exec(
							new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_quandl_empty_response));
					if (boo_res4.keySet().size() == 1 && boo_res4.containsValue(true)) {
						setResults_object(new JSONObject());
					}

					// 2
					HashMap<String, Object> boo_res5 = TestMethodsExecutor.exec(
							new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_quandl_dataset_key_absent));
					if (boo_res5.keySet().size() == 1 && boo_res5.containsValue(true)) {
						getResults_object().remove("dataset");
					}

					// 3
					HashMap<String, Object> boo_res6 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
							DownloadEntityExchangeTests.simulate_quandl_data_key_absent_inside_dataset));
					if (boo_res6.keySet().size() == 1 && boo_res6.containsValue(true)) {
						getResults_object().getJSONObject("dataset").remove("data");
					}

					// here are all the individual exceptions regarding the json
					// object.
					if (getResults_object().length() < 1) {
						commit_info_exception_log("malformed response: no keys in results object", null,
								new Exception(StockConstants.quandl_empty_response));
						return false;
					} else if (!getResults_object().has("dataset")) {
						commit_info_exception_log("malformed response: no \'dataset\' key in results object", null,
								new Exception(StockConstants.quandl_dataset_key_not_found_in_response));
						return false;
					} else if (!getResults_object().getJSONObject("dataset").has("data")) {
						commit_info_exception_log("malformed response: no \'data\' key in results object[dataset]",
								null, new Exception(StockConstants.quandl_data_key_not_found_in_dataset_key));
						return false;
					} else {

						lg_1.commit_info_log("valid response");
					}
				} catch (JSONException e) {

					commit_info_exception_log(StockConstants.response_json_parse_exception, lg_1, e);
					return false;
				}

			}

			setRequest_response_successfull(true);
			return getRequest_response_successfull();

		} catch (Exception e) {

			commit_info_exception_log(StockConstants.request_failed, lg_1, e);
			return false;
		}

	}
	
	
	
	

	/****
	 * for open, high, low , volumen -> only set if not null. for datestring and
	 * close -> null check is not done.
	 * 
	 * once we come here we are assured that the response code is ok. so suppose
	 * we dont get anything in the response, we say that this is a malformed
	 * response error, with the details. we also cover this entire
	 * add_data_point to catch for any other exception, and that will get sent
	 * up the tree, so that the whole program will stop.
	 * 
	 */
	@SuppressWarnings("static-access")
	@Override
	public void prepare_datapoint_for_entity() throws Exception {
		TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.add_datapoint_to_entity_method_triggered, new HashMap<String, Object>() {
					{
						put("entity", getE());
					}
				}));

		// TODO Auto-generated method stub
		// System.out.println(getResults_object());
		LogObject lg = new LogObject(getClass(), getE());

		// this should throw an exception called entity does not exist.
		// this exception should not stop

		JSONObject dataset_object = getResults_object().getJSONObject("dataset");

		lg.add_to_info_packet("dataset_object", true);

		JSONArray data = dataset_object.getJSONArray("data");
		JSONArray column_names = dataset_object.getJSONArray("column_names");

		lg.add_to_info_packet("data_array", true);

		lg.add_to_info_packet("data_array_length", data.length());

		
		for (int i = 0; i < data.length(); i++) {
			
			JSONArray data_point = data.getJSONArray(i);
			DataPoint dp = new DataPoint();
			if(column_names.toString().equals("[\"Date\",\"Value\"]")){
				//here only set the close, there is no open or high or close.
				for (int k = 0; k < data_point.length(); k++) {
					if(k == 0){
						dp.setDateString(data_point.getString(k));
					}
					else if(k == 1){
						dp.setClose(data_point.getString(k));
					}
				}
			}
			dp.after_initialize_hook();
			Integer day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id.get(dp.getDateString());
			add_data_point_to_entity(dp, day_id);
			
		}

	}

	/***
	 * def used for test conditions to explore a jsonobject.
	 * 
	 * @param json_object
	 * @throws JSONException
	 */
	private void enumerate_object_keys_and_values(JSONObject json_object) throws JSONException {
		Iterator<String> json_keys = json_object.keys();
		while (json_keys.hasNext()) {
			String key = json_keys.next();
			// System.out.println("next key is:" + key);
			// System.out.println("next value is:" + json_object.get(key));
		}
	}

}
