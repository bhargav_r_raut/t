package DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.okhttp.Response;
import com.tradegenie.tests.DownloadEntityExchangeTests;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EventLogger;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.TestMethodExecutorArgs;
import ExchangeBuilder.TestMethodsExecutor;
import Indicators.RedisManager;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;

public class RequestBase implements IRequestBase {

	private String symbol;
	private String fullName;
	private LinkedHashMap<String, Object> other_data;
	private JSONObject results_object;
	private Entity e;
	private ArrayList<Integer> all_downloaded_day_ids;
	private Boolean request_response_successfull;

	public Boolean getRequest_response_successfull() {
		return request_response_successfull;
	}

	public void setRequest_response_successfull(Boolean request_response_successfull) {
		this.request_response_successfull = request_response_successfull;
	}

	private Response response;
	private String response_string;
	private Integer response_code;
	private String request_url;

	public String getRequest_url() {
		return request_url;
	}

	public void setRequest_url(String request_url) {
		this.request_url = request_url;
	}

	public String getResponse_string() {
		return response_string;
	}

	public void setResponse_string(String response_string) {
		this.response_string = response_string;
	}

	public Integer getResponse_code() {
		return response_code;
	}

	public void setResponse_code(Integer response_code) {
		this.response_code = response_code;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public ArrayList<Integer> getAll_downloaded_day_ids() {
		return all_downloaded_day_ids;
	}

	public void setAll_downloaded_day_ids(ArrayList<Integer> all_downloaded_day_ids) {
		this.all_downloaded_day_ids = all_downloaded_day_ids;
	}

	public Entity getE() {
		return e;
	}

	public void setResults_object(JSONObject results_object) {
		this.results_object = results_object;
	}

	public JSONObject getResults_object() {
		return results_object;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getFullName() {
		return fullName;
	}

	public LinkedHashMap<String, Object> getOther_data() {
		return other_data;
	}

	public RequestBase(String symbol, String fullName, LinkedHashMap<String, Object> other_data, Entity e) {
		this.symbol = symbol;
		this.fullName = fullName;
		this.other_data = other_data;
		this.results_object = new JSONObject();
		this.e = e;
		this.e.setForce_redownload(false);
		this.all_downloaded_day_ids = new ArrayList<Integer>();
	}

	public void run() throws Exception {

		Boolean request_and_response_is_ok = false;

		new EventLogger(EventLogger.ENTITY_DOWNLOADING, getE().getIndice(), getE().getUnique_name()).commit();

		request_and_response_is_ok = make_request();

		if (request_and_response_is_ok) {
			prepare_datapoint_for_entity();
		}

	}

	// by defaul this method assumes that the request was successfully made.
	// this method is overrideen in implementing classes
	// returning true implies that the request was successfully made and the
	// response code was
	// 200
	protected Boolean make_request() throws Exception {
		return true;
	}

	/****
	 * @logic:
	 * 
	 * 		if the datapoint is valid - if the datapoint is already there -
	 *         if it is equal to the existing datapoint - do nothing. - if it is
	 *         not equal - force redownload - if the datapoint is not already
	 *         there - add to the price map internal. if it is invalid - then
	 *         force redownload provided that the datapoint is already there in
	 *         the price map.
	 */
	@Override
	public void add_data_point_to_entity(DataPoint dp, Integer day_id) throws Exception {
		// TODO Auto-generated method stub

		getAll_downloaded_day_ids().add(day_id);

		/***
		 * test method.
		 */
		HashMap<String, Object> boo_res = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_force_redownload_due_close_zero));
		if (boo_res.values().size() == 1 && boo_res.containsValue(true)) {
			dp.invalidate_datapoint_by_setting_close_to_zero(getE().getPrice_map_internal().containsKey(day_id));
		}

		HashMap<String, Object> boo_res2 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.simulate_force_redownload_due_to_changed_existing_datapoint));
		if (boo_res2.values().size() == 1 && boo_res2.containsValue(true)) {
			dp.invalidate_datapoint_by_changing_existing_datapoint(getE().getPrice_map_internal().containsKey(day_id));
		}

		HashMap<String, Object> boo_res3 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.simulate_new_datatpoint_not_added_when_it_is_invalid));

		if (boo_res3.values().size() == 1 && boo_res3.containsValue(true)) {
			// dp.invalidate_datapoint_by_changing_existing_datapoint(getE().getPrice_map_internal().containsKey(day_id));
			System.out.println("boo res 3 is positive.");
			dp.new_datapoint_not_added_due_to_invalidity(getE());
		}

		if (!dp.datapoint_invalid_due_to_zero_attributes()) {
			if (getE().getPrice_map_internal().containsKey(day_id)) {
				if (!dp.attributes_equal_to_existing_datapoint(getE().getPrice_map_internal().get(day_id))) {

					getE().setForce_redownload(true);
				}
			} else {

				getE().add_datapoint_to_price_map_internal(dp, day_id);
			}
		} else {

			if (getE().getPrice_map_internal().containsKey(day_id)) {

				getE().setForce_redownload(true);
			}
		}
	}

	/**
	 * public static Double adjust_double_rounding(String s) {
	 * 
	 * Integer double_length = s.length();
	 * 
	 * Integer index_of_dot = s.indexOf(".");
	 * 
	 * if (index_of_dot == -1) {
	 * 
	 * index_of_dot = double_length; } s = s + "0000"; s = s.replace(".", "");
	 * 
	 * s = new StringBuilder(s).insert(index_of_dot + 4, ".").toString();
	 * 
	 * s = s.substring(0, index_of_dot + 4);
	 * 
	 * Double dq = new Double(s);
	 * 
	 * return dq; }
	 **/

	/**************************************************************************************************
	 * 
	 * METHODS FOR TEST.
	 * 
	 ************************************************************************************************** 
	 */

	// bypass making the request
	// so directly set the jsonobject from redis.
	public void build_result_object_from_redis() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			JSONObject object = new JSONObject(jedis.get(this.e.getUnique_name()));
			setResults_object(object);

		} catch (Exception e) {

		}
	}

	public void add_result_object_to_redis(JSONObject object) {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			jedis.set(this.e.getUnique_name(), object.toString());
		}
	}

	public Boolean make_test_request() {
		try {
			DummyQuandlData.generate_dummy_response();
			this.results_object = DummyQuandlData.getResponse_object();
			setRequest_response_successfull(true);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	@Override
	public void commit_info_exception_log(String exception_classification, LogObject lg, Exception e) {
		// TODO Auto-generated method stub

		setRequest_response_successfull(false);
		lg.add_to_info_packet(StockConstants.ESTypes_error_log_entity_unique_name, getE().getUnique_name());
		lg.add_to_info_packet(StockConstants.EsTypes_error_log_exception_type,
				StockConstants.EsTypes_error_log_exception_levels
						.get(StockConstants.EsTypes_error_log_exception_type_info));
		lg.add_to_info_packet(StockConstants.EsTypes_error_log_exception_classification, exception_classification);
		lg.commit_error_log("status:", e);

	}

	/****
	 * here we clear out any datapoints which have a volume of zero. we dont
	 * accept.
	 */
	@Override
	public void after_request() throws Exception {

	}

	@Override
	public void prepare_datapoint_for_entity() throws Exception {
		// TODO Auto-generated method stub

	}

}
