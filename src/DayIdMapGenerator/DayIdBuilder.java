package DayIdMapGenerator;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.type.TypeReference;
import org.javatuples.Pair;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import Indicators.RedisManager;

public class DayIdBuilder {

	public static final String gold_close_time = "10:30:0";
	public static final String forex_close_time = "15:00:0";
	public static final String nifty_close_time = "16:00:0";
	public static final String ftse_close_time = "16:30:0";
	public static final String paris_close_time = "17:30:0";
	public static final String frankfurt_close_time = "17:30:0";
	public static final String nasdaq_close_time = "16:30:0";

	public static Integer start_day_id = 0;

	public static HashMap<String, Pair<String, String>> close_times_map;

	
	/***
	 * 
	 * structure of final map. suppose you want to ask for what happens to
	 * nasdaq when adidas rises by 3 percent the previous week, and tata steel
	 * is like last week.
	 * 
	 * so you have to take a point when adidas rises by 3 percent, and then we
	 * need the next nearest market day. .
	 * 
	 * 
	 * day id exchange => target_exchange_name => [nearest_close,nearest_open]
	 * target_exchange_name => [nearest_close,nearest_open] target_exchange_name
	 * => [nearest_close,nearest_open] target_exchange_name =>
	 * [nearest_close,nearest_open]
	 * 
	 * 
	 * LinkedHashMap<String,nearest_object>
	 * 
	 * 
	 * 
	 */

	public static HashMap<String, TreeMap<Integer, Integer>> exchange_day_id_gmt_id;

	public static TreeMap<Integer, DataPoint> gmt_day_id_to_dataPoint;

	public static LinkedHashMap<String, Integer> gmt_datestring_to_day_id;

	public static HashMap<String, Integer> query_time_day_id_lookup_map;

	

	public static DateTime get_default_start_date() {

		DateTimeZone dtz = DateTimeZone.forID("GMT");
		DateTime dt = new DateTime(2010, 5, 17, 0, 0, dtz);
		return dt;
	}

	public DayIdBuilder() {

	}

	public static String left_pad(String input_string) {

		input_string = StringUtils.leftPad(input_string, 2, "0");

		return input_string;
	}

	public static String get_date(DateTime d) {

		String date = "";

		date = String.valueOf(d.getYear()) + "-"
				+ left_pad(String.valueOf(d.getMonthOfYear())) + "-"
				+ left_pad(String.valueOf(d.getDayOfMonth()));

		return date;

	}

	public static String get_key_for_final_queryable_map(String base_exch,
			String other_exch, Integer base_exch_gmt_id,
			String from_open_close, String to_open_close) {

		String key_name = null;

		key_name = base_exch_gmt_id.toString() + "_" + base_exch + "_"
				+ other_exch + "_" + from_open_close.charAt(0) + "t"
				+ to_open_close.charAt(0);

		return key_name;

	}

	public void run() {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			if (jedis
					.get(StockConstants.DayIdBuilder_gmt_datestring_to_day_id_redis_key) == null
					|| jedis.get(StockConstants.DayIdBuilder_gmt_day_id_to_data_point_redis_key) == null
					|| jedis.get(StockConstants.DayIdBuilder_query_time_day_id_lookup_map_redis_key) == null) {

				close_times_map = new LinkedHashMap<String, Pair<String, String>>();
				query_time_day_id_lookup_map = new HashMap<String, Integer>();
				gmt_datestring_to_day_id = new LinkedHashMap<String, Integer>();
				gmt_day_id_to_dataPoint = new TreeMap<Integer, DataPoint>();
				exchange_day_id_gmt_id = new LinkedHashMap<String, TreeMap<Integer, Integer>>();

				for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants
						.entrySet()) {

					if (entry.getKey().equals(StockConstants.name_for_cac)) {
						close_times_map.put(StockConstants.name_for_cac,
								new Pair<String, String>(frankfurt_close_time,
										"Europe/Berlin"));
						exchange_day_id_gmt_id.put(StockConstants.name_for_cac,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_dax)) {
						close_times_map.put(StockConstants.name_for_dax,
								new Pair<String, String>(frankfurt_close_time,
										"Europe/Berlin"));
						exchange_day_id_gmt_id.put(StockConstants.name_for_dax,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_ftse)) {
						close_times_map.put(StockConstants.name_for_ftse,
								new Pair<String, String>(ftse_close_time,
										"Europe/London"));
						exchange_day_id_gmt_id.put(
								StockConstants.name_for_ftse,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_forex)) {
						close_times_map.put(StockConstants.name_for_forex,
								new Pair<String, String>(forex_close_time,
										"Europe/Berlin"));
						exchange_day_id_gmt_id.put(
								StockConstants.name_for_forex,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_nasdaq)) {
						close_times_map.put(StockConstants.name_for_nasdaq,
								new Pair<String, String>(nasdaq_close_time,
										"America/New_York"));

						exchange_day_id_gmt_id.put(
								StockConstants.name_for_nasdaq,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_metals)) {
						close_times_map.put(StockConstants.name_for_metals,
								new Pair<String, String>(gold_close_time,
										"Europe/London"));
						exchange_day_id_gmt_id.put(
								StockConstants.name_for_metals,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_oil)) {
						close_times_map.put(StockConstants.name_for_oil,
								new Pair<String, String>(nasdaq_close_time,
										"America/New_York"));
						exchange_day_id_gmt_id.put(StockConstants.name_for_oil,
								new TreeMap<Integer, Integer>());

					} else if (entry.getKey().equals(
							StockConstants.name_for_nifty)) {
						close_times_map.put(StockConstants.name_for_nifty,
								new Pair<String, String>(nifty_close_time,
										"Asia/Kolkata"));
						exchange_day_id_gmt_id.put(
								StockConstants.name_for_nifty,
								new TreeMap<Integer, Integer>());

					}
				}

				Integer total_number_of_exchanges = close_times_map.size();

				DateTime start_cal = get_default_start_date();

				for (int i = 0; i < StockConstants.CentralSingleton_total_datapoints; i++) {

					String curr_gmt_date = get_date(start_cal);

					gmt_datestring_to_day_id.put(curr_gmt_date, i);

					/***
					 * generating all the datapoints here.
					 * 
					 */
					DataPoint dp = new DataPoint(curr_gmt_date);
					dp.generate_time_information();
					gmt_day_id_to_dataPoint.put(i, dp);

					TreeMap<Long, List<String>> epoches_map = new TreeMap<Long, List<String>>();

					for (Map.Entry<String, Pair<String, String>> entry : close_times_map
							.entrySet()) {

						String[] hour_min_sec = entry.getValue().getValue0()
								.split(":");

						DateTimeZone dtz = DateTimeZone.forID(entry.getValue()
								.getValue1());
						DateTime dt = new DateTime(start_cal.getYear(),
								start_cal.getMonthOfYear(),
								start_cal.getDayOfMonth(),
								Integer.parseInt(hour_min_sec[0]),
								Integer.parseInt(hour_min_sec[1]),
								Integer.parseInt(hour_min_sec[2]), dtz);

						if (epoches_map.isEmpty()) {
							epoches_map.put(
									dt.getMillis(),
									new ArrayList<String>(Arrays.asList(entry
											.getKey())));
						} else if (epoches_map.containsKey(dt.getMillis())) {
							epoches_map.get(dt.getMillis()).add(entry.getKey());
						} else {
							epoches_map.put(
									dt.getMillis(),
									new ArrayList<String>(Arrays.asList(entry
											.getKey())));
						}
					}

					for (Map.Entry<Long, List<String>> entry : epoches_map
							.entrySet()) {

						for (String exch : entry.getValue()) {

							// i is the day id and the gmt index.
							// exch is the exchange
							// start day id is the day id of the exchange on
							// that
							// index.
							exchange_day_id_gmt_id.get(exch).put(start_day_id,
									i);
						}

						start_day_id++;

					}

					start_cal = start_cal.plusDays(1);
				}

				/****
				 * actually working def.
				 * 
				 * 
				 */
				Set<String> names_of_exchanges = StockConstants.exchange_builder_constants
						.keySet();

				String[] open_close = { "open", "close" };

				// so the logic is that given an exchange.
				for (String exch : names_of_exchanges) {
					// now we iterate all its day ids.

					for (Map.Entry<Integer, Integer> entry : exchange_day_id_gmt_id
							.get(exch).entrySet()) {
						// now we iterate all the other exchanges, and get the
						// next
						// nearest day id to this one.
						Integer exch_day_id = entry.getKey();
						Integer gmt_day_id = entry.getValue();
						for (String other_exch : names_of_exchanges) {
							if (exch != other_exch) {
								// now we want a tailmap starting from the exch
								// day
								// id
								TreeMap<Integer, Integer> other_exch_map = exchange_day_id_gmt_id
										.get(other_exch);

								SortedMap<Integer, Integer> other_exch_tmap = other_exch_map
										.tailMap(exch_day_id);

								if (other_exch_tmap.size() > 0) {
									Integer first_day_id_of_other_exch_greater_than_or_equal_to_current_exchange_day_id = other_exch_tmap
											.firstKey();

									Integer gmt_id_at_that_key = other_exch_map
											.get(first_day_id_of_other_exch_greater_than_or_equal_to_current_exchange_day_id);

									Integer diff = exch_day_id
											- first_day_id_of_other_exch_greater_than_or_equal_to_current_exchange_day_id;

									// first open - open

									String key_name_for_final_queryable_map = null;

									key_name_for_final_queryable_map = get_key_for_final_queryable_map(
											exch, other_exch, gmt_day_id,
											"open", "open");

									query_time_day_id_lookup_map.put(
											key_name_for_final_queryable_map,
											gmt_id_at_that_key);

									// second open-close

									key_name_for_final_queryable_map = get_key_for_final_queryable_map(
											exch, other_exch, gmt_day_id,
											"open", "close");

									query_time_day_id_lookup_map.put(
											key_name_for_final_queryable_map,
											gmt_id_at_that_key);

									// third close-open

									key_name_for_final_queryable_map = get_key_for_final_queryable_map(
											exch, other_exch, gmt_day_id,
											"close", "open");

									if (diff == 0) {
										query_time_day_id_lookup_map
												.put(key_name_for_final_queryable_map,
														gmt_id_at_that_key + 1);
									} else {
										query_time_day_id_lookup_map
												.put(key_name_for_final_queryable_map,
														gmt_id_at_that_key);
									}

									// fourth close-close

									key_name_for_final_queryable_map = get_key_for_final_queryable_map(
											exch, other_exch, gmt_day_id,
											"close", "close");

									query_time_day_id_lookup_map.put(
											key_name_for_final_queryable_map,
											gmt_id_at_that_key);
								}
							}
						}
					}
				}

				CentralSingleton.getInstance().gmt_datestring_to_day_id = gmt_datestring_to_day_id;
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint = gmt_day_id_to_dataPoint;
				CentralSingleton.getInstance().query_time_day_id_lookup_map = query_time_day_id_lookup_map;

				// write to redis.
				try {
					jedis.set(
							StockConstants.DayIdBuilder_gmt_datestring_to_day_id_redis_key,
							CentralSingleton.getInstance().objmapper
									.writeValueAsString(gmt_datestring_to_day_id));
					jedis.set(
							StockConstants.DayIdBuilder_gmt_day_id_to_data_point_redis_key,
							CentralSingleton.getInstance().objmapper
									.writeValueAsString(gmt_day_id_to_dataPoint));

					jedis.set(
							StockConstants.DayIdBuilder_query_time_day_id_lookup_map_redis_key,
							CentralSingleton.getInstance().objmapper
									.writeValueAsString(query_time_day_id_lookup_map));

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				
				try {
					CentralSingleton.getInstance().gmt_datestring_to_day_id = CentralSingleton
							.getInstance().objmapper
							.readValue(
									jedis.get(StockConstants.DayIdBuilder_gmt_datestring_to_day_id_redis_key),
									new TypeReference<LinkedHashMap<String, Integer>>() {
									});

					CentralSingleton.getInstance().gmt_day_id_to_dataPoint = CentralSingleton
							.getInstance().objmapper
							.readValue(
									jedis.get(StockConstants.DayIdBuilder_gmt_day_id_to_data_point_redis_key),
									new TypeReference<TreeMap<Integer, DataPoint>>() {
									});

					CentralSingleton.getInstance().query_time_day_id_lookup_map = CentralSingleton
							.getInstance().objmapper
							.readValue(
									jedis.get(StockConstants.DayIdBuilder_query_time_day_id_lookup_map_redis_key),
									new TypeReference<HashMap<String, Integer>>() {
									});
				} catch (Exception e) {
					e.printStackTrace();
				}
				// read from redis.

			}

			// one more map which holds the day id and the
		}

		CentralSingleton.getInstance().query_time_pair_lookup_map = new ConcurrentHashMap<String, int[]>();
		//will need to populate this with existing stuff if the computer shuts down, 
		//which it will.
		//for that will need to load from remote es.
		
	}
	
	public static String zonedatetime_to_datestring(ZonedDateTime zdt) {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(zdt);
	}

}
