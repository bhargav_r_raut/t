package DayIdMapGenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;

public class DayIdExchangeObject {

	private String primary_exchange_name;
	private HashMap<String, HashMap<Integer, Integer>> target_exchange_difference;

	/***
	 * the hashmap carries the differences as follows:
	 * 
	 * 1.for open ->next nearest open. 2.for open ->next nearest close. 3.for
	 * close -> next nearest open. 4.for close -> next nearest close.
	 * 
	 * 
	 * for eg. what happens to nasdaq when yahoo opens higher by 3%;
	 * 
	 * @return
	 */
	public HashMap<String, HashMap<Integer, Integer>> getTarget_exchange_difference() {
		return target_exchange_difference;
	}

	public DayIdExchangeObject(String primary_exchange_name) {
		this.primary_exchange_name = primary_exchange_name;
		this.target_exchange_difference = new LinkedHashMap<String, HashMap<Integer, Integer>>();
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants
				.entrySet()) {
			// so we create the target exchange difference hashmap.
			target_exchange_difference.put(entry.getKey(),
					new HashMap<Integer, Integer>());

		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
			return CentralSingleton.getInstance().objmapper
					.defaultPrettyPrintingWriter().writeValueAsString(this);
		} catch (IOException e) {
			// TODO Auto-generated catch bloc
			e.printStackTrace();
			return null;
		}

	}

}
