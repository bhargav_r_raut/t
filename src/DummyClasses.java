import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Random;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.ComplexCalculationRequest;
import Indicators.RedisManager;

import Titan.TitanBaseConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import elasticSearchNative.BaseElasticHttpRequest;
import elasticSearchNative.ClientInstance;

public class DummyClasses {

	public DummyClasses() {

	}

	public static void main(String[] args) throws IOException {

		

	}

	private void build_random_calculation_request(String subindicator,
			TitanGraph g) throws ParseException {
		try {
			/***
			 * 
			 * change the desired name of the subindicator for testing here.
			 * 
			 */
			XContentBuilder query = XContentFactory.jsonBuilder().startObject()
					.startObject("query").startObject("term")
					.startObject("subindicator_name")
					.field("value", "subindicator_5_session_sma_crosses_above_subindicator_9_session_sma")
					.endObject().endObject().endObject().endObject();

			BaseElasticHttpRequest beq = new BaseElasticHttpRequest(
					"post",
					query.string(),
					new ArrayList<String>(Arrays.asList("tradegenie_titan",
							"subindicator_indicator_stock_complex", "_search")),
					new HashMap<String, Object>());
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JSONParser parser = new JSONParser();
			JSONObject object = (JSONObject) parser.parse(beq
					.getResponse_body());
			JSONObject hits = (JSONObject) object.get("hits");
			//System.out.println("total hits:" + hits.get("total"));
			JSONArray all_hits = (JSONArray) hits.get("hits");
			Integer random_index = new Random().nextInt(all_hits.size());
			//System.out.println("this is the random index.");
			//System.out.println(random_index);
			JSONObject first_hit = (JSONObject) all_hits.get(random_index);
			
			//System.out.println("here is the hit in question.");
			//System.out.println(first_hit);
			JSONObject source = (JSONObject)first_hit.get("_source");
			String stockName = (String) source.get("stockName");
			
			stockName = "LUPIN.NS";
			
			
			String stockFullName = (String) source.get("stockFullName");
			
			stockFullName = "Lupin Ltd.";
			
			String indice = (String) source.get("indice");
			String self_code = (String) source.get("self_code");
			
			self_code =
"LUPIN.NS_Lupin Ltd._NIFTY_single_ema_indicator_period_start_200_period_end_subindicator_5_session_sma_crosses_above_subindicator_9_session_sma";
			
			
			String es_object_id = (String) first_hit.get("_id");
			
			es_object_id = "AU0s_k8zmEVrAZHvdHj6";
			
			String indicator_name = (String) source.get("indicator_name");
			
			indicator_name = "single_ema_indicator";
			
			String subindicator_name = (String) source
					.get("subindicator_name");

			subindicator_name = "subindicator_5_session_sma_crosses_above_subindicator_9_session_sma";
			
			ComplexCalculationRequest creq = new ComplexCalculationRequest(
					stockName, stockFullName, indice, self_code,
					indicator_name, subindicator_name, es_object_id, g, 16508,16558);		
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void run() throws InterruptedException {

		Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		GroovyStatements gs = null;
		Gson gson = null;

		try {
			client = ClientInstance.getInstance().getCl();
			cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			g = cfg.get_graph();

			mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g, mgmt);

			// CreateCommonDayIdVertices cid = new CreateCommonDayIdVertices(g);

			gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"dummy_classes", "finished initialization corretly");

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}

		

		try {
			build_random_calculation_request("subindicator_rises_for_2_days",g);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		g.shutdown();

	}

}
