package ExchangeBuilder;

public class BulkPayLoad {

	private String entity_unique_name;
	private String doc_id;

	public String getEntity_unique_name() {
		return entity_unique_name;
	}

	public void setEntity_unique_name(String entity_unique_name) {
		this.entity_unique_name = entity_unique_name;
	}

	public String getDoc_id() {
		return doc_id;
	}

	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}

	// for jackson
	public BulkPayLoad() {

	}

	public BulkPayLoad(String entity_unique_name, String doc_id) {

		this.entity_unique_name = entity_unique_name;
		this.doc_id = doc_id;

	}
	
	

}
