package ExchangeBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.type.TypeReference;
import org.elasticsearch.action.index.IndexResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Indicators.Indicator;
import DailyOperationsLogging.BaseError;
import DayIdMapGenerator.DayIdBuilder;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import elasticSearchNative.LocalEs;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;

public class CentralSingleton {

	public static CentralSingleton instance;
	private static final String identifier_for_log = "CS";
	public static ObjectMapper objmapper;
	public static org.json.JSONObject indicators_jsonfile;
	public static Long session_start_time;
	public static TreeMap<Double, Integer> percentage_to_integer_treemap;

	/**
	 * the four main maps that are referenced all over the place.
	 * 
	 * first four are fully created and populated in day id builder.
	 * 
	 * fifth one is just initialized in day id builder, it is populated with
	 * errors as and when required.
	 * 
	 */
	public static TreeMap<Integer, DataPoint> gmt_day_id_to_dataPoint;
	public static LinkedHashMap<String, Integer> gmt_datestring_to_day_id;
	public static HashMap<String, Integer> query_time_day_id_lookup_map;
	public static ConcurrentHashMap<String, int[]> query_time_pair_lookup_map;
	/***
	 * key -> bare indicator name
	 * value -> list of indicator objects, each consisits of indicator_name + period _names
	 */
	public static ConcurrentHashMap<String,List<Indicator>> indicators_hashmap;
	/***
	 * key -> indicator_name + period_nane
	 * value -> indicator object
	 */
	public static ConcurrentHashMap<String, Indicator> stub_hashmap;
	
	public static String get_indicator_id(String indicator_name){
		return null;
	}
	
	public static HashMap<String, List<Integer>> get_indicator_periods_from_json(String indicator_name,
			JSONObject indicator_object) throws Exception {
		ArrayList<ArrayList<Integer>> periods = new ArrayList<ArrayList<Integer>>();
		HashMap<String, List<Integer>> periods_for_indicator_calculations = new HashMap<String, List<Integer>>();

		JSONArray indicator_periods = indicator_object
				.getJSONArray(StockConstants.CalculateIndicators_indicator_jsonfile_periods_key_name);

		for (int i = 0; i < indicator_periods.length(); i++) {
			// add to the entity initial stub names
			JSONArray jarr = indicator_periods.getJSONArray(i);
			ArrayList<Integer> period_list = new ArrayList<Integer>();
			for (int k = 0; k < jarr.length(); k++) {
				period_list.add(jarr.getInt(k));
			}
			if (period_list.isEmpty()) {
				// if the periods list is empty we dont populate the periods.

			} else {
				periods.add(period_list);
			}
		}

		Integer outer_counter = 0;

		for (ArrayList<Integer> list_a : periods) {

			Integer inner_counter = 0;

			for (ArrayList<Integer> list_b : periods) {

				if (outer_counter < inner_counter) {
					for (Integer list_a_integer : list_a) {
						for (Integer list_b_integer : list_b) {

							periods_for_indicator_calculations.put(
									indicator_name + "_" + "period_start_" + String.valueOf(list_a_integer) + "_"
											+ String.valueOf(list_b_integer) + "_period_end",
									Arrays.asList(new Integer[] { list_a_integer, list_b_integer }));

						}
					}
				}
				inner_counter++;
			}
			outer_counter++;
		}

		if (outer_counter.equals(1)) {

			for (Integer period : periods.get(0)) {

				periods_for_indicator_calculations.put(
						indicator_name + "_" + "period_start_" + String.valueOf(period) + "_period_end",
						Arrays.asList(new Integer[] { period }));
			}

		}

		return periods_for_indicator_calculations;
	}

	/****
	 * reads the indicator_jsonfile
	 * for each indicator found there
	 * calculates the names and periods of all the stubs that can be made from it
	 * stub_name -> indicator_name + "_" +... some period info
	 * stub_periods -> arraylist of periods.
	 * after this, it creates an indicator_object and sets these attributes on it.
	 * indicator_name : the bare indicator_name
	 * indicator_and_period_name : the stub name
	 * periods : the list of periods.
	 * id : this is assigned by autoincrementing as follows:
	 * increment one counter for indicators, and within each indicator increment for stubs
	 * then final id is assigned as: I + "indicator_counter + "stub_counter"
	 * finally -> tries to find the indicator in RemoteEs(by searching by the stub name),
	 *  if its not there, tries to save it in RemoteEs, if this fails, then throws exception.
	 * at the end, will add the indicator to the indicators_hashmap.
	 * key -> indicator_name
	 * value -> List<indicator object>
	 * 
	 * @throws IOException
	 * @throws JSONException
	 * 
	 */
	public static void build_indicator_map_and_indicator_to_remote_es() throws Exception {
		org.json.JSONObject indicators_jsonfile_read = new org.json.JSONObject(ReadWriteTextToFile.getInstance()
				.readFile(StockConstants.indicators_jsontextfile, TitanConstants.ENCODING));
		CentralSingleton.indicators_jsonfile = indicators_jsonfile_read;
		Iterator<String> indicators = indicators_jsonfile_read.keys();
		Integer indicator_counter = 0;
		
		while (indicators.hasNext()) {
			String indicator_name = indicators.next();
			
			JSONObject indicator_object = 
					indicators_jsonfile_read.getJSONObject(indicator_name);
			if (indicator_object == null) {
				throw new Exception("could not find this indicator in the jsonfile: " + indicator_name);
			} else {
				HashMap<String, List<Integer>> indicator_names_and_periods = 
						get_indicator_periods_from_json(
						indicator_name, indicator_object);
				
				Integer stub_counter = 0;
				for (Map.Entry<String, List<Integer>> entry : indicator_names_and_periods.entrySet()) {
					Indicator i = new Indicator();
					i.setIndicator_name(indicator_name);
					i.setIndicator_and_period_name(entry.getKey());
					i.setPeriods(entry.getValue());
					i.setId("I" + indicator_counter.toString() + "_" + stub_counter.toString());
					if(!i.get_by_name(i.getIndicator_and_period_name())){
						if(!i.save()){
							throw new Exception("failed to save indicator:" + i.getIndicator_and_period_name());
						}
					}
					List<Indicator> l = indicators_hashmap.getOrDefault(indicator_name, new ArrayList<Indicator>());
					l.add(i);
					indicators_hashmap.put(indicator_name, l);
					stub_hashmap.put(i.getIndicator_and_period_name(), i);
					stub_counter++;
				}
			}
			indicator_counter++;
		}
	}

	public static void set_indicators_jsonfile() {
		try {
			@SuppressWarnings("static-access")
			org.json.JSONObject indicators_jsonfile_read = new org.json.JSONObject(ReadWriteTextToFile.getInstance()
					.readFile(StockConstants.indicators_jsontextfile, TitanConstants.ENCODING));

			// deserialize from redis otherwise build it.
			try (Jedis jedis = RedisManager.getInstance().getJedis()) {
				String existing_indicators_jsonfile = jedis
						.get(StockConstants.CentralSingleton_indicators_jsonfile_redis_key_name);
				if (existing_indicators_jsonfile == null) {
					Iterator<String> indicators = indicators_jsonfile_read.keys();
					while (indicators.hasNext()) {
						String indicator_name = indicators.next();
						JSONObject indicator_object = indicators_jsonfile_read.getJSONObject(indicator_name);
						// make a jsonobject out of the not applicable to .
						// and restore that into the groovystatements
						/**
						 * JSONArray
						 * subindicators_not_applicable_to_indicator_array =
						 * indicator_object .getJSONArray("not_applicable_to");
						 * 
						 * JSONObject
						 * subindicators_not_applicable_to_indicator_jsonobject
						 * = new JSONObject();
						 * 
						 * for (int i = 0; i <
						 * subindicators_not_applicable_to_indicator_array
						 * .length(); i++) { //
						 * subindicators_not_applicable_to_indicator_jsonobject
						 * .put // (, arg1)
						 * subindicators_not_applicable_to_indicator_jsonobject
						 * .put(subindicators_not_applicable_to_indicator_array
						 * .getString(i), true); }
						 * 
						 * indicator_object
						 * .put(StockConstants.not_applicable_to_object,
						 * subindicators_not_applicable_to_indicator_jsonobject
						 * );
						 **/
						indicators_jsonfile_read.put(indicator_name, indicator_object);
					}

					// now put
					instance.indicators_jsonfile = indicators_jsonfile_read;

					jedis.set(StockConstants.CentralSingleton_indicators_jsonfile_redis_key_name,
							instance.indicators_jsonfile.toString());
				} else {

					instance.indicators_jsonfile = new JSONObject(
							jedis.get(StockConstants.CentralSingleton_indicators_jsonfile_redis_key_name));

				}
			}

		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/****
	 * we dont need this treemap anymore. this setter is no longer called.
	 */
	public static void set_percentage_to_integer_treemap() {

		percentage_to_integer_treemap = new TreeMap<Double, Integer>();
		for (Integer i = -10000; i < 10000; i++) {
			percentage_to_integer_treemap.put(i.doubleValue(), i);
		}

	}

	public static CentralSingleton getInstance() {
		return instance;
	}

	public static synchronized void initializeSingleton() throws Exception {
		if (instance == null) {
			instance = new CentralSingleton();
			instance.objmapper = new ObjectMapper();
			instance.objmapper.setSerializationInclusion(Inclusion.NON_NULL);
			instance.objmapper.configure(Feature.USE_ANNOTATIONS, true);
			instance.session_start_time = new GregorianCalendar().getTimeInMillis();
			instance.indicators_hashmap = new ConcurrentHashMap<String,List<Indicator>>();
			instance.stub_hashmap = new ConcurrentHashMap<String,Indicator>();
			//System.out.println("came to build indicator map.");
			build_indicator_map_and_indicator_to_remote_es();
			//System.out.println("finished building indicator map");
			build_exchange_map_and_day_id_map();
			// set_percentage_to_integer_treemap();
		}
	}

	public static synchronized void destroySingleton() throws Exception {
		instance = null;
	}

	/***
	 * this may or may not change, for now it synchronizes with the wikipedia
	 * component pages.
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 * 
	 */
	public static void build_exchange_map_and_day_id_map()
			throws JsonGenerationException, JsonMappingException, IOException {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			if (jedis.get(StockConstants.CentralSingleton_exchange_constants_redis_key_name) == null) {
				Integer counter = 0;
				// the index.
				StockConstants.exchange_builder_constants = new LinkedHashMap<String, String>();
				for (String s : StockConstants.wikipedia_components_pages) {

					if (counter.equals(0)) {

						StockConstants.name_for_nasdaq = s.replace("https://en.wikipedia.org/wiki/", "")
								.replace("_Index", "");
						StockConstants.exchange_builder_constants.put(StockConstants.name_for_nasdaq, s);
					} else if (counter.equals(1)) {
						StockConstants.name_for_dax = s.replace("https://en.wikipedia.org/wiki/", "").replace("_Index",
								"");
						StockConstants.exchange_builder_constants.put(StockConstants.name_for_dax, s);
					} else if (counter.equals(2)) {
						StockConstants.name_for_cac = s.replace("https://en.wikipedia.org/wiki/", "").replace("_Index",
								"");
						StockConstants.exchange_builder_constants.put(StockConstants.name_for_cac, s);
					} else if (counter.equals(3)) {
						StockConstants.name_for_ftse = s.replace("https://en.wikipedia.org/wiki/", "").replace("_Index",
								"");
						StockConstants.exchange_builder_constants.put(StockConstants.name_for_ftse, s);
					} else if (counter.equals(4)) {
						StockConstants.name_for_nifty = "NIFTY";
						StockConstants.exchange_builder_constants.put(StockConstants.name_for_nifty,
								StockConstants.url_for_nifty_component_csv);
					}

					counter++;
				}

				// add forex.
				StockConstants.name_for_forex = "forex";
				StockConstants.exchange_builder_constants.put(StockConstants.name_for_forex, "");

				// add gold
				StockConstants.name_for_metals = "metals";
				StockConstants.exchange_builder_constants.put(StockConstants.name_for_metals, "");

				// add oil
				StockConstants.name_for_oil = "oil_index";
				StockConstants.exchange_builder_constants.put(StockConstants.name_for_oil, "");
				jedis.set(StockConstants.CentralSingleton_exchange_constants_redis_key_name,
						objmapper.writeValueAsString(StockConstants.exchange_builder_constants));
			} else {
				StockConstants.exchange_builder_constants = objmapper.readValue(
						jedis.get(StockConstants.CentralSingleton_exchange_constants_redis_key_name),
						new TypeReference<LinkedHashMap<String, String>>() {
						});
				for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
					if (entry.getKey().contains("NASDAQ")) {
						StockConstants.name_for_nasdaq = entry.getKey();
					} else if (entry.getKey().contains("DAX")) {
						StockConstants.name_for_dax = entry.getKey();
					} else if (entry.getKey().contains("CAC")) {
						StockConstants.name_for_cac = entry.getKey();
					} else if (entry.getKey().contains("FTSE")) {
						StockConstants.name_for_ftse = entry.getKey();
					} else if (entry.getKey().contains("NIFTY")) {
						StockConstants.name_for_nifty = entry.getKey();
					} else if (entry.getKey().contains("forex")) {
						StockConstants.name_for_forex = entry.getKey();
					} else if (entry.getKey().contains("metals")) {
						StockConstants.name_for_metals = entry.getKey();
					} else if (entry.getKey().contains("oil")) {
						StockConstants.name_for_oil = entry.getKey();
					}
				}
			}

		}
		DayIdBuilder db = new DayIdBuilder();
		db.run();
	}

	public Integer index_new_component(final ArrayList<String> component_constituents, final String hex_id) {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			Long current_component_counter = jedis.incr("component_counter");
			jedis.hset(hex_id, "component_counter", String.valueOf(current_component_counter.intValue()));
			final Integer component_counter_of_this_element = current_component_counter.intValue();

			final Integer component_count = component_constituents.size();
			HashMap<String, Object> hm = new HashMap<String, Object>() {
				{
					put("component_numeric_id", component_counter_of_this_element);
					put("component_hex_id", hex_id);
					put("component_constituents",
							component_constituents.toArray(new String[component_constituents.size()]));
					put("component_count", component_count);
				}
			};

			// System.out.println("sent the hashmap for indexing.");
			IndexResponse res = LocalEs.getInstance().prepareIndex("tradegenie_titan", "component").setSource(hm)
					.execute().actionGet();
			// System.out.println("got resp");
			if (res.getId() == null) {
				// System.out.println("not created");
				return null;
			} else {

				return component_counter_of_this_element;
			}
		} catch (Exception e) {
			e.printStackTrace();
			BaseError be = new BaseError(e, "indexing new component.");
			return null;
		}

	}

	public Integer get_counter_for_element_or_index_it(ArrayList<String> component_constituents) {

		String hex_id = DigestUtils.sha256Hex(component_constituents.toString());

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			String existing_component_counter = jedis.hget(hex_id, "component_counter");
			// System.out.println("existing component counter" +
			// existing_component_counter);
			if (existing_component_counter == null) {
				Integer j = index_new_component(component_constituents, hex_id);
				if (j != null) {

					return j;
				} else {

					return null;
				}
			} else {
				return Integer.parseInt(existing_component_counter);
			}
		} catch (Exception e) {
			throw e;
		}

	}

}
