package ExchangeBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tradegenie.tests.TestSingleton;
import com.tradegenie.tests.TestSingletonValue;

import Indicators.Indicator;
import IdAndShardResolver.IdAndShardResolver;
import yahoo_finance_historical_scraper.StockConstants;

/***
 * this contains most of the methods from historical stock while building it, we
 * have to provide, all the necessary values give it a method for
 * 
 * @author aditya
 * 
 */

public class DataPoint implements IDataPoint {

	// jackson constructor.
	public DataPoint() {

	}

	/****
	 * for building a datapoint from the jsonarray that we get in the quandl
	 * request.
	 * 
	 * @param ja
	 * @throws JSONException
	 */
	public DataPoint(JSONArray ja) throws JSONException {
		for (int k = 0; k < ja.length(); k++) {

			if (k == 0) {
				this.dateString = ja.getString(k);

			} else if (k == 1) {
				setOpen(ja.getString(k));
			} else if (k == 2) {
				setHigh(ja.getString(k));
			} else if (k == 3) {
				setLow(ja.getString(k));
			} else if (k == 4) {
				// ignore as this is the raw close, we want the adjusted close.
			} else if (k == 5) {
				setVolume(ja.getString(k));
			} else if (k == 6) {

				setClose(ja.getString(k));
			}
		}
		after_initialize_hook();
	}

	public DataPoint(String dateString, String close_price) {
		this.dateString = dateString;
		setClose(close_price);
		after_initialize_hook();
	}

	/***
	 * only needed when the datapoint is created new. not needed when datapoint
	 * is loaded from db.
	 * 
	 */
	public void after_initialize_hook() {
		generate_time_information();
		setDp_linked_list(new LinkedList<Map<String, Object>>());
		setIndicator_values_on_datapoint(new HashMap<String, dataPointIndicator>());
	}

	public DataPoint(String dateString) {
		this.dateString = dateString;
	}

	/****
	 * nth weekday eg 23rd monday of the year.
	 * 
	 */
	public Integer nth_weekday_of_year;

	public Integer getNth_weekday_of_year() {
		return nth_weekday_of_year;
	}

	public void setNth_weekday_of_year(Integer nth_weekday_of_year) {
		this.nth_weekday_of_year = nth_weekday_of_year;
	}

	/****
	 * 
	 * 
	 * 
	 */

	public Integer last_day_of_week;

	public Integer getLast_day_of_week() {
		return last_day_of_week;
	}

	public void setLast_day_of_week(Integer last_day_of_week) {
		this.last_day_of_week = last_day_of_week;
	}

	/****
	 * is last trading day of month. 0 -> if not. 1 -> if yes
	 * 
	 */
	public Integer last_day_of_month;

	public Integer getLast_day_of_month() {
		return last_day_of_month;
	}

	public void setLast_day_of_month(Integer last_day_of_month) {
		this.last_day_of_month = last_day_of_month;
	}

	/***
	 * is last trading day of year. 0 -> if not. 1 -> if yes.
	 * 
	 */
	public Integer last_day_of_year;

	public Integer getLast_day_of_year() {
		return last_day_of_year;
	}

	public void setLast_day_of_year(Integer last_day_of_year) {
		this.last_day_of_year = last_day_of_year;
	}

	/**
	 * the yyyy-mm-dd representation of the dafy id. zone is gmt.
	 * 
	 */
	private String dateString;

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	// ////////////////////

	private String nth_day;

	public String getNth_day() {
		return nth_day;
	}

	public void setNth_day(String nth_day) {
		this.nth_day = nth_day;
	}

	// ////nth day integer.
	private Integer nth_day_integer;

	public Integer getNth_day_integer() {
		return nth_day_integer;
	}

	public void setNth_day_integer(Integer nth_day_integer) {
		this.nth_day_integer = nth_day_integer;
	}

	// ///////////////////

	private Integer year;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	// /////////////////////long versions of open and close
	// need to also protect for open or close price being zero or negative.
	// ///////////////////

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal open;

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(String open) {
		if (open != null) {
			BigDecimal bd = new BigDecimal(open).setScale(StockConstants.DataPoint_bd_scale,
					StockConstants.DataPoint_bd_rounding_mode);
			// if (bd.compareTo(new BigDecimal("0.00")) > 0) {
			this.open = bd;
			// } else {
			// throw new
			// NumberFormatException(ExceptionStringGenerator.generate_exception_string(
			// StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
			// new HashMap<String, String>() {
			// {
			// put(StockConstants.datapoint_PRICE, "open_price");
			// put(StockConstants.datapoint_VALUE, bd.toPlainString());
			// }
			// }));
			// }
		}
	}

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal close;

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(String close) {

		if (close != null) {
			BigDecimal bd = new BigDecimal(close).setScale(StockConstants.DataPoint_bd_scale,
					StockConstants.DataPoint_bd_rounding_mode);
			// if (bd.compareTo(new BigDecimal("0.00")) > 0) {
			this.close = bd;
			// } else {
			// throw new
			// NumberFormatException(ExceptionStringGenerator.generate_exception_string(
			// StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
			// new HashMap<String, String>() {
			// {
			// put(StockConstants.datapoint_PRICE, "close_price");
			// put(StockConstants.datapoint_VALUE, bd.toPlainString());
			// }
			// }));
			// }
		}

	}

	@JsonSerialize(using = BigDecimalSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private BigDecimal high;

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(String high) {
		if (high != null) {
			BigDecimal bd = new BigDecimal(high).setScale(StockConstants.DataPoint_bd_scale,
					StockConstants.DataPoint_bd_rounding_mode);
			// if (bd.compareTo(new BigDecimal("0.00")) > 0) {
			this.high = bd;
			// } else {
			// throw new
			// NumberFormatException(ExceptionStringGenerator.generate_exception_string(
			// StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
			// new HashMap<String, String>() {
			// {
			// put(StockConstants.datapoint_PRICE, "high_price");
			// put(StockConstants.datapoint_VALUE, bd.toPlainString());
			// }
			// }));
			// }
		}
	}

	@JsonSerialize(using = BigDecimalSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private BigDecimal low;

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(String low) {
		if (low != null) {
			BigDecimal bd = new BigDecimal(low).setScale(StockConstants.DataPoint_bd_scale,
					StockConstants.DataPoint_bd_rounding_mode);
			// if (bd.compareTo(new BigDecimal("0.00")) > 0) {
			this.low = bd;
			// } else {
			// throw new
			// NumberFormatException(ExceptionStringGenerator.generate_exception_string(
			// StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
			// new HashMap<String, String>() {
			// {
			// put(StockConstants.datapoint_PRICE, "low_price");
			// put(StockConstants.datapoint_VALUE, bd.toPlainString());
			// }
			// }));
			// }
		}
	}

	@JsonSerialize(using = BigDecimalSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private BigDecimal volume;

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		if (volume != null) {
			BigDecimal bd = new BigDecimal(volume).setScale(StockConstants.DataPoint_bd_scale,
					StockConstants.DataPoint_bd_rounding_mode);
			// if (bd.compareTo(new BigDecimal("0.00")) > 0) {
			this.volume = bd;
			// }
			// else{
			// throw new
			// NumberFormatException(ExceptionStringGenerator.generate_exception_string(
			// StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
			// new HashMap<String, String>() {
			// {
			// put(StockConstants.datapoint_PRICE, "volume_price");
			// put(StockConstants.datapoint_VALUE, bd.toPlainString());
			// }
			// }));
			// }
		}
	}

	/**
	 * 10th or 21st
	 * 
	 */
	private Integer day_of_month;

	public Integer getDay_of_month() {
		return day_of_month;
	}

	public void setDay_of_month(Integer day_of_month) {
		this.day_of_month = day_of_month;
	}

	/**
	 * monday, tuesday
	 */
	private String day_of_week;

	public String getDay_of_week() {
		return day_of_week;
	}

	public void setDay_of_week(String day_of_week) {
		this.day_of_week = day_of_week;
	}

	/**
	 * 0 - 6
	 * 
	 */
	private Integer day_of_week_integer;

	public Integer getDay_of_week_integer() {
		return day_of_week_integer;
	}

	public void setDay_of_week_integer(Integer day_of_week_integer) {
		this.day_of_week_integer = day_of_week_integer;
	}

	/**
	 * january,feb
	 * 
	 */
	private String month_of_year;

	public String getMonth_of_year() {
		return month_of_year;
	}

	public void setMonth_of_year(String month_of_year) {
		this.month_of_year = month_of_year;
	}

	/**
	 * 
	 * 0 - 11
	 */
	private Integer month_of_year_integer;

	public Integer getMonth_of_year_integer() {
		return month_of_year_integer;
	}

	public void setMonth_of_year_integer(Integer month_of_year_integer) {
		this.month_of_year_integer = month_of_year_integer;
	}

	/**
	 * 0-3
	 * 
	 */
	private Integer week_of_month_integer;

	public Integer getWeek_of_month_integer() {
		return week_of_month_integer;
	}

	public void setWeek_of_month_integer(Integer week_of_month_integer) {
		this.week_of_month_integer = week_of_month_integer;
	}

	/**
	 * first, second, third, fourth
	 * 
	 */
	private String week_of_month_string;

	public String getWeek_of_month_string() {
		return week_of_month_string;
	}

	public void setWeek_of_month_string(String week_of_month_string) {
		this.week_of_month_string = week_of_month_string;
	}

	/***
	 * 1 - 52
	 * 
	 */
	private Integer week_of_year;

	public Integer getWeek_of_year() {
		return week_of_year;
	}

	public void setWeek_of_year(Integer week_of_year) {
		this.week_of_year = week_of_year;
	}

	/***
	 * 0,3
	 * 
	 */
	private Integer quarter_of_year_integer;

	public Integer getQuarter_of_year_integer() {
		return quarter_of_year_integer;
	}

	public void setQuarter_of_year_integer(Integer quarter_of_year_integer) {
		this.quarter_of_year_integer = quarter_of_year_integer;
	}

	/**
	 * first, second, third , fourth
	 * 
	 */
	private String quarter_of_year;

	public String getQuarter_of_year() {
		return quarter_of_year;
	}

	public void setQuarter_of_year(String quarter_of_year) {
		this.quarter_of_year = quarter_of_year;
	}

	/**
	 * first or second
	 * 
	 */
	private String half_of_year;

	public String getHalf_of_year() {
		return half_of_year;
	}

	public void setHalf_of_year(String half_of_year) {
		this.half_of_year = half_of_year;
	}

	@JsonIgnore
	private HashMap<String, dataPointIndicator> indicator_values_on_datapoint;

	@JsonIgnore
	public HashMap<String, dataPointIndicator> getIndicator_values_on_datapoint() {
		return indicator_values_on_datapoint;
	}

	@JsonIgnore
	public void setIndicator_values_on_datapoint(HashMap<String, dataPointIndicator> indicator_values_on_datapoint) {
		this.indicator_values_on_datapoint = indicator_values_on_datapoint;
	}

	// when the thing is committed.
	// at that time, before update_newly_added_datapoints_to_es.
	// this has to be set.
	private LinkedList<Map<String, Object>> dp_linked_list;

	public LinkedList<Map<String, Object>> getDp_linked_list() {
		return dp_linked_list;
	}

	public void setDp_linked_list(LinkedList<Map<String, Object>> dp_linked_list) {
		this.dp_linked_list = dp_linked_list;

	}

	// or we could just

	/*******
	 * 
	 * gives us whether a given day i.e monday, tuesday, wednesday,
	 * thursday,friday,saturday,sunday is the first,second,third,fourth or fifth
	 * or sixth of the month.
	 * 
	 * it is possible for there to be six mondays in a month apparently.
	 * 
	 * it is necessary to also know if it is_last?
	 * 
	 */
	public Integer calculate_nth_day(DateTime dt) {

		Integer nth_day = 0;
		Integer current_month_of_year = dt.getMonthOfYear();
		Integer new_month_of_year = null;

		for (int i = 0; i < 5; i++) {

			dt = dt.minusWeeks(1);

			new_month_of_year = dt.getMonthOfYear();

			if (new_month_of_year != current_month_of_year) {
				nth_day = i + 1;
				break;
			}
		}

		return nth_day;
	}

	/***
	 * is this the last monday of the month?
	 * 
	 * will add 7 days to the current day, and see if the month changes. in that
	 * case it had to be the last monday/ or whatever day of the month.
	 * 
	 * @param dt
	 * @param nth_day
	 * @return
	 */
	public Boolean is_last_nday_of_month(DateTime dt, Integer nth_day) {

		// basically add one week to this , and see if the month changes.
		DateTime one_week_later = dt.plusDays(7);
		if (one_week_later.getMonthOfYear() != dt.getMonthOfYear()) {
			return true;
		}

		return false;
	}

	/***
	 * is this the first monday of the month? just checks if nth day is 1, and
	 * returns true if yes.
	 * 
	 * @return
	 */
	private Boolean is_first_nday_of_month(DateTime dt, Integer nth_day) {
		if (nth_day == 1) {
			return true;
		}
		return false;
	}

	public Integer calculate_week_of_month(DateTime dt) {

		Integer week_of_month = 0;

		Integer month_of_year = dt.getMonthOfYear();

		Integer year = dt.getYear();

		Integer day_of_month = dt.getDayOfMonth();

		Integer day = 1;

		DateTimeZone dz = dt.getZone();

		DateTime first_day_of_month = new DateTime(year, month_of_year, day, 0, 0, dz);

		// each time check if the current day of month is less than the current
		// new day.
		// if yes, then that is the week of the month.
		Integer new_day_of_month = null;
		Integer new_day_month = null;
		Integer new_day_year = null;
		for (int i = 0; i < 5; i++) {
			first_day_of_month = first_day_of_month.plusWeeks(1);
			new_day_of_month = first_day_of_month.getDayOfMonth();
			new_day_month = first_day_of_month.getMonthOfYear();
			new_day_year = first_day_of_month.getYear();
			if ((day_of_month <= new_day_of_month) || (month_of_year < new_day_month) || (year < new_day_year)) {
				week_of_month = i + 1;
				break;
			}
		}

		return week_of_month;
	}

	@Override
	public void generate_time_information() {
		// TODO Auto-generated method stub

		String[] ymd = getDateString().split("-");
		DateTime dt = new DateTime(Integer.parseInt(ymd[0]), Integer.parseInt(ymd[1]), Integer.parseInt(ymd[2]), 0, 0,
				DateTimeZone.forID("GMT"));

		// setting month of year, half of year and quarter of year.
		// the datetime months starts with january as 1.
		Integer month_of_year = dt.getMonthOfYear();
		setMonth_of_year_integer(month_of_year);

		String month_of_year_lit = IdAndShardResolver.months_map.get(month_of_year);
		setMonth_of_year(month_of_year_lit);

		if (month_of_year <= 6) {
			setHalf_of_year("first");
			if (month_of_year <= 3) {
				setQuarter_of_year("first");
				setQuarter_of_year_integer(1);
			} else {
				setQuarter_of_year("second");
				setQuarter_of_year_integer(2);
			}
		} else {
			setHalf_of_year("second");
			if (month_of_year <= 9) {
				setQuarter_of_year("third");
				setQuarter_of_year_integer(3);
			} else {
				setQuarter_of_year("fourth");
				setQuarter_of_year_integer(4);
			}
		}

		Integer day_of_week = dt.getDayOfWeek();
		setDay_of_week_integer(day_of_week);

		String day_of_week_lit = IdAndShardResolver.days_map.get(day_of_week);
		setDay_of_week(day_of_week_lit);

		setDay_of_month(dt.getDayOfMonth());

		setNth_day((String) IdAndShardResolver.nth_occurrence.get(calculate_nth_day(dt))[0]);

		setNth_day_integer(calculate_nth_day(dt));

		setWeek_of_month_integer(calculate_week_of_month(dt));
		setWeek_of_month_string((String) IdAndShardResolver.nth_occurrence.get(getWeek_of_month_integer())[0]);

		setWeek_of_year(dt.getWeekOfWeekyear());

		setYear(dt.getYear());

		// go to the first month of the current year => see what day it is.
		// then go to the end of the month

	}

	/****
	 * multiplication by 1000 because:
	 * 
	 * normally would be:
	 * 
	 * (e -s)/(s) * 100 to get percentage change.
	 * 
	 * but in our change maps, we have 0.1 representated as 1.0 so we multiply
	 * by 10 more, so it becomes multiply by 1000
	 * 
	 * after multiplying we set a scale of zero and round down.
	 * 
	 * this is because our precision is upto 0.1 percent. so suppose you get a
	 * value of 7.2 you only want the 7 so, you set a scale of zero and round
	 * its ass down.
	 * 
	 * 
	 */
	@Override
	public PercentageIntegerCorrelate calculate_differences_from_end_day_datapoint(DataPoint end_day,
			Boolean calculate_for_high_and_low) {

		Integer open_correlation = null;

		Integer close_correlation = null;

		Integer high_correlation = null;

		Integer low_correlation = null;

		if (end_day.getOpen() != null) {

			open_correlation = get_correlation_for_indicator(end_day.getOpen(), this.getOpen());

		}

		if (calculate_for_high_and_low) {

			high_correlation = get_correlation_for_indicator(end_day.getHigh(), this.getHigh());
			low_correlation = get_correlation_for_indicator(end_day.getLow(), this.getLow());
		}

		close_correlation = get_correlation_for_indicator(end_day.getClose(), this.getClose());

		return new PercentageIntegerCorrelate(open_correlation, close_correlation, high_correlation, low_correlation);
	}

	/****
	 * basic steps are as follows:\n 1.first we calculate the percentage change
	 * upto 3 decimal places(that is the scale of three defined in the first
	 * step below) , rounding up wherever needed(rounding mode is up.) \n 2.then
	 * we multiply it by 1000 (because we want to get it as an integer.)
	 * 3.finally we just take the integer part of the result.
	 * 
	 * ----
	 * 
	 */
	@Override
	public Integer get_correlation_for_indicator(BigDecimal end_day_indicator, BigDecimal start_day_indicator) {
		/**
		 * BigDecimal percentage_change; Integer percentage_correlation;
		 * 
		 * 
		 * percentage_change =
		 * (end_day_indicator.subtract(start_day_indicator).divide(
		 * start_day_indicator, 3, StockConstants.DataPoint_bd_rounding_mode));
		 * 
		 * percentage_change = percentage_change .multiply(new
		 * BigDecimal(StockConstants.
		 * DataPoint_multiplicand_for_deriving_percentage_correlate));
		 * 
		 * percentage_correlation = percentage_change.intValueExact();
		 **/
		return Indicator.get_percentage_change(start_day_indicator, end_day_indicator);
	}

	/*****
	 * 
	 * ---------THIS FUNCTION IS NO LONGER USED, we directly store the integer
	 * correlation that we get in the function
	 * {@link #get_correlation_for_indicator(BigDecimal, BigDecimal)}
	 * 
	 */
	@Override
	public Integer get_nearest_correlation_from_percentage_map(Double percentage_change) {

		percentage_change = percentage_change * 10;

		Integer correlation = null;

		SortedMap<Double, Integer> sm = CentralSingleton.getInstance().percentage_to_integer_treemap
				.subMap(percentage_change - 1, percentage_change + 1);

		if (sm.size() == 3) {
			correlation = sm.get(percentage_change);
			return correlation;
		} else {

			int[] diffs = new int[2];
			Integer counter = 0;
			for (Map.Entry<Double, Integer> entry : sm.entrySet()) {

				if (Math.abs(entry.getKey() - percentage_change) < 0.400) {

					correlation = entry.getValue();

				}
				diffs[counter] = entry.getValue();
				counter++;
			}

			if (correlation == null) {

				correlation = diffs[1];

			}

			return correlation;

		}

	}

	@Override
	public String print_open_high_low_close_volume_datestring() {
		JSONObject object = new JSONObject();
		try {
			object.put("datestring", getDateString());
			object.put("open", getOpen());
			object.put("high", getHigh());
			object.put("low", getLow());
			object.put("close", getClose());
			object.put("volume", getVolume());
			int spacestoindent = 11;
			return object.toString(spacestoindent);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public DateTime dateString_to_dateTime() {
		String[] yyyy_mm_dd = this.dateString.split("-");
		DateTime dt = new DateTime(Integer.parseInt(yyyy_mm_dd[0]), Integer.parseInt(yyyy_mm_dd[1]),
				Integer.parseInt(yyyy_mm_dd[2]), 0, 0, DateTimeZone.forID("UTC"));

		return dt;
	}

	@Override
	public ZonedDateTime dateString_to_zdt() {
		String[] yyyy_mm_dd = this.dateString.split("-");
		// DateTime dt = new DateTime(Integer.parseInt(yyyy_mm_dd[0]),
		// Integer.parseInt(yyyy_mm_dd[1]),
		// Integer.parseInt(yyyy_mm_dd[2]), 0, 0, DateTimeZone.forID("UTC"));
		LocalDateTime ldt = LocalDateTime.of(Integer.parseInt(yyyy_mm_dd[0]), Integer.parseInt(yyyy_mm_dd[1]),
				Integer.parseInt(yyyy_mm_dd[2]), 0, 0);
		ZonedDateTime utc = ldt.atZone(ZoneId.of("Etc/GMT0"));
		return utc;
	}

	/****
	 * the entity to which this datapoint belongs
	 * 
	 * last_of_time_unit : a string which tells, that this datapoint is the last
	 * of year, month, day or week. it can be "year","month","day","week"
	 * 
	 * @return : a datapoint, which will be the first trading day of whatever
	 *         was mentioned above.
	 */
	@Override
	public DataPoint get_first_of(Entity e, String last_of_time_unit, Integer current_day_id) {

		String datestring = "";
		Integer day_id_of_first_day_of_time_unit = null;
		Integer search_price_map_from_day_id = null;

		// if we are on the last day of the year, then
		// make the datestring as the first of jan.
		System.out.println("last time of unit is: " + last_of_time_unit);
		
		if (last_of_time_unit.equals("year")) {
			datestring = getYear().toString() + "-" + "01-01";
			
			search_price_map_from_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id
					.get(datestring);
			// if we are on the last day of the month,
			// make datestring as first day of month.
		} else if (last_of_time_unit.equals("month")) {
			// datestring = getYear().toString() + "-" + getmon
			String month_padded = StringUtils.leftPad(getMonth_of_year_integer().toString(), 2, "0");
			datestring = getYear().toString() + "-" + month_padded + "-" + "01";
			// we need the first of the
			// if quarter then get the first day of the respective month.
			
			search_price_map_from_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id.get(datestring);


			
		} else if (last_of_time_unit.equals("quarter")) {
			if (getQuarter_of_year_integer() == 1) {
				// set the datestring as first of jan
				datestring = getYear().toString() + "-01-01";
			} else if (getQuarter_of_year_integer() == 2) {
				// set the datestring as first of april
				datestring = getYear().toString() + "-04-01";
			} else if (getQuarter_of_year_integer() == 3) {
				datestring = getYear().toString() + "-07-01";
				// set the datestring as first of july
			} else {
				// set as first of october.
				datestring = getYear().toString() + "-10-01";

			}
			search_price_map_from_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id.get(datestring);
		} else if (last_of_time_unit.equals("week")) {
			// if week then assume week day id is 6
			// to go to monday minus 5 from it.
			if(current_day_id == 4){
				System.out.println("got last time unit as week.");
			}
			
			search_price_map_from_day_id = current_day_id - (getDay_of_week_integer() - 1);
			if(current_day_id == 4){
			System.out.println("search price map becomes:");
			System.out.println(search_price_map_from_day_id);
			
			}
		}

		// if the price map has a datapoint for the day id that we have got
		// above
		// then return that.
		//System.out.println("e is null" + (e == null));
		//System.out.println("e.get price map internal");
		//System.out.println(e.getPrice_map_internal()== null);
		//System.out.println(search_price_map_from_day_id);
		
		if(search_price_map_from_day_id == null){
			return null;
		}
		
		if (e.getPrice_map_internal().containsKey(search_price_map_from_day_id)) {
			return e.getPrice_map_internal().get(search_price_map_from_day_id);
		}

		// otherwise consider the next available datapoint
		else {
			if (e.getPrice_map_internal().higherKey(search_price_map_from_day_id) == null) {
				// in this case there is no higher key, means that
				// the last day and first day will be the same.
				// for eg: suppose you had a last day as 12th jan.
				// from that you calculated a first day as 12th jan.
				// this is only possible if this def is called without checking
				// whether we are on the last day to begin with.
				// in any case if we dont get any higher key,
				// we just return null
				return null;
			} else {
				// now if there is a higher key, and it is the same as the
				// current
				// days id
				// then it means
				// you got last day as 12th jan.
				// and you got first day as 3rd jan.
				// but from 3rd jan till 12th jan, there is no other datapoint.
				// so in this case we cannot process for first day/last day
				// so we return null.
				Integer higher_key = e.getPrice_map_internal().higherKey(search_price_map_from_day_id);
				if (higher_key == current_day_id) {
					return null;
				} else {
					return e.getPrice_map_internal().get(higher_key);
				}
			}
		}

	}

	@Override
	public void after_build_datapoint_hook() throws Exception {
		// TODO Auto-generated method stub
		LogObject lg_i = new LogObject(getClass(), this);

		this.indicator_values_on_datapoint = new HashMap<String, dataPointIndicator>();
		for (Map<String, Object> obj : getDp_linked_list()) {
			byte[] json;

			json = CentralSingleton.getInstance().objmapper
					.writeValueAsBytes(obj.get(StockConstants.datapointIndicator_dataPointIndicator));

			dataPointIndicator dp = CentralSingleton.getInstance().objmapper.readValue(json, dataPointIndicator.class);
			getIndicator_values_on_datapoint().put(dp.getIndicator_name(), dp);
		}
		lg_i.add_to_info_packet("built total indicators:", getIndicator_values_on_datapoint().size());
		// lg_i.commit_debug_log("init: after build dp hook");

		getDp_linked_list().clear();

		LogObject lg_e = new LogObject(getClass(), this);
		// lg_e.commit_debug_log("completed:after build dp hook");
	}

	@Override
	public void before_commit_to_es_hook() throws Exception {
		// TODO Auto-generated method stub
		// take everything from the indicator_values_on_datapoint map and put it
		// into the dp_linked_list
		// then clear the indicator_values_on_datapoint_map.
		LogObject lg_i = new LogObject(getClass(), this);

		if (getIndicator_values_on_datapoint() == null) {
			this.dp_linked_list = new LinkedList<Map<String, Object>>();
			lg_i.add_to_info_packet("dp_linked_list", "built new");

		} else {
			for (final Map.Entry<String, dataPointIndicator> entry : getIndicator_values_on_datapoint().entrySet()) {
				getDp_linked_list().add(new HashMap<String, Object>() {
					{
						put(StockConstants.datapointIndicator_indicator_name, entry.getKey());
						put(StockConstants.datapointIndicator_dataPointIndicator, entry.getValue());
					}
				});
			}
			lg_i.add_to_info_packet("dp_linked_list", "built with:" + getDp_linked_list().size() + " elements");
			// getIndicator_values_on_datapoint().clear();
		}

		// lg_i.commit_debug_log("init: before_commit_datapoint_to_es");

		// LogObject lg_2 = new LogObject(getClass(), this);
		// lg_2.commit_debug_log("completed:before commit datapoint to es");
	}

	@Override
	public String print_last_week_month_and_year_values()
			throws JsonGenerationException, JsonMappingException, IOException {
		HashMap<String, String> report = new LinkedHashMap<String, String>();
		report.put("report for date", getDateString());
		report.put("last day of week", this.last_day_of_week.toString());
		report.put("last_day_of_month", this.last_day_of_month.toString());
		report.put("last day of year", this.last_day_of_year.toString());
		return CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter().writeValueAsString(report);

	}

	/***
	 * checks each attribute of both objects, not to be null as well as for
	 * equality.
	 * 
	 * @param obj
	 * @return
	 */
	public boolean equals_and_not_null(Object obj) {
		// TODO Auto-generated method stub
		DataPoint other_dp = (DataPoint) obj;
		// check each field.
		if (!(other_dp.getClose() == null && getClose() == null) && !other_dp.getClose().equals(getClose())
				|| other_dp.getClose() == null || getClose() == null) {
			System.out.println("close was diff");
			return false;
		} else if (!(other_dp.getOpen() == null && getOpen() == null)
				&& (!other_dp.getOpen().equals(getOpen()) || other_dp.getOpen() == null || getOpen() == null)) {
			return false;
		} else if (!(other_dp.getHigh() == null && getHigh() == null)
				&& (!other_dp.getHigh().equals(getHigh()) || other_dp.getHigh() == null || getHigh() == null)) {
			System.out.println("high was diff");
			return false;
		} else if (!(other_dp.getLow() == null && getLow() == null)
				&& (!other_dp.getLow().equals(getLow()) || other_dp.getLow() == null || getLow() == null)) {
			System.out.println("low was diff");
			return false;
		} else if (!(other_dp.getVolume() == null && getVolume() == null)
				&& (!other_dp.getVolume().equals(getVolume()) || other_dp.getVolume() == null || getVolume() == null)) {
			System.out.println("volume was diff");
			return false;
		} else if (!other_dp.getDateString().equals(getDateString()) || other_dp.getDateString() == null
				|| getDateString() == null) {
			System.out.println("datestring was diff");
			return false;
		} else if (!other_dp.getNth_day().equals(getNth_day()) || other_dp.getNth_day() == null
				|| getNth_day() == null) {
			System.out.println("nth day was diff");
			return false;
		} else if (!other_dp.getNth_day_integer().equals(getNth_day_integer()) || other_dp.getNth_day_integer() == null
				|| getNth_day_integer() == null) {
			System.out.println("nth day integer was diff");
			return false;
		} else if (!other_dp.getYear().equals(getYear()) || other_dp.getYear() == null || getYear() == null) {
			System.out.println("year was diff");
			return false;
		} else if (!other_dp.getDay_of_month().equals(getDay_of_month()) || other_dp.getDay_of_month() == null
				|| getDay_of_month() == null) {
			System.out.println("day of month was diff");
			return false;
		} else if (!other_dp.getDay_of_week().equals(getDay_of_week()) || other_dp.getDay_of_week() == null
				|| getDay_of_week() == null) {
			System.out.println("day of week was diff");
			return false;
		} else if (!other_dp.getDay_of_week_integer().equals(getDay_of_week_integer())
				|| other_dp.getDay_of_week_integer() == null || getDay_of_week_integer() == null) {
			System.out.println("day of week integer was diff");
			return false;
		} else if (!other_dp.getMonth_of_year().equals(getMonth_of_year()) || other_dp.getMonth_of_year() == null
				|| getMonth_of_year() == null) {
			System.out.println("month of year was diff");
			return false;
		} else if (!other_dp.getMonth_of_year_integer().equals(getMonth_of_year_integer())
				|| other_dp.getMonth_of_year_integer() == null || getMonth_of_year_integer() == null) {
			System.out.println("month of year integer was diff");
			return false;
		} else if (!other_dp.getWeek_of_month_integer().equals(getWeek_of_month_integer())
				|| other_dp.getWeek_of_month_integer() == null || getWeek_of_month_integer() == null) {
			System.out.println("week of month integer was diff");
			return false;
		} else if (!other_dp.getWeek_of_month_string().equals(getWeek_of_month_string())
				|| other_dp.getWeek_of_month_string() == null || getWeek_of_month_string() == null) {
			System.out.println("week of month string was diff");
			return false;
		} else if (!other_dp.getWeek_of_year().equals(getWeek_of_year()) || getWeek_of_year() == null
				|| other_dp.getWeek_of_year() == null) {
			System.out.println("week of year was diff");
			return false;
		} else if (!other_dp.getQuarter_of_year().equals(getQuarter_of_year()) || other_dp.getQuarter_of_year() == null
				|| getQuarter_of_year() == null) {
			System.out.println("quarter of year was diff");
			return false;
		} else if (!other_dp.getQuarter_of_year_integer().equals(getQuarter_of_year_integer())
				|| other_dp.getQuarter_of_year_integer() == null || getQuarter_of_year_integer() == null) {
			System.out.println("quarter of year integer was diff");
			return false;
		} else if (!other_dp.getHalf_of_year().equals(getHalf_of_year()) || other_dp.getHalf_of_year() == null
				|| getHalf_of_year() == null) {
			System.out.println("half of year was diff");
			return false;
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {

		// TODO Auto-generated method stub
		DataPoint other_dp = (DataPoint) obj;
		// check each field.
		if (!other_dp.getClose().equals(getClose())) {
			return false;
		} else if (!other_dp.getOpen().equals(getOpen())) {
			return false;
		} else if (!other_dp.getHigh().equals(getHigh())) {
			return false;
		} else if (!other_dp.getLow().equals(getLow())) {
			return false;
		} else if (!other_dp.getVolume().equals(getVolume())) {
			return false;
		} else if (!other_dp.getDateString().equals(getDateString())) {
			return false;
		} else if (!other_dp.getNth_day().equals(getNth_day())) {
			return false;
		} else if (!other_dp.getNth_day_integer().equals(getNth_day_integer())) {
			return false;
		} else if (!other_dp.getYear().equals(getYear())) {
			return false;
		} else if (!other_dp.getDay_of_month().equals(getDay_of_month())) {
			return false;
		} else if (!other_dp.getDay_of_week().equals(getDay_of_week())) {
			return false;
		} else if (!other_dp.getDay_of_week_integer().equals(getDay_of_week_integer())) {
			return false;
		} else if (!other_dp.getMonth_of_year().equals(getMonth_of_year())) {
			return false;
		} else if (!other_dp.getMonth_of_year_integer().equals(getMonth_of_year_integer())) {
			return false;
		} else if (!other_dp.getWeek_of_month_integer().equals(getWeek_of_month_integer())) {
			return false;
		} else if (!other_dp.getWeek_of_month_string().equals(getWeek_of_month_string())) {
			return false;
		} else if (!other_dp.getWeek_of_year().equals(getWeek_of_year())) {
			return false;
		} else if (!other_dp.getQuarter_of_year().equals(getQuarter_of_year())) {
			return false;
		} else if (!other_dp.getQuarter_of_year_integer().equals(getQuarter_of_year_integer())) {
			return false;
		} else if (!other_dp.getHalf_of_year().equals(getHalf_of_year())) {
			return false;
		} else {
			// now we have to assess the hashmap of indicator values.

		}
		return true;
	}

	@Override
	public Boolean value_is_less_than_or_equal_to_zero(BigDecimal bd) {
		if (bd != null) {
			return (bd.compareTo(StockConstants.bd_0) < 1);
		}
		return false;
	}

	@Override
	public Boolean volume_is_less_than_or_equal_to_zero() {
		return value_is_less_than_or_equal_to_zero(getVolume());
	}

	@Override
	public Boolean close_is_less_than_or_equal_to_zero() {
		return value_is_less_than_or_equal_to_zero(getClose());
	}

	@Override
	public Boolean open_is_less_than_or_equal_to_zero() {
		return value_is_less_than_or_equal_to_zero(getOpen());
	}

	@Override
	public Boolean high_is_less_than_or_equal_to_zero() {
		return value_is_less_than_or_equal_to_zero(getHigh());
	}

	@Override
	public Boolean low_is_less_than_or_equal_to_zero() {
		return value_is_less_than_or_equal_to_zero(getLow());
	}

	/*****
	 * returns true if any of its attributes are less than or equal to zero.
	 * returns true if there are no attributes on it at all.
	 */
	@Override
	public Boolean datapoint_invalid_due_to_zero_attributes() {
		return (open_is_less_than_or_equal_to_zero() || high_is_less_than_or_equal_to_zero()
				|| low_is_less_than_or_equal_to_zero() || close_is_less_than_or_equal_to_zero()
				|| all_attributes_are_null());
	}

	/***
	 * returns true if all the attributes are null.
	 * 
	 * @return
	 */
	@Override
	public Boolean all_attributes_are_null() {
		return (getOpen() == null && getClose() == null && getHigh() == null && getLow() == null
				&& getVolume() == null);
	}

	/****
	 * we pass in the datapoint from the price map internal. return true if all
	 * the attributes are equal.
	 * 
	 * @param dp
	 * @return
	 */
	@Override
	public Boolean attributes_equal_to_existing_datapoint(DataPoint dp) {
		Boolean equals = true;
		if(getOpen() != null || dp.getOpen() != null){
			equals = dp.getOpen().equals(getOpen());
			if(equals == false){
				return equals;
			}
		}
		if(getClose() != null || dp.getClose() != null){
			equals = dp.getClose().equals(getClose());
			if(equals == false){
				return equals;
			}
		}
		if(getHigh() != null || dp.getHigh() != null){
			equals = dp.getHigh().equals(getHigh());
			if(equals == false){
				return equals;
			}
		}
		if(getLow() != null || dp.getLow() != null){
			equals = dp.getLow().equals(getLow());
			if(equals == false){
				return equals;
			}
		}
		if(getVolume() != null || dp.getVolume() != null){
			equals = dp.getVolume().equals(getVolume());
			if(equals == false){
				return equals;
			}
		}
		return equals;
	}

	@Override
	public String toString() {
		HashMap<String, String> json = new HashMap<String, String>();
		try {
			if (getOpen() != null) {
				json.put("open", getOpen().toString());
			}

			if (getClose() != null) {
				json.put("close", getClose().toString());
			}

			if (getHigh() != null) {
				json.put("high", getHigh().toString());
			}

			if (getLow() != null) {
				json.put("low", getLow().toString());
			}

			if (getVolume() != null) {
				json.put("volume", getVolume().toString());
			}

			if (getDateString() != null) {
				json.put("datestring", getDateString());
			}
			// System.out.println(CentralSingleton == null);
			return CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter().writeValueAsString(json);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
			return super.toString();
		}

	}

	@Override
	public void invalidate_datapoint_by_setting_close_to_zero(Boolean day_id_already_exists) {
		// TODO Auto-generated method stub
		if (day_id_already_exists) {
			setClose("0.00");
		}
	}

	@Override
	public void invalidate_datapoint_by_changing_existing_datapoint(Boolean day_id_already_exists) {
		// System.out.println("day id already exists wsa" +
		// day_id_already_exists.toString());
		if (day_id_already_exists) {

			// System.out.println("since it was true, we resetting close");
			setClose("100000000000.00");
		}
	}

	@Override
	public Boolean new_datapoint_not_added_due_to_invalidity(Entity e) {
		// TODO Auto-generated method stub
		// just knock of datapoints randomly.
		System.out.println("came to new datapoint not added.");
		Random r = new Random();
		TestSingletonValue tv1 = new TestSingletonValue(e);
		tv1 = TestSingleton.getTsingle().get_singleton_value(tv1);
		tv1.getTotal_datapoints_encountered().incrementAndGet();
		TestSingleton.getTsingle().put_singleton_value(tv1);
		System.out.println("total datapoints added is:");
		System.out.println(
				TestSingleton.getTsingle().get_singleton_value(tv1).getTotal_datapoints_encountered().intValue());

		if (r.nextInt(2) == 0) {
			setClose("0.00");
			TestSingletonValue tv = new TestSingletonValue(e);
			tv = TestSingleton.getTsingle().get_singleton_value(tv);
			tv.getdatapoints_invalidated_randomly().incrementAndGet();
			tv = TestSingleton.getTsingle().put_singleton_value(tv);
			return true;
		}
		return false;

	}

	public DateTime datestring_to_datetime() {
		String[] ymd = getDateString().split("-");
		DateTime dt = new DateTime(Integer.parseInt(ymd[0]), Integer.parseInt(ymd[1]), Integer.parseInt(ymd[2]), 0, 0,
				DateTimeZone.forID("GMT"));
		return dt;
	}

	public Boolean new_year(DataPoint prev_dp) {
		if (!prev_dp.getYear().equals(getYear())) {
			return true;
		}
		return false;
	}

	public Boolean new_month(DataPoint prev_dp) {
		if (!prev_dp.getMonth_of_year_integer().equals(getMonth_of_year_integer())) {
			return true;
		}
		return false;
	}

	public Boolean new_quarter(DataPoint prev_dp) {
		if (!prev_dp.getQuarter_of_year_integer().equals(getQuarter_of_year_integer())) {
			return true;
		}
		return false;
	}

	public Boolean new_week(DataPoint prev_dp) {
		if (!prev_dp.getWeek_of_year().equals(getWeek_of_year())) {
			return true;
		}
		return false;
	}

}
