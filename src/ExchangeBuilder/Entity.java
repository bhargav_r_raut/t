package ExchangeBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSetter;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.script.ScriptService;
import org.elasticsearch.script.ScriptService.ScriptType;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradegenie.tests.ChunkTest;
import com.tradegenie.tests.DownloadEntityExchangeTests;
import com.tradegenie.tests.StopLossTests;

import ComplexProcessor.Complex;
import DataProvider.ForexRequest;
import DataProvider.IDataProviderCodeGenerator;
import DataProvider.QuandlRequest;
import DataProvider.RequestBase;
import IdAndShardResolver.IdAndShardResolver;
import Indicators.RedisManager;
import elasticSearchNative.LocalEs;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;

/****
 * how the clearing of the lists works.
 * 
 * there are two points here:
 * 
 * 1.when the after_build_enttiy from es hook is called and the
 * price_map_internal is populated from the es linked list, then the es linked
 * list is cleared to save memory(beause it is no longer need.) 2.when the
 * generate_cramped_treemap is called (it is called before the stop loss arrays
 * are calculated) then it has to be first populated from the price_map
 * internal. when an entry is put from the price_map internal into the cramped
 * map, then that entries datapoint in the price map internal is made null.
 * 3.there is only one reference which is made again to the datapoint , directly
 * on the price map internal. and that is during the
 * before_commit_entity_to_es_hook, and there, instead of getting the datapoint
 * from the price map internal the call is made to the cramped_map on the same
 * key.
 * 
 * 4.all other places only ever use the keys of the price_map_internal so it is
 * never a problem to have null as the datapoints.
 * 
 * @author aditya
 * 
 */
public class Entity implements IDataProviderCodeGenerator, IEntity {

	private String indice;

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	/**
	 * the integer code for this index in stockconstants file
	 * 
	 */
	private Integer indice_stockConstants_code;

	public Integer getIndice_stockConstants_code() {
		return indice_stockConstants_code;
	}

	public void setIndice_stockConstants_code(Integer indice_stockConstants_code) {
		this.indice_stockConstants_code = indice_stockConstants_code;
	}

	// only close -> c1
	// close + open,high,low -> c2
	// close + open,high,low,volume-> c3
	// close + volume -> c4
	private String ohlcv_type;

	public String getOhlcv_type() {
		return ohlcv_type;
	}

	public void setOhlcv_type(String ohlcv_type) {
		this.ohlcv_type = ohlcv_type;
	}

	private String unique_name;

	public String getUnique_name() {
		return unique_name;
	}

	public void setUnique_name() {
		this.unique_name = generate_unique_name();
	}

	private String fullName;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String stockFullName) {
		this.fullName = stockFullName;
	}

	private String symbol;

	public String getsymbol() {
		return symbol;
	}

	public void setsymbol(String symbol) {
		this.symbol = symbol;
		// here we get the dataprovider code.
		this.dataProviderCode = generate_data_provider_code(symbol, this.indice, this.indice_stockConstants_code);
	}

	/**
	 * for now the quandl code for this symbol
	 * 
	 */
	private String dataProviderCode;

	public String getDataProviderCode() {
		return dataProviderCode;
	}

	public void setDataProviderCode(String dataProviderCode) {
		this.dataProviderCode = dataProviderCode;
	}

	/**
	 * link to page on wikipedia
	 * 
	 */
	private String link_to_wikipedia_page;

	public String getLink_to_wikipedia_page() {
		return link_to_wikipedia_page;
	}

	public void setLink_to_wikipedia_page(String link_to_wikipedia_page) {
		this.link_to_wikipedia_page = link_to_wikipedia_page;
	}

	/**
	 * industry of this stock as scraped from wikipedia.
	 * 
	 */
	private ArrayList<String> products;

	public ArrayList<String> getProducts() {
		return products;
	}

	@JsonSetter
	public void setProducts(ArrayList<String> es_products) {
		this.products = es_products;
	}

	public void addProducts(String product_string) {
		for (String product : product_string.split(",")) {
			this.products.add(product);
		}

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			for (String product : this.products) {
				jedis.sadd(product, generate_unique_name());
			}
		}

	}

	public void derive_products_length() {
		for (String product : products) {
			System.out.println("product is:" + product + " and its length is:" + product.length());
		}
	}

	/**
	 * products of the stock as scraped from wikipedia
	 * 
	 */
	private ArrayList<String> industry;

	public ArrayList<String> getIndustry() {
		return industry;
	}

	/****
	 * used by jackson to deserliaze the industry arraylist.
	 * 
	 * 
	 * @param es_industry
	 */
	@JsonSetter
	public void setIndustry(ArrayList<String> es_industry) {
		this.industry = es_industry;
	}

	public void addIndustry(String industry) {
		this.industry.addAll(Arrays.asList(industry.split(",")));
		// add to redis sets here.
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			// so we need autocomplete on the companies.
			// so how do we do this?
			jedis.sadd(industry, generate_unique_name());
		}
	}

	/**
	 * the map with the price information for this stock. the key is the day id
	 * the value is the
	 * 
	 */

	public LinkedList<Map<String, Object>> es_linked_list;

	public LinkedList<Map<String, Object>> getEs_linked_list() {
		return es_linked_list;
	}

	public void setEs_linked_list(LinkedList<Map<String, Object>> es_linked_list)
			throws JsonGenerationException, JsonMappingException, IOException {

		this.es_linked_list = es_linked_list;

	}

	// ////////////////////////////////////////////////////////////////////////

	/***
	 * 
	 * 
	 * JSONIGNORABLES START HERE.
	 * 
	 * 
	 * 
	 */

	// /////////////////////////////////////////////////////////////////////////

	/****
	 * 
	 * a map that stores how many days with actual datapoints are available
	 * before each day id in the price_map internal. it is set in the
	 * before_calculate_complexes hook.
	 * 
	 * key -> day id value -> total day ids available before it.
	 */

	@JsonIgnore
	private HashMap<Integer, Integer> total_day_ids_before_day_id;

	@JsonIgnore
	public HashMap<Integer, Integer> getTotal_day_ids_before_day_id() {
		return total_day_ids_before_day_id;
	}

	@JsonIgnore
	public void setTotal_day_ids_before_day_id(HashMap<Integer, Integer> total_day_ids_before_day_id) {
		this.total_day_ids_before_day_id = total_day_ids_before_day_id;
	}

	/***
	 * 
	 * key -> index in the price_map_internal.keyset() collection. value ->
	 * day_id at that index. so the first day id will have a value of 0 in this
	 * map. the next day id will have a value of 1.
	 * 
	 * set in the before_complex_calculate hook.
	 * 
	 * 
	 */
	@JsonIgnore
	private HashMap<Integer, Integer> day_id_and_its_location_in_price_map_internal;

	@JsonIgnore
	public HashMap<Integer, Integer> getDay_id_and_its_location_in_price_map_internal() {
		return day_id_and_its_location_in_price_map_internal;
	}

	@JsonIgnore
	public void setDay_id_and_its_location_in_price_map_internal(
			HashMap<Integer, Integer> day_id_and_its_location_in_price_map_internal) {
		this.day_id_and_its_location_in_price_map_internal = day_id_and_its_location_in_price_map_internal;
	}

	/****
	 * this map has the following structure:
	 * 
	 * array_name -> key start_index -> value.
	 * 
	 * 
	 * this map carries the names of all the price_change and the stop loss
	 * arrays as its keys and the start_index of the first chunk that was added
	 * to as the vlaue.
	 * 
	 * it is used in the entity_index, after download hook, to build the request
	 * to be sent to the remote server
	 * 
	 * it is instantitated and populated in
	 * #update_price_change_and_stop_loss_arrays_to_es
	 * 
	 * basically there each time a array is processed, we consider the first
	 * chunk id, and add it to this map.
	 * 
	 */
	@JsonIgnore
	private HashMap<String, String> stop_loss_and_price_change_arrays_with_first_modified_chunk_id;

	@JsonIgnore
	public HashMap<String, String> getStop_loss_and_price_change_arrays_with_first_modified_chunk_id() {
		return stop_loss_and_price_change_arrays_with_first_modified_chunk_id;
	}

	@JsonIgnore
	public void setStop_loss_and_price_change_arrays_with_first_modified_chunk_id(
			HashMap<String, String> stop_loss_and_price_change_arrays_with_first_modified_chunk_id) {
		this.stop_loss_and_price_change_arrays_with_first_modified_chunk_id = stop_loss_and_price_change_arrays_with_first_modified_chunk_id;
	}

	/****
	 * 
	 * 
	 * ====================================================== for eg:
	 * 
	 * the index of the first newly_added_close_close_price_difference. it is
	 * set in the {@link #update_stop_loss_open_close_arrays()} in that function
	 * a treemap is built whose key is the index of the pair in the
	 * close_close_price_change_map, and whose value is the
	 * price_change_correlate object. so we set the first key from that map
	 * here.
	 * 
	 * and the index of the last newly_added_close_close_price_difference is
	 * also necessary, to know till where to take the newly added chunks.and
	 * hence set the last key from that map here
	 * 
	 * it will be same for open and close, since we run at market close.
	 * 
	 * 
	 * this is used in the
	 * {@link #update_price_change_and_stop_loss_arrays_to_es(BulkProcessor)} to
	 * decide from which chunk updates need to be made to es.
	 * 
	 * -------------------- keys: first, and last values:integers -> pair
	 * indexes.
	 * 
	 * 
	 * 
	 */
	@JsonIgnore
	private HashMap<String, Integer> indices_of_first_and_last_newly_added_pairs;

	@JsonIgnore
	public HashMap<String, Integer> getIndices_of_first_and_last_newly_added_pairs() {
		return indices_of_first_and_last_newly_added_pairs;
	}

	@JsonIgnore
	public void setIndices_of_first_and_last_newly_added_pairs(
			HashMap<String, Integer> indices_of_first_and_last_newly_added_pairs) {
		this.indices_of_first_and_last_newly_added_pairs = indices_of_first_and_last_newly_added_pairs;
	}

	@JsonIgnore
	private Boolean filter_entity;

	public Boolean getFilter_entity() {
		return filter_entity;
	}

	public void setFilter_entity(Boolean filter_entity) {
		this.filter_entity = filter_entity;
	}

	/***
	 * set from {{@link EntityIndex#build_entity_index_from_elasticsearch()}
	 * 
	 */
	@JsonIgnore
	private String entity_es_id;

	@JsonIgnore
	public String getEntity_es_id() {
		return entity_es_id;
	}

	@JsonIgnore
	public void setEntity_es_id(String entity_es_id) {
		this.entity_es_id = entity_es_id;
	}

	/**
	 * whether this entity has to be recalculated from scratch.
	 * 
	 */

	@JsonIgnore
	private Boolean force_redownload;

	@JsonIgnore
	public Boolean getForce_redownload() {
		return force_redownload;
	}

	@JsonIgnore
	public void setForce_redownload(Boolean force_redownload) {
		this.force_redownload = force_redownload;
	}

	/****
	 * when you retreive the price map from es, and set it, it should also set
	 * the price map internal since this is the treemap that is used everywhere.
	 * 
	 * 
	 */

	@JsonIgnore
	private TreeMap<Integer, DataPoint> price_map_internal;

	@JsonIgnore
	public TreeMap<Integer, DataPoint> getPrice_map_internal() {
		return price_map_internal;
	}

	@JsonIgnore
	public void setPrice_map_internal(LinkedList<Map<Integer, DataPoint>> es_linked_list) {
		this.price_map_internal = new TreeMap<Integer, DataPoint>();
	}

	@JsonIgnore
	private TreeMap<Integer, Integer> index_to_day_id_mapping;

	@JsonIgnore
	public TreeMap<Integer, Integer> getIndex_to_day_id_mapping() {
		return index_to_day_id_mapping;
	}

	@JsonIgnore
	public void setIndex_to_day_id_mapping(TreeMap<Integer, Integer> index_to_day_id_mapping) {
		this.index_to_day_id_mapping = index_to_day_id_mapping;
	}

	/**
	 * the latest day id in the entity when the data was loaded from
	 * elasticsearch. this is set in the function
	 * {@link EntityIndex#get_start_date_string_for_request(Integer, Integer)}
	 * it is basically set before the request is made.
	 * 
	 * 
	 */
	@JsonIgnore
	private Integer latest_existing_day_id;

	@JsonIgnore
	public Integer getLatest_existing_day_id() {
		return latest_existing_day_id;
	}

	@JsonIgnore
	public void setLatest_existing_day_id(Integer latest_existing_day_id) {
		this.latest_existing_day_id = latest_existing_day_id;
	}

	/****
	 * 
	 * the start day id for the new request. used in :
	 * after_entity_download_hook, to check whether the expected day ids are
	 * present in the request. set in: calculate_request_start_datestring.
	 * 
	 */
	@JsonIgnore
	public Integer request_start_day_id;

	@JsonIgnore
	public Integer getRequest_start_day_id() {
		return request_start_day_id;
	}

	@JsonIgnore
	public void setRequest_start_day_id(Integer request_start_day_id) {
		this.request_start_day_id = request_start_day_id;
	}

	/****
	 * 
	 * the start date for the new request.
	 * 
	 */
	@JsonIgnore
	public String request_start_date;

	@JsonIgnore
	public String getRequest_start_date() {
		return request_start_date;
	}

	@JsonIgnore
	public void setRequest_start_date(String request_start_date) {
		this.request_start_date = request_start_date;
	}

	/****
	 * 
	 * the end date for the new request.
	 * 
	 */

	@JsonIgnore
	private String request_end_date;

	public String getRequest_end_date() {
		return request_end_date;
	}

	public void setRequest_end_date(String request_end_date) {
		this.request_end_date = request_end_date;
	}

	/***
	 * the names of the indicators applicable to this entity. key ->
	 * indicator_name(bare, as from jsonfile) value -> array of names
	 * constructed like :(indicator_name + "_" ..period info)
	 */
	@JsonIgnore
	private LinkedHashMap<String, ArrayList<String>> entity_initial_stub_names;

	@JsonIgnore
	public LinkedHashMap<String, ArrayList<String>> getEntity_initial_stub_names() {
		return entity_initial_stub_names;
	}

	@JsonIgnore
	public void setEntity_initial_stub_names(LinkedHashMap<String, ArrayList<String>> entity_indicators) {
		this.entity_initial_stub_names = entity_indicators;
	}

	@JsonIgnore
	private RequestBase rb;

	@JsonIgnore
	public RequestBase getRb() {
		return rb;
	}

	@JsonIgnore
	public void setRb(RequestBase rb) {
		this.rb = rb;
	}

	@JsonIgnore
	private TimeSeries ts;

	@JsonIgnore
	public TimeSeries getTs() {
		return ts;
	}

	@JsonIgnore
	public void setTs(TimeSeries ts) {
		this.ts = ts;
	}

	// CONSTRUCTORS.---------------------------------------------------->
	@JsonCreator
	public Entity() {

		this.entity_initial_stub_names = new LinkedHashMap<String, ArrayList<String>>();

	}

	/**
	 * used for entity index building from components.
	 * 
	 */
	public Entity(String indice, Integer indice_stockConstants_code) {
		this.indice = indice;
		this.indice_stockConstants_code = indice_stockConstants_code;
		this.es_linked_list = new LinkedList<Map<String, Object>>();
		this.products = new ArrayList<String>();
		this.industry = new ArrayList<String>();
	}

	// ----------------------------------------------------------------->

	// ENTITY ELASTICSEARCH METHODS------------------------------------->

	// ENTITY GENERAL METHODS ------------------------------------------>

	public String generate_unique_name() {
		return this.symbol + "_" + this.fullName + "_" + this.indice;
	}

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	// ///////////------------------------------------------------------------------interface
	// methods. start here

	@Override
	public String generate_data_provider_code(String component_scraper_symbol, String index,
			Integer index_code_in_stockConstants) {
		// TODO Auto-generated method stub
		if (index.equals(StockConstants.name_for_nasdaq)) {
			return "WIKI/" + component_scraper_symbol;
		} else if (index.equals(StockConstants.name_for_cac)) {
			return "YAHOO/PA_" + component_scraper_symbol;
		} else if (index.equals(StockConstants.name_for_dax)) {
			return "YAHOO/DE_" + component_scraper_symbol;
		} else if (index.equals(StockConstants.name_for_ftse)) {
			return "YAHOO/L_" + component_scraper_symbol;
		} else if (index.equals(StockConstants.name_for_nifty)) {
			return "NSE/" + component_scraper_symbol;
		} else if (index.equals(StockConstants.name_for_forex)) {

			// forex doesnt need a data provider code.
			return "";
		} else if (index.equals(StockConstants.name_for_metals)) {

			if (getsymbol().equals("gold")) {
				return "BUNDESBANK/BBK01_WT5511";
			} else {
				// dont yet know what i do for silver and others.
				return null;
			}

		} else if (index.equals(StockConstants.name_for_oil)) {
			// return "YAHOO/INDEX_XOI";
			// return "NASDAQOMX/NQG0001";
			return "OPEC/ORB";
		} else {
			return null;
		}

	}

	/****
	 * 
	 * CALLED INTERNALLY ONLY called internally in {@link #Entity()
	 * #setEs_linked_list(LinkedList)}
	 * 
	 * 
	 */
	@Override
	public String calculate_start_date_string_for_request() {

		String datestring_n_days_ago = null;

		Integer day_id_n_days_ago = getLatest_existing_day_id() - StockConstants.Entity_days_ago;

		if (day_id_n_days_ago < CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey()) {

			datestring_n_days_ago = CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstEntry().getValue()
					.getDateString();
			setRequest_start_day_id(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey());

		} else {

			datestring_n_days_ago = CentralSingleton.getInstance().gmt_day_id_to_dataPoint.get(day_id_n_days_ago)
					.getDateString();
			setRequest_start_day_id(day_id_n_days_ago);
		}

		return datestring_n_days_ago;

	}

	/****
	 * 
	 * CALLED INTERNALLY ONLY called internally in {@link #Entity()
	 * #setEs_linked_list(LinkedList)}
	 * 
	 * 
	 */
	@Override
	public String calculate_end_date_for_request() {

		DateTime dt = new DateTime(DateTimeZone.forID("GMT"));

		if (StockConstants.define_custom_end_date) {
			dt = dt.minusDays(StockConstants.minus_days_from_current_datetime);
		} else {
			dt = dt.plusDays(2);
		}

		String date_string = String.valueOf(dt.getYear()) + "-"
				+ StringUtils.leftPad(String.valueOf(dt.getMonthOfYear()), 2, "0") + "-"
				+ StringUtils.leftPad(String.valueOf(dt.getDayOfMonth()), 2, "0");

		return date_string;
	}

	/****
	 * 
	 * CALLED INTERNALLY ONLY called internally in {@link #Entity()
	 * #setEs_linked_list(LinkedList)}
	 * 
	 * 
	 */
	@Override
	public Integer calculate_latest_existing_day_id() {

		Integer last_existing_day_id = null;

		if (!getPrice_map_internal().isEmpty()) {
			last_existing_day_id = getPrice_map_internal().lastKey();

		} else {

			last_existing_day_id = CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey();

		}

		return last_existing_day_id;
	}

	/**
	 * if the latest existing day id is the default start day id, then we make a
	 * tailmap that is inclusive of the latest existing day id, since we will
	 * have to calculate the complexes for that also.
	 * 
	 * if its not the default start day id,then we need to derive complexes from
	 * one day after whatever is the latest one. if that (latest + 1) is a
	 * saturday or something, it wont matter, the minimum increment is 1.
	 * 
	 * 
	 * CALLED FROM ENTITY INDEX AFTER THE DATA HAS BEEN DOWNLOADED FOR THIS
	 * ENTITY
	 * 
	 * @link {@link EntityIndex#download_new_data_for_entity(Entity)}
	 * 
	 * 
	 */
	@Override
	public Integer derive_calculate_complexes_from_day_id() {

		if (getLatest_existing_day_id().equals(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey())) {

			return getPrice_map_internal().tailMap(getLatest_existing_day_id(), true).firstKey();

		} else {

			return getPrice_map_internal().tailMap(getLatest_existing_day_id(), true).firstKey() + 1;

		}

	}

	/****
	 * called from
	 * 
	 * {@link EntityIndex#update_entity_to_es(Entity)}
	 * 
	 * CALLED WHEN WE WANT TO UPDATE THE ENTITY BACK TO ES , SO FIRST WE
	 * POPULATE THE ES LINKED LIST WITH THE DATA FROM PRICE MAP INTERNAL.
	 * 
	 * 
	 * EXPLANATORY NOTES : 16/4/2017 ---------------------------------------
	 * 
	 * How does the pair index numbering work? The numbering is based on the
	 * start index, example:
	 * 
	 * 0-1,0-2,0-3,0-4,0-5,0-6,0-7.......1-2,1-3,1-4,1-5,1-6 So now suppose that
	 * we have downloaded prices upto day id 4. so it will have values for
	 * 0-1,0-2,0-3,0-4, then nothing for 0-5,0-6,0-7 and then again for
	 * 1-2,1-3,1-4 it will have, and nothing there onwards, and again for
	 * 2-3,2-4 it will have.etc.
	 * 
	 * then suppose we download prices again, but this time upto 7, now pairs
	 * will have to be modified, for the 0 series, then jump to the 1 series and
	 * so.
	 * 
	 * So pairs earlier than the latest newly download pair will be modified.
	 * -----------------------------------------------------------------------
	 * 
	 * How does the stat stop loss concept work: ------------------- what
	 * happens is that the we make a treemap as follows each time:
	 * 
	 * key -> index in large array value -> price change at that index.
	 * 
	 * we then iterate this treemap from top to bottom (i.e in ascending order)
	 * we take the pair, and then we see if it satisifies a price change equal
	 * to any stop loss if yes, then we check whether at that index(start index)
	 * in the stat stop loss array, we already have a larger value. this is only
	 * possible actually when we havent yet got a stop loss. eg: the default
	 * value at any index in the stat stop loss array is the value of the next
	 * pair, eg: at 0eth index, there is value of pair 1-2. suppose we are
	 * iterating for pair 0 and we find that at 0 - 20 we have a 5 percent stop
	 * loss then we check at 0 position in stat map if we already have a greater
	 * value, it will be greater by default, so at 0 we set the large array
	 * index conversion of 0 - 20 now suppose we only had values upto day 20,
	 * and then on the next run we get values upto day 21. even if 0 - 21 is a
	 * fall of 20% , since the pairs will all be iterated starting from the
	 * first pair, it can never affect what is already stored at 0-20. so
	 * successive runs of the stat stop loss should not change what is already
	 * stored.
	 * 
	 * 
	 * 
	 * How will stat_stop loss be affected on multiple downloads.
	 * 
	 * -----------------------------------------------------------------------
	 * 
	 * How does the tsl concept work.
	 * 
	 * How will it be affected on multiple downloads
	 * 
	 * ----------------------------------------------------------------------
	 * 
	 * 
	 * 
	 * @throws Exception
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * 
	 */
	@Override
	public void update_stop_loss_open_close_arrays() throws Exception {

		/****
		 * test calc stat stop losses. will modify this shti.
		 * 
		 */

		HashMap<String, Object> boo_res = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_stat_stop_loss));
		if (boo_res.values().size() == 1 && boo_res.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_stat_stop_loss_test(this, null);
		}

		HashMap<String, Object> boo_res2 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_basic_trailing_stop_loss));
		if (boo_res2.values().size() == 1 && boo_res2.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_basic_trailing_stop_loss_test(this);
		}

		HashMap<String, Object> boo_res3 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_highest_price_trailing_stop_loss));
		if (boo_res3.values().size() == 1 && boo_res3.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_basic_trailing_stop_loss_test(this);
		}

		HashMap<String, Object> boo_res4 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_multiple_sub_tsl_falls_trigger_tsl));
		if (boo_res4.values().size() == 1 && boo_res4.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_multiple_sub_tsl_falls_triggering_tsl(this);
		}

		HashMap<String, Object> boo_res5 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_stat_stop_loss_first_run));
		if (boo_res5.values().size() == 1 && boo_res5.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_stat_stop_loss_test(this, null);
		}

		HashMap<String, Object> boo_res6 = TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				StopLossTests.normalize_price_map_internal_before_stat_stop_loss_second_run));
		if (boo_res6.values().size() == 1 && boo_res6.containsValue(true)) {
			TestMethodsExecutor.normalize_price_map_internal_for_second_run_of_stat_stop_loss_test(this);
			;
		}

		HashMap<String, Object> boo_res7 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculated_tsl_as_bsl));
		if (boo_res7.values().size() == 1 && boo_res7.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_tsl_as_bsl_test(this);
			;
		}

		HashMap<String, Object> boo_res8 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_calculate_tsl_threshold));
		if (boo_res8.values().size() == 1 && boo_res8.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_tsl_threshold_test(this);
		}

		HashMap<String, Object> boo_res9 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_tsl_first_run));
		if (boo_res9.values().size() == 1 && boo_res9.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_tsl_first_run(this);
		}

		HashMap<String, Object> boo_res10 = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_tsl_second_run));
		if (boo_res10.values().size() == 1 && boo_res10.containsValue(true)) {
			TestMethodsExecutor.modify_price_map_internal_for_second_tsl_run(this);
		}

		/***
		 * 
		 * end test calc stop losses.
		 * 
		 */

		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("init:stop loss calc");

		LogObject lg1 = new LogObject(getClass(), this);
		lg1.commit_info_log("g:unsorted arrs");

		/****
		 * initializes an array of 99999 at each element for 300*3000 elements.
		 * and it sets values at for every element based on price change. and
		 * the elements are like: 0-1,0-2,0-3,......,1-2,1-3,1-4 so to report
		 * the present state, it should just check that the value for every
		 * price change > latest_existing_day - each index is 99999. that will
		 * be an adequate test that things are in correct shape.
		 * 
		 * okay so here the main thing is how to calculate the pair indices.
		 * basically we have to know the latest existing day id. so that minus
		 * each preceeding day id will give us the index. and then we can know
		 * that any indice after that, it should be null.
		 * 
		 */
		int[] unsorted_decompressed_arr_open_open = PairArray.get_decompressed_arr(
				this.unique_name + "_" + StockConstants.Entity_open_open_map_suffix, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);
						// System.out.println("existing value at the zeroeith
						// index :" + unsorted_decompressed_arr_open_open[0]);
						// Thread.sleep(10000);
						/***
						 * same as above, but for close_close
						 * 
						 * 
						 */
		int[] unsorted_decompressed_arr_close_close = PairArray.get_decompressed_arr(
				this.unique_name + "_" + StockConstants.Entity_close_close_map_suffix, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);

		/*****
		 * 
		 * here we check for the purpose of event logging if these two arrays
		 * contain 99999 for the indice one day after what is expected to be the
		 * last calculated indice.
		 * 
		 */
		check_price_change_arrays_for_event_logging(unsorted_decompressed_arr_close_close,
				unsorted_decompressed_arr_open_open);

		/****
		 * this contains 3000 entries one for each day in our db. its for daily
		 * open prices.
		 * 
		 */
		int[] unsorted_decompressed_arr_daily_open_price = PairArray.get_decompressed_arr(
				this.unique_name + "_" + StockConstants.Entity_daily_open_map_suffix, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		/***
		 * same as above , but for close.
		 */
		int[] unsorted_decompressed_arr_daily_close_price = PairArray.get_decompressed_arr(
				this.unique_name + "_" + StockConstants.Entity_daily_close_map_suffix, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		// so here we want to know.
		// what is the

		/***
		 * initialize a simple treemap for the stat_stop_losses. the key is the
		 * index in the big array, the value is the percentageintegercorrelate
		 * object, It is repopulated from scratch i.e for each and every day in
		 * the price map internal, each and every time. It is iterated , for the
		 * stat stop loss generation for each key in it, the stat stop losses
		 * are checked. so basically stat stop losses are also rechecked and
		 * calculated each time.
		 */
		final TreeMap<Integer, PercentageIntegerCorrelate> treemap_for_stat_stop_losses = new TreeMap<Integer, PercentageIntegerCorrelate>();

		/***
		 * initialize the hashmap that will hold the indices of the first and
		 * last newly added pairs, these indices refer to the indices in the big
		 * array.
		 */
		setIndices_of_first_and_last_newly_added_pairs(new HashMap<String, Integer>());
		// System.out.println("price map internal day ids are:");
		// System.out.println(getPrice_map_internal().keySet());
		for (Map.Entry<Integer, DataPoint> entry : getPrice_map_internal().entrySet()) {
			LogObject lgdd = new LogObject(getClass(), this);
			// lgdd.commit_debug_log("the first key of the newest added
			// datapoints is:"
			// + generate_newly_downloaded_datapoints().firstKey());

			/***
			 * treat the current day as the end day id.
			 *
			 */
			Integer end_day_id = entry.getKey();

			/***
			 * assign the current datapoint to this variable, basically
			 * continuation of above comment.
			 * 
			 */
			DataPoint end_day_id_data_point = entry.getValue();

			/****
			 * here we are using price_map_internal and we want to just get a
			 * section of it, upto but not including the end_day_id or the
			 * current day id that we are iterating. we want it to include 300
			 * days before the current day id. we will only take the price map
			 * upto days which are numerically total_pairs_ minused.
			 */
			SortedMap<Integer, DataPoint> sm = generate_price_map_in_range(end_day_id,
					StockConstants.Entity_total_pairs_for_a_day, false);

			/***
			 * now iterate this section that was created above.
			 * 
			 */
			for (Map.Entry<Integer, DataPoint> sm_entry : sm.entrySet()) {

				/***
				 * now assign this day as the start day id, just like above how
				 * we assigned the end day id. we basically want to process each
				 * entry in this section,= against the end_day_id specified
				 * above.
				 */
				Integer start_day_id = sm_entry.getKey();

				/**
				 * and this datapoint as the start day id datapoint.
				 * 
				 */
				DataPoint start_day_id_data_point = sm_entry.getValue();

				/****
				 * we now need to know which index in the big array and which
				 * index in the routine array to consider.
				 * 
				 * big_array => open_open or close_close array defined above.
				 * routine_array => the daily_open or daily_close array defined
				 * above.
				 * 
				 * this function gisves those values, be returning a PairArray
				 * object, that contains both those values.
				 * 
				 */
				PairArray array_indices = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
						end_day_id, this);

				/****
				 * gives the integer correlation of the price change for open
				 * and close.
				 * 
				 * basically we convert all the price changes into integers for
				 * ease of array compression.
				 * 
				 * it is calculated between the current end_point -> start_point
				 * pair.
				 */
				PercentageIntegerCorrelate price_change_integer_corr = start_day_id_data_point
						.calculate_differences_from_end_day_datapoint(end_day_id_data_point, false);

				HashMap<String, Object> t_args = new HashMap<String, Object>();
				t_args.put("entity", this);
				t_args.put("start_day_id", start_day_id);
				t_args.put("end_day_id", end_day_id);
				t_args.put("pair_array", array_indices);
				t_args.put("size_of_price_map_in_range", sm.size());
				t_args.put("pc_correlate", price_change_integer_corr);
				t_args.put("start_day_id_datapoint", start_day_id_data_point);
				t_args.put("end_day_id_datapoint", end_day_id_data_point);
				TestMethodsExecutor.exec(new TestMethodExecutorArgs(
						StopLossTests.test_generate_price_map_internal_and_pair_array_index_calculation, t_args));

				/**
				 * 
				 * populating then open-open array.
				 * 
				 * 
				 */

				if (price_change_integer_corr.getOpen_open_price_change_correlate() != null) {

					/***
					 * 
					 * now at the index in the big array, for open, open, at
					 * this integer correlate calculated above
					 * 
					 */

					unsorted_decompressed_arr_open_open[array_indices
							.getPair_difference_array_index()] = price_change_integer_corr
									.getOpen_open_price_change_correlate();
									// if
									// (array_indices.getPair_difference_array_index().intValue()
									// == 0) {
									// System.out.println("at the zeroethe index
									// for open open this is the pc correlate");
									// System.out.println(price_change_integer_corr);
									// System.out.println("value set at the
									// index in the array was:");
									// System.out.println(
									// unsorted_decompressed_arr_open_open[array_indices.getPair_difference_array_index()]);
									// Thread.sleep(20000);
									// }
									/***
									 * and at the index for the small array, put
									 * just the open price correlate(just the
									 * open price converted into integer) this
									 * correlate is got by calling a function on
									 * the datapoint itself, this no longer
									 * works, dunno y rite now.
									 */

					// unsorted_decompressed_arr_daily_open_price[array_indices
					// .getStart_day_id_based_index()] =
					// start_day_id_data_point
					// .getInt_open();

				}

				/**
				 * 
				 * 
				 * populating the close-close array. same thing as above for
				 * open_open, but for close_close difference.
				 * 
				 */
				unsorted_decompressed_arr_close_close[array_indices
						.getPair_difference_array_index()] = price_change_integer_corr
								.getClose_close_price_change_correlate();

				/**
				 * 
				 * 
				 * populating the close price array. same thing as above but for
				 * close. also does not work anymore.
				 * 
				 */
				// unsorted_decompressed_arr_daily_close_price[array_indices
				// .getStart_day_id_based_index()] = start_day_id_data_point
				// .getInt_close();

				/**
				 * 
				 * populating the stat stop loss treemap.
				 * 
				 * 
				 */

				treemap_for_stat_stop_losses.put(array_indices.getPair_difference_array_index(),
						price_change_integer_corr);

				// HERE DO THE TSL PART.

				/***
				 * THIS IS ONLY USED IN THE setting the index of the first
				 * actually newly added pair. provided that the day id is
				 * greater than or equal to the first newly added day, and the
				 * index of the first newly added pair has not yet been set,
				 * then we set the index of the first newly added pair as the
				 * current array_indices.getPair_difference_array_index.
				 */
				if (entry.getKey() >= generate_newly_downloaded_datapoints().firstKey()
						&& !getIndices_of_first_and_last_newly_added_pairs()
								.containsKey(StockConstants.Entity_first_newly_added_pair_index)) {
					getIndices_of_first_and_last_newly_added_pairs().put(
							StockConstants.Entity_first_newly_added_pair_index,
							array_indices.getPair_difference_array_index());
				}

			}

		}

		/****
		 * so basically add just two things to
		 * "Indices_of_first_and_last_newly_added_pairs" the index of the first
		 * pair done. and now below add the index of the last pair done, which
		 * is basically the last key in the treemap of stat_stop_losses, which
		 * contains the large array index as the key and the
		 * pricechangecorrelate object as the value.
		 * 
		 */
		/***
		 * consists of all the arrays stat stop losses, as well as trailing stop
		 * losses. initially it contains the stat stop loss arrays, got from
		 * centralsingleton or initialized new.
		 */
		HashMap<String, int[]> hashmap_of_combined_stop_loss_arrays = PairArray
				.get_hashmap_of_all_stat_stop_loss_arrays(this.unique_name);

		/****
		 * this is just a copy of the above array for the purpose of event
		 * logging. because we need a copy of the arrays before.
		 */
		HashMap<String, int[]> stop_loss_arrays_before_processing = new HashMap<String, int[]>();
		stop_loss_arrays_before_processing.putAll(hashmap_of_combined_stop_loss_arrays);

		if (!treemap_for_stat_stop_losses.isEmpty()) {
			/***
			 * we pass it in and now added in the trailing stop loss arrays,
			 * either from centralsingleton or initialized new.
			 */
			hashmap_of_combined_stop_loss_arrays = PairArray.get_hashmap_of_all_trailing_stop_loss_arrays(
					this.unique_name, hashmap_of_combined_stop_loss_arrays);
			stop_loss_arrays_before_processing.putAll(hashmap_of_combined_stop_loss_arrays);

			LogObject lgd = new LogObject(getClass(), this);

			getIndices_of_first_and_last_newly_added_pairs().put(StockConstants.Entity_last_newly_added_pair_index,
					treemap_for_stat_stop_losses.lastKey());

			lgd.commit_debug_log("index of first newly added pair:"
					+ getIndices_of_first_and_last_newly_added_pairs()
							.get(StockConstants.Entity_first_newly_added_pair_index)
					+ " and index of last newly added pair:" + getIndices_of_first_and_last_newly_added_pairs()
							.get(StockConstants.Entity_last_newly_added_pair_index));

			for (Integer tsl : StockConstants.Entity_stop_loss_amounts) {
				TrailingStopLoss ts_obj = new TrailingStopLoss(this, tsl, tsl, "close_close",
						hashmap_of_combined_stop_loss_arrays, treemap_for_stat_stop_losses);
				ts_obj.process();
			}
		}

		// PUT BACK INTO THE CONCURRENT HASHMAP all these arrays.
		// simply put back what we calculated above.

		PairArray.put_arr(unsorted_decompressed_arr_open_open,
				this.unique_name + "_" + StockConstants.Entity_open_open_map_suffix, 0);

		PairArray.put_arr(unsorted_decompressed_arr_close_close,
				this.unique_name + "_" + StockConstants.Entity_close_close_map_suffix, 0);

		PairArray.put_arr(unsorted_decompressed_arr_daily_open_price,
				this.unique_name + "_" + StockConstants.Entity_daily_open_map_suffix, 0);

		PairArray.put_arr(unsorted_decompressed_arr_daily_close_price,
				this.unique_name + "_" + StockConstants.Entity_daily_close_map_suffix, 0);

		LogObject lg2 = new LogObject(getClass(), this);
		lg2.commit_info_log("end: calc unsorted arrs");

		/****
		 * upto this point its all pretty straightforward. now comes the
		 * complicated part of the stop losses, themselves.
		 * 
		 * 
		 */

		/***
		 * 
		 * UPDATE THE STAT STOP LOSS ARRAYS.
		 * 
		 * 
		 * 
		 */

		LogObject lg3 = new LogObject(getClass(), this);
		lg3.commit_info_log("init: calc stat st_loss");
		// this is the treemap.
		for (Map.Entry<Integer, PercentageIntegerCorrelate> entry : treemap_for_stat_stop_losses.entrySet()) {

			Integer index_in_pair_differences_arr = entry.getKey();
			PercentageIntegerCorrelate pc = entry.getValue();

			/****
			 * we iterate as many times as we have stop loss amounts. so
			 * basically if the price change satisfies the stat stop loss amount
			 * then we check if the stat stop loss amount has already been
			 * satisfied by an earlier thing, and if not replace it.
			 * 
			 * so basically, what this means is that the existing state for any
			 * particular start day id, we want some kind of a snapshot. but
			 * that is pretty rediculous. if you just want to check it, we can
			 * report new stat stop losses if they are detected.
			 * 
			 */
			for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {

				if (pc.getOpen_open_applicable_stop_losses()[i] == true) {

					String name_of_relevant_stat_stop_loss_arr_open_open = PairArray
							.get_name_of_relevant_stat_stop_loss_map(this.unique_name,
									StockConstants.Entity_open_open_map_suffix,
									StockConstants.Entity_stop_loss_amounts[i]);

					PairArray.modify_sorted_int_array_at_index(index_in_pair_differences_arr,
							hashmap_of_combined_stop_loss_arrays.get(name_of_relevant_stat_stop_loss_arr_open_open),
							this.unique_name, StockConstants.Entity_stop_loss_amounts[i],
							hashmap_of_combined_stop_loss_arrays, name_of_relevant_stat_stop_loss_arr_open_open,
							StockConstants.Entity_open_open_map_suffix);

				}
				if (pc.getClose_close_applicalbe_stop_losses()[i] == true) {

					String name_of_relevant_stat_stop_loss_arr_close_close = PairArray
							.get_name_of_relevant_stat_stop_loss_map(this.unique_name,
									StockConstants.Entity_close_close_map_suffix,
									StockConstants.Entity_stop_loss_amounts[i]);

					PairArray.modify_sorted_int_array_at_index(index_in_pair_differences_arr,
							hashmap_of_combined_stop_loss_arrays.get(name_of_relevant_stat_stop_loss_arr_close_close),
							this.unique_name, StockConstants.Entity_stop_loss_amounts[i],
							hashmap_of_combined_stop_loss_arrays, name_of_relevant_stat_stop_loss_arr_close_close,
							StockConstants.Entity_close_close_map_suffix);

				}

			}

		}

		for (Map.Entry<String, int[]> entry : hashmap_of_combined_stop_loss_arrays.entrySet()) {

			PairArray.put_arr(entry.getValue(), entry.getKey(), 1);

		}

		/****
		 * for event logger.
		 * 
		 */
		compare_stop_loss_arrays_for_event_logging(stop_loss_arrays_before_processing,
				hashmap_of_combined_stop_loss_arrays);

		LogObject lg4 = new LogObject(getClass(), this);
		lg4.commit_info_log("end: calc stat st_loss");

		// test equality.

		/********
		 * 
		 * UPDATE THE TRAILING STOP LOSS ARRAYS.
		 * 
		 * in simplicity, we want to check whether for a given single day change
		 * we process all the pairs starting from a day 300 days minus the day
		 * after the end day id of the single day change
		 * 
		 * whatever is the change in that pair should be > the stat stop loss
		 * multiplied by 10(if negative stat stop loss)
		 * 
		 * should be < the stat stop loss multiplied by 10(if positive stat stop
		 * loss) if yes => just check that at this index in the tsl the value is
		 * more than this pair value, so then change it. if no => dont do
		 * anything.
		 * 
		 */

		/***
		 * LogObject lg5 = new LogObject(getClass(), this); lg5.commit_info_log(
		 * "init: calc tsl");
		 * 
		 * PairArray.get_single_day_changes_arr_for_stock(this.unique_name,
		 * StockConstants.Entity_open_open_map_suffix,
		 * getPrice_map_internal().lastKey(), treemap_for_stat_stop_losses,
		 * hashmap_of_combined_stop_loss_arrays,
		 * getPrice_map_internal().firstKey(), this);
		 * 
		 * PairArray.get_single_day_changes_arr_for_stock(this.unique_name,
		 * StockConstants.Entity_close_close_map_suffix,
		 * getPrice_map_internal().lastKey(), treemap_for_stat_stop_losses,
		 * hashmap_of_combined_stop_loss_arrays,
		 * getPrice_map_internal().firstKey(), this);
		 **/
		// now update all the things from this map to the concurrent hashmap.

		// //////////?///////////////////////////////////////////////////////////////////////////////
		//
		//
		// ALGORITHM FOR THE STOP LOSSES AND THE TRAILING STOP LOSSES.
		//
		//
		// //////////////////////////////////////////////////////////////////////////////////////////

		/**
		 * 
		 * first do the stat stop losses. create a treemap with key
		 * 
		 * => integer(index in the unsorted map) value -> [boolean
		 * array(open),boolean array(closed)]
		 * 
		 * 
		 * take the first one, write a function to get its start day id, i.e its
		 * index in the sorted map, deriving from index in unsorted map. go to
		 * that index in the sorted map. if the value there is more than the
		 * (index in unsorted map)/ or the value there cannot possibly be
		 * there., then replace it with this value.
		 * 
		 * do this for all the things that are there.
		 * 
		 * 
		 * -------------------------------------------------
		 * 
		 * now for the trailing stop losses
		 * 
		 * build a simple treemap of the single day change(day id) -> amount of
		 * change.
		 * 
		 * now we have to do this for all relevant combinations, so if 2%
		 * trailing, then do for with 2,3,5 proceed only if the change
		 * satisifies a stop loss condition, i.e 2,3,5, -2,-3,-5
		 * 
		 * write formula to get all start day ids that can be affected by this
		 * single day change. go to the first start day id, get its datapoint
		 * and see what is its (relevant open or close value) now on the current
		 * day id change, check that this value is either above or below(in case
		 * of short), the value on the said day id.
		 * 
		 * now get the relevant pair i.e start_day_id + end_day_id of the single
		 * day change. eg
		 * 
		 * for 4,5 and start day id 1 it would be 1-5 - get its index in the
		 * unsorted map.
		 * 
		 * now if it satisfies that condition, check the value in the relevant
		 * stat stop loss array for this start day id, if it is less than this
		 * pair index, then store it , otherwise store this pair index.
		 * 
		 * now run a normalization function on all sorted arrays before storing
		 * them. i.e if a value is greater than a following value, make it equal
		 * to the following value.
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
	}

	@Override
	public void generate_index_to_day_id_mapping() {

		Integer key = 0;
		for (Map.Entry<Integer, DataPoint> entry : getPrice_map_internal().entrySet()) {

			getIndex_to_day_id_mapping().put(key, entry.getKey());

			key++;
		}

		/**
		 * LogObject lg = new LogObject(getClass(), this); lg.commit_info_log(
		 * "init: g cramped tm"); Integer first_available_day_id =
		 * getPrice_map_internal().firstKey();
		 * 
		 * TreeMap<Integer, DataPoint> map_for_pairs = new TreeMap<Integer,
		 * DataPoint>(); for (Map.Entry<Integer, DataPoint> entry :
		 * getPrice_map_internal() .entrySet()) {
		 * 
		 * map_for_pairs.put(entry.getKey(), entry.getValue()); // making the
		 * value in price map internal as null. // memory overload!!!
		 * entry.setValue(null); }
		 * 
		 * for (Integer day_id :
		 * CentralSingleton.getInstance().gmt_day_id_to_dataPoint .keySet()) {
		 * 
		 * if (day_id > first_available_day_id) {
		 * 
		 * SortedMap<Integer, DataPoint> sm = map_for_pairs .subMap(day_id,
		 * true, day_id + CentralSingleton.getInstance().gmt_day_id_to_dataPoint
		 * .size(), true);
		 * 
		 * // if the day id does not exist in the map for pairs(basically // if
		 * the first key of the submap does // not have the day id, means that
		 * the day ids does not exist in // teh map for pairs) if (sm.size() > 0
		 * && !sm.firstKey().equals(day_id)) {
		 * 
		 * // for each missing day, fill it with the forward day // i.e if there
		 * is a gap from 4 .. 9 // then fill the gap with the value for 9
		 * 
		 * for (int i = day_id; i < sm.firstKey(); i++) {
		 * 
		 * map_for_pairs.put(i, map_for_pairs.get(sm.firstKey()));
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * setMap_for_pairs(map_for_pairs); LogObject lg1 = new
		 * LogObject(getClass(), this); lg1.commit_info_log("end: g cramped tm"
		 * );
		 **/
	}

	@Override
	public void generate_price_change_buckets_and_stddev_array(BulkProcessor processor, Boolean do_buckets)
			throws Exception {
		// TODO Auto-generated method stub

		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("init : gen std_Dev + price_bucket");

		if (do_buckets) {
			HashMap<Integer, PeriodBucket> map_for_stddev_calculations = new HashMap<Integer, PeriodBucket>();

			for (Integer day_id : getPrice_map_internal().keySet()) {
				// LogObject lg1 = new LogObject(getClass(), this);
				// lg1.commit_info_log("init: gen_std_dev + price_bucket: day
				// id:"
				// + day_id);

				for (Integer period : IdAndShardResolver.periods_map.keySet()) {

					Integer end_day_id = day_id + period;

					if (end_day_id <= generate_newly_downloaded_datapoints().lastKey()) {
						PairArray pa = PairArray.get_int_array_index_given_start_day_id_and_difference(day_id, period);
						Integer close_price_change = PairArray.get_value_from_arr_at_index(
								getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix, 0,
								CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
										* StockConstants.Entity_total_pairs_for_a_day,
								pa.getPair_difference_array_index());

						// if it has open, then also get open price change.
						Integer open_price_change = null;

						if (getPrice_map_internal().get(day_id).getOpen() != null) {
							open_price_change = PairArray.get_value_from_arr_at_index(
									getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix, 0,
									CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
											* StockConstants.Entity_total_pairs_for_a_day,
									pa.getPair_difference_array_index());
						}

						PeriodBucket pcb = new PeriodBucket(close_price_change, open_price_change, period.toString());

						// now all you have to tdo is to push this into an
						// array.
						if (map_for_stddev_calculations.containsKey(period)) {

							map_for_stddev_calculations.get(period).getClose_price_arraylist().add(close_price_change);

							map_for_stddev_calculations.get(period).getOpen_price_arraylist().add(open_price_change);

						} else {
							map_for_stddev_calculations.put(period, pcb);

						}
					}

				}

			}

			LogObject lg2 = new LogObject(getClass(), this);
			lg2.commit_info_log("completed: price_change_bucket_map generation");

			LogObject lg3_i = new LogObject(getClass(), this);
			lg3_i.commit_info_log("init: building stddev maps and/or pc_bucket_update to es");

			for (final Map.Entry<Integer, PeriodBucket> entry : map_for_stddev_calculations.entrySet()) {
				// LogObject lg4 = new LogObject(getClass(), this);
				// lg4.commit_info_log("process day id:" + entry.getKey());
				final Integer period = entry.getKey();

				for (final Integer close_price_amount : entry.getValue().getClose_price_arraylist()) {
					String id = "A:" + close_price_amount;

					UpdateRequest ureq = new UpdateRequest("tradegenie_titan", "price_buckets", id);

					ureq.script("price_bucket_script", ScriptService.ScriptType.FILE, new HashMap<String, Object>() {
						{
							put("period", period);
							put("close_price_amount", close_price_amount);
						}
					});

					ureq.scriptedUpsert(true);

					ureq.upsert(new HashMap<String, Object>() {
						{
							put("period", period);
							put("close_price_amount", close_price_amount);
						}
					});

					BulkPayLoad bl = new BulkPayLoad(getUnique_name(), id);
					processor.add(ureq, bl);
				}

			}

			LogObject lg3_e = new LogObject(getClass(), this);
			lg3_e.commit_info_log("completed: building stddev maps and/or pc_bucket_update to es");

			LogObject lg_1 = new LogObject(getClass(), this);

			processor.flush();

			lg_1.commit_info_log("flushed processor after price buckets update");
		} else {
			LogObject lg_ex = new LogObject(getClass(), this);
			lg_ex.commit_info_log("do buckets disabled");
		}
	}

	/****
	 * download entity from provider, makes the request, and then calls
	 * afterdownload.
	 * 
	 * in afterdownload all the conditions are checked to see if the downloaded
	 * datapoints are valid, and then it returns either true or false. true if
	 * this entity should be filtered from further processing.f false if it
	 * should not be filtered. the bulk processor passed in here is used only
	 * for the generate_price_change_buckets, which will only be ever done once
	 * in the entire history of this project. the bulk processor is NOT used for
	 * anything else.
	 * 
	 * the set filter is done so that int he entity index after download
	 * function, the entity map is filtered based on this set filter clause.
	 * 
	 * 
	 * 
	 */
	@Override
	public void download_entity_from_provider(BulkProcessor remote_processor, BulkProcessor local_processor)
			throws Exception {
		// TODO Auto-generated method stub

		if (getIndice().equals(StockConstants.name_for_forex)) {

			final ArrayList<String> request_dates = new ArrayList<String>();
			for (Map.Entry<String, Integer> entry : CentralSingleton.getInstance().gmt_datestring_to_day_id
					.entrySet()) {

				if (entry.getValue() >= getRequest_start_day_id()) {
					request_dates.add(entry.getKey());
					if (entry.getKey().equals(getRequest_end_date())) {
						break;
					}
				}
			}

			// now make the forex request.
			ForexRequest frex = new ForexRequest(getsymbol(), getFullName(), new LinkedHashMap<String, Object>() {
				{
					put(StockConstants.Entity_forex_request_dates, request_dates);
				}
			}, this);

			frex.run();
			setRb(frex);

			setFilter_entity(after_download_entity_hook(frex, remote_processor, local_processor));

		} else {
			QuandlRequest qd = new QuandlRequest(getsymbol(), getFullName(), new LinkedHashMap<String, Object>() {
				{
					put(StockConstants.dataProvider_start_date, getRequest_start_date());
					put(StockConstants.dataProvider_end_date, getRequest_end_date());
					put(StockConstants.dataProvider_dataset_code, getDataProviderCode());
				}
			}, this);
			qd.run();
			setRb(qd);

			setFilter_entity(after_download_entity_hook(qd, remote_processor, local_processor));
		}

		new EventLogger(EventLogger.ENTITY_FILTERING, getIndice(), getUnique_name(), new HashMap<String, String>() {
			{
				put("filter_entity", getFilter_entity().toString());
			}
		});

	}

	/****
	 * 
	 * returns true if this entity is to be blocked.
	 * 
	 * returns false if this entity is allowed to go forward to the calculate
	 * indicators and calculate complexes
	 * 
	 * 1. the request contains its errors in its error object, first check if
	 * there are any errors, and return true if they are there.(the only error
	 * which is managed is the make request call), any other errors are not
	 * handled and will bubble through, causing the whole program to stop.
	 * 
	 * the order of the following events is important in that force redownload
	 * must be checked first, because inside the request function, it breaks out
	 * of the iteration of the response if it finds a difference in the close
	 * prices, so first check force redownload, and then check the equality of
	 * the expected dates.
	 * 
	 * 2. is it force redownload -> if yes, then return true.
	 * 
	 * 3. the request holds whatever day ids it returned. check all expected day
	 * ids to be present. if the expected day ids are not there, log error and
	 * return true
	 * 
	 * 4. log how many new datapoints were got, or whether no datapoints were
	 * got, in which case return true
	 * 
	 * 5. set the derive calculate complexes from
	 * 
	 * 
	 * 
	 * any other things can be done here.
	 * 
	 * @throws Exception
	 * 
	 *             yo
	 * 
	 */
	@Override
	public Boolean after_download_entity_hook(RequestBase rb, BulkProcessor remote_processor,
			BulkProcessor local_processor) throws Exception {
		LogObject lg1 = new LogObject(this.getClass());
		lg1.commit_debug_log("init: after download entity hook");
		// TODO Auto-generated method stub

		/******
		 * check if we want to abort the after download entity hook in the
		 * testings.
		 * 
		 */
		HashMap<String, Object> boo_res = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.skip_after_enitty_download_hook));
		if (boo_res.values().size() == 1 && boo_res.containsValue(true)) {
			return false;
		}

		/*****
		 * 
		 * test code ends.
		 * 
		 */

		if (!rb.getRequest_response_successfull()) {
			// this error is already logged in the request
			// error in the request -> check the logobject logs.
			new EventLogger(EventLogger.ENTITY_DOWNLOAD_ERROR, getIndice(), getUnique_name()).commit();
			return true;
		}

		LogObject lg = new LogObject(getClass(), this);
		if (getForce_redownload().equals(true)) {
			lg.commit_info_log("force redownload:true");
			new EventLogger(EventLogger.ENTITY_FORCE_REDOWNLOAD, getIndice(), getUnique_name()).commit();
			// force redownload was true.
			return true;
		}

		/*****
		 * the theory here is that when we make the request, we ask for some day
		 * ids before whatever we already have. when we get the response from
		 * the dataprovider, it checks to see that the response has these day
		 * ids,(which we already have in teh db), as a means to verify that the
		 * data is not changing.
		 * 
		 * this will not be caught in force_redownload checking in the request,
		 * because force redownload, checking only checks whatever is returned
		 * in the response to be the same as what we have, so if some day id is
		 * not returned at all, it will not catch that.
		 * 
		 * EXCEPTION: if the size of the sortedmap is greater than one, only
		 * then this check is applied. because if the size is 1, that means that
		 * the reqeust start day id and latest existing day id are both the
		 * same, and that happens only when we are requesting data for the first
		 * time. because in any other eventuality, the start date is always the
		 * latest date - (some days), but in case of the first time where the
		 * latest existing date is already 0, the start date is also kept zero,
		 * (ref function; calcuale_request_start_date)
		 * 
		 */
		// System.out.println("came till day ids expected to exist");
		SortedMap<Integer, DataPoint> day_ids_expected_to_exist_in_returned_response = getPrice_map_internal()
				.subMap(getRequest_start_day_id(), true, getLatest_existing_day_id(), true);

		// test code
		HashMap<String, Object> expected_day_id_missing_simulation = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_expected_day_id_is_absent));
		if (boo_res.values().size() == 1 && boo_res.containsValue(true)) {
			// we remove one of the days from the downloaded day ids.
			rb.getAll_downloaded_day_ids().remove(0);
		}
		// test code ends.
		// System.out.println("day ids expected to exist in returned response
		// is:");
		// System.out.println(day_ids_expected_to_exist_in_returned_response);
		if (day_ids_expected_to_exist_in_returned_response.size() > 1) {
			for (Integer day_id : day_ids_expected_to_exist_in_returned_response.keySet()) {
				if (!rb.getAll_downloaded_day_ids().contains(day_id)) {
					// throw an error that some expected day ids were not
					// present in
					// the
					// returned response.
					// if this happens, we should abort everything or go
					// forward?
					// we can send an error log and return false from here, let
					// everything else continue.
					// expected day id was absent.
					new EventLogger(EventLogger.ENTITY_EXPECTED_DATAPOINT_ABSENT, getIndice(), getUnique_name())
							.commit();
					lg.add_to_info_packet("expected_day_id_absent_from_response", day_id);
					lg.commit_error_log("", new Exception("expected day id absent from response"));
					return true;
				} else {
					// for debug
					// lg.add_to_info_packet(day_id.toString(),
					// "present as required");
				}
			}
		}

		if (generate_newly_downloaded_datapoints().size() == 0) {
			// no new datapoints -> so the entity will be filtered from all
			// subsequent things.
			lg.commit_info_log("no new datapoints");
			new EventLogger(EventLogger.ENTITY_NO_NEW_DATAPOINTS, getIndice(), getUnique_name()).commit();
			return true;
		}

		System.out.println("came past the no new datapoinst");
		/********
		 * test method to check if the program came past this point, in certain
		 * test conditions, it is expected to return true and exit out of this
		 * method by this point.
		 * 
		 */
		TestMethodExecutorArgs t_args = new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.after_download_entity_hook_method_triggered);
		t_args.getArguments().put("entity", this);
		TestMethodsExecutor.exec(t_args);

		lg.add_to_info_packet("total_newly_added_points", generate_newly_downloaded_datapoints().size());

		lg.add_to_info_packet("calculate complexes from", generate_newly_downloaded_datapoints().firstKey());

		lg.add_to_info_packet("newest data point", getPrice_map_internal().lastKey());

		lg.commit_info_log("eligible to add to entity map");

		new EventLogger(EventLogger.ENTITY_DOWNLOADED_DATE_AND_DATAPOINTS, getIndice(), getUnique_name(),
				new HashMap<String, String>() {
					{
						put("first newly downloaded day_id",
								generate_newly_downloaded_datapoints().firstKey().toString() + ":"
										+ CentralSingleton.gmt_day_id_to_dataPoint
												.get(generate_newly_downloaded_datapoints().firstKey())
												.getDateString());
						put("last newly downloaded date : datapoint",
								getPrice_map_internal().lastEntry().getValue().getDateString() + ":"
										+ getPrice_map_internal().lastKey());
						put("total newly downloaded datapoints:",
								String.valueOf(generate_newly_downloaded_datapoints().size()));
					}
				}).commit();

		generate_index_to_day_id_mapping();

		System.out.println("came to update stop loss open close arrays");
		update_stop_loss_open_close_arrays();

		HashMap<String, Object> skip_after_update_stop_loss = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(StopLossTests.test_should_skip_part_after_update_stop_loss));
		if (skip_after_update_stop_loss.values().size() == 1 && skip_after_update_stop_loss.containsValue(true)) {
			LogObject lgd = new LogObject(getClass(), this);
			lgd.commit_debug_log("exiting after update_stop_loss ");
			return false;
		}

		// update_last_day_of_week_month_and_year();

		update_price_change_and_stop_loss_arrays_to_es(remote_processor);

		HashMap<String, Object> skip_after_remote_chunk_update = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(ChunkTest.test_should_skip_part_after_update_to_remote));
		remote_processor.flush();
		local_processor.flush();

		if (skip_after_remote_chunk_update.values().size() == 1 && skip_after_remote_chunk_update.containsValue(true)) {
			LogObject lgd = new LogObject(getClass(), this);
			lgd.commit_debug_log("exiting after update to remote chunk ");
			return false;
		}

		// generate_price_change_buckets_and_stddev_array(local_processor,
		// StockConstants.EntityIndex_build_price_buckets);

		LogObject lgd = new LogObject(getClass(), this);
		lgd.commit_debug_log("stopping after committing the flushing the bulk, see the logs.");

		return false;
	}

	/******
	 * this hook is called from
	 * {EntityIndex#build_entity_index_from_elasticsearch}
	 * 
	 * it instantiates the price map internal new. this is done because the
	 * price map internal is jsonignored, so it does not exist in es. each entry
	 * in the es_linked_list is deserialized into a datapoint. then the
	 * {@link #DataPoint#after_build_hook} is called. this is because after the
	 * datapoint is built, its indicator values hashmap needs to be populated
	 * from its dp_linked_list(the linked list which is the es representation of
	 * the indicator_values_hashmap) in the datapoint, the indicator values
	 * hashmap is just like the price map internal here, jsonignored. so we need
	 * to populate it from the dp_linked_list which is not ignored.
	 * 
	 * both hooks work similarly. there are no differences.
	 * 
	 * after the hashmap(price_map_internal in entity and indicator_values_map
	 * in datapoint) are built, their respective linked_lists_ are cleared, to
	 * save memory.
	 * 
	 * 
	 * ONE POINT TO REMEMBER:
	 * 
	 * when a datapoint is newly downloaded we need to instantiate its
	 * indicator_values_on_datapoint hashmap. To solve this problem the hashmap
	 * must be set after the set close method is called in the request class.
	 * 
	 * for datapoints that were already downloaded, in the below function ,the
	 * after_build_hook gets called on them and there the
	 * indicator_values_on_datapoint hashmap is initialized and populated from
	 * the dp linked list.
	 * 
	 * this never happens for newly downloaded datapoints , hence the need to
	 * initialize that hashmap seperately in the request class.
	 * 
	 * 
	 * 
	 */
	@Override
	public void after_build_entity_hook() throws Exception {

		// TODO Auto-generated method stub
		/**
		 * populate the price map treemap with this data.
		 * 
		 */
		this.price_map_internal = new TreeMap<Integer, DataPoint>();
		this.index_to_day_id_mapping = new TreeMap<Integer, Integer>();

		Integer index = 0;
		for (Map<String, Object> map : this.es_linked_list) {
			Integer day_id = (Integer) map.get(StockConstants.Entity_day_id);
			byte[] json;

			json = CentralSingleton.getInstance().objmapper
					.writeValueAsBytes(map.get(StockConstants.Entity_data_point));
			DataPoint dp = CentralSingleton.getInstance().objmapper.readValue(json, DataPoint.class);

			dp.after_build_datapoint_hook();

			this.price_map_internal.put(day_id, dp);
			this.index_to_day_id_mapping.put(index, day_id);
			index++;
		}

		/****
		 * executing test method to ensure es_linked_list is empty.
		 */
		TestMethodExecutorArgs margs = new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.when_es_empty_check_that_entity_linked_list_is_empty);
		margs.getArguments().put("entity", this);
		TestMethodsExecutor.exec(margs);
		/****
		 * test method execution ends.
		 */

		/***
		 * clear the linked list so that we dont hold the whole thing double.
		 * 
		 */
		LogObject lg_2 = new LogObject(getClass(), this);
		lg_2.add_to_info_packet("total datapoints in es:", getEs_linked_list().size());
		this.es_linked_list.clear();

		setLatest_existing_day_id(calculate_latest_existing_day_id());
		setRequest_start_date(calculate_start_date_string_for_request());
		setRequest_end_date(calculate_end_date_for_request());
		
		// this is only if there are already existing datapoints.
		if(getPrice_map_internal().isEmpty()){
			new EventLogger(EventLogger.ENTITY_EXISTING_DATE_AND_DATAPOINTS,getIndice(),
					getUnique_name(),new HashMap<String,String>(){{
							put("existing_price_map_empty","true");
					}}).commit();
		}
		else{
			new EventLogger(EventLogger.ENTITY_EXISTING_DATE_AND_DATAPOINTS, getIndice(),
					getUnique_name(),
					new HashMap<String, String>() {
						{
							put("Oldest date : day_id", getPrice_map_internal().firstEntry().getValue().getDateString()
									+ ":" + getPrice_map_internal().firstKey().toString());
							put("Latest date : day_id", getPrice_map_internal().lastEntry().getValue().getDateString() + ":"
									+ getPrice_map_internal().lastKey().toString());
							put("Request Start Date", getRequest_start_date());
							put("Request End Date", getRequest_end_date());
						}
			}).commit();
		}
		// lg_2.add_to_info_packet("last_existing_date:",
		// getLatest_existing_day_id());
		// lg_2.add_to_info_packet("start datestring for request",
		// getRequest_start_date());
		// lg_2.add_to_info_packet("end datestring fro request",
		// getRequest_end_date());
		// lg_2.add_to_info_packet("start day id for request",
		// getRequest_start_day_id());
		// lg_2.commit_info_log("entity_built_from_es");

	}

	/****
	 * universal before commit hook called before committing the entity to es if
	 * the price map internal is null(as will be when the entity is first
	 * downloaded) then just instantiate an empty es linked list.
	 * 
	 * otherwise, populate the es_linked_list with all the data from the
	 * price_map_internal.
	 * 
	 * 
	 */
	@Override
	public void before_commit_entity_to_elasticsearch() throws Exception {
		// TODO Auto-generated method stub
		LogObject lg = new LogObject(getClass(), this);

		if (getPrice_map_internal() == null) {
			this.es_linked_list = new LinkedList<Map<String, Object>>();
			lg.add_to_info_packet("es_linked_list", "built empty");
		} else {
			for (final Map.Entry<Integer, DataPoint> entry : getPrice_map_internal().entrySet()) {

				entry.getValue().before_commit_to_es_hook();

				// checking whether the map for pairs has anything in the dp
				// linked list or not.

				getEs_linked_list().add(new HashMap<String, Object>() {
					{
						put(StockConstants.Entity_day_id, entry.getKey());
						put(StockConstants.Entity_data_point, entry.getValue());
					}
				});

			}
			lg.add_to_info_packet("es linked list", "built with:" + getEs_linked_list().size() + " elements");
		}

		lg.commit_info_log("init:before commit entity to es");

		LogObject lg_c = new LogObject(getClass(), this);
		lg_c.commit_info_log("completed: before_commit_entity_to_es");
	}

	/****
	 * 
	 * @param id
	 *            : the es_id
	 * @param index_or_update
	 *            : 0 -> index, 1 -> update.
	 */
	@Override
	public Boolean commit_entity_to_elasticsearch(String id, Integer index_or_update) throws Exception {

		before_commit_entity_to_elasticsearch();

		LogObject lg = new LogObject(Entity.class, this);

		if (index_or_update.equals(1)) {
			if (getForce_redownload().equals(false) && generate_newly_downloaded_datapoints().size() > 0) {

				try {
					UpdateResponse response = LocalEs.getInstance()
							.prepareUpdate("tradegenie_titan", "entity", getEntity_es_id())
							.setDoc(CentralSingleton.getInstance().objmapper.writeValueAsBytes(this)).setRefresh(true)
							.execute().actionGet();

					if (response.getId().equals(getEntity_es_id())) {
						lg.commit_info_log("succesfully updated new dataPoints to es");
						after_commit_entity_to_elasticsearch_hook();
						return true;
					} else {
						lg.commit_error_log("update new datapoints to es",
								new Exception("gracefully failed, no es exception, just failed"));
						after_commit_entity_to_elasticsearch_hook();
						return false;
					}
				} catch (Exception e) {
					lg.commit_error_log("update new datapoints to es", e);
					after_commit_entity_to_elasticsearch_hook();
					return false;
				}
			}
			return true;
		} else {
			try {
				IndexResponse response = LocalEs.getInstance().prepareIndex().setType("entity")
						.setIndex("tradegenie_titan").setId(id)
						.setSource(CentralSingleton.getInstance().objmapper.writeValueAsBytes(this)).setRefresh(true)
						.execute().actionGet();
				if (response.getId().equals(id)) {
					lg.commit_info_log("commit:success");
					after_commit_entity_to_elasticsearch_hook();
					return true;
				} else {
					lg.commit_error_log("commit", new Exception("failed, network ok"));
					after_commit_entity_to_elasticsearch_hook();
					return false;
				}
			} catch (Exception e) {
				lg.commit_error_log("commit", e);
				after_commit_entity_to_elasticsearch_hook();
				return false;
			}
		}

	}

	@Override
	public void after_commit_entity_to_elasticsearch_hook() {
		// TODO Auto-generated method stub

	}

	/*******
	 * IMPLEMENTATIONS OF MISCELLANEOUS FUNCTIONS there is an issue here that if
	 * this is the first time, and the latest existing day id, then the generate
	 * newly_downloaded_datapoints, will not have the latest existing day id,
	 * because that is set as zero, but needs to be included in the
	 * calculations. SOLVED BY CHECKING THE LATEST EXISITN DAY ID AGAINST THE
	 * FIRST DAY IN THE CENTRALSINGLETON DAY ID MAD.
	 * 
	 * returns the price map internal starting from the first newly added day to
	 * the last one whatever it is. uses Tailmap. called from the
	 * {@link #after_download_entity_hook(RequestBase)} called from the }
	 * 
	 */
	@Override
	public SortedMap<Integer, DataPoint> generate_newly_downloaded_datapoints() {
		if (getLatest_existing_day_id().equals(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey())) {
			return getPrice_map_internal().tailMap(getLatest_existing_day_id(), true);
		} else {
			return getPrice_map_internal().tailMap(getLatest_existing_day_id(), false);
		}
	}

	/****
	 * breaks array into chunks of
	 * {@link StockConstants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays}
	 * compresses them, based on whether they are sorted or unsorted. sends the
	 * chunks to elasticsearch,
	 * 
	 * chunks are zero padded in case the length of the chunk is less than the
	 * above size, but this will not matter since, those indices will never be
	 * queried for. the said compressed chunks are then added to the bulk
	 * processor, as updaterequests.
	 * 
	 * 
	 * 
	 * @step_one: THIS is for the open_open_price_change arrays only. use the
	 *            {@link #index_of_first_newly_added_close_close_pair}, to
	 *            figure out from where to make the chunks
	 * 
	 * 
	 * @nomenclature_of_chunk: entity_es_id -
	 *                         ("c"->common_chunk_prefix)(chunk_id->id of
	 *                         chunk)-(n->array_id)
	 * 
	 *                         arrays are ordered as per whatever is the normal
	 *                         return order from
	 *                         {@link PairArray#get_names_of_arrays_applicable_to_entity(Entity)}
	 * 
	 *                         so the array_id just starts with 0 for the first
	 *                         array and then increments.
	 * 
	 * @param remote_processor
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public void update_price_change_and_stop_loss_arrays_to_es(BulkProcessor remote_processor) throws Exception {

		new EventLogger(EventLogger.ENTITY_UPDATING_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_TO_ES, getIndice(),
				getUnique_name()).commit();

		HashMap<String, Object> boo_res = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(ChunkTest.test_basic_chunk_dechunk));

		if (boo_res.values().size() == 1 && boo_res.containsValue(true)) {
			TestMethodsExecutor.modify_all_price_change_and_stop_loss_arrays_for_basic_chunk_dechunk_test(this);
		}

		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("init: update price change arr to es");

		setStop_loss_and_price_change_arrays_with_first_modified_chunk_id(new HashMap<String, String>());

		// as long as some indices have been downloaded.
		if (getIndices_of_first_and_last_newly_added_pairs() != null) {

			// since we have added some new pairs.
			// we calculate the id of the first chunk that has been added to.
			// basically we divided the large open_open and close_close arrays
			// into
			// chunks
			// and based on which pair was newly downloaded, we can get the
			// chunk
			// id of the first chunk that changed.
			int first_chunk_id = getIndices_of_first_and_last_newly_added_pairs()
					.get(StockConstants.Entity_first_newly_added_pair_index)
					/ StockConstants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays;

			// same for last_chunk_id
			int last_chunk_id = getIndices_of_first_and_last_newly_added_pairs()
					.get(StockConstants.Entity_last_newly_added_pair_index)
					/ StockConstants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays;

			LogObject lgd1 = new LogObject(getClass(), this);

			// just get names of the large open and close arrays.
			// we need these to check in the subsequent loops if these arrays
			// are
			// the one being iterated since it changes the way the chunks are
			// made.
			ArrayList<String> open_close_price_change_arrays = new ArrayList<String>(
					Arrays.asList(getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
							getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));

			// names of all the stop loss, open_open, close_close price change
			// arrays
			// it includes the above two as well.
			ArrayList<String> names_of_price_change_and_stop_loss_arrays_and_last_day_month_week_arrays = PairArray
					.get_names_of_arrays_applicable_to_entity(this);

			Integer counter = 0;

			// last chunk whatever it was,
			// that plus + chunk_size
			// eg : 0 , and chunk size is 300 ->
			// it will 0-299.
			// so if event logger is there, then we can log.

			// iterate the names got above
			for (final String arrN : names_of_price_change_and_stop_loss_arrays_and_last_day_month_week_arrays) {

				LogObject lgd2 = new LogObject(getClass(), this);
				lgd2.commit_debug_log("doing array:" + arrN);

				// get the decompressed array, based on whether it is a large
				// open_open
				// array or a normal small array.
				int[] decompressed_arr = PairArray.get_decompressed_arr(arrN,
						(open_close_price_change_arrays.contains(arrN) ? 0 : 1),

						(open_close_price_change_arrays.contains(arrN)
								? CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
										* StockConstants.Entity_total_pairs_for_a_day
								: CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size())

				);

				LogObject lgd3 = new LogObject(getClass(), this);
				lgd3.commit_debug_log("decompressed array size is:" + decompressed_arr.length);

				// the chunk size is decided based on whether it is a large
				// array
				// or a small array.
				int chunk_size = open_close_price_change_arrays.contains(arrN)
						? StockConstants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays
						: CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size();

				LogObject lgd5 = new LogObject(getClass(), this);
				lgd5.commit_debug_log("chunk size determined as:" + chunk_size);

				// if it is a large array, then we will go from the first chunk
				// id determined above
				// till the last chunk id.
				// if it is a small array, then there is only one chunk and that
				// is the
				// whole array.
				for (int i = (open_close_price_change_arrays.contains(arrN) ? first_chunk_id
						: 0); i <= (open_close_price_change_arrays.contains(arrN) ? last_chunk_id : 0); i++) {

					LogObject lgd4 = new LogObject(getClass(), this);

					int[] arr = Arrays.copyOfRange(decompressed_arr, i * chunk_size, (i + 1) * chunk_size);

					final int start_index = i * chunk_size;

					// System.out.println("the array size is:" + arr.length);
					// System.out.println("the array type is:" +
					// (open_close_price_change_arrays.contains(arrN)));
					// System.out.println("array name is:" + arrN);
					final int[] carr = PairArray.get_compressed_int_array(arr,
							(open_close_price_change_arrays.contains(arrN) ? 0 : 1));

					String es_id = getEntity_es_id() + StockConstants.Result_Hyphen + StockConstants.Entity_chunk_prefix
							+ String.valueOf(i) + StockConstants.Result_Hyphen + String.valueOf(counter);

					// lgd4.commit_debug_log("chunk id:" + i + " start index:" +
					// start_index + " end index "
					// + (i + 1) * chunk_size + " expected length: " +
					// chunk_size + " and observerd length: "
					// + arr.length + " compressed length: " + carr.length + "
					// and es id:" + es_id);

					/***
					 * 
					 * populating the map that will be used to make the remote
					 * request with all the modified first chunk ids. basically
					 * if the map contains the array key then we do nothing.
					 * otherwise it is obviously the first time so we populate
					 * it with the array name as key and the start index as the
					 * value.
					 * 
					 * ---------------------------------------------------------
					 * 21/4/2018 : this hashmap is now used only for the event
					 * logger what it does is that the name of the array carries
					 * the start_index of the first chunk which was sent to
					 * remote.
					 * 
					 * and we add another_entry : name_last_index : the
					 * start_index of the last chunk that was
					 * 
					 */
					if (!getStop_loss_and_price_change_arrays_with_first_modified_chunk_id().containsKey(arrN)) {
						getStop_loss_and_price_change_arrays_with_first_modified_chunk_id().put(arrN,
								String.valueOf(start_index));
					}

					// and also the last index ?
					// should we also add that ?

					UpdateRequest ureq = new UpdateRequest("tradegenie_titan", StockConstants.ESTypes_arrays, es_id);

					// if we are adding the last chunk, then give it the
					// latest_newly_added_pair_index
					// otherwise we don't add that.
					if (i == last_chunk_id) {
						getStop_loss_and_price_change_arrays_with_first_modified_chunk_id().put(arrN + "_last_chunk",
								String.valueOf(start_index));

						ureq.script("update_chunks_script", ScriptType.FILE, new HashMap<String, Object>() {
							{
								put("arr", carr);
								put("start_index", start_index);
								put("arr_name", arrN);
								put("latest_newly_added_pair_index",
										Entity.this.getIndices_of_first_and_last_newly_added_pairs()
												.get(StockConstants.Entity_last_newly_added_pair_index));
							}
						});
					} else {
						ureq.script("update_chunks_script", ScriptType.FILE, new HashMap<String, Object>() {
							{
								put("arr", carr);
								put("start_index", start_index);
								put("arr_name", arrN);
								put("latest_newly_added_pair_index", null);
							}
						});
					}

					ureq.scriptedUpsert(true);
					ureq.upsert(new HashMap<String, Object>());
					remote_processor.add(ureq);
				}
				counter++;
			}

		} else {
			LogObject lg1 = new LogObject(getClass(), this);
			if (generate_newly_downloaded_datapoints().size() == 0) {
				lg1.commit_info_log("no index of first pair found, expected");
			} else {
				new EventLogger(EventLogger.ENTITY_FIRST_NEWLY_DOWNLOADED_PAIR_INDEX_MISSING, getIndice(),
						getUnique_name()).commit();
				lg1.commit_error_log(
						"newly downloaded datapoints found but index of first newly added close close price change is null",
						new Exception());
			}
		}

		if (!getStop_loss_and_price_change_arrays_with_first_modified_chunk_id().isEmpty()) {
			new EventLogger(EventLogger.ENTITY_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_CHUNKS_UPDATED, getIndice(),
					getUnique_name(), getStop_loss_and_price_change_arrays_with_first_modified_chunk_id()).commit();
		}

	}

	@Override
	public void update_t_test_double_arrays() {
		// TODO Auto-generated method stub
		LogObject lg_i = new LogObject(getClass(), this);
		lg_i.commit_info_log("init:update_ttst_arrs");
		PairArray.generate_t_test_double_arrays(this);
		LogObject lg_c = new LogObject(getClass(), this);
		lg_c.commit_info_log("complete:update_ttst_arrs");
	}

	@Override
	public void update_standard_deviation_and_mean_for_indicator() throws Exception {

		for (Integer day_id : getPrice_map_internal().keySet()) {

			for (Map.Entry<String, ArrayList<String>> entry : getEntity_initial_stub_names().entrySet()) {

				for (String stub_name : entry.getValue()) {
					// build a tick list out of each datapoint.

				}

			}
		}

	}

	/***
	 * just checks whether we have enough datapoints, inclusive of the end day
	 * id to do the indicator calculation.
	 * 
	 * @param period
	 * @param end_day_id
	 * @return
	 */
	public Boolean sufficient_datapoints_for_indicator_calculation(Integer period, Integer end_day_id,
			String indicator_stub_name) {
		if (indicator_stub_name == "acceleration_decelration_indicator_period_start_5_34_period_end") {
			System.out.println("period coming in is:" + period);
			System.out.println("end day id is:" + end_day_id);
			// this is a headmap
			// that means that it does not include the end_day_id
			// that is why i have added one to it.
			System.out.println("price map internal headmap size is:");
			System.out.println(getPrice_map_internal().headMap(end_day_id).size());
			System.out.println("will return");
			System.out.println((getPrice_map_internal().headMap(end_day_id).size() + 1) >= period);
		}
		return (getPrice_map_internal().headMap(end_day_id).size() + 1) >= period;

	}

	/****
	 * this just minuses from the end day id, a number equal to the difference
	 * to get a start day id. and generates a submap from the price_map_internal
	 * given the start and end day ids.
	 * 
	 * 
	 * @param end_day_id
	 * @param difference
	 * @param end_day_id_inclusive
	 * @return
	 */
	@Override
	public SortedMap<Integer, DataPoint> generate_price_map_in_range(Integer end_day_id, Integer difference,
			Boolean end_day_id_inclusive) {

		// we basically want to short circuit this to just have the end day id
		// minus the difference
		// whatever start day id that gives is what we take.
		Integer start_day_id = null;
		start_day_id = end_day_id - difference;

		//// EARLIER THING WHICH WE NOW CHANGED.
		// Integer position_of_end_day_id =
		//// getPrice_map_internal().headMap(end_day_id).size();
		// Integer position_of_start_day_id = position_of_end_day_id -
		//// difference;
		// if (position_of_start_day_id < 0) {
		// start_day_id = getIndex_to_day_id_mapping().get(0);
		// } else {
		// start_day_id =
		//// getIndex_to_day_id_mapping().get(position_of_start_day_id);
		// }

		// here we add the test code.

		return getPrice_map_internal().subMap(start_day_id, true, end_day_id, end_day_id_inclusive);
	}

	/***
	 * counts back from the end day id, all available day ids, till it moves by
	 * (difference) number of days then if the resulting start_day position is
	 * less than zero -> returns null. otherwise returns the day id at
	 * (end_day_position - difference) key in the index mapping map.
	 * 
	 * 
	 */
	@Override
	public Integer generate_day_id_n_days_before_given_day_id(Integer end_day_id, Integer difference) {
		// TODO Auto-generated method stub
		Integer position_of_end_day_id = getPrice_map_internal().headMap(end_day_id).size();
		Integer position_of_start_day_id = position_of_end_day_id - difference;
		Integer start_day_id = null;
		if (position_of_start_day_id < 0) {
			return null;
		} else {

			return getIndex_to_day_id_mapping().get(position_of_start_day_id);
		}

	}

	/*****
	 * for any given datapoint 1. if any of [close|open|high|low|volume] of
	 * these is not null, i.e it is set, and it is less than zero, then it will
	 * remove that datapoint.
	 * 
	 * why do we remove and not throw an error. the logic is simple: 1. for
	 * volume if it is zero, then it means there was no trading , so we can just
	 * remove the datapoint 2. for other values, for eg: close, if it is zero,
	 * it may be because all things are zero on that day, in that case, we just
	 * remove it. when we do the next download, suppose that if a non-zero value
	 * comes for the same attribute, force redownload will be triggered anyways.
	 * so instead of throwing an error in the first occurrence, we prefer to let
	 * it happen on a subsequent download.
	 */
	@Override
	public void prune_newly_downloaded_datapoints() {

		HashMap<String, Object> simulate_volume_zero_prune = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.simulate_force_redownload_due_close_zero));

		if (simulate_volume_zero_prune.values().size() == 1 && simulate_volume_zero_prune.containsValue(true)) {
			LogObject lgd = new LogObject(getClass(), this);
			lgd.commit_debug_log("exiting after pruning. ");

		}

		for (Iterator<Map.Entry<Integer, DataPoint>> it = getPrice_map_internal().entrySet().iterator(); it
				.hasNext();) {
			Entry<Integer, DataPoint> entry = it.next();
			Integer day_id = entry.getKey();
			DataPoint dp = entry.getValue();
			if (day_id > getLatest_existing_day_id()) {
				if (dp.close_is_less_than_or_equal_to_zero() || dp.open_is_less_than_or_equal_to_zero()
						|| dp.high_is_less_than_or_equal_to_zero() || dp.low_is_less_than_or_equal_to_zero()
						|| dp.volume_is_less_than_or_equal_to_zero()) {
					LogObject lg = new LogObject(DataPoint.class);
					lg.commit_info_log("removed datapoint at day_id, " + "due to less than zero:" + day_id);
					it.remove();
				}
			}
		}
	}

	/*******
	 * THIS IS TO ANSWER QUESTIONS LIKE:
	 * 
	 * - WHAT HAPPENS WHEN RELIANCE FALLS THE PREVIOUS WEEK. - WHAT HAPPENS WHEN
	 * RELIANCE FALLS THE PREVIOUS MONTH, BUT RISES THE PREVIOUS WEEK.
	 * 
	 * 
	 * the last day in the price map is never processed. this is because of the
	 * methodology:
	 * 
	 * what it does is for any given day id. it checks whether the previous day
	 * id has a different(week/month/year)id than the present day id. if yes,
	 * then the PREVIOUS day id is given the status of last day of week or month
	 * or year.
	 * 
	 * so for the last day id it will not be processed.
	 * 
	 * hence when calculating the consecutive and previous, we use the special
	 * {@link Complex}
	 * 
	 * 
	 * 
	 */
	@Override
	public void update_last_day_of_week_month_and_year() throws Exception {
		// TODO Auto-generated method stub
		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("init:update_last_day");

		/***
		 * variables for debug logs
		 * 
		 */

		Integer previous_day_id = null;
		Integer previous_month_id = null;
		Integer previous_year_id = null;
		Integer previous_week_id = null;

		/***
		 * int[] for storing the correlation between each day id and the last
		 * day of the previous week as well as the last day of the previous
		 * month.
		 * 
		 * 
		 * 
		 * 
		 * basically the array starts from day id 0 and ends till whatever we
		 * have as the last existing day id so the value at the index() will be
		 * the day id of the last day of the previous week or month.
		 * 
		 * till the first day id that exists for this stock, -1 is inserted into
		 * both arrays. if no previous day id is available still -1 is inserted.
		 * 
		 * eg. to get the day id of the last day of the previous week for day id
		 * 10. go to index (10) in the arr. and get the value.
		 * 
		 * since our day ids start from zero, we can just go directly to the
		 * index and get the value. s to derive these values in the below
		 * function, the following logic is used:
		 * 
		 * @rule_one:if for a particular day, there is no previous day, then -1
		 *              is set at that index
		 * 
		 * @rule_two : whethere there is a change or not, the last element in
		 *           the #last_day_of_week_ids is set at the index of the day id
		 *           being processed.
		 * 
		 * 
		 *           these are later converted into int[] and stored into the
		 *           same map which holds the stop_loss_arrays in the central
		 *           singleton.
		 * 
		 *           they are thus committed into es the same way as the rest of
		 *           the stop loss arrays.
		 * 
		 */

		for (Map.Entry<Integer, DataPoint> entry : getPrice_map_internal().entrySet()) {

			if (previous_day_id == null) {

			} else {

				/*****
				 * 
				 * for week. so previous day id is set only for those days when
				 * the next day with a price is a monday, but the problem is
				 * here. what if the next day with a price is a tuesday?? THIS
				 * WILL FAIL.
				 * 
				 * so as long as it is anyday, provided that the previous
				 * datapoint has a different week id.
				 * 
				 */

				if (previous_week_id.intValue() != entry.getValue().getWeek_of_year().intValue()) {
					getPrice_map_internal().get(previous_day_id).setLast_day_of_week(1);

				} else {
					getPrice_map_internal().get(previous_day_id).setLast_day_of_week(0);
				}

				// last_day_of_week_for_each_day_id[entry.getKey()] =
				// last_day_of_week_ids
				// .isEmpty() ? -1 : last_day_of_week_ids
				// .get(last_day_of_week_ids.size() - 1);

				// System.out.println("the last day of week for day id:"
				// + entry.getKey() + " is :" + previous_day_id);
				// System.out.println("the week array now looks like");
				// print_array(last_day_of_week_for_each_day_id);
				// System.in.read();

				/****
				 * for month
				 * 
				 */
				if (previous_month_id.intValue() != entry.getValue().getMonth_of_year_integer().intValue()) {
					getPrice_map_internal().get(previous_day_id).setLast_day_of_month(1);

				} else {
					getPrice_map_internal().get(previous_day_id).setLast_day_of_month(0);
				}
				// last_day_of_month_for_each_day_id[entry.getKey()] =
				// last_day_of_month_ids
				// .isEmpty() ? -1 : last_day_of_month_ids
				// .get(last_day_of_month_ids.size() - 1);

				/****
				 * for year.
				 * 
				 */
				if (previous_year_id.intValue() != entry.getValue().getYear().intValue()) {

					getPrice_map_internal().get(previous_day_id).setLast_day_of_year(1);

				} else {
					getPrice_map_internal().get(previous_day_id).setLast_day_of_year(0);

				}

			}

			previous_day_id = entry.getKey();

			previous_month_id = entry.getValue().getMonth_of_year_integer();

			previous_year_id = entry.getValue().getYear();

			previous_week_id = entry.getValue().getWeek_of_year();

		}

		// building the two sorted arrays which contain the last day of the prev
		// week and month for each day id.
		// for this entity, and then putting them into the query time lookup
		// map, using the pair_array put_arr function.
		// the very first day has no setting for last_day_of_week or month, on
		// its datapoint.
		// hence the not null check.
		Integer det_last_day_of_week = -1;
		Integer det_last_day_of_month = -1;

		// these are to be got from centralsingleton.
		// pairarray function needs to be added.

		// int[] last_day_of_prev_week_for_each_day_id =
		// new int[CentralSingleton.getInstance().gmt_day_id_to_dataPoint
		// .size()];
		int[] last_day_of_prev_week_for_each_day_id = PairArray.get_decompressed_arr(
				getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_week, 1,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		int[] last_day_of_prev_month_for_each_day_id = PairArray.get_decompressed_arr(
				getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_month, 1,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		for (int i = 0; i < CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size(); i++) {

			last_day_of_prev_week_for_each_day_id[i] = det_last_day_of_week;
			last_day_of_prev_month_for_each_day_id[i] = det_last_day_of_month;

			// if the day has a price.
			// and it is the last day of the week.
			// then it is the determined last day of the week.
			// and it is set in the loop in above lines to all the days that
			// come
			// till it changes here again
			if (getPrice_map_internal().containsKey(i) && getPrice_map_internal().get(i).getLast_day_of_week() != null
					&& getPrice_map_internal().get(i).getLast_day_of_week().intValue() == 1) {
				det_last_day_of_week = i;
			}
			if (getPrice_map_internal().containsKey(i) && getPrice_map_internal().get(i).getLast_day_of_month() != null
					&& getPrice_map_internal().get(i).getLast_day_of_month().intValue() == 1) {
				det_last_day_of_month = i;
			}

		}

		PairArray.put_arr(last_day_of_prev_week_for_each_day_id,
				getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_week, 1);
		PairArray.put_arr(last_day_of_prev_month_for_each_day_id,
				getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_month, 1);

		LogObject lgi2 = new LogObject(getClass(), this);
		lgi2.commit_info_log("completed: update last days");

		// so what happens when we do update?

	}

	/***
	 * static method used from entityindex to get the es id of an entity given
	 * the subindicator complex name
	 * 
	 * @param complex_id
	 * @return
	 */
	public static String get_entity_id_from_complex_id(String complex_id) {
		return complex_id.substring(0, complex_id.indexOf(StockConstants.Result_Hyphen));
	}

	public void print_array(int[] arr) {
		System.out.println("array length is:" + arr.length);
		System.out.println("array components are:");
		String components = "";
		for (int i = 0; i < arr.length; i++) {
			components += String.valueOf(arr[i]) + ",";
		}
		System.out.println(components);
	}

	@Override
	public boolean equals(Object obj) {
		Entity other_entity = (Entity) obj;
		if (other_entity.getPrice_map_internal().size() != getPrice_map_internal().size()) {
			return false;
		}
		for (Map.Entry<Integer, DataPoint> other_entry : other_entity.getPrice_map_internal().entrySet()) {
			DataPoint self_dp = getPrice_map_internal().get(other_entry.getKey());
			if (self_dp == null) {
				return false;
			} else if (!self_dp.equals(other_entry.getValue())) {
				return false;
			}
		}
		return true;
	}

	/***
	 * checks three things 1) price map internal is not empty 2) datapoints are
	 * not null in price map internal 3) none of the datapoint attributes are
	 * null.
	 * 
	 * @param obj
	 * @return
	 */
	public boolean equals_and_not_empty(Object obj) {
		Entity other_entity = (Entity) obj;
		if (other_entity.getPrice_map_internal().size() != getPrice_map_internal().size()
				|| other_entity.getPrice_map_internal().isEmpty() || getPrice_map_internal().isEmpty()) {
			LogObject lgd = new LogObject(this.getClass());

			lgd.commit_error_log(
					"compared entities unequal because" + " price map internal sizes differ" + this.getFullName(),
					new Exception());
			return false;
		}
		for (Map.Entry<Integer, DataPoint> other_entry : other_entity.getPrice_map_internal().entrySet()) {
			DataPoint self_dp = getPrice_map_internal().get(other_entry.getKey());
			if (self_dp == null) {
				return false;
			} else if (!self_dp.equals_and_not_null(other_entry.getValue())) {
				LogObject lgd = new LogObject(this.getClass());
				lgd.commit_error_log("compared entities unequal" + " because datapoint differ" + this.getFullName(),
						new Exception());
				return false;
			}
		}
		return true;
	}

	@Override
	public void add_datapoint_to_price_map_internal(DataPoint dp, Integer day_id) {
		// TODO Auto-generated method stub

		getPrice_map_internal().put(day_id, dp);
	}

	@Override
	public void before_complex_calculate() throws Exception {
		// TODO Auto-generated method stub
		setTotal_day_ids_before_day_id(new HashMap<Integer, Integer>());
		setDay_id_and_its_location_in_price_map_internal(new HashMap<Integer, Integer>());
		Integer days_encountered = 0;
		for (Map.Entry<Integer, DataPoint> entry : getPrice_map_internal().entrySet()) {
			Integer day_id = entry.getKey();
			getTotal_day_ids_before_day_id().put(day_id, days_encountered);
			getDay_id_and_its_location_in_price_map_internal().put(days_encountered, day_id);
			days_encountered++;
		}
	}

	@Override
	public Boolean has_days_before(Integer days, Integer day_id) throws Exception {
		// TODO Auto-generated method stub
		// return getTotal_day_ids_before_day_id().get(day_id) >= days;
		Integer first_day_id = getPrice_map_internal().firstKey();
		return (day_id - first_day_id) >= days;
	}

	/****
	 * required : which day is present x days before the given day id. here we
	 * want x datapoints actual. so step one: the index of this day_id in the
	 * price map, got by getting how many days are there before it total, in the
	 * price map step two: from that index - x now get the day id stored at that
	 * index in the location -> day_id hashmap.
	 * 
	 * CURRENTLY JUST RETURN THE NUMERICAL DAY ID , N DAYS BEFORE THIS DAY ID SO
	 * IF CURRENT IS 10, AND DAYS BEFORE IS 8, JUST RETURN 2
	 * 
	 */
	@Override
	public Integer get_day_n_days_before(Integer days, Integer day_id) throws Exception {
		// TODO Auto-generated method stub
		/***
		 * Integer day_id_index = getTotal_day_ids_before_day_id().get(day_id);
		 * Integer days_before_index = day_id_index - days; return
		 * getDay_id_and_its_location_in_price_map_internal().get(
		 * days_before_index);
		 ***/
		return day_id - days;
	}

	/****
	 * returns true if the indicator is applicable to the entity
	 * 
	 * 
	 */
	@Override
	public Boolean indicator_applicable_to_entity(String indicator_name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeMap<Integer, DataPoint> get_last_n_datapoints(Integer day_id, Integer n, Boolean inclusive)
			throws Exception {

		// the map that will be returned.
		TreeMap<Integer, DataPoint> price_map_section = new TreeMap<Integer, DataPoint>();

		// get a map whose keys are less than or equal to the provided day id,
		// depending
		// upon inclusive or not.
		NavigableMap<Integer, DataPoint> nv = getPrice_map_internal().headMap(day_id, inclusive);

		// get keys of the above map in descending order.
		NavigableSet<Integer> nv_set = nv.descendingKeySet();

		// get the iterator for that set.
		Iterator<Integer> nv_set_iterator = nv_set.descendingIterator();

		// iterate till the price map has n elements. added to it.
		while (nv_set_iterator.hasNext()) {
			final Integer curr_day_id = nv_set_iterator.next();
			price_map_section.put(curr_day_id, getPrice_map_internal().get(curr_day_id));
			if (price_map_section.size() == n) {
				break;
			}
		}

		return price_map_section;
	}

	@Override
	public void price_map_internal_to_timeseries() {
		// TODO Auto-generated method stub
		ArrayList<Tick> ts_holder = new ArrayList<Tick>();

		for (int i = getPrice_map_internal().firstKey(); i <= getPrice_map_internal().lastKey(); i++) {

			Tick tick = new Tick(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.get(i).dateString_to_zdt(),
					Decimal.valueOf(get_ohlcv("open", i)), Decimal.valueOf(get_ohlcv("high", i)),
					Decimal.valueOf(get_ohlcv("low", i)), Decimal.valueOf(get_ohlcv("close", i)),
					Decimal.valueOf(get_ohlcv("volume", i)));
			if (getPrice_map_internal().containsKey(i)) {
				ts_holder.add(tick);
			}
		}

		this.ts = new TimeSeries(ts_holder);
	}

	@Override
	public String get_ohlcv(String type, Integer day_id) {
		BigDecimal indicator_val = null;

		if (this.getPrice_map_internal().containsKey(day_id)) {
			switch (type) {

			case "open":
				indicator_val = this.getPrice_map_internal().get(day_id).getOpen();
				break;

			case "high":
				indicator_val = this.getPrice_map_internal().get(day_id).getHigh();
				break;

			case "low":
				indicator_val = this.getPrice_map_internal().get(day_id).getLow();
				break;

			case "close":
				indicator_val = this.getPrice_map_internal().get(day_id).getClose();
				break;

			case "volume":
				indicator_val = this.getPrice_map_internal().get(day_id).getVolume();
				break;
			}

			if (indicator_val == null) {
				return "NaN";
			} else {
				return indicator_val.toString();
			}
		} else {
			return "NaN";
		}

	}

	/****
	 * given an indicator name and a indicator_cum_period_name sets the
	 * indicator_name on the initial stub names map. and adds the
	 * indicator_cum_period_name if it is not present.
	 */
	@Override
	public void add_indicator_cum_period_name_to_initial_stub_names(String indicator_name,
			String indicator_cum_period_name) {
		// TODO Auto-generated method stub

		if (getEntity_initial_stub_names().containsKey(indicator_name)) {

			if (!getEntity_initial_stub_names().get(indicator_name).contains(indicator_cum_period_name)) {
				getEntity_initial_stub_names().get(indicator_name).add(indicator_cum_period_name);
			}
		} else {
			getEntity_initial_stub_names().put(indicator_name,
					new ArrayList<String>(Arrays.asList(indicator_cum_period_name)));
		}

	}

	@Override
	public Integer day_id_index(Integer day_id) {
		// TODO Auto-generated method stub

		if (day_id == 8) {
			System.out.println("day id coming in is 8");
			System.out.println("returning day id index as, should be 5");
			System.out.println(getPrice_map_internal().headMap(day_id).size());
			System.out.println(getPrice_map_internal().headMap(day_id).keySet());
		}

		return getPrice_map_internal().headMap(day_id).size();

	}

	@Override
	public Integer datapoints_upto_current_day_id(Integer day_id, Boolean inclusive) {
		Integer datapoints_less_than_current_day_id = day_id_index(day_id);
		if (inclusive) {
			return datapoints_less_than_current_day_id + 1;
		}
		return datapoints_less_than_current_day_id;
	}

	@Override
	public Integer period_for_indicator_calculation(Integer day_id) {
		return day_id - getPrice_map_internal().firstKey() + 1;
	}

	@Override
	public ArrayList<Integer> check_price_change_arrays_for_event_logging(int[] close_close, int[] open_open) {
		// TODO Auto-generated method stub

		ArrayList<Integer> last_existing_pairs_for_each_day_id = new ArrayList<Integer>();

		ArrayList<Integer> last_existing_pairs_for_small_arrays = new ArrayList<Integer>();

		Integer latest_existing_day_id = getLatest_existing_day_id();
		// now we want to make the pairs for all this.
		for (Integer day_id : getPrice_map_internal().keySet()) {
			if (day_id < latest_existing_day_id) {

				PairArray p = null;

				if (latest_existing_day_id - day_id > StockConstants.Entity_total_pairs_for_a_day) {
					p = PairArray.get_int_array_index_given_start_and_end_day_id(day_id,
							day_id + StockConstants.Entity_total_pairs_for_a_day, this);
				} else {
					p = PairArray.get_int_array_index_given_start_and_end_day_id(day_id, latest_existing_day_id, this);
				}
				last_existing_pairs_for_each_day_id.add(p.getPair_difference_array_index());
				last_existing_pairs_for_small_arrays.add(p.getStart_day_id_based_index());
			}
		}

		for (Integer last_existing_pair_index : last_existing_pairs_for_each_day_id) {
			if (last_existing_pairs_for_each_day_id.contains(last_existing_pair_index + 1)) {

			} else {
				int index_to_check = last_existing_pair_index + 1;
				// now this index should be 99999 on both the incoming arrays.
				// because that pair difference cannot yet have been calculated.
				// the last_existing_pair_index may or may not be 99999, depends
				// on whether
				// that last day id exists or it is just a numerical existence.

				if ((close_close[index_to_check] != PairArray.default_value_for_large_array_indices)) {
					new EventLogger(EventLogger.ENTITY_PRICE_CHANGE_ARRAYS_BEFORE_CALCULATION, getIndice(),
							getUnique_name(), new HashMap<String, String>() {
								{
									put("all pairs as expected", "false");
								}
							}).commit();
				} else if (open_open[index_to_check] != PairArray.default_value_for_large_array_indices) {
					new EventLogger(EventLogger.ENTITY_PRICE_CHANGE_ARRAYS_BEFORE_CALCULATION, getIndice(),
							getUnique_name(), new HashMap<String, String>() {
								{
									put("all pairs as expected", "false");
								}
							}).commit();
				}
			}
		}

		return null;

	}

	@Override
	public void compare_stop_loss_arrays_for_event_logging(HashMap<String, int[]> arrays_before,
			HashMap<String, int[]> arrays_after) {
		HashMap<String, String> changes = new HashMap<String, String>();
		for (String arr_name : arrays_before.keySet()) {

			int[] arr_before = arrays_before.get(arr_name);
			int[] arr_after = arrays_after.get(arr_name);

			// i want to know how many things have changed in these two arrays.
			int change_count = 0;

			for (int i = 0; i < arr_before.length; i++) {
				if (arr_after[i] != arr_before[i]) {
					change_count++;
				}
			}
			changes.put(arr_name, String.valueOf(change_count));
		}

		new EventLogger(EventLogger.ENTITY_STOP_LOSS_ARRAYS_CHANGES, getIndice(), getUnique_name(), changes).commit();

	}

}
