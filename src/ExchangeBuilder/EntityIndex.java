package ExchangeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkProcessor.Listener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.ScriptService.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tradegenie.tests.DownloadEntityExchangeTests;

import ComplexProcessor.BlockingThreadPoolExecutor;
import ComponentsProvider.ForexComponents;
import ComponentsProvider.MetalsComponents;
import ComponentsProvider.OilComponents;
import ComponentsProvider.WikipediaScraper;
import Indicators.CalculateIndicators;
import RemoteComms.JobStatusResponse;
import Titan.ReadWriteTextToFile;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import subindicators.CalculationTask;
import subindicators.Complex;
import subindicators.ForTheMonthWeekYearQuarter;
import subindicators.IndicatorValueChangeStandardDeviation;
import subindicators.NDayHighLow;
import subindicators.Pattern;
import subindicators.RiseFallAmount;
import subindicators.SmaCross;
import subindicators.StandardDeviation;
import yahoo_finance_historical_scraper.StockConstants;

public class EntityIndex implements IEntityIndex {

	/****
	 * when shutdown bulk processor is called. this field is set. it means that
	 * both the local and remote bulk processors were successfully shutdown they
	 * are used in
	 * {@link ExchangeBuilderPackageCoordinator#download_all_exchanges()} to
	 * check whether the bulk processor for the previous exchange could be
	 * successfully stopped or not. if not, then we dont go forward.
	 * 
	 */
	private Boolean bulk_processors_successfully_shutdown;

	public Boolean getBulk_processors_successfully_shutdown() {
		return bulk_processors_successfully_shutdown;
	}

	public void setBulk_processors_successfully_shutdown(Boolean bulk_processors_successfully_shutdown) {
		this.bulk_processors_successfully_shutdown = bulk_processors_successfully_shutdown;
	}

	private String price_and_stop_loss_arr_update_job_id_received;

	public String getPrice_and_stop_loss_arr_update_job_id_received() {
		return price_and_stop_loss_arr_update_job_id_received;
	}

	public void setPrice_and_stop_loss_arr_update_job_id_received(
			String price_and_stop_loss_arr_update_job_id_received) {
		this.price_and_stop_loss_arr_update_job_id_received = price_and_stop_loss_arr_update_job_id_received;
	}

	private String price_and_stop_loss_arr_update_job_id_sent;

	public String getPrice_and_stop_loss_arr_update_job_id_sent() {
		return price_and_stop_loss_arr_update_job_id_sent;
	}

	public void setPrice_and_stop_loss_arr_update_job_id_sent(String price_and_stop_loss_arr_update_job_id_sent) {
		this.price_and_stop_loss_arr_update_job_id_sent = price_and_stop_loss_arr_update_job_id_sent;
	}

	private String index_name;

	public String getIndex_name() {
		return index_name;
	}

	public void setIndex_name(String index_name) {
		this.index_name = index_name;
	}

	private Integer index_code_in_stockConstants;

	public Integer getIndex_code_in_stockConstants() {
		return index_code_in_stockConstants;
	}

	public void setIndex_code_in_stockConstants(Integer index_code_in_stockConstants) {
		this.index_code_in_stockConstants = index_code_in_stockConstants;
	}

	private HashMap<String, Entity> entity_map;

	public HashMap<String, Entity> getEntity_map() {
		return entity_map;
	}

	public void setEntity_map(HashMap<String, Entity> entity_map) {
		this.entity_map = entity_map;
		// putifabsent - all pairs of this entity to the pair lookup map.

	}

	/****
	 * key is the entity es id.
	 * 
	 * value is the unique name.
	 * 
	 * map is built to enable getting entity by its unique id, this is needed
	 * only in the {@link #calculate_entity_index_complexes()}, and specifically
	 * the {@link #process_bs_update_bulk_response(BulkResponse)}, where any
	 * error has only the item-id, which is in the form of en-ian-san here
	 * entity can only be identified by es-id, so having this map enables entity
	 * logging which entity had the error.
	 * 
	 * the map is built when the {@link #entity_map} is built, the entity map is
	 * filtered in subsequent steps, but this map is not. its not necessary
	 * because errors can only be produced on entities which exist, so this map
	 * will only be used to lookup entites that are actually there in the
	 * {@link #entity_map}
	 * 
	 */
	private HashMap<String, String> eid_to_euname;

	public HashMap<String, String> getEid_to_euname() {
		return eid_to_euname;
	}

	public void setEid_to_euname(HashMap<String, String> eid_to_euname) {
		this.eid_to_euname = eid_to_euname;
	}

	private BulkProcessor remote_processor;

	public BulkProcessor getRemote_Bulk_processor() {
		return remote_processor;
	}

	/****
	 * whether the remote bulk processor was successfully shutdown. set in
	 * {@link EntityIndex#shutdown_bulk_processors()}
	 * 
	 */
	private Boolean remote_bulk_processor_successfully_shutdown;

	public Boolean getRemote_bulk_processor_successfully_shutdown() {
		return remote_bulk_processor_successfully_shutdown;
	}

	public void setRemote_bulk_processor_successfully_shutdown(Boolean remote_bulk_processor_successfully_shutdown) {
		this.remote_bulk_processor_successfully_shutdown = remote_bulk_processor_successfully_shutdown;
	}

	/***
	 * whether the local bulk processor was successfully shutdown set in
	 * {@link EntityIndex#shutdown_bulk_processors()}
	 * 
	 */
	public BulkProcessor local_processor;

	public BulkProcessor getLocal_processor() {
		return local_processor;
	}

	private Boolean local_bulk_processor_successfully_shutdown;

	public Boolean getLocal_bulk_processor_successfully_shutdown() {
		return local_bulk_processor_successfully_shutdown;
	}

	public void setLocal_bulk_processor_successfully_shutdown(Boolean local_bulk_processor_successfully_shutdown) {
		this.local_bulk_processor_successfully_shutdown = local_bulk_processor_successfully_shutdown;
	}

	/*****
	 * used while first building the entity index from the components provider
	 * only. before committing the entity_index to elasticsearch, we check how
	 * many entities there already are in es we use this number as the starting
	 * counter for the ids of the entities in this index. it is set
	 * automatically in {@link #before_commit_entity_index_to_elasticsearch()}
	 * 
	 */
	private Integer entities_in_es_before_committing_this_entity_index;

	public Integer getEntities_in_es_before_committing_this_entity_index() {
		return entities_in_es_before_committing_this_entity_index;
	}

	public void setEntities_in_es_before_committing_this_entity_index(
			Integer entities_in_es_before_committing_this_entity_index) {
		this.entities_in_es_before_committing_this_entity_index = entities_in_es_before_committing_this_entity_index;
	}

	private ArrayList<String> complex_ids_detected_on_latest_day_id;

	public ArrayList<String> getComplex_ids_detected_on_latest_day_id() {
		return complex_ids_detected_on_latest_day_id;
	}

	public void setComplex_ids_detected_on_latest_day_id(ArrayList<String> complex_ids_detected_on_latest_day_id) {
		this.complex_ids_detected_on_latest_day_id = complex_ids_detected_on_latest_day_id;
	}

	/*****
	 * constructor
	 * 
	 * 
	 * @param index_name
	 * @param index_number_in_stockConstants
	 */
	public EntityIndex(String index_name, Integer index_number_in_stockConstants) {

		this.index_name = index_name;
		this.entity_map = new LinkedHashMap<String, Entity>();
		this.index_code_in_stockConstants = index_number_in_stockConstants;
		this.eid_to_euname = new LinkedHashMap<String, String>();
		this.bulk_processors_successfully_shutdown = false;
		this.local_bulk_processor_successfully_shutdown = false;
		this.remote_bulk_processor_successfully_shutdown = false;
		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("instantiated");

	}

	// ///////////////////////////////////////////////////////////////
	//
	// METHOD FOR FIRST TIME BUILDING THE ENTITY DATA.
	//
	// //////////////////////////////////////////////////////////////

	@Override
	public void before_commit_entity_index_to_elasticsearch() throws Exception {
		// TODO Auto-generated method stub
		LogObject lg = new LogObject(getClass(), this);

		CountResponse resp = LocalEs.getInstance().prepareCount("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();

		setEntities_in_es_before_committing_this_entity_index((int) resp.getCount());

		lg.commit_info_log("before commit found:" + getEntities_in_es_before_committing_this_entity_index()
				+ " entities already in es");

	}

	public void commit_entity_index_to_elasticsearch(Integer index_or_update) throws Exception {
		// first get the total number of entities in the index, make the counter
		// equal to that
		// and then do the commit phase.
		before_commit_entity_index_to_elasticsearch();

		LogObject lg = new LogObject(getClass(), this);
		Integer counter = getEntities_in_es_before_committing_this_entity_index();
		for (Map.Entry<String, Entity> entry : this.entity_map.entrySet()) {
			entry.getValue().commit_entity_to_elasticsearch(
					StockConstants.EntityIndex_Entity_id_prefix + counter.toString(), index_or_update);
			counter++;
		}

		lg.commit_info_log("committed to es");
	}

	public void build_entity_index_from_wikipedia() throws Exception {

		new EventLogger(EventLogger.BUILDING_INDEX, index_name).commit();

		WikipediaScraper wsc = new WikipediaScraper(index_name, index_code_in_stockConstants);

		wsc.get_components();

		setEntity_map(wsc.getExchange_entity_map());

		commit_entity_index_to_elasticsearch(0);

		new EventLogger(EventLogger.BUILT_INDEX, index_name).commit();
	}

	public void build_forex_entity_index() throws Exception {

		new EventLogger(EventLogger.BUILDING_INDEX, index_name).commit();

		ForexComponents fcomp = new ForexComponents(index_name, index_code_in_stockConstants);

		fcomp.get_components();

		setEntity_map(fcomp.getExchange_entity_map());

		commit_entity_index_to_elasticsearch(0);

		new EventLogger(EventLogger.BUILT_INDEX, index_name).commit();
	}

	public void build_metals_entity_index() throws Exception {

		new EventLogger(EventLogger.BUILDING_INDEX, index_name).commit();

		MetalsComponents mc = new MetalsComponents(index_name, index_code_in_stockConstants);
		mc.get_components();
		setEntity_map(mc.getExchange_entity_map());
		commit_entity_index_to_elasticsearch(0);

		new EventLogger(EventLogger.BUILT_INDEX, index_name).commit();

	}

	public void build_oils_entity_index() throws Exception {

		new EventLogger(EventLogger.BUILDING_INDEX, index_name).commit();

		OilComponents oil = new OilComponents(index_name, index_code_in_stockConstants);

		oil.get_components();

		setEntity_map(this.entity_map = oil.getExchange_entity_map());

		commit_entity_index_to_elasticsearch(0);

		new EventLogger(EventLogger.BUILT_INDEX, index_name).commit();

	}

	// /////////////////////////////////////////////////////////////////
	// //
	// METHODS FOR NEW DOWNLOAD. //
	// //
	// /////////////////////////////////////////////////////////////////

	@Override
	public void build_entity_index_from_elasticsearch() throws Exception {

		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity").setSize(500)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termFilter("indice_stockConstants_code", getIndex_code_in_stockConstants())))
				.execute()

				.actionGet();

		LogObject lg_1 = new LogObject(this.getClass(), this);

		lg_1.add_to_info_packet("total_entities_in_es_from_entity_index", resp.getHits().getHits().length);
		if (resp.getHits().getHits().length == 0) {
			lg_1.commit_error_log("no entites found for entity index",
					new Exception("no entities found in es for this entity index"));
			throw new Exception("no entities found in es for this entity index");
		}

		lg_1.commit_info_log("init:build each entity");

		for (SearchHit hit : resp.getHits().getHits()) {
			// now parse the hit as an entity.
			String source_string = hit.getSourceAsString();
			@SuppressWarnings("static-access")
			Entity e = CentralSingleton.getInstance().objmapper.readValue(source_string, Entity.class);
			e.setEntity_es_id(hit.getId());

			// given a marker of no_data_in_es -> the entity es_linked_list
			// should be empty
			// given a marker of some_data_in_es -> the entity es linked list
			// should not be empty

			e.after_build_entity_hook();
			this.entity_map.put(e.getUnique_name(), e);
			this.eid_to_euname.put(e.getEntity_es_id(), e.getUnique_name());
		}

		LogObject lg_3 = new LogObject(this.getClass(), this);
		lg_3.commit_info_log("completed:build each entity");

	}

	@Override
	public Boolean index_exists_in_database() throws Exception {

		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity").setSize(500)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termFilter("indice_stockConstants_code", getIndex_code_in_stockConstants())))
				.execute()

				.actionGet();

		LogObject lg_1 = new LogObject(this.getClass(), this);

		lg_1.add_to_info_packet("total_entities_in_es_from_entity_index", resp.getHits().getHits().length);
		if (resp.getHits().getHits().length > 0) {

			new EventLogger(EventLogger.INDEX_EXISTS, this.index_name, new HashMap<String, String>() {
				{
					put("index_exists", "yes");
				}
			}).commit();
			return true;
		} else {
			new EventLogger(EventLogger.INDEX_EXISTS, this.index_name, new HashMap<String, String>() {
				{
					put("index_exists", "no");
				}
			}).commit();
			return false;
		}
	}

	@Override
	public void before_download_entity_index() throws Exception {
		// TODO Auto-generated method stub
		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("before download");

		TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				DownloadEntityExchangeTests.stop_es_before_initializing_bulk_processor, null));

		// TestMethodsExecutor.exec(new ArrayList<String>(
		// Arrays.asList(DownloadEntityExchangeTests
		// .stop_es_before_initializing_bulk_processor)));
		initialize_remote_bulk_processor();
		initialize_local_bulk_processor();
		build_entity_index_from_elasticsearch();

		LogObject lg_e = new LogObject(getClass(), this);
		lg_e.commit_info_log("completed:before download");
	}

	/***
	 * first tries to load the entity from elasticsearch if it does not exist,
	 * then will throw an error if it exists, will load it from elasticsearch,
	 * and then call download
	 * 
	 */
	@Override
	public void download_entity_index() throws Exception {

		new EventLogger(EventLogger.DOWNLOADING_INDEX, index_name).commit();

		before_download_entity_index();

		for (String key : getEntity_map().keySet()) {
			// the local processor has no role anymore.
			// its just hanging there for no reason whatsoever.
			getEntity_map().get(key).download_entity_from_provider(getRemote_Bulk_processor(), getLocal_processor());
		}

		after_download_entity_index();

	}

	/*****
	 * this method is now useless other than the call to filter_entity_map we no
	 * longer do the job status based updates to the remote server everything is
	 * done directly to the remote es server.
	 * 
	 * 
	 * 
	 */
	@Override
	public void after_download_entity_index() throws Exception {
		LogObject lg_s = new LogObject(getClass(), this);
		lg_s.commit_info_log("init:after download");

		// basically we want to skip this part as well.
		HashMap<String, Object> test_results = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.skip_after_entity_index_download_hook));
		if (test_results.size() == 1 && test_results.containsValue(true)) {
			LogObject lg_s1 = new LogObject(getClass(), this);
			lg_s1.commit_info_log("skipping entity index after download hook, as per test");
			return;
		}

		/***
		 * removes entities which have no data.
		 * 
		 */
		filter_entity_map();

		/***
		 * shuts the bulk processors. we have a test in download entity exchange
		 * tests that simulates a situation where the bulk processors do not get
		 * shutdown.
		 * 
		 * DownloadEntityExchangeTests
		 * #when_bulk_processor_shutdown_returns_false_no_further_exchanges_should_be_processed
		 * 
		 * so this test_method returns true when we are testing.
		 * 
		 * so here we check if the method returns true and then we skip the call
		 * to shutdown_bulk_processors
		 * 
		 * otherwise the call goes through as normal.
		 */

		/****
		 * test code to decide if shutdown bulk processors should be skipped. it
		 * wraps a working function, shutdown_bulk_processors(), this function
		 * is necessary in the normal flow of the program.
		 */
		HashMap<String, Object> test_res = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(DownloadEntityExchangeTests.skip_shutdown_bulk_processors, null));
		if (test_res.size() == 1 && test_res.values().contains(true)) {

		} else {
			shutdown_bulk_processors();
		}
		/****
		 * end test code.
		 * 
		 */

		if (!bulk_processors_successfully_shutdown) {
			new EventLogger(EventLogger.FAILED_TO_SHUTDOWN_BULK_PROCESSORS, getIndex_name()).commit();
			throw new Exception(StockConstants.EntityIndex_failed_to_shut_bulk_processors);
		}

		new EventLogger(EventLogger.DOWNLOADED_INDEX, getIndex_name()).commit();

		LogObject lg_e = new LogObject(getClass(), this);
		lg_e.commit_info_log("completed:after download");

	}

	public void old_unused_function_for_remote_jobs() throws Exception {
		if (getEntity_map().size() > 0) {

			/****
			 * 
			 * now make the remote request ot update the price change and stop
			 * loss arrays
			 * 
			 * how to wait till the bulk processor has finished.
			 */
			LogObject lg_d1 = new LogObject(EntityIndex.class, this);
			String job_id = String.valueOf(new GregorianCalendar().getTimeInMillis());
			lg_d1.add_to_info_packet("price_arr_j_id", job_id);
			setPrice_and_stop_loss_arr_update_job_id_sent(job_id);

			// now we poll for the job to finish.
			Integer poll_times = 20;
			Boolean break_loop = false;
			// while received job id is not equal to the sent job id.

			while (true) {
				if (getPrice_and_stop_loss_arr_update_job_id_received() == null) {
					// make the request
					JobStatusResponse jsr_req = ReadWriteTextToFile.getInstance()
							.make_stop_loss_and_price_change_update_request(getEntity_map(),
									getPrice_and_stop_loss_arr_update_job_id_sent());
					// if the jsr_req is null,
					if (jsr_req == null) {
						// throw execption
						lg_d1.commit_info_log("response execption");
						throw new Exception("remote server responded with null job id.");
					} else if (jsr_req.getJob_id() != getPrice_and_stop_loss_arr_update_job_id_sent()) {
						// dont do anything, some other job is pending.
						lg_d1.add_to_info_packet("rec j_id diff from sent, waiting", jsr_req.getJob_id());
					} else {
						// set the recieved id as whatever was received.
						setPrice_and_stop_loss_arr_update_job_id_received(jsr_req.getJob_id());
						lg_d1.add_to_info_packet("rec j_id same as sent", jsr_req.getJob_id());
					}
				} else {
					// get teh status

					JobStatusResponse jsr_status = ReadWriteTextToFile.getInstance()
							.make_status_request(getPrice_and_stop_loss_arr_update_job_id_sent());
					// so we wait for the thing to finish its work.
					if (jsr_status.getStatus_response().equals("success")) {
						lg_d1.commit_info_log("job success");
						break_loop = true;
					} else if (jsr_status.getStatus_response().equals("pending")) {
						// dont do anything, wait.
						lg_d1.add_to_info_packet("job status", "pending");
						Thread.sleep(10000);
					} else {
						lg_d1.commit_info_log("job status not pending or success.");
						throw new Exception("job returned abnormal status:" + jsr_status.getStatus_response());
					}

				}
				if (poll_times > 20 || break_loop) {
					break;
				}
				poll_times++;
			}

			if (!break_loop) {
				// exceeded the wait period.
				// throw exception.
				lg_d1.commit_info_log("exceeded wait period");
				throw new Exception("exceeded wait period on job");

			}

		}

	}

	/**
	 * both will add entity to entity map -> so in wikipedia building, it will
	 * build entities with components and an empty price map and add it to the
	 * entity map.
	 * 
	 * 
	 * in elasticsearch it will read data from es, and with all the price map
	 * also it will read and add enitty to entity map.
	 * 
	 * @param e
	 */
	public void add_entity_to_entity_map(Entity e) {
		this.entity_map.put(e.generate_unique_name(), e);
	}

	@Override
	public void before_calculate_entity_index_indicators() throws Exception {
		// TODO Auto-generated method stub
		// currently sets the timeseries on the entity
		LogObject lg_s = new LogObject(getClass(), this);
		lg_s.commit_info_log("init:before calculate indicators");

		for (Map.Entry<String, Entity> entry : entity_map.entrySet()) {
			entry.getValue().price_map_internal_to_timeseries();
		}

		LogObject lg_e = new LogObject(getClass(), this);
		lg_e.commit_info_log("completed: before calculate indicators");
	}

	@Override
	public void calculate_entity_index_indicators() throws Exception {

		before_calculate_entity_index_indicators();

		LogObject lg_s = new LogObject(getClass(), this);
		lg_s.commit_info_log("init:calculate indicators");
		// TODO Auto-generated method stub

		new EventLogger(EventLogger.CALCULATING_INDICATORS, getIndex_name()).commit();
		// CALCULATE THE INDICATOR VALUES FOR ALL INDICATORS AND ALL ITS //
		// ApplicalbelENTITIES. JSONObject indicators_jsonfile =

		// now
		// we iterate the entire entity map and see which entity has // any
		// of
		// these ohlcv groups as applicable.

		for (Map.Entry<String, Entity> entry : entity_map.entrySet()) {
			JSONObject indicators_jsonfile = CentralSingleton.getInstance().indicators_jsonfile;
			Iterator<String> keys_iterator = indicators_jsonfile.keys();
			Entity entity = entry.getValue();
			CalculateIndicators cind = new CalculateIndicators(entity);

			while (keys_iterator.hasNext()) {
				final String indicator_name = keys_iterator.next();

				JSONObject indicator_object = indicators_jsonfile.getJSONObject(indicator_name);

				JSONArray ohlcv_groups_jsonarray = indicator_object
						.getJSONArray(StockConstants.indicator_ohlcv_groups_key_name);

				LogObject lg = new LogObject(getClass(), this);
				lg.add_to_info_packet("checking indicator:" + indicator_name + " vs entity:", entry.getKey());
				lg.add_to_info_packet("entity ohlcv_type:", entity.getOhlcv_type());
				lg.add_to_info_packet("indicator ohlcv_groups", ohlcv_groups_jsonarray);
				lg.commit_info_log("checking indicator compat");

				for (int i = 0; i < ohlcv_groups_jsonarray.length(); i++) {
					String indicator_ohlcv_group = ohlcv_groups_jsonarray.getString(i);

					if (entity.getOhlcv_type().equals(indicator_ohlcv_group)) {
						// nowcalculate the indicator for the entity
						LogObject lg1 = new LogObject(getClass(), this);
						lg1.commit_info_log("match found");
						cind.setIndicator_name(indicator_name);
						new EventLogger(EventLogger.ENTITY_CALCULATING_INDICATOR, getIndex_name(),
								cind.getE().getUnique_name(), new HashMap<String, String>() {
									{
										put("indicator_name", indicator_name);
									}
								}).commit();
						cind.calculate_required_indicator();
						entry.setValue(cind.getE());

					}
				}

			}

		}

		LogObject lg_e = new LogObject(getClass(), this);
		lg_e.commit_info_log("completed: calculate indicators");

		after_calculate_entity_index_indicators();
	}

	@Override
	public void after_calculate_entity_index_indicators() throws Exception {
		// TODO Auto-generated method stub
		// from here we got to calculate complexes.
		// here we call the price bucket and the standard deviation
		LogObject lg_s = new LogObject(getClass(), this);
		lg_s.commit_info_log("init:after calculate indicators");

		LogObject lg_e = new LogObject(getClass(), this);
		lg_e.commit_info_log("completed: aftet calculate indicators");
	}

	@Override
	public void log_added_calculation_task(final String calculation_task, final Integer total_tasks_added) {
		// TODO Auto-generated method stub
		new EventLogger(EventLogger.COMPLEX_CALCULATION_TASKS_ADDED, getIndex_name(), new HashMap<String, String>() {
			{
				put("calculation_task", calculation_task);
				put("total_tasks_added", total_tasks_added.toString());
			}
		}).commit();
	}

	@Override
	public void calculate_entity_index_complexes() throws Exception {
		// TODO Auto-generated method stub

		before_calculate_entity_index_complexes();

		LogObject lg_s = new LogObject(getClass(), this);
		lg_s.commit_info_log("init:calculate complexes");

		new EventLogger(EventLogger.CALCULATING_COMPLEXES, getIndex_name()).commit();

		Integer threadCounter = 0;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(50);
		BlockingThreadPoolExecutor executor = new BlockingThreadPoolExecutor(1, 1, 5000, TimeUnit.MILLISECONDS,
				blockingQueue);
		executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

			public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {

				executor.execute(r);
			}
		}); // Let start all core threads initially
		// executor.prestartAllCoreThreads();
		// so this going to act on all the entities in the index seperately.

		List<Future<HashMap<Integer, List<Complex>>>> list = new ArrayList<Future<HashMap<Integer, List<Complex>>>>();
		LogObject lg0 = new LogObject(getClass(), this);
		lg0.commit_info_log("init: add tasks -> executor");
		Integer task_counter = 0;

		// for the entity n tasks were added.
		// and how many tasks were recovered.

		for (Map.Entry<String, Entity> entry : getEntity_map().entrySet()) {

			/***
			 * pattern.
			 * 
			 * 
			 * 
			 */
			CalculationTask pattern_task = new CalculationTask(entry.getValue(), new Pattern());
			if (list.add(executor.submit(pattern_task))) {
				task_counter++;
				log_added_calculation_task(pattern_task.getClass().getName(), task_counter);
			}

			/***
			 * Nday High Low `
			 */

			// CalculationTask ndayhighlow_task = new CalculationTask();
			// ndayhighlow_task.setE(entry.getValue());
			// ndayhighlow_task.setIsub(new NDayHighLow());
			// list.add(executor.submit(ndayhighlow_task));
			// task_counter++;

			/***
			 * rise fall amount
			 * 
			 * 
			 */
			// CalculationTask rise_fall_amount_task = new CalculationTask();
			// rise_fall_amount_task.setE(entry.getValue());
			// rise_fall_amount_task.setIsub(new RiseFallAmount());
			// list.add(executor.submit(rise_fall_amount_task));
			// task_counter++;

			/****
			 * for the week month year.
			 * 
			 * 
			 */
			// CalculationTask fwmy_task = new CalculationTask();
			// fwmy_task.setE(entry.getValue());
			// fwmy_task.setIsub(new ForTheMonthWeekYearQuarter());
			// list.add(executor.submit(fwmy_task));
			// task_counter++;

			/***
			 * 
			 * indicator value change standard deviation.
			 * 
			 *
			 */
			// CalculationTask indicator_standard_deviation_change_task = new
			// CalculationTask();
			// indicator_standard_deviation_change_task.setE(entry.getValue());
			// indicator_standard_deviation_change_task.setIsub(new
			// IndicatorValueChangeStandardDeviation());
			// list.add(executor.submit(indicator_standard_deviation_change_task));
			// task_counter++;

			/****
			 * 
			 * sma cross task.
			 * 
			 * 
			 */
			// CalculationTask sma_cross_task = new CalculationTask();
			// sma_cross_task.setE(entry.getValue());
			// sma_cross_task.setIsub(new SmaCross());
			// list.add(executor.submit(sma_cross_task));
			// task_counter++;

			/**
			 * 
			 * 
			 * 
			 */
			// CalculationTask standard_deviation_task = new CalculationTask();
			// standard_deviation_task.setE(entry.getValue());
			// standard_deviation_task.setIsub(new StandardDeviation());
			// list.add(executor.submit(standard_deviation_task));
			// task_counter++;

		}

		LogObject lg1 = new LogObject(getClass(), this);
		lg1.commit_info_log("complete: added total:" + task_counter + " tasks to executor");
		// part where the calculate complexes is actual done.
		// executor.awaitTermination(10, TimeUnit.SECONDS);
		// result holder contains all the subindicators detected for a
		// particular subindicator but for all the
		// stubs of an indicator of an entity.
		// 6 bytes per integer.
		Integer futures_retrieved = 0;
		for (Future<HashMap<Integer, List<Complex>>> fut : list) {
			// LogObject lg2 = new LogObject(getClass(), this);
			// lg2.commit_info_log("retrieving future:" + task_counter);
			try {

				HashMap<String, ArrayList<Integer>> complexes_by_day_id = new HashMap<String, ArrayList<Integer>>();

				HashMap<Integer, List<Complex>> fut_res = fut.get();

				// here the future is retrieved,
				// so here we can just basially update the total tasks
				// retrieved.
				//

				for (Map.Entry<Integer, List<Complex>> fut_res_entry : fut_res.entrySet()) {

					Integer day_id = fut_res_entry.getKey();
					List<Complex> c_list = fut_res_entry.getValue();

					for (Complex c : c_list) {
						// System.out.println(c.getSubindicator_name());
						if (complexes_by_day_id.containsKey(c.getComplex_id())) {
							complexes_by_day_id.get(c.getComplex_id()).add(day_id);
						} else {
							complexes_by_day_id.put(c.getComplex_id(), new ArrayList<Integer>(Arrays.asList(day_id)));
						}
					}
				}

				// System.out.println("complexes by day id are:");
				// System.out.println(complexes_by_day_id);

				for (final Map.Entry<String, ArrayList<Integer>> complex_entry : complexes_by_day_id.entrySet()) {

					if (complex_entry.getKey().equals("E-0I16_0sb6")) {
						System.out.println("this complex was added to the bulk request");
					}

					UpdateRequest ureq = new UpdateRequest("tradegenie_titan", "bs", complex_entry.getKey());
					ureq.script("update_bs_script", ScriptType.FILE, new HashMap<String, Object>() {
						{
							put("sparse_arr", ArrayUtils.toPrimitive(
									complex_entry.getValue().toArray(new Integer[complex_entry.getValue().size()])));
						}
					});
					ureq.scriptedUpsert(true);
					ureq.upsert(new HashMap<String, Object>());
					getRemote_Bulk_processor().add(ureq);
				}
				futures_retrieved++;
				log_future_retrieved(futures_retrieved);

			} catch (Exception e) {
				LogObject lge = new LogObject(getClass(), this);
				lge.commit_error_log("future error", e);
			}

		}

		executor.shutdown();
		// Wait until all threads are finish
		new EventLogger(EventLogger.EXECUTOR_WAITING_TO_SHUTDOWN, getIndex_name()).commit();
		while (!executor.isTerminated()) {

		}
		new EventLogger(EventLogger.EXECUTOR_SHUTDOWN_COMPLETED, getIndex_name()).commit();

	}

	@Override
	public void after_calculate_entity_index_complexes() throws Exception {
		// TODO Auto-generated method stub
		// then save the entity.
		shutdown_bulk_processors();
		for (Map.Entry<String, Entity> entry : getEntity_map().entrySet()) {

			// entry.getValue().update_stop_loss_open_close_arrays();
			// entry.getValue().update_stop_loss_arrays_to_es(getBulk_processor());
			entry.getValue().commit_entity_to_elasticsearch(entry.getValue().getEntity_es_id(), 1);

		}

		// here shutdown the bulk processor

		// shutdown_bulk_processors();
	}

	// if this gives false, the program must not proceed because, it means that
	// there will
	// already be an open bulk processor, so we have to not allow anything else
	// to go forward.
	@Override
	public boolean shutdown_bulk_processors() throws Exception {
		// TODO Auto-generated method stub

		getRemote_Bulk_processor().flush();
		Boolean shutdown_resp = getRemote_Bulk_processor().awaitClose(StockConstants.EntityIndex_bulk_processor_timeout,
				TimeUnit.MILLISECONDS);
		setRemote_bulk_processor_successfully_shutdown(shutdown_resp);

		getLocal_processor().flush();
		Boolean local_shutdown_response = getLocal_processor()
				.awaitClose(StockConstants.EntityIndex_bulk_processor_timeout, TimeUnit.MILLISECONDS);
		setLocal_bulk_processor_successfully_shutdown(local_shutdown_response);

		setBulk_processors_successfully_shutdown(
				getLocal_bulk_processor_successfully_shutdown() && getRemote_bulk_processor_successfully_shutdown());
		return getBulk_processors_successfully_shutdown();

	}

	@Override
	public void initialize_local_bulk_processor() throws Exception {
		// TODO Auto-generated method stub
		BulkProcessor processor = BulkProcessor.builder(LocalEs.getInstance(), new Listener() {

			/****
			 * how this works:
			 * 
			 * it serves as a callback for both when the entire bulk fails and
			 * even for the normal after bulk callback.
			 * 
			 * @instantiate a hashmap which has the entity_unique_name as the
			 *              key and a logobject for the entity as the value.
			 * 
			 * @instantiate a boolean : response_failed, and set it to false.
			 * 
			 * @method:iterate all the payloads, one at a time.
			 * @instantiate a variable to hold the entity unique name and the
			 *              doc id that was sent in
			 * 
			 *              now check if either the response is not null and it
			 *              has failed at this index, or if the whole bulk has
			 *              failed and there is a failure message.
			 * 
			 *              in either case, failure message gets set.
			 * 
			 *              then if the failure message is not null, check if
			 *              the hashmap has the entity unique name.
			 * 
			 *              if yes then simply add to its info packet. otherwise
			 *              create an entry for the entity in the logs map and
			 *              append to its info packet.
			 * 
			 *              finally at the end commit all the log objects from
			 *              the map.
			 * 
			 * 
			 * 
			 * 
			 * @param response
			 * @param request
			 * @param failure
			 * @throws JSONException
			 */
			public void log_bulk_results(BulkResponse response, BulkRequest request, Throwable failure,
					long execution_id) throws JSONException {

				// a given entity can have multiple failed docs
				// so we just create one logobject per entity
				// then into its logobjects info packet we add all the
				// docs that failed with their
				// failure messages.

				HashMap<String, LogObject> logs_map = new HashMap<String, LogObject>();

				for (BulkItemResponse resp : response.getItems()) {

					String failure_message = null;
					String entity_es_id = resp.getId().substring(0, resp.getId().indexOf(StockConstants.Result_Hyphen));
					String entity_unique_name = getEid_to_euname().get(entity_es_id);
					/***
					 * first assign the failure message. its either that the
					 * individual request has failed or the whole bulk has
					 * failed in which case all we have to do is check failure.
					 * 
					 */
					if (response != null && resp.isFailed()) {
						failure_message = resp.getFailureMessage();
					} else if (failure != null) {
						failure_message = failure.getLocalizedMessage();
						failure.printStackTrace();
					}

					/***
					 * now process and log the failure message. if the logs map
					 * contains the entity unique name, then we add to its info
					 * packet the doc id and whatever is the failure message.
					 * 
					 * if the logs map doesnt contain it then we create a new
					 * entry in the logs map for this entity.
					 * 
					 * 
					 * 
					 */
					if (failure_message != null) {
						if (logs_map.containsKey(entity_unique_name)) {
							logs_map.get(entity_unique_name)
									.add_to_info_packet(StockConstants.EntityIndex_bulk_processor_doc_id + ":"
											+ resp.getId() + " failed with ", failure_message);
						} else {

							LogObject lg = new LogObject(getClass());
							lg.log_entity_name(entity_unique_name);
							lg.add_to_info_packet(StockConstants.EntityIndex_bulk_processor_doc_id + ":" + resp.getId()
									+ " failed with ", failure_message);
							logs_map.put(entity_unique_name, lg);

						}
					} else {
						LogObject lgd = new LogObject(EntityIndex.class, EntityIndex.this);

						lgd.add_to_info_packet(resp.getId(), entity_unique_name);

						lgd.commit_debug_log("success ids");
					}

				}

				/****
				 * 
				 * finally commit as error all the items in the log map.
				 * 
				 */
				for (Map.Entry<String, LogObject> entry : logs_map.entrySet()) {

					entry.getValue().commit_error_log(StockConstants.EntityIndex_bulk_failure,
							new Exception("bulk failure with exec id:" + execution_id));
				}

			}

			@Override
			public void beforeBulk(long executionId, BulkRequest request) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, Throwable failure) {

				// log the entire bulk as failed., basically same
				// implementation as below.
				try {
					log_bulk_results(null, request, failure, executionId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					LogObject lg = new LogObject(getClass());
					lg.commit_error_log("failed to log bulk errors", e);
				}
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
				// TODO Auto-generated method stub
				try {
					log_bulk_results(response, request, null, executionId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					LogObject lg = new LogObject(getClass());
					lg.commit_error_log("failed to log bulk errors", e);
				}
			}
		}).setBulkActions(2).setBulkSize(new ByteSizeValue(1, ByteSizeUnit.KB)).setConcurrentRequests(1).build();

		this.local_processor = processor;
		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("bulk processor set");
	}

	@Override
	public void initialize_remote_bulk_processor() throws Exception {
		// TODO Auto-generated method stub
		BulkProcessor processor = BulkProcessor.builder(RemoteEs.getInstance(), new Listener() {

			public void log_bulk_results(BulkResponse response, BulkRequest request, Throwable failure,
					long execution_id) throws JSONException {

				// a given entity can have multiple failed docs
				// so we just create one logobject per entity
				// then into its logobjects info packet we add all the
				// docs that failed with their
				// failure messages.

				HashMap<String, LogObject> logs_map = new HashMap<String, LogObject>();

				for (BulkItemResponse resp : response.getItems()) {

					String failure_message = null;
					String entity_es_id = resp.getId().substring(0, resp.getId().indexOf(StockConstants.Result_Hyphen));
					String entity_unique_name = getEid_to_euname().get(entity_es_id);
					/***
					 * first assign the failure message. its either that the
					 * individual request has failed or the whole bulk has
					 * failed in which case all we have to do is check failure.
					 * 
					 */
					if (response != null && resp.isFailed()) {
						failure_message = resp.getFailureMessage();
					} else if (failure != null) {
						failure_message = failure.getLocalizedMessage();
						failure.printStackTrace();
					}

					/***
					 * now process and log the failure message. if the logs map
					 * contains the entity unique name, then we add to its info
					 * packet the doc id and whatever is the failure message.
					 * 
					 * if the logs map doesnt contain it then we create a new
					 * entry in the logs map for this entity.
					 * 
					 * 
					 * 
					 */
					if (failure_message != null) {
						if (logs_map.containsKey(entity_unique_name)) {
							logs_map.get(entity_unique_name)
									.add_to_info_packet(StockConstants.EntityIndex_bulk_processor_doc_id + ":"
											+ resp.getId() + " failed with ", failure_message);
						} else {

							LogObject lg = new LogObject(getClass());
							lg.log_entity_name(entity_unique_name);
							lg.add_to_info_packet(StockConstants.EntityIndex_bulk_processor_doc_id + ":" + resp.getId()
									+ " failed with ", failure_message);
							logs_map.put(entity_unique_name, lg);

						}
					} else {
						LogObject lgd = new LogObject(EntityIndex.class, EntityIndex.this);

						lgd.add_to_info_packet(resp.getId(), entity_unique_name);

						lgd.commit_debug_log("success ids");
					}

				}
				/****
				 * 
				 * finally commit as error all the items in the log map.
				 * 
				 */
				for (Map.Entry<String, LogObject> entry : logs_map.entrySet()) {

					entry.getValue().commit_error_log(StockConstants.EntityIndex_bulk_failure,
							new Exception("bulk failure with exec id:" + execution_id));
				}

			}

			@Override
			public void beforeBulk(long executionId, BulkRequest request) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, Throwable failure) {

				// log the entire bulk as failed., basically same
				// implementation as below.
				try {
					log_bulk_results(null, request, failure, executionId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					LogObject lg = new LogObject(getClass());
					lg.commit_error_log("failed to log bulk errors", e);
				}
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
				// TODO Auto-generated method stub
				try {
					log_bulk_results(response, request, null, executionId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					LogObject lg = new LogObject(getClass());
					lg.commit_error_log("failed to log bulk errors", e);
				}
			}
		}).setBulkActions(2).setBulkSize(new ByteSizeValue(1, ByteSizeUnit.KB)).setConcurrentRequests(1).build();

		this.remote_processor = processor;

		LogObject lg = new LogObject(getClass(), this);
		lg.commit_info_log("bulk processor set");

	}

	@Override
	public void filter_entity_map() throws Exception {
		// TODO Auto-generated method stub

		Iterator<Map.Entry<String, Entity>> entity_map_iterator = getEntity_map().entrySet().iterator();
		while (entity_map_iterator.hasNext()) {
			Map.Entry<String, Entity> entry = entity_map_iterator.next();
			if (entry.getValue().getFilter_entity()) {
				LogObject lg = new LogObject(getClass(), entry.getValue());
				entity_map_iterator.remove();
				lg.commit_debug_log("filtered out of entity map");

			}
		}
	}

	@Override
	public void calculate_entity_scores_for_complex() throws Exception {
		// TODO Auto-generated method stub

	}

	/***
	 * logs an error on the entity if the response has failed. otherwise,
	 * returns an arraylist of all successfull ids.
	 * 
	 * @param: remote_or_local
	 *             0 -> error in the local bulkrequest. 1 -> error in the remote
	 *             bulkrequest.
	 * 
	 */
	@Override
	public ArrayList<String> process_bs_update_bulk_response(BulkResponse br, Integer remote_or_local)
			throws Exception {
		// TODO Auto-generated method stub
		ArrayList<String> successfull_complex_ids = new ArrayList<String>();
		for (BulkItemResponse bir : br.getItems()) {
			if (bir.isFailed()) {
				// log exception on entity name.
				String entity_unique_name = getEid_to_euname().get(Entity.get_entity_id_from_complex_id(bir.getId()));
				LogObject lg = new LogObject(getClass(), getEntity_map().get(entity_unique_name));
				lg.commit_error_log("bulk request failure-" + (remote_or_local == 0 ? "local" : "remote"),
						new Exception("bulk request failure"));
			} else {
				successfull_complex_ids.add(bir.getId());
			}
		}
		return null;
	}

	@Override
	public void before_calculate_entity_index_complexes() throws Exception {
		// TODO Auto-generated method stub
		initialize_remote_bulk_processor();
		initialize_local_bulk_processor();
		for (Map.Entry<String, Entity> entry : getEntity_map().entrySet()) {
			entry.getValue().before_complex_calculate();
		}
	}

	@Override
	public boolean equals(Object obj) {
		EntityIndex other_ei = (EntityIndex) obj;
		if (other_ei.getEntity_map().size() != getEntity_map().size()) {
			return false;
		}
		for (Map.Entry<String, Entity> entry : other_ei.getEntity_map().entrySet()) {
			Entity self_entry = getEntity_map().get(entry.getKey());
			if (self_entry == null) {
				return false;
			} else if (!self_entry.equals(entry.getValue())) {
				return false;
			}
		}
		return true;
	}

	/***
	 * checks three things: 1) entity index has entities 2) each entity has
	 * datapoints 3) each datapoint is equal to whatever is being compared to
	 * and none of either of their attributes are nil.
	 * 
	 * @param obj
	 * @return
	 */
	public boolean equals_and_not_empty(Object obj) {
		EntityIndex other_ei = (EntityIndex) obj;
		if (other_ei.getEntity_map().size() != getEntity_map().size() || other_ei.getEntity_map().isEmpty()
				|| getEntity_map().isEmpty()) {
			return false;
		}
		for (Map.Entry<String, Entity> entry : other_ei.getEntity_map().entrySet()) {
			Entity self_entry = getEntity_map().get(entry.getKey());
			if (self_entry == null) {
				return false;
			} else if (!self_entry.equals_and_not_empty(entry.getValue())) {
				return false;
			}
		}
		return true;
	}

	/***
	 * @return : true if no entities in exchange, or false otherwise.
	 */
	public boolean exchange_is_empty() {
		return getEntity_map().isEmpty();
	}

	@Override
	public void log_future_retrieved(final Integer total_futures_retrieved) {
		// TODO Auto-generated method stub
		new EventLogger(EventLogger.FUTURES_RETRIEVED, getIndex_name(), new HashMap<String, String>() {
			{
				put("futures_retrieved", total_futures_retrieved.toString());
			}
		}).commit();
	}

}
