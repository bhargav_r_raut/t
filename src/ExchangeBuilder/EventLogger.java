package ExchangeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.elasticsearch.action.index.IndexResponse;

import elasticSearchNative.RemoteEs;

/****
 * 
 * 
 * 
 * 
 * @author bhargav
 *
 */
public class EventLogger {

	public static final String INDEX_EXISTS = "INDEX_EXISTS";
	public static final String BUILDING_INDEX = "BUILDING_INDEX_FOR_FIRST_TIME";
	public static final String BUILT_INDEX = "BUILT_INDEX_FOR_FIRST_TIME_COMPLETED";
	public static final String ENTITY_EXISTING_DATE_AND_DATAPOINTS = "ENTITY_EXISTING_DATE_AND_DATAPOINTS";
	public static final String DOWNLOADING_INDEX = "DOWNLOADING_INDEX";
	public static final String ENTITY_DOWNLOADING = "DOWNLOADING_ENTITY";
	public static final String ENTITY_DOWNLOADED_DATE_AND_DATAPOINTS = "ENTITY_DOWNLOADED_DATE_AND_DATAPOINTS";
	public static final String ENTITY_EXPECTED_DATAPOINT_ABSENT = "ENTITY_EXPECTED_DATAPOINT_ABSENT";
	public static final String ENTITY_FORCE_REDOWNLOAD = "ENTITY_FORCE_REDOWNLOAD";
	public static final String ENTITY_DOWNLOAD_ERROR = "ENTITY_DOWNLOAD_ERROR";
	public static final String ENTITY_NO_NEW_DATAPOINTS = "ENTITY_NO_NEW_DATAPOINTS";
	public static final String ENTITY_FILTERING = "ENTITY_FILTERING";
	public static final String ENTITY_PRICE_CHANGE_ARRAYS_BEFORE_CALCULATION = "ENTITY_PRICE_CHANGE_ARRAYS_BEFORE_CALCULATION";
	public static final String ENTITY_PRICE_CHANGE_ARRAYS_AFTER_CALCULATION = "ENTITY_PRICE_CHANGE_ARRAYS_AFTER_CALCULATION";
	public static final String ENTITY_STOP_LOSS_ARRAYS_CHANGES = "ENTITY_STOP_LOSS_ARRAYS_CHANGES";
	public static final String ENTITY_UPDATING_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_TO_ES = "ENTITY_UPDATING_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_TO_ES";
	public static final String ENTITY_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_CHUNKS_UPDATED = "ENTITY_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_CHUNKS_UPDATED";
	public static final String ENTITY_FIRST_NEWLY_DOWNLOADED_PAIR_INDEX_MISSING = "ENTITY_FIRST_NEWLY_DOWNLOADED_PAIR_INDEX_MISSING";
	public static final String FAILED_TO_SHUTDOWN_BULK_PROCESSORS = "FAILED_TO_SHUTDOWN_BULK_PROCESSORS";
	public static final String DOWNLOADED_INDEX = "DOWNLOADED_INDEX";
	public static final String CALCULATING_INDICATORS = "CALCULATING_INDICATORS";
	public static final String ENTITY_CALCULATING_INDICATOR = "ENTITY_CALCULATING_INDICATOR";
	public static final String CALCULATING_COMPLEXES = "CALCULATING_COMPLEXES";
	public static final String COMPLEX_CALCULATION_TASKS_ADDED = "COMPLEX_CALCULATION_TASKS_ADDED";
	public static final String ENTITY_TASK_STARTED = "ENTTIY_TASK_STARTED";
	public static final String ENTITY_TASK_FINISHED = "ENTITY_TASK_FINISHED";
	public static final String FUTURES_RETRIEVED = "FUTURES_RETRIEVED";
	public static final String FUTURE_RETRIEVE_ERROR = "FUTURE_RETRIEVE_ERROR";
	public static final String EXECUTOR_WAITING_TO_SHUTDOWN = "EXECUTOR_WAITING_TO_SHUTDOWN";
	public static final String EXECUTOR_SHUTDOWN_COMPLETED = "EXECUTOR_SHUTDOWN_COMPLETED";

	/*******
	 * THIS NEEDS TO BE UPDATED TO REFLECT ALL THE EVENTS WRITTEN ABOVE.
	 * 
	 * 
	 */
	public static ArrayList<String> event_order = new ArrayList<String>(Arrays.asList(
			INDEX_EXISTS,
			BUILDING_INDEX,
			BUILT_INDEX,
			ENTITY_EXISTING_DATE_AND_DATAPOINTS,
			DOWNLOADED_INDEX,
			ENTITY_DOWNLOADING,
			ENTITY_DOWNLOADED_DATE_AND_DATAPOINTS,
			ENTITY_EXPECTED_DATAPOINT_ABSENT,
			ENTITY_FORCE_REDOWNLOAD,
			ENTITY_DOWNLOAD_ERROR,
			ENTITY_NO_NEW_DATAPOINTS,
			ENTITY_FILTERING,
			ENTITY_PRICE_CHANGE_ARRAYS_BEFORE_CALCULATION,
			ENTITY_PRICE_CHANGE_ARRAYS_AFTER_CALCULATION,
			ENTITY_STOP_LOSS_ARRAYS_CHANGES,
			ENTITY_UPDATING_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_TO_ES,
			ENTITY_STOP_LOSS_AND_PRICE_CHANGE_ARRAYS_CHUNKS_UPDATED,
			ENTITY_FIRST_NEWLY_DOWNLOADED_PAIR_INDEX_MISSING,
			FAILED_TO_SHUTDOWN_BULK_PROCESSORS,
			DOWNLOADED_INDEX,
			CALCULATING_INDICATORS,
			ENTITY_CALCULATING_INDICATOR,
			CALCULATING_COMPLEXES,
			COMPLEX_CALCULATION_TASKS_ADDED,
			ENTITY_TASK_STARTED,
			ENTITY_TASK_FINISHED,
			FUTURES_RETRIEVED,
			FUTURE_RETRIEVE_ERROR,
			EXECUTOR_WAITING_TO_SHUTDOWN,
			EXECUTOR_SHUTDOWN_COMPLETED
			));

	// now i have to add all this to the right places in the code.

	/***
	 * ELASTICSEARCH TYPE: event_logger,
	 * 
	 * ELASTICSEARCH PROPERTIES : "event_log":{ "properties":{
	 * "event_name":{"type":"string","index":"not_analyzed"},
	 * "entity_index":{"type":"string","index":"not_analyzed"},
	 * "entity_unique_name":{"type":"string","index":"not_analyzed"},
	 * "time":{"type":"long"}, "other_info":{"type":"object"} } }
	 * 
	 * ELASTICSEARCH DEFINITION_LOCATION :
	 * es_for_xeon/config/tradegenie_stats_mapping.json
	 ***/

	private String event_name;
	private String entity_index;
	private String entity_unique_name;
	private static String session_id;
	private Long time;
	private HashMap<String, String> other_info;
	private Integer order_of_event;

	public static String getSession_id() {
		return session_id;
	}

	public static void setSession_id(String session_id) {
		EventLogger.session_id = session_id;
	}

	public String getEvent_name() {
		return event_name;
	}

	public void setEvent_name(String event_name) {
		if (EventLogger.event_order.contains(event_name)) {
			this.event_name = event_name;
		}
	}

	public String getEntity_index() {
		return entity_index;
	}

	public void setEntity_index(String entity_index) {
		this.entity_index = entity_index;
	}

	public String getEntity_unique_name() {
		return entity_unique_name;
	}

	public void setEntity_unique_name(String entity_unique_name) {
		this.entity_unique_name = entity_unique_name;
	}

	public Long getTime() {
		return time;
	}

	public HashMap<String, String> getOther_info() {
		return other_info;
	}

	public void setOther_info(HashMap<String, String> other_info) {
		this.other_info = other_info;
	}

	public EventLogger(String event_name, String entity_index) {
		super();
		setEvent_name(event_name);
		setEntity_index(entity_index);
		this.time = System.currentTimeMillis();
		this.order_of_event = event_order.indexOf(getEvent_name());
	}

	public EventLogger(String event_name, String entity_index, HashMap<String, String> other_info) {
		super();
		setEvent_name(event_name);
		setEntity_index(entity_index);
		setOther_info(other_info);
		this.time = System.currentTimeMillis();
		this.order_of_event = event_order.indexOf(getEvent_name());
	}

	public EventLogger(String event_name, String entity_index, String entity_unique_name) {
		super();
		this.event_name = event_name;
		this.entity_index = entity_index;
		this.entity_unique_name = entity_unique_name;
		this.time = System.currentTimeMillis();
		this.order_of_event = event_order.indexOf(getEvent_name());
	}

	public EventLogger(String event_name, String entity_index, String entity_unique_name,
			HashMap<String, String> other_info) {
		super();
		this.event_name = event_name;
		this.entity_index = entity_index;
		this.entity_unique_name = entity_unique_name;
		this.other_info = other_info;
		this.time = System.currentTimeMillis();
		this.order_of_event = event_order.indexOf(getEvent_name());
	}

	public static final String es_type = "event_logger";

	public Boolean commit() {
		// check that the entity_index, entity_unique_name and
		if (this.entity_index == null) {
			return false;
		}
		try {
			IndexResponse response = RemoteEs.getInstance().prepareIndex().setType(es_type).setIndex("tradegenie_titan")
					.setSource(CentralSingleton.getInstance().objmapper.writeValueAsBytes(this)).setRefresh(true)
					.execute().actionGet();
			
			System.out.println("committed event:");
			System.out.println(CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter().writeValueAsString(this));
			
			return response.isCreated();
		} catch (Exception e) {
			return false;
		}
	}
}
