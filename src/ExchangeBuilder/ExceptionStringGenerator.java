package ExchangeBuilder;

import java.util.HashMap;
import java.util.Map;

public class ExceptionStringGenerator {
	
	/****
	 * @param template : a string which contains variables that are to be replaced
	 * @param replacements : replacement: a hashmap with the following structure:
	 * key -> name of variables that should be present in the template
	 * value -> value to replace that variable with.
	 * 
	 * @example:
	 * template : hello VARIABLE how are you
	 * replacements: {"VARIABLE" : "kumud"}
	 * would return : hello kumud how are you.
	 * 
	 * @return : template with substitutions in place
	 */
	public static String generate_exception_string(String template, HashMap<String, String> replacements) {
		for (Map.Entry<String, String> e : replacements.entrySet()) {
			template = template.replace(e.getKey(), e.getValue());
		}
		return template;
	}
}
