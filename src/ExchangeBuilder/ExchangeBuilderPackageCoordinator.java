package ExchangeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStats;

import yahoo_finance_historical_scraper.StockConstants;
import IdAndShardResolver.IdAndShardResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tradegenie.tests.FlushIndicesAndRedis;
import com.tradegenie.tests.RemoteConnectionToggleTests;
import com.tradegenie.tests.ShScript;
import com.tradegenie.tests.TestSingleton;

import elasticSearchNative.LocalEs;

public class ExchangeBuilderPackageCoordinator implements IExchangeBuilder {

	public ExchangeBuilderPackageCoordinator() {
		System.out.println("came to erp constructor");
		try {
			LogObject lgd = new LogObject(ExchangeBuilderPackageCoordinator.class);
			lgd.commit_debug_log("initializing central singleton");
			CentralSingleton.initializeSingleton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogObject lge = new LogObject(ExchangeBuilderPackageCoordinator.class);
			lge.commit_error_log("error initializing central singleton", e);
			e.printStackTrace();
		}
	}

	@Override
	public void build_entity() throws Exception {
		// manually build the entity,
		// example.
		/**
		 * Entity e = new Entity("x", 10);
		 * EntityIndex.commit_entity_to_elasticsearch(e);
		 * CreateDailyIndicatorsJsonString cdrj = new
		 * CreateDailyIndicatorsJsonString( null, null, e.getIndice(), e);
		 * 
		 * return e;
		 **/

	}

	@Override
	public HashMap<String, EntityIndex> build_all_exchanges() throws Exception {
		HashMap<String, EntityIndex> ei_index_map = new HashMap<String, EntityIndex>();
		LogObject lg = new LogObject(this.getClass());
		lg.commit_info_log("init:build all exchanges");
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = build_entityExchange(i, entry.getKey());
			EntityIndex ei2 = ei_index_map.get(ei.getIndex_name());

			i++;
		}
		return ei_index_map;
	}

	@Override
	public HashMap<String, EntityIndex> download_all_exchanges() throws Exception {

		LogObject lg = new LogObject(this.getClass());
		lg.commit_info_log("init:download all exchanges");
		HashMap<String, EntityIndex> ei_index_map = new HashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = download_entityExchange(entry.getValue(), i);
			ei_index_map.put(ei.getIndex_name(), ei);
			i++;

		}

		return ei_index_map;
	}

	@Override
	public EntityIndex download_entityExchange(String exchange_name, Integer exchange_number) throws Exception {
		// TODO Auto-generated method stub

		EntityIndex ei = new EntityIndex(exchange_name, exchange_number);

		LogObject lg = new LogObject(this.getClass(), ei);

		lg.commit_info_log("starting to download entity exchange");

		ei.download_entity_index();

		LogObject lg_end = new LogObject(this.getClass(), ei);
		lg_end.commit_info_log("successfully downloaded entity exchange");

		return ei;

	}

	@Override
	public EntityIndex build_entityExchange(Integer exchange_number, String exchange_name) throws Exception {

		EntityIndex ei = new EntityIndex(exchange_name, exchange_number);
		LogObject lg = new LogObject(this.getClass(), ei);
		lg.add_to_info_packet("exchange number coming in", exchange_number);
		lg.add_to_info_packet("exchange name coming in", exchange_name);
		lg.commit_info_log("starting to build entity exchange");

		// if it finds this key in the stockconstants test_methods hash
		// keys, then
		// it will execute whatever methods have been registered for that .

		TestMethodsExecutor.exec(new TestMethodExecutorArgs(
				RemoteConnectionToggleTests.stop_local_es_before_build_entity_exchange_but_after_checking_exceptions,
				null));

		if (exchange_name.equals(StockConstants.name_for_forex)) {
			ei.build_forex_entity_index();
		} else if (exchange_name.equals(StockConstants.name_for_cac)
				|| exchange_name.equals(StockConstants.name_for_dax)
				|| exchange_name.equals(StockConstants.name_for_nifty)
				|| exchange_name.equals(StockConstants.name_for_ftse)
				|| exchange_name.equals(StockConstants.name_for_nasdaq)) {
			ei.build_entity_index_from_wikipedia();
		} else if (exchange_name.equals(StockConstants.name_for_oil)) {
			ei.build_oils_entity_index();
		} else if (exchange_name.equals(StockConstants.name_for_metals)) {
			ei.build_metals_entity_index();
		}

		return ei;

	}

	/***
	 * 
	 * main function to be called for the whole program. this is the only entry
	 * point.
	 * 
	 * @throws Exception
	 * 
	 */
	public void run(ArrayList<String> exchanges_to_run) throws Exception {
		// first step.
		// we have to consider all the entity indices.
		// LogObject lg = new LogObject(this.getClass());
		// lg.commit_info_log("init:download all exchanges");
		HashMap<String, EntityIndex> ei_index_map = new HashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			String exchange_name = entry.getValue();
			if (exchanges_to_run.contains(exchange_name)) {
				EntityIndex ei = download_entityExchange(entry.getValue(), i);

				// first have to check if it exists or not?
				// check if there are entities.
				if (!ei.index_exists_in_database()) {
					if (exchange_name.equals(StockConstants.name_for_forex)) {
						ei.build_forex_entity_index();
					} else if (exchange_name.equals(StockConstants.name_for_cac)
							|| exchange_name.equals(StockConstants.name_for_dax)
							|| exchange_name.equals(StockConstants.name_for_nifty)
							|| exchange_name.equals(StockConstants.name_for_ftse)
							|| exchange_name.equals(StockConstants.name_for_nasdaq)) {
						ei.build_entity_index_from_wikipedia();
					} else if (exchange_name.equals(StockConstants.name_for_oil)) {
						ei.build_oils_entity_index();
					} else if (exchange_name.equals(StockConstants.name_for_metals)) {
						ei.build_metals_entity_index();
					}
				}

				// first i need to evaluate if everything even runs once at
				// least.
				// we can download data for only 100 days after the beginning
				// and
				// and that can be a small test run.

				// okay so let me first do log interpretation.
				// let me make a list of events that should be logged.
				// things that i would like to know about.
				// and then let us attach logs to only those events.
				// let us make a seperate and a new logger.
				// logs are also going to be written to the remote es instance
				// so we just want one static method.
				// which will make a call to the remote es instance.
				// what will be the structure of that object.
				// it will have the entity name, index name, time, event_name,
				// and any other information.

				ei.download_entity_index();

				ei.calculate_entity_index_indicators();

				ei.calculate_entity_index_complexes();

				i++;

			}
		}
	}

	public EntityIndex test_entity_index_from_phase(String exchange_name, Integer exchange_number) {

		try {
			EntityIndex ei = new EntityIndex(exchange_name, exchange_number);

			LogObject lg = new LogObject(this.getClass(), ei);

			lg.commit_info_log("starting to download entity exchange");

			ei.download_entity_index();

			ei.calculate_entity_index_indicators();

			ei.commit_entity_index_to_elasticsearch(1);

			/****
			 * load from cache and calculate complexes
			 * 
			 */

			// ei.build_entity_index_from_elasticsearch();

			// ei.after_load_from_cache();

			// ei.calculate_entity_index_complexes();

			LogObject lg_end = new LogObject(this.getClass(), ei);
			lg_end.commit_info_log("successfully test entity exchange");
			return ei;

		} catch (Exception e) {
			// BaseError be = new BaseError(e,
			// "failed in download entity exchange");
			// e.printStackTrace();
			LogObject lg = new LogObject(getClass());
			lg.commit_error_log("error while downloading entity exchange", e);

			return null;
		}

	}

	@Override
	public void build_price_change_aggs() throws Exception {
		// TODO Auto-generated method stub
		// step one aggregations
		// get the standard deviation for each period.
		// then do another aggregation, this time, for
		// now divide the thing within one standard deviation into 5 equal
		// parts.
		// for this find the minimum and maximum within 1 standard deviation.
		// adjust the intervals, to be diff/5, and then get the bucket keys.
		// do same for each standard deviation.
		HashMap<String, ExtendedStats> standard_deviation_map = new HashMap<String, ExtendedStats>();

		SearchResponse sr = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("price_buckets")
				.setQuery(QueryBuilders.matchAllQuery())
				.addAggregation(AggregationBuilders.terms("by_period").field("periods").size(0)
						.subAggregation(AggregationBuilders.extendedStats("by_close_price").field("close_price_amount"))

		).execute().actionGet();

		Terms agg = sr.getAggregations().get("by_period");

		// For each entry
		for (Terms.Bucket entry : agg.getBuckets()) {
			String key = entry.getKey(); // bucket key(period.)
			Long docCount = entry.getDocCount(); // Doc count
			ExtendedStats ext_agg = entry.getAggregations().get("by_close_price");
			Double standard_deviation = ext_agg.getStdDeviation();

			// System.out.println("period is:" + key);
			// System.out.println("docCOunt is" + docCount);
			// System.out.println("standard deviation is:" +
			// standard_deviation);
			// System.out.println("mean is: " + ext_agg.getAvg());
			standard_deviation_map.put(key, ext_agg);

		}

		// System.out
		// .println("\n starting uber
		// calc==========================================");
		// for each period we have to have a hashmap of rise and fall amounts,
		// as nearest multiples of five.
		// so we make one hashmap
		// then within that period, is an array and we look if that array has
		HashMap<Integer, TreeMap<Integer, Object>> periods_map = new HashMap<Integer, TreeMap<Integer, Object>>();

		TreeMap<Integer, Object> pool_map = new TreeMap<Integer, Object>();

		Integer[] observations_for_standard_deviation = new Integer[] { 7, 5, 3, 2 };
		for (Map.Entry<String, ExtendedStats> stddev_entry : standard_deviation_map.entrySet()) {

			Integer period = Integer.parseInt(stddev_entry.getKey());
			System.out.println("the period is:" + period);

			// 7,5,3,3
			for (int i = 0; i < 4; i++) {

				Double interval_d = (stddev_entry.getValue().getStdDeviation()
						/ observations_for_standard_deviation[i].doubleValue());
				Long interval = interval_d.longValue();

				Double from = stddev_entry.getValue().getStdDeviation() * i;
				Double to = stddev_entry.getValue().getStdDeviation() * (i + 1);

				System.out.println("from is:" + from + " and to is:" + to);
				System.out.println("interval is:" + interval);
				SearchResponse response = LocalEs.getInstance().prepareSearch("tradegenie_titan")
						.setTypes("price_buckets")
						.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
								FilterBuilders.boolFilter()
										.must(FilterBuilders.termFilter("periods",
												Integer.parseInt(stddev_entry.getKey())))
										.must(FilterBuilders.rangeFilter("close_price_amount").from(from).to(to))

				)

				).addAggregation(AggregationBuilders.histogram("rel").field("close_price_amount").interval(interval))

						.execute().actionGet();

				Histogram hist_agg = response.getAggregations().get("rel");
				for (Bucket b : hist_agg.getBuckets()) {
					System.out.println("in aggregation percentage change is:" + b.getKey());
					Integer percentage_change = Integer.parseInt(b.getKey());
					percentage_change = new Integer(roundUp(percentage_change));

					if (periods_map.containsKey(period)) {

					} else {
						periods_map.put(period, new TreeMap<Integer, Object>());
					}

					if (periods_map.get(period).containsKey(percentage_change)) {

					} else {
						periods_map.get(period).put(percentage_change, null);
					}

					pool_map.put(percentage_change, null);

				}

			}
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		// System.out.println(mapper.writeValueAsString(periods_map));
		// System.in.read();

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Integer, TreeMap<Integer, Object>> entry : periods_map.entrySet()) {

			StringBuilder period_builder = new StringBuilder();

			String start_string = "put(" + entry.getKey()
					+ ",new TreeMap<Integer,Pair<String,ArrayList<String>>>(){{NEW_LINE";

			Integer first_non_zero_key = entry.getValue().higherKey(0);
			Integer highest_key = entry.getValue().lastKey();

			LinkedHashMap<Integer, String> chutyacha_map = new LinkedHashMap<Integer, String>();

			for (Map.Entry<Integer, Object> null_entry : entry.getValue().entrySet()) {

				String pc_percent = String.valueOf((null_entry.getKey().doubleValue()) / (10.0));

				if (null_entry.getKey().equals(0)) {
					pc_percent = "less than " + String.valueOf((first_non_zero_key.doubleValue()) / (10.0));
				}

				period_builder.append("put(" + null_entry.getKey() + "," +

				"new Pair<String,ArrayList<String>>(\"" + pc_percent + " percent"
						+ "\",new ArrayList<String>(Arrays.asList(\"LAUDA\"))));");
				period_builder.append("NEW_LINE");

				chutyacha_map.put(null_entry.getKey(), "put(" + null_entry.getKey() + "," +

				"new Pair<String,ArrayList<String>>(\"" + pc_percent + " percent"
						+ "\",new ArrayList<String>(Arrays.asList(\"LAUDA\"))));NEW_LINE");

				if (null_entry.getKey().equals(highest_key)) {

					pc_percent = pc_percent = "more than " + String.valueOf((highest_key.doubleValue()) / (10.0));

					period_builder.append("put(" + (null_entry.getKey() + 1) + "," +

					"new Pair<String,ArrayList<String>>(\"" + pc_percent + " percent"
							+ "\",new ArrayList<String>(Arrays.asList(\"LAUDA\"))));");
					period_builder.append("NEW_LINE");

					chutyacha_map.put(null_entry.getKey() + 1, "put(" + (null_entry.getKey() + 1) + "," +

					"new Pair<String,ArrayList<String>>(\"" + pc_percent + " percent"
							+ "\",new ArrayList<String>(Arrays.asList(\"LAUDA\"))));NEW_LINE");

				}

			}

			// System.out.println("chutya map");
			// System.out.println(chutyacha_map);
			// System.in.read();

			// now process the verbal correlates
			Integer verbal_corr_size = IdAndShardResolver.verbal_correlates.size();
			Integer chutyacha_map_size = chutyacha_map.size();
			ArrayList<String> replace_with = new ArrayList<String>();

			for (Map.Entry<Integer, String> ventry : IdAndShardResolver.verbal_correlates.entrySet()) {
				// System.out.println("corr is;" + ventry.getValue());

				for (int i = 0; i < 2; i++) {
					// check size of laudyacha map, and take diff from counter
					// verbal_corr_size - counter.
					// System.out.println("chutya size: " + chutyacha_map_size);
					// System.out.println("verbal corr size:" +
					// verbal_corr_size);
					Integer size_diff = chutyacha_map_size - verbal_corr_size;
					// System.out.println("size diff is:" + size_diff);

					if (size_diff < 0) {
						// dont do anything.
					} else {
						replace_with.add(ventry.getValue());
						chutyacha_map_size--;
					}

				}

				verbal_corr_size--;
			}

			// System.out.println("chutya size:" + chutyacha_map.size());
			// System.out.println("replace size:" + replace_with.size());
			// System.out.println("replace_with:" + replace_with.toString());
			// System.in.read();
			String end_string = "";
			Integer counter = 0;
			for (Integer key : chutyacha_map.keySet()) {
				String s = chutyacha_map.get(key).replace("LAUDA", replace_with.get(counter));
				end_string += s;
				counter++;
			}

			sb.append(start_string + end_string + "}});NEW_LINE");

		}

		System.out.println(sb.toString());

		// JUST TAKE THE RESULTING STRING INTO SUBLIME AND REPLACE ALL
		// OCCURRENCES OF "NEW_LINE" WITH \n\n

	}

	public static int roundUp(int n) {
		return (n + 4) / 5 * 5;
	}
	
	public static void main(String[] args) throws Exception{
		System.out.println("RUNNING MAIN");
		
		ShScript.start_all_servers();
		
		try {
			CentralSingleton.initializeSingleton();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StockConstants.test_methods.clear();
		ShScript.start_wifi();
		System.out.println("flushing indices");
		FlushIndicesAndRedis comode = new FlushIndicesAndRedis();
		comode.flush();
		System.out.println("finished flushing");
		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		try {
			erp.run(new ArrayList<String>(Arrays.asList(StockConstants.name_for_oil)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
