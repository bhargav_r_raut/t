package ExchangeBuilder;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.joda.time.DateTime;

public interface IDataPoint {
	
	/***
	 * these methods are used in pruning the datapoint.
	 * they include checks to see if the volume is zero.
	 * or if any of the values are zero.
	 * 
	 * @param dt
	 * @return
	 */
	
	public abstract Boolean value_is_less_than_or_equal_to_zero(BigDecimal bd);
	
	public abstract Boolean volume_is_less_than_or_equal_to_zero();
	
	public abstract Boolean close_is_less_than_or_equal_to_zero();
	
	public abstract Boolean open_is_less_than_or_equal_to_zero();
	
	public abstract Boolean high_is_less_than_or_equal_to_zero();
	
	public abstract Boolean low_is_less_than_or_equal_to_zero();
	
	public abstract Boolean datapoint_invalid_due_to_zero_attributes();
	
	public abstract Boolean all_attributes_are_null();
	
	public abstract Boolean attributes_equal_to_existing_datapoint(DataPoint dp);
	
	
	/***
	 * ends
	 * @param dt
	 * @return
	 */
	
	public Integer calculate_nth_day(DateTime dt);
	
	public Integer calculate_week_of_month(DateTime dt);
	
	//this method takes the two datapoints and calculates the difference between open and close for them.
	//it returns the correlation integer in the percentage_map as a result , pair<open_correlation,close_correlation>
	//it first does (end_day - start_day)/start_day * 100.0
	//then it calls the get_nearest_correlation_from_percentage_map_method. described below.
	//if calculate_for_high_and_low is true, then it will calculate for them as well as open and close.
	public PercentageIntegerCorrelate calculate_differences_from_end_day_datapoint(DataPoint end_day,Boolean calculate_for_high_and_low);
	
	public Integer get_correlation_for_indicator(BigDecimal end_day_indicator, BigDecimal start_day_indicator);
	/****
	 * this method first multiplies the percentage change by 10. 
	 * this is because the percentage map starts with 0.1 represented as 1.0 
	 * 
	 * it then queries the percentage map for everything 1 less and 1 more than itself.
	 * if there are three entries in the correlation map, then it means that it is an exact whole number.
	 * so we get its integer correlate from the percentage map and return that.
	 * 
	 * if there are two entries.
	 * then first we calculate the difference between the (difference_itself - entry)
	 * if this Math.abs(difference) is less than 0.4, then we take it and use this entries correlate as the 
	 * correlate to return.
	 * 
	 * if neither of the two are less than 0.4, then the greater one is taken.
	 * 
	 * example:
	 * 
	 * percentage change was 0.134
	 * first multiply by 10.
	 * 1.34
	 * 
	 * now get one less and one more
	 * (since the map has things like 0,1,2)
	 * so we get--->
	 * 1,2
	 * 
	 * now calculate the difference between each and the diff itself.
	 * 
	 * so we get for the first one
	 * 
	 * Math.abs(1 - 1.134) --> 0.133 ==> this is less than 0.400 -> so we take it.
	 * Math.abs(2.- 1.134) --> 0.600 => not considered.
	 * 
	 * some things can still go wrong in this.
	 * 
	 * --still these are pretty safe conditions.
	 * something might falsely look like its less than 0.400.
	 * something might falsely look like its more than 0.400.
	 * 
	 * @param difference
	 * @return
	 */
	public Integer get_nearest_correlation_from_percentage_map(Double difference);
	
	public void generate_time_information();
	
	public DateTime dateString_to_dateTime();
	
	public ZonedDateTime dateString_to_zdt();
	
	/***
	 *
	 *if the present datapoint is a "last"
	 *trading day of year, month, week or quarter
	 * 
	 * this def will return the "first" trading 
	 * day of the same year, month, week, or quarter
	 */
	public abstract DataPoint get_first_of(Entity e, String last_of_time_unit,Integer current_day_id);
	
	public abstract void after_build_datapoint_hook() throws Exception;
	
	public abstract void before_commit_to_es_hook() throws Exception;
		
	/***
	 * methods for 
	 * test
	 * 
	 */
	public abstract void invalidate_datapoint_by_setting_close_to_zero(Boolean day_id_already_exists);
	
	public abstract void invalidate_datapoint_by_changing_existing_datapoint(Boolean day_id_already_exists);
	
	public abstract Boolean new_datapoint_not_added_due_to_invalidity(Entity e);
	/***
	 * methods for printing select stuff.
	 * 
	 * 
	 */
	
	//print ohlcv and datestring.
	public String print_open_high_low_close_volume_datestring();
	
	public String print_last_week_month_and_year_values() throws Exception;
	
	public DateTime datestring_to_datetime();
}
