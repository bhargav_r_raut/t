package ExchangeBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.bulk.BulkProcessor;

import DataProvider.RequestBase;

public interface IEntity {

	public abstract void before_complex_calculate() throws Exception;

	/***
	 * checks if the entity has @param days, before the day_id in the price map
	 * internal these days MUST be datapoints , and not just numerical days.
	 * used in subindicators.ndayhighlow
	 * 
	 * @param days
	 * @param day_id
	 * @return
	 * @throws Exception
	 */
	public abstract Boolean has_days_before(Integer days, Integer day_id) throws Exception;

	/***
	 * gets the day id @param days before this @param day id it means just the
	 * days: eg day_id is 10, and days is 2, then return 8.
	 */
	public abstract Integer get_day_n_days_before(Integer days, Integer day_id) throws Exception;

	/***
	 * given a day_id #day_id,and #n, it will return n datapoints before this
	 * datapoint. If #inclusive is true, then it will return n-1 datapoints, and
	 * the datapoint at #day_id as the last entry. If #inclusive is false, then
	 * will return n datapoints before the present day_id This refers to actual
	 * datapoints(days with price values) and not blank day_ids.
	 * 
	 * @param day_id
	 * @param n
	 * @return
	 * @throws Exception
	 */
	public abstract TreeMap<Integer, DataPoint> get_last_n_datapoints(Integer day_id, Integer n, Boolean inclusive)
			throws Exception;

	/******
	 * download entity from net.provider and hooks.
	 * 
	 * 
	 * 
	 * @throws Exception
	 */

	public abstract void download_entity_from_provider(BulkProcessor remote_processor, BulkProcessor local_processor)
			throws Exception;

	public abstract Boolean after_download_entity_hook(RequestBase rb, BulkProcessor remote_processor,
			BulkProcessor local_processor) throws Exception;

	public abstract Integer derive_calculate_complexes_from_day_id();

	public abstract void prune_newly_downloaded_datapoints();

	public abstract void generate_index_to_day_id_mapping();

	public abstract void update_stop_loss_open_close_arrays() throws Exception;

	public abstract void update_t_test_double_arrays();

	public abstract void update_last_day_of_week_month_and_year() throws Exception;

	public abstract void add_datapoint_to_price_map_internal(DataPoint dp, Integer day_id);

	/***
	 * 
	 * this should be called from the post calculate complexes hook.
	 * 
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws Exception
	 */

	public abstract void update_price_change_and_stop_loss_arrays_to_es(BulkProcessor remote_processor)
			throws Exception;

	public abstract void generate_price_change_buckets_and_stddev_array(BulkProcessor processor, Boolean do_buckets)
			throws Exception;

	/*****
	 * build entity from es and hooks.
	 * 
	 */

	/***
	 * called from the Entity constructor used by jackson to build the entity.
	 * 
	 * @throws Exception
	 */
	public abstract void after_build_entity_hook() throws Exception;

	public abstract Integer calculate_latest_existing_day_id();

	public abstract String calculate_start_date_string_for_request();

	public abstract String calculate_end_date_for_request();

	/*****
	 * 
	 * commit entity to elasticsearch after commit hook. used when the entity is
	 * first built from the components provider.
	 * 
	 */

	public abstract void before_commit_entity_to_elasticsearch() throws Exception;

	public abstract Boolean commit_entity_to_elasticsearch(String id, Integer index_or_update) throws Exception;

	public abstract void after_commit_entity_to_elasticsearch_hook();

	/****
	 * 
	 * miscellaneous functions
	 * 
	 */

	public abstract SortedMap<Integer, DataPoint> generate_newly_downloaded_datapoints();

	public abstract Integer generate_day_id_n_days_before_given_day_id(Integer end_day_id, Integer difference);

	public abstract SortedMap<Integer, DataPoint> generate_price_map_in_range(Integer end_day_id, Integer difference,
			Boolean end_day_id_inclusive);

	/***
	 * after calculate indicator, set its standard deviation and mean.
	 * 
	 * 
	 */
	public abstract void update_standard_deviation_and_mean_for_indicator() throws Exception;

	/****
	 * example scenario:
	 * 
	 * we have prices from 0 - 120
	 * 
	 * we have a single day change from pair 88-89 it is of 2%
	 * 
	 * we are checking it with a base stop loss of 3%
	 * 
	 * now suppose the following situations:
	 * 
	 * 1.suppose that when we had prices only till day 88 we had found a 3% stop
	 * loss, at pair 0 - 77. and so at index 0 of the stat stop loss array we
	 * had this pair index - 77.
	 * 
	 * now on day 89 we found this one day change. it is applicable for
	 * everything that start from 0 for sure. so we check what is the value in
	 * the stat stop loss array , and find 77. we also check the existing value
	 * at the trailing stop loss array, and find the filler value. so we put 77
	 * into the trailing stop loss array at this index.
	 * 
	 * henceforth there is no way that anything can change this.
	 * 
	 * 2. suppose that when we had prices till day 89 we had never found any
	 * stat stop loss of 3% so when we analyzed we see that at the existing
	 * index, there are both fillers in the stat stop loss and the tsl , so the
	 * new value is added.
	 * 
	 * 3. suppose that
	 * 
	 * 
	 */

	public abstract Boolean indicator_applicable_to_entity(String indicator_name);

	/***
	 * used in convert price map to timeseries.
	 * 
	 * @param type
	 * @param day_id
	 * @return
	 */
	public abstract String get_ohlcv(String type, Integer day_id);

	/****
	 * 
	 * entity price map to timeseries
	 * 
	 * converts the price map internal to a Ta4J timeseries. this is done in the
	 * before_calculate_indicators hook.
	 * 
	 * each datapoint in the price map internal is added as a tick in the
	 * timeseries.
	 * 
	 */
	public abstract void price_map_internal_to_timeseries();

	/***
	 * add indicator_cum_period name to the entity_initial_stub_names.
	 * 
	 * entity initial stub names is like this:
	 * 
	 * key -> bare indicator name value -> arraylist string -> Many indicator +
	 * periods name combined.
	 * 
	 */
	public abstract void add_indicator_cum_period_name_to_initial_stub_names(String indicator_name,
			String indicator_cum_period_name);

	
	/****
	 * 
	 * day id in price map internal is at arraylist index?
	 * 
	 * suppose you express all the 
	 * day ids in the price map internal as an arraylist
	 * what then is the index of a given day id?
	 * 
	 */
	public abstract Integer day_id_index(Integer day_id);
	
	
	/***
	 * how many datapoints less than and/or 
	 * inclusive of current day_id datapoint.
	 * 
	 * if inclusive is true : adds one to number of datapoints
	 * less than this day id's datapoint.
	 */
	public abstract Integer datapoints_upto_current_day_id(Integer day_id,Boolean inclusive);

	
	/***
	 * 
	 * period calculation for indicator
	 * basically the day id minus - the first day id in price map internal
	 * + 1
	 * 
	 * basically how many numerical days have passed between the current day 
	 * id and the first day id in the price map internal.
	 * 
	 * we only calculate the indicator if this period is greater
	 * than or equal to the max required period for that indicator
	 * 
	 */
	 public abstract Integer period_for_indicator_calculation(Integer day_id);
	 
	 /***
	  * checks that the price change arrays have price changes only
	  * upto expected points.
	  * for this it will take the latest day id and find the indices of it with
	  * every smaller day id, then will check that at the subsequent day to that:
	  * as long as this subsequent day is not one of the pairs, 
	  * the value is 99999(the default value)
	  */
	 public abstract ArrayList<Integer> check_price_change_arrays_for_event_logging(int[] close_close, int[] open_open);

	 public abstract void compare_stop_loss_arrays_for_event_logging(HashMap<String,int[]> arrays_before, HashMap<String,int[]> arrays_after);
}
