package ExchangeBuilder;

import java.util.ArrayList;

import org.elasticsearch.action.bulk.BulkResponse;

public interface IEntityIndex {

	public Boolean index_exists_in_database() throws Exception;
	/****
	 * 
	 * when the entity_index is first built from the calculate complexes. we
	 * commit it to elasticsearch
	 * 
	 */

	/****
	 * currently does nothing.
	 * 
	 * 
	 * @throws Exception
	 */
	public void before_commit_entity_index_to_elasticsearch() throws Exception;

	/****
	 * just commits the index to elasticsearch.
	 * 
	 * @param index_or_update:
	 *            0 if you want to do an index op, 1 if you want to do an update
	 *            op.
	 * @throws Exception
	 */
	public void commit_entity_index_to_elasticsearch(Integer index_or_update) throws Exception;

	/****************************************************************************************
	 * 
	 * 
	 * 
	 * BEFORE ENTITY_INDEX_DOWNLOAD HOOKS.
	 * 
	 * 
	 * 
	 */

	/*****
	 * initialize the bulk processor before downloading the entity index.
	 * 
	 * 
	 * 
	 */
	public abstract void initialize_remote_bulk_processor() throws Exception;

	public abstract void initialize_local_bulk_processor() throws Exception;

	/******
	 * build entity index from elasticsearch, before downloading it.
	 * 
	 * 
	 * @throws Exception
	 */

	public abstract void build_entity_index_from_elasticsearch() throws Exception;

	/****
	 * before download entity index hook
	 * 
	 * download entity index
	 * 
	 * after download entity index hook
	 * 
	 * 
	 */

	public abstract void before_download_entity_index() throws Exception;

	public abstract void download_entity_index() throws Exception;

	public abstract void after_download_entity_index() throws Exception;

	public abstract void filter_entity_map() throws Exception;

	/*******************************************************************************************
	 * 
	 * 
	 * 
	 * AFTER DOWNLOAD ENTITY INDEX HOOKS.
	 * 
	 * 
	 * 
	 */

	/***
	 * calculate entity index indicators(for all its entities)
	 * 
	 * and after calculate indicators hook.
	 */

	/***
	 * currently sets the timeseries for each entity.
	 * 
	 * @throws Exception
	 */
	public abstract void before_calculate_entity_index_indicators() throws Exception;

	public abstract void calculate_entity_index_indicators() throws Exception;

	public abstract void after_calculate_entity_index_indicators() throws Exception;

	/****
	 * calculate entity index complexes
	 * 
	 * after calculate hook.
	 * 
	 * 
	 */

	public abstract void before_calculate_entity_index_complexes() throws Exception;

	public abstract void calculate_entity_index_complexes() throws Exception;

	public abstract void after_calculate_entity_index_complexes() throws Exception;

	public abstract void calculate_entity_scores_for_complex() throws Exception;

	public abstract ArrayList<String> process_bs_update_bulk_response(BulkResponse br, Integer remote_or_local)
			throws Exception;

	/******
	 * wait for the bulk processor to shudown
	 * 
	 * 
	 * @throws Exception
	 */
	public abstract boolean shutdown_bulk_processors() throws Exception;

	public abstract void log_added_calculation_task(String task_type, Integer total_tasks_added);

	
	public abstract void log_future_retrieved(Integer total_futures_retrieved);
}
