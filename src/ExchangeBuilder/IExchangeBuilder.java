package ExchangeBuilder;

import java.util.HashMap;

public interface IExchangeBuilder {
	
	public HashMap<String,EntityIndex> download_all_exchanges() throws Exception;
	
	public HashMap<String,EntityIndex> build_all_exchanges() throws Exception;
	
	public EntityIndex download_entityExchange(String exchange_name, Integer exchange_number) throws Exception;
	
	public EntityIndex build_entityExchange(Integer exchange_number,
			String exchange_name) throws Exception;
	
	public void build_entity() throws Exception;
	
	public void build_price_change_aggs() throws Exception;
	
	
	
}
