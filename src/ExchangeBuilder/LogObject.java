package ExchangeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import ComplexProcessor.Result;
import ComplexProcessor.ResultHolder;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class LogObject extends JSONObject {

	private static final String process_name = "process_name";
	private static ObjectMapper mapper;
	private Logger logger;
	private JSONObject info_packet;
	private Marker marker;
	/****
	 * designed for period based stats reporage.
	 * 
	 * key -> period name. value -> hashmap key 1 -> eg:
	 * day_ids_rejected_due_to_no_available_start_id value 1 -> [1,2,3,4,5]
	 * 
	 * 
	 */
	public LinkedHashMap<String, LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<Object>>>> report_log_map;

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	/***
	 * returns those exceptions which have an exception level more
	 *  than or equal to the provided one.
	 * @param level
	 * @return
	 */
	public static SearchHit[] get_exceptions_with_level(Integer level_from,Integer level_to){
		SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan")
				.setTypes(StockConstants.ESTypes_error_log)
				.setFetchSource(true)
				.setSize(500)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.andFilter(
								FilterBuilders.rangeFilter(StockConstants.EsTypes_error_log_exception_type)
										.from(level_from)
										.to(level_to),
								FilterBuilders.termFilter(StockConstants.ESTypes_error_log_session_id,
										CentralSingleton.getInstance().session_start_time)

		))).execute().actionGet();
		return sr.getHits().getHits();
	}
	
	public static Boolean get_errors() {

		// return true if any exceptions are found with the current session id.
		try {
			CountResponse resp = RemoteEs.getInstance().prepareCount("tradegenie_titan")
					.setTypes(StockConstants.ESTypes_error_log)
					.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
							FilterBuilders.andFilter(
									FilterBuilders.rangeFilter(StockConstants.EsTypes_error_log_exception_type)
											.from(StockConstants.EsTypes_error_log_exception_levels
													.get(StockConstants.EsTypes_error_log_exception_type_error)),
									FilterBuilders.termFilter(StockConstants.ESTypes_error_log_session_id,
											CentralSingleton.getInstance().session_start_time)

			))).execute().actionGet();
			return resp.getCount() > 0 ? true : false;
		} catch (Exception e) {
			LogObject lge = new LogObject(LogObject.class);
			lge.commit_debug_log("failed to check exceptions in logObject");
			return true;
		}

	}

	/***
	 * constructor for payload object, to be used in base of failure in bulk
	 * processor.
	 * 
	 * so anything going into the bulk processor, should have an entity unique
	 * name and a doc id.
	 * 
	 * 
	 * @param cls
	 * @param payload
	 */
	public LogObject(Class<?> cls, HashMap<String, Object> payload) {
		try {
			if (payload.containsKey(StockConstants.Entity_unique_name)) {
				put(StockConstants.LogObject_EntityName, (String) payload.get(StockConstants.Entity_unique_name));
			}
			if (payload.containsKey(StockConstants.EntityIndex_bulk_processor_doc_id)) {
				put(StockConstants.EntityIndex_bulk_processor_doc_id,
						(String) payload.get(StockConstants.EntityIndex_bulk_processor_doc_id));
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(this.getClass()).error("", e);
		}
	}

	public LogObject(Class<?> cls) {
		this.logger = LoggerFactory.getLogger(cls);
		this.info_packet = new JSONObject();
		mapper = new ObjectMapper();
	}

	public LogObject(Class<?> cls, EntityIndex ei) {
		this(cls);
		log_entity_index(ei);
	}

	public LogObject(Class<?> cls, EntityIndex ei, Entity e) {
		this(cls);
		log_entity_index(ei);
		log_entity(e);
	}

	public LogObject(Class<?> cls, Entity e) {
		this(cls);
		log_entity(e);
	}

	public LogObject(Class<?> cls, DataPoint dp) {
		this(cls);
		log_datapoint(dp);
	}

	public LogObject(Class<?> cls, Result r) {
		this(cls);
		log_result(r);
	}

	public LogObject(Class<?> cls, ResultHolder res_holder) {
		this(cls);
		log_result_holder(res_holder);

	}

	private void log_entity(Entity entity) {
		try {
			put(StockConstants.EntityIndex_name, entity.getIndice());
			put(StockConstants.Entity_unique_name, entity.getUnique_name());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();
		}
	}

	private void log_result_holder(ResultHolder res_h) {
		try {
			put(StockConstants.Result_results_for_indicator, res_h.getIndicator_name());
			put(StockConstants.Result_subindicator_name, res_h.getSubindicator_name());
			log_entity_name(res_h.getEntity_unique_name());

		} catch (Exception e) {
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();

		}
	}

	private void log_result(Result r) {
		try {
			put(StockConstants.Result_results_for_indicator, r.getIndicator_stub_name());
			put(StockConstants.Result_subindicator_name, r.getSubindicator_name());
			log_entity_name(r.getEntity_unique_name());

		} catch (Exception e) {
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();

		}

	}

	private void log_entity_index(EntityIndex ei) {
		try {
			put(StockConstants.EntityIndex_name, ei.getIndex_name());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();
		}
	}

	private void log_datapoint(DataPoint dp) {
		try {
			put(StockConstants.DataPoint_name, dp.getDateString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LoggerFactory.getLogger(this.getClass()).error("", e);
		}

	}

	public void log_entity_name(String entity_unique_name) {
		try {
			put(StockConstants.Entity_unique_name, entity_unique_name);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void add_to_info_packet(String key, Object value) {

		try {
			this.info_packet.put(key, value);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();
		}

	}

	public void commit_report_log(String status) {
		for (String initial_stub_name : this.report_log_map.keySet()) {
			for (Integer period : this.report_log_map.get(initial_stub_name).keySet()) {
				for (Map.Entry<String, ArrayList<Object>> entry : this.report_log_map.get(initial_stub_name).get(period)
						.entrySet()) {

					if (!entry.getValue().isEmpty()) {
						Integer max_value = (Integer) entry.getValue().get(entry.getValue().size() - 1);
						Integer min_value = (Integer) entry.getValue().get(0);
						entry.setValue(
								new ArrayList<Object>(Arrays.asList(min_value, max_value, entry.getValue().size())));
					}

				}

			}
		}

		try {
			this.logger.info(status + "----" + this.toString(5) + "----" + this.info_packet.toString(5) + "----"
					+ CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter()
							.writeValueAsString(report_log_map));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void commit_info_log(String status) {
		try {
			this.logger.info(status + this.toString(5) + "-- " + this.info_packet.toString(5));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void commit_error_log(String status, Throwable e) {
		try {
			// assess the info packet for the entity unique name.
			// post that error to the
			this.logger.error(status + this.toString(5) + "-- " + this.info_packet.toString(5), e);

			ErrorLog er = new ErrorLog(CentralSingleton.getInstance().session_start_time, info_packet, status, e);
			er.commit();

		} catch (Exception e1) {
			LogObject lg_d = new LogObject(LogObject.class);
			lg_d.commit_debug_log("failed to commit error log to es.");
		}

	}

	public void commit_debug_log(String status) {
		try {
			this.logger.debug(status + this.toString(5) + "-- " + this.info_packet.toString(5));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	class ErrorLog {

		private Integer exception_type;

		public Integer getException_type() {
			return exception_type;
		}

		public void setException_type(Integer exception_type) {
			this.exception_type = exception_type;
		}

		private String entity_unique_name;

		public String getEntity_unique_name() {
			return entity_unique_name;
		}

		public void setEntity_unique_name(String entity_unique_name) {
			this.entity_unique_name = entity_unique_name;
		}

		private Long session_id;

		public Long getSession_id() {
			return session_id;
		}

		public void setSession_id(Long session_id) {
			this.session_id = session_id;
		}

		private LinkedList<Map<String, Object>> info_packet;

		private String exception;

		public String getException() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}

		public LinkedList<Map<String, Object>> getInfo_packet() {
			return info_packet;
		}

		/****
		 * the info packet of the log object itself is used to discover things
		 * like the enity name, and an arbitary pair of keys and values.
		 * 
		 * 
		 * @param info_packet_j
		 * @throws JSONException
		 */
		public void setInfo_packet(final JSONObject info_packet_j) throws JSONException {
			this.info_packet = new LinkedList<Map<String, Object>>();
			final Iterator<String> info_packet_iterator = info_packet_j.keys();
			while (info_packet_iterator.hasNext()) {
				final String key = info_packet_iterator.next();
				if (key.equals(StockConstants.ESTypes_error_log_entity_unique_name)) {
					setEntity_unique_name(info_packet_j.getString(key));
				}
				if (key.equals(StockConstants.EsTypes_error_log_exception_type)) {
					setException_type(info_packet_j.getInt(key));
				}
				Map<String, Object> entry = new LinkedHashMap<String, Object>() {
					{
						put(StockConstants.ESTypes_error_log_info_packet_key, key);
						put(StockConstants.ESTypes_error_log_info_packet_value, String.valueOf(info_packet_j.get(key)));
					}
				};
				this.info_packet.add(entry);
			}
		}

		private String status;

		public ErrorLog(Long session_id, JSONObject info_packet_j, String status, Throwable e) throws Exception {

			this.session_id = session_id;
			/***
			 * the exception type is by default set to error.
			 * if the info packet contains an exception_type, then that is assigned
			 * in setInfo_Packet, thus overriding this default.
			 */
			this.exception_type = StockConstants.EsTypes_error_log_exception_levels
					.get(StockConstants.EsTypes_error_log_exception_type_error);
			setInfo_packet(info_packet_j);
			this.status = status;
			this.exception = e.getLocalizedMessage();
			

		}

		public void commit() throws Exception {

			System.out.println("came to commit error log.");
			IndexResponse resp = RemoteEs.getInstance()
					.prepareIndex("tradegenie_titan", StockConstants.ESTypes_error_log)
					.setSource(mapper.writeValueAsBytes(this)).setTimeout("5s").setRefresh(true).execute().actionGet();
			System.out.println("finished commit error log");
			if (!resp.isCreated()) {
				throw new Exception("failed to commit the error log.");
			}

		}

	}

}
