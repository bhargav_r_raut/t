package ExchangeBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;
import yahoo_finance_historical_scraper.StockConstants;
import IdAndShardResolver.IdAndShardResolver;

public class PairArray {
	
	public static int default_value_for_large_array_indices = 99999;
	
	private Integer pair_difference_array_index;

	private Integer start_day_id_based_index;

	public Integer getPair_difference_array_index() {
		return pair_difference_array_index;
	}

	public Integer getStart_day_id_based_index() {
		return start_day_id_based_index;
	}

	public PairArray(Integer pair_difference_array_index, Integer start_day_id_based_index) {
		this.pair_difference_array_index = pair_difference_array_index;
		this.start_day_id_based_index = start_day_id_based_index;
	}

	/***
	 * 
	 * TESTED given array name:***
	 * 
	 * if no array exists, returns a new array of default size.*** if the array
	 * type is sorted, then run filler function on each day of array before
	 * returning it.*** if the array type is unsorted, then it gets its default
	 * as 0 for all elements.
	 * 
	 * if array exists, then creates an appropriate integercodec and
	 * decompresses the array and returns the decompressed array.***
	 * 
	 * type 0: unsorted ***
	 * 
	 * type 1:sorted ***
	 * 
	 * @param name
	 * @param type
	 * @param default_size
	 * @return
	 */
	public static int[] get_decompressed_arr(String name, Integer type, Integer default_size) {

		int[] compressed_arr = CentralSingleton.getInstance().query_time_pair_lookup_map.get(name);
		if (compressed_arr == null) {
			// System.out.println("found that this array did not exist:" +
			// name);
			int[] new_array = new int[default_size];
			if (type.equals(0)) {

				for (int i = 0; i < new_array.length; i++) {
					new_array[i] = default_value_for_large_array_indices;
				}
				// System.out
				// .println("returned a default unsorted array of size: "
				// + new_array.length + "\n");

			} else {
				for (int i = 0; i < default_size; i++) {

					new_array[i] = get_filler_id_for_sorted_map(i);

				}
				// System.out.println("returned a default sorted array with
				// size:"
				// + new_array.length + "\n");

			}
			return new_array;
		}

		return get_decompressed_array_from_compressed_array(compressed_arr, default_size, type);

	}

	/****
	 * given a compressed array, and its type,(whether unsorted or sorted)
	 * returns a decompressed array.
	 * 
	 * 
	 * WARNING : THIS METHOD IS USED IN SPRINGAPP - APPSIDE.
	 * 
	 * 
	 * @param compressed_arr
	 * @param expected_length_of_decompressed_array
	 * @param type
	 * @return
	 */
	public static int[] get_decompressed_array_from_compressed_array(int[] compressed_arr,
			int expected_length_of_decompressed_array, Integer type) {

		int[] recovered = new int[expected_length_of_decompressed_array];

		if (type.equals(0)) {
			// unsorted
			IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
			IntWrapper recoffset = new IntWrapper(0);
			codec.uncompress(compressed_arr, new IntWrapper(0), compressed_arr.length, recovered, recoffset);
			// System.out
			// .println("invoking decompression of unsorted array and returned
			// \n");

		} else {
			// sorted
			IntegratedIntegerCODEC codec = new IntegratedComposition(new IntegratedBinaryPacking(),
					new IntegratedVariableByte());
			recovered = new int[expected_length_of_decompressed_array];
			IntWrapper recoffset = new IntWrapper(0);
			codec.uncompress(compressed_arr, new IntWrapper(0), compressed_arr.length, recovered, recoffset);
			// System.out
			// .println("invoking decompression of sorted array and returned
			// \n");
		}

		return recovered;

	}

	/****
	 * returns an array of name #name from the centralsingleton map holding all
	 * the arrays, does not decompress the array, returns as is. i.e compressed.
	 * 
	 * 
	 * @param name
	 * @return int[]
	 * @throws Exception
	 */
	public static int[] get_compressed_arr(String name) throws Exception {
		/**
		 * System.out.println("printing names"); System.out.println(
		 * "required name is:" + name); for(Map.Entry<String, int[]> entry:
		 * CentralSingleton.getInstance().query_time_pair_lookup_map
		 * .entrySet()){ System.out.println(entry.getKey()); System.out.println(
		 * "the int[] value is "); if(entry.getValue() == null){
		 * System.out.println("null"); } else{ System.out.println("not null"); }
		 * }
		 **/

		int[] compressed_arr = CentralSingleton.getInstance().query_time_pair_lookup_map.get(name);
		/**
		 * System.out.println("is the returned array null?");
		 * System.out.println(compressed_arr.length); System.in.read();
		 **/
		return compressed_arr;

	}

	/***
	 * TESTED
	 * 
	 * @param name
	 * @param type
	 * @param default_size
	 * @param index
	 * @return
	 */
	public static int get_value_from_arr_at_index(String name, Integer type, Integer default_size, int index) {
		int[] arr = get_decompressed_arr(name, type, default_size);
		return arr[index];
	}

	/***
	 * TESTED
	 * 
	 * 
	 * @param arr
	 * @param name
	 * @param type
	 *            : 0 unsorted, 1 sorted
	 */
	public static void put_arr(int[] arr, String name, int type) {
		int[] compressed_arr = get_compressed_int_array(arr, type);
		CentralSingleton.getInstance().query_time_pair_lookup_map.put(name, compressed_arr);
	}

	/****
	 * for any given start day , end day id pair. the following algorithm is to
	 * be run to determine the location of the tsl.
	 * 
	 * @step: Check the existing tsl array value for this start day id. If it is
	 *        less than the index of the start_end day pair, then do not
	 *        proceed.
	 * 
	 * @step: iterate each day from start_day_id -> end_day_id.
	 * 
	 * @step: the first day during which the day_id - start_day_id price is
	 *        greater by at least 0.5 percent than the tsl amount - store it(the
	 *        day id and the price)(tsl level) no need to check the bsl. it will
	 *        be checked anyway at query time so it doesnt matter.
	 * 
	 * @step: for each subsequent day, there are three possiblities: 1. the
	 *        price goes below the tsl_level - say that tsl was triggered -
	 *        store this index 2. the price goes above the earlier price - reset
	 *        the tsl level, and that day id. 3. the price remains in between. -
	 *        do nothing.
	 * 
	 * @implications: 1. so when we have tsl -> two array lookups are needed. a.
	 *                first look at tsl -> if it has been triggered, then apply
	 *                that tsl price change(not the change in the price, but the
	 *                tsl amount, because change in price can be anything.) b.
	 *                if it is not registered for tsl -> there are two
	 *                possibilities 1. it was pre-empted by a bsl and so we have
	 *                to look into the bsl and see if we have a bsl at all or
	 *                not. 2. there is no bsl also, so we can go ahead and apply
	 *                the basic price change.
	 */
	public static void tsl_redefined() {

	}

	/****
	 * @step one: WHy do we need the combination of bsl and tsl
	 * 
	 *       consider a situation where for a given single day change
	 * 
	 *       i.e
	 * 
	 *       pair 56 -> 57, there is a fall of 2%. and that the trader has set a
	 *       trailing stop loss of 2%. now why do we need to differentiate
	 *       between trailing stop loss and base stop loss. suppose all that you
	 *       set was a trailing stop loss. of say 2%. the market never falls by
	 *       2% on any single day , but falls by 0.5% everyday. your tsl never
	 *       gets hit. but you will eventually be in a loss. So for additional
	 *       protection you will have to set a base stop loss of something, that
	 *       is calculated relative to your entry price.
	 * 
	 * 
	 * @step two:
	 * 
	 *       first we get the full array of all the unsorted price changes. this
	 *       is stored in the variable:
	 * @unsorted_arr_of_price_changes
	 * 
	 * 								now into this function is also passed the
	 *                                smallest and greatest available day ids
	 *                                with us.
	 * 
	 * @step three; iterate from smallest day id , to one day less than greatest
	 *       day id. this is because we make single day change pairs, of start
	 *       day id => (start day id + 1)
	 * @then get the index in the unsorted day change map of that pair.
	 * @then from the treemap of the integer -> percentagecorrelation that is
	 *       passed in, (this is calculated while calculating the open_open and
	 *       close_close price changes, and passed into the function as @tm) for
	 *       each day id, it contains the percentagecorrelation object which
	 *       contains as a boolean array the stop losses the changes(open-open
	 *       and close-close) on that particular day qualify for. so from this
	 *       tm , get the relevant stop losses qualified for on that day for
	 *       either open or close(whichever is sent into the function also as
	 * @open_or_close).
	 * 
	 * 					now iterate that array of boolean stop loss applicable
	 *                  changes.
	 * @if one of them is @true, then it means that this day change is
	 *     applicable to that stop loss.
	 * 
	 *     -now get the actual tsl amount, using the index of the boolean and
	 *     refering to the Entity_stop_loss_amounts constant array, set this
	 *     into the int variable @tsl
	 * 
	 *     now run the function that returns which bsl are applicable to that
	 *     tsl, and store in the array
	 * @applicable_base_stop_loss_amounts
	 * 
	 * 									now comes the fun part. :)
	 * 
	 *                                    we say that for a given single day
	 *                                    change, the only market entries that
	 *                                    it can affect in teh form of a tsl are
	 *                                    the day ids which begin before its end
	 *                                    day id.(i.e for single day change
	 *                                    56->57, all day ids from 0 -> 56 are
	 *                                    affected by this day change.)
	 * 
	 * 
	 *                                    so what we do now is first accumulate
	 *                                    all these day ids, by calling the
	 *                                    function
	 *                                    get_sorted_arr_indices_affected_by_single_day_change
	 *                                    (i+1) here the i , is the smallest to
	 *                                    largest day id iterator. for a given
	 *                                    day id, we want to generate all day
	 *                                    ids which could have been affected by
	 *                                    its single day change. this obviously
	 *                                    includes it itself, hence i + 1
	 *                                    because this function generates
	 *                                    everything upto 1 day less than the
	 *                                    provided day id(so that it will
	 *                                    generate upto i)
	 * 
	 *                                    now we iterate all these "affected"
	 *                                    day ids.
	 * 
	 *                                    this fucntion can generate negative
	 *                                    values(because it minuses 300 from the
	 *                                    provided end day id) so when iterating
	 *                                    we make sure that we are only taking
	 *                                    values above the smallest day id that
	 *                                    we have.
	 * 
	 * @now we generate the index in the unsorted price changes map, using the
	 *      day which is being iterated (the affected day) and as the end day id
	 *      , the actual end day id(i + 1) store the price change at that index
	 *      in a variable called #value_at_that_index
	 * 
	 * @now we iterate all the base stop losses that are applicable to the
	 *      current tsl.
	 * 
	 *      now for a given bsl, the notations are important:
	 * 
	 *      suppose that you have a negative base stop loss(i.e you dont want to
	 *      loose on the down side.) (in the program referred to as bsl < 0)
	 *      here we need to be sure, that #value_at_that_index is greater than
	 *      the bsl. this is because suppose you set a downside stop loss of 5
	 *      percent, and we have now hit a tsl of say 2% , it is only relevant
	 *      to say that the tsl should be used to exit, provided that we are
	 *      still in profit, because otherwise by corollary, this day would
	 *      qualify for the bsl anyways.
	 * 
	 *      so basically we check whether the difference from the affected day
	 *      to the end day(i + 1) is less than the bsl.
	 * 
	 *      if yes, then we check whether already at this point in the tsl index
	 *      we have a smaller (value) already stored. remebert taht these values
	 *      are just pointers / indexes to the values in the big unsorted pairs
	 *      map.
	 * 
	 *      if there is no smaller value , then store the index calculated above
	 *      otherwise leave it as is.
	 * 
	 *      it is opposite for bsl > 0
	 * 
	 *      *finally remember that you have to multiply the bsl*10 to do the
	 *      comparison for < or > because price changes are as usual in 10
	 *      multiple , but bsl represents 2 percent as 2.
	 * 
	 * 
	 * @else dont do anything because the boolean is false, so the change on
	 *       this day is not applicable as a tsl.
	 * 
	 * @param unique_name
	 * @param open_or_close
	 * @return
	 */
	public static int[] get_single_day_changes_arr_for_stock(String unique_name, String open_or_close,
			Integer greatest_available_day_id, TreeMap<Integer, PercentageIntegerCorrelate> tm,
			HashMap<String, int[]> hm_of_combined_stop_losses, Integer smallest_available_day_id, Entity e) {

		// System.out
		// .println("entered to get the single day changes arr for stock");
		LogObject lgds = new LogObject(PairArray.class);
		lgds.add_to_info_packet("latest newly downloaded day id is:",
				e.generate_newly_downloaded_datapoints().lastKey());
		lgds.add_to_info_packet("its index pair is", get_int_array_index_given_start_day_id_and_difference(
				e.generate_newly_downloaded_datapoints().lastKey(), 1).getPair_difference_array_index());
		lgds.add_to_info_packet("latest day in tm is", tm.lastKey());
		// lgds.commit_debug_log("see info packet");

		String name_of_arr = unique_name + "_" + open_or_close;

		// System.out.println("name of arr is:" + name_of_arr);

		int[] unsorted_arr_of_price_changes = get_decompressed_arr(name_of_arr, 0,
				StockConstants.Entity_total_pairs_for_a_day
						* CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());
		// now from this compose the list of all the existing total price
		// changes.
		// TODO
		// iterate the keyset of the price map internal.
		// we cant do the last day, because we dont have the next day to build a
		// single day change from.
		ArrayList<Integer> day_ids = new ArrayList<Integer>(e.generate_newly_downloaded_datapoints().keySet());
		day_ids.remove(day_ids.size() - 1);
		for (Integer i : day_ids) {

			// TODO
			// THIS CAN GO FORWARD AS IS.
			LogObject lgd = new LogObject(PairArray.class);
			lgd.add_to_info_packet("day id in price map internal", i);

			Integer index_in_unsorted_map = get_int_array_index_given_start_day_id_and_difference(i, 1)
					.getPair_difference_array_index();

			PercentageIntegerCorrelate pc = tm.get(index_in_unsorted_map);
			if (pc != null) {
				boolean[] pc_barr = pc.get_applicable_stop_loss_array(open_or_close);
				for (int b = 0; b < pc_barr.length; b++) {
					// if a particular b is true, then , we have a trailing stop
					// loss.
					// System.out.println("the index of the boolean array tested
					// is:"
					// + b);
					if (pc_barr[b] == true) {
						// System.out.println("its true.");
						// now get all its applicable base stop losses.
						int tsl = StockConstants.Entity_stop_loss_amounts[b];
						// System.out.println("so the tsl is:" + tsl);

						Integer[] applicable_base_stop_loss_amounts = get_applicable_base_stop_loss_amounts_for_a_tsl(
								tsl);

						// System.out.println("applicable bsl are:"
						// + applicable_base_stop_loss_amounts);

						// TODO
						// HERE GET THE KEYSET OF THE HEADMAP OF PRICEMAP
						// INTERNAL
						// UPTO i+1

						int[] arr_of_st_day_ids = get_sorted_arr_indices_affected_by_single_day_change(i + 1, e);

						// System.out
						// .println("these are the applicable start day ids.");

						// now for each start day id, compose a pair upto the
						// said end day id, and
						// then ensure that that change is
						for (int si : arr_of_st_day_ids) {
							if (si >= smallest_available_day_id) {
								// System.out
								// .println("now doing pair of start day id to
								// the
								// end day id of single day change:");
								// System.out.println(String.valueOf(si) + "_"
								// + String.valueOf(i + 1));

								Integer index_in_unsorted_map_for_this_pair = get_int_array_index_given_start_and_end_day_id(
										si, i + 1, e).getPair_difference_array_index();
								// System.out.println("its index in unsorted map
								// is:"
								// + index_in_unsorted_map_for_this_pair);

								Integer value_at_that_index = unsorted_arr_of_price_changes[index_in_unsorted_map_for_this_pair];

								// System.out
								// .println("the change of price at that index
								// is:"
								// + value_at_that_index);

								// System.out
								// .println("now going into check each
								// applicable
								// bsl");
								for (Integer bsl : applicable_base_stop_loss_amounts) {

									// System.out.println("bsl multi 10 is:" +
									// bsl
									// * 10);
									if (bsl > 0) {
										if (value_at_that_index < bsl * 10) {
											// System.out
											// .println("so the value at the
											// index
											// is less than the bsl multi,"
											// +
											// " so we replace at the said index
											// of
											// the tsl if the tsl condition is
											// satisfied");
											// make the modification.
											// name of array will be .
											String name_of_tsl_arr = get_name_of_relevant_trailing_stop_loss_arr(
													unique_name, open_or_close, tsl, bsl);
											// System.out.println("name of tsl
											// is: "
											// + name_of_tsl_arr);
											// if the tsl at the start index is
											// System.out
											// .println("the value at the tsl at
											// this start index is: "
											// + hm_of_combined_stop_losses
											// .get(name_of_tsl_arr)[si]);

											if (hm_of_combined_stop_losses
													.get(name_of_tsl_arr)[si] > index_in_unsorted_map_for_this_pair) {
												// System.out
												// .println("so its replaced.");
												if (open_or_close
														.equals(StockConstants.Entity_close_close_map_suffix)) {

												}
												hm_of_combined_stop_losses
														.get(name_of_tsl_arr)[si] = index_in_unsorted_map_for_this_pair;
											}
										}
									} else {
										if (value_at_that_index > bsl * 10) {
											String name_of_tsl_arr = get_name_of_relevant_trailing_stop_loss_arr(
													unique_name, open_or_close, tsl, bsl);
											// if the tsl at the start index is
											if (hm_of_combined_stop_losses
													.get(name_of_tsl_arr)[si] > index_in_unsorted_map_for_this_pair) {
												hm_of_combined_stop_losses
														.get(name_of_tsl_arr)[si] = index_in_unsorted_map_for_this_pair;
											}
										}
									}
								}

							}
						}
					}
				}
			}
		}

		return null;
	}

	/*****
	 * methodology for each stock
	 * 
	 * store three arrays, compressed and unsorted. 1 week change -> plus
	 * 1_week_change_graph [holds the actual unsorted changes.] 1 month change
	 * 15 day change
	 * 
	 * those will be 1000 elements - so need to benchmark that.
	 * 
	 * also store.
	 * 
	 * 
	 * 
	 * @return
	 */

	public static Integer[] get_applicable_base_stop_loss_amounts_for_a_tsl(int tsl) {

		ArrayList<Integer> ret = new ArrayList<Integer>();
		for (int sl : StockConstants.Entity_stop_loss_amounts) {
			if (tsl < 0) {
				if (sl <= tsl) {
					ret.add(sl);
				}

			} else {
				if (sl >= tsl) {
					ret.add(sl);
				}
			}
		}

		Integer[] reti = new Integer[ret.size()];
		reti = ret.toArray(reti);
		return reti;
	}

	/****
	 * TESTED TAKES THE GIVEN UNCOMPRESSED ARRAY AND COMPRESSES IT DEPENDING ON
	 * ITS TYPE.
	 * 
	 * 
	 * @param data
	 * @param type
	 *            : 0 unsorted, 1 sorted
	 * @return
	 */
	public static int[] get_compressed_int_array(int[] data, int type) {
		if (type == 0) {
			final int N = data.length;
			int[] compressed = new int[N + 1024];// could need more
			IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());

			IntWrapper inputoffset = new IntWrapper(0);
			IntWrapper outputoffset = new IntWrapper(0);
			codec.compress(data, inputoffset, data.length, compressed, outputoffset);

			compressed = Arrays.copyOf(compressed, outputoffset.intValue());
			// System.out.println("compressed unsorted integers from "
			// + data.length * 4 / 1024 + "KB to "
			// + outputoffset.intValue() * 4 / 1024 + "KB");
			// System.out.println("invoked compression of unsorted arary");
			return compressed;
		} else {
			IntegratedIntegerCODEC codec = new IntegratedComposition(new IntegratedBinaryPacking(),
					new IntegratedVariableByte());

			int[] compressed = new int[data.length + 1024];

			IntWrapper inputoffset = new IntWrapper(0);
			IntWrapper outputoffset = new IntWrapper(0);
			codec.compress(data, inputoffset, data.length, compressed, outputoffset);

			// System.out.println("compressed from " + data.length * 4 / 1024
			// + "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");

			compressed = Arrays.copyOf(compressed, outputoffset.intValue());
			// System.out.println("invoked compression of sorted array:");
			return compressed;
		}
	}

	/***
	 * 
	 * THIS IS TO BE USED WHEN YOU WANT THE MATHEMATICAL INDEX, USE ONLY WHEN
	 * YOU WANT THE INDEX AT A CERTAIN RELATIVE POSITION FROM THE START DAY ID.
	 * 
	 * EG:
	 * 
	 * START_DAY_ID: 0
	 * 
	 * WHAT IS THE INDEX AT 200 DAYS FROM THE START DAY ID? THIS FUNCTION CAN
	 * PROVIDE THE ANSWER.
	 * 
	 * IT IS CALLED IN {@link #get_filler_id_for_sorted_map(int)};
	 * {@link #get_single_day_changes_arr_for_stock(String, String, Integer, TreeMap, HashMap, Integer)}
	 * in the line: (Integer index_in_unsorted_map =
	 * get_int_array_index_given_start_and_end_day_id( i, i +
	 * 1).getPair_difference_array_index();)
	 * 
	 * in both these cases, we just want the position in the unsorted array of
	 * the given start day id and end day id pair.
	 * 
	 * what it actually means is as follows: eg: given these actual day ids:
	 * 
	 * 0,1,3,5,19
	 * 
	 * so here: pairs are:
	 * 
	 * 0_1 => 1 - 0, 0_2 => 3 - 0, 0_3 => 5 - 0, 0_4 => 19 -0,
	 * 
	 * so when you say give start-day_id as: 0 and difference as 4, you are
	 * actually getting the index in the unsorted map which would reference the
	 * calculated difference of 19 - 0.
	 * 
	 * 
	 * 
	 * @param start_day_id
	 * @param difference
	 * @return
	 */
	public static PairArray get_int_array_index_given_start_day_id_and_difference(Integer start_day_id,
			Integer difference) {

		Integer end_day_id = start_day_id + difference;
		Integer index_in_pair_differences_format = ((start_day_id * (StockConstants.Entity_total_pairs_for_a_day - 1))
				+ end_day_id - 1);

		Integer index_in_start_day_id_format = start_day_id;

		// System.out.println("returns index:" +
		// index_in_pair_differences_format);
		return new PairArray(index_in_pair_differences_format, index_in_start_day_id_format);
	}

	/***
	 * 
	 *
	 * 
	 * 
	 * TESTED
	 * 
	 * assumen that we take only 5 days back.
	 * 
	 * suppose start day id is 0 and end day id is 8 5 days back will give you 3
	 * as the start day id. so we need to instead minus 3 from the end day id.
	 * and that is the correct pair that is generated. the big array is built
	 * considering each pair to actually be a valid price change. it does not
	 * consider the pairs to include the non-price holding days. we do not
	 * correct, we say if the difference is greater then we dont calculate the
	 * price change
	 * 
	 * 
	 * our formula is caliberated based on the
	 * stockconstants.entity_total_pairs_in_a_day since here the end day id -
	 * start day id is greater than that, using them directly will lead to an
	 * index which actually refers to another start_day_id -> end_day_id
	 * pair.(that of a greater start day id.)
	 * 
	 * so we have to correct the end day id, we do this by finding the position
	 * in the index_mapping treemap for the start day id and the end day. then
	 * we get the difference in their positions.
	 * 
	 * then we say that the corrected end day id is the start_day_id +
	 * differnce.
	 * 
	 * provided that this difference is less than the
	 * stockconstants.entity_total_pairs_for_any_given_day
	 * 
	 * if not, then an error is thrown, this is just for protection because it
	 * is expected that we will always ask for values that are actually within
	 * {@link StockConstants#Entity_total_pairs_for_a_day}
	 * 
	 * this function is called from
	 * {@link Entity#update_stop_loss_open_close_arrays()} , where we have first
	 * used the index_mapping function to get start_day_ids that are within 300
	 * days of the end day id,(this is done by simply saying take the end_day_id
	 * and get its position and then return everything that is 300 positions
	 * less than it.) here we now want to know the index in the unsorted map of
	 * each of these pairs(start_day_id -> end_day_id), so we call this
	 * funciton. this function is called in a similar manner from
	 * #IndicatorRiseFallAmounts ,#IndicatorStandardDeviation where we have an
	 * end day id, and want a start day id, that is n periods before it, and
	 * then we want its index in the unsorted map. This function is also called
	 * in the same way from
	 * {@link #get_single_day_changes_arr_for_stock(String, String, Integer, TreeMap, HashMap, Integer)}
	 * in the line: (Integer index_in_unsorted_map_for_this_pair =
	 * get_int_array_index_given_start_and_end_day_id( si, i +
	 * 1).getPair_difference_array_index(); )
	 * 
	 * 
	 * 
	 * @param start_day_id
	 * @param end_day_id
	 * @return
	 */
	public static PairArray get_int_array_index_given_start_and_end_day_id(Integer start_day_id, Integer end_day_id,
			Entity e) {

		/**
		 * Integer position_of_start_day_id = e.getPrice_map_internal()
		 * .headMap(start_day_id).size(); Integer position_of_end_day_id =
		 * e.getPrice_map_internal() .headMap(end_day_id).size(); Integer
		 * difference_in_positions = position_of_end_day_id -
		 * position_of_start_day_id; Integer corrected_end_day_id = start_day_id
		 * + difference_in_positions;
		 **/

		// Integer index_in_pair_differences_format =
		// ((start_day_id * (StockConstants.Entity_total_pairs_for_a_day - 1))
		// + end_day_id - 1);

		// Integer index_in_start_day_id_format = start_day_id;

		// System.out.println("returns index:" +
		// index_in_pair_differences_format);
		// return new PairArray(index_in_pair_differences_format,
		// index_in_start_day_id_format);
		return get_int_array_index_given_start_day_id_and_difference(start_day_id, end_day_id - start_day_id);
	}

	/***
	 * TESTED
	 * 
	 * @param pair_differences_index
	 * @return
	 */
	public static int pair_differences_index_to_start_day_id(int pair_differences_index) {
		// just do a pure integer division.
		return (pair_differences_index) / (StockConstants.Entity_total_pairs_for_a_day);

	}

	public static HashMap<String, Integer> pair_differences_index_start_and_end_day_id(int pair_differences_index) {
		HashMap<String, Integer> start_and_end_day_ids = new HashMap<String, Integer>();
		Integer start_day_id = PairArray.pair_differences_index_to_start_day_id(pair_differences_index);
		Integer difference = pair_differences_index % StockConstants.Entity_total_pairs_for_a_day;
		Integer end_day_id = start_day_id + difference + 1;
		start_and_end_day_ids.put("start_day_id", start_day_id);
		start_and_end_day_ids.put("end_day_id", end_day_id);
		return start_and_end_day_ids;
	}

	/***
	 * TESTED
	 * 
	 * the filler index is basically the first index of the next start_day_id to
	 * this one.***
	 * 
	 * so if current start day id is 10 then filler will return the index in the
	 * unsorted map of the first pair of start day id 11.***
	 * 
	 * 
	 * @param index_in_sorted_map
	 * @return
	 */
	public static int get_filler_id_for_sorted_map(int index_in_sorted_map) {
		// just multiply it by the total pairs.
		return get_int_array_index_given_start_day_id_and_difference(index_in_sorted_map,
				StockConstants.Entity_total_pairs_for_a_day).getPair_difference_array_index() + 1;
	}

	/***
	 * 
	 * TESTED
	 * 
	 * @param unique_name
	 * @return
	 */
	public static HashMap<String, int[]> get_hashmap_of_all_stat_stop_loss_arrays(String unique_name) {

		HashMap<String, int[]> hm_all = new HashMap<String, int[]>();

		String[] open_open_or_close_close = { StockConstants.Entity_open_open_map_suffix,
				StockConstants.Entity_close_close_map_suffix };
		String name_of_relevant_stat_stop_loss_int_array = null;

		for (Integer stop_loss_amount : StockConstants.Entity_stop_loss_amounts) {
			for (String open_open_or_close_close_name : open_open_or_close_close) {
				name_of_relevant_stat_stop_loss_int_array = get_name_of_relevant_stat_stop_loss_map(unique_name,
						open_open_or_close_close_name, stop_loss_amount);
				hm_all.put(name_of_relevant_stat_stop_loss_int_array,
						get_decompressed_arr(name_of_relevant_stat_stop_loss_int_array, 1,
								CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()));
			}
		}

		return hm_all;

	}

	public static HashMap<String, int[]> get_hashmap_of_all_trailing_stop_loss_arrays(String unique_name,
			HashMap<String, int[]> hashmap_of_all_stat_stop_loss_arrays) {

		String[] open_open_or_close_close = { StockConstants.Entity_open_open_map_suffix,
				StockConstants.Entity_close_close_map_suffix };
		for (String oo_cc : open_open_or_close_close) {

			for (Integer bsl : StockConstants.Entity_stop_loss_amounts) {
				// now we want int array of all applicable trailing stop loss
				// arrays.
				// int[] applicable_tsl =
				// get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(bsl);
				// for (int ts : applicable_tsl) {

				String name_of_tsl_array = get_name_of_relevant_trailing_stop_loss_arr(unique_name, oo_cc, bsl, bsl);

				int[] tsl_array_from_concurrent_hashmap = get_decompressed_arr(name_of_tsl_array, 1,
						CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

				hashmap_of_all_stat_stop_loss_arrays.put(name_of_tsl_array, tsl_array_from_concurrent_hashmap);
				// }
			}

		}

		return hashmap_of_all_stat_stop_loss_arrays;
	}

	/***
	 * so basically in the stat map we store the index of the larger map, which has a change 
	 * equal to or more than the stop loss that we are looking for.
	 * eg small map index : 5
	 * large map index : 100392 (corresponds to pair 5 -> 900)
	 * large map at that index holds the value of the percentage change in the price 
	 * small map at that index(5) holds the large map index 100392
	 * if we find that for a small map index , we are getting a smaller index of the 
	 * large map(eg. 9999) then we replace the existing value at this index(5) in the
	 * small map with the new value of 9999.
	 * 
	 * example:
	 * 
	 * suppose that in the main calculation in entity you find that for pair
	 * 
	 * 4 -> 286 (there is a fall of 5 percent)
	 * 
	 * now you need to know whether , this is to become the stop loss for all
	 * pairs that start from 4. however while doing the main calculation in the
	 * entity, what you had was the index of this pair but in the large unsorted
	 * map, so we use the functin, pair_differences_inde_to_start_dday_id to get
	 * the index in the form of the small sorted 3000 days map.
	 * 
	 * then you see whether at that index(in the existing stat stop loss map)
	 * whatever value you have is more than the value of the index from the
	 * unsorted map(index per sae.)
	 * 
	 * now if it is more, then you replace it, because 4 -> 286 is the first
	 * time after 4 that a stop loss of five is triggered, so for this stock for
	 * any pair starting on day 4, the stop loss is at this index in the
	 * unsorted map.
	 * 
	 * -------------
	 * 
	 * basically what we do here is to check if the index in the smaller array
	 * that we get, is less than whatever is stored currently at that index, but
	 * in the bigger array.
	 * 
	 * -------------------------------------------------------------------------
	 * 
	 * what is the explanation for even checking it is more. suppose that first
	 * we had ony days till day id 100. now take the example of starting day 4 .
	 * upto now we have for example, for the start day of 4, the max pair: 4 ->
	 * 100 let us assume that at this pair we already have detected a fall of 5
	 * percent. and then we have stored the large array index for 4 -> 100 , at
	 * this index of the stop loss array. **REMMEBER THAT THE STOP LOSS ARRAYS
	 * ARE AS LONG AS THE TOTAL NUMBER OF DAY IDS. AND AT EACH INDEX WE JUST
	 * STORE THE index in the large unsorted array for the earliest pair , which
	 * start which this day, and falls by whatever stop loss percent is in
	 * question. FURTHERMORE THE DEFAULT VALUES AT STORED IN TEH STAT STOP
	 * LOSSES ARRAY,
	 * 
	 * **
	 * 
	 * 
	 * 
	 * 
	 */
	public static void modify_sorted_int_array_at_index(
			int index_from_unsorted_pair_diffs_array,
			int[] existing_stat_stop_loss_array, 
			String unique_name, int bsl,
			HashMap<String, int[]> combined_stop_losses_hashmap,
			String name_of_stat_stop_loss_arr,
			String open_open_or_close_close) {

		// how to be sure that 
		
		/****
		 * convert the index in the large array to the corresponding index in
		 * the daily price changes array.
		 */
		int index = pair_differences_index_to_start_day_id(index_from_unsorted_pair_diffs_array);
		
		/***
		 * get the trailing stop loss amounts that can be applied to this base
		 * stop loss amount.
		 * 
		 */
		// int[] applicable_trailing_stop_loss_amounts =
		// get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(
		// bsl);

		/****
		 * just initializing an empty array.
		 * 
		 */
		/***
		 * String[] names_of_applicable_trailing_stop_loss_amounts = new
		 * String[applicable_trailing_stop_loss_amounts.length];
		 * 
		 * 
		 * Integer tsl_index = 0;
		 * 
		 * for (int amt : applicable_trailing_stop_loss_amounts) {
		 * 
		 * names_of_applicable_trailing_stop_loss_amounts[tsl_index] =
		 * get_name_of_relevant_trailing_stop_loss_arr( unique_name,
		 * open_open_or_close_close, amt, bsl);
		 * 
		 * tsl_index++; }
		 **/

		if (existing_stat_stop_loss_array[index] > index_from_unsorted_pair_diffs_array) {
			// System.out.println("so we replace");
			existing_stat_stop_loss_array[index] = index_from_unsorted_pair_diffs_array;
			combined_stop_losses_hashmap.put(name_of_stat_stop_loss_arr, existing_stat_stop_loss_array);

			// iterate each of the tsls and if at index its value is more than
			// thsi one, then put this one.
			// System.out.println("now checking the tsls");
			/**
			 * for (String tsl_arr_name :
			 * names_of_applicable_trailing_stop_loss_amounts) { //
			 * System.out.println("tsl arr name:" + tsl_arr_name); int[]
			 * existing_tsl_arr =
			 * combined_stop_losses_hashmap.get(tsl_arr_name); //
			 * System.out.println("existing value at this index in array // is:"
			 * // + existing_tsl_arr[index]); if (existing_tsl_arr[index] >
			 * index_from_unsorted_pair_diffs_array) { // System.out.println(
			 * "so we replace at index: " + index); existing_tsl_arr[index] =
			 * index_from_unsorted_pair_diffs_array;
			 * combined_stop_losses_hashmap.put(tsl_arr_name, existing_tsl_arr);
			 * } }
			 **/
		}

	}

	/****
	 * TESTED.
	 * 
	 * 
	 */
	public static String get_name_of_relevant_stat_stop_loss_map(String unique_name,
			String open_open_or_close_close,
			Integer stop_loss_amount) {
		
		String name_of_relevant_stat_stop_loss_map = StockConstants.
				Entity_template_for_sorted_stat_stop_loss_map_name
				.replace("nb", String.valueOf(stop_loss_amount))
				.replace(StockConstants.Entity_unique_name_placeholder, unique_name)
				.replace(StockConstants.Entity_open_open_or_close_close_placeholder,
						open_open_or_close_close);
		
		return name_of_relevant_stat_stop_loss_map;
	}

	/****
	 * TESTED
	 * 
	 * 
	 * @param unique_name
	 * @param open_open_or_close_close
	 *            - stockconstants.entity_open_open_map_suffix or close_close
	 * @param tsl
	 * @param bsl
	 * @return
	 */
	public static String get_name_of_relevant_trailing_stop_loss_arr(String unique_name,
			String open_open_or_close_close, int tsl, int bsl) {

		// return
		// StockConstants.Entity_template_for_sorted_trailing_stop_loss_map_name
		// .replace("nbsl", String.valueOf(bsl)).replace("ntsl",
		// String.valueOf(tsl))
		// .replace(StockConstants.Entity_unique_name_placeholder, unique_name)
		// .replace(StockConstants.Entity_open_open_or_close_close_placeholder,
		// open_open_or_close_close);

		String name_of_relevant_tsl_stop_loss_map = StockConstants.Entity_template_for_sorted_trailing_stop_loss_map_name
				.replace("nb", String.valueOf(tsl)).replace(StockConstants.Entity_unique_name_placeholder, unique_name)
				.replace(StockConstants.Entity_open_open_or_close_close_placeholder, open_open_or_close_close);

		return name_of_relevant_tsl_stop_loss_map;

	}

	/***
	 * TESTED
	 * 
	 * so given 8-9 what all can it affect. so can affect everything from 9 -
	 * total_pairs, upto 9 - 1 .i.e 8
	 * 
	 * // we want a headmap upto but not including the end day id. // we can use
	 * the function from entity itself.
	 * 
	 */
	public static int[] get_sorted_arr_indices_affected_by_single_day_change(int end_day_id, Entity e) {

		SortedMap<Integer, DataPoint> ss = e.generate_price_map_in_range(end_day_id,
				StockConstants.Entity_total_pairs_for_a_day, false);

		ArrayList<Integer> affected_day_ids = new ArrayList<Integer>(ss.keySet());

		int[] sorted_arr_indices_affected_by_single_day_change = new int[StockConstants.Entity_total_pairs_for_a_day];

		for (int i = 0; i < affected_day_ids.size(); i++) {
			sorted_arr_indices_affected_by_single_day_change[i] = affected_day_ids.get(i);
		}

		return sorted_arr_indices_affected_by_single_day_change;
	}

	/****
	 * TESTED what trailing stop loss amount can be applied to a given base stop
	 * loss. for eg: if my base stop loss is -5. then trailing can be -2, -3, -5
	 * 
	 * so suppose the base stop loss amount is less than zero. then we can only
	 * include those stop loss amounts, which are greater than it, but less than
	 * zero(obviously cannot have something greater than zero.)
	 * 
	 * and suppose it is greater than zero, than opposite applies.
	 * 
	 * important to note that base stop loss and trailing stop losses can be
	 * equal.
	 *
	 * @param base_stop_loss_amount
	 * @return
	 */
	public static int[] get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(int base_stop_loss_amount) {
		// now suppose we have -5, we want
		ArrayList<Integer> trailing_stop_loss_amounts = new ArrayList<Integer>();
		if (base_stop_loss_amount < 0) {
			for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
				if ((StockConstants.Entity_stop_loss_amounts[i] >= base_stop_loss_amount)
						&& StockConstants.Entity_stop_loss_amounts[i] < 0) {
					trailing_stop_loss_amounts.add(StockConstants.Entity_stop_loss_amounts[i]);
				}
			}
		} else {
			for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
				if ((StockConstants.Entity_stop_loss_amounts[i] <= base_stop_loss_amount)
						&& StockConstants.Entity_stop_loss_amounts[i] > 0) {
					trailing_stop_loss_amounts.add(StockConstants.Entity_stop_loss_amounts[i]);
				}
			}
		}

		int[] trailing_stop_loss_amnts_int_arr = new int[trailing_stop_loss_amounts.size()];
		Integer counter = 0;
		for (Integer k : trailing_stop_loss_amounts) {
			trailing_stop_loss_amnts_int_arr[counter] = k;
			counter++;
		}
		return trailing_stop_loss_amnts_int_arr;
	}

	/****
	 * 
	 * 
	 * 
	 * @param e
	 *            : The Entity
	 * @return : arraylist of names of the arrays holding the last_day_of_month
	 *         and last_day_of_week arrays
	 */
	public static ArrayList<String> get_names_of_last_day_month_week_arrays(Entity e) {
		ArrayList<String> arr_names = new ArrayList<String>();
		arr_names.add(e.getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_month);
		arr_names.add(e.getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_week);
		return arr_names;
	}

	/****
	 * returns the names of the open_open, and close_close price change arrays,
	 * and the names of all the base_stop_loss and trailing_stop_loss_arrays.
	 * 
	 * 
	 * @param e
	 * @return
	 */
	public static ArrayList<String> get_names_of_arrays_applicable_to_entity(Entity e) {

		ArrayList<String> names = new ArrayList<String>();

		/****
		 * 
		 * step one build names for stat and trailing stop loss arrays.
		 * 
		 * 
		 */
		String[] open_open_or_close_close = { StockConstants.Entity_open_open_map_suffix,
				StockConstants.Entity_close_close_map_suffix };

		for (Integer stop_loss_amount : StockConstants.Entity_stop_loss_amounts) {
			for (String open_open_or_close_close_name : open_open_or_close_close) {
				names.add(get_name_of_relevant_stat_stop_loss_map(e.getUnique_name(), open_open_or_close_close_name,
						stop_loss_amount));
						// get names of all applicable trailing stop loss.
						// int[] tsl_amounts =
						// get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(
						// stop_loss_amount);

				// for (int tsl_amount : tsl_amounts) {
				names.add(get_name_of_relevant_trailing_stop_loss_arr(e.getUnique_name(), open_open_or_close_close_name,
						stop_loss_amount, stop_loss_amount));
				// }
			}
		}

		/***
		 * 
		 * build names for the unsorted open_open diffs, close_close diffs and
		 * raw open and close arrays.
		 * 
		 */

		names.add(e.getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix);
		names.add(e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix);
		// names.add(e.getUnique_name() + "_"
		// + StockConstants.Entity_daily_open_map_suffix);
		// names.add(e.getUnique_name() + "_"
		// + StockConstants.Entity_daily_close_map_suffix);

		return names;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
			return CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter().writeValueAsString(this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void print_all_pairs_with_indexes_in_unsorted_array() {
		Integer index_in_unsorted_arr = 0;
		for (int i = 0; i < CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size(); i++) {
			System.out.println("its filler is:" + get_filler_id_for_sorted_map(i));

			for (int e = 1; e < StockConstants.Entity_total_pairs_for_a_day + 1; e++) {
				System.out.println("calculated start day index is:"
						+ pair_differences_index_to_start_day_id(index_in_unsorted_arr));
				System.out.println("calculated index is:"
						+ get_int_array_index_given_start_day_id_and_difference(i, e).getPair_difference_array_index());
				System.out.println("index in unsorted arr is:" + index_in_unsorted_arr);
				System.out.println("pair is:" + String.valueOf(i) + "_" + String.valueOf(i + e));
				System.out.println("\n");
				index_in_unsorted_arr++;
			}
		}
	}

	// Example usage:
	//
	// int[] numbers = {1, 2, 3, 4, 5, 6, 7};
	// int[][] chunks = chunkArray(numbers, 3);
	//
	// chunks now contains [
	// [1, 2, 3],
	// [4, 5, 6],
	// [7]
	// ]
	public static int[][] chunkArray(int[] array, int chunkSize) {
		int numOfChunks = (int) Math.ceil((double) array.length / chunkSize);
		int[][] output = new int[numOfChunks][];

		for (int i = 0; i < numOfChunks; ++i) {
			int start = i * chunkSize;
			int length = Math.min(array.length - start, chunkSize);

			int[] temp = new int[length];
			System.arraycopy(array, start, temp, 0, length);
			output[i] = temp;
		}

		return output;
	}

	public static int[] concat_Arrays(int[] a, int[] b) {
		int aLen = a.length;
		int bLen = b.length;
		int[] c = new int[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
	}

	/****
	 * what is being done here is that,
	 * 
	 * take each day id in the price_map_internal instantiate a counter outside
	 * the main iteration and incrment it for each day id. this is because for
	 * each period ,for this entity, there is an array in the
	 * query_time_lookup_map in cs and that array starts from zero. so basically
	 * we want to fill up, for eah day id, what is the change for each period.
	 * and store them in small arrays. so the fist day id will correlate with
	 * index 0 in the respective array. thats why the counter.
	 * 
	 * 
	 * if the end day id is greater than the last available day, then nothing
	 * will be added to the array/ when a newer day id comes, it will get added.
	 * 
	 * now inside it iterate each period in teh period map. calculate the price
	 * difference from close_close_array of the change over (day_id ==> day_id +
	 * period).
	 * 
	 * 
	 * 
	 * @param e
	 */
	public static void generate_t_test_double_arrays(Entity e) {

		Set<Integer> periods_used_for_t_tests = IdAndShardResolver.periods_map_for_t_tests.keySet();

		int[] decompressed_close_close_map = get_decompressed_arr(
				e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix, 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);

		HashMap<String, int[]> price_changes_for_t_test = new LinkedHashMap<String, int[]>();
		Integer last_valid_pair_change_index_on_entity = get_int_array_index_given_start_day_id_and_difference(
				e.getPrice_map_internal().lastKey() - 1, 1).pair_difference_array_index;
		Integer counter = 0;
		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {
			Integer start_day_id = entry.getKey();
			for (Integer period : periods_used_for_t_tests) {

				String name_of_key = e.getUnique_name() + "_" + period.toString();

				Integer index = get_int_array_index_given_start_day_id_and_difference(start_day_id, period)
						.getPair_difference_array_index();
				// why is it - period + 1?
				// suppose you have a total size of price map internal as: 10
				// now if your period is 2
				// for day id 8 you will get a result.
				// so size of the array should be 9.
				// so that the first 9 day ids will get a result.
				if (index <= last_valid_pair_change_index_on_entity) {
					if (!price_changes_for_t_test.containsKey(name_of_key)) {
						price_changes_for_t_test.put(name_of_key,
								new int[e.getPrice_map_internal().size() - period + 1]);
					}

					price_changes_for_t_test.get(name_of_key)[counter] = decompressed_close_close_map[index];
				}
			}

			counter++;

		}

		LogObject lg = new LogObject(PairArray.class, e);
		for (Map.Entry<String, int[]> entry : price_changes_for_t_test.entrySet()) {
			lg.add_to_info_packet(entry.getKey(), entry.getValue().length);
			put_arr(entry.getValue(), entry.getKey(), 0);
			// lg.add_to_info_packet(entry.getKey() + "-last-element",
			// entry.getValue()[entry.getValue().length - 1]);
		}
		lg.commit_info_log("PairArray:built_t_test_arrs");
		// update it to the central singleton's map holding all the t-test shit.

	}

	/***
	 * useful for benchmarks etc.
	 * 
	 * @param length
	 *            - the initial length of the array
	 * @param sorted_or_unsorted
	 *            - whether it should be sorted or not(0 unsorted, 1 sorted)
	 * @param compressed_or_not
	 *            - whether it should be compressed or not(0 uncompressed, 1
	 *            compressed)
	 * @param max_value
	 *            - the maximum value in the array.
	 * @param min_value
	 *            - the minimum value in the array.
	 */
	public static int[] get_random_test_array(Integer length, Integer sorted_or_unsorted, Integer compressed_or_not,
			Integer max_value, Integer min_value) {

		ArrayList<Integer> random_values = new ArrayList<Integer>();
		Random r = new Random();
		for (int i = 0; i < length; i++) {
			random_values.add(r.nextInt(max_value * 2) - min_value);
		}

		int[] init_arr = new int[length];
		for (int i = 0; i < random_values.size(); i++) {
			init_arr[i] = random_values.get(i);
		}

		// if need sorted.
		if (sorted_or_unsorted.equals(1)) {
			Arrays.sort(init_arr);
		}

		if (compressed_or_not == 1) {
			return get_compressed_int_array(init_arr, sorted_or_unsorted);
		}

		return init_arr;

	}

	public static void print_heap_stats() {
		int mb = 1024 * 1024;

		// Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();

		System.out.println("##### Heap utilization statistics [MB] #####");

		// Print used memory
		System.out.println("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);

		// Print free memory
		System.out.println("Free Memory:" + runtime.freeMemory() / mb);

		// Print total available memory
		System.out.println("Total Memory:" + runtime.totalMemory() / mb);

		// Print Maximum available memory
		System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	}

}
