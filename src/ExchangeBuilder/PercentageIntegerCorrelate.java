package ExchangeBuilder;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import yahoo_finance_historical_scraper.StockConstants;

public class PercentageIntegerCorrelate {

	private Integer open_open_price_change_correlate;
	private Integer close_close_price_change_correlate;
	private Integer high_high_price_change_correlate;
	private Integer low_low_price_change_correlate;

	// -5,-3,-2,2,3,5
	@JsonIgnore
	@JsonProperty("open")
	private boolean[] open_open_applicable_stop_losses;

	@JsonIgnore
	@JsonProperty("close")
	private boolean[] close_close_applicalbe_stop_losses;

	@JsonIgnore
	public boolean[] getOpen_open_applicable_stop_losses() {
		return open_open_applicable_stop_losses;
	}

	@JsonIgnore
	public boolean[] getClose_close_applicalbe_stop_losses() {
		return close_close_applicalbe_stop_losses;
	}

	public Integer getOpen_open_price_change_correlate() {
		return open_open_price_change_correlate;
	}

	public void setOpen_open_price_change_correlate(Integer open_open_price_change_correlate) {
		this.open_open_price_change_correlate = open_open_price_change_correlate;
	}

	public Integer getClose_close_price_change_correlate() {
		return close_close_price_change_correlate;
	}

	public void setClose_close_price_change_correlate(Integer close_close_price_change_correlate) {
		this.close_close_price_change_correlate = close_close_price_change_correlate;
	}

	public Integer getHigh_high_price_change_correlate() {
		return high_high_price_change_correlate;
	}

	public void setHigh_high_price_change_correlate(Integer high_high_price_change_correlate) {
		this.high_high_price_change_correlate = high_high_price_change_correlate;
	}

	public Integer getLow_low_price_change_correlate() {
		return low_low_price_change_correlate;
	}

	public void setLow_low_price_change_correlate(Integer low_low_price_change_correlate) {
		this.low_low_price_change_correlate = low_low_price_change_correlate;
	}

	public PercentageIntegerCorrelate(Integer open_open_pc_correlate, Integer close_close_pc_correlate,
			Integer high_high_pc_correlate, Integer low_low_pc_correlate) {

		this.open_open_price_change_correlate = open_open_pc_correlate;
		this.close_close_price_change_correlate = close_close_pc_correlate;
		this.high_high_price_change_correlate = high_high_pc_correlate;
		this.low_low_price_change_correlate = low_low_pc_correlate;
		set_stop_loss_boolean_arrays(open_open_price_change_correlate, 0);
		set_stop_loss_boolean_arrays(close_close_price_change_correlate, 1);
	}

	/**
	 * pc correlate => the correlate that was passed into the constructor of the
	 * function. open_or_close => whether it is the open or close correlate.
	 * 0:open, 1:close
	 * 
	 * @param pc_correlate
	 * @param open_or_close
	 */
	public void set_stop_loss_boolean_arrays(Integer pc_correlate, Integer open_or_close) {

		boolean[] arr = new boolean[6];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = false;
		}

		if (pc_correlate != null) {

			if (pc_correlate <= -50) {
				arr[0] = true;
				arr[1] = true;
				arr[2] = true;
			} else if (pc_correlate <= -30) {
				arr[1] = true;
				arr[2] = true;
			} else if (pc_correlate <= -20) {
				arr[2] = true;
			} else if (pc_correlate >= 50) {
				arr[5] = true;
				arr[3] = true;
				arr[4] = true;
			} else if (pc_correlate >= 30) {
				arr[3] = true;
				arr[4] = true;
			} else if (pc_correlate >= 20) {
				arr[3] = true;
			}
		}

		if (open_or_close.equals(0)) {
			open_open_applicable_stop_losses = arr;
		} else {
			close_close_applicalbe_stop_losses = arr;
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
			return CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter().writeValueAsString(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public boolean[] get_applicable_stop_loss_array(String open_or_close) {

		if (open_or_close.equals("open_open")) {

			return getOpen_open_applicable_stop_losses();
		} else {

			return getClose_close_applicalbe_stop_losses();
		}
	}

	/****
	 * 
	 * same sign means what 
	 * tsl of -5 percent means fall in price by 5 percent,
	 * suppose the price change is also negative then the tsl doesnt get triggered,
	 *  to move, as the tsl is only triggered to raise or lower itself, if the price moves
	 *  in the opposite direction, i.e at least 0.1 percent above/below 
	 *  the price at which the stock was bought.
	 * 
	 * @param open_or_close
	 * @param tsl_amount
	 * @return
	 */
	public Boolean trigger_tsl_move(String open_or_close, Integer tsl_amount) {

		if (open_or_close.equals("open_open")) {
			if (same_sign_as(tsl_amount, getOpen_open_price_change_correlate())) {
				return false;
			} else {
				return Math.abs(
						getOpen_open_price_change_correlate()) > StockConstants.Entity_trailing_stop_loss_trigger_threshold;
				// && (!same_sign_as(tsl_amount,
				// getOpen_open_price_change_correlate()));
			}
		} else {
			if (same_sign_as(tsl_amount, getClose_close_price_change_correlate())) {
				return false;
			} else {
				return Math.abs(
						getClose_close_price_change_correlate()) > StockConstants.Entity_trailing_stop_loss_trigger_threshold;
				// && !(same_sign_as(tsl_amount,
				// getClose_close_price_change_correlate()));
			}
		}
	}
	
	/***
	 * checks direct violation of tsl amount, even before tsl can be triggerd ,
	 * just like a routine bsl.
	 * 
	 * @param open_or_close
	 * @param tsl_amount
	 * @return
	 */
	public Boolean tsl_violated_before_move(String open_or_close, Integer tsl_amount) {
		if (open_or_close.equals("open_open")) {
			if (same_sign_as(tsl_amount, getOpen_open_price_change_correlate())
					&& (Math.abs(getOpen_open_price_change_correlate()) > Math.abs(tsl_amount))) {
				return true;
			} else {
				return false;
			}
		} else {
			if (same_sign_as(tsl_amount, getClose_close_price_change_correlate())
					&& (Math.abs(getClose_close_price_change_correlate()) > Math.abs(tsl_amount))) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static Boolean same_sign_as(Integer a, Integer b) {
		if (a.intValue() > 0 && b.intValue() > 0) {
			return true;
		} else if (a.intValue() < 0 && b.intValue() < 0) {
			return true;
		} else if (a.intValue() == 0 && b.intValue() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean other_pc_is_greater(String open_or_close, PercentageIntegerCorrelate other_pc) {
		if (open_or_close.equals("open_open")) {
			return other_pc.getOpen_open_price_change_correlate() > getOpen_open_price_change_correlate();
		} else {
			return other_pc.getClose_close_price_change_correlate() > getClose_close_price_change_correlate();
		}
	}

	public Integer vector_compared_to_tsl_amount(Integer tsl_amount, String open_or_close) throws Exception {

		if (open_or_close.equals("open_open")) {
			if (!same_sign_as(tsl_amount, getOpen_open_price_change_correlate())) {
				return 0;
			}
			if (Math.abs(getOpen_open_price_change_correlate()) > Math.abs(tsl_amount * 10)) {
				return -1;
			} else {
				return 1;
			}
		} else {
			if (!same_sign_as(tsl_amount, getClose_close_price_change_correlate())) {
				return 0;
			}
			if (Math.abs(getClose_close_price_change_correlate()) > Math.abs(tsl_amount * 10)) {
				return -1;
			} else {
				return 1;
			}

		}

	}

}
