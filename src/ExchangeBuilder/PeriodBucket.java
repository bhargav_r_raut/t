package ExchangeBuilder;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnore;

public class PeriodBucket {

	
	private ArrayList<Integer> close_price_arraylist;
	
	
	private ArrayList<Integer> open_price_arraylist;

	
	private String period_name;
	
	
	public String getPeriod_name() {
		return period_name;
	}


	public void setPeriod_name(String period_name) {
		this.period_name = period_name;
	}


	public ArrayList<Integer> getClose_price_arraylist() {
		return close_price_arraylist;
	}

	
	public void setClose_price_arraylist(ArrayList<Integer> close_price_arraylist) {
		this.close_price_arraylist = close_price_arraylist;
	}
	
	
	public ArrayList<Integer> getOpen_price_arraylist() {
		return open_price_arraylist;
	}
	
	
	public void setOpen_price_arraylist(ArrayList<Integer> open_price_arraylist) {
		this.open_price_arraylist = open_price_arraylist;
	}

	//for jackson.
	public PeriodBucket(){
		
	}
	
	public PeriodBucket(Integer close_price,Integer open_price,String period_name){
		this.close_price_arraylist = new ArrayList<Integer>();
		this.open_price_arraylist = new ArrayList<Integer>();
		this.period_name = period_name;
	}
	
	

}
