package ExchangeBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequestBuilder;
import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.get.GetResult;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.ScriptService.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import Indicators.RedisManager;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;

public class Runner {

	public static void main(String[] args) {

		try {
			new Runner().run();
		} catch (Exception e) {
			LogObject log = new LogObject(Runner.class);
			log.commit_error_log("test", e);
		}

	}

	public void run() throws Exception {

		 test_central_singleton();
		 test_build_exchange();
		 System.out.println("sleeping 10 seconds between build and download");
		 Thread.sleep(10000);
		 test_download_exchange();
		// test_build_price_change_aggs();
		// test_groovy_update_script();
		// test_search_response_for_chunking("oil_Amex Oil Index_oil_index_open_open");
		test_chunk_on_two_consecutive_runs();
	}

	public void test_build_price_change_aggs() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_price_change_aggs();
	}

	public void clear_local_and_remote_indices(String[] types) {

		for (String type : types) {
			DeleteByQueryResponse drq = new DeleteByQueryRequestBuilder(
					LocalEs.getInstance())
					.setQuery(QueryBuilders.matchAllQuery())
					.setIndices("tradegenie_titan").setTypes(type).execute()
					.actionGet();

			DeleteByQueryResponse drr = new DeleteByQueryRequestBuilder(
					RemoteEs.getInstance())
					.setQuery(QueryBuilders.matchAllQuery())
					.setIndices("tradegenie_titan").setTypes(type).execute()
					.actionGet();

		}

	}

	public EntityIndex test_build_exchange() throws Exception {

		clear_local_and_remote_indices(new String[] {
				StockConstants.ESTypes_arrays, StockConstants.ESTypes_entity });

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			jedis.flushAll();
		}

		System.out.println("testing build exchange");
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		Integer counter = 0;
		EntityIndex ei = null;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants
				.entrySet()) {

			// so only check if it is oil.
			if (entry.getKey().equals(StockConstants.name_for_oil)) {
				ei = erp.build_entityExchange(counter, entry.getKey());
				// download_entityExchange(counter, entry.getKey());
			}

			counter++;
		}

		return ei;
	}

	public void test_download_exchange() throws Exception {

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		Integer counter = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants
				.entrySet()) {

			if (entry.getKey().equals(StockConstants.name_for_oil)) {
				erp.test_entity_index_from_phase(entry.getKey(), counter);
				// erp.download_entityExchange(entry.getKey(), counter);
			}

			counter++;
		}
	}

	public void test_central_singleton() throws Exception {
		GregorianCalendar start_cal = new GregorianCalendar();
		CentralSingleton.initializeSingleton();

		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println("time taken:"
				+ (end_cal.getTimeInMillis() - start_cal.getTimeInMillis()));
	}

	public void test_groovy_update_script() throws InterruptedException,
			ExecutionException {

		// first insert a document with some sorted integers.

		// then update with some more.

		// then get back the whole thing and test with expected values.

		DeleteByQueryResponse drq = new DeleteByQueryRequestBuilder(
				LocalEs.getInstance())
				.setQuery(QueryBuilders.matchAllQuery())
				.setIndices("tradegenie_titan").setTypes("bs").execute()
				.actionGet();

		System.out.println("deleted all docs in index");

		UpdateRequest ureq = new UpdateRequest("tradegenie_titan", "bs", "1");
		ureq.script("update_bs_script", ScriptType.FILE,
				new HashMap<String, Object>() {
					{
						put("sparse_arr", new int[] { 4, 14, 34 });
					}
				});
		ureq.scriptedUpsert(true);
		ureq.upsert(new HashMap<String, Object>());
		ureq.fields(new String[] { "buy", "sell", "decompressed_length" });
		UpdateResponse resp = LocalEs.getInstance().update(ureq)
				.get();

		if (resp.isCreated()) {
			System.out.println("created the initial document.");

			GetResult gr = resp.getGetResult();
			ArrayList<Integer> buy = (ArrayList<Integer>) gr.field("buy")
					.getValue();
			ArrayList<Integer> sell = (ArrayList<Integer>) gr.field("sell")
					.getValue();
			Integer expected_length = (Integer) gr.field("decompressed_length")
					.getValue();
			// now decompress them to see whether both are 34 + 1 long.
			int[] buy_arr = ArrayUtils.toPrimitive(buy.toArray(new Integer[buy
					.size()]));
			int[] sell_arr = ArrayUtils.toPrimitive(sell
					.toArray(new Integer[sell.size()]));
			buy_arr = PairArray.get_decompressed_array_from_compressed_array(
					buy_arr, expected_length + 1, 1);
			sell_arr = PairArray.get_decompressed_array_from_compressed_array(
					sell_arr, expected_length + 1, 1);
			System.out.println("the first buy arr decompressed length is:"
					+ buy_arr.length);
			System.out.println("the first sell arr decompressed length is:"
					+ sell_arr.length);

			// now we add some more to it.
			UpdateRequest ureq2 = new UpdateRequest("tradegenie_titan", "bs",
					"1");
			ureq2.script("update_bs_script", ScriptType.FILE,
					new HashMap<String, Object>() {
						{
							put("sparse_arr", new int[] { 35, 40, 78 });
						}
					});
			ureq2.scriptedUpsert(true);
			ureq2.upsert(new HashMap<String, Object>());
			ureq2.fields(new String[] { "buy", "sell", "decompressed_length" });
			UpdateResponse resp2 = LocalEs.getInstance()
					.update(ureq2).get();

			if (resp2.getId().equals("1")) {
				System.out.println("second update successfull");
				GetResult gr2 = resp2.getGetResult();
				List<Object> obj = gr.field("buy").getValues();
				System.out.println(obj);
				// now decompress them to see whether both are 34 + 1 long.
				/**
				 * int[] buy_arr2 = ArrayUtils.toPrimitive(buy2.toArray(new
				 * Integer[buy2 .size()])); int[] sell_arr2 =
				 * ArrayUtils.toPrimitive(sell2 .toArray(new
				 * Integer[sell2.size()])); buy_arr2 =
				 * PairArray.get_decompressed_array_from_compressed_array(
				 * buy_arr2, expected_length2 + 1, 1); sell_arr2 =
				 * PairArray.get_decompressed_array_from_compressed_array(
				 * sell_arr2, expected_length2 + 1, 1); System.out.println(
				 * "the first buy arr decompressed length is:" +
				 * buy_arr2.length); System.out.println(
				 * "the first sell arr decompressed length is:" +
				 * sell_arr2.length);
				 **/
			}

		}

	}

	public static void test_() {
		int[] in = new int[] { 64, 5, 335544324, 0, 40960, 0, 0, 6, 84148224,
				159383552, 0, 0, 0, 0, -2139062066, -2139062144, -2139062144,
				8421504 };
		int[] dec = PairArray.get_decompressed_array_from_compressed_array(in,
				79, 1);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < dec.length; i++) {
			sb.append(dec[i] + ",");
		}
		System.out.println(sb.toString());
	}

	public void test_search_response_for_chunking(String arr_name) {
		ArrayList<Integer> compressed_arr_list = new ArrayList<Integer>();
		SearchResponse resp = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes(StockConstants.ESTypes_arrays)
				.setQuery(
						QueryBuilders.termQuery(
								StockConstants.ESTypes_arrays_arr_name,
								arr_name)).setSize(110).addField("arr")
				.addSort("start_index", SortOrder.ASC).execute().actionGet();

		for (SearchHit hit : resp.getHits().getHits()) {
			List<Object> ret = hit.field("arr").getValues();
			for (Object o : ret) {
				compressed_arr_list.add((Integer) o);
			}
		}

		System.out.println(compressed_arr_list);

	}

	/****
	 * 
	 * first builds oil entity index. then downloads it till 6 months back. then
	 * decompresses and checks equality.
	 * 
	 * then downloads it for another 6 months. then decompresses it and checks
	 * equality.
	 * 
	 * 
	 */

	public void test_chunk_on_two_consecutive_runs() throws Exception {

		test_central_singleton();

		EntityIndex ei = test_build_exchange();

		LogObject lgsd1 = new LogObject(getClass());
		lgsd1.commit_debug_log("sleeping to let es index the entity_index");
		Thread.sleep(10000);

		StockConstants.define_custom_end_date = true;

		test_chunk_and_dechunk(ei);
		ei.shutdown_bulk_processors();
		LogObject lgd = new LogObject(getClass());
		lgd.commit_debug_log("first run complete");

		StockConstants.define_custom_end_date = false;

		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants
				.entrySet()) {
			if (entry.getKey().equals(StockConstants.name_for_oil)) {
				test_chunk_and_dechunk(new EntityIndex(entry.getKey(), i));
			}
			i++;
		}

	}

	/*****
	 * builds "oil" entity index and then downloads the latest data, and builds
	 * the chunks and commits them to elasticsearch then gets back all chunks
	 * from elasticsearch and decompresses them and compares to the existing
	 * chunks in the {@link CentralSingleton#query_time_pair_lookup_map}
	 * 
	 * @throws Exception
	 * 
	 */
	public void test_chunk_and_dechunk(EntityIndex ei) throws Exception {

		ei.download_entity_index();

		ei.commit_entity_index_to_elasticsearch(1);

		LogObject lgsd3 = new LogObject(getClass());
		lgsd3.commit_debug_log("sleeping again to let price change arrays absorb into the index");
		Thread.sleep(10000);
		// now iterate over each name in the pair arrays.
		for (Map.Entry<String, Entity> entry : ei.getEntity_map().entrySet()) {

			ArrayList<String> open_and_close_arrs = new ArrayList<String>(
					Arrays.asList(entry.getValue().getUnique_name() + "_"
							+ StockConstants.Entity_open_open_map_suffix, entry
							.getValue().getUnique_name()
							+ "_"
							+ StockConstants.Entity_close_close_map_suffix));

			for (String arr_name : PairArray
					.get_names_of_arrays_applicable_to_entity(entry.getValue())) {

				// search elasticsearch for all components of this arrays.
				// sort them by the start_index.
				// then decompress and assemble.
				// after that compare for equality against the existing array in
				// the centralsingleton.

				
				ArrayList<Integer> remote_decompressed_arr_list = new ArrayList<Integer>();
				

				SearchResponse remote_resp = RemoteEs
						.getInstance()
						.prepareSearch("tradegenie_titan")
						.setTypes(StockConstants.ESTypes_arrays)
						.setQuery(
								QueryBuilders.termQuery(
										StockConstants.ESTypes_arrays_arr_name,
										arr_name)).setSize(110).addField("arr")
						.addSort("start_index", SortOrder.ASC).execute()
						.actionGet();

				int type = open_and_close_arrs.contains(arr_name) ? 0 : 1;
				int desired_decompressed_length = open_and_close_arrs
						.contains(arr_name) ? CentralSingleton.getInstance().gmt_day_id_to_dataPoint
						.size() * StockConstants.Entity_total_pairs_for_a_day
						: CentralSingleton.getInstance().gmt_day_id_to_dataPoint
								.size();

				
				for (SearchHit hit : remote_resp.getHits().getHits()) {
					List<Object> ret = hit.field("arr").getValues();
					ArrayList<Integer> compressed_chunk_list = new ArrayList<Integer>();
					for (Object o : ret) {
						compressed_chunk_list.add((Integer) o);
					}
					int[] compressed_chunk_arr = ArrayUtils
							.toPrimitive(compressed_chunk_list
									.toArray(new Integer[compressed_chunk_list
											.size()]));
					int decompressed_length = open_and_close_arrs
							.contains(arr_name) ? StockConstants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays
							: CentralSingleton.getInstance().gmt_day_id_to_dataPoint
									.size();

					LogObject lgd3 = new LogObject(getClass());
					lgd3.commit_debug_log("arr name is:" + arr_name
							+ "  and desired chunk decompressed length is: "
							+ decompressed_length + "  and type is:" + type);

					int[] decompressed_chunk_arr = PairArray
							.get_decompressed_array_from_compressed_array(
									compressed_chunk_arr, decompressed_length,
									type);

					for (int i = 0; i < decompressed_chunk_arr.length; i++) {
						remote_decompressed_arr_list
								.add(decompressed_chunk_arr[i]);
					}
				}

				// we want to get a decompressed int arra.
				// then we wanna decompress the same arr from our cs.
				// then we waana compare.
				// this decompressed array will not be the size of the existing
				// array, because it contains
				// so pad it.

				

				int[] remote_decompressed_arr = ArrayUtils
						.toPrimitive(remote_decompressed_arr_list
								.toArray(new Integer[remote_decompressed_arr_list
										.size()]));

				remote_decompressed_arr = Arrays.copyOf(
						remote_decompressed_arr, desired_decompressed_length);

				int[] existing_decompressed_arr = PairArray
						.get_decompressed_arr(arr_name, type,
								desired_decompressed_length);

				

				if (Arrays.equals(remote_decompressed_arr,
						existing_decompressed_arr)) {
					LogObject lgd = new LogObject(getClass());
					lgd.commit_debug_log("remote array is equal: " + arr_name);
				} else {
					LogObject lge = new LogObject(getClass());
					lge.commit_error_log("remote arrs are not equal",
							new Exception("unequal arr: " + arr_name
									+ " and sizes are :" + " es arr:"
									+ remote_decompressed_arr.length
									+ " and local arr: "
									+ existing_decompressed_arr.length));

					LogObject lgde = new LogObject(getClass());
					lgde.commit_debug_log("iterating one index at a time and comparing.");

					LogObject lgde2 = new LogObject(getClass());

					for (int i = 0; i < remote_decompressed_arr.length; i++) {
						if (existing_decompressed_arr[i] != remote_decompressed_arr[i]) {
							lgde2.commit_debug_log("remote indices are unqueal:"
									+ i
									+ " decompressed arr has:"
									+ remote_decompressed_arr[i]
									+ " and existing arr has:"
									+ existing_decompressed_arr[i]);
							// System.in.read();
						}
					}

					throw new Exception("remote arrays were unequal test fails");

				}

			}

		}
	}

	public EntityIndex test_entity_index_serialization_and_deserialization(
			String exchange_name, Integer exchange_number) {
		try {
			EntityIndex ei = new EntityIndex(exchange_name, exchange_number);

			LogObject lg = new LogObject(this.getClass(), ei);

			lg.commit_info_log("starting to download entity exchange");

			ei.download_entity_index();

			ei.calculate_entity_index_indicators();

			

			ei.commit_entity_index_to_elasticsearch(1);

			HashMap<String, TreeMap<Integer, DataPoint>> before_ser_map = new LinkedHashMap<String, TreeMap<Integer, DataPoint>>();

			for (Map.Entry<String, Entity> entity : ei.getEntity_map()
					.entrySet()) {
				before_ser_map.put(entity.getValue().getUnique_name(), entity
						.getValue().getPrice_map_internal());
			}

			System.out.println("sleeping for 20 seconds");
			Thread.sleep(20000);

			ei = new EntityIndex(exchange_name, exchange_number);

			ei.build_entity_index_from_elasticsearch();

			// check the datapoint_indicators on each datapoint.

			for (Map.Entry<String, Entity> entry : ei.getEntity_map()
					.entrySet()) {
				for (Map.Entry<Integer, DataPoint> datapoint_entry : entry
						.getValue().getPrice_map_internal().entrySet()) {
					System.out.println("testing day id:"
							+ datapoint_entry.getKey());
					for (Map.Entry<String, dataPointIndicator> ind_entry : datapoint_entry
							.getValue().getIndicator_values_on_datapoint()
							.entrySet()) {

						dataPointIndicator old_dpi = before_ser_map
								.get(entry.getKey())
								.get(datapoint_entry.getKey())
								.getIndicator_values_on_datapoint()
								.get(ind_entry.getKey());

						if (!old_dpi.getIndicator_big_decimal_value().equals(
								ind_entry.getValue()
										.getIndicator_big_decimal_value())) {
							System.out.println("fucked:");
							System.out.println("old");
							System.out.println(old_dpi
									.getIndicator_big_decimal_value()
									.toString());

							System.out.println("new");
							System.out.println(ind_entry.getValue()
									.getIndicator_big_decimal_value());
							System.in.read();
						}

					}
				}
			}

			// ei.after_load_from_cache();

			// ei.calculate_entity_index_indicators();

			// ei.before_put_to_cache();

			// ei.commit_entity_index_to_elasticsearch(1);

			LogObject lg_end = new LogObject(this.getClass(), ei);
			lg_end.commit_info_log("successfully test entity exchange");
			return ei;

		} catch (Exception e) {
			// BaseError be = new BaseError(e,
			// "failed in download entity exchange");
			// e.printStackTrace();
			LogObject lg = new LogObject(getClass());
			lg.commit_error_log("error while downloading entity exchange", e);

			return null;
		}
	}

	public void test_PairArray() {

		// do we fix the size, or what?
		// we dont.
		/***
		 * testing getting default arrays from concurrent hashmap.
		 * 
		 */
		int[] unsorted_arr = PairArray.get_decompressed_arr("unsorted", 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);
		int[] sorted_arr = PairArray.get_decompressed_arr("sorted", 1,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		/***
		 * 
		 * testing putting back arrays into cocurrent hashmap.
		 * 
		 */
		unsorted_arr[0] = 100;
		PairArray.put_arr(sorted_arr, "sorted", 1);
		PairArray.put_arr(unsorted_arr, "unsorted", 0);

		/**
		 * testing getting array from concurrent hashmap
		 * 
		 */

		int[] unsorted_arr_recovered = PairArray.get_decompressed_arr(
				"unsorted", 0,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
						* StockConstants.Entity_total_pairs_for_a_day);

		int[] sorted_arr_recovered = PairArray.get_decompressed_arr("sorted",
				1,
				CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

		/**
		 * testing getting value at index in array from concurrent hashamp.
		 * 
		 * 
		 */

		System.out.println("value in sorted array at index 0:"
				+ PairArray.get_value_from_arr_at_index("sorted", 1, 3650, 0));

		System.out.println("value in unsorted array at index 0:"
				+ PairArray.get_value_from_arr_at_index("unsorted", 0, 1095000,
						0));

		/***
		 * 
		 * testing trailing stop loss amount applicable to base stop loss
		 * amount.
		 * 
		 */
		System.out
				.println("testing: testing trailing stop loss amount applicable to base stop loss amount.");
		for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
			System.out.println("testing base:"
					+ StockConstants.Entity_stop_loss_amounts[i]);
			int[] tsl = PairArray
					.get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(StockConstants.Entity_stop_loss_amounts[i]);
			for (int t = 0; t < tsl.length; t++) {
				System.out.println(tsl[t]);
			}
		}

		/***
		 * 
		 * testing getting the start day id of all the things that can be
		 * affected by the single day change.
		 * 
		 * 
		 * 
		 * /*** testing getting name of relevant trailing stop loss arrays.
		 * 
		 * 
		 */
		// for all the tsl and all the bsl.
		System.out
				.println("testing:"
						+ "testing getting name of relevant base stop loss arrays and  trailing stop loss arrays.");
		for (int bsl : StockConstants.Entity_stop_loss_amounts) {
			int[] applicable_tsl = PairArray
					.get_trailing_stop_loss_amounts_applicable_to_base_stop_loss_amount(bsl);
			System.out.println("\n now printing base stop loss names");
			System.out.println(PairArray
					.get_name_of_relevant_stat_stop_loss_map("unique_name",
							StockConstants.Entity_open_open_map_suffix, bsl));
			System.out.println(PairArray
					.get_name_of_relevant_stat_stop_loss_map("unique_name",
							StockConstants.Entity_close_close_map_suffix, bsl));

			System.out.println("\n now printing its trailing stop loss names:");
			for (int t : applicable_tsl) {
				// open open and close close.
				System.out.println(PairArray
						.get_name_of_relevant_trailing_stop_loss_arr(
								"unique_name",
								StockConstants.Entity_open_open_map_suffix, t,
								bsl));
				System.out.println(PairArray
						.get_name_of_relevant_trailing_stop_loss_arr(
								"unique_name",
								StockConstants.Entity_close_close_map_suffix,
								t, bsl));
			}
		}

		/****
		 * testing get hashmap of all stat stop loss arrays.
		 * 
		 * 
		 */
		HashMap<String, int[]> hm = PairArray
				.get_hashmap_of_all_stat_stop_loss_arrays("unique_name");
		for (Map.Entry<String, int[]> entry : hm.entrySet()) {
			System.out.println("name of stat stop loss arr:" + entry.getKey());
			System.out.println("its 10th entry:" + entry.getValue()[10]);
		}

		/****
		 * testing get int array index given start day id and end day id. all
		 * the index calculation and pair conversion functions.
		 * 
		 * 
		 */
		// PairArray.print_all_pairs_with_indexes_in_unsorted_array();

		/***
		 * testing the modify stat stop loss arr at index.
		 * 
		 * 
		 * 
		 */

	}

}
