package ExchangeBuilder;

import java.util.ArrayList;

public class TestMethod {
	
	private String location;
	private ArrayList<String> methods_to_execute;
	

	public String getLocation() {
		return location;
	}



	public void setLocation(String marker) {
		this.location = marker;
	}



	public ArrayList<String> getMethods_to_execute() {
		return methods_to_execute;
	}



	public void setMethods_to_execute(ArrayList<String> methods_to_execute) {
		this.methods_to_execute = methods_to_execute;
	}



	public TestMethod(String marker,
			ArrayList<String> methods_to_execute) {

		this.location = marker;
		this.methods_to_execute = methods_to_execute;
	}
	
	

}
