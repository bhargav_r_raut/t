package ExchangeBuilder;

import java.util.HashMap;

public class TestMethodExecutorArgs {
	
	String marker;
	HashMap<String,Object> arguments;
	
	
	
	public String getMarker() {
		return marker;
	}



	public void setMarker(String marker) {
		this.marker = marker;
	}



	public HashMap<String, Object> getArguments() {
		return arguments;
	}



	public void setArguments(HashMap<String, Object> arguments) {
		this.arguments = arguments;
	}



	public TestMethodExecutorArgs(String marker){
		setMarker(marker);
		setArguments(new HashMap<String,Object>());
	}
	
	public TestMethodExecutorArgs(String marker, HashMap<String,Object> args){
		setMarker(marker);
		setArguments(args);
	}
	

	
}
