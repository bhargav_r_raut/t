package ExchangeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.tradegenie.tests.DownloadEntityExchangeTests;
import com.tradegenie.tests.ShScript;
import com.tradegenie.tests.TestSingleton;
import com.tradegenie.tests.TestSingletonValue;

import yahoo_finance_historical_scraper.StockConstants;

public class TestMethodsExecutor {

	public static final String start_remote_es_server = "start_remote_es_server";
	public static final String stop_remote_es_server = "stop_remote_es_server";
	public static final String start_local_es_server = "start_local_es_server";
	public static final String stop_local_es_server = "stop_local_es_server";
	public static final String start_remote_redis_server = "start_remote_redis_server";
	public static final String stop_remote_redis_server = "stop_remote_redis_server";
	public static final String dont_shutdown_bulk_processors = "dont_shutdown_bulk_processors";
	public static final String check_entity_es_linked_list_is_empty = "check_entity_es_linked_list_is_empty";
	public static final String check_quandl_request_made_when_es_is_empty = "check_quandl_request_made";
	public static final String check_quandl_request_start_and_end_dates_when_es_is_empty = "check_quandl_request_start_and_end_dates";
	public static final String skip_after_download_entity_hook = "dont_do_after_download_entity_hook";
	public static final String skip_after_download_entity_index_hook = "" + "skip_after_download_entity_index_hook";
	public static final String skip_part_after_prune_price_map_internal = "skip_part_after_prune_price_map_internal";
	public static final String method_triggered = "method_triggered";
	public static final String throw_exception = "throw_exception";
	public static final String non_200_response = "non_200_response";
	public static final String invalid_json_response = "invalid_json_response";
	public static final String forex_date_not_in_response = "forex_date_not_in_response";
	public static final String forex_rates_key_not_in_response = "forex_rates_key_not_in_response";
	public static final String forex_symbol_not_in_response = "forex_symbol_not_in_response";
	public static final String quandl_empty_response = "quandl_empty_response";
	public static final String quandl_dataset_key_not_in_response = "quandl_dataset_key_not_in_response";
	public static final String quandl_data_key_not_in_dataset_key_in_response = "data_key_not_in_dataset_key_in_response";

	public static final String open_number_format_exception = "open_number_format_exception";
	public static final String close_number_format_exception = "close_number_format_exception";
	public static final String high_number_format_exception = "high_number_format_exception";
	public static final String low_number_format_exception = "low_number_format_exception";
	public static final String volume_number_format_exception = "volume_number_format_exception";

	// simulations for force redownload.
	public static final String force_redownload = "force redownload";

	// simulations for expected day id missing.
	public static final String expected_day_id_missing = "expected_day_id_missing";

	// simulation for prune due to volume zero
	public static final String volume_is_zero = "volume_is_zero";

	// simulation for force redownload due to close zero
	public static final String close_is_zero = "close_is_zero";

	// simulation for force redownload due to changed existing datapoint
	public static final String changed_existing_datapoint = "changed_existing_datapoint";

	// simulation where new datapoint is not added because it is invalid.
	public static final String new_datapoint_not_added_due_to_invalidity = "new_dataPoint_not_added_due_to_invalidity";

	//////////////////// simulations for STOP LOSS
	//////////////////// TESTS////////////////////////////////
	// skip execution in after_download_entity_hook
	public static final String stop_after_update_stop_loss = "stop_after_update_stop_loss";

	// simulation for generate_price_map_internal_test
	public static final String store_start_and_end_day_ids_of_price_map_internal = "store_start_and_end_day_ids_of_price_map_internal";

	// simulate test stat stop loss calculation.
	public static final String test_stat_stop_loss_calculation = "test_stat_stop_loss_calculation";

	// simulat test trailing stop loss calculation
	public static final String test_trailing_stop_loss_calculation = "test_trailing_stop_loss_calculation";

	public static final String test_trailing_stop_loss_highest_price_calculation = "test_trailing_stop_loss_highest_point_calculation";

	public static final String test_multiple_sub_tsl_falls_trigger_tsl = "multiple_sub_tsl_falls_trigger_tsl";
	
	//actually running
	public static final String test_tsl_as_bsl = "test_tsl_as_bsl";
	
	//actually running
	public static final String test_tsl_threshold = "test_tsl_threshold";
		
	public static final String test_tsl_first_run = "test_tsl_first_run";
	
	public static final String test_tsl_second_run = "test_tsl_second_run";
	
	public static final String test_basic_chunk_dechunk = "basic_chunk_dechunk";

	public static final String test_should_skip_part_after_update_to_remote = "test_should_skip_part_after_update_to_remote";

	/*****
	 * 
	 * stat stop loss multiple runs
	 * 
	 */
	public static final String test_stat_stop_loss_first_run = "test_stat_stop_loss_first_run";
	public static final String normalize_price_map_internal_before_stat_stop_loss_second_run =
			"normalize_price_map_internal_before_stat_stop_loss_second_run";

	public static void modify_all_price_change_and_stop_loss_arrays_for_basic_chunk_dechunk_test(Entity e) {

		ArrayList<String> open_close_price_change_arrays = new ArrayList<String>(
				Arrays.asList(e.getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
						e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));
		// first get the arrays in the order that they will be got in the
		// chunk_dechunk part.
		ArrayList<String> names_of_price_change_and_stop_loss_arrays = PairArray
				.get_names_of_arrays_applicable_to_entity(e);
		// if it is a large array then one thing
		HashMap<String, int[]> dummy_price_change_and_stop_loss_arrays_for_chunk_dechunk_test = new HashMap<String, int[]>();
		for (String s : names_of_price_change_and_stop_loss_arrays) {
			if (open_close_price_change_arrays.contains(s)) {

				PairArray.put_arr(
						PairArray.get_random_test_array(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
								* StockConstants.Entity_total_pairs_for_a_day, 0, 0, 100, -100),
						s, 0);
			} else {

				PairArray.put_arr(
						PairArray.get_random_test_array(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size(),
								1, 0, CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size()
										* StockConstants.Entity_total_pairs_for_a_day,
								1),
						s, 1);

			}
		}

	}
	
	/***
	 * assumed for a tsl of 2 meaning that the price rises by 2 percent
	 * so we say that if without falling the price directly falls by 3 percent,
	 * the tsl should be triggered, without moving in any direction.
	 * and behave like a simple bsl.
	 * 
	 * for this purpose, we set all the close prices to 100.00
	 * then we set one day the price to rise by 2 percent.
	 * 
	 * then for everyday before that, and upto 300 hundred days before
	 * the tsl should be triggered at that day.
	 * 
	 * 
	 */
	public static void modify_price_map_internal_for_tsl_as_bsl_test(Entity e){
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");
		}
		Integer start_modifying_at = 1457;
		e.getPrice_map_internal().get(start_modifying_at).setClose("102.00");
		TestSingletonValue tv = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
		tv.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().put(2,
				start_modifying_at);
		TestSingleton.getTsingle().put_singleton_value(tv);
	}
	
	/***
	 * basically the tsl should be triggered even with a 0.1% price move in the 
	 * opposite direction as the tsl 
	 * for eg: 
	 * if tsl is 2 percent, then it means that we want to protect against a rise in the price
	 * so if the price dips by even 0.1% it should shift the tsl down .i.e
	 * to 1.9% from the initial short price.
	 * 
	 * so for this what we do is that we set everything to 100.00
	 * then we set one day to move down by 0.1% so we make a price 99.9
	 * 
	 * then we move the price up again to go to 101.9
	 * and there the tsl should have got triggered.
	 * 
	 * @param e
	 */
	public static void modify_price_map_internal_for_tsl_threshold_test(Entity e){
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");
		}
		Integer start_modifying_at = 1457;
		e.getPrice_map_internal().get(start_modifying_at).setClose("99.9");
		e.getPrice_map_internal().get(1459).setClose("101.9");
		TestSingletonValue tv = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
		tv.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().put(2,
				1459);
		TestSingleton.getTsingle().put_singleton_value(tv);
	}
		
	
	/****
	 * the whole simulation is for a 2 percent tsl.
	 * this means that the end price is 2% higher than the start price.
	 * so it means that we dont want the price to rise.
	 * if the price falls, then we are happy as we will be in profit.
	 * so as soon as it moves to 9.99, the tsl gets activated.
	 * so at (last day) - 100 days, we first make the price 99.9
	 * this will be a 0.1% change compared to 100, hence triggering the tsl.
	 * 
	 * now at (last-day) - 50 days, we make the price 101.9 -> this is done, 
	 * basically the price has now moved against us, so here the tsl should be triggered.
	 * 
	 * for any other day we set the price to 100.0
	 * 
	 * this will trigger the tsl to start moving alongwith the price
	 * 
	 * @param e
	 */
	public static void modify_price_map_internal_for_tsl_first_run(Entity e){
		Integer total_dps = e.getPrice_map_internal().size();
		Integer dp_counter = 0;
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			if(dp_counter == (total_dps - 100)){
				//trigger the threshold change.
				e.getPrice_map_internal().get(day_id).setClose("99.9");
			}
			else if(dp_counter == (total_dps - 50)){
				e.getPrice_map_internal().get(day_id).setClose("101.9");
				TestSingletonValue tv = TestSingleton.getTsingle().
						get_singleton_value(new TestSingletonValue(e));
				tv.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().put(2,
						day_id);
				TestSingleton.getTsingle().put_singleton_value(tv);
			}
			else{
				e.getPrice_map_internal().get(day_id).setClose("100.00");
			}
			dp_counter++;
		}
		
	}
	
	/***
	 * in the second run we set all the newly downloaded datapoints to 100.00
	 * this is same as what is done in bsl second run test.
	 * @param e
	 */
	public static void modify_price_map_internal_for_second_tsl_run(Entity e){
		for(Integer day_id : e.getPrice_map_internal().keySet()){
			if(day_id > e.getLatest_existing_day_id()){
				e.getPrice_map_internal().get(day_id).setClose("100.00");
			}
		}
	}
	
	
	/***
	 * set all the close prices initially to 100
	 * 
	 * then take a random start day id.
	 * 
	 * there first set the value to 102.
	 * 
	 * this signifies a 2 percent trailing stop loss on the downside.
	 * 
	 * eg day_id -> price eg 1->100, 2->100, 3->100, 4->102, 5->100
	 * 
	 * so it should generate at the day id subsequent to the 102, a trailing
	 * stop loss is hit.
	 * 
	 * so for each and every day prior to that day the trailing stop loss should
	 * be hit at start_day_id -> 5
	 * 
	 * @param e
	 */
	public static void modify_price_map_internal_for_basic_trailing_stop_loss_test(Entity e) {
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");
		}
		Integer total_datapoints = e.getPrice_map_internal().size();
		Random r = new Random();
		Integer start_modifying_at = r.nextInt(total_datapoints - 60);
		start_modifying_at = 1457;
		e.getPrice_map_internal().get(start_modifying_at).setClose("102.50");
		TestSingletonValue tv = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
		tv.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().put(-2, start_modifying_at + 1);
		TestSingleton.getTsingle().put_singleton_value(tv);
	}

	/****
	 * 
	 * 
	 * 
	 * @param e
	 */
	public static void modify_price_map_internal_for_highest_price_tsl_test(Entity e) {
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");
		}
		Integer total_datapoints = e.getPrice_map_internal().size();
		Random r = new Random();
		Integer start_modifying_at = r.nextInt(total_datapoints - 60);
		start_modifying_at = 1457;

	}

	/***
	 * 
	 * modify the price map internal for the multiple sub-tsl falls
	 * 
	 * assume price change is like:
	 * 
	 * 0 -> 2.55 -> 2.25 -> 2.0 -> 1.7 -> 1 -> 0.5(here the tsl should be
	 * triggered)
	 * 
	 * 
	 * 
	 * 
	 */
	public static void modify_price_map_internal_for_multiple_sub_tsl_falls_triggering_tsl(Entity e) {
		Integer total_datapoints = e.getPrice_map_internal().size();
		Random r = new Random();
		Integer start_modifying_at = r.nextInt(total_datapoints - 60);
		start_modifying_at = 1457;
		int i = 0;
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");

			if (day_id >= start_modifying_at && i < 7) {

				if (i == 0) {
					e.getPrice_map_internal().get(day_id).setClose("100.00");
				} else if (i == 1) {
					e.getPrice_map_internal().get(day_id).setClose("102.55");
				} else if (i == 2) {
					e.getPrice_map_internal().get(day_id).setClose("102.25");
				} else if (i == 3) {
					e.getPrice_map_internal().get(day_id).setClose("102.00");
				} else if (i == 4) {
					e.getPrice_map_internal().get(day_id).setClose("101.70");
				} else if (i == 5) {
					e.getPrice_map_internal().get(day_id).setClose("101.00");
				} else if (i == 6) {
					e.getPrice_map_internal().get(day_id).setClose("100.40");
					TestSingletonValue tv = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
					tv.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().put(-2, day_id);
					TestSingleton.getTsingle().put_singleton_value(tv);
				}
				System.out.println(i);
				System.out.println("set day id:" + (day_id) + " to :"
						+ e.getPrice_map_internal().get(day_id).getClose().toString());
				i++;
			} else {
				// System.out.println("condition not satisfied because");
				// System.out.println("day id:" + day_id);
				// System.out.println("i:" + i);
			}

		}

	}
	
	/****
	 * WHAT WE DO HERE AND WHY?
	 * in the two run stat stop loss test
	 * what we want to do is as foloows:
	 * 1) in the first run, we first make all the close prices as 100.00
	 * then we modify prices of 6 days , starting 60 days before the last day, and at jumps of 
	 * five days so that we can simulate individual stop losses.
	 * we then compute the stat stop loss maps and commit the entity to elasticsearch
	 * 2) in the second run, i just want to keep the prices of all the newer downloaded 
	 * datapoints as 100.00, so that we can directly compare the stop loss maps before and after
	 * because the datapoints actually downloaded will be from real data, hence we must normalize.
	 * 
	 * 
	 * @param e
	 */
	public static void normalize_price_map_internal_for_second_run_of_stat_stop_loss_test(Entity e){
		for(Integer day_id : e.getPrice_map_internal().keySet()){
			if(day_id > e.getLatest_existing_day_id()){
				e.getPrice_map_internal().get(day_id).setClose("100.00");
			}
		}
	}

	/***
	 * how this works is as follows:
	 * 
	 * we start modifying at 60 days before the last_day_existing_id. i.e
	 * total_Datapoints - 60 we basically modify just one day id for each stop
	 * loss
	 * 
	 * eg:the stop losses array is: [-5,-3,-2,2,3,5] remember that we have
	 * flipped this array we started from the back so it became:
	 * [5,3,2,-2,-3,-5], this is done by setting the counter to 5, and then
	 * going backwards. we do this so that we could test the overlapping of the
	 * positive stop losses i.e 5 should override 3,2 we change the close price
	 * at day 500 - set the close to be 105 then after 5 days(we have set this
	 * arbitarily to skip every 5 days and then) for the next stop loss, we now
	 * will set at day 505 : close to be 103 and so on.
	 * 
	 */
	public static void modify_price_map_internal_for_stat_stop_loss_test
	(Entity e, Integer start_modifying_at) {
		// step one -> make all prices 1
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			e.getPrice_map_internal().get(day_id).setClose("100.00");
			// System.out.println(day_id);
		}

		Integer total_datapoints = e.getPrice_map_internal().size();
		Random r = new Random();

		start_modifying_at = start_modifying_at == null ? (total_datapoints - 60) : start_modifying_at;
		Integer counter = StockConstants.Entity_stop_loss_amounts.length - 1;
		
		
		
		
		for (Integer day_id : e.getPrice_map_internal().keySet()) {
			if (day_id >= start_modifying_at && counter > -1) {

				Integer stop_loss_amount = StockConstants.Entity_stop_loss_amounts[counter];

				Double d = Double.valueOf(String.valueOf(stop_loss_amount));
				// we want to change the thing by x percent of whatever.
				// and just add it to the current close.
				Double new_close = (d / 100.0) * (e.getPrice_map_internal().get(day_id).getClose().doubleValue())
						+ e.getPrice_map_internal().get(day_id).getClose().doubleValue();

				System.out.println("-----------------------------------------------------");
				System.out.println("day id we are setting is:" + day_id);
				System.out.println("stop loss amount is:" + stop_loss_amount);
				System.out.println("new close set is:" + new_close);
				System.out.println("-----------------------------------------------------");
				e.getPrice_map_internal().get(day_id).setClose(String.valueOf(new_close));
				TestSingletonValue tv = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
				tv.getDay_ids_where_dummy_stat_stop_losses_were_simulated()
						.put(StockConstants.Entity_stop_loss_amounts[counter], day_id);
				TestSingleton.getTsingle().put_singleton_value(tv);
				start_modifying_at += 5;
				counter--;

			}
		}

		// System.exit(1);

	}

	/****
	 * 
	 * some test functions that are called in different parts of the code
	 * shortened a bit.
	 * 
	 */
	public static void simulate_number_format_exception(String ohlcv_type) {
		String exception_type = null;
		switch (ohlcv_type) {
		case "open":
			exception_type = DownloadEntityExchangeTests.simulate_number_format_exception_open;
			break;

		case "close":
			exception_type = DownloadEntityExchangeTests.simulate_number_format_exception_close;
			break;

		case "high":
			exception_type = DownloadEntityExchangeTests.simulate_number_format_exception_high;
			break;

		case "low":
			exception_type = DownloadEntityExchangeTests.simulate_number_format_exception_low;
			break;

		case "volume":
			exception_type = DownloadEntityExchangeTests.simulate_number_format_exception_volume;
			break;

		default:
			break;
		}
		HashMap<String, Object> number_format_exception = TestMethodsExecutor
				.exec(new TestMethodExecutorArgs(exception_type));
		if (number_format_exception.keySet().size() == 1 && number_format_exception.containsValue(true)) {
			// throw new
			// Exception(DownloadEntityExchangeTests.simulate_non_200_response_code);
			throw new NumberFormatException();
		}
	}

	/**
	 * EXCHANGEBUILDERPACKAGECOORDINATOR
	 * 
	 */
	public static final String ExchangeBuilder_build_entity_exchange = "EB_build_entityExchange";

	/**
	 * @param markers_at_location
	 *            : at any point in our main code we can call this exec method
	 *            passing in a list of markers. Each marker should represent a
	 *            key in the #StockConstants.test_methods hash.
	 * 
	 *            So suppose at a place in the test program we want to execute
	 *            some arbitary code. We first put into the test_methods hash an
	 *            arbitarily named key, and as the value we pass in an arraylist
	 *            of methods to execute. These methods are public constants in
	 *            the TestMethodExecutor class.
	 * 
	 *            Then at the point in the program where you want the said
	 *            methods to be executed we call the exec method with an
	 *            arraylist of keys #markers_at_location. The method iterates
	 *            over each of these keys and if they are present in the
	 *            #StockConstants.test_methods_hash, then for that key whichever
	 *            methods are registered in teh above hash are executed and the
	 *            results of each of these methods is added to an arraylist
	 * 
	 *            so for a location -> many_markers for each marker ->
	 *            many_test_methods
	 * 
	 *            so returned value is an arraylist of arraylists of boolean
	 *            values.
	 * 
	 *            the argument is a hashmap of key -> marker name value ->
	 *            optional arraylist of arguments that can be passed in.
	 * 
	 */
	public static HashMap<String, Object> exec(TestMethodExecutorArgs t_args) {

		String marker = t_args.getMarker();
		TestMethod t = null;
		HashMap<String, Object> results_hashmap = new LinkedHashMap<String, Object>();
		// System.out.println("inside the exec method");
		// System.out.println("the marker is: " + marker);

		t = StockConstants.test_methods.containsKey(marker) ? StockConstants.test_methods.get(marker) : null;

		if (t != null) {
			// System.out.println("got a test method in the stockconstants with
			// this marker");
			// System.out.println(t.getMethods_to_execute());

			// LogObject lgd = new
			// LogObject(ExchangeBuilderPackageCoordinator.class);
			// lgd.add_to_info_packet("location", t.getLocation());
			// lgd.commit_debug_log("triggered test method");
			// System.out.println("the methods to execute in this test method
			// are");
			// System.out.println(t.getMethods_to_execute());
			for (String met : t.getMethods_to_execute()) {
				Entity e = null;
				TestSingletonValue v = null;
				Boolean method_result = false;
				switch (met) {
				case start_remote_es_server:
					// blah blah blah
					results_hashmap.put(start_remote_es_server, ShScript.do_es(false, true));
					break;
				case stop_remote_es_server:
					results_hashmap.put(stop_remote_es_server, ShScript.do_es(false, false));
					break;
				case start_local_es_server:
					results_hashmap.put(start_local_es_server, ShScript.do_es(true, true));
					break;
				case stop_local_es_server:
					results_hashmap.put(stop_local_es_server, ShScript.do_es(true, false));
					break;
				case start_remote_redis_server:
					results_hashmap.put(start_remote_redis_server, ShScript.start_remote_redis());
					break;
				case stop_remote_redis_server:
					results_hashmap.put(stop_remote_redis_server, ShScript.stop_remote_redis());
					break;
				case dont_shutdown_bulk_processors:
					results_hashmap.put(dont_shutdown_bulk_processors, true);
					break;

				case check_entity_es_linked_list_is_empty:
					e = (Entity) t_args.getArguments().get("entity");
					if (e.es_linked_list.isEmpty()) {
						v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
						v.setEntity_es_linked_list_empty(true);
						TestSingleton.getTsingle().put_singleton_value(v);
					}
					results_hashmap.put(check_entity_es_linked_list_is_empty, true);
					break;

				case check_quandl_request_made_when_es_is_empty:

					e = (Entity) t_args.getArguments().get("entity");
					if ((Boolean) t_args.getArguments().get("request_made")) {
						v = new TestSingletonValue(e);
						v.setRequest_made(true);
						TestSingleton.getTsingle().put_singleton_value(v);
					}
					results_hashmap.put(check_quandl_request_made_when_es_is_empty, true);

					// build the expected request start and end dates.
					v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));

					// System.out.println("the existing test singleton value"
					// + " holds whether request was made");
					// System.out.println(v.getRequest_made());
					// System.out.println("these are the arguments");
					// System.out.println(t_args.getArguments());
					// System.out.println("the request start date in the args
					// was");
					// System.out.println(t_args.getArguments()
					// .get("request_start_date"));
					// System.out.println("the central singleton expected start
					// date was");
					// System.out.println(CentralSingleton.getInstance().gmt_day_id_to_dataPoint
					// .firstEntry().getValue().getDateString());

					if (t_args.getArguments().get("request_start_date")
							.equals(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstEntry().getValue()
									.getDateString())) {
						// System.out.println("the start date was as expected");
						v.setRequest_start_date((String) t_args.getArguments().get("request_start_date"));
						// System.out.println("setting the start date on the
						// singleton value");
						// System.out.println(v.getRequest_start_date());
					}

					DateTime dt = new DateTime(DateTimeZone.forID("GMT"));
					dt = dt.plusDays(2);
					String expected_end_date_string = String.valueOf(dt.getYear()) + "-"
							+ StringUtils.leftPad(String.valueOf(dt.getMonthOfYear()), 2, "0") + "-"
							+ StringUtils.leftPad(String.valueOf(dt.getDayOfMonth()), 2, "0");

					// System.out.println("the end date in the args was");
					// System.out.println(t_args.getArguments().get("request_end_date"));
					// System.out.println("the expected end date was:");
					// System.out.println(expected_end_date_string);

					if (t_args.getArguments().get("request_end_date").equals(expected_end_date_string)) {
						// System.out.println("the end date equals expected");
						v.setRequest_end_date(expected_end_date_string);
						// System.out.println("setting the end date on the
						// singleton value");
						// System.out.println(v.getRequest_end_date());
					}

					TestSingleton.getTsingle().put_singleton_value(v);

					results_hashmap.put(check_quandl_request_start_and_end_dates_when_es_is_empty, true);

					break;

				case skip_after_download_entity_hook:
					results_hashmap.put(skip_after_download_entity_hook, true);
					break;

				case skip_after_download_entity_index_hook:
					results_hashmap.put(skip_after_download_entity_index_hook, true);
					break;

				case method_triggered:
					String method_triggered_name = (String) t_args.getArguments().get(method_triggered);
					e = (Entity) t_args.getArguments().get("entity");
					v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
					v.getMethods_triggered().add(method_triggered_name);
					TestSingleton.getTsingle().put_singleton_value(v);
					break;

				case throw_exception:
					results_hashmap.put(throw_exception, true);
					break;

				case non_200_response:
					results_hashmap.put(non_200_response, true);
					break;

				case invalid_json_response:
					results_hashmap.put(invalid_json_response, true);
					break;

				case forex_date_not_in_response:
					results_hashmap.put(forex_date_not_in_response, true);
					break;

				case forex_rates_key_not_in_response:
					results_hashmap.put(forex_rates_key_not_in_response, true);
					break;

				case forex_symbol_not_in_response:
					results_hashmap.put(forex_symbol_not_in_response, true);
					break;

				case quandl_empty_response:
					results_hashmap.put(quandl_empty_response, true);
					break;

				case quandl_dataset_key_not_in_response:
					results_hashmap.put(quandl_dataset_key_not_in_response, true);
					break;

				case quandl_data_key_not_in_dataset_key_in_response:
					results_hashmap.put(quandl_data_key_not_in_dataset_key_in_response, true);
					break;

				case open_number_format_exception:
					results_hashmap.put(open_number_format_exception, true);
					break;
				case close_number_format_exception:
					results_hashmap.put(close_number_format_exception, true);
					break;
				case high_number_format_exception:
					results_hashmap.put(high_number_format_exception, true);
					break;
				case low_number_format_exception:
					results_hashmap.put(low_number_format_exception, true);
					break;
				case volume_number_format_exception:
					results_hashmap.put(volume_number_format_exception, true);
					break;

				case force_redownload:
					results_hashmap.put(force_redownload, true);
					break;
				case expected_day_id_missing:
					results_hashmap.put(expected_day_id_missing, true);
					break;
				case stop_after_update_stop_loss:
					results_hashmap.put(stop_after_update_stop_loss, true);
					break;
				case volume_is_zero:
					results_hashmap.put(volume_is_zero, true);
					break;
				case close_is_zero:
					results_hashmap.put(close_is_zero, true);
					break;
				case changed_existing_datapoint:
					results_hashmap.put(changed_existing_datapoint, true);
					break;
				case new_datapoint_not_added_due_to_invalidity:
					results_hashmap.put(new_datapoint_not_added_due_to_invalidity, true);
					break;

				/****
				 * stop loss stuff starts here.
				 * 
				 */
				case test_stat_stop_loss_calculation:
					results_hashmap.put(test_stat_stop_loss_calculation, true);
					break;

				case test_trailing_stop_loss_calculation:
					results_hashmap.put(test_trailing_stop_loss_calculation, true);
					break;

				case test_trailing_stop_loss_highest_price_calculation:
					results_hashmap.put(test_trailing_stop_loss_highest_price_calculation, true);
					break;

				case test_multiple_sub_tsl_falls_trigger_tsl:
					results_hashmap.put(test_multiple_sub_tsl_falls_trigger_tsl, true);
					break;
					
				case test_tsl_as_bsl:
					results_hashmap.put(test_tsl_as_bsl, true);
					break;
					
				case test_tsl_threshold:
					results_hashmap.put(test_tsl_threshold, true);
					break;
					
				case test_tsl_first_run:
					results_hashmap.put(test_tsl_first_run, true);
					break;
				
				case test_tsl_second_run:
					results_hashmap.put(test_tsl_second_run, true);
					break;
				
				case test_stat_stop_loss_first_run:
					results_hashmap.put(test_stat_stop_loss_first_run, true);
					break;

				case normalize_price_map_internal_before_stat_stop_loss_second_run:
					results_hashmap.put(normalize_price_map_internal_before_stat_stop_loss_second_run, true);
					break;

				case test_basic_chunk_dechunk:
					results_hashmap.put(test_basic_chunk_dechunk, true);
					break;

				case test_should_skip_part_after_update_to_remote:
					results_hashmap.put(test_should_skip_part_after_update_to_remote, true);
					break;

				case store_start_and_end_day_ids_of_price_map_internal:
					// this will only have the details of the last one for each
					// entity.
					// for each end day id we iterate start day ids.
					// this will only contain for the last such pair.
					// LogObject lgd = new LogObject(Entity.class);
					// lgd.commit_debug_log("hit store start and end day ids
					// price map internal");
					e = (Entity) t_args.getArguments().get("entity");
					DataPoint start_day_id_datapoint = (DataPoint) t_args.getArguments().get("start_day_id_datapoint");
					DataPoint end_day_id_datapoint = (DataPoint) t_args.getArguments().get("end_day_id_datapoint");
					Integer start_day_id = (Integer) t_args.getArguments().get("start_day_id");
					Integer end_day_id = (Integer) t_args.getArguments().get("end_day_id");
					Integer size_of_price_map_in_range = (Integer) t_args.getArguments()
							.get("size_of_price_map_in_range");
					PairArray pa = (PairArray) t_args.getArguments().get("pair_array");
					PercentageIntegerCorrelate pc_correlate = (PercentageIntegerCorrelate) t_args.getArguments()
							.get("pc_correlate");
					v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));

					System.out.println("reach store stard end.");

					if (v.getP() != null) {
					} else {
						v.setP(pa);
						v.setPrice_map_in_range_size(size_of_price_map_in_range);
						v.setPrice_map_in_range_end_day_datapoint(end_day_id_datapoint);
						v.setPrice_map_in_range_start_day_datapoint(start_day_id_datapoint);
						v.setPc_correlate(pc_correlate);
						v.setStart_day_id(start_day_id);
						v.setEnd_day_id(end_day_id);
						System.out.println("testmethod executor.");
						System.out.println("v:" + v.getStart_day_id());
						TestSingleton.getTsingle().put_singleton_value(v);
					}

					System.out.println("retreiveing value from tssingleton.");
					v = TestSingleton.getTsingle().get_singleton_value(v);
					System.out.println("stored start day id is");
					System.out.println(v.getStart_day_id());
					System.out.println("end day id is:");
					System.out.println(v.getEnd_day_id());
					results_hashmap.put(store_start_and_end_day_ids_of_price_map_internal, true);
					break;
				default:
					break;
				}

			}
		}

		// LogObject lg = new LogObject(TestMethodsExecutor.class);
		// lg.add_to_info_packet("results_hasmap from testmethodexec",
		// results_hashmap);
		// lg.commit_debug_log("output from testmethodexecutor");
		return results_hashmap;

	}

}
