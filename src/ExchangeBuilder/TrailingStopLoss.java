package ExchangeBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import yahoo_finance_historical_scraper.StockConstants;

public class TrailingStopLoss {

	private Entity e;
	private TreeMap<Integer, PercentageIntegerCorrelate> tm;
	private Integer unsorted_array_index;
	private Integer tsl_amount;
	private Integer bsl_amount;
	private String tsl_array_name;
	private String open_or_close;
	private HashMap<String, int[]> tsl_hm;

	public HashMap<String, int[]> getTsl_hm() {
		return tsl_hm;
	}

	public void setTsl_hm(HashMap<String, int[]> tsl_hm) {
		this.tsl_hm = tsl_hm;
	}

	public String getOpen_or_close() {
		return open_or_close;
	}

	public void setOpen_or_close(String open_or_close) {
		this.open_or_close = open_or_close;
	}

	public String getTsl_array_name() {
		return tsl_array_name;
	}

	public void setTsl_array_name(String tsl_array_name) {
		this.tsl_array_name = tsl_array_name;
	}

	public Integer getTsl_amount() {
		return tsl_amount;
	}

	public void setTsl_amount(Integer tsl_amount) {
		this.tsl_amount = tsl_amount;
	}

	public Integer getBsl_amount() {
		return bsl_amount;
	}

	public void setBsl_amount(Integer bsl_amount) {
		this.bsl_amount = bsl_amount;
	}

	public TreeMap<Integer, PercentageIntegerCorrelate> getTm() {
		return tm;
	}

	public void setTm(TreeMap<Integer, PercentageIntegerCorrelate> tm) {
		this.tm = tm;
	}

	public Entity getE() {
		return e;
	}

	public void setE(Entity e) {
		this.e = e;
	}

	public Integer getUnsorted_array_index() {
		return unsorted_array_index;
	}

	public void setUnsorted_array_index(Integer unsorted_array_index) {
		this.unsorted_array_index = unsorted_array_index;
	}

	public TrailingStopLoss(Entity e, Integer tsl_amount, Integer bsl_amount, String open_or_close,
			HashMap<String, int[]> tsl_hm, TreeMap<Integer, PercentageIntegerCorrelate> tm) {
		this.e = e;
		this.open_or_close = open_or_close;
		this.tsl_array_name = PairArray.get_name_of_relevant_trailing_stop_loss_arr(e.getUnique_name(), open_or_close,
				tsl_amount, bsl_amount);
		this.tsl_amount = tsl_amount;
		this.bsl_amount = bsl_amount;
		this.tsl_hm = tsl_hm;
		this.tm = tm;
	}

	/***
	 * seems to be generating a list of day ids starting from the newest downloaded 
	 * day id, till the last newly added day id, but without skipping any day-ids in the middel.
	 * 
	 * why do we need this filled list
	 * this is because we miss some ids otherwise.
	 * the architecture is based on how the map is iterated later on.
	 * for eg:
	 * if we use the day ids directly from the price map internal,
	 * then when me minus three hundred from a particular day id, 
	 * a particular start day id will never be processed at all for stop losses,
	 * because it is outside the reach of the 300.
	 * so we construct a filled list to avoid this issue.
	 * assume situation where 
	 * 500, 510, 511
	 * 
	 * now consider the following situation.
	 * 500 - 300 => 200, so day 200 will be processed and all days following it with their pairs.
	 * 
	 * eg: 200 -> 201,202,203 .... 500
	 * 
	 * after doing 200(start_day_id), there is a break statement, so we basically only 
	 * process the first day id , with all the subsequent day_ids as end_day_ids, and then break.
	 * 
	 * now assume that after 500, there is directly 510
	 * , if we now minus 300 from this, we will directly have to use day_id 210 as the 
	 * start day id,and all the intervening day ids i.e 201 - 209, will not be processed at all.
	 * 
	 * for this reason we use this filled list.
	 * 
	 * 
	 * 
	 * 
	 * @return : continuous list
	 */
	public ArrayList<Integer> get_filled_list() {
		SortedMap<Integer, DataPoint> newly_downloaded_datapoints = getE().generate_newly_downloaded_datapoints();
		Integer newest_downloaded_dp = newly_downloaded_datapoints.firstKey();

		Integer prev_key = getE().getPrice_map_internal().lowerKey(newest_downloaded_dp);
		if (prev_key == null) {
			prev_key = -1;
		}
		ArrayList<Integer> a = new ArrayList<Integer>();
		for (int i = prev_key + 1; i < newly_downloaded_datapoints.lastKey(); i++) {
			a.add(i);
		}
		return a;
	}
	
	/***
	 * TODO: change the {@link PercentageIntegerCorrelate#trigger_tsl_move(String, Integer)}
	 * to use something like 0.1 percent of 1 , instead of the tsl argument being passed (Integer).
	 * 
	 * Then test what will happen on subsequent runs.
	 * 
	 * @throws Exception
	 */
	public void process() throws Exception {

		for (Integer day_id : get_filled_list()) {
			
			/***
			 * creating a map starting either at 0, or the current day 
			 * id - total_pairs_for_a_day.
			 * 
			 */
			Integer from_key = 0;
			if (day_id - StockConstants.Entity_total_pairs_for_a_day > 0) {
				from_key = day_id - StockConstants.Entity_total_pairs_for_a_day;
			}
			
			
			/***
			 * making the map using the above from_key and to_key being the day_id
			 * here even if the end day id is non-existent, it wont matter because map 
			 * is non-inclusive. Furthermore, the pair of maximum distance,
			 * i.e start_day_id -> end_day_id(the non inclusive one) is never checked to see if 
			 * it is the trailing stop loss. this is because even it if is the tsl, we will be exiting on 
			 * it anyway. So basically we are not analyzing that last pair here. but it doesnt make any difference
			 * 
			 * start day id will always exist.
			 * 
			 */
			SortedMap<Integer, DataPoint> iteratee_map = 
					getE().getPrice_map_internal().subMap(from_key, true, day_id, true);
			
			
			
			for (Map.Entry<Integer, DataPoint> ee : iteratee_map.entrySet()) {

				Integer start_day_id = ee.getKey();
				/***
				 * the existing value in the tsl_stop_loss_array
				 * for the given start_day_id
				 */
				Integer existing_tsl_index = getTsl_hm().get(getTsl_array_name())[start_day_id];
				
				/***
				 * ?? what is this
				 * 
				 */
				Integer tsl_trigger_pair_index = null;
				
				/***
				 * this condition means that the value at the tsl_index
				 * is the index of the first day of the next day_id.
				 * this means that no tsl has been set for this day yet.
				 */
				if (existing_tsl_index == PairArray.get_filler_id_for_sorted_map(start_day_id)) {
					
					/****
					 * again iterating the map created above.
					 * basically we are going one pair at a time.
					 * 
					 * so first we picked a day id as an end day id from the filled list
					 * then we got a map of that - 300 
					 * then we started iterating that map, from the start date
					 * now for each start date, we iterate the map again , 
					 * using the days as end dates 
					 */
					for (Integer end_day_id : iteratee_map.keySet()) {
						
						if ((end_day_id > start_day_id)
								&& (getTsl_hm().get(getTsl_array_name())[start_day_id] == 
								PairArray
										.get_filler_id_for_sorted_map(start_day_id))) {
							
							/****
							 * just getting the pair index in the large unsroted array
							 * for this start_day_id <-> end_day_id combination
							 */
							Integer pair_index = PairArray
									.get_int_array_index_given_start_and_end_day_id
									(start_day_id, end_day_id, getE())
									.getPair_difference_array_index();
							
							PercentageIntegerCorrelate pc = getTm().get(pair_index);
							
							/****
							 * check if this pair is applicable to the trailing stop loss
							 * amount, instead this should be maybe just 0.1 percent in the opposite direction
							 * of the stop loss amount sign.
							 * if yes -> set the trigger pair index to this index.
							 */
							if (tsl_trigger_pair_index == null) {
								
								if (pc.trigger_tsl_move
										(getOpen_or_close(),
										getTsl_amount())) {
									
									tsl_trigger_pair_index = pair_index;
								}
								else if(pc.tsl_violated_before_move(getOpen_or_close(), getTsl_amount())){
									
									getTsl_hm().get(getTsl_array_name())[start_day_id] = pair_index;
									//this breaks out of further day_id checking as if
									//a condition similar to a bsl being triggered has happend here.
									break;
								}
								//check here directly for a violation of the baseline
								//stop loss
							} else {
								//this does not need to be changed.
								//basically what we check is if there is another move more
								//than this, if yes then this becomes the trigger point.
								if (getTm().get(tsl_trigger_pair_index)
										.other_pc_is_greater(getOpen_or_close(), pc)) {
									
									tsl_trigger_pair_index = pair_index;
									
								} else {
									
									// need to get the pc from the end_day_id of
									// the previous trigger pair.
									// to the current end_day_id
									// and check if that is either
									Integer end_day_id_of_trigger_pair = PairArray
											.pair_differences_index_start_and_end_day_id
											(tsl_trigger_pair_index)
											.get("end_day_id");
									
									/***
									 * we are now constructing the pair from 
									 * the end_day_id of the trigger pair
									 * to the current end day_id.
									 * 
									 */
									PercentageIntegerCorrelate vector = getTm()
											.get(PairArray
													.get_int_array_index_given_start_and_end_day_id(
															end_day_id_of_trigger_pair,
															end_day_id, getE())
													.getPair_difference_array_index());
									// check if vector close_close or open_open
									// is
									
									/***
									 * so here the trigger is finally hit.
									 * 
									 */
									if (vector.vector_compared_to_tsl_amount(getTsl_amount(),
											getOpen_or_close()) == -1) {
										// tsl is triggered here
										// break out of the loop.

										getTsl_hm().get(getTsl_array_name())[start_day_id] = pair_index;

									}
								}

							}
						}
					}
				}
				break;
			}
		}

	}

}
