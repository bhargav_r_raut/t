package ExchangeBuilder;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSetter;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import yahoo_finance_historical_scraper.StockConstants;

public class dataPointIndicator {

	@JsonSerialize(using = BigDecimalSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private BigDecimal indicator_big_decimal_value;

	public BigDecimal getIndicator_big_decimal_value() {
		return indicator_big_decimal_value;
	}

	@JsonSetter
	public void setIndicator_big_decimal_value(String s) {
		this.indicator_big_decimal_value = new BigDecimal(s);
		this.indicator_integer_value = get_integer_from_big_decimal(this.indicator_big_decimal_value);
	}

	public void setIndicator_big_decimal_value(
			BigDecimal indicator_big_decimal_value) {
		this.indicator_big_decimal_value = indicator_big_decimal_value;
		this.indicator_integer_value = get_integer_from_big_decimal(this.indicator_big_decimal_value);
	}

	@JsonIgnore
	private Integer indicator_integer_value;

	@JsonIgnore
	public Integer getIndicator_integer_value() {
		return indicator_integer_value;
	}

	@JsonIgnore
	public void setIndicator_integer_value(Integer indicator_integer_value) {
		this.indicator_integer_value = indicator_integer_value;
	}

	private String indicator_name;

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	// -------------------------------------CONSTRCUTORS------------------------------------------------//

	// jackson
	public dataPointIndicator() {

	}

	/****
	 * there are two setters for the big_decimal_values(value, stddev, mean) one
	 * is string one is bigdecimal
	 * 
	 * @setter_one:string setter is used by jackson, when it deserializes from
	 *                    es.
	 * @setter_two:bigdecimal setter is used in the program when it is
	 *                        calculated during the program.
	 * 
	 *                        when the first setter is invoked no scaling or
	 *                        rounding is done, because that would have already
	 *                        been done when we put it into the database in the
	 *                        first place.
	 * 
	 *                        when the second setter is invoked the bigdecimal
	 *                        is rounded and scaled to the set constant values.
	 *                        for this the function
	 *                        {@link dataPointIndicator#get_bigdecimal_at_our_scale(BigDecimal)}
	 * 
	 *                        when either setter is invoked it also sets the
	 *                        indicator_integer_version of the value. for this
	 *                        the the function
	 *                        {@link dataPointIndicator#get_integer_from_big_decimal(BigDecimal)}
	 *                        is used.
	 * 
	 *                        thats all.
	 * 
	 * @param indicator_big_decimal_value
	 * @param indicator_name
	 */
	public dataPointIndicator(BigDecimal indicator_big_decimal_value,
			String indicator_name) {
		setIndicator_name(indicator_name);
		setIndicator_big_decimal_value(indicator_big_decimal_value);
	}

	public dataPointIndicator(String indicator_value, String indicator_name) {
		setIndicator_name(indicator_name);
		setIndicator_big_decimal_value(generate_bigdecimal_at_our_scale(indicator_value));
	}

	public BigDecimal generate_bigdecimal_at_our_scale(BigDecimal bd) {
		BigDecimal scaled_bd = bd.setScale(StockConstants.DataPoint_bd_scale,
				StockConstants.DataPoint_bd_rounding_mode);
		return scaled_bd;
	}

	public BigDecimal generate_bigdecimal_at_our_scale(String s) {
		BigDecimal scaled_bd = new BigDecimal(s).setScale(
				StockConstants.DataPoint_bd_scale,
				StockConstants.DataPoint_bd_rounding_mode);
		return scaled_bd;
	}

	public Integer get_integer_from_big_decimal(BigDecimal bd) {
		return bd
				.multiply(
						new BigDecimal(
								StockConstants.dataPointIndicator_mutliplier_for_big_decimal_conversion))
				.intValueExact();
	}
	
	/***
	 * when bigdecimal is converted to integer it is multiplied by a multiplier defined above.
	 * 
	 * that returns an integer.
	 * 
	 * this method returns just the bigdecimal after multiplication , without the integer conversion.
	 * 
	 * 
	 * @return
	 */
	public BigDecimal generate_multiplied_big_decimal() {
		return getIndicator_big_decimal_value()
				.multiply(
						new BigDecimal(
								StockConstants.dataPointIndicator_mutliplier_for_big_decimal_conversion));
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		dataPointIndicator other_dpi = (dataPointIndicator)obj;
		if(!other_dpi.getIndicator_name().equals(getIndicator_name())){
			return false;
		}
		if(!other_dpi.getIndicator_big_decimal_value().equals(getIndicator_big_decimal_value())){
			return false;
		}
		return true;
	}
	
	
	
	
}
