package IdAndShardResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.map.ObjectMapper;
import org.javatuples.Pair;

import ComplexProcessor.ConsecutiveAndPrevious;
import ComplexProcessor.IndicatorRiseFallAmount;
import ComplexProcessor.IndicatorSmaCross;
import ComplexProcessor.IndicatorStandardDeviation;
import ComplexProcessor.indicatorNDayHighLow;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.LogObject;

/**
 * provides a threadsafe class to resolve the id and the shard which holds the
 * relevant complex document in the es database.
 * 
 * it also provides suggestions ba sed on the provided input. it uses the
 * central singleton to access a concurrent hashmap that holds the name and id
 * of each subindicator , indicator or entity in the document, alongwith its
 * shard location.
 * 
 * for the subindicators it also has a small list of related subindicators that
 * can be appended to the indicator and the entity
 * 
 * this same class is used to generate and store into elasticsearch all the
 * complexes that we process.
 * 
 * it is also used in the complex calculation thread to determine the name of
 * the complex based on amounts of change etc.
 * 
 * @author aditya
 * 
 */
public class IdAndShardResolver {

	/***
	 * e221 -> what is the shard 07-e12-i12-s444
	 * 
	 * so we have to convert this representation to a zero padded version. we
	 * store it in the ids index
	 * 
	 * 
	 */

	/****
	 * 
	 * layout and space considerations for complex storage.
	 * 
	 * total indicators : 25 total entities : 335 ------------------------
	 * combinations : 8375 ------------------------
	 * 
	 * total subindicators per indicators : 1200
	 * 
	 * total complex documents to store in es: 10050000
	 * 
	 * each document stores the compressed buy list and compressed sell list.
	 * space required for 1 million documents with just 1 list per document -> 4
	 * gb.
	 * 
	 * total space required is(10 million documents, 2 lists per document, 4 gb
	 * per million documents) 10*4*2 = 80 gb.
	 * 
	 * 
	 * ----------------------------------- a seperate autocomplete index called
	 * the ids index contains the id terms. it is queried also for each word. in
	 * it the aliases are added , they hold the same id as whatever they alias
	 * for.
	 * 
	 */

	/**
	 * total periods we consider are 18.
	 * 
	 */

	public static final TreeMap<Integer, String> periods_map = new TreeMap<Integer, String>() {
		{
			put(1, "1 day");
			put(2, "2 days");
			put(3, "3 days");
			put(7, "1 week");
			put(9, "9 days");
			put(14, "2 weeks");
			put(21, "3 weeks");
			put(28, "1 month");
			put(42, "6 weeks");
			put(60, "2 months");
			put(90, "3 months");
			put(180, "6 months");
			put(270, "9 months");
			put(365, "1 year");
			put(450, "15 months");
			put(540, "18 months");
			put(630, "21 months");
			put(730, "2 years");
		}
	};
	
	
	/****
	 * currently its same as periods map, but can be changed to whatever is needed.
	 * 
	 * 
	 */
	public static final TreeMap<Integer, String> periods_map_for_t_tests = new TreeMap<Integer, String>() {
		{
			put(1, "1 day");
			put(2, "2 days");
			put(3, "3 days");
			put(7, "1 week");
			put(9, "9 days");
			put(14, "2 weeks");
			put(21, "3 weeks");
			put(28, "1 month");
			put(42, "6 weeks");
			put(60, "2 months");
			put(90, "3 months");
			put(180, "6 months");
			put(270, "9 months");
			put(365, "1 year");
			put(450, "15 months");
			put(540, "18 months");
			put(630, "21 months");
			put(730, "2 years");
		}
	};

	public static final LinkedHashMap<Integer, String> months_map = new LinkedHashMap<Integer, String>() {
		{
			put(1, "January");
			put(2, "February");
			put(3, "March");
			put(4, "April");
			put(5, "May");
			put(6, "June");
			put(7, "July");
			put(8, "August");
			put(9, "September");
			put(10, "October");
			put(11, "November");
			put(12, "December");
		}
	};

	public static final LinkedHashMap<Integer, String> days_map = new LinkedHashMap<Integer, String>() {
		{
			put(1, "Monday");
			put(2, "Tuesday");
			put(3, "Wednesday");
			put(4, "Thursday");
			put(5, "Friday");
			put(6, "Saturday");
			put(7, "Sunday");
		}
	};

	public static final LinkedHashMap<Integer, Object[]> nth_occurrence = new LinkedHashMap<Integer, Object[]>() {
		{
			put(1, new Object[] { "first", "1st" });
			put(2, new Object[] { "second", "2nd" });
			put(3, new Object[] { "third", "3rd" });
			put(4, new Object[] { "fourth", "4th" });
			put(5, new Object[] { "fifth", "5th" });
			put(6, new Object[] { "sixth", "6th" });
		}
	};

	public static final TreeMap<Integer, String> verbal_correlates = new TreeMap<Integer, String>() {
		{
			put(1, "infinitesimally small");
			put(2, "very small");
			put(3, "moderately small");
			put(4, "average");
			put(5, "moderately large");
			put(6, "large");
			put(7, "very large");
			put(8, "huge");
			put(9, "mammoth");
			put(10, "humongous");
		}
	};

	/********
	 * permutations map.
	 * 
	 * 
	 */

	public static final HashMap<String, String[]> permutations_map = new LinkedHashMap<String, String[]>() {
		{

			put("11", new String[] { "0", "shows up_up pattern",
					"rises for 2 days" });
			put("21", new String[] { "1", "shows down_up pattern" });
			put("12", new String[] { "2", "shows up_down pattern" });
			put("22", new String[] { "3", "shows down_down pattern",
					"falls for 2 days" });
			put("111", new String[] { "4", "shows up_up_up pattern",
					"rises for 3 days" });
			put("211", new String[] { "5", "shows down_up_up pattern" });
			put("121", new String[] { "6", "shows up_down_up pattern" });
			put("221", new String[] { "7", "shows down_down_up pattern" });
			put("112", new String[] { "8", "shows up_up_down pattern" });
			put("212", new String[] { "9", "shows down_up_down pattern" });
			put("122", new String[] { "10", "shows up_down_down pattern" });
			put("222", new String[] { "11", "shows down_down_down pattern",
					"falls for 3 days" });
			put("1111", new String[] { "12", "shows up_up_up_up pattern",
					"rises for 4 days" });
			put("2111", new String[] { "13", "shows down_up_up_up pattern" });
			put("1211", new String[] { "14", "shows up_down_up_up pattern" });
			put("2211", new String[] { "15", "shows down_down_up_up pattern" });
			put("1121", new String[] { "16", "shows up_up_down_up pattern" });
			put("2121", new String[] { "17", "shows down_up_down_up pattern" });
			put("1221", new String[] { "18", "shows up_down_down_up pattern" });
			put("2221",
					new String[] { "19", "shows down_down_down_up pattern" });
			put("1112", new String[] { "20", "shows up_up_up_down pattern" });
			put("2112", new String[] { "21", "shows down_up_up_down pattern" });
			put("1212", new String[] { "22", "shows up_down_up_down pattern" });
			put("2212",
					new String[] { "23", "shows down_down_up_down pattern" });
			put("1122", new String[] { "24", "shows up_up_down_down pattern" });
			put("2122",
					new String[] { "25", "shows down_up_down_down pattern" });
			put("1222",
					new String[] { "26", "shows up_down_down_down pattern" });
			put("2222", new String[] { "27",
					"shows down_down_down_down pattern", "falls for 4 days" });
			put("11111", new String[] { "28", "shows up_up_up_up_up pattern",
					"rises for 5 days" });
			put("21111",
					new String[] { "29", "shows down_up_up_up_up pattern" });
			put("12111",
					new String[] { "30", "shows up_down_up_up_up pattern" });
			put("22111", new String[] { "31",
					"shows down_down_up_up_up pattern" });
			put("11211",
					new String[] { "32", "shows up_up_down_up_up pattern" });
			put("21211", new String[] { "33",
					"shows down_up_down_up_up pattern" });
			put("12211", new String[] { "34",
					"shows up_down_down_up_up pattern" });
			put("22211", new String[] { "35",
					"shows down_down_down_up_up pattern" });
			put("11121",
					new String[] { "36", "shows up_up_up_down_up pattern" });
			put("21121", new String[] { "37",
					"shows down_up_up_down_up pattern" });
			put("12121", new String[] { "38",
					"shows up_down_up_down_up pattern" });
			put("22121", new String[] { "39",
					"shows down_down_up_down_up pattern" });
			put("11221", new String[] { "40",
					"shows up_up_down_down_up pattern" });
			put("21221", new String[] { "41",
					"shows down_up_down_down_up pattern" });
			put("12221", new String[] { "42",
					"shows up_down_down_down_up pattern" });
			put("22221", new String[] { "43",
					"shows down_down_down_down_up pattern" });
			put("11112",
					new String[] { "44", "shows up_up_up_up_down pattern" });
			put("21112", new String[] { "45",
					"shows down_up_up_up_down pattern" });
			put("12112", new String[] { "46",
					"shows up_down_up_up_down pattern" });
			put("22112", new String[] { "47",
					"shows down_down_up_up_down pattern" });
			put("11212", new String[] { "48",
					"shows up_up_down_up_down pattern" });
			put("21212", new String[] { "49",
					"shows down_up_down_up_down pattern" });
			put("12212", new String[] { "50",
					"shows up_down_down_up_down pattern" });
			put("22212", new String[] { "51",
					"shows down_down_down_up_down pattern" });
			put("11122", new String[] { "52",
					"shows up_up_up_down_down pattern" });
			put("21122", new String[] { "53",
					"shows down_up_up_down_down pattern" });
			put("12122", new String[] { "54",
					"shows up_down_up_down_down pattern" });
			put("22122", new String[] { "55",
					"shows down_down_up_down_down pattern" });
			put("11222", new String[] { "56",
					"shows up_up_down_down_down pattern" });
			put("21222", new String[] { "57",
					"shows down_up_down_down_down pattern" });
			put("12222", new String[] { "58",
					"shows up_down_down_down_down pattern" });
			put("22222", new String[] { "59",
					"shows down_down_down_down_down pattern",
					"falls for 5 days" });
			put("111111", new String[] { "60",
					"shows up_up_up_up_up_up pattern", "rises for 6 days" });
			put("222222", new String[] { "61",
					"shows down_down_down_down_down_down pattern",
					"falls for 6 days" });
			put("1111111", new String[] { "62",
					"shows up_up_up_up_up_up_up pattern", "rises for 7 days" });
			put("2222222", new String[] { "63",
					"shows down_down_down_down_down_down_down pattern",
					"falls for 7 days" });
			put("11111111",
					new String[] { "64",
							"shows up_up_up_up_up_up_up_up pattern",
							"rises for 8 days" });
			put("22222222", new String[] { "65",
					"shows down_down_down_down_down_down_down_down pattern",
					"falls for 8 days" });
			put("111111111", new String[] { "66",
					"shows up_up_up_up_up_up_up_up_up pattern",
					"rises for 9 days" });
			put("222222222",
					new String[] {
							"67",
							"shows down_down_down_down_down_down_down_down_down pattern",
							"falls for 9 days" });
			put("1111111111", new String[] { "68",
					"shows up_up_up_up_up_up_up_up_up_up pattern",
					"rises for 10 days" });
			put("2222222222",
					new String[] {
							"69",
							"shows down_down_down_down_down_down_down_down_down_down pattern",
							"falls for 10 days" });

		}
	};

	/******************************************************************************************
	 * 
	 * 
	 * 
	 * STANDARD DEVIATION NAME RESOLUTION AND FIND NEAREST STARTS HERE.
	 * 
	 * FOR STANDARD DEVIATION THE TOTAL SUBINDICATORS BOTH WAYS ARE:
	 * 
	 * 19*18*2 = 684
	 * 
	 */

	public static final TreeMap<Integer, Pair<String, ArrayList<String>>> 
	standard_deviation_names_and_aliases =

	new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
		{

			put(100, new Pair<String, ArrayList<String>>(
					"10 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("infinitesimally small"))));

			put(200, new Pair<String, ArrayList<String>>(
					"20 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("infinitesimally small"))));

			put(300, new Pair<String, ArrayList<String>>(
					"30 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("very small"))));

			put(400, new Pair<String, ArrayList<String>>(
					"40 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("very small"))));

			put(500, new Pair<String, ArrayList<String>>(
					"50 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("moderately small"))));

			put(600, new Pair<String, ArrayList<String>>(
					"60 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("moderately small"))));

			put(700, new Pair<String, ArrayList<String>>(
					"70 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("small"))));

			put(800, new Pair<String, ArrayList<String>>(
					"80 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("small"))));

			put(900, new Pair<String, ArrayList<String>>(
					"90 percent of standard deviation", new ArrayList<String>(
							Arrays.asList("average"))));

			put(1000,
					new Pair<String, ArrayList<String>>("1 standard deviation",
							new ArrayList<String>(Arrays.asList("average"))));

			put(1250,
					new Pair<String, ArrayList<String>>(
							"1.25x standard deviation", new ArrayList<String>(
									Arrays.asList("average"))));

			put(1500,
					new Pair<String, ArrayList<String>>(
							"1.5x standard deviation", new ArrayList<String>(
									Arrays.asList("moderately large"))));

			put(1750,
					new Pair<String, ArrayList<String>>(
							"1.75x standard deviation", new ArrayList<String>(
									Arrays.asList("moderately large"))));

			put(2000,
					new Pair<String, ArrayList<String>>(
							"2.0x standard deviation", new ArrayList<String>(
									Arrays.asList("large"))));

			put(2500,
					new Pair<String, ArrayList<String>>(
							"2.5x standard deviation", new ArrayList<String>(
									Arrays.asList("large"))));

			put(3000,
					new Pair<String, ArrayList<String>>(
							"3.0x standard deviation", new ArrayList<String>(
									Arrays.asList("very large"))));

			put(4000,
					new Pair<String, ArrayList<String>>(
							"4.0x standard deviation", new ArrayList<String>(
									Arrays.asList("very large"))));

			put(5000,
					new Pair<String, ArrayList<String>>(
							"5.0x standard deviation", new ArrayList<String>(
									Arrays.asList("huge"))));

			put(6000,
					new Pair<String, ArrayList<String>>(
							"6.0x standard deviation", new ArrayList<String>(
									Arrays.asList("mammoth"))));

		}
	};

	public static final String subindicator_prefix_for_standard_deviation_subindicators = "sa";

	/***
	 * @rise_or_fall -> if rise then 0 is passed, if fall then 1 is passed, if
	 *               none then null.
	 * @period -> a period in days for which the standard deviation was
	 *         calculated for the given indicator.
	 * @standard_deviation -> an integer holding the difference between todays
	 *                     mean and todays price as a ratio of the standard
	 *                     deviation today.
	 * 
	 * 
	 * @method:
	 * 
	 * @instantiate three arrays:
	 * @String[] rise_or_fall_prefix: if the provided rise or fall is null, then
	 *           both rises_by and falls_by are added, otherwise only the one
	 *           that is passed is added.
	 * @Integer[] periods : if the provided period is null, then all the periods
	 *            from the periods_map are added, otherwise only the provided
	 *            period is added.
	 * @Bigdecimal[] to hold the standard deviations if the provided standard
	 *               deviation is null, then we create integers out of all the
	 *               standard devaitions in the standard_deviations_and_aliases
	 *               map.
	 * 
	 * @method: after this we simply iterate over all the arrays in a nested
	 *          fashion.
	 * @step_one:the first step is to get the position of the standard deviation
	 *               in the standard deviation map. i.e its index. -> this is
	 *               used in the formula
	 * @step_two: also get position for the period, in the periods map.
	 * 
	 * 
	 * 
	 *            use the formula below to get teh id of the subindicator. the
	 *            name and the alias are simple string concats.
	 * 
	 * @formula: ((rise_or_fall_counter) i.e 0 or 1) MULTIPLIED BY
	 *           (max_possible_periods) MULTIPLIED_BY (max_possible standard
	 *           deviations) ADDED TO (current period_counter *
	 *           current_standard_Deviation_counter) + (current standard
	 *           deviation counter) ADDED TO rise_or_fall_counter
	 * 
	 * @explanation: first we need to figure out what are the maximum number of
	 *               subindicators that can be present for one side. i.e rise or
	 *               fall. so we get that by multiplying the total periods by
	 *               the total standard deviations. for rise we keep it as
	 *               0*(max_subindicators) -> so this will default to zero. for
	 *               fall (it will start with minimum value as max subindicators
	 *               for one side + (rise_or_fall_counter)).
	 * 
	 *               then we multiply the current period * current subindicator.
	 *               consider the first period and first standard dev. -> it
	 *               will be 0 * 0 => 0.
	 * 
	 *               then we add the current standard dev which will also be
	 *               zero => 0
	 * 
	 *               so the first one will get a grand value of 0
	 * 
	 *               and the last one will be simply the multiplication of the
	 *               two + the total standard deviation counter.
	 * 
	 *               ------------------ why do we add finally the rise or fall
	 *               counter to the formula? this is because for the rise we get
	 *               say n total things. for the fall, the first one will be =
	 *               n. this is not right, so at the end of the formula we also
	 *               add the rise or fall counter. so for fall, the first one
	 *               will become n + 1 the second one will become n + 1 + 1 and
	 *               so on.
	 * @return
	 */
	public static HashMap<String, String[]> get_standard_deviation_subindicator_names(
			Integer rise_or_fall, Integer period, Integer standard_deviation) {

		HashMap<String, String[]> subindicator_names_and_ids = new LinkedHashMap<String, String[]>();

		ArrayList<String> rise_or_fall_prefix = new ArrayList<String>();
		ArrayList<Integer> periods = new ArrayList<Integer>();

		if (rise_or_fall == null) {
			rise_or_fall_prefix = new ArrayList<String>(Arrays.asList(
					"rises by", "falls by"));
		} else if (rise_or_fall.equals(0)) {
			rise_or_fall_prefix = new ArrayList<String>(
					Arrays.asList("rises by"));
		} else {
			rise_or_fall_prefix = new ArrayList<String>(
					Arrays.asList("falls by"));
		}

		if (period == null) {
			periods = IndicatorStandardDeviation.custom_periods;
			;

		} else {
			periods = new ArrayList<Integer>(Arrays.asList(period));
		}

		ArrayList<Integer> standard_deviations = new ArrayList<Integer>();
		if (standard_deviation == null) {
			standard_deviations = new ArrayList<Integer>(
					standard_deviation_names_and_aliases.keySet());

		} else {

			standard_deviations = new ArrayList<Integer>(
					Arrays.asList(standard_deviation));
		}

		LogObject lgd = new LogObject(IdAndShardResolver.class);
		lgd.add_to_info_packet("rise or fall prefix becomes",
				rise_or_fall_prefix);
		lgd.add_to_info_packet("standard deviations are", standard_deviations);
		lgd.add_to_info_packet("periods become", periods);
		lgd.commit_debug_log("debug:Standard deviation name resolution.");

		for (String rofp : rise_or_fall_prefix) {
			for (Integer stddev : standard_deviations) {
				for (Integer p : periods) {

					LogObject lgd1 = new LogObject(IdAndShardResolver.class);
					lgd1.add_to_info_packet("rise or fall", rofp);
					lgd1.add_to_info_packet("standard deviation", stddev);
					lgd1.add_to_info_packet("period", p);

					Integer standard_deviation_position = find_position_of_nearest_element_to_this_one_in_the_given_treemap(
							stddev, standard_deviation_names_and_aliases);

					Integer nearest_standard_deviation = find_nearest(stddev,
							standard_deviation_names_and_aliases);

					Integer period_position = find_position_of_nearest_element_to_this_one_in_the_given_treemap(
							p, IndicatorStandardDeviation.custom_periods_map);

					Integer nearest_period = find_nearest(p,
							IndicatorStandardDeviation.custom_periods_map);

					Integer rise_or_fall_counter = 0;
					if (rofp.equals("falls by")) {
						rise_or_fall_counter = 1;
					}

					lgd1.add_to_info_packet("standard deviation position",
							standard_deviation_position);
					lgd1.add_to_info_packet("period position", period_position);
					lgd1.add_to_info_packet("rise or fall counter",
							rise_or_fall_counter);

					String subindicator_id = subindicator_prefix_for_standard_deviation_subindicators
							+ String.valueOf((rise_or_fall_counter
									* IndicatorStandardDeviation.custom_periods_map
											.size() * standard_deviation_names_and_aliases
										.size())
									+ (period_position * (standard_deviation_names_and_aliases
											.size()))
									+ standard_deviation_position);

					String subindicator_name = rofp
							+ " "
							+ standard_deviation_names_and_aliases.get(
									nearest_standard_deviation).getValue0()
							+ " in "
							+ IndicatorStandardDeviation.custom_periods_map
									.get(nearest_period);

					String subindicator_alias_name = rofp
							+ " a "
							+ standard_deviation_names_and_aliases
									.get(nearest_standard_deviation)
									.getValue1().get(0)
							+ " amount in "
							+ IndicatorStandardDeviation.custom_periods_map
									.get(nearest_period);

					lgd1.add_to_info_packet("id", subindicator_id);
					lgd1.add_to_info_packet("name", subindicator_name);
					lgd1.add_to_info_packet("alias name",
							subindicator_alias_name);
					lgd1.commit_debug_log("debug");

					subindicator_names_and_ids.put(subindicator_id,
							new String[] { subindicator_name,
									subindicator_alias_name });

				}
			}
		}

		return subindicator_names_and_ids;

	}

	/******************************************************************************************************
	 * STANDARD DEVIATION NAME RESOLUTION ENDS HERE.
	 * 
	 * 
	 * 
	 * 
	 **/

	/**********************************************************************************************
	 * 
	 * INDICATOR HIGH LOW NAME RESOLUTION STARTS HERE.
	 * 
	 * total subindicators from this group: 18(periods)*(2(either high or low))
	 * => 36, 169, 
	 * 
	 */

	public static final String subindicator_group_prefix_for_n_day_high_low = "sb";

	/******
	 * @days: the days for which it has been high or low.
	 * @high_or_low: 1 or -1
	 * @method:
	 * 
	 *          if high or low is null, then it will generate for both. if days
	 *          is null then it will generate for all available days.
	 * 
	 * @instantiate:
	 * @String[] high_low_prefixes: if high or low is either 1 or -1 then use
	 *           "high" or "low", otherwise it will add both.
	 * @Integer[] days:if days is null, then all periods from the period map are
	 *            used, otherwise only the required days is used. first
	 *            #find_nearest_period is invoked on it, then the nearest period
	 *            is used
	 * 
	 * 
	 * @method: iterate the high or low and inside that iterate the periods and
	 *          use the formula below to get the id, and simple string concat to
	 *          get the high or low subindicator name.
	 * 
	 * @formula: (high_low_counter*total_periods)*high_low_counter +
	 *           (periods_position) + (high_low_counter)
	 * 
	 *           to this prefix the subindicator_group_symbol.
	 * 
	 */
	public static HashMap<String, String[]> get_n_day_high_low_subindicator_names(
			Integer days, Integer high_or_low) {
		HashMap<String, String[]> subindicator_names_and_ids = new LinkedHashMap<String, String[]>();

		ArrayList<String> high_low_prefix = null;
		ArrayList<Integer> periods = null;
		if (high_or_low == null) {
			high_low_prefix = new ArrayList<String>(
					Arrays.asList("high", "low"));
		} else if (high_or_low.equals(0)) {
			high_low_prefix = new ArrayList<String>(Arrays.asList("high"));
		} else {
			high_low_prefix = new ArrayList<String>(Arrays.asList("low"));
		}

		if (days == null) {

			periods = indicatorNDayHighLow.custom_periods;

		} else {

			periods = new ArrayList<Integer>(Arrays.asList(days));

		}

		// System.out.println("days are:" + days);
		// System.out.println("periods are: " + periods);
		// System.out.println("high low prefixes are:" + high_low_prefix);
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Integer high_low_counter = 0;
		for (String hl : high_low_prefix) {

			if (hl.equals("high")) {
				high_low_counter = 0;
			} else {
				high_low_counter = 1;
			}

			for (Integer period : periods) {
				// System.out.println("period is:" + period);
				int period_position = find_position_of_nearest_element_to_this_one_in_the_given_treemap(
						period, indicatorNDayHighLow.custom_periods_map);
				// System.out.println(indicatorNDayHighLow.custom_periods);
				// System.out.println(indicatorNDayHighLow.custom_periods_map.headMap(period));

				// System.out.println("period position is:" + period_position);
				// System.out.println("high low counter:" + high_low_counter);

				String subindicator_id = subindicator_group_prefix_for_n_day_high_low
						+ ((high_low_counter * indicatorNDayHighLow.custom_periods_map
								.size()) + (period_position));
				// System.out.println("id is:" + subindicator_id);
				String name = indicatorNDayHighLow.custom_periods_map
						.get(period);
				Pattern p = Pattern.compile("s$");
				Matcher m = p.matcher(name);
				while (m.find()) {

					name = m.replaceFirst("");
				}

				String subindicator_name = name + " " + hl;
				subindicator_names_and_ids.put(subindicator_id,
						new String[] { subindicator_name });
			}

		}

		/**
		 * ObjectMapper mapper = new ObjectMapper(); try {
		 * System.out.println(mapper.defaultPrettyPrintingWriter()
		 * .writeValueAsString(subindicator_names_and_ids)); } catch
		 * (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { System.in.read(); } catch (IOException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 **/
		return subindicator_names_and_ids;
	}

	/***********************************************************************************************
	 * 
	 * INDICATOR N DAY HIGH LOW ENDS.
	 * 
	 * 
	 * 
	 */

	/*********************************************************************************
	 * 
	 * 
	 * 
	 * 
	 * SMA NAMES DERIVATION STARTS.
	 * 
	 * total subindicators provided:
	 * 
	 * {@link #periods_map.size} * {#periods_map.size - 1} basically for each period,
	 * there are total of (total periods - 1) combinations for crosses above. so
	 * for eg. 5 day sma.
	 * 
	 * 5 day sma crosses above everything except itself.
	 * 
	 * another eg: 3 day sma crosses above 5 day sma. and 5 day sma crosses
	 * above 3 day sma. so both ways are taken care of.
	 * 
	 * so currently we get a total of :=> 18*17 subindicators like this.
	 * 
	 * 306
	 * 
	 * 
	 */

	public static final String subindicator_prefix_for_sma_subindicators = "sc";

	/***
	 * given a crosser and a crossed.
	 * 
	 * @crosser -> can be null, or an integer period.
	 * @crossed -> can be null or an integer period.
	 * 
	 * @method:
	 * @instantiate two integer arrays.
	 * @crosser_array : contains either the crosser or all the periods in the
	 *                periods map.
	 * @crossed_array : contains either the crossed or all the periods in the
	 *                periods map.
	 * 
	 *                iterate each crosser, and inside that iterate each
	 *                crossed.
	 * 
	 *                get the position of the crosser,and the crossed, and
	 *                provided that they are not equal , then build.
	 * 
	 * @nomenclature_technique: {@link #get_sma_name_based_on_period(Integer)}
	 *                          contains the logic of how the name is derived.
	 * 
	 * @formula: prefix + ((total_periods - 1))*position of crosser + (position
	 *           of crossed)
	 * 
	 * @opposites_derivation: 1 -> 2, 1 -> 3, 1 -> 4, 1 -> 5, 1 -> 6 2 -> 1, 2
	 *                        -> 3, 2 -> 4, 2 -> 5, 2 -> 6 3 -> 1, 3 -> 2, 3 ->
	 *                        4, 3 -> 5, 3 -> 6 4 -> 1, 4 -> 2, 4 -> 3, 4 -> 5,
	 *                        4 -> 6 5 -> 1, 5 -> 2, 5 -> 3, 5 -> 4, 5 -> 6 6 ->
	 *                        1, 6 -> 2, 6 -> 3, 6 -> 4, 6 -> 5
	 * 
	 * @formula for opposites derivation: ((position of desired - position of
	 *          self)*(total_pairs)) - (position diff) : where positions refer
	 *          to the position of the said period in the periods map.
	 * 
	 * 
	 * @return
	 * @throws IOException
	 */
	public static HashMap<String, String[]> get_names_of_sma_subindicators(
			Integer crosser, Integer crossed) throws IOException {

		HashMap<String, String[]> subindicator_names_and_ids = new LinkedHashMap<String, String[]>();
		ArrayList<Integer> crosser_arraylist = crosser == null ? new ArrayList<Integer>(
				IndicatorSmaCross.custom_periods_map.keySet())
				: new ArrayList<Integer>(Arrays.asList(crosser));

		ArrayList<Integer> crossed_arraylist = crossed == null ? new ArrayList<Integer>(
				IndicatorSmaCross.custom_periods_map.keySet())
				: new ArrayList<Integer>(Arrays.asList(crossed));

		for (Integer crosser_element : crosser_arraylist) {
			// System.out.println("crosser element is:" + crosser_element);

			for (Integer crossed_element : crossed_arraylist) {
				// System.out.println("crossed element is:" + crossed_element);
				Integer position_of_crosser_element = find_position_of_nearest_element_to_this_one_in_the_given_treemap(
						crosser_element, IndicatorSmaCross.custom_periods_map);
				Integer position_of_crossed_element = find_position_of_nearest_element_to_this_one_in_the_given_treemap(
						crossed_element, IndicatorSmaCross.custom_periods_map);

				if (position_of_crossed_element.intValue() != position_of_crosser_element
						.intValue()) {
					String crosser_sma_name = get_sma_name_based_on_period(crosser_element);
					String crossed_sma_name = get_sma_name_based_on_period(crossed_element);

					Integer addendum = (position_of_crosser_element.intValue() > 0 && (position_of_crossed_element
							.intValue() < position_of_crosser_element)) ? 1 : 0;

					Integer subindicator_numeric_id = (((IndicatorSmaCross.custom_periods_map
							.size() - 1) * position_of_crosser_element)
							+ position_of_crossed_element + addendum);

					String subindicator_id = subindicator_prefix_for_sma_subindicators
							+ subindicator_numeric_id;

					String subindicator_name = crosser_sma_name
							+ " crosses above the " + crossed_sma_name;
					String alias_name = crossed_sma_name
							+ " crosses below the " + crosser_sma_name;

					subindicator_names_and_ids.put(subindicator_id,
							new String[] { subindicator_name, alias_name });

				}
			}
		}
		return subindicator_names_and_ids;
	}

	/*****
	 * @nomenclature: 1.if either of them is 1 -> then it becomes "" 2.if either
	 *                of them is 9 -> then it becomes "signal line" 3.for all
	 *                other situations -> it becomes "n day sma"
	 * 
	 * 
	 * 
	 * @param period
	 * @return
	 */
	public static String get_sma_name_based_on_period(Integer period) {
		if (period.equals(1)) {
			return "subindicator";
		} else if (period.equals(9)) {
			return "signal line";
		} else {
			return period + " day sma";
		}
	}

	/**********************************************************************************
	 * 
	 * 
	 * 
	 * sma names derivation ends....
	 * 
	 * 
	 * 
	 */

	/*********************************************************************************
	 * 
	 * 
	 * 
	 * CONSECUTIVE WEEKS MONTHS, YEARS RISE FALL STARTS
	 * 
	 * total subindicators got is 6 *2 * 2 = 24
	 * 
	 * {@link #how_many_weeks_or_months_subindicator_closes_up_or_down}*2*2
	 * 
	 * @explanation: suppose how many weeks_and_nth_day or months to take is 3
	 *               then it has to be done for both weeks_and_nth_day and
	 *               months, so multiply by two. and it has to be done for
	 *               closing up and down, so again multiply by 2.
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	public static final String subindicator_prefix_for_consecutive_subindicators = "sd";
	public static final Integer how_many_weeks_or_months_subindicator_closes_up_or_down = 6;

	/***
	 * we have up and down. we have weeks months and years
	 * 
	 * weeks -> 6 weeks - consecutive months -> 6 months - consecutive years ->
	 * 6 years - consecutive
	 * 
	 * if it is one -> then for the week.
	 * 
	 * 
	 * @param weeks_months_years
	 *            -> integer -> 0:weeks, 1:months, 2:year
	 * @param successive
	 *            -> integer , ranges from: -6 -> +6, cannot be zero. if
	 *            negative then implies fall, if positive then impplies rise for
	 *            those many successive periods.
	 * 
	 *            if either is null, then full options are considered for both.
	 *            ie. all custom periods will be used instead of successive. and
	 *            all three weeks, months and years will be used.
	 * 
	 */
	public static HashMap<String, String[]> get_names_for_consecutive_weeks_months_years(
			Integer weeks_months_years, Integer successive) {

		HashMap<String, String[]> results = new LinkedHashMap<String, String[]>();

		ArrayList<Integer> weeks_months_years_arraylist = new ArrayList<Integer>();
		ArrayList<Integer> periods = new ArrayList<Integer>();
		ArrayList<String> rise_and_fall_prefix = new ArrayList<String>();

		if (weeks_months_years == null || successive == null) {
			weeks_months_years_arraylist = ConsecutiveAndPrevious.weeks_or_months_or_years;
			periods = ConsecutiveAndPrevious.custom_periods;
			rise_and_fall_prefix.add("rises for");
			rise_and_fall_prefix.add("falls for");
		} else {
			weeks_months_years_arraylist.add(weeks_months_years);
			periods.add(successive);
			boolean res = successive.intValue() > 0 ? rise_and_fall_prefix
					.add("rises for") : rise_and_fall_prefix.add("falls for");
		}

		System.out.println("the weeks months years arraylist is:"
				+ weeks_months_years_arraylist);
		System.out.println("the periods are:" + periods);
		System.out.println("the rise and fall prefix is:"
				+ rise_and_fall_prefix);
		// now iterate outside from rise or fall prefix.
		// then iterate weeks_months_or_years
		// then iterate teh periods.
		// formula: (rise_or_fall)*18 + (week_or_month_of_year)*6 + period
		for (String rise_or_fall : rise_and_fall_prefix) {
			int rise_or_fall_counter = rise_or_fall.equals("rises for") ? 0 : 1;
			for (int week_or_month_or_year : weeks_months_years_arraylist) {
				String week_or_month_or_year_name = ConsecutiveAndPrevious.weeks_or_months_or_years_correlation
						.get(week_or_month_or_year);
				for (int period : periods) {
					int period_position = ConsecutiveAndPrevious.custom_periods
							.indexOf(Math.abs(period));

					String subindicator_id = subindicator_prefix_for_consecutive_subindicators
							+ ((rise_or_fall_counter * 18)
									+ (week_or_month_or_year) * 6 + period_position);

					String subindicator_name = rise_or_fall + " "
							+ Math.abs(period) + " consecutive "
							+ week_or_month_or_year_name;

					// substring to knock off the trailing s.
					if (Math.abs(period) == 1) {
						subindicator_name = rise_or_fall
								+ " the "
								+ week_or_month_or_year_name
										.substring(0,
												week_or_month_or_year_name
														.length() - 1);
					}

					results.put(subindicator_id,
							new String[] { subindicator_name });

				}
			}
		}

		// System.out.println(results);
		return results;

	}

	/*****
	 * 
	 * 
	 * SUBINDICATOR CONSECUTIVE ENDS.
	 * 
	 */

	/****
	 * 
	 * subindicator zero line
	 * 
	 * total 2 subindicators.
	 * 
	 * 
	 * @method: self explanator. id is 0 for crosses above and 1 for crosses
	 *          below. is prefixed by the group prefix. end of story.
	 * 
	 */
	public static final String subindicator_prefix_for_subindicator_zero_line_group = "se";

	/***
	 * 
	 * 
	 * @param cross
	 *            : 0 if indicator crosses above signal line, 1 if indicator
	 *            crosses below.
	 * @return
	 */
	public static HashMap<String, String[]> get_subindicator_names_for_zero_line(
			Integer cross) {

		HashMap<String, String[]> results_hashmap = new LinkedHashMap<String, String[]>();

		ArrayList<Integer> crosses = cross == null ? new ArrayList<Integer>(
				Arrays.asList(0, 1)) : new ArrayList<Integer>(
				Arrays.asList(cross));

		for (Integer cr : crosses) {
			// if its zero then
			String subindicator_id;
			String subindicator_name;
			String subindicator_alias_name;
			if (cr.intValue() == 0) {
				subindicator_id = subindicator_prefix_for_subindicator_zero_line_group
						+ "0";
				subindicator_name = "subindicator crosses above zero line.";
				subindicator_alias_name = "zero line crosses below subindicator";

			} else {
				subindicator_id = subindicator_prefix_for_subindicator_zero_line_group
						+ "1";
				subindicator_name = "subindicator crosses below zero line";
				subindicator_alias_name = "zero line crosses above subindicator";

			}
			results_hashmap.put(subindicator_id, new String[] {
					subindicator_name, subindicator_alias_name });
		}

		return results_hashmap;
	}

	/*******
	 * 
	 * END ZERO LINE GROUP.
	 * 
	 * 
	 * 
	 */

	public static final String subindicator_prefix_for_permutations_subindicator_group = "sf";

	/******************
	 * 
	 * @patterns have been generated upto a length of
	 *           {@link #permutation_max_length} days.
	 * @total patterns: 62
	 * @logic of this part:
	 * 
	 *        subindicator names starts of as "shows"
	 * 
	 * @param: pattern_permutation: this expects a string argument like for eg:
	 *         "11" if it is null, then all the patterns from the
	 *         {@link #permutations_map} are used. if it is not null then it is
	 *         used.
	 * 
	 * @formula: the subindicator id is got by simply the prefix + the value in
	 *           the permutations map.
	 * @nomenclature : the name is just the element at position 1 in the
	 *               permutation map(of the value)
	 * 
	 * 
	 * 
	 */

	public static HashMap<String, String[]> get_subindicator_name_for_permutations(
			String pattern_permutation) {
		HashMap<String, String[]> subindicator_names_and_ids = new LinkedHashMap<String, String[]>();

		if (pattern_permutation == null) {
			for (Map.Entry<String, String[]> entry : permutations_map
					.entrySet()) {

				String subindicator_id = subindicator_prefix_for_permutations_subindicator_group
						+ entry.getValue()[0];

				subindicator_names_and_ids.put(
						subindicator_id,
						Arrays.copyOfRange(entry.getValue(), 1,
								entry.getValue().length));
			}
		} else {

			if (permutations_map.get(pattern_permutation) != null) {

				String subindicator_id = subindicator_prefix_for_permutations_subindicator_group
						+ permutations_map.get(pattern_permutation)[0];

				subindicator_names_and_ids
						.put(subindicator_id,
								Arrays.copyOfRange(
										permutations_map
												.get(pattern_permutation),
										1,
										permutations_map
												.get(pattern_permutation).length));
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.defaultPrettyPrintingWriter()
					.writeValueAsString(subindicator_names_and_ids));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return subindicator_names_and_ids;
	}

	/*****
	 * 
	 * start subindicator for bullish and bearish divergence.
	 * 
	 * 2
	 * 
	 */

	/******
	 * starting subindicators for bullish and bearish divergence, can be null
	 * 
	 * given,
	 * 
	 * @integer(1 -> bullish divergence, -1 bearish divergence)
	 * 
	 * @method: the usual,
	 * @instantiate: a string array called bullish_or_bearish_array -> it has
	 *               "shows bullish divergence" and "shows bearish divergence"
	 *               if the provided param is null, otherwise it is either of
	 *               the two, depending on the provided parameter, so we
	 *               instantiate it by default to contain both and only change
	 *               it if the provided param is either 1 or -1
	 * 
	 * @instantiate: the usual bullish_bearish_counter
	 * 
	 *               then iterate the string array. ->
	 * @formula: usual, 0 -> bullish 1 -> bearish.
	 * 
	 *           end of story.
	 * 
	 * 
	 * 
	 * 
	 */
	public static final String subindicator_prefix_for_bullish_and_bearish_divergence_groups = "sg";

	public HashMap<String, String[]> get_subindicator_names_for_bullish_and_bearish_divergences(
			Integer bullish_or_bearish_divergence) {

		HashMap<String, String[]> subindicator_names_and_ids = new HashMap<String, String[]>();

		String[] bullish_or_bearish_divergence_array = new String[] {
				"shows bullish divergence", "shows bearish divergence" };

		bullish_or_bearish_divergence_array = (bullish_or_bearish_divergence
				.equals(1)) ? new String[] { "shows bullish divergence" }
				: new String[] { "shows bearish divergence" };

		Integer bullish_or_bearish_counter;
		for (String divergence_type : bullish_or_bearish_divergence_array) {

			bullish_or_bearish_counter = (divergence_type
					.equals("shows bullish divergence")) ? 1 : -1;

			String subindicator_id = subindicator_prefix_for_bullish_and_bearish_divergence_groups
					+ bullish_or_bearish_counter;
			String subindicator_name = divergence_type;
			subindicator_names_and_ids.put(subindicator_id,
					new String[] { subindicator_name });

		}

		return subindicator_names_and_ids;
	}

	/*****
	 * 
	 * start subindicators for time based subindicators.
	 * 
	 * days total : 7 * 7 = 49 months total: 12 = 12 weeks_of_months: 6 = 6
	 * quarters: 4 = 4
	 * 
	 * total : 71
	 * 
	 * 
	 */

	public static final String subindicator_prefix_for_time_based_subindicators = "sh";

	/******
	 * so an entity can be designated as the time calculator. from each index.
	 * it calculates the time shit.
	 * 
	 * @months_formula: start_index + position_in_months_map
	 * @days_formula: @formula: start_index + (7*(position_in_days_map - 1)) +
	 *                (position in nth occurrence map) + 1
	 * 
	 * 
	 * 
	 * @weeks_of_month_formula: start_index + position_in_nth_occurrence_map.
	 * @quarter_of_year: start_index + position_in_nth_occurrence_map (but only
	 *                   first four.)
	 * 
	 * @method: to start with instantiate four arraylists all holding integers:
	 * 
	 * 
	 * @if you have datapoint.
	 * @for the months array the: dp.month_of_year_integer is used, eg
	 *      1->january.
	 * @for the days array the: dp.day_of_week_integer is used, eg. 1->monday
	 * @for the week of month :dp.get_week_of_month_integer => eg. second, third
	 *      etc.
	 * @for the day of month: dp.get_nth_day_integer => eg 1,2,3,4,5,6 -> for
	 *      first monday, first tuesday etc.
	 * @for the quarter of month: dp.get_quarter_of_year_integer => 1,2,3,4
	 * 
	 * @you dont have datapoint. they just hold either the keyset of the
	 *      months_map(for months) they hold the keyset of the days map for the
	 *      days(i.e day of the week) they hold the keyset of the nth occurrence
	 *      for the week of months, nth day of month and the quarters.
	 * 
	 * 
	 *      for all of them just follow the formula. only in case of day of
	 *      month there is a special case:
	 * 
	 * @iterate : outside
	 * 
	 * @return
	 */
	public HashMap<String, String[]> get_subindicator_names_for_time_based_group(
			ArrayList<DataPoint> ar_dp) {

		HashMap<String, String[]> subindicator_names_and_ids = new HashMap<String, String[]>();

		ArrayList<Integer> months = new ArrayList<Integer>();
		ArrayList<Integer> days = new ArrayList<Integer>();
		ArrayList<Integer> week_of_month_integers = new ArrayList<Integer>();
		ArrayList<Integer> day_of_month_integers = new ArrayList<Integer>();
		ArrayList<Integer> quarters_integers = new ArrayList<Integer>();

		if (!ar_dp.equals(null)) {
			for (DataPoint dp : ar_dp) {
				months.add(dp.getMonth_of_year_integer());
				days.add(dp.getDay_of_week_integer());
				week_of_month_integers.add(dp.getWeek_of_month_integer());
				day_of_month_integers.add(dp.getNth_day_integer());
				quarters_integers.add(dp.getQuarter_of_year_integer());
			}
		} else {
			months.addAll(months_map.keySet());
			days.addAll(days_map.keySet());
			week_of_month_integers.addAll(nth_occurrence.keySet());
			day_of_month_integers.addAll(nth_occurrence.keySet());
			quarters_integers.addAll(nth_occurrence.keySet());
		}

		Integer start_index = 0;
		for (Integer month : months) {
			String subindicator_id = subindicator_prefix_for_time_based_subindicators
					+ (month - 1) + start_index;
			String subindicator_name = months_map.get(month);
			subindicator_names_and_ids.put(subindicator_id,
					new String[] { subindicator_name });

		}

		start_index = 11;
		for (Integer day : days) {
			for (Integer nth_occ : day_of_month_integers) {
				// case: artificially make the nth occ as 0.
				String subindicator_id = subindicator_prefix_for_time_based_subindicators
						+ (7 * (day - 1)) + 0 + 1 + start_index;
				// eg : on monday.
				String subindicator_name = days_map.get(day);
				subindicator_names_and_ids.put(subindicator_id,
						new String[] { subindicator_name });

				// case 1 - use the actual nth occ:
				subindicator_id = subindicator_prefix_for_time_based_subindicators
						+ (7 * (day - 1)) + nth_occ + 1 + start_index;
				subindicator_name = (String) nth_occurrence.get(nth_occ)[0]
						+ " " + days_map.get(day);
				String subindicator_alias_name = (String) nth_occurrence
						.get(nth_occ)[1] + " " + days_map.get(day);

				subindicator_names_and_ids.put(subindicator_id, new String[] {
						subindicator_name, subindicator_alias_name });

			}
		}

		start_index = 60;
		for (Integer week_of_month : week_of_month_integers) {
			String subindicator_id = subindicator_prefix_for_time_based_subindicators
					+ (week_of_month + start_index);
			String subindicator_name = (String) nth_occurrence
					.get(week_of_month)[0] + " week";
			String subindicator_alias_name = (String) nth_occurrence
					.get(week_of_month)[1] + " week";
			subindicator_names_and_ids.put(subindicator_id, new String[] {
					subindicator_name, subindicator_alias_name });

		}

		start_index = 66;
		for (Integer quarter : quarters_integers) {
			if (quarter <= 4) {
				String subindicator_id = subindicator_prefix_for_time_based_subindicators
						+ (quarter + start_index);
				String subindicator_name = (String) nth_occurrence.get(quarter)[0]
						+ " quarter";
				String subindicator_alias_name = (String) nth_occurrence
						.get(quarter)[1] + " quarter";
				subindicator_names_and_ids.put(subindicator_id, new String[] {
						subindicator_name, subindicator_id });
			}
		}

		return subindicator_names_and_ids;

	}

	/******************************************************************************************
	 * rise and fall amounts resolution starts here.
	 * 
	 * 
	 * 
	 */

	public static final Integer max_permissile_standard_deviations_or_percentage_changes_for_a_period = 19;

	public static final String subindicator_prefix_for_rise_and_fall_group = "si";

	/***
	 * 
	 * @param period
	 *            : the period over which the change is calculated
	 * @param percentage_change
	 *            : the integer correlate of the percentage change as calculated
	 *            in the {@link #DataPoint
	 *            #calculate_differences_from_end_day_datapoint} - can be null,
	 *            in which case names will be generated for all the things in
	 *            the {@link #rise_and_fall_amounts}
	 * @param rise_or_fall
	 *            : 0 if rise, and 1 if fall, or null if required to generate
	 *            for all.
	 * 
	 * @method_of_calculatation:
	 * 
	 * @get position of desired period in the treemap.
	 * @19 slots are pre-alloted per period.
	 * 
	 * 
	 * @formula: #total_possible_elements_for_either_side = 19*18 = 342
	 * 
	 *           rise_or_fall*(total_possible_elements_for_either_side) +
	 *           (position_in_periods_map*19) +
	 *           position_in_percentage_map_at_that_period
	 * 
	 * 
	 * @explanation:
	 * 
	 *               rise_or_fall*total_possible_elements_for_either_side => so
	 *               for rise, this will work out to zero, for fall it will work
	 *               out to 342
	 * 
	 *               then (position in periods_map*19) => if this is zero it
	 *               will remain zero(max position can be 17) then (position in
	 *               percentage_map_at_that_period) => max can be 18
	 * 
	 *               so for the first element for rise in the first period the
	 *               value will be: 0 + 0 + 0
	 * 
	 *               last element in rise will be 0 + 17*19 + 18 = 341
	 * 
	 * 
	 *               and for the firt element for fall will be : 342 + 0 + 0
	 * 
	 * 
	 *               to generate this the following things are instantiated.
	 * @instantiate rise_or_fall_prefix_array -> contains both "rises_by" and
	 *              "falls_by" if provided rise or fall is null.
	 * 
	 * @instantiate: periods: Integer[] contains all the periods from the
	 *               periods map or the provided period.
	 * 
	 * @instantiate: percentage_change: integer[] if a percentage is not
	 *               provided, then this array is kept empty, otherwise it
	 *               contains the percentage change that was provided.
	 * 
	 *               if no percentage is provded, then the map is rebuilt inside
	 *               each period with all the percentages that are available in
	 *               that period.
	 * 
	 * @iterate iterate outer rise_or_fall_array inside that iterate the periods
	 *          inside that iterate the percentage changes.
	 * 
	 */
	public static HashMap<String, String[]> get_rise_and_fall_amount_subindicators_names(
			Integer period, Integer percentage_change, Integer rise_or_fall)
			throws Exception {

		// System.out.println("percentage change is:" + percentage_change);
		// System.out.println("rise or fall is:" + rise_or_fall);
		// System.out.println("period is:" + period);
		HashMap<String, String[]> results_hashmap = new LinkedHashMap<String, String[]>();

		ArrayList<String> rise_or_fall_prefix_array = new ArrayList<String>();
		if (rise_or_fall == null) {
			// System.out.println("rise or fall is null");
			rise_or_fall_prefix_array = new ArrayList<String>(Arrays.asList(
					"rises by", "falls by"));
		} else {
			if (rise_or_fall.equals(0)) {
				rise_or_fall_prefix_array = new ArrayList<String>(
						Arrays.asList("rises by"));
			} else {
				rise_or_fall_prefix_array = new ArrayList<String>(
						Arrays.asList("falls by"));
			}
		}

		ArrayList<Integer> period_array = new ArrayList<Integer>();

		if (period != null) {
			period_array = new ArrayList<Integer>(Arrays.asList(period));
		} else {
			period_array = IndicatorRiseFallAmount.custom_periods;
		}

		ArrayList<Integer> percentage_change_array = new ArrayList<Integer>();
		Boolean do_all_percentages_for_all_periods = false;
		if (percentage_change != null) {
			percentage_change_array = new ArrayList<Integer>(
					Arrays.asList(percentage_change));

		} else {
			do_all_percentages_for_all_periods = true;
		}

		// System.out.println("rise or fall amounts are:");
		for (String s : rise_or_fall_prefix_array) {
			// System.out.println(s);
		}

		// System.out.println("percentage change amounts are:");
		for (Integer i : percentage_change_array) {
			// System.out.println(i);
		}

		for (String rise_or_fall_string : rise_or_fall_prefix_array) {

			for (Integer period_i : period_array) {
				// System.out.println("doing period i:" + period_i);
				if (do_all_percentages_for_all_periods) {

					percentage_change_array = new ArrayList<Integer>(
							RiseFallConstants.rise_and_fall_amounts.get(
									period_i).keySet());

				}

				// System.out.println("rise or fall string is:" +
				// rise_or_fall_string);

				Integer rise_or_fall_counter = rise_or_fall_string
						.equals("rises by") ? 0 : 1;

				// System.out.println("rise or fall counter is:"
				// + rise_or_fall_counter);

				Integer nearest_period_in_periods_map = find_nearest(period_i,
						IndicatorRiseFallAmount.custom_periods_map);
				Integer position_of_period_in_periods_map = find_position(
						nearest_period_in_periods_map,
						IndicatorRiseFallAmount.custom_periods_map);

				// System.out.println("the nearest period is:"
				// + nearest_period_in_periods_map);

				// System.out.println("the position of that nearest period is:"
				// + position_of_period_in_periods_map);

				for (Integer percentage_change_i : percentage_change_array) {

					// System.out.println("doing percentage change:"
					// + percentage_change_i);

					Integer nearest_amount_in_rise_and_fall_amounts_map_at_the_said_period_to_this_percentage = find_nearest(
							percentage_change_i,
							RiseFallConstants.rise_and_fall_amounts
									.get(period_i));

					Integer position_in_rise_and_fall_amounts_map_at_the_said_period_of_this_percentage = find_position(
							nearest_amount_in_rise_and_fall_amounts_map_at_the_said_period_to_this_percentage,
							RiseFallConstants.rise_and_fall_amounts
									.get(period_i));

					// System.out
					// .println("nearest percentage change is:"
					// +
					// nearest_amount_in_rise_and_fall_amounts_map_at_the_said_period_to_this_percentage);
					// System.out
					// .println("positon of nearest percentage change is :"
					// +
					// position_in_rise_and_fall_amounts_map_at_the_said_period_of_this_percentage);

					String subindicator_id = subindicator_prefix_for_rise_and_fall_group
							+ (rise_or_fall_counter
									* (IndicatorRiseFallAmount.custom_periods_map
											.size() * max_permissile_standard_deviations_or_percentage_changes_for_a_period)
									+ (position_of_period_in_periods_map * max_permissile_standard_deviations_or_percentage_changes_for_a_period) + (position_in_rise_and_fall_amounts_map_at_the_said_period_of_this_percentage));

					String subindicator_name = rise_or_fall_string
							+ " "
							+

							RiseFallConstants.rise_and_fall_amounts
									.get(period_i)
									.get(nearest_amount_in_rise_and_fall_amounts_map_at_the_said_period_to_this_percentage)
									.getValue0()
							+ " in "
							+ IndicatorRiseFallAmount.custom_periods_map
									.get(period_i);

					ArrayList<String> alias_names = RiseFallConstants.rise_and_fall_amounts
							.get(period_i)
							.get(nearest_amount_in_rise_and_fall_amounts_map_at_the_said_period_to_this_percentage)
							.getValue1();

					// System.out.println("subindicator id is:" +
					// subindicator_id);
					// System.out.println("subindicator name is:" +
					// subindicator_name);
					// System.out.println("alias names are:" + alias_names);

					String[] subindicator_name_and_aliases = new String[alias_names
							.size() + 1];
					subindicator_name_and_aliases[0] = subindicator_name;
					Integer alias_counter = 1;
					for (String alias_name : alias_names) {
						subindicator_name_and_aliases[alias_counter] = rise_or_fall_string
								+ " a "
								+ alias_name
								+ " amount "
								+ " in "
								+ IndicatorRiseFallAmount.custom_periods_map
										.get(period_i);
						alias_counter++;
					}

					results_hashmap.put(subindicator_id,
							subindicator_name_and_aliases);

				}

			}

		}

		return results_hashmap;

	}

	/****************************************
	 * there are two autocomplete indices: one is the queries cache -this
	 * contains just the two predefined queries -what happens to x when this
	 * like last all -what happens to x when y happens.(y being on
	 * esubindicator) -all the user generated queries = successfully answered
	 * -this cache has the individual split up tokens of the queries as
	 * contexts. -so suppose that someone is searching for something and we dont
	 * find it ine base inputs. ,but we have found using the other cache some
	 * entity, or indicator or subindicator then we search the context of the
	 * first cache for the combination of these. a format for the order of
	 * combination will have to be decided. it can contain news items, or
	 * anything. but from the final result of the query that was generated.
	 * 
	 * what you are planning to put into the other cache: were single,double and
	 * multiterm inputs for the entity, indicator, subindicator and news terms.
	 * 
	 * so let it just return the top ranking stuff, forget about what it is.
	 * first tiem query is sent, it send in the params, a session variable which
	 * tells us to take from which character of the query string, if this
	 * character is zero, and if a query does not match, we can search from the
	 * last whitespace, since the start charac is zero. otherwise we will have
	 * the charac other than zero.
	 * 
	 * when we reach the point where we get dont get a result two queries are
	 * fired.
	 * 
	 * 
	 * 
	 * one generates combinations as per the formula and peforms a context
	 * search for those cobinations. hoping that this is just another invocation
	 * of somethign that we already have to generate these combinations, we use
	 * the payload of whatever we have till now.
	 * 
	 * another searches from the last whitespace, but for a specific context
	 * called term(this contains all the individual indicator,
	 * subindicator,entity and news terms.)
	 * 
	 * the combined payloads of the combination of things that are got by the
	 * time the user presses enter are sent to the query parser, alongiwth the
	 * positino in the query string of each thing in the payload. so we want to
	 * know where are the entities, indicators, subindicators, time determinants
	 * and news terms in teh query string.
	 * 
	 * then the query parser simply builds combinations out of the things. it
	 * ifnds the nearest subindicator to each indicator and the nearest entity
	 * to each indicator then it checks for boolean operators in between these
	 * postions and fires a query, and sends back the results .
	 * 
	 * it stores the other possible query strings in the session and returns the
	 * first result. then the client is instructed to send the remaining query
	 * strings, for the rest of the resulst in one go.
	 * 
	 * 
	 * 
	 * 
	 * one is the single terms cache. this contains the
	 * 
	 */

	/***
	 * 
	 * building the subindicator names for the rises by and falls by amounts.
	 * will write a program to download all the data for nasdaq stocks. then
	 * will build aggregations on the datapoints. to find the changes in prices
	 * for periods. this will also test our download logic.
	 * 
	 * 
	 * 
	 */

	/*********************************************************************************************************
	 * 
	 * 
	 * 
	 * UTILITY METHODS
	 * 
	 * 
	 */

	/***
	 * given a permutation pattern like "11221" what this does is iterates each
	 * character in the pattern permutation and if it is 1 then it writes up_
	 * otherwise down_ it was used when we prepared the permutations map and not
	 * actively used.
	 * 
	 * @param pattern_permutation
	 * @param subindicator_name
	 * @return
	 */
	public static String get_permutation_subindicator_name_from_pattern(
			String pattern_permutation, String subindicator_name) {
		for (int i = 0; i < pattern_permutation.length(); i++) {
			if (pattern_permutation.charAt(i) == ("1").charAt(0)) {
				subindicator_name += "up_";
			} else {
				subindicator_name += "down_";
			}
		}
		subindicator_name = subindicator_name.substring(0,
				subindicator_name.length() - 1);
		subindicator_name = subindicator_name + " pattern";
		return subindicator_name;
	}

	public static Integer[] get_periods_from_periods_map(Integer first_n) {

		if (first_n.equals(null)) {
			first_n = periods_map.size();
		}

		Integer[] periods = null;

		periods = new Integer[periods_map.size()];

		periods = new ArrayList<Integer>(periods_map.keySet()).toArray(periods);

		return Arrays.copyOfRange(periods, 0, first_n);

	}

	/***
	 * gets the zero based postion of the nearest element to this element in the
	 * treemap
	 * 
	 * 
	 */

	public static int find_position_of_nearest_element_to_this_one_in_the_given_treemap(
			Integer element, TreeMap<Integer, ?> search_in) {
		return search_in.headMap(find_nearest(element, search_in)).size();
	}

	/****
	 * @explanation:
	 * @if the req is already in the map, return it.
	 * 
	 * @if there is no higher key, return the lower key.
	 * @else there is a higher key:
	 * @if there is no lower key => return the higher key.
	 * @else
	 * @if higher_period_diff <= lower_period_diff return the higher key.
	 * @else return the lower key. preference is thus for the high key in case
	 *       of equality.
	 * 
	 * @param req
	 * @param search_in
	 * @return
	 */
	public static Integer find_nearest(Integer req,
			TreeMap<Integer, ?> search_in) {

		req = Math.abs(req);
		if (search_in.containsKey(req)) {
			return req;
		} else {
			if (search_in.higherKey(req) != null) {
				if (search_in.lowerKey(req) == null) {
					return search_in.higherKey(req);
				}
				Integer higher_period_diff = search_in.higherKey(req) - req;
				Integer lower_period_diff = req - search_in.lowerKey(req);
				if (higher_period_diff <= lower_period_diff) {
					return search_in.higherKey(req);
				}
				return search_in.lowerKey(req);
			} else {
				return search_in.lowerKey(req);
			}
		}
	}

	public static Integer find_position(Integer key,
			TreeMap<Integer, ?> search_in) {

		return search_in.headMap(key).size();

	}

}
