package IdAndShardResolver;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Permutations {
	private Permutations() {
	}

	public static <T> List<T[]> get(Class<T> itemClass, T... itemsPool) {
		return get(itemsPool.length, itemClass, itemsPool);
	}

	public static <T> List<T[]> get(int size, Class<T> itemClass,
			T... itemsPool) {
		if (size < 1) {
			return new ArrayList<T[]>();
		}

		int itemsPoolCount = itemsPool.length;

		List<T[]> permutations = new ArrayList<T[]>();
		for (int i = 0; i < Math.pow(itemsPoolCount, size); i++) {
			T[] permutation = (T[]) Array.newInstance(itemClass, size);
			for (int j = 0; j < size; j++) {
				// Pick the appropriate item from the item pool given j and i
				int itemPoolIndex = (int) Math.floor((double) (i % (int) Math
						.pow(itemsPoolCount, j + 1))
						/ (int) Math.pow(itemsPoolCount, j));
				permutation[j] = itemsPool[itemPoolIndex];
			}
			permutations.add(permutation);
		}

		return permutations;
	}

	/*******
	 * first uses the permutations class to generate the permutations. then
	 * takes each array generated and builds the string that we used to actually
	 * generate the "put" for the permuttions map. the subindicator id is the
	 * counter of hte array, and the subindicator name is got by calling
	 * {@link #get_permutation_subindicator_name_from_pattern(String, String)}
	 * it returns the series of "puts" taht we used to make the permutation map.
	 * 
	 * 
	 * @param first_n
	 * @return
	 */

	public static final Integer permutation_max_length = 5;

	public static String get_patterns_permutations_map() {

		ArrayList<Integer[]> all_arrays = new ArrayList<Integer[]>();

		for (int i = 0; i < permutation_max_length; i++) {
			all_arrays.addAll(Permutations.get(i + 1, Integer.class, 1, 2));
		}

		all_arrays = new ArrayList<Integer[]>(all_arrays.subList(2,
				all_arrays.size()));
		
		
		//append rises and falls 6,7,8,9,10
		for(int i=6; i <=10; i++){
			Integer[] rise = new Integer[i];
			Integer[] fall = new Integer[i];
			for(int j=0; j < i; j++){
				rise[j] = 1;
				fall[j] = 2;
			}
			all_arrays.add(rise);
			all_arrays.add(fall);
		}

		StringBuilder put_builder = new StringBuilder();
		for (int i = 0; i < all_arrays.size(); i++) {
			StringBuilder sb = new StringBuilder();
			Integer[] arr = all_arrays.get(i);
			for (int k = 0; k < arr.length; k++) {
				sb.append(arr[k]);
			}

			String subindicator_name = "shows "
					+ IdAndShardResolver
							.get_permutation_subindicator_name_from_pattern(
									sb.toString(), "");

			Integer[] uniq_arr = new HashSet<Integer>(Arrays.asList(arr))
					.toArray(new Integer[0]);

			String subindicator_alias_name = null;
			// second condition is because we dont want rises for 1 days or
			// falls for 1 days.

			if (uniq_arr.length == 1) {
				// now you can also have a subindicator alias.
				String rises_or_falls = null;
				if (arr[0] == 1) {
					rises_or_falls = "rises";
					subindicator_alias_name = rises_or_falls + " for "
							+ arr.length + " days";
				} else {
					rises_or_falls = "falls";
					subindicator_alias_name = rises_or_falls + " for "
							+ arr.length + " days";
				}
			}

			if (subindicator_alias_name != null) {
				put_builder.append("put(\"" + sb.toString()
						+ "\",new String[]{\"" + String.valueOf(i) + "\",\""
						+ subindicator_name + "\",\"" + subindicator_alias_name
						+ "\"});");
			} else {
				put_builder.append("put(\"" + sb.toString()
						+ "\",new String[]{\"" + String.valueOf(i) + "\",\""
						+ subindicator_name + "\"});");
			}
			put_builder.append("NEW_LINE");

		}

		return put_builder.toString();

	}

	public static void main(String[] args) {
		System.out.println(get_patterns_permutations_map());
	}
}