package IdAndShardResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import org.javatuples.Pair;

public abstract class RiseFallConstants {

	public static final TreeMap<Integer, TreeMap<Integer, Pair<String, ArrayList<String>>>> rise_and_fall_amounts = new TreeMap<Integer, TreeMap<Integer, Pair<String, ArrayList<String>>>>() {
		{

			put(1, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 0.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(5,
							new Pair<String, ArrayList<String>>("0.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(10, new Pair<String, ArrayList<String>>("1.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(20, new Pair<String, ArrayList<String>>("2.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(25,
							new Pair<String, ArrayList<String>>("2.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("large"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("very large"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("huge"))));
					put(45, new Pair<String, ArrayList<String>>("4.5 percent",
							new ArrayList<String>(Arrays.asList("mammoth"))));
					put(46, new Pair<String, ArrayList<String>>(
							"more than 4.5 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(450, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(55, new Pair<String, ArrayList<String>>("5.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(70,
							new Pair<String, ArrayList<String>>("7.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(85,
							new Pair<String, ArrayList<String>>("8.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(145,
							new Pair<String, ArrayList<String>>("14.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(170,
							new Pair<String, ArrayList<String>>("17.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(195,
							new Pair<String, ArrayList<String>>("19.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(240,
							new Pair<String, ArrayList<String>>("24.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(280,
							new Pair<String, ArrayList<String>>("28.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(320,
							new Pair<String, ArrayList<String>>("32.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(360,
							new Pair<String, ArrayList<String>>(
									"36.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(420,
							new Pair<String, ArrayList<String>>("42.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(421, new Pair<String, ArrayList<String>>(
							"more than 42.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(2, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 0.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(5,
							new Pair<String, ArrayList<String>>("0.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(10, new Pair<String, ArrayList<String>>("1.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(15, new Pair<String, ArrayList<String>>("1.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(25,
							new Pair<String, ArrayList<String>>("2.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(40,
							new Pair<String, ArrayList<String>>("4.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(45, new Pair<String, ArrayList<String>>("4.5 percent",
							new ArrayList<String>(Arrays.asList("large"))));
					put(50, new Pair<String, ArrayList<String>>("5.0 percent",
							new ArrayList<String>(Arrays.asList("very large"))));
					put(55, new Pair<String, ArrayList<String>>("5.5 percent",
							new ArrayList<String>(Arrays.asList("huge"))));
					put(60, new Pair<String, ArrayList<String>>("6.0 percent",
							new ArrayList<String>(Arrays.asList("mammoth"))));
					put(61, new Pair<String, ArrayList<String>>(
							"more than 6.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(3, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(10,
							new Pair<String, ArrayList<String>>("1.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(15, new Pair<String, ArrayList<String>>("1.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(20, new Pair<String, ArrayList<String>>("2.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(25,
							new Pair<String, ArrayList<String>>("2.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(30,
							new Pair<String, ArrayList<String>>("3.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(45, new Pair<String, ArrayList<String>>("4.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(55,
							new Pair<String, ArrayList<String>>("5.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(65,
							new Pair<String, ArrayList<String>>("6.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(75, new Pair<String, ArrayList<String>>("7.5 percent",
							new ArrayList<String>(Arrays.asList("large"))));
					put(85, new Pair<String, ArrayList<String>>("8.5 percent",
							new ArrayList<String>(Arrays.asList("very large"))));
					put(90, new Pair<String, ArrayList<String>>("9.0 percent",
							new ArrayList<String>(Arrays.asList("huge"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(106, new Pair<String, ArrayList<String>>(
							"more than 10.5 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(7, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(10,
							new Pair<String, ArrayList<String>>("1.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(20, new Pair<String, ArrayList<String>>("2.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(40,
							new Pair<String, ArrayList<String>>("4.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(45,
							new Pair<String, ArrayList<String>>("4.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(55, new Pair<String, ArrayList<String>>("5.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(60, new Pair<String, ArrayList<String>>("6.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(65,
							new Pair<String, ArrayList<String>>("6.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(75,
							new Pair<String, ArrayList<String>>("7.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(85, new Pair<String, ArrayList<String>>("8.5 percent",
							new ArrayList<String>(Arrays.asList("large"))));
					put(100,
							new Pair<String, ArrayList<String>>("10.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(110,
							new Pair<String, ArrayList<String>>("11.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(130,
							new Pair<String, ArrayList<String>>(
									"13.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(150,
							new Pair<String, ArrayList<String>>(
									"15.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(170,
							new Pair<String, ArrayList<String>>("17.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(171, new Pair<String, ArrayList<String>>(
							"more than 17.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(9, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(10,
							new Pair<String, ArrayList<String>>("1.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(20, new Pair<String, ArrayList<String>>("2.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(40,
							new Pair<String, ArrayList<String>>("4.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(45,
							new Pair<String, ArrayList<String>>("4.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(55, new Pair<String, ArrayList<String>>("5.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(65, new Pair<String, ArrayList<String>>("6.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(95,
							new Pair<String, ArrayList<String>>("9.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(150,
							new Pair<String, ArrayList<String>>(
									"15.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(170,
							new Pair<String, ArrayList<String>>("17.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(171, new Pair<String, ArrayList<String>>(
							"more than 17.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(42, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(55,
							new Pair<String, ArrayList<String>>("5.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(65,
							new Pair<String, ArrayList<String>>("6.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(80, new Pair<String, ArrayList<String>>("8.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(95, new Pair<String, ArrayList<String>>("9.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(115,
							new Pair<String, ArrayList<String>>("11.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(135,
							new Pair<String, ArrayList<String>>("13.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(155,
							new Pair<String, ArrayList<String>>("15.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(175,
							new Pair<String, ArrayList<String>>(
									"17.5 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(190,
							new Pair<String, ArrayList<String>>("19.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(191, new Pair<String, ArrayList<String>>(
							"more than 19.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(365, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(45, new Pair<String, ArrayList<String>>("4.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(60,
							new Pair<String, ArrayList<String>>("6.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(75,
							new Pair<String, ArrayList<String>>("7.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(90, new Pair<String, ArrayList<String>>("9.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(110,
							new Pair<String, ArrayList<String>>("11.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(135,
							new Pair<String, ArrayList<String>>("13.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(155,
							new Pair<String, ArrayList<String>>("15.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(180,
							new Pair<String, ArrayList<String>>("18.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(200,
							new Pair<String, ArrayList<String>>(
									"20.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(220,
							new Pair<String, ArrayList<String>>("22.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(221, new Pair<String, ArrayList<String>>(
							"more than 22.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(14, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(25, new Pair<String, ArrayList<String>>("2.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(45,
							new Pair<String, ArrayList<String>>("4.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(55,
							new Pair<String, ArrayList<String>>("5.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(70, new Pair<String, ArrayList<String>>("7.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(75, new Pair<String, ArrayList<String>>("7.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(90,
							new Pair<String, ArrayList<String>>("9.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(135,
							new Pair<String, ArrayList<String>>("13.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(150,
							new Pair<String, ArrayList<String>>("15.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(175,
							new Pair<String, ArrayList<String>>(
									"17.5 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(200,
							new Pair<String, ArrayList<String>>("20.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(201, new Pair<String, ArrayList<String>>(
							"more than 20.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(270, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(60, new Pair<String, ArrayList<String>>("6.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(100,
							new Pair<String, ArrayList<String>>("10.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(140,
							new Pair<String, ArrayList<String>>("14.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(145,
							new Pair<String, ArrayList<String>>("14.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(175,
							new Pair<String, ArrayList<String>>("17.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(205,
							new Pair<String, ArrayList<String>>("20.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(235,
							new Pair<String, ArrayList<String>>("23.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(265,
							new Pair<String, ArrayList<String>>("26.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(290,
							new Pair<String, ArrayList<String>>("29.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(340,
							new Pair<String, ArrayList<String>>(
									"34.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(385,
							new Pair<String, ArrayList<String>>("38.5 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(386, new Pair<String, ArrayList<String>>(
							"more than 38.5 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(180, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(25,
							new Pair<String, ArrayList<String>>("2.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(50, new Pair<String, ArrayList<String>>("5.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(70, new Pair<String, ArrayList<String>>("7.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(95,
							new Pair<String, ArrayList<String>>("9.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(115,
							new Pair<String, ArrayList<String>>("11.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(140,
							new Pair<String, ArrayList<String>>("14.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(160,
							new Pair<String, ArrayList<String>>("16.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(165,
							new Pair<String, ArrayList<String>>("16.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(195,
							new Pair<String, ArrayList<String>>("19.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(225,
							new Pair<String, ArrayList<String>>("22.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(260,
							new Pair<String, ArrayList<String>>("26.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(290,
							new Pair<String, ArrayList<String>>("29.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(320,
							new Pair<String, ArrayList<String>>("32.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(325,
							new Pair<String, ArrayList<String>>(
									"32.5 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(380,
							new Pair<String, ArrayList<String>>(
									"38.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(435,
							new Pair<String, ArrayList<String>>("43.5 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(436, new Pair<String, ArrayList<String>>(
							"more than 43.5 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(21, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(25, new Pair<String, ArrayList<String>>("2.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(50,
							new Pair<String, ArrayList<String>>("5.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(60,
							new Pair<String, ArrayList<String>>("6.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(75, new Pair<String, ArrayList<String>>("7.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(85, new Pair<String, ArrayList<String>>("8.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(140,
							new Pair<String, ArrayList<String>>("14.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(155,
							new Pair<String, ArrayList<String>>("15.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(170,
							new Pair<String, ArrayList<String>>(
									"17.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(200,
							new Pair<String, ArrayList<String>>("20.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(201, new Pair<String, ArrayList<String>>(
							"more than 20.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(630, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(25, new Pair<String, ArrayList<String>>("2.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(45,
							new Pair<String, ArrayList<String>>("4.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(55,
							new Pair<String, ArrayList<String>>("5.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(70, new Pair<String, ArrayList<String>>("7.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(75, new Pair<String, ArrayList<String>>("7.5 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(90,
							new Pair<String, ArrayList<String>>("9.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(135,
							new Pair<String, ArrayList<String>>("13.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(150,
							new Pair<String, ArrayList<String>>(
									"15.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(200,
							new Pair<String, ArrayList<String>>("20.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(201, new Pair<String, ArrayList<String>>(
							"more than 20.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(730, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(60, new Pair<String, ArrayList<String>>("6.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(95,
							new Pair<String, ArrayList<String>>("9.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(115,
							new Pair<String, ArrayList<String>>("11.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(135,
							new Pair<String, ArrayList<String>>("13.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(165,
							new Pair<String, ArrayList<String>>("16.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(190,
							new Pair<String, ArrayList<String>>("19.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(220,
							new Pair<String, ArrayList<String>>("22.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(245,
							new Pair<String, ArrayList<String>>("24.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(270,
							new Pair<String, ArrayList<String>>("27.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(280,
							new Pair<String, ArrayList<String>>("28.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(325,
							new Pair<String, ArrayList<String>>(
									"32.5 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(370,
							new Pair<String, ArrayList<String>>("37.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(371, new Pair<String, ArrayList<String>>(
							"more than 37.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(90, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(60, new Pair<String, ArrayList<String>>("6.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(80,
							new Pair<String, ArrayList<String>>("8.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(100,
							new Pair<String, ArrayList<String>>("10.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(140,
							new Pair<String, ArrayList<String>>("14.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(145,
							new Pair<String, ArrayList<String>>("14.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(175,
							new Pair<String, ArrayList<String>>("17.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(205,
							new Pair<String, ArrayList<String>>("20.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(235,
							new Pair<String, ArrayList<String>>("23.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(265,
							new Pair<String, ArrayList<String>>(
									"26.5 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(290,
							new Pair<String, ArrayList<String>>("29.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(291, new Pair<String, ArrayList<String>>(
							"more than 29.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(28, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 1.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(15,
							new Pair<String, ArrayList<String>>("1.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(30, new Pair<String, ArrayList<String>>("3.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(40, new Pair<String, ArrayList<String>>("4.0 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(55,
							new Pair<String, ArrayList<String>>("5.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(65,
							new Pair<String, ArrayList<String>>("6.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(80, new Pair<String, ArrayList<String>>("8.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(90, new Pair<String, ArrayList<String>>("9.0 percent",
							new ArrayList<String>(Arrays.asList("average"))));
					put(95,
							new Pair<String, ArrayList<String>>("9.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(110,
							new Pair<String, ArrayList<String>>("11.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(130,
							new Pair<String, ArrayList<String>>("13.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(145,
							new Pair<String, ArrayList<String>>("14.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(165,
							new Pair<String, ArrayList<String>>("16.5 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(180,
							new Pair<String, ArrayList<String>>("18.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(190,
							new Pair<String, ArrayList<String>>(
									"19.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(220,
							new Pair<String, ArrayList<String>>("22.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(221, new Pair<String, ArrayList<String>>(
							"more than 22.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(540, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.5 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(25,
							new Pair<String, ArrayList<String>>("2.5 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(45, new Pair<String, ArrayList<String>>("4.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(65, new Pair<String, ArrayList<String>>("6.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(85,
							new Pair<String, ArrayList<String>>("8.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(130,
							new Pair<String, ArrayList<String>>("13.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(150,
							new Pair<String, ArrayList<String>>("15.0 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(180,
							new Pair<String, ArrayList<String>>("18.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(210,
							new Pair<String, ArrayList<String>>("21.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(240,
							new Pair<String, ArrayList<String>>("24.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(270,
							new Pair<String, ArrayList<String>>("27.0 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(300,
							new Pair<String, ArrayList<String>>("30.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(350,
							new Pair<String, ArrayList<String>>("35.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(400,
							new Pair<String, ArrayList<String>>(
									"40.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(450,
							new Pair<String, ArrayList<String>>("45.0 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(451, new Pair<String, ArrayList<String>>(
							"more than 45.0 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});
			put(60, new TreeMap<Integer, Pair<String, ArrayList<String>>>() {
				{
					put(0, new Pair<String, ArrayList<String>>(
							"less than 2.0 percent", new ArrayList<String>(
									Arrays.asList("infinitesimally small"))));
					put(20,
							new Pair<String, ArrayList<String>>("2.0 percent",
									new ArrayList<String>(Arrays
											.asList("infinitesimally small"))));
					put(35, new Pair<String, ArrayList<String>>("3.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(55, new Pair<String, ArrayList<String>>("5.5 percent",
							new ArrayList<String>(Arrays.asList("very small"))));
					put(70,
							new Pair<String, ArrayList<String>>("7.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(85,
							new Pair<String, ArrayList<String>>("8.5 percent",
									new ArrayList<String>(Arrays
											.asList("moderately small"))));
					put(105,
							new Pair<String, ArrayList<String>>("10.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(115,
							new Pair<String, ArrayList<String>>("11.5 percent",
									new ArrayList<String>(Arrays
											.asList("average"))));
					put(120,
							new Pair<String, ArrayList<String>>("12.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(140,
							new Pair<String, ArrayList<String>>("14.0 percent",
									new ArrayList<String>(Arrays
											.asList("moderately large"))));
					put(165,
							new Pair<String, ArrayList<String>>("16.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(185,
							new Pair<String, ArrayList<String>>("18.5 percent",
									new ArrayList<String>(Arrays
											.asList("large"))));
					put(210,
							new Pair<String, ArrayList<String>>("21.0 percent",
									new ArrayList<String>(Arrays
											.asList("very large"))));
					put(230,
							new Pair<String, ArrayList<String>>(
									"23.0 percent",
									new ArrayList<String>(Arrays.asList("huge"))));
					put(235,
							new Pair<String, ArrayList<String>>("23.5 percent",
									new ArrayList<String>(Arrays
											.asList("mammoth"))));
					put(236, new Pair<String, ArrayList<String>>(
							"more than 23.5 percent", new ArrayList<String>(
									Arrays.asList("humongous"))));
				}
			});

		}
	};

}
