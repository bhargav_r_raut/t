package ImageScraper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import Titan.ReadWriteTextToFile;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import static jodd.jerry.Jerry.jerry;

public class ImageScraper {

	public ImageScraper() throws Exception{
		super();
		generateJsonString();
	}

	public String generateJsonString() throws Exception {
		String name_age_url = null;
		final JSONObject object = new JSONObject();
		String url = "http://www.wallpaperup.com/tags/show/cosplay";
		System.out.println("came inside");
		ReadWriteTextToFile rwt = ReadWriteTextToFile.getInstance();
		String pageText = rwt.getTextFromUrl(url);
		Jerry doc = jerry(pageText);
		doc.$(".prw").each(new JerryFunction() {
			
			@Override
			public boolean onNode(Jerry $this, int index) {
				final Integer dog = index;
				final HashMap<String,String> jsonmap = new HashMap<String, String>();
				$this.$("img").each(new JerryFunction() {
					@Override
					public boolean onNode(Jerry $innerthis, int innerindex) {
						// TODO Auto-generated method stub
						
						jsonmap.put("description",$innerthis.attr("alt"));
						jsonmap.put("image_url", $innerthis.attr("src"));
						System.out.println(jsonmap.toString());
						try {
							object.put(String.valueOf(dog), (Map)jsonmap);	
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//System.out.println($innerthis.attr("src"));
						return true;
					}
				});
				return true;
			}
		});
		
		name_age_url = object.toString();
		System.out.println(name_age_url);
		rwt.getInstance().writeFile("jsonString.json", name_age_url);
		return name_age_url;
	}

}
