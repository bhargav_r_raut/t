import java.util.ArrayList;
import java.util.HashMap;


public class Indicator_SubIndicator_Stock_Complex {

	public ArrayList<Integer> periods;
	public String complex_name;
	public ArrayList<HashMap<String,Object>> day_ids;
	
	public Indicator_SubIndicator_Stock_Complex(ArrayList<Integer> periods, 
			String complex_name){
		this.periods = periods;
		this.complex_name = complex_name;
	}

	/**
	 * @param complex_name
	 * @param day_ids
	 */
	public Indicator_SubIndicator_Stock_Complex(String complex_name,
			ArrayList<HashMap<String, Object>> day_ids) {
		super();
		this.complex_name = complex_name;
		this.day_ids = day_ids;
	}
	
	
}
