package Indicators;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import DayIdMapGenerator.DayIdBuilder;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.helpers.HighestValueIndicator;
import eu.verdelhan.ta4j.indicators.helpers.LowestValueIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.AroonDownIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.AroonUpIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.AwesomeOscillatorIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.CCIIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.StochasticOscillatorDIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.StochasticOscillatorKIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.MaxPriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.MedianPriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.MinPriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.OpenPriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.VolumeIndicator;
import eu.verdelhan.ta4j.indicators.trackers.AccelerationDecelerationIndicator;
import eu.verdelhan.ta4j.indicators.trackers.AverageDirectionalMovementIndicator;
import eu.verdelhan.ta4j.indicators.trackers.DoubleEMAIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import eu.verdelhan.ta4j.indicators.trackers.MACDIndicator;
import eu.verdelhan.ta4j.indicators.trackers.RSIIndicator;
import eu.verdelhan.ta4j.indicators.trackers.TripleEMAIndicator;
import eu.verdelhan.ta4j.indicators.trackers.WilliamsRIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class CalculateIndicators {

	/*****************************************************
	 * GETTERS AND SETTERS START HERE.
	 ****************************************************/
	private static final String identifier_for_log = "calculate_indicators";
	private ClosePriceIndicator close_price_indicator;
	private OpenPriceIndicator open_price_indicator;
	private VolumeIndicator volume_indicator;
	private MaxPriceIndicator high_price_indicator;
	private MinPriceIndicator low_price_indicator;
	private HashMap<String, List<Integer>> periods_for_indicator_calculations;

	public HashMap<String, List<Integer>> getPeriods_for_indicator_calculations() {
		return periods_for_indicator_calculations;
	}

	public void setPeriods_for_indicator_calculations(
			HashMap<String, List<Integer>> periods_for_indicator_calculations) {
		this.periods_for_indicator_calculations = periods_for_indicator_calculations;
	}

	public MaxPriceIndicator getHigh_price_indicator() {
		if (high_price_indicator == null) {
			setHigh_price_indicator();
		}
		return high_price_indicator;
	}

	public void setHigh_price_indicator() {
		this.high_price_indicator = new MaxPriceIndicator(getTs());
	}

	public MinPriceIndicator getLow_price_indicator() {
		if (low_price_indicator == null) {
			setLow_price_indicator();
		}
		return low_price_indicator;
	}

	public void setLow_price_indicator() {
		this.low_price_indicator = new MinPriceIndicator(getTs());
	}

	public ClosePriceIndicator getClose_price_indicator() {
		if (close_price_indicator == null) {
			setClose_price_indicator();
		}
		return close_price_indicator;
	}

	public void setClose_price_indicator() {
		this.close_price_indicator = new ClosePriceIndicator(getTs());
	}

	public OpenPriceIndicator getOpen_price_indicator() {
		if (open_price_indicator == null) {
			setOpen_price_indicator();
		}
		return open_price_indicator;
	}

	public void setOpen_price_indicator() {

		this.open_price_indicator = new OpenPriceIndicator(getTs());
	}

	public VolumeIndicator getVolume_indicator() {
		if (volume_indicator == null) {
			setVolume_indicator();
		}
		return volume_indicator;
	}

	public void setVolume_indicator() {
		this.volume_indicator = new VolumeIndicator(getTs());
	}

	private String indicator_name;

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	private Entity e;

	public Entity getE() {
		return e;
	}

	public void setE(Entity e) {
		this.e = e;
	}

	public TimeSeries getTs() {
		return getE().getTs();
	}

	/******************************************************
	 * GETTERS AND SETTERS END.
	 * ****************************************************
	 */

	public ArrayList<String> get_indicator_ohlcv_group(String indicator_name) throws JSONException {

		ArrayList<String> ohlcv_groups = new ArrayList<String>();

		JSONArray ohlcv_groups_for_this_indicator = CentralSingleton.getInstance().indicators_jsonfile
				.getJSONObject(indicator_name).getJSONArray(StockConstants.indicator_ohlcv_groups_key_name);

		for (int i = 0; i < ohlcv_groups_for_this_indicator.length(); i++) {
			ohlcv_groups.add(ohlcv_groups_for_this_indicator.getString(i));
		}

		return ohlcv_groups;

	}

	/***
	 * basically returns a map of the following:
	 * 
	 * key -> indicator_cum_period_name value -> periods as integers array.
	 * 
	 * at the same time also populates the entity initial stub names
	 * 
	 * key -> bare indicator name value -> arraylist string of
	 * indicator_period_names
	 * 
	 * 
	 * the indicator information for both the above is got from the
	 * centralsingleton indicator hashmap. that is generated and built on
	 * centralsingleton initialization.
	 * 
	 * This method is called
	 * 
	 * @return
	 */
	public HashMap<String, List<Integer>> periods_for_indicator_calculation() {

		List<Indicator> indicator_list = CentralSingleton.indicators_hashmap.get(indicator_name);
		HashMap<String, List<Integer>> actual_stub_names_and_periods = new HashMap<String, List<Integer>>();
		for (Indicator i : indicator_list) {

			getE().add_indicator_cum_period_name_to_initial_stub_names(indicator_name,
					i.getIndicator_and_period_name());

			actual_stub_names_and_periods.put(i.getIndicator_and_period_name(), i.getPeriods());
		}
		setPeriods_for_indicator_calculations(actual_stub_names_and_periods);
		return actual_stub_names_and_periods;
	}

	/**
	 * 
	 * @param indicator_name
	 * @param stockName
	 * @param stockFullName
	 * @param indice
	 * @param start_and_end_day_id
	 */
	public CalculateIndicators(Entity e) {

		this.e = e;

	}

	/****
	 * HOW INDICATORS ARE CALCULATED?
	 * 
	 * 
	 * the timeseries is assembled in the entity in before_calculate_indicators
	 * hook. this timeseries is just a reflection of the price map internal.
	 * 
	 * Individual ticks are datapoints in the price map.
	 * 
	 * While calculating any indicator on a day id we check the following:
	 * 
	 * if the number of numerical days between that day and the first day id in
	 * the price map internal is greater than or equal to the maximum required
	 * period for calculating that indicator. If yes, and if the day id exists
	 * in the price map internal then and only then is the indicator calculated,
	 * and added to the datapoint provided that the calculated indicator value
	 * is not NaN
	 * 
	 * 
	 * there are some issues with aroon up,down and RSI indicator , which give
	 * values which are not 'NaN' even when calculating the indicator on a
	 * totally null Timeseries.
	 * 
	 * for the moment there are valid points to ignore this such as:
	 * 
	 * 1. we have already calculated the ohlcv groups of the entities, and know
	 * for certain that the data we get from quandl will have the required ohlcv
	 * for the required group. 2. we only start indicator calculation from the
	 * first_downloaded_Datapoint i.e the first datapoint with a price. 3. as a
	 * result for any indicator that needs a period > 1, when it is calculated,
	 * say it needs a period of 2, then it will be calculated on day 2, and by
	 * that point , there will be at least one value (for certain before it
	 * which has all the price information necessary for its calculation,
	 * because we only start calculating from the first downloaded datapoint
	 * onwards), so whatever value is calculated will have at least one other
	 * value before it, so the indicator calculation will not be complete
	 * bullshit. In short we will never have a situation where an indicator is
	 * calculated using just one tick(for any indicator that has a period > 1) I
	 * believe for the moment this is enough to proceed.
	 * 
	 * ----- why it was decided to build the timeseries from the price map
	 * internal and not leave NaN values anywhere in the middle?
	 * 
	 * For eg in case of SMA. In case there is a NaN value in the close price
	 * anywhere, then it gives NaN for SMA for any day after that day (within
	 * the SMA Period) so basically we cant allow NaN values inside the
	 * timeseries, it returns NaN as the indicator value for days where the
	 * close is NaN(former) as well as other days(latter). The former is
	 * desirable but not the latter. SO above while creating the timeseries, we
	 * only add ticks which acutally have a datapoint, basically a reflection of
	 * the price_map_internal.
	 * 
	 * 
	 * 
	 * 
	 * @param indicator
	 * @param initial_stub_name
	 */
	public void generate_and_set_indicator_values(eu.verdelhan.ta4j.Indicator<? extends Decimal> indicator,
			String initial_stub_name) {

		Integer max_period_required_for_calculating_this_indicator = Collections
				.max(getPeriods_for_indicator_calculations().get(initial_stub_name));

		Integer first_day_id = getE().getPrice_map_internal().firstKey();

		for (int i = getTs().getBegin(); i < getTs().getEnd() + 1; i++) {

			ZonedDateTime d = getTs().getTick(i).getEndTime();

			Integer day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id.get(DayIdBuilder.zonedatetime_to_datestring(d));
			
			Integer period = getE().period_for_indicator_calculation(day_id);
			
			if (period >= (max_period_required_for_calculating_this_indicator)) {
				// if this is 1 and the other is 0, then how many periods
				// have happened?
				// so we have to add 
				// 1 to this.
				if (getE().getPrice_map_internal().containsKey(day_id)) {
					if (indicator.getValue(i).toString() != "NaN") {
						dataPointIndicator dpi = new dataPointIndicator(indicator.getValue(i).toString(),
								initial_stub_name);

						getE().getPrice_map_internal().get(day_id).getIndicator_values_on_datapoint()
								.put(initial_stub_name, dpi);
					}
				}

			}
		}

	}

	/**
	 * 
	 * Flow of events:
	 * 
	 * 1. get the hashmap of indicator_cum_period_name => [periods]
	 * #periods_for_indicator_calculation() simultaneously inside that def
	 * populate the entity initial stub names
	 * 
	 * 
	 * iterate it. depending on the indicator name, case switch
	 * 
	 * 2. call generate_and_set_indicator values set the calculated indicators
	 * value on datapoints inside the entity.
	 * 
	 * 
	 * over.
	 * 
	 * 
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws JSONException
	 * 
	 * 
	 */
	public void calculate_required_indicator()
			throws JsonParseException, JsonMappingException, IOException, JSONException {

		ArrayList<String> indicator_ohlcv_groups = get_indicator_ohlcv_group(indicator_name);
		eu.verdelhan.ta4j.Indicator<Double> ta_indicator = null;

		/****
		 * key : indicator_name + "_"..period_information. value : arraylist
		 * integer periods for this indicator calculation.
		 */
		for (Map.Entry<String, List<Integer>> entry : periods_for_indicator_calculation().entrySet()) {

			// for each of the period params.
			if (indicator_ohlcv_groups.contains("c1")) {

				switch (indicator_name) {

				case "acceleration_deceleration_indicator":
					//System.out.println("came to calculate acceleration decelartion.");
					AccelerationDecelerationIndicator adi = new AccelerationDecelerationIndicator(getTs(),
							entry.getValue().get(0), entry.getValue().get(1));
					/****
					MedianPriceIndicator mdi = new MedianPriceIndicator(getTs());
					
					// gonna have to debug this crap here.
					 AwesomeOscillatorIndicator awo = 
					 new AwesomeOscillatorIndicator(mdi,
							 entry.getValue().get(0), entry.getValue().get(1));
					
					 SMAIndicator sma = new SMAIndicator(awo, entry.getValue().get(0));
					 
					 for (int i = getTs().getBegin(); i < getTs().getEnd() + 1; i++){
						 System.out.println("i :" + i);
						 System.out.println("median price indicator:" + mdi.getValue(i));
						 System.out.println("awesome:" + awo.getValue(i));
						 System.out.println("sma indicator:" + sma.getValue(i));
					 }
					 System.exit(1);
					 **/
					
					generate_and_set_indicator_values(adi, entry.getKey());
					break;

				case "aroon_up":
					AroonUpIndicator aroonUpIndicator = new AroonUpIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonUpIndicator, entry.getKey());
					break;

				case "aroon_down":
					AroonDownIndicator aroonDownIndicator = new AroonDownIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonDownIndicator, entry.getKey());
					break;

				case "moving_average_convergence_divergence":
					MACDIndicator macd_indicator = new MACDIndicator(getClose_price_indicator(),
							entry.getValue().get(0), entry.getValue().get(1));
					generate_and_set_indicator_values(macd_indicator, entry.getKey());
					break;

				case "relative_strength_indicator":
					RSIIndicator rsi_indicator = new RSIIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(rsi_indicator, entry.getKey());
					break;

				case "single_ema_indicator":
					EMAIndicator ema_indicator = new EMAIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(ema_indicator, entry.getKey());
					break;

				case "double_ema_indicator":
					DoubleEMAIndicator double_ema_indicator = new DoubleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(double_ema_indicator, entry.getKey());
					break;

				case "triple_ema_indicator":
					TripleEMAIndicator triple_ema_indicator = new TripleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(triple_ema_indicator, entry.getKey());
					break;

				case "awesome_oscillator_indicator":
					AwesomeOscillatorIndicator awe = new AwesomeOscillatorIndicator(new MedianPriceIndicator(getTs()));
					generate_and_set_indicator_values(awe, entry.getKey());
					break;

				case "close":
					ClosePriceIndicator close_price_indicator_calculated = getClose_price_indicator();
					generate_and_set_indicator_values(close_price_indicator_calculated, entry.getKey());
					break;

				default:

					break;
				}
			} else if (indicator_ohlcv_groups.contains("c2")) {

				switch (indicator_name) {

				case "relative_strength_indicator":
					RSIIndicator rsi_indicator = new RSIIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(rsi_indicator, entry.getKey());
					break;

				case "acceleration_deceleration_indicator":
					System.out.println("came to calculate acceleration decelartion.");
					AccelerationDecelerationIndicator adi = new AccelerationDecelerationIndicator(getTs(),
							entry.getValue().get(0), entry.getValue().get(1));
					generate_and_set_indicator_values(adi, entry.getKey());
					break;

				case "aroon_up":
					AroonUpIndicator aroonUpIndicator = new AroonUpIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonUpIndicator, entry.getKey());
					break;

				case "aroon_down":
					AroonDownIndicator aroonDownIndicator = new AroonDownIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonDownIndicator, entry.getKey());
					break;

				case "moving_average_convergence_divergence":
					MACDIndicator macd_indicator = new MACDIndicator(getClose_price_indicator(),
							entry.getValue().get(0), entry.getValue().get(1));
					generate_and_set_indicator_values(macd_indicator, entry.getKey());
					break;

				case "cci_indicator":
					CCIIndicator cci_indicator = new CCIIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(cci_indicator, entry.getKey());
					break;

				case "stochastic_oscillator_k_indicator":
					StochasticOscillatorKIndicator stochastick = new StochasticOscillatorKIndicator(
							new ClosePriceIndicator(getTs()), entry.getValue().get(0), new MaxPriceIndicator(getTs()),
							new MinPriceIndicator(getTs()));
					generate_and_set_indicator_values(stochastick, entry.getKey());
					break;

				case "stochastic_oscillator_d_indicator":

					StochasticOscillatorKIndicator stochastic_k = new StochasticOscillatorKIndicator(
							new ClosePriceIndicator(getTs()), entry.getValue().get(0), new MaxPriceIndicator(getTs()),
							new MinPriceIndicator(getTs()));
					StochasticOscillatorDIndicator stochastic_d = new StochasticOscillatorDIndicator(stochastic_k);
					// debug_stochastic(getTs());
					generate_and_set_indicator_values(stochastic_d, entry.getKey());
					break;

				case "average_directional_movement_indicator":
					AverageDirectionalMovementIndicator avdm = new AverageDirectionalMovementIndicator(getTs(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(avdm, entry.getKey());
					break;

				case "awesome_oscillator_indicator":
					// AwesomeOscillatorIndicator a
					AwesomeOscillatorIndicator awe = new AwesomeOscillatorIndicator(new MedianPriceIndicator(getTs()));
					generate_and_set_indicator_values(awe, entry.getKey());
					break;

				case "williams_r_indicator":
					WilliamsRIndicator wri = new WilliamsRIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(wri, entry.getKey());
					break;

				case "open":
					// by this time the open will already have been caculated
					OpenPriceIndicator open_price_indicator_calculated = getOpen_price_indicator();
					generate_and_set_indicator_values(open_price_indicator_calculated, entry.getKey());
					break;

				case "high":
					// needs to be calculated.
					// System.out.println("came to do high.");

					MaxPriceIndicator high_price_indicator_calculated = getHigh_price_indicator();
					generate_and_set_indicator_values(high_price_indicator_calculated, entry.getKey());
					break;

				case "low":

					MinPriceIndicator low_price_indicator_calculated = getLow_price_indicator();
					generate_and_set_indicator_values(low_price_indicator_calculated, entry.getKey());
					break;

				case "close":

					ClosePriceIndicator close_price_indicator_calculated = getClose_price_indicator();
					generate_and_set_indicator_values(close_price_indicator_calculated, entry.getKey());
					break;

				case "single_ema_indicator":
					EMAIndicator ema_indicator = new EMAIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(ema_indicator, entry.getKey());
					break;

				case "double_ema_indicator":
					DoubleEMAIndicator double_ema_indicator = new DoubleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(double_ema_indicator, entry.getKey());
					break;

				case "triple_ema_indicator":
					TripleEMAIndicator triple_ema_indicator = new TripleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(triple_ema_indicator, entry.getKey());
					break;

				default:
					break;
				}

			} else if (indicator_ohlcv_groups.contains("c3")) {
				switch (indicator_name) {

				case "relative_strength_indicator":
					RSIIndicator rsi_indicator = new RSIIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(rsi_indicator, entry.getKey());
					break;

				case "acceleration_deceleration_indicator":
					System.out.println("came to calculate acceleration decelartion.");
					AccelerationDecelerationIndicator adi = new AccelerationDecelerationIndicator(getTs(),
							entry.getValue().get(0), entry.getValue().get(1));
					generate_and_set_indicator_values(adi, entry.getKey());
					break;

				case "aroon_up":
					AroonUpIndicator aroonUpIndicator = new AroonUpIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonUpIndicator, entry.getKey());
					break;

				case "aroon_down":
					AroonDownIndicator aroonDownIndicator = new AroonDownIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(aroonDownIndicator, entry.getKey());
					break;

				case "moving_average_convergence_divergence":
					MACDIndicator macd_indicator = new MACDIndicator(getClose_price_indicator(),
							entry.getValue().get(0), entry.getValue().get(1));
					generate_and_set_indicator_values(macd_indicator, entry.getKey());
					break;

				case "cci_indicator":
					CCIIndicator cci_indicator = new CCIIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(cci_indicator, entry.getKey());
					break;

				case "stochastic_oscillator_k_indicator":
					StochasticOscillatorKIndicator stochastick = new StochasticOscillatorKIndicator(
							new ClosePriceIndicator(getTs()), entry.getValue().get(0), new MaxPriceIndicator(getTs()),
							new MinPriceIndicator(getTs()));
					generate_and_set_indicator_values(stochastick, entry.getKey());
					break;

				case "stochastic_oscillator_d_indicator":

					StochasticOscillatorKIndicator stochastic_k = new StochasticOscillatorKIndicator(
							new ClosePriceIndicator(getTs()), entry.getValue().get(0), new MaxPriceIndicator(getTs()),
							new MinPriceIndicator(getTs()));
					StochasticOscillatorDIndicator stochastic_d = new StochasticOscillatorDIndicator(stochastic_k);
					// debug_stochastic(getTs());
					generate_and_set_indicator_values(stochastic_d, entry.getKey());
					break;

				case "average_directional_movement_indicator":
					AverageDirectionalMovementIndicator avdm = new AverageDirectionalMovementIndicator(getTs(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(avdm, entry.getKey());
					break;

				case "awesome_oscillator_indicator":
					// AwesomeOscillatorIndicator a
					AwesomeOscillatorIndicator awe = new AwesomeOscillatorIndicator(new MedianPriceIndicator(getTs()));
					generate_and_set_indicator_values(awe, entry.getKey());
					break;

				case "williams_r_indicator":
					WilliamsRIndicator wri = new WilliamsRIndicator(getTs(), entry.getValue().get(0));
					generate_and_set_indicator_values(wri, entry.getKey());
					break;

				case "open":
					// by this time the open will already have been caculated
					OpenPriceIndicator open_price_indicator_calculated = getOpen_price_indicator();
					generate_and_set_indicator_values(open_price_indicator_calculated, entry.getKey());
					break;

				case "high":
					// needs to be calculated.
					// System.out.println("came to do high.");

					MaxPriceIndicator high_price_indicator_calculated = getHigh_price_indicator();
					generate_and_set_indicator_values(high_price_indicator_calculated, entry.getKey());
					break;

				case "low":

					MinPriceIndicator low_price_indicator_calculated = getLow_price_indicator();
					generate_and_set_indicator_values(low_price_indicator_calculated, entry.getKey());
					break;

				case "close":

					ClosePriceIndicator close_price_indicator_calculated = getClose_price_indicator();
					generate_and_set_indicator_values(close_price_indicator_calculated, entry.getKey());
					break;

				case "single_ema_indicator":
					EMAIndicator ema_indicator = new EMAIndicator(getClose_price_indicator(), entry.getValue().get(0));
					generate_and_set_indicator_values(ema_indicator, entry.getKey());
					break;

				case "double_ema_indicator":
					DoubleEMAIndicator double_ema_indicator = new DoubleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(double_ema_indicator, entry.getKey());
					break;

				case "triple_ema_indicator":
					TripleEMAIndicator triple_ema_indicator = new TripleEMAIndicator(getClose_price_indicator(),
							entry.getValue().get(0));
					generate_and_set_indicator_values(triple_ema_indicator, entry.getKey());
					break;

				case "volume":
					VolumeIndicator vol = getVolume_indicator();
					generate_and_set_indicator_values(vol, entry.getKey());
					break;

				default:
					break;
				}
			} else if (indicator_ohlcv_groups.contains("c4")) {

				switch (indicator_name) {
				case "volume":
					VolumeIndicator vol = getVolume_indicator();
					generate_and_set_indicator_values(vol, entry.getKey());
					break;

				default:
					break;
				}

			}
		}

	}

	public static String datetime_to_datestring(DateTime dt) {
		String datestring = "";
		String year = String.valueOf(dt.getYear());
		String month = DayIdBuilder.left_pad(String.valueOf(dt.getMonthOfYear()));
		String day = DayIdBuilder.left_pad(String.valueOf(dt.getDayOfMonth()));
		datestring = year + "-" + month + "-" + day;
		return datestring;
	}

	public static String zonedatetime_to_datestring(ZonedDateTime zdt) {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(zdt);
	}

	public void debug_stochastic(TimeSeries ts) {

		Integer timeFrame = 3;

		MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(ts);
		MinPriceIndicator minPriceIndicator = new MinPriceIndicator(ts);

		HighestValueIndicator highestHigh = new HighestValueIndicator(maxPriceIndicator, timeFrame);

		LowestValueIndicator lowestMin = new LowestValueIndicator(minPriceIndicator, timeFrame);

		ClosePriceIndicator clp = new ClosePriceIndicator(ts);

		for (Integer index : getE().getPrice_map_internal().keySet()) {
			System.out.println("high price indicator is:" + maxPriceIndicator.getValue(index));
			System.out.println("low price indicator is:" + minPriceIndicator.getValue(index));
			System.out.println("day id is:" + index);
			Decimal highestHighPrice = highestHigh.getValue(index);
			Decimal lowestLowPrice = lowestMin.getValue(index);

			System.out.println("highest high is:" + highestHighPrice.toString());
			System.out.println("lowest low is:" + lowestLowPrice.toString());
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Decimal bd = clp.getValue(index).minus(lowestLowPrice).dividedBy(highestHighPrice.minus(lowestLowPrice))
					.multipliedBy(Decimal.HUNDRED);
		}

	}

	public CalculateIndicators() {

		/**
		 * ArrayList<Tick> ts_holder = new ArrayList<Tick>(); Random
		 * randomGenerator = new Random(); for (int idx = 1; idx <= 2000; ++idx)
		 * { Double randomInt = randomGenerator.nextDouble(); Tick tick = new
		 * Tick(new DateTime().minus(86400 * idx), new Double( 0), new
		 * Double(0), new Double(0), new Double(randomInt), new Double(randomInt
		 * * 1000)); ts_holder.add(tick); } TimeSeries ts = new
		 * TimeSeries(ts_holder); ClosePriceIndicator closePrice = new
		 * ClosePriceIndicator(ts);
		 **/

		// SMAIndicator indicator = new SMAIndicator(closePrice, 11);
		// AverageDirectionalMovementIndicator adm = new
		// AverageDirectionalMovementIndicator(ts, 11);

		/*
		 * for(int i=0; i< 2000; i++){ System.out.println(String.valueOf(i));
		 * System.out.println(" average directional movement " +
		 * adm.getValue(i)); System.out.println(" sma " +
		 * indicator.getValue(i));
		 * 
		 * }
		 */

		/**
		 * REQUIREMENTS:
		 * 
		 * 1.indicator crosses signal line(9 day moving average of indicator
		 * itself.) 2.indicator goes above or below a certain level. 3.indicator
		 * to price bullish and bearish divergence. 4.indicator rate of change
		 * at time of crossing the important levels. 5.number of times an
		 * indicator crosses a certain(critical) level in a given period.
		 * 6.indicator range - indicator dependence on price - ifrequired
		 * consider as percentage of price. 7.calculation of higher highs and
		 * higher lows and vice versa = smoothing ? 8.indicator is rising -
		 * define. 9.indicator is falling - define
		 * 
		 * 10.indicator is rising for x number of days, while price is?
		 * 
		 * 11.indicator zero line(difference between the indicator itself, and
		 * the signal line - zero line is always zero, the difference oscillates
		 * above and below it.)
		 * 
		 * zero line crossover happens when the macd itself becomes zero.(i.e
		 * difference between its 12 and 26 day ema becomes zero)
		 * 
		 * -so u can calculate the zero line for any indicator like this.
		 * 
		 * -oscillators which can cross above and below zero - there the point
		 * where it crosses the zero line itself is taken.
		 * 
		 * -indicators which dont cross zero, for them we cant have a zero line.
		 * -we can have the place where it crosses the signal line.
		 * 
		 */

		/**
		 * you can take the sma of any indicator to simply smooth it out. this
		 * applies well to rate of change indicator.
		 * 
		 * you should also calculate the rate of change of indicators around the
		 * time they cross relevant points, for eg like the rsi, when its
		 * crossing above or below 30 or 70
		 */

		/*
		 * ONLY CLOSE COMPARABLE WITHOUT ADDITIONAL MODIFICATIONS. periods: 25
		 * day, 50 day. AROON UP OR DOWN - NEEDS ONLY CLOSE PRICE. varies
		 * between minimum of 0 - 100. greater than fifty is bullish
		 * 
		 * CRITICAL LEVEL IS - 50 (CRITICAL LINE CROSS) less than fifty is
		 * bearish. important to analyze together, - rise in one and a fall in
		 * the other.
		 */

		/*
		 * NEEDS TO BE DIVIDED BY THE 34 DAY SMA TO BE COMAPRABLE.
		 * 
		 * awesomeoscillator indicator needs high and low prices. first
		 * calculate the average of the price for each day.(high-low/2) then use
		 * this as the time series for the sma calculation in the formula. - if
		 * this indicator crosses below zero, it may signal and upwards trend,
		 * and opposite for above zero.
		 * -https://extra.agea.com/en/education/types
		 * -of-technical-indicators/accelerator-oscillator
		 */

		/*
		 * COMPARABLE WITHOUT ADDITIONAL MODIFICATION. cci indicator requires
		 * high low and close simple to plug into the existing function, as it
		 * just needs the time series and time frame positive = favors bulls,
		 * negative favors bears. TIME FRAMES => 10, 20 ,40 DAYS
		 */

		/*
		 * ONLY CLOSECOMPARABLE WITHOUT ADDITIONAL ADJUSTMENT PPO indicator this
		 * requires only the close prices after that we have to cacluate
		 * something called the signal line, which is just the 9 day ema of the
		 * ppo indicator itself, we have to also calculate additionally where
		 * the signal line crosses the ppo line, by usibg the ta4j cross
		 * indicator.
		 * 
		 * 
		 * 
		 * when ppo goes above signal line we call it a bullish indicator.
		 * https: //www.tradingview.com/stock-charts-support/index.php/
		 * Price_Oscillator_ %28PPO%29
		 */

		/*
		 * HIGH, LOW CLOSE COMPARABLE WITHOUT ADDITIONAL MODIFICATION.
		 * STOCHASTIC OSCILLATOR K INDICATOR. take the cross of this with the
		 * stochastic d , and that line can be used. usual periods are 14 days,
		 * and 3 day sma is used for smoothing - that is the d stochastic. it
		 * can be further smoothed by an additional 3 day, but that is
		 * unnecessary.
		 * 
		 * 1.see where the k crosses the d. 2.monitor for changes below and
		 * above 80 and 20. 3.it is a bound indicator so only goes between 0 and
		 * 100.
		 */

		/*
		 * BASED ON AWESOME INDICATOR. here he has given two constructors = one
		 * where we can enter two time periods for start and end. but u use the
		 * other one where it uses the fixed 5,34 rules. - divide by the 34 day
		 * to make it COMPARABLE. Acceleration deceleration indicator, based on
		 * awesome indicator, so has all the dependencies like it. not much
		 * information available regarding it. for all indicators one parameter
		 * is where it crosses the 0 point, or zero line.
		 */

		/*
		 * COMPARABLE WITHOUT ADDITIONAL MODIFICIATIONS. NEEDS HIGH,LOW,CLOSE
		 * use 150 days as the time frame average directional movement
		 * indicator: it is between 0 and 100. above 25 trend is strong. at the
		 * same time we have to see the directional up indicator and the
		 * directional down indicator, for where they cross. if up crosses above
		 * down then it is a bullish sign, provided trend is above 25. and vice
		 * versa.
		 */

		/*
		 * ONLY CLOSE NEEDS TO BE DIVIDED BY THE PRICE ITSELF TO BE MADE
		 * COMPARABLE 3 indicators- double ema. single ema triple ema
		 * 
		 * simple take pairs 10 - 50 day moving average periods , and map the
		 * crossovers. if shorter average crosses above longer, then so called
		 * buy signal. and vice versa.
		 */

		/*
		 * ONLY CLOSE MACD uses the 26 day and 12 day ema;s signal line comes
		 * from the 9 day ema of the indicator itself. also plot an ema of this
		 * indicator itself and detect crossses. not comparable, -SO CANNOT BE
		 * USED FOR THE RANGE GROUPS.
		 */

		/*
		 * NOT ONLY CLOSE NEEDS TO BE DIVIDED BY PRICE TO BE COMPARABLE.
		 * Parabolic sar indicator. it basically trails a rising stock and caps
		 * a falling stock. it can cross the real price when there is a trend
		 * shift so watch for that. also have to check if its rising or falling
		 * and how strong the change is.
		 */

		/*
		 * COMPARABLE WITHOUT ADDITIONAL MODIFICATION. ONLY CLOSE NEEDED
		 * relative strength indicator. watch crossover above or below 30 and 70
		 * , or 20 and 80 if it crosses from below to above 30 then bullish, and
		 * from above to below 70 then bearish. here also time it stayed above
		 * or below is important.
		 */

		/*
		 * high low close COMPARABLE WITHOUT ADDITIONAL MODIFICATIONS. williams
		 * r indicator: inverse of stochastic moves between -100 and 0. also
		 * watch -20 , -80 and -50 closer to zero it is more overbought. and
		 * vice versa. there is no 3 day signal line here like stochastic.
		 */
	}

}
