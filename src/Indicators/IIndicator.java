package Indicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.TreeMap;

import ExchangeBuilder.dataPointIndicator;

public interface IIndicator {
	
	public abstract Boolean save() throws Exception;
	
	public abstract Boolean get_by_name(String name) throws Exception;
	
	public abstract TreeMap<Integer,dataPointIndicator> get_period_change_indicator_values(
			TreeMap<Integer, dataPointIndicator> indicator_values,Integer period);
	
}
