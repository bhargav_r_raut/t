package Indicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import elasticSearchNative.RemoteEs;
import subindicators.SubIndicatorCalculation;
import yahoo_finance_historical_scraper.StockConstants;

public class Indicator implements IIndicator {

	private String indicator_name;
	private String indicator_and_period_name;
	private List<Integer> periods;

	private String id;

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	public String getIndicator_and_period_name() {
		return indicator_and_period_name;
	}

	public void setIndicator_and_period_name(String indicator_and_period_name) {
		this.indicator_and_period_name = indicator_and_period_name;
	}

	public List<Integer> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Integer> periods) {
		this.periods = periods;
	}

	public Indicator(String indicator_name, String indicator_and_period_name, ArrayList<Integer> periods) {
		super();
		this.indicator_name = indicator_name;
		this.indicator_and_period_name = indicator_and_period_name;
		this.periods = periods;
	}

	// for Jackson.
	public Indicator() {

	}

	@Override
	public Boolean save() throws Exception {
		// TODO Auto-generated method stub
		IndexResponse response = RemoteEs.getInstance().prepareIndex().setType("indicator").setIndex("tradegenie_titan")
				.setId(getId()).setSource(CentralSingleton.getInstance().objmapper.writeValueAsBytes(this))
				.setRefresh(true).execute().actionGet();
		if (response.getId().equals(id)) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public Boolean get_by_name(String indicator_and_period_name) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("came to search for indicator and period name:" + indicator_and_period_name);
		SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("indicator")
				.setQuery(QueryBuilders.termQuery("indicator_and_period_name", indicator_and_period_name))
				.setFetchSource(true).execute().actionGet();
		if (sr.getHits().getTotalHits() > 1) {
			throw new Exception("more than one indicator found for name:" + indicator_and_period_name);
		}
		for (SearchHit hit : sr.getHits().getHits()) {
			String source_string = hit.getSourceAsString();
			Indicator i = CentralSingleton.getInstance().objmapper.readValue(source_string, Indicator.class);
			this.setIndicator_and_period_name(i.getIndicator_and_period_name());
			this.setPeriods(i.getPeriods());
			this.setId(i.getId());
			return true;
		}
		return false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static Integer get_percentage_change(BigDecimal start_day_indicator, BigDecimal end_day_indicator) {
		BigDecimal percentage_change;
		Integer percentage_correlation;

		percentage_change = (end_day_indicator.subtract(start_day_indicator).divide(start_day_indicator, 3,
				StockConstants.DataPoint_bd_rounding_mode));

		percentage_change = percentage_change
				.multiply(new BigDecimal(StockConstants.DataPoint_multiplicand_for_deriving_percentage_correlate));

		percentage_correlation = percentage_change.intValueExact();

		return percentage_correlation;
	}

	/****
	 * need a treemap of this type:
	 * 
	 * day-id => datapointIndicator (for this.indicator_period_name) so iterate
	 * the price map internal and for each day id that has
	 * (this.indicator_period_name) add the datapoitnIndicator to the treemap.
	 * 
	 * @used_in: subindicators/Pattern
	 * @used_in: this
	 * 
	 * @param e
	 *            : Entity
	 * @return
	 */
	public TreeMap<Integer, dataPointIndicator> get_indicator_values(Entity e) {
		TreeMap<Integer, dataPointIndicator> indicator_values = new TreeMap<Integer, dataPointIndicator>();
		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {
			Integer day_id = entry.getKey();
			DataPoint dp = entry.getValue();

			if (dp.getIndicator_values_on_datapoint().containsKey(indicator_and_period_name)) {
				indicator_values.put(day_id, dp.getIndicator_values_on_datapoint().get(indicator_and_period_name));
			}
		}

		return indicator_values;
	}

	/***
	 * needed was a treemap like this:
	 * 
	 * day_id => 1(indicator rose on this day id) day_id => 2(indicator fell on
	 * this day id) day_id => 1(indicator rose on this day id)
	 * 
	 * first calls @get_indicator_values(Entity e) then uses that treemap to
	 * iterate and build required treemap.
	 * 
	 * @used_in : subindicators/Pattern
	 * @param e
	 *            : the Entity
	 * @param rise_representation
	 *            : represent indicator rise on day as
	 * @param fall_representation
	 *            : represent indicator fall on day as
	 * @return : TreeMap<Integer, String>
	 * @throws Exception
	 */
	public TreeMap<Integer, String> get_indicator_values_as_pattern(Entity e, String rise_representation,
			String fall_representation) throws Exception {

		TreeMap<Integer, dataPointIndicator> v = get_indicator_values(e);
		TreeMap<Integer, String> pattern_map = new TreeMap<Integer, String>();
		for (Integer day_id : v.keySet()) {
			dataPointIndicator current_value = v.get(day_id);
			if (v.lowerKey(day_id) != null) {
				dataPointIndicator previous_value = v.lowerEntry(day_id).getValue();
				if (SubIndicatorCalculation.GREATER_THAN(current_value.getIndicator_big_decimal_value(),
						previous_value.getIndicator_big_decimal_value())) {
					pattern_map.put(day_id, rise_representation);
				} else {
					pattern_map.put(day_id, fall_representation);
				}
			}
		}
		//if (getIndicator_name().equals("close")) {
		System.out.println("indicator cum period name is:");
		System.out.println(indicator_and_period_name);
		System.out.println("the returned pattern map is:");
		System.out.println(pattern_map);
		//}
		return pattern_map;

	}

	/***
	 * used in indicator value change standard deviation. \n basically given a
	 * period and series of indicator values, \n it will do the following: \n
	 * 
	 * day id 1 -> indicator value 20, day id 2 -> indicator value 24, day id 3
	 * -> \n indicator value 23, day id 4 -> indicator value 29 \n
	 * 
	 * now suppose period is 2 \n
	 * 
	 * what it will do is as follows: \n
	 * 
	 * day id 1 minus 2 -> does not exist \n day id 2 minus 2 -> does not exist
	 * \n day id 3 minus 2 -> exists (calculates as 23 - 20 : 3) \n day id 4
	 * minus 2 -> exists (calculates 29 - 24 : 5) \n
	 * 
	 * so will return a treemap as follows: \n 3(day_id) -> 3(change in the
	 * indicator value) \n 4(day_id) -> 5(change in the indicator value) \n
	 */
	@Override
	public TreeMap<Integer, dataPointIndicator> get_period_change_indicator_values(
			TreeMap<Integer, dataPointIndicator> indicator_values, Integer period) {

		TreeMap<Integer, dataPointIndicator> indicator_value_changes_in_period = new TreeMap<Integer, dataPointIndicator>();

		for (Integer day_id : indicator_values.keySet()) {
			if (indicator_values.containsKey(day_id - period)) {

				dataPointIndicator dpi = indicator_values.get(day_id);
				BigDecimal difference = indicator_values.get(day_id).getIndicator_big_decimal_value()
						.subtract(indicator_values.get(day_id - period).getIndicator_big_decimal_value());

				indicator_value_changes_in_period.put(day_id,
						new dataPointIndicator(difference, dpi.getIndicator_name()));
			}
		}

		return indicator_value_changes_in_period;
	}

}
