package Indicators;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import yahoo_finance_historical_scraper.StockConstants;
 
public class RedisManager {
    private static RedisManager instance;
    private static JedisPool pool;
    private RedisManager() {}
    public final static RedisManager getInstance() {
    	if(instance == null){
    		instance = new RedisManager();
    		instance.connect();
    	}
        return instance;
    }
    public void connect() {
        // Create and set a JedisPoolConfig
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(25);
        pool = new JedisPool(poolConfig,StockConstants.redis_remote_host_name, 6379);
    }
    public void release() {
        pool.destroy();
    }
    public Jedis getJedis() {
        return pool.getResource();
    }
}