package Indicators;

import java.time.ZonedDateTime;
import java.util.TreeMap;

import com.tinkerpop.gremlin.Tokens.T;

import DayIdMapGenerator.DayIdBuilder;
import ExchangeBuilder.CentralSingleton;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.indicators.CachedIndicator;

public class TickMapIndicator {
	
	private CachedIndicator<T> cind;
	private TreeMap<Integer,Integer> tick_to_timeseries_index_map;
	
	public TreeMap<Integer, Integer> getTick_to_timeseries_index_map() {
		return tick_to_timeseries_index_map;
	}

	public void setTick_to_timeseries_index_map(TreeMap<Integer, Integer> tick_to_timeseries_index_map) {
		this.tick_to_timeseries_index_map = tick_to_timeseries_index_map;
	}

	public CachedIndicator<T> getCind() {
		return cind;
	}

	public void setCind(CachedIndicator<T> cind) {
		this.cind = cind;
	}

	

	public TickMapIndicator(CachedIndicator cached_indicator){
		this.cind = cached_indicator;
		this.tick_to_timeseries_index_map = new TreeMap<Integer,Integer>();
		for(int i = 0; i < getCind().getTimeSeries().getTickCount(); i++){
			Tick t = getCind().getTimeSeries().getTick(i);
			ZonedDateTime zdt = t.getEndTime();
			String datestring = DayIdBuilder.zonedatetime_to_datestring(zdt);
			Integer day_id = CentralSingleton.gmt_datestring_to_day_id.get(datestring);
			getTick_to_timeseries_index_map().put(day_id, i);
		}
	}
	
	
	
}
