import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.emory.clir.clearnlp.dependency.DEPNode;
import edu.emory.clir.clearnlp.util.arc.SRLArc;

public class JSONDepNode {

	@JsonIgnore
	private DEPNode depnode;

	private int token_id;

	private String word_form;

	private String lemma;

	private String POS;

	private HashMap<String, String> feats;

	private int head_token_id;

	private String head_label;

	private HashMap<Integer, String> semantic_heads;

	public HashMap<Integer, String> getSemantic_heads() {
		return semantic_heads;
	}

	public void setSemantic_heads(HashMap<Integer, String> semantic_heads) {
		this.semantic_heads = semantic_heads;
	}

	private String ner_tag;

	public String getHead_label() {
		return head_label;
	}

	public void setHead_label(String head_label) {
		this.head_label = head_label;
	}

	public int getToken_id() {
		return token_id;
	}

	public void setToken_id(int token_id) {
		this.token_id = token_id;
	}

	public String getWord_form() {
		return word_form;
	}

	public void setWord_form(String word_form) {
		this.word_form = word_form;
	}

	public String getLemma() {
		return lemma;
	}

	public void setLemma(String lemma) {
		this.lemma = lemma;
	}

	public String getPOS() {
		return POS;
	}

	public void setPOS(String pOS) {
		POS = pOS;
	}

	public HashMap<String, String> getFeats() {
		return feats;
	}

	public void setFeats(HashMap<String, String> feats) {
		this.feats = feats;
	}

	public int getHead_token_id() {
		return head_token_id;
	}

	public void setHead_token_id(int head_token_id) {
		this.head_token_id = head_token_id;
	}

	public String getNer_tag() {
		return ner_tag;
	}

	public void setNer_tag(String ner_tag) {
		this.ner_tag = ner_tag;
	}
	
	@JsonIgnore
	public DEPNode getDepnode() {
		return depnode;
	}

	@JsonIgnore
	public void setDepnode(DEPNode depnode) {
		this.depnode = depnode;
	}

	public JSONDepNode(DEPNode depn) {

		this.depnode = depn;
		setToken_id(depn.getID());
		setWord_form(depn.getLowerSimplifiedWordForm());
		setLemma(depn.getLemma());
		setPOS(depn.getPOSTag());

		if (depn.hasHead()) {
			setHead_token_id(depn.getHead().getID());
			setHead_label(depn.getLabel());
		} else {
			setHead_token_id(-1);
			setHead_label("");

		}

		setFeats(new LinkedHashMap<String, String>());
		if (!depn.getFeats().isEmpty()) {
			setFeats(new HashMap<String, String>());
			for (Map.Entry<String, String> feat : depn.getFeats().entrySet()) {

				getFeats().put(feat.getKey(), feat.getValue());
			}
		}

		setSemantic_heads(new LinkedHashMap<Integer, String>());
		for (SRLArc arc : depn.getSemanticHeadArcList()) {
			getSemantic_heads().put(arc.getNode().getID(), arc.getLabel());
		}

	}
	
	//for jackson.
	public JSONDepNode(){
		
	}

	

}
