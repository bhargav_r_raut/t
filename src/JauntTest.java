import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.dom4j.io.DOMReader;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import yahoo_finance_historical_scraper.StockConstants;
import Indicators.RedisManager;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;

import com.google.gson.Gson;

import elasticSearchNative.LocalEs;
import genericIndicatorFunctions.EsSubIndicator;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

public class JauntTest {

	private Double[] rise_or_fall_amounts = { 0.2, 0.3, 0.4, 0.5, 0.7, 0.9,
			1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0,
			7.0, 8.0, 9.0 };

	private static final Integer total_elements_for_es_load_test = 500;
	private static final Integer start_day_id_for_es_load_test = 16300;
	private static final Integer end_day_id_for_es_load_test = 17800;

	private static final String[] patterns_to_match_subindicator = {
			"subindicator_rises_for_(\\d+)_days",
			"subindicator_falls_for_(\\d+)_days",
			"subindicator_rises_by(_(\\d+\\.\\d+)_percent(_plus)?)?_in_(\\d)_(sessions?|weeks?|months?)",
			"subindicator_falls_by(_(\\d+\\.\\d+)_percent(_plus)?)?_in_(\\d)_(sessions?|weeks?|months?)",
			"subindicator_goes_above_(critical|signal|zero)_line",
			"subindicator_goes_below_(critical|signal|zero)_line",
			"subindicator_(\\d+)_days?_high", "subindicator_(\\d+)_days?_low",
			"subindicator_days_above_(critical|signal|zero)_line",
			"subindicator_days_below_(critical|signal|zero)_line",
			"subindicator_above_(\\d+)_day_sma",
			"subindicator_below_(\\d+)_day_sma" };

	private void run() {
		// test();
		GroovyStatements.initializeGroovyStatements(null);
		// get_docs_by_ids();
		// es_size_test();
		//generate_redis_hashes();
		//benchmark_pipeline();
		//calculate_max_documents();
		SearchResponse sr = LocalEs.
				getInstance().prepareSearch("tradegenie_titan")
				.setTypes("subindicator")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.termQuery("subindicator_name", "subindicator_rises_for_1_session"))
				.setFetchSource(new String[]
						{"subindicator_name","subindicator_numeric_id","subindicator_hex_id","groups"}, null)
				.setSize(1).execute().actionGet();
		
		EsSubIndicator db_subindicator = new EsSubIndicator("subindicator_rises_for_1_session");
		for(SearchHit hit: sr.getHits().hits()){
			//System.out.println(hit.sourceAsMap().get("groups"));
			db_subindicator.setGroups((ArrayList<HashMap<String,Object>>)hit.sourceAsMap().get("groups"));
			//db_subindicator.setNumeric_id((Integer)hit.getSource().get("subindicator_numeric_id"));
			//db_subindicator.setHex_id((String)hit.field("hex_id").value());
			
		}
		
	}
	
	public void calculate_max_documents(){
		Integer sum = 0;
		
		for(int i=0 ; i < 1200; i++){
			sum+=i;
		}
		System.out.println(sum);
	}
	
	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public void generate_redis_hashes() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			for (int i = 0; i < (1250 * 300); i++) {
				Pipeline p = jedis.pipelined();
				Map<String, String> stock_changes = new HashMap<String, String>();
				for (int k = 0; k < 300; k++) {
					stock_changes.put(k + "_stock",
							String.valueOf(randInt(-5, 4)));
				}

				p.hmset("pair_" + i, stock_changes);

				if (i % 300 == 0) {
					System.out.println("did sync with i: " + i + " of total :"
							+ (1250 * 300));
					p.sync();
				}
			}
		}
	}

	public void benchmark_pipeline() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Pipeline pipe = jedis.pipelined();
			for (int i = 0; i < 700; i++) {
				int rand = randInt(0, 375000);
				String pair_name = "pair_" + rand;
				String stockName = "4_stock";
				pipe.hget(pair_name, stockName);
			}
			GregorianCalendar cal = new GregorianCalendar();
			List<Object> results = pipe.syncAndReturnAll();
			GregorianCalendar cal2 = new GregorianCalendar();
			System.out.println(cal2.getTimeInMillis() - cal.getTimeInMillis());
			for(Object res : results){
				System.out.println(res.toString());
			}
		}
	}

	public static ArrayList<Integer> generate_random_arraylist() {

		ArrayList<Integer> periods_list = new ArrayList<Integer>();

		for (int i = 0; i < total_elements_for_es_load_test; i++) {

			periods_list.add(randInt(start_day_id_for_es_load_test,
					end_day_id_for_es_load_test));

		}
		Set<Integer> si = new HashSet<Integer>();
		si.addAll(periods_list);
		periods_list.clear();
		periods_list.addAll(si);

		return periods_list;

	}

	public void get_docs_by_ids() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			List<String> ids = jedis.lrange("ids_list", 0, -1);
			ids.clear();
			for (int i = 0; i < 600; i++) {
				int k = randInt(1600, 1850);
				ids.add("vanshi_" + k);
			}

			Client client = LocalEs.getInstance();
			IdsQueryBuilder iqb = new IdsQueryBuilder(
					"subindicator_indicator_stock_complex");

			TermsQueryBuilder tqb = new TermsQueryBuilder("complex_name", ids);

			for (String id : ids) {
				iqb.addIds(id);
			}

			SearchResponse resp = client.prepareSearch("tradegenie_titan")
					.setTypes("subindicator_indicator_stock_complex")
					.setSearchType(SearchType.QUERY_AND_FETCH).setQuery(tqb)
					.setFrom(0).setSize(1000).setExplain(true).execute()
					.actionGet();
			SearchHit[] sh = resp.getHits().getHits();
			for (int j = 0; j < sh.length; j++) {
				SearchHit s = sh[j];
				ids.add(s.getId());
				System.out.println(s.getId());
			}

		}
	}

	public void test() {
		QueryBuilders.idsQuery(new String[] {});

		ArrayList<String> ids = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {

			Client client = LocalEs.getInstance();

			SearchResponse resp = client
					.prepareSearch("tradegenie_titan")
					.setTypes("subindicator_indicator_stock_complex")
					.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
					.setQuery(
							QueryBuilders.termQuery("complex_name", "vanshi_"
									+ i))
					// Query
					.setFrom(0).setSize(60).setExplain(true).execute()
					.actionGet();
			SearchHit[] sh = resp.getHits().getHits();
			for (int j = 0; j < sh.length; j++) {
				SearchHit s = sh[j];
				ids.add(s.getId());
				System.out.println(s.getId());
			}

		}
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String list_name = "ids_list";
			for (String id : ids) {
				jedis.rpush(list_name, id);
			}
		}

		System.out.println(ids);
	}
	
	/**
	 * create pairs, first in es.
	 * then update pair by query.
	 * 
	 * structure of document.
	 * "pairs"
	 * pair name : contains start and end day id.
	 * pair head complexes:
	 * pair tail complexes:
	 * pair stock changes:
	 * 		"stock_name"
	 * 		"stock_price_change"
	 * 
	 */
	
	
	public void create_es_complexes() {
		Gson gson = new Gson();
		ArrayList<Integer> test = new ArrayList<Integer>();
		Random rand = new Random();
		Integer counter = 0;
		for (int i = 0; i < 1200000; i++) {
			/*
			 * if(i%10000==0 && i>0){ Indicator_SubIndicator_Stock_Complex dum = new Indicator_SubIndicator_Stock_Complex(test);
			 * String message = gson.toJson(dum); try(Jedis jedis =
			 * RedisManager.getInstance().getJedis()){
			 * System.out.println(test.size());
			 * jedis.publish(StockConstants.name_of_redis_channel_for_es_documents
			 * , message); }
			 * 
			 * test.clear();
			 * 
			 * } else{ test.add(i); }
			 */
			ArrayList<Integer> periods_list = generate_random_arraylist();
			Indicator_SubIndicator_Stock_Complex dum = new Indicator_SubIndicator_Stock_Complex(periods_list, "vanshi_"
					+ String.valueOf(i));
			String message = gson.toJson(dum);
			try (Jedis jedis = RedisManager.getInstance().getJedis()) {
				jedis.publish(
						StockConstants.name_of_redis_channel_for_es_documents,
						message);
			}
			if (i % 5000 == 0) {
				Integer total_done = 5000 + 5000 * counter;
				System.out.println(total_done);
				System.out.println("did:" + total_done);
				counter++;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
		
	public void test_component_creation(){
		ArrayList<String> test_components = new ArrayList<String>(Arrays.asList("doggy","catty"));
		Integer result = GroovyStatements.getInstance().get_counter_for_element_or_index_it(test_components);
		System.out.println(result);
	}	
	
	
	public org.dom4j.Document hackety_hack() {

		try {
			String xml_string = ReadWriteTextToFile.readFile(
					"forex_history.xml", Titan.TitanConstants.ENCODING);
			InputStream ins = new ByteArrayInputStream(xml_string.getBytes());
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document w3cDoc = db.parse(ins);
			org.dom4j.io.DOMReader dom4j_reader = new DOMReader();
			org.dom4j.Document document = dom4j_reader.read(w3cDoc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new JauntTest().run();
	}

}
