import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.script.ScriptService;
import org.mortbay.util.ajax.JSON;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Tuple;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.AddComplexValueEdgesToGraph;
import Indicators.RedisManager;
import Titan.GroovyStatements;
import Titan.TitanBaseConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lambdazen.bitsy.BitsyGraph;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Vertex;

import dailyDownload.Download;

import elasticSearchNative.LocalEs;

public class JedisTest {

	private CountDownLatch quit_message_received_latch = new CountDownLatch(1);
	private BulkProcessor bulkprocessor;
	private static final String identifier_for_log = "jedis_es_adder";
	private final Integer bulk_items_amount = 500;
	private Gson gson;
	private AtomicInteger atomic_counter;
	private static final Integer total_elements_for_es_load_test = 2400000;
	private static final Integer start_day_id_for_es_load_test = 1600;
	private static final Integer end_day_id_for_es_load_test = 2720;
	private static final String TAG = "test";
	private static final Double[] rise_or_fall_amounts = { 0.25, 0.5, 1.0, 1.5,
			2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0,
			14.0, 16.0, 18.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0,
			60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 100.0 };

	public static void main(String[] args) throws InterruptedException {

		// new JedisTest().run();
		// test_jedis_pipe();
		// check_jedis_space_usage();
		// System.out.println(compressString("id better get something out of all this"));
		// basicExample();
		// test_binary_search();
		// test_rounding();

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, "daily_download-BEGIN",
						"#########################----------begin-------##############################");

		Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		GroovyStatements gs = null;
		Gson gson = null;

		try {
			client = LocalEs.getInstance();
			// cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			// g = cfg.get_graph();

			// mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g, mgmt);

			// CreateCommonDayIdVertices cid = new
			// CreateCommonDayIdVertices(g,mgmt);

			// gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"daily_download", "finished initialization corretly");

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}
		new Download().run(client, gson);
		System.exit(1);
		// System.out.println(StockConstants.standard_deviation_subindicator_names_and_periods);

		// System.out.println(StockConstants.standard_deviation_subindicator_names_and_periods);
	}

	public static void test_rounding() {
		Double test_number = 91.66;
		System.out.println(5 * (Math.round(test_number / 5.0)));
		System.out.println(2 * (Math.round(test_number / 2.0)));

	}

	public static void test_binary_search() {
		double[] dog = { 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0,
				45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0,
				95.0, 100.0, 105.0, 110.0, 115.0, 120.0, 125.0, 130.0, 135.0,
				140.0, 145.0, 150.0, 155.0, 160.0, 165.0, 170.0, 175.0, 180.0,
				185.0, 190.0, 195.0, 200.0, 205.0, 210.0, 215.0, 220.0, 225.0,
				230.0, 235.0, 240.0, 245.0, 250.0, 255.0, 260.0, 265.0, 270.0,
				275.0, 280.0, 285.0, 290.0, 295.0, 300.0 };
		System.out.println(dog.length);
		int result = Arrays.binarySearch(dog, 3.0);
		System.out.println(result);
	}

	public static byte[] int_array_to_byte_array(int[] con) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			for (int i : con)
				dos.writeInt(i);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		byte[] arr = baos.toByteArray();
		return arr;
	}

	public static int[] byte_array_to_int_array(byte[] arr) {
		ByteArrayInputStream bais = new ByteArrayInputStream(arr); // byte[] arr
		DataInputStream dis = new DataInputStream(bais);
		int[] tab = null;
		int lenght = 0;
		try {
			lenght = dis.available() / 4;
			tab = new int[lenght];
			for (int i = 0; i < lenght; i++) {
				tab[i] = dis.readInt();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return tab;
	}

	public static void basicExample() {
		int[] data = new int[2000];
		System.out
				.println("Compressing " + data.length + " integers in one go");
		// data should be sorted for best
		// results
		for (int k = 0; k < data.length; ++k)
			data[k] = k;
		// Very important: the data is in sorted order!!! If not, you
		// will get very poor compression with IntegratedBinaryPacking,
		// you should use another CODEC.

		// next we compose a CODEC. Most of the processing
		// will be done with binary packing, and leftovers will
		// be processed using variable byte
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		// output vector should be large enough...
		int[] compressed = new int[data.length + 1024];
		// compressed might not be large enough in some cases
		// if you get java.lang.ArrayIndexOutOfBoundsException, try
		// allocating more memory

		/**
		 * 
		 * compressing
		 * 
		 */
		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);
		// got it!
		// inputoffset should be at data.length but outputoffset tells
		// us where we are...
		System.out.println("compressed from " + data.length * 4 / 1024
				+ "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");
		// we can repack the data: (optional)
		compressed = Arrays.copyOf(compressed, outputoffset.intValue());

		System.out
				.println("done---------------------------------------------------------------");
		byte[] byte_arr = int_array_to_byte_array(compressed);
		String key_name = "testy";

		// ////////////////////////////////////////////////////////////////////////

		GregorianCalendar cal_s = new GregorianCalendar();
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			for (int i = 0; i < 10000; i++) {

				byte[] resp = jedis.get(("testy" + String.valueOf(i))
						.getBytes());

				int[] l = byte_array_to_int_array(resp);

				int[] recovered = new int[3000];
				IntWrapper recoffset = new IntWrapper(0);

				codec.uncompress(l, new IntWrapper(0), l.length, recovered,
						recoffset);

			}

		}
		GregorianCalendar cal_e = new GregorianCalendar();
		System.out.println(cal_e.getTimeInMillis() - cal_s.getTimeInMillis());
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * 
		 * now uncompressing
		 * 
		 */

	}

	private static String compressString(String stringToCompress) {
		GregorianCalendar cal = new GregorianCalendar();
		byte[] input = stringToCompress.getBytes();
		// Create the compressor with highest level of compression
		Deflater compressor = new Deflater();
		// compressor.setLevel(Deflater.BEST_COMPRESSION);
		// Give the compressor the data to compress
		compressor.setInput(input);
		compressor.finish();
		// Create an expandable byte array to hold the compressed data.
		// You cannot use an array that's the same size as the orginal because
		// there is no guarantee that the compressed data will be smaller than
		// the uncompressed data.
		ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);
		// Compress the data
		byte[] buf = new byte[1024];
		while (!compressor.finished()) {
			int count = compressor.deflate(buf);
			bos.write(buf, 0, count);
		}

		try {
			bos.close();
		} catch (IOException e) {

		}
		// Get the compressed data
		byte[] compressedData = bos.toByteArray();
		String name_of_key = "testy";

		GregorianCalendar cal_end = new GregorianCalendar();
		System.out.println(cal_end.getTimeInMillis() - cal.getTimeInMillis());

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			jedis.set(name_of_key.getBytes(), compressedData);
		}

		return new String(compressedData);
	}

	public static void check_java_inflation() {
		Inflater inflater = new Inflater();
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String value = jedis.get("testy");
			byte[] byte_array = value.getBytes();
			inflater.inflate(byte_array);
		} catch (DataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void check_jedis_space_usage() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String test_string = "h";
			for (int ik = 0; ik < 10; ik++) {
				test_string = test_string + String.valueOf(ik);
			}
			Pipeline pipe = jedis.pipelined();
			for (int i = 0; i < 40000; i++) {
				for (int k = 0; k < 1; k++) {
					pipe.hset(String.valueOf(i), "tf_" + String.valueOf(i),
							test_string);
				}
			}
			pipe.sync();
		}
		System.out.println("done");
	}

	@SuppressWarnings("deprecation")
	private void whats_wrong_with_jedis() {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			/**
			 * System.out.println(jedis.hget("pair_es_ids", "16504_16505"));
			 * 
			 * ScanParams scanparams = new ScanParams(); scanparams.count(10);
			 * ScanResult<Entry<String,String>> scanres =
			 * jedis.hscan("pair_es_ids", 10, scanparams);
			 * List<Entry<String,String>> hash_keys = scanres.getResult();
			 * for(Entry<String,String> hk : hash_keys){
			 * System.out.println(hk.getKey());
			 * System.out.println(hk.getValue()); }
			 */
			TreeMap tm = new TreeMap();
			tm.put(10, "a");
			tm.put(11, "b");
			tm.put(12, "c");
			NavigableMap<Integer, String> nvm = tm.headMap(11, true);
			NavigableMap<Integer, String> desc = nvm.descendingMap();
			System.out.println(desc);

		}
	}

	private void test_deserialize() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			ObjectMapper m = new ObjectMapper();
			Set<Tuple> ohlcv_maps_for_this_stock = jedis.zrangeWithScores(
					"stock_BNP.PA_CAC_ohlcv_values", 0l, -1l);
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
			};
			for (Tuple tup : ohlcv_maps_for_this_stock) {
				Double day_id = tup.getScore();
				String ohlcv_map = tup.getElement();
				HashMap<String, Object> object = (HashMap<String, Object>) JSON
						.parse(ohlcv_map);
				System.out.println("day id in redis is:" + day_id
						+ " ohlcv map in redis on that day is:" + ohlcv_map);
				Integer day_id_i = day_id.intValue();

			}

		}
	}

	private void understand_longs() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Long hash_length = jedis.hlen("pair_es_ids");
			if (hash_length
					.equals(StockConstants.total_pairs_that_should_exist_in_es)) {
				System.out.println("works");
			}
		}
	}

	public static void test_jedis_pipe() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			Pipeline pipe = jedis.pipelined();
			for (int i = 0; i < 1095000; i++) {
				pipe.hset("test_key_10", "i_" + String.valueOf(i),
						String.valueOf(i));
				if (i % 10000 == 0) {
					System.out
							.println("syncing pipe I IS:" + String.valueOf(i));
					pipe.sync();
				}
			}
		}
	}

	private void test_bitsy_speed() {
		BitsyGraph graph = new BitsyGraph();
		graph.createKeyIndex("day_id", Vertex.class);
		for (int i = 0; i < 1200; i++) {
			Vertex v = graph.addVertex(null);
			v.setProperty("day_id", i);
			v.setProperty("has_day_id", 1);
		}

		GroovyStatements.getInstance().initializeGroovyStatements(graph);
		ArrayList<Integer> random_elements = new ArrayList<Integer>();
		for (int i = 0; i < 500; i++) {
			random_elements.add(randInt(0, 1200));
		}
		GroovyStatements.getInstance().check_bitsy_speed(random_elements);
	}

	private void generate_pair_docs() {
		for (int i = start_day_id_for_es_load_test; i < end_day_id_for_es_load_test; i++) {

			for (int k = i; k < i + 300; k++) {
				HashMap<String, Object> hh = new LinkedHashMap<String, Object>();
				ArrayList<LinkedHashMap<String, Object>> pair_stock_changes = new ArrayList<LinkedHashMap<String, Object>>();
				hh.put("pair_name", String.valueOf(i) + "_" + String.valueOf(k));
				for (int o = 0; o < 300; o++) {
					LinkedHashMap<String, Object> jk = new LinkedHashMap<String, Object>();
					jk.put("stock_name", String.valueOf(o) + "_stock");
					jk.put("stock_price_change", randInt(-2, 2));
					pair_stock_changes.add(jk);
				}
				hh.put("pair_stock_changes", pair_stock_changes);
				index_pairs_periods(hh);
			}
			System.out.println("doing pair docs:" + i);
		}
	}

	public void index_pairs_periods(final HashMap<String, Object> hh) {
		IndexRequest ind = new IndexRequest("tradegenie_titan", "pairs");
		ind.source(hh);
		getBulkprocessor().add(ind);
	}

	public void index_historical_stock_object(Map<String, Object> hh) {
		IndexRequest ind = new IndexRequest("tradegenie_titan",
				"historical_data_point");
		ind.source(hh);
		getBulkprocessor().add(ind);
	}

	private void run() throws InterruptedException {
		/**
		 * atomic_counter = new AtomicInteger(0); this.gson = new Gson();
		 * //generate_dummy_ind_subind_stock_complex_docs();
		 * 
		 * generate_pair_docs(); //quit_message_received_latch.await();
		 * log("Got quit message"); // jedisPubSub.unsubscribe();
		 * LoggingDailyOperations.get_instance().log_on_exit();
		 **/
		// test_upsert();
		// test_bitsy_speed();
		// test_delete_by_query_api();
		// understand_longs();
		// test_deserialize();
		whats_wrong_with_jedis();
	}

	private void test_delete_by_query_api() {
		DeleteByQueryResponse del_response = LocalEs.getInstance()
				.prepareDeleteByQuery("tradegenie_titan").setTypes("pairs")
				.setQuery(matchAllQuery()).execute().actionGet();
		if (del_response.status().toString().equals("OK")) {
			System.out.println("ok");
		}
	}

	private ArrayList<String> get_list_of_ids() {

		ArrayList<String> ar = new ArrayList<String>();
		String ls = "vanshi";
		for (int i = 130; i < 530; i++) {
			ar.add("\"" + ls + "_" + i + "\"");
		}
		System.out.println(ar);
		return ar;
	}

	public static int[] convertIntegers(List<Integer> integers) {
		int[] ret = new int[integers.size()];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = integers.get(i).intValue();
		}
		return ret;
	}

	public void test_compare() {
		Set<Integer> random_list_one = new HashSet<Integer>();
		Set<Integer> random_list_two = new HashSet<Integer>();

		for (int i = 0; i < 700; i++) {
			random_list_one.add(randInt(16500, 180000));
			random_list_two.add(randInt(16500, 180000));
		}

		ArrayList<Integer> rl1 = new ArrayList<Integer>();
		ArrayList<Integer> rl2 = new ArrayList<Integer>();
		rl1.addAll(random_list_one);
		rl2.addAll(random_list_two);
		Collections.sort(rl1);
		Collections.sort(rl2);
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Pipeline pipe = jedis.pipelined();
			System.out.println("started");
			System.out.println(rl2);

			ArrayList<String> compere = new ArrayList<String>();
			GregorianCalendar cal = new GregorianCalendar();
			int[] b = convertIntegers(rl2);
			int[] a = convertIntegers(rl1);

			for (Integer i : rl1) {
				// System.out.println("searched for:" + i);
				int k = Arrays.binarySearch(b, i);
				pipe.hget("", "");
				// System.out.println(k);
			}

			System.out.println("finished");
			System.out.println(compere);
			GregorianCalendar cal2 = new GregorianCalendar();
			System.out.println(cal2.getTimeInMillis() - cal.getTimeInMillis());

		}
	}

	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static void add_compresses_lists_to_redis() {

	}

	public static ArrayList<Integer> generate_random_arraylist() {

		ArrayList<Integer> periods_list = new ArrayList<Integer>();

		for (int i = 0; i < total_elements_for_es_load_test; i++) {

			periods_list.add(randInt(start_day_id_for_es_load_test,
					end_day_id_for_es_load_test));

		}
		Set<Integer> si = new HashSet<Integer>();
		si.addAll(periods_list);
		periods_list.clear();
		periods_list.addAll(si);

		return periods_list;

	}

	/**
	 * this generates an arraylist of hashmaps which contain the information for
	 * the day ids in the indicator subindicator stock complex.
	 * 
	 */
	public ArrayList<HashMap<String, Object>> generate_final_day_ids() {

		ArrayList<HashMap<String, Object>> day_ids = new ArrayList<HashMap<String, Object>>();
		// first generate 750 random numbers.

		Integer start_day = randInt(0, 10);
		ArrayList<Integer> random_list = new ArrayList<Integer>();
		GregorianCalendar cal = new GregorianCalendar();
		for (int i = 0; i < 1200; i++) {
			// System.out.println(" i was:" + i);
			if (i < start_day) {
				random_list.add(start_day);
			} else {
				if (i + 1 >= 1200) {
					// System.out.println("i + 1 was greater");
					start_day = 1200;
				} else if (i + 1 < 1200 && i + 3 > 1200) {
					// System.out.println("i + 3 was greater");
					start_day = randInt(i + 1, 1200);
				} else {
					// System.out.println("netierh was greater");
					start_day = randInt(i + 1, i + 3);
				}
				random_list.add(start_day);
			}
		}

		Integer counter = 0;
		for (Integer q : random_list) {
			HashMap<String, Object> dd = new HashMap<String, Object>();
			dd.put("day_id", q);
			// dd.put("value", 1.0);
			dd.put("position", counter);
			day_ids.add(dd);
			counter++;
		}
		// System.out.println(random_list);

		return day_ids;
	}

	public void generate_dummy_ind_subind_stock_complex_docs() {
		Gson gson = new Gson();
		ArrayList<Integer> test = new ArrayList<Integer>();
		Random rand = new Random();
		Integer counter = 0;
		for (int i = 0; i < total_elements_for_es_load_test; i++) {
			// ArrayList<Integer> periods_list = generate_random_arraylist();

			index_individual_dummy_ind_subind_stock_complex_doc(
					generate_final_day_ids(), "vanshi_" + String.valueOf(i));

			System.out.println("did counter:" + i);
			if (i > 0 && i % 10000 == 0) {
				System.out.println("sleeping for two seconds.");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	public void index_individual_dummy_ind_subind_stock_complex_doc(
			final ArrayList<HashMap<String, Object>> hm,
			final String complex_name) {

		IndexRequest ind = new IndexRequest("tradegenie_titan",
				"subindicator_indicator_stock_complex");

		final ArrayList<Integer> just_day_ids = new ArrayList<Integer>();
		for (HashMap<String, Object> hhm : hm) {
			for (Map.Entry<String, Object> entry : hhm.entrySet()) {
				if (entry.getKey().equals("day_id")) {
					just_day_ids.add((Integer) entry.getValue());
				}
			}
		}

		ind.source(new HashMap<String, Object>() {
			{
				put("day_ids", just_day_ids);
				put("complex_name", complex_name);
			}
		});
		getBulkprocessor().add(ind);
	}

	public void test_upsert() {
		/*
		 * UpdateRequest updateRequest = new UpdateRequest("tradegenie_titan",
		 * "pairs", "1").script("test_upsert", ScriptService.ScriptType.FILE,
		 * new HashMap<String, Object>() { { put("count", 2); } }).upsert(new
		 * HashMap<String, Object>() { { put("pair_difference", 2); } });
		 * updateRequest.scriptedUpsert(true);
		 */

		UpdateRequest uRequest = new UpdateRequest("tradegenie_titan", "pairs",
				"1").script("test_upsert", ScriptService.ScriptType.FILE,
				new HashMap<String, Object>() {
					{
						put("pair_stock_change", new HashMap<String, Object>() {
							{
								put("stock_name", "dog");
							}
						});
					}
				});

		try {
			LocalEs.getInstance().update(uRequest).get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * this function is kept for legacy value to see how to use update script
	 * from groovy using es.
	 * 
	 * @param message
	 */
	public void decipher_elastic_object_and_update_day_ids(String message) {

		if (message.equals("stop")) {

			try {
				getBulkprocessor().awaitClose(30, TimeUnit.SECONDS);
				quit_message_received_latch.countDown();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e, identifier_for_log, null);
			}

		}

		AddComplexValueEdgesToGraph adv = gson.fromJson(message,
				AddComplexValueEdgesToGraph.class);
		// System.out.println("came inside the decipher message part.these are the complex values");
		// System.out.println(adv.getComplex_values().toString());

		for (Map.Entry<Integer, Double> entry : adv.getComplex_values()
				.entrySet()) {

			final Entry<Integer, Double> ee = entry;

			final Map<String, Object> e = new HashMap<String, Object>() {
				{
					put("day_id", ee.getKey());
					put("value", ee.getValue());
				}
			};

			UpdateRequest updateRequest = new UpdateRequest("tradegenie_titan",
					"subindicator_indicator_stock_complex",
					adv.getEs_object_id()).script("add_day_id",
					ScriptService.ScriptType.FILE,
					new HashMap<String, Object>() {
						{
							put("day_id_and_value", e);
						}
					});

			getBulkprocessor().add(updateRequest);

		}

	}

	/********************************************************************************
	 * 
	 * BULK PROCESSOR AND REDIS SETUP SUBSCRIBER CODE IS FOLLOWS.
	 * 
	 * 
	 * 
	 * @return
	 */

	public BulkProcessor getBulkprocessor() {

		if (bulkprocessor == null) {
			this.bulkprocessor = BulkProcessor
					.builder(LocalEs.getInstance(),
							new BulkProcessor.Listener() {

								@Override
								public void afterBulk(long execution_id,
										BulkRequest request,
										BulkResponse response) {
									// TODO Auto-generated method stub

									BulkItemResponse[] responses = response
											.getItems();
									for (int i = 0; i < responses.length; i++) {

										if (responses[i].isFailed()) {

											LoggingDailyOperations
													.get_instance()
													.add_to_log(
															"exception",
															new Exception(
																	responses[i]
																			.getFailureMessage()),
															identifier_for_log,
															null);
										} else {
											// getIds_of_es_complex_docs().add(
											// responses[i].getId());
										}
									}
									if (!LoggingDailyOperations.get_instance()
											.has_exception()) {
										LoggingDailyOperations
												.get_instance()
												.add_to_log(
														"info",
														null,
														identifier_for_log,
														"added to es:"
																+ (bulk_items_amount + responses.length
																		* atomic_counter
																				.intValue()));
										atomic_counter.getAndIncrement();
									}

								}

								@Override
								public void afterBulk(long arg0,
										BulkRequest arg1, Throwable arg2) {
									// TODO Auto-generated method stub
									arg2.printStackTrace();
									LoggingDailyOperations
											.get_instance()
											.add_to_log(
													"exception",
													new Exception(
															arg2.getLocalizedMessage()),
													identifier_for_log, null);

								}

								@Override
								public void beforeBulk(long arg0,
										BulkRequest arg1) {
									// TODO Auto-generated method stub
									System.out
											.println("these were the number of requests sent to es. bulk");
									// System.out.println(arg1.numberOfActions());

								}

							}).setBulkActions(bulk_items_amount.intValue())
					.setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
					.setFlushInterval(TimeValue.timeValueSeconds(30))
					.setConcurrentRequests(1).build();
		}
		return bulkprocessor;
	}

	private JedisPubSub setupSubscriber() {
		final JedisPubSub jedisPubSub = new JedisPubSub() {
			@Override
			public void onUnsubscribe(String channel, int subscribedChannels) {
				log("onUnsubscribe");
			}

			@Override
			public void onSubscribe(String channel, int subscribedChannels) {
				log("onSubscribe");
			}

			@Override
			public void onPUnsubscribe(String pattern, int subscribedChannels) {
			}

			@Override
			public void onPSubscribe(String pattern, int subscribedChannels) {
			}

			@Override
			public void onPMessage(String pattern, String channel,
					String message) {
			}

			@Override
			public void onMessage(String channel, String message) {

				// index_individual_dummy_ind_subind_stock_complex_doc(message);
				// /decipher_elastic_object_and_update_day_ids(message);

			}
		};
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					log("Connecting");
					try (Jedis jedis = RedisManager.getInstance().getJedis()) {
						log("subscribing");
						jedis.subscribe(
								jedisPubSub,
								StockConstants.name_of_redis_channel_for_es_documents);
						log("subscribe returned, closing down");
						jedis.quit();
					}
				} catch (Exception e) {
					log(">>> OH NOES Sub - " + e.getMessage());
					// e.printStackTrace();
				}
			}

		}, "subscriberThread").start();
		return jedisPubSub;
	}

	static final long startMillis = System.currentTimeMillis();

	private static void log(String string, Object... args) {
		long millisSinceStart = System.currentTimeMillis() - startMillis;
		System.out.printf("%20s %6d %s\n", Thread.currentThread().getName(),
				millisSinceStart, String.format(string, args));
	}
}