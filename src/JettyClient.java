import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.List;

import Titan.ReadWriteTextToFile;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JettyClient {

	public static void main(String[] args) throws Exception {

		String request_string = "";
		request_string = URLEncoder.encode(request_string);
		String response = ReadWriteTextToFile
				.getTextFromUrl("http://192.168.10.5:8080/?" + request_string);
		System.out.println(URLDecoder.decode(response));

		ObjectMapper mapper = new ObjectMapper();
		List<LinkedHashMap<String, Object>> s = mapper.readValue(
				URLDecoder.decode(response), List.class);
		for (LinkedHashMap<String, Object> ln : s) {
			System.out.println(ln);
		}

	}

}
