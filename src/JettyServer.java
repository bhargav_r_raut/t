import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JettyServer extends AbstractHandler{

	
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html;charset=utf-8");
		
		System.out.println(request.getQueryString());
		response.setStatus(HttpServletResponse.SC_OK);
		baseRequest.setHandled(true);
		ArrayList<String> resp = new ArrayList<String>();
		resp.add("\"test\"");
		ObjectMapper mapper = new ObjectMapper();
		
		response.getWriter().println(URLEncoder.encode(mapper.writeValueAsString(resp)));
	}

	public static void main(String[] args) throws Exception {
		Server server = new Server(8081);
		server.setHandler(new JettyServer());
		server.start();
		server.join();
	}
}
