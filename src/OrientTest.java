import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;

import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;


public class OrientTest {
	
	private static final Integer total_elements_for_es_load_test = 500;
	private static final Integer start_day_id_for_es_load_test = 0;
	private static final Integer end_day_id_for_es_load_test = 1200;
	
	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
	
	public static ArrayList<Integer> generate_random_arraylist() {

		ArrayList<Integer> periods_list = new ArrayList<Integer>();

		for (int i = 0; i < total_elements_for_es_load_test; i++) {

			periods_list.add(randInt(start_day_id_for_es_load_test,
					end_day_id_for_es_load_test));

		}
		Set<Integer> si = new HashSet<Integer>();
		si.addAll(periods_list);
		periods_list.clear();
		periods_list.addAll(si);

		return periods_list;

	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// AT THE BEGINNING
		
		try {
			ReadWriteTextToFile.getInstance().readFile("/home/aditya/Desktop/demo/database.ocf", TitanConstants.ENCODING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		OrientGraphFactory factory = new OrientGraphFactory("memory:test");
		factory.getTx().shutdown(); // AUTO-CREATE THE GRAPH IF NOT EXISTS
		factory.setupPool(1,10);

		// EVERY TIME YOU NEED A GRAPH INSTANCE
		OrientGraph g = factory.getTx();
		try {
			for(int i=0; i < 1200; i++){
				Vertex v = g.addVertex(null);
				v.setProperty("day_id", i);
				g.commit();
			}
		
			
			
		} finally {
		   g.shutdown();
		}
		
	}

}
