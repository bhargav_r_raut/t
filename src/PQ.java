import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Titan.GroovyStatements;


public class PQ {

	private static final int MYTHREADS = 4;
	public static final int total_pairs = 300*3650*18;
	public static void main(String args[]) throws Exception {
		GregorianCalendar start_cal = new GregorianCalendar();
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		String[] queries_list = new String[10000];
		GroovyStatements.initializeGroovyStatements(null);
		UnsortedCompressionRatio.build_concurrent_hashmap();
		for (int i = 0; i < queries_list.length; i++) {
 
			String url = queries_list[i];
			Runnable worker = new MyRunnable();
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {
 
		}
		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println("\nFinished all threads in:" + (end_cal.getTimeInMillis() - start_cal.getTimeInMillis()));
	}
 
	public static class MyRunnable implements Runnable {
		
 
		MyRunnable() {
			
		}
 
		@Override
		public void run() {
			GregorianCalendar cal = new GregorianCalendar();
			Random r = new Random();
			//we want a random number from 0 350
			String stock = String.valueOf(r.nextInt(350));
			
			String random_sorted_key_name = stock + "_" + String.valueOf(0);
			String name_of_unsorted_map = stock + "_" + String.valueOf(1);
			//we will recover a random sorted map.
			//for(int k=0; k < 10; k++){
				GregorianCalendar start_decom_sorted = new GregorianCalendar();
				int[] sorted_array = UnsortedCompressionRatio.get_uncompressed_sorted_int_array(GroovyStatements.getInstance()
						.cmap.get(random_sorted_key_name),total_pairs);
				GregorianCalendar end_decom_sorted = new GregorianCalendar();
				System.out.println("decom sorted:" + (end_decom_sorted.getTimeInMillis() - start_decom_sorted.getTimeInMillis()));
				
				//we will recover a the unsorted map.
				int[] unsorted_array = UnsortedCompressionRatio.get_uncompressed_integer_array
						(GroovyStatements.getInstance().cmap.get(name_of_unsorted_map),
								total_pairs);
				
				//we will decompress both and take
				for(int i=0; i < 3000; i++){
					int l = sorted_array[r.nextInt(total_pairs)];
					int d = unsorted_array[r.nextInt(total_pairs)];
				}
			//}
			//3000 random elements from both of them.
			
			GregorianCalendar end_cal = new GregorianCalendar();
			System.out.println("query took:" + (end_cal.getTimeInMillis() - cal.getTimeInMillis()));
		}
	}

	

}
