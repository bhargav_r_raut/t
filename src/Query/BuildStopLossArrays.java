package Query;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.PairArray;
import elasticSearchNative.LocalEs;

public class BuildStopLossArrays {

	public BuildStopLossArrays(ArrayList<String> entity_unique_names) {
		for (String entity_unique_name : entity_unique_names) {

		}

	}

	public static HashMap<String, int[]> build_stop_loss_arrays(
			ArrayList<String> entity_unique_names) throws JsonParseException,
			JsonMappingException, IOException {

		HashMap<String, int[]> all_returned_arrays = new HashMap<String, int[]>();

		for (String entity_unique_name : entity_unique_names) {

			HashMap<String, int[]> entity_arrays = new LinkedHashMap<String, int[]>();
			SearchResponse resp = LocalEs
					.getInstance()
					.prepareSearch("tradegenie_titan")
					.setTypes("stop_loss_arrays")
					.setQuery(
							QueryBuilders.boolQuery().must(
									QueryBuilders.termQuery(
											StockConstants.Entity_unique_name,
											entity_unique_name)))
					.addSort(StockConstants.Entity_chunk_number, SortOrder.ASC)
					.setFetchSource(true).setSize(22).execute().actionGet();

			for (SearchHit hit : resp.getHits().getHits()) {

				Chunk co = CentralSingleton.getInstance().objmapper
						.readValue(hit.getSourceAsString(), Chunk.class);

				if (entity_arrays.containsKey(co.getArr_name())) {

					int[] concatenated_arr = PairArray.concat_Arrays(
							entity_arrays.get(co.getArr_name()), co.getArr());
					entity_arrays.put(co.getArr_name(), concatenated_arr);
				} else {

					entity_arrays.put(co.getArr_name(), co.getArr());

				}

			}

			all_returned_arrays.putAll(entity_arrays);

		}

		return all_returned_arrays;
	}

	public static void main(String[] args) throws InterruptedException,
			ExecutionException, JsonParseException, JsonMappingException, IOException {
		
		CentralSingleton.initializeSingleton();
		ArrayList<String> names = new ArrayList<String>();
		names.add("test_0");
		names.add("test_1");
		build_stop_loss_arrays(names);
		System.exit(1);

		try {
			
			Random r = new Random();
			int[] test_arr = new int[CentralSingleton.getInstance().gmt_day_id_to_dataPoint
					.size() * 300];
			for (int i = 0; i < CentralSingleton.getInstance().gmt_day_id_to_dataPoint
					.size() * 300; i++) {
				test_arr[i] = r.nextInt(500);
			}
			// now compress it.
			int[] compressed_int_arr = PairArray.get_compressed_int_array(
					test_arr, 0);
			for (Integer t = 0; t < 2; t++) {
				// now chop and commit.
				final String entity_uniq_name = "test_" + t;
				final String entity_id = "test_id_" + t;
				int[][] chunks_array = PairArray.chunkArray(compressed_int_arr,
						3000);

				Integer array_counter = 0;
				for (int arr = 0; arr < 2; arr++) {
					Integer counter = 0;
					for (final int[] chunk : chunks_array) {
						System.out.println("doing chunk:" + counter
								+ " of array: " + arr + " of entity:" + t);
						final String id = entity_id + "C"
								+ StockConstants.Entity_chunk_prefix + counter;

						final Integer chunk_number = counter;
						final Integer arr_counter = array_counter;

						UpdateRequest ureq = new UpdateRequest(
								"tradegenie_titan", "stop_loss_arrays", id);

						HashMap<String, Object> doc = new HashMap<String, Object>() {
							{
								put(StockConstants.Entity_arr, chunk);
								put(StockConstants.Entity_chunk_number,
										chunk_number);
								put(StockConstants.Entity_unique_name,
										entity_uniq_name);
								put(StockConstants.Entity_arr_name, "array"
										+ arr_counter);
							}
						};

						ureq.upsert(CentralSingleton.getInstance().objmapper
								.writeValueAsBytes(doc));

						ureq.doc(CentralSingleton.getInstance().objmapper
								.writeValueAsBytes(doc));

						LocalEs.getInstance().update(ureq).get();

						counter++;
					}
					array_counter++;
				}

			}

		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void get_mem_dump() {
		Runtime runtime = Runtime.getRuntime();

		NumberFormat format = NumberFormat.getInstance();

		StringBuilder sb = new StringBuilder();
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();

		sb.append("free memory: " + format.format(freeMemory / 1024) + "<br/>");
		sb.append("allocated memory: " + format.format(allocatedMemory / 1024)
				+ "<br/>");
		sb.append("max memory: " + format.format(maxMemory / 1024) + "<br/>");
		sb.append("total free memory: "
				+ format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)
				+ "<br/>");
		System.out.println(sb.toString());
	}

	public static void check_memory_usage() {
		HashMap<String, int[]> holder = new HashMap<String, int[]>();
		for (int i = 0; i < 600; i++) {
			Random r = new Random();
			int[] test_arr = new int[3600 * 300];
			for (int k = 0; k < 3600 * 300; k++) {
				test_arr[k] = r.nextInt(500);
			}
			holder.put("tst" + String.valueOf(i),
					PairArray.get_compressed_int_array(test_arr, 0));
			System.out.println("after entity:" + String.valueOf(i)
					+ " dump is:");
			get_mem_dump();
		}
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
