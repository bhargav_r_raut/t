package Query;

public class Chunk {

	private String arr_name;
	private String entity_unique_name;
	private int[] arr;
	private Integer chunk_number;
	
	public String getArr_name() {
		return arr_name;
	}

	public String getEntity_unique_name() {
		return entity_unique_name;
	}

	public int[] getArr() {
		return arr;
	}

	public Integer getChunk_number() {
		return chunk_number;
	}
	
	public Chunk(){
		
	}

	public Chunk(String arr_name, String entity_unique_name, int[] arr, Integer chunk_number){
		this.arr = arr;
		this.arr_name = arr_name;
		this.entity_unique_name = entity_unique_name;
		this.chunk_number = chunk_number;
	}
}
