package RedisAccessObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.verdelhan.ta4j.analysis.criteria.BuyAndHoldCriterion;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import Indicators.RedisManager;

public class ComplexBuySellLists {

	private ArrayList<String> complex_names;

	private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Integer>>> buy_sell_lists_by_complex_name;

	private LinkedHashMap<String, Response<List<byte[]>>> byte_responses;

	public LinkedHashMap<String, LinkedHashMap<String, ArrayList<Integer>>> getBuy_sell_lists_by_complex_name() {
		return buy_sell_lists_by_complex_name;
	}

	public ComplexBuySellLists(ArrayList<String> complex_names) {
		this.complex_names = complex_names;
		this.buy_sell_lists_by_complex_name = new LinkedHashMap<String, LinkedHashMap<String, ArrayList<Integer>>>();
		this.byte_responses = new LinkedHashMap<String, Response<List<byte[]>>>();
	}

	public static ComplexBuySellLists get_buy_sell_lists(
			ArrayList<String> complex_names) {

		ComplexBuySellLists cbsl = new ComplexBuySellLists(complex_names);
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			Pipeline pipe = jedis.pipelined();
			for (String complex_name : cbsl.complex_names) {
				Response<List<byte[]>> resp = pipe
						.hmget(complex_name.getBytes(),
								(StockConstants.complex_values_buy_key_name)
										.getBytes(),
								StockConstants.complex_values_sell_key_name
										.getBytes());
				cbsl.byte_responses.put(complex_name, resp);

			}

			pipe.sync();

			for (final Map.Entry<String, Response<List<byte[]>>> entry : cbsl.byte_responses
					.entrySet()) {

				cbsl.buy_sell_lists_by_complex_name.put(entry.getKey(),
						new LinkedHashMap<String, ArrayList<Integer>>() {
							{
								put(StockConstants.complex_values_buy_key_name,
										CompressUncompress
												.decompress_byte_array_to_integer_list(entry
														.getValue().get()
														.get(0)));
								put(StockConstants.complex_values_sell_key_name,
										CompressUncompress
												.decompress_byte_array_to_integer_list(entry
														.getValue().get()
														.get(1)));
							}
						});

			}

		}

		return cbsl;

	}

}
