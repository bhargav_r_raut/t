package RedisAccessObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.reflect.TypeToken;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import yahoo_finance_historical_scraper.StockConstants;
import Indicators.RedisManager;
import Titan.GroovyStatements;

public class ComplexValues {

	private ArrayList<String> complex_names;

	private ArrayList<String> getComplex_names() {
		return complex_names;
	}

	private LinkedHashMap<String, Response<byte[]>> byte_responses;

	private LinkedHashMap<String, TreeMap<Integer, LinkedHashMap<String, Double>>> complex_values;

	public LinkedHashMap<String, TreeMap<Integer, LinkedHashMap<String, Double>>> getComplex_values() {
		return complex_values;
	}

	private TreeMap<Integer, LinkedHashMap<String, Double>> get_treemap_from_byte_array(
			byte[] barr) {

		String existing_treemap_as_string = new String(barr);

		java.lang.reflect.Type t = new TypeToken<TreeMap<Integer, LinkedHashMap<String, Double>>>() {
		}.getType();

		TreeMap<Integer, LinkedHashMap<String, Double>> tm = GroovyStatements
				.getInstance().gson.fromJson(existing_treemap_as_string, t);

		return tm;
	}

	public ComplexValues(ArrayList<String> complex_names) {
		this.complex_names = complex_names;
		this.complex_values = new LinkedHashMap<String, TreeMap<Integer, LinkedHashMap<String, Double>>>();
		this.byte_responses = new LinkedHashMap<String, Response<byte[]>>();
	}

	public static ComplexValues resolve_complex_values(
			ArrayList<String> complex_names) {
		ComplexValues cobj = new ComplexValues(
				complex_names);
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Pipeline pipe = jedis.pipelined();
			for (String complex_name : cobj.getComplex_names()) {
				cobj.byte_responses.put(complex_name, pipe.hget(
						complex_name.getBytes(),
						StockConstants.complex_values.getBytes()));
			}
			pipe.sync();
			for (Map.Entry<String, Response<byte[]>> entry : cobj.byte_responses
					.entrySet()) {
				TreeMap<Integer, LinkedHashMap<String, Double>> tm = cobj
						.get_treemap_from_byte_array(entry.getValue().get());
				cobj.complex_values.put(entry.getKey(), tm);
			}
		}

		return cobj;
	}

}
