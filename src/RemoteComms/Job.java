package RemoteComms;

import yahoo_finance_historical_scraper.StockConstants;

public class Job {
	
	
	private String job_id;
	
	private String auth_t;
	
	public String getAuth_t() {
		return auth_t;
	}
	public void setAuth_t(String auth_t) {
		this.auth_t = auth_t;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	
		
	public Job(){
		this.auth_t = StockConstants.rauth_t;
	}
	
	public Job(String auth_token){
		this.auth_t = auth_token;
	}
	
	
}

