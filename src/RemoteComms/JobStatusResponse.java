package RemoteComms;

public class JobStatusResponse {
	
	private Integer pending_jobs;
	
	public Integer getPending_jobs() {
		return pending_jobs;
	}

	public void setPending_jobs(Integer pending_jobs) {
		this.pending_jobs = pending_jobs;
	}
	
	private Integer semaphore_count;
	

	public Integer getSemaphore_count() {
		return semaphore_count;
	}

	public void setSemaphore_count(Integer semaphore_count) {
		this.semaphore_count = semaphore_count;
	}

	private String job_id;

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	private Boolean cancel_response;

	public Boolean getCancel_response() {
		return cancel_response;
	}

	public void setCancel_response(Boolean cancel_response) {
		this.cancel_response = cancel_response;
	}

	private String status_response;

	public String getStatus_response() {
		return status_response;
	}

	public void setStatus_response(String status_response) {
		this.status_response = status_response;
	}
	
	
	
	public JobStatusResponse(){
		
	}

}