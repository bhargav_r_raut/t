package RemoteComms;

import java.util.HashMap;

public class StopLossAndPriceChangeUpdateRequest extends Job{

	private HashMap<String,Integer> arrs_names_and_values;

	public HashMap<String, Integer> getArrs_names_and_values() {
		return arrs_names_and_values;
	}

	public void setArrs_names_and_values(
			HashMap<String, Integer> arrs_names_and_values) {
		this.arrs_names_and_values = arrs_names_and_values;
	}

	/***
	 * 
	 * for jackson.
	 */
	public StopLossAndPriceChangeUpdateRequest() {

	}
	


	
}
