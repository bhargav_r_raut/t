import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.tradegenie.tests.DeChunk;

import Titan.ReadWriteTextToFile;

public class RemoteRequestTests {

	public static void debug_remote() throws JsonParseException, JsonMappingException, IOException {
		DeChunk de = new DeChunk();
		de.setArray_name("oil_Amex Oil Index_oil_index_open_open");
		de.setExpected_digest("7559e6f43f60469df81b76bb548ddba36a49db4da3d56738576681410c716641");
		de.setRequired_large_array_index(725400);
		ObjectMapper mapper = new ObjectMapper();
		ReadWriteTextToFile.remote_dechunk_test_response(mapper.writeValueAsString(de),
				"http://localhost:8080/SpringApp/dechunk");
	}
	
	public static void main(String[] args){
		try {
			debug_remote();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
