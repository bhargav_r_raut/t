package RiseAndFallFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.JSONException;

import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import ComplexProcessor.SubIndicatorCalculationSuperClass;
import RedisAccessObject.ComplexValues;
import Titan.GroovyStatements;

public class IndicatorRiseAndFall extends SubIndicatorCalculationSuperClass {

	private String name_of_complex_representing_all_n_day_rise_fall_values;
	/***
	 * 
	 * indicator name
	 * 
	 * 
	 */
	private String indicator_name;

	private String getIndicator_name() {
		return indicator_name;
	}

	private void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	/***
	 * this is accessed by several other classes through ndaycomplexcalculation.
	 * 
	 * 
	 * 
	 */
	public LinkedHashMap<String, Double> pair_differences_map;

	public void setPair_differences_map(
			LinkedHashMap<String, Double> pair_differences_map) {
		this.pair_differences_map = pair_differences_map;
	}

	public LinkedHashMap<String, Double> getPair_differences_map() {
		return pair_differences_map;
	}

	/**
	 * 
	 * contains those read in from redis, and also all new ones that are added
	 * are put into this map and at the end it is added to the complex change
	 * map.
	 * 
	 */
	private TreeMap<Integer, LinkedHashMap<String, Double>> n_day_rise_fall_amounts;

	private TreeMap<Integer, LinkedHashMap<String, Double>> getN_day_rise_fall_amounts() {
		return n_day_rise_fall_amounts;
	}

	/**
	 * need rolling treemaps' need a rolling treemap implementation keep a count
	 * of the values inside it.
	 * 
	 * better to have a hashmap fo objects.
	 * 
	 * 
	 */
	private HashMap<Integer, RollingMap> map_of_rolling_maps;

	private void setMap_of_rolling_maps() {
		this.map_of_rolling_maps = new LinkedHashMap<Integer, RollingMap>();
		for (Integer i : StockConstants.standard_deviation_subindicator_periods) {
			map_of_rolling_maps.put(i, new RollingMap(i));
		}
	}

	private HashMap<Integer, RollingMap> getMap_of_rolling_maps() {
		return map_of_rolling_maps;
	}

	/***
	 * 
	 * this class has to run once and it has to generate the pair change map.
	 * this it has to update and keep ready for use by the other classes.
	 * 
	 * it has to also classify the price change.
	 * 
	 * it has to store the n day differences for each indicator. it can store
	 * this in a hashmap. for eg/
	 * 
	 * it has to also classify the price change as massive, very large -> for
	 * those indicators where the price change is not applicable.
	 * 
	 * 
	 * @param all_available_indicator_values
	 * @param required_day_ids
	 * @param all_day_ids
	 * @param inital_stub
	 */
	public IndicatorRiseAndFall(
			TreeMap<Integer, BigDecimal> all_available_indicator_values,
			ArrayList<Integer> required_day_ids,
			ArrayList<Integer> all_day_ids, String inital_stub,
			String indicator_name) {
		super(all_available_indicator_values, required_day_ids, all_day_ids,
				inital_stub);
		setIndicator_name(indicator_name);
		setPair_differences_map(new LinkedHashMap<String, Double>());
		load_existing_n_day_rise_fall_values();

		// TODO Auto-generated constructor stub
	}

	private void load_existing_n_day_rise_fall_values() {
		ArrayList<String> complex_names = new ArrayList<String>();
		complex_names.add(getInital_stub() + StockConstants.seperator
				+ StockConstants.subindicator_n_day_rise_fall_amounts);

		this.name_of_complex_representing_all_n_day_rise_fall_values = getInital_stub()
				+ StockConstants.seperator
				+ StockConstants.subindicator_n_day_rise_fall_amounts;

		ComplexValues cv = new ComplexValues(complex_names);
		LinkedHashMap<String, TreeMap<Integer, LinkedHashMap<String, Double>>> complex_values = cv
				.getComplex_values();
		// we need to have treemaps for day changes on each day.
		this.n_day_rise_fall_amounts = complex_values.get(complex_names.get(0));

		/****************************
		 * 
		 * 
		 * BIG CAUTION ,REMOVE THIS FOLLLOWING LINE LATER.
		 * 
		 * 
		 * 
		 */
		this.n_day_rise_fall_amounts = new TreeMap<Integer, LinkedHashMap<String, Double>>();
		/********************************
		 * 
		 * 
		 * caution ends.
		 * 
		 * 
		 */

		// now load this into the map of rolling maps.

		setMap_of_rolling_maps();
		// now load all the stuff into it.
		for (Map.Entry<Integer, LinkedHashMap<String, Double>> entry : this.n_day_rise_fall_amounts
				.entrySet()) {
			Integer day_id = entry.getKey();

			for (Map.Entry<String, Double> n_day_change_on_period_on_day : entry
					.getValue().entrySet()) {

				Integer change_on_period = Integer
						.parseInt(n_day_change_on_period_on_day.getKey());
				Double change = n_day_change_on_period_on_day.getValue();
				// add this to the requisite rolling map.
				getMap_of_rolling_maps().get(change_on_period)
						.add_entry_to_map(change, day_id, false);

			}
		}

	}

	/***
	 * generates the pair changes for each of the required day ids, for each
	 * period as defined in the standard deviation periods.
	 * 
	 * @throws JSONException
	 * 
	 * 
	 */
	public void generate_pair_change_map() throws JSONException {

		List<Map.Entry<Integer, BigDecimal>> indicator_values_as_list = new LinkedList<>(
				getAll_available_indicator_values().entrySet());

		Collections.reverse(indicator_values_as_list);

		Integer size_of_all_available_indicator_values = indicator_values_as_list
				.size();
		Integer max_possible_index = size_of_all_available_indicator_values - 1;

		TreeMap<Integer, Double> map_of_required_day_ids = new TreeMap<Integer, Double>();
		for (Integer day_id : getRequired_day_ids()) {
			map_of_required_day_ids.put(day_id, null);
		}

		Integer current_index = 0;

		for (Entry<Integer, Double> indicator_day_id_and_value : indicator_values_as_list) {

			if (map_of_required_day_ids.containsKey(indicator_day_id_and_value
					.getKey())) {

				for (Map.Entry<String, Integer> stddev_names_and_periods_entry : StockConstants.standard_deviation_subindicator_names_and_periods
						.entrySet()) {

					Integer period_of_n_day = stddev_names_and_periods_entry
							.getValue();

					Integer required_index = current_index + period_of_n_day;

					if (!(required_index > max_possible_index)) {

						Double pair_change_percentage = null;

						if (!GroovyStatements.getInstance().indicators_jsonfile
								.getJSONObject(getIndicator_name())
								.getJSONObject(
										StockConstants.not_applicable_to_object)
								.has(StockConstants.representative_subindicator_for_massive_rise_fall)) {

							pair_change_percentage = get_pair_change_percentage(
									indicator_values_as_list.get(current_index)
											.getValue(),
									indicator_values_as_list
											.get(required_index).getValue(),
									true);

							getMap_of_rolling_maps()
									.get(period_of_n_day)
									.add_entry_to_map(
											pair_change_percentage,
											indicator_day_id_and_value.getKey(),
											true);

						}

						if (!GroovyStatements.getInstance().indicators_jsonfile
								.getJSONObject(getIndicator_name())
								.getJSONObject(
										StockConstants.not_applicable_to_object)
								.has(StockConstants.representative_subindicator_for_price_change_percentage)) {

							pair_change_percentage = get_pair_change_percentage(
									indicator_values_as_list.get(current_index)
											.getValue(),
									indicator_values_as_list
											.get(required_index).getValue(),
									false);

							String complex_name = classify_price_change_percentage(
									period_of_n_day, pair_change_percentage);

							if (complex_name != null) {

								add_to_complex_change_map(complex_name,
										indicator_day_id_and_value.getKey());
							}

						}

						update_pair_differences_map(indicator_values_as_list
								.get(required_index).getKey(),
								indicator_values_as_list.get(current_index)
										.getKey(), pair_change_percentage,
								period_of_n_day);

					}

				}

			}

			current_index++;
		}

		/**
		 * add all the complexes from the rolling maps.
		 * 
		 */
		for (Map.Entry<Integer, RollingMap> entry : getMap_of_rolling_maps()
				.entrySet()) {

			RollingMap m = entry.getValue();

			for (Map.Entry<Integer, String> ientry : m
					.getComplex_stubs_detected().entrySet()) {

				add_to_complex_change_map(getInital_stub()
						+ StockConstants.seperator + ientry.getValue(),
						ientry.getKey());

			}
		}

		// add all the n day prices changes also.
		for (Map.Entry<Integer, LinkedHashMap<String, Double>> entry : getN_day_rise_fall_amounts()
				.entrySet()) {
			for (Map.Entry<String, Double> ientry : entry.getValue().entrySet()) {

				add_to_complex_change_map(
						this.name_of_complex_representing_all_n_day_rise_fall_values,
						entry.getKey(), ientry.getValue(), ientry.getKey());

			}
		}
		System.out.println("the complex change mpa is like:" + getComplex_changes_map());

	}

	/***
	 * update the pair differences map.
	 * 
	 * 
	 */
	public void update_pair_differences_map(Integer lesser_day_id,
			Integer greater_day_id, final Double percentage_price_change,
			final Integer period_of_n_day) {
		String map_key = lesser_day_id.toString() + "_"
				+ greater_day_id.toString();
		getPair_differences_map().put(map_key, percentage_price_change);
		if (getN_day_rise_fall_amounts().get(greater_day_id) != null) {
			getN_day_rise_fall_amounts().get(greater_day_id).put(
					period_of_n_day.toString(), percentage_price_change);
		} else {
			getN_day_rise_fall_amounts().put(greater_day_id,
					new LinkedHashMap<String, Double>() {
						{
							put(period_of_n_day.toString(),
									percentage_price_change);
						}
					});
		}
	}

	/***
	 * get the difference in percentage between two prices.
	 * 
	 * 
	 * @param later_value
	 * @param earlier_value
	 * @return
	 */
	private Double get_pair_change_percentage(Double later_value,
			Double earlier_value, Boolean use_larger_value) {

		Double larger_value = null;
		if (earlier_value.equals(0.0) && later_value.equals(0.0)) {

			return 0.0;
		}
		if (use_larger_value) {

			if (Math.abs(later_value) >= Math.abs(earlier_value)) {

				larger_value = later_value;

			} else {

				larger_value = earlier_value;
			}

			return ((later_value - earlier_value) / Math.abs(larger_value)) * 100.0;

		} else {
			return ((later_value - earlier_value) / Math.abs(earlier_value)) * 100.0;
		}
	}

	/**
	 * only for open high low close we need to calculate and classify the
	 * percentage change difference we need to do this for each required day id
	 * for all the periods.
	 * 
	 * 
	 * counter -> the number of days over which there was this price change.
	 * 
	 * price change -> the actual price change in percentage
	 * 
	 */
	protected String classify_price_change_percentage(Integer day_amount,
			Double price_change) {

		Double absolute_change_amount = get_rounded_price_change(price_change);

		Double max_price_change_for_this_period = StockConstants.price_change_thresholds_based_on_periods
				.get(day_amount);

		String complex_name = null;
		Integer rise_or_fall = null;
		if (price_change > 0) {
			rise_or_fall = 0;
		} else {
			rise_or_fall = 1;

		}

		if (absolute_change_amount == 0.0) {
			complex_name = getInital_stub()
					+ StockConstants.subindicator
					+ StockConstants.seperator
					+ StockConstants.unchanged
					+ StockConstants.in
					+ StockConstants.period_in_integer_period_in_words_map
							.get(day_amount);

			return complex_name;
		}

		complex_name = getInital_stub() + StockConstants.subindicator
				+ StockConstants.rise_or_fall[rise_or_fall];

		if (absolute_change_amount > max_price_change_for_this_period) {
			complex_name = complex_name + StockConstants.more_than;
			absolute_change_amount = max_price_change_for_this_period;
		}

		complex_name = complex_name
				+ StockConstants.seperator
				+ absolute_change_amount.toString()
				+ StockConstants.percent
				+ StockConstants.in
				+ StockConstants.period_in_integer_period_in_words_map
						.get(day_amount);

		return complex_name;

	}

	protected Double get_rounded_price_change(Double price_change) {

		if (price_change == 0.0) {
			return price_change;
		}
		return CompressUncompress
				.find_nearest(
						Math.abs(price_change),
						StockConstants.rise_or_fall_amounts_for_price_change_percentage_diff);

	}

}
