package RiseAndFallFunctions;

import java.util.Map;

import org.json.JSONException;

import SMAandZeroFunctions.Test;
import Titan.GroovyStatements;

public class RiseAndFallFunctionsTest extends Test {

	public RiseAndFallFunctionsTest(Double min, Double max) {
		super(min, max);
		// TODO Auto-generated constructor stub
	}

	public RiseAndFallFunctionsTest() {
		super();
	}

	public static void main(String[] args) {
		GroovyStatements.initializeGroovyStatements(null);
		RiseAndFallFunctionsTest rt = new RiseAndFallFunctionsTest(-23.0,23.0);
		
		rt.getAll_available_indicator_values().clear();
		rt.getAll_available_indicator_values().put(10, 0.0);
		rt.getAll_available_indicator_values().put(11, 0.0);
		rt.getAll_available_indicator_values().put(12, 0.0);
		rt.getAll_available_indicator_values().put(13, 0.0);
		rt.getAll_available_indicator_values().put(14, 0.0);
		rt.getAll_available_indicator_values().put(15, 0.0);
		rt.getAll_available_indicator_values().put(16, 0.0);
		rt.getAll_available_indicator_values().put(17, 0.0);
		// rt.test_rolling_map_sort();
		// rt.test_rolling_map_basic();
		// rt.test_rolling_map_zero_values();
		rt.test_basic_functioning_of_indicator_rise_fall_class();
	}

	/***
	 * test whether the rolling map sorts as per requirment. send in 1000
	 * numbers all the same and see what happens.
	 * 
	 */
	public void test_rolling_map_sort() {
		RollingMap rm = new RollingMap(10);
		// System.out.println("indicator values size:" +
		// getAll_available_indicator_values().size());

		getAll_available_indicator_values().clear();
		getAll_available_indicator_values().put(10, 12.229);
		getAll_available_indicator_values().put(12, 12.225);

		for (Map.Entry<Integer, Double> entry : getAll_available_indicator_values()
				.entrySet()) {
			rm.add_entry_to_map(entry.getValue(), entry.getKey(), true);
		}

		if (rm.getRtree().size() == 1) {
			System.out.println("rolling map sort test passed");
			return;
		}
		System.out.println("rolling map sort test failed.");

	}

	/**
	 * test whether the rolling map basic functioning works
	 * 
	 * 
	 */
	public void test_rolling_map_basic() {

		// okay so here we check if it correctly gets the percentage days above
		// and below.

		RollingMap rm = new RollingMap(3);
		for (Map.Entry<Integer, Double> entry : getAll_available_indicator_values()
				.entrySet()) {
			rm.add_entry_to_map(entry.getValue(), entry.getKey(), true);
		}

	}

	/***
	 * tested what happens to rolling map with zero values.
	 * 
	 * 
	 */
	public void test_rolling_map_zero_values() {
		getAll_available_indicator_values().clear();
		for (int i = 10; i > -10; i--) {
			getAll_available_indicator_values().put(i, (double) i);
		}
		RollingMap rm = new RollingMap(3);
		for (Map.Entry<Integer, Double> entry : getAll_available_indicator_values()
				.entrySet()) {
			rm.add_entry_to_map(entry.getValue(), entry.getKey(), true);
		}

	}

	/**
	 * test whether the rolling map correctly reads in existing rise fall
	 * amounts.
	 * 
	 * 
	 */
	public void test_rolling_map_loading_of_existing_rise_fall_amounts() {

	}

	/**
	 * test basic functioning of the indicatorriseandfall class go step wise.
	 * 
	 * 
	 */
	public void test_basic_functioning_of_indicator_rise_fall_class() {
		IndicatorRiseAndFall irf = new IndicatorRiseAndFall(
				getAll_available_indicator_values(), getRequired_day_ids(),
				getAll_day_ids(), getInitial_stub(),
				"close");
		
		try {
			irf.generate_pair_change_map();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
