package RiseAndFallFunctions;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import yahoo_finance_historical_scraper.StockConstants;

public class RollingMap {

	private TreeMap<Double, Integer> rtree;

	public TreeMap<Double, Integer> getRtree() {
		return rtree;
	}

	private TreeMap<Integer, Double> day_ordered_map;
	private Integer map_size;
	private Integer map_created_for_period;
	private LinkedHashMap<Integer, String> complex_stubs_detected;

	public LinkedHashMap<Integer, String> getComplex_stubs_detected() {
		return complex_stubs_detected;
	}

	public RollingMap(Integer period) {
		this.complex_stubs_detected = new LinkedHashMap<Integer, String>();
		this.map_size = 0;
		this.map_created_for_period = period;
		this.day_ordered_map = new TreeMap<Integer, Double>();
		this.rtree = new TreeMap<Double, Integer>(new Comparator<Double>() {
			public int compare(Double o1, Double o2) {
				if (Math.abs(o1 - o2) <= StockConstants.epsilon_for_rollling_map) {
					return 0;
				} else {
					return o1.compareTo(o2);
				}
			}
		});
	}

	public void add_entry_to_map(Double value, Integer day_id,
			Boolean classify_complex) {

		if (this.map_size >= (StockConstants.max_map_size)) {

			Integer least_recent_day_id = day_ordered_map.firstKey();
			Integer most_recent_day_id = day_ordered_map.lastKey();

			Double value_at_most_recent_day_id = day_ordered_map
					.get(most_recent_day_id);
			Double value_at_least_recent_day_id = day_ordered_map
					.get(least_recent_day_id);

			if (value_at_most_recent_day_id > 0.0) {

				NavigableMap<Double, Integer> all_values_greater_than_zero = this.rtree
						.tailMap(0.0, false);

				Double size_of_all_values_greater_than_zero = (double) all_values_greater_than_zero
						.size();

				NavigableMap<Double, Integer> value_is_greater_than_map = this.rtree
						.subMap(0.0, false, value_at_most_recent_day_id, false);

				Double size_of_value_is_greater_than_map = (double) value_is_greater_than_map
						.size();

				Double value_is_greater_than_percentage = (size_of_value_is_greater_than_map / size_of_all_values_greater_than_zero) * 100.0;

				if (classify_complex) {
					classify_percentage_greater_than_less_than(
							(int) Math.round(value_is_greater_than_percentage),
							most_recent_day_id, value_at_most_recent_day_id);
				}
			} else if (value_at_most_recent_day_id < 0.0) {

				NavigableMap<Double, Integer> all_values_less_than_zero = this.rtree
						.headMap(0.0, false);

				Double size_of_all_values_less_than_zero = (double) all_values_less_than_zero
						.size();

				NavigableMap<Double, Integer> values_is_less_than_map = this.rtree
						.subMap(value_at_most_recent_day_id, false, 0.0, false);

				Double size_of_values_is_less_than_map = (double) values_is_less_than_map
						.size();

				Double value_is_less_than_percentage = (size_of_values_is_less_than_map / size_of_all_values_less_than_zero) * 100.0;

				if (classify_complex) {
					classify_percentage_greater_than_less_than(
							(int) Math.round(value_is_less_than_percentage),
							most_recent_day_id, value_at_most_recent_day_id);
				}
			} else {
				System.out.println("change is zero, so nothign is done.");
			}

			// remove most recent added entry from both the maps.

			this.rtree.remove(value_at_most_recent_day_id);
			this.day_ordered_map.remove(most_recent_day_id);

		} else {
			// System.out
			// .println("not yet crossed the max map size: and curr size is:"
			// + this.map_size);
		}

		this.rtree.put(value, day_id);
		this.day_ordered_map.put(day_id, value);

		this.map_size++;
	}

	/***************
	 * 
	 * FUNCTIONS TO CLASSIFY THE N DAY RISE FALL
	 * 
	 * 
	 * 
	 * @param above_or_below_x_percentage_of_values
	 */
	private void classify_percentage_greater_than_less_than(
			Integer above_or_below_x_percentage_of_values,
			Integer current_day_id,
			Double basic_percentage_change_positive_or_negative) {

		for (Map.Entry<Integer, String> range_correlation : StockConstants.range_correlation_for_n_session_rise_fall
				.entrySet()) {

			if (Math.abs(above_or_below_x_percentage_of_values) >= range_correlation
					.getKey()) {

				String rise_or_fall = null;

				if (basic_percentage_change_positive_or_negative > 0) {
					rise_or_fall = "rise";
				} else {
					rise_or_fall = "fall";
				}

				String complex_stub_name = StockConstants.template_for_n_session_rise_or_fall
						.replace("range_correlate",
								range_correlation.getValue() + "_")
						.replace("_n_", this.map_created_for_period + "_")
						.replace("rise_or_fall", rise_or_fall);
				this.complex_stubs_detected.put(current_day_id,
						complex_stub_name);

				return;

			}

		}
	}

}
