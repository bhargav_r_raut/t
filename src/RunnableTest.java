import java.util.concurrent.ScheduledExecutorService;


public class RunnableTest implements Runnable {
		
	private ScheduledExecutorService ser;
	private static Integer run_counts;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(run_counts > 4){
			System.out.println("" +
					"run counts is: " + run_counts + ", now shutting down:");
			ser.shutdown();
			System.out.println("we can live");
		}
		else{
			System.out.println("run counts is:" + run_counts);
		}
		run_counts++;
	}

	public RunnableTest(ScheduledExecutorService ser){
		this.ser = ser;
		this.run_counts = 0;
	}

}
