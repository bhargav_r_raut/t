package SMAandZeroFunctions;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import ComplexProcessor.SubIndicatorCalculationSuperClass;
import RedisAccessObject.ComplexBuySellLists;
import RedisAccessObject.ComplexValues;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import ComplexProcessor.ComplexChange;

public class IndicatorSignalLineAndSmaCalculations extends
		SubIndicatorCalculationSuperClass {

	private RollingQueue rqueue;

	public RollingQueue getRqueue() {
		return rqueue;
	}

	private ArrayList<String> complex_containing_subindicator_sma_values;

	private ComplexBuySellLists buy_sell_lists_of_subindicator_and_zero_line_cross_values;

	/**
	 * gets the buy sell lists for the cross above / below for this indicator.
	 * 
	 * this is used to detect how many days were spent above or below when a
	 * cross is detected.
	 * 
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 */
	private ComplexBuySellLists generate_complex_buy_sell_lists()
			throws JSONException, IOException {

		ArrayList<String> names_of_complexes_holding_buy_sell_lists_of_all_sma_crosses = new ArrayList<String>();

		@SuppressWarnings("static-access")
		JSONObject subindicators_without_stddev_and_rise_fall_groups = new JSONObject(
				ReadWriteTextToFile.getInstance().readFile(
						StockConstants.subindicators_jsontextfile,
						TitanConstants.ENCODING));

		JSONArray groups = (JSONArray) subindicators_without_stddev_and_rise_fall_groups
				.get("group");

		Pattern p = Pattern
				.compile("subindicator(_\\d+_session_sma)?_crosses_(above|below)_\\d+_session_sma");
		Pattern z = Pattern.compile("goes_(above|below)_(zero|signal)_line");

		// add the zero line cross patterns
		// goes above or goes below zero line.

		for (int i = 0; i < groups.length(); i++) {
			JSONObject group = groups.getJSONObject(i);
			JSONArray names = group.getJSONArray(StockConstants.names);
			Boolean add_group = false;
			for (int k = 0; k < names.length(); k++) {
				String name = names.getString(k);
				Matcher m = p.matcher(name);
				Matcher m1 = z.matcher(name);
				if (m.find() || m1.find()) {
					add_group = true;
					break;
				}
			}
			if (add_group) {
				for (int k = 0; k < names.length(); k++) {
					String name = names.getString(k);
					names_of_complexes_holding_buy_sell_lists_of_all_sma_crosses
							.add(getInital_stub() + StockConstants.seperator
									+ name);
				}
			}
		}

		ComplexBuySellLists cbsl = ComplexBuySellLists
				.get_buy_sell_lists(names_of_complexes_holding_buy_sell_lists_of_all_sma_crosses);

		return cbsl;
	}

	public IndicatorSignalLineAndSmaCalculations(
			final TreeMap<Integer, BigDecimal> all_available_indicator_values,
			final ArrayList<Integer> required_day_ids,
			final ArrayList<Integer> all_day_ids, final String inital_stub,
			final Integer[] periods_for_the_rolling_queue)
			throws JSONException, IOException {

		super(all_available_indicator_values, required_day_ids, all_day_ids,
				inital_stub);

		/***
		 * static initializer.
		 */

		this.complex_containing_subindicator_sma_values = new ArrayList<String>(
				Arrays.asList(inital_stub + StockConstants.seperator
						+ StockConstants.subindicator_sma_values));

		this.buy_sell_lists_of_subindicator_and_zero_line_cross_values = generate_complex_buy_sell_lists();

		// instantiate and override a rolling queue as needed in an anonymous
		// inner class.
		this.rqueue = new RollingQueue(periods_for_the_rolling_queue) {

			@Override
			public void get_existing_results_map() {

				ComplexValues cobj = ComplexValues
						.resolve_complex_values(complex_containing_subindicator_sma_values);

				for (Map.Entry<String, TreeMap<Integer, LinkedHashMap<String, Double>>> entry : cobj
						.getComplex_values().entrySet()) {

					// there is always only one key(complex_name) so we iterate
					// the value, and
					// fill up the existing results
					// inside the rolling queue with it.

					for (Map.Entry<Integer, LinkedHashMap<String, Double>> inner_entry : entry
							.getValue().entrySet()) {

						Integer day_id = inner_entry.getKey();

						Integer rank = 0;

						LinkedHashMap<Integer, SmaCalculationResultObject> existing_smas_on_this_day_id = new LinkedHashMap<Integer, SmaCalculationResultObject>();

						/**
						 * for the zero line cross we need to have the sum of
						 * all the smas and then we take the average by dividing
						 * by the number of the smas in the sorted map. we pass
						 * that into the zero cross class.
						 * 
						 */
						// Double sum_of_all_smas = 0.0;

						for (Map.Entry<String, Double> sma_value_for_day_id : inner_entry
								.getValue().entrySet()) {

							SmaCalculationResultObject smar = new SmaCalculationResultObject(
									Integer.parseInt(sma_value_for_day_id
											.getKey()),
									sma_value_for_day_id.getValue());
							smar = update_rank_and_period_and_period_combination(
									smar, getLast_day_id_in_results(), rank,
									true);

							// sum_of_all_smas += smar.getAverage();
							existing_smas_on_this_day_id.put(smar.getPeriod(),
									smar);

							rank++;
						}

						getResults().put(day_id, existing_smas_on_this_day_id);

						setLast_day_id_in_results(day_id);
					}
				}

			}

			@Override
			public void post_process(Integer day_id,
					Integer last_day_id_processed) {

				// first sort it.
				// System.out.println("day id is:" + day_id);
				// System.out.println("last day id processed is:"
				// + last_day_id_processed);

				// try {
				// System.in.read();
				// } catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				LinkedHashMap<Integer, SmaCalculationResultObject> result_at_day_id = getResults()
						.get(day_id);

				// System.out.println("the result map is:" + result_at_day_id);
				// try {
				// System.in.read();
				// } catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				/***
				 * sort as per the sorting algorithm defined in
				 * smacalculationresultobject.
				 * 
				 * basically sorted in descending order.
				 * 
				 * and
				 * 
				 * a lower index means a higher rank.
				 * 
				 * 
				 */

				LinkedHashMap<Integer, SmaCalculationResultObject> sorted_map_at_day_id = get_sorted_map(result_at_day_id);

				 System.out.println("\n \n \n starting new day:");
				 System.out.println("curr day id is :" + day_id);
				 System.out.println("this is the sorted map");
				 System.out.println(sorted_map_at_day_id.toString());
				 
				 try {
					System.in.read();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
				// update the rank and calculate all the required things.
				Integer index = 0;
				// well need another hashmap containing only the rank
				// combination and the period.
				Double sum_of_all_smas = 0.0;
				/**
				 * the value of the subindicator on the day previous to this. so
				 * we take the 1 day sma , since that is the same thing. this is
				 * used in the zero cross.
				 * 
				 */
				Double previous_day_subindicator_value = null;
				LinkedHashMap<Integer, Integer> period_and_rank_combination_integer_code = new LinkedHashMap<Integer, Integer>();

				for (Map.Entry<Integer, SmaCalculationResultObject> entry : sorted_map_at_day_id
						.entrySet()) {
					SmaCalculationResultObject smacarr = entry.getValue();

					/***
					 * basic provision of this function is to calculate 1.
					 * update the rank and the rank combination and the rank
					 * combination integer code of the combination. 2. no other
					 * function found at this point, just provided a hook
					 * incase. the false parameter is just for that, i.e if you
					 * want to do something more, other than point 1, then pass
					 * in false.
					 */
					// System.out.println("\n \n entering update rank.");
					SmaCalculationResultObject smacarr_new = update_rank_and_period_and_period_combination(
							smacarr, last_day_id_processed, index, false);
					// System.out.println("this is the smacarr new: " +
					// smacarr_new);
					// try {
					// System.in.read();
					// } catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					// }

					entry.setValue(smacarr_new);
					// we can only have a rank combination if there was a value
					// for the
					// previous day.
					if (smacarr_new.getPrevious_day_value_for_this_sma() != null) {
						period_and_rank_combination_integer_code
								.put(smacarr_new.getPeriod(),
										smacarr_new
												.getRank_combination_binary_search_integer_code());
						// System.out.println("the period rank and rank combination map looks like:");
						// System.out.println(period_and_rank_combination_integer_code.toString());
						// try {
						// System.in.read();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}
					/**
					 * for the purpose of zero cross these two values are
					 * updated.
					 * 
					 */
					sum_of_all_smas += smacarr_new.getAverage();

					index++;
				}
				
				
				
				
				/***
				 * 
				 * added this sorted map to the results.
				 * 
				 */

				getResults().put(day_id, sorted_map_at_day_id);

				/**
				 * calculating and the zero cross complexes.
				 * 
				 * 
				 */
				
				if (getResults().get(day_id).get(1)
						.getPrevious_day_value_for_this_sma() != null) {

					System.out.println("\n \n \n");
					System.out.println("before sending into the zero cross, the complex change map size is:");
					System.out.println(getComplex_changes_map().size());
					System.out.println("these are the complexes");
					System.out.println(GroovyStatements
							.getInstance()
							.gson.toJson(getComplex_changes_map().keySet()));
					
					ResolveZeroCross rzcross = new ResolveZeroCross(
							sorted_map_at_day_id.get(1)
									.getPrevious_day_value_for_this_sma(),
							sorted_map_at_day_id.get(1).getAverage(),
							sum_of_all_smas / getResults().get(day_id).size(),
							day_id,
							getAll_available_indicator_values(),
							buy_sell_lists_of_subindicator_and_zero_line_cross_values,
							getComplex_changes_map(), getInital_stub());

					rzcross.resolve_cross();

					rzcross.resolve_days_above_or_below_zero_line_and_cross();

					for (String complex : rzcross
							.getComplexes_detected_on_this_day_id()) {
						System.out.println("added to complex change map this complex");
						System.out.println(complex + " and the day id is: " + day_id);
						add_to_complex_change_map(complex, day_id);
					}
					

				}
				else{
					System.out.println("the sorted map at this day id does not contain a previous day value for one.");
					try {
						System.in.read();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// okay now retrieve the last day of the required previous
				// complex, and get the adjusted days passed since then.

				/**
				 * CALCULTING SUBINDICATOR ABOVE OR BELOW SIGNAL LINE BY AMOUNT.
				 * 
				 * 
				 */
				String subindicator_above_or_below_signal_line_complex_name = calculate_subindicator_difference_from_signal_line(sorted_map_at_day_id);
				if (subindicator_above_or_below_signal_line_complex_name != null) {
					add_to_complex_change_map(
							subindicator_above_or_below_signal_line_complex_name,
							day_id);
				}

				/***
				 * 
				 * CALCULATING COMPLEXES CROSSES AND SUBINDICATOR ABOVE N DAY
				 * SMA OR BELOW N DAY SMA FOR HOW MANY DAYS.
				 * 
				 * in the following loop we take each period , and combine it
				 * with all the other periods. so for each such pair we check if
				 * either crosses above the other. so we first build one
				 * arraylist which contains these pairs, and only process a
				 * given pair if it has not already been done.
				 * 
				 * if yes - (one crosses above the other) - post to relevant
				 * complex - that it occured. - then forward to function above
				 * called day_above_below
				 * 
				 * if no - (it does not cross) - determine which one is greater
				 * than the other. - forward to function above called
				 * day_above_below.
				 * 
				 * 
				 */
				Integer entry_rank = 0;

				// System.out.println(period_and_rank_combination_integer_code
				// .toString());
				// try {
				// System.in.read();
				// } catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				// try {
				// System.in.read();
				// } catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				ResolveCross rcr = new ResolveCross(
						day_id,
						getInital_stub(),
						buy_sell_lists_of_subindicator_and_zero_line_cross_values,
						getComplex_changes_map(),
						all_available_indicator_values);

				for (Map.Entry<Integer, Integer> entry : period_and_rank_combination_integer_code
						.entrySet()) {
					Integer reentry_rank = 0;
					for (Map.Entry<Integer, Integer> re_entry : period_and_rank_combination_integer_code
							.entrySet()) {
						if (re_entry.getKey() != entry.getKey()) {

							// now check if entry.getValue lies in the crosses
							String rank_combination_of_re_entry = StockConstants.cross_detection_string_pairs_for_binary_search_integer_as_key
									.get(re_entry.getValue());

							Integer integer_code_of_rank_combination_of_re_entry = re_entry
									.getValue();

							String rank_combination_of_entry = StockConstants.cross_detection_string_pairs_for_binary_search_integer_as_key
									.get(entry.getValue());

							Integer integer_code_of_rank_combination_of_entry = entry
									.getValue();

							int[] ranks_crossing_above_re_entry_rank_combination = StockConstants.rank_combination_and_array_of_ranks_which_cross_above_it
									.get(rank_combination_of_re_entry);

							int[] ranks_crossing_above_entry_rank_combination = StockConstants.rank_combination_and_array_of_ranks_which_cross_above_it
									.get(rank_combination_of_entry);

							Boolean pair_was_processed = rcr
									.add_pair_to_list_of_pairs_already_processed(
											integer_code_of_rank_combination_of_entry,
											entry.getKey(),
											ranks_crossing_above_entry_rank_combination,
											entry_rank,
											integer_code_of_rank_combination_of_re_entry,
											re_entry.getKey(),
											ranks_crossing_above_re_entry_rank_combination,
											reentry_rank);
							// now add all the newly added complexes to the
							// complex change map.
							// System.out.println("day id is :" + day_id);
							// System.out.println("complexes are:");
							// System.out.println("entry is:" + entry.getKey());
							// System.out.println("reentry is:"
							// + re_entry.getKey());
							// System.out.println(rcr
							// .getComplexes_detected_on_this_day_id());
							// try {
							// System.in.read();
							// } catch (IOException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							// }

							// try {
							// System.in.read();
							// } catch (IOException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							// }
						}
						reentry_rank++;
					}
					entry_rank++;
				}

				for (String complex_got_on_this_day_id : rcr
						.getComplexes_detected_on_this_day_id()) {
					// remove the complex added from the rcr
					add_to_complex_change_map(complex_got_on_this_day_id,
							day_id);

				}

				setLast_day_id_in_results(day_id);

			}

			public SmaCalculationResultObject update_rank_and_period_and_period_combination(
					SmaCalculationResultObject smacarr,
					Integer last_day_id_processed, Integer index,
					Boolean only_update_rank) {

				smacarr.setRank(index);

				Integer period_of_smacarr = smacarr.getPeriod();

				// System.out.println("last day id processed is:" +
				// last_day_id_processed);

				// if(last_day_id_processed!=null){
				// System.out.println
				// ("sma calculation object for this period on that previous day:"
				// +
				// getResults().get(last_day_id_processed).get(period_of_smacarr));
				// }

				if (last_day_id_processed != null
						&& getResults().get(last_day_id_processed).get(
								period_of_smacarr) != null) {
					Integer previous_rank = getResults()
							.get(last_day_id_processed).get(period_of_smacarr)
							.getRank();
					Double previous_day_value = getResults()
							.get(last_day_id_processed).get(period_of_smacarr)
							.getAverage();
					String rank_combination = previous_rank.toString() + "_"
							+ index.toString();
					smacarr.setRank_combination(rank_combination);
					smacarr.setPrevious_day_value_for_this_sma(previous_day_value);
				}

				// System.out.println("the new smacarr is: " + smacarr);

				return smacarr;
			}

			/***
			 * returns the name of the complex which is the diffrence btw signal
			 * line and subidnicator.
			 * 
			 * @param ln
			 * @return
			 */
			public String calculate_subindicator_difference_from_signal_line(
					LinkedHashMap<Integer, SmaCalculationResultObject> ln) {
				Double subindicator_value = ln.get(1).getAverage();

				// in case there were not enough days for the signal line
				// calculations.
				if (ln.get(StockConstants.days_considered_for_signal_line_calculation) == null) {
					return null;
				}
				Double signal_line_value = ln
						.get(StockConstants.days_considered_for_signal_line_calculation)
						.getAverage();
				Double diff = subindicator_value - signal_line_value;
				// measured as percentage of signal value.
				Double percentage = (diff / subindicator_value) * 100.0;
				// we need to find the nearest.
				Double nearest_find = CompressUncompress
						.find_nearest(
								percentage,
								StockConstants.subindicator_above_or_below_signal_line_amounts);

				if (nearest_find > 0) {
					return StockConstants.subindicator
							+ StockConstants.seperator
							+ nearest_find.toString()
							+ StockConstants.seperator + StockConstants.above
							+ StockConstants.seperator
							+ StockConstants.signal_line;
				} else if (nearest_find < 0) {
					return StockConstants.subindicator
							+ StockConstants.seperator
							+ nearest_find.toString()
							+ StockConstants.seperator + StockConstants.below
							+ StockConstants.seperator
							+ StockConstants.signal_line;
				} else {
					// in this case no complex is detected.
					return null;
				}

			}

		};

		// add all the indicator values of the required day ids ot the queue.

		for (Integer required_day_id : required_day_ids) {
			try {
				getRqueue().add_to_all_queues(required_day_id,
						all_available_indicator_values.get(required_day_id));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// add the sma_values from the queue to the complex change map.

		for (Map.Entry<Integer, LinkedHashMap<Integer, SmaCalculationResultObject>> entry : getRqueue()
				.getResults().entrySet()) {
			Integer day_id = entry.getKey();
			for (Map.Entry<Integer, SmaCalculationResultObject> sma_entry : entry
					.getValue().entrySet()) {
				add_to_complex_change_map(
						StockConstants.subindicator_sma_values, day_id,
						sma_entry.getValue().getAverage(), sma_entry.getValue()
								.getPeriod().toString());
			}
		}

	}
}
