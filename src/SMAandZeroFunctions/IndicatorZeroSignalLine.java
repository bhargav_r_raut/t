package SMAandZeroFunctions;


import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;
import Indicators.SubIndicatorCalculationSuperClass;

import org.elasticsearch.action.count.CountResponse;

import yahoo_finance_historical_scraper.Comparators;
import yahoo_finance_historical_scraper.StockConstants;
import elasticSearchNative.LocalEs;

public class IndicatorZeroSignalLine extends SubIndicatorCalculationSuperClass {
	private String indicator_name;

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	private TreeMap<Integer, Double> copy_of_indicator_values_map;

	public TreeMap<Integer, Double> getCopy_of_indicator_values_map() {
		return copy_of_indicator_values_map;
	}

	public void setCopy_of_indicator_values_map(
			TreeMap<Integer, Double> copy_of_indicator_values_map) {
		this.copy_of_indicator_values_map = copy_of_indicator_values_map;
	}

	public IndicatorZeroSignalLine(
			TreeMap<Integer, Double> all_available_indicator_values,
			ArrayList<Integer> required_day_ids,
			ArrayList<Integer> all_day_ids, String in_stub,
			String indicator_name) {

		super(all_available_indicator_values, required_day_ids, all_day_ids,
				in_stub);
		setCopy_of_indicator_values_map(getAll_available_indicator_values());
	}

	public void check_zero_signal_line_applicability_for_indicator() {
		/**
		 * first check for zero line.
		 * 
		 */
		CountResponse response = LocalEs
				.getInstance()
				.prepareCount("tradegenie_titan")
				.setTypes("indicator")
				.setQuery(
						boolQuery()
								.must(termQuery("indicator_name",
										indicator_name))
								.must(nestedQuery(
										"applicable_subindicators",
										boolQuery()
												.must(termQuery(
														"applicable_subindicators.applicable_subindicator_name",
														StockConstants.string_to_test_if_indicator_supports_zero_line)))))
				.execute().actionGet();

		if (response.getCount() > 0) {
			// so it is applicable to zero.
			// so we should execute the code for all the zero line calculations.
			// so first we have to see for each day if the previous day is
			// negative and today is positive.
			// then we say that there has been a zero line crossover.
			// so we wanna check how many days its been above the zero line.
			// we just need to count the last day on which it was
			// we just need all the negative values.
			// then sort by day id.
			// we get the last negative day id.
			// then we know how many days passed since then.
			Iterator<Entry<Integer, Double>> iter = getCopy_of_indicator_values_map().descendingMap()
					.entrySet().iterator();
			
			while (iter.hasNext()) {
				Entry<Integer, Double> entry = iter.next();
				
				//suppose that this one is positive.
				//then we have to add it to a rolling map.
				//and the minute we find a negative , we store the number of days.
				//so suppose we start with negative
				//then we go back
				//then we keep two maps
				//one positive and one negative.
				//we have to have a shadow indicator called signal line.
				//it should be calculated when the indicator is calculated.
				//it should be updated to redis when the indicator values are updated to redis.
				//it should follow 
				
				if(Comparators.is_double_positive(entry.getValue())){
					iter.remove();
				}
			}
			
		}

	}

}
