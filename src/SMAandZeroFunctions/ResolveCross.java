package SMAandZeroFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TreeMap;

import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import Indicators.ComplexChange;
import RedisAccessObject.ComplexBuySellLists;

public class ResolveCross {

	private HashMap<String, ComplexChange> complex_change_map;
	private Integer day_id_being_processed;
	private ComplexBuySellLists cbsl;
	private String initial_stub;
	private TreeMap<Integer, Double> all_available_indicator_values;

	public String getInitial_stub() {
		return initial_stub;
	}

	public void setInitial_stub(String initial_stub) {
		this.initial_stub = initial_stub;
	}

	private Set<String> complexes_detected_on_this_day_id;

	public Set<String> getComplexes_detected_on_this_day_id() {
		return complexes_detected_on_this_day_id;
	}

	private ArrayList<String> list_of_pairs_already_processed;

	public Integer get_days_passed(String opposite_name) {
		// so basically we have to find when was the last time the opposite
		// crossed.
		// if it exists, otherwise.
		// there can also be realtime nausea.
		// so if there was a cross in the past then how do i assess it now
		// will have to somehow update this crap.
		// check in both the cbsl and the complex change map.
		LinkedHashMap<String, ArrayList<Integer>> buy_sell_lists_for_opposite_name = cbsl
				.getBuy_sell_lists_by_complex_name().get(opposite_name);
		// get the last date.
		Integer buy_lists_last_date = null;

		// System.out.println("this is the name in question");
		// System.out.println(opposite_name);

		// get the buy list for it and get the last date.
		ArrayList<Integer> buy_dates = buy_sell_lists_for_opposite_name
				.get(StockConstants.complex_values_buy_key_name);
		if (!buy_dates.isEmpty()) {
			buy_lists_last_date = buy_dates.get(buy_dates.size() - 1);
		}

		// now get the last date from the complex change map.
		// basically this map holds all the values that are being calculated in
		// teh current calculation
		// especially in the indicatorsignal line calculation after the sma
		// calculations are done for
		// each day in the rolling queue we add all the complexes that are got,
		// to the complex change map.
		// then we pass in the complex change map into this resolve cross class
		if (complex_change_map.get(opposite_name) != null) {
			// then we get the last date in it.
			ArrayList<Integer> complex_change_map_day_ids_for_this_complex = complex_change_map
					.get(opposite_name).getDay_ids();
			Integer last_day_in_complex_change_map = complex_change_map_day_ids_for_this_complex
					.get(complex_change_map_day_ids_for_this_complex.size() - 1);
			if (buy_lists_last_date != null) {
				if (buy_lists_last_date < last_day_in_complex_change_map) {
					buy_lists_last_date = last_day_in_complex_change_map;
				} else {
					buy_lists_last_date = last_day_in_complex_change_map;
				}
			}
		}

		// if you cannot find a point where there has been a cross ignore it.
		if (buy_lists_last_date != null) {
			// now calculate the number of days since.
			// we should get real number of sessions
			// not just minus.
			// in both cases,
			// we are calculating days since.
			// so we need to get a headmap.
			// of the indicator values linkedhashmap between these two day ids.
			Integer days_passed = CompressUncompress
					.get_total_number_of_actual_sessions_between_two_day_ids(
							this.day_id_being_processed, buy_lists_last_date,
							this.all_available_indicator_values);
			// now we make the names of the complexes.
			// find closest day to this out of all the periods.
			return CompressUncompress
					.find_nearest(
							days_passed.doubleValue(),
							CompressUncompress
									.integer_array_to_double_array(StockConstants.standard_deviation_subindicator_periods))
					.intValue();
		}
		return null;
	}

	/***
	 * 
	 * 
	 * 
	 * @param integer_code_of_first_period
	 * @param period_of_first_period
	 * @param ranks_which_cross_above_first_period
	 * @param basic_rank_of_first_period
	 * @param integer_code_of_second_period
	 * @param period_of_second_period
	 * @param ranks_which_cross_above_second_period
	 * @param basic_rank_of_second_period
	 * @return - true :if the given pair is processed, implying that it was not
	 *         already in the list of pairs on that day id. - false:if the given
	 *         pair is not processed, because it is already in the pairs that
	 *         were processed on that day id. - this boolean is used outside in
	 *         the indicatorsignalline class to add the complexes on this day id
	 *         to the complex change map. basically this is so that we only add
	 *         the calculated complex once.
	 */
	public Boolean add_pair_to_list_of_pairs_already_processed(
			Integer integer_code_of_first_period,
			Integer period_of_first_period,
			int[] ranks_which_cross_above_first_period,
			Integer basic_rank_of_first_period,

			Integer integer_code_of_second_period,
			Integer period_of_second_period,
			int[] ranks_which_cross_above_second_period,
			Integer basic_rank_of_second_period) {

		//System.out.println("integer code of first period: "
		//		+ integer_code_of_first_period);
		//System.out.println("period of first period:" + period_of_first_period);
		//System.out.println("ranks which cross above first period:");
		//for (int i = 0; i < ranks_which_cross_above_first_period.length; i++) {
		//	System.out.println("rank -> "
		//			+ ranks_which_cross_above_first_period[i]);
		//}
		//System.out.println("basic rank of first period:"
		//		+ basic_rank_of_first_period);

		//System.out
		//		.println("now printing about second period --------------------");

		//System.out.println("integer code of second period: "
		//		+ integer_code_of_second_period);
		//System.out
		//		.println("period of second period:" + period_of_second_period);
		//System.out.println("ranks which cross above second period:");
		//for (int i = 0; i < ranks_which_cross_above_second_period.length; i++) {
		//	System.out.println("rank -> "
		//			+ ranks_which_cross_above_second_period[i]);
		//}
		//System.out.println("basic rank of second period:"
		///		+ basic_rank_of_second_period);

		String combined_pair = null;
		if (integer_code_of_first_period > integer_code_of_second_period) {
			combined_pair = String.valueOf(integer_code_of_first_period) + "_"
					+ String.valueOf(integer_code_of_second_period);
			//System.out
			//		.println("integer code of first period was greater than second");

		} else {
			combined_pair = String.valueOf(integer_code_of_second_period) + "_"
					+ String.valueOf(integer_code_of_first_period);
			//System.out
			//		.println("integer code of second was greater than first.");
		}

		//System.out.println("combined name of the two periods:" + combined_pair);

		//System.out.println("list of pairs already processed is.");
		//System.out.println(getlist_of_Pairs_already_processed());

		if (getlist_of_Pairs_already_processed().contains(combined_pair)) {
			//System.out.println("not going to process");
			return false;
		} else {

			//System.out.println("going ahead to process it.");
			//try {
			//	System.in.read();
			//} catch (IOException e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
			getlist_of_Pairs_already_processed().add(combined_pair);

			/***
			 * if there is a cross then this is the complex name.
			 * 
			 */
			String complex_cross_name = null;

			/**
			 * 
			 * 
			 * whether cross or not, this name holds the name of the complex i.e
			 * for how many days subindicator was above or below. only applies
			 * if one of the two is the subindicator itself. i.e sma period is
			 * 1.
			 * 
			 */
			String subindicator_above_below_sma_for_days_complex_name = null;

			String opposite_name = null;

			if (Arrays.binarySearch(ranks_which_cross_above_second_period,
					integer_code_of_first_period) >= 0) {

				SmaNameResolver snr = new SmaNameResolver(
						period_of_first_period, period_of_second_period);

				complex_cross_name = getInitial_stub() + "_" + snr.getName();
				opposite_name = getInitial_stub() + "_"
						+ snr.getOpposite_name();
				complexes_detected_on_this_day_id.add(complex_cross_name);

				/****
				 * the days for which the subindicator is above or below a
				 * particular sma is applicable obviously only if
				 * 
				 */
				if (period_of_first_period == 1 || period_of_second_period == 1) {
					Integer days_passed = get_days_passed(opposite_name);
					if (days_passed != null) {
						subindicator_above_below_sma_for_days_complex_name = snr
								.get_name_of_days_above(days_passed, true);
						complexes_detected_on_this_day_id
								.add(getInitial_stub()
										+ "_"
										+ subindicator_above_below_sma_for_days_complex_name);
					}
				}
				// now we have to generate the name of this complex and add it
				// to the map.
				// here the crossed has been above the crosser for these many
				// days.

			} else if (Arrays.binarySearch(
					ranks_which_cross_above_first_period,
					integer_code_of_second_period) >= 0) {

				SmaNameResolver snr = new SmaNameResolver(
						period_of_second_period, period_of_first_period);
				complex_cross_name = getInitial_stub() + "_" + snr.getName();
				opposite_name = getInitial_stub() + "_"
						+ snr.getOpposite_name();
				complexes_detected_on_this_day_id.add(complex_cross_name);
				if (period_of_first_period == 1 || period_of_second_period == 1) {

					Integer days_passed = get_days_passed(opposite_name);
					if (days_passed != null) {
						subindicator_above_below_sma_for_days_complex_name = snr
								.get_name_of_days_above(days_passed, true);
						complexes_detected_on_this_day_id
								.add(getInitial_stub()
										+ "_"
										+ subindicator_above_below_sma_for_days_complex_name);
					}
				}
			} else {

				// figure out which of the two is greater.
				Integer greater_period = null;
				Integer lesser_period = null;
				if (basic_rank_of_first_period > basic_rank_of_second_period) {
					greater_period = period_of_second_period;
					lesser_period = period_of_first_period;
				} else {
					greater_period = period_of_first_period;
					lesser_period = period_of_second_period;
				}

				// here the smaname resolver is used but its not because we want
				// to add something to the
				// complex map, but just to get the name and opposite name,
				// because we want to calculate
				// how many days above or below.
				SmaNameResolver snr = new SmaNameResolver(greater_period,
						lesser_period);

				// /here we have to use the complex cross name itself to get the
				// days.
				complex_cross_name = getInitial_stub() + "_"
						+ snr.getOpposite_name();

				// either the subindicator was the crosser or it was crossed.

				if (period_of_first_period == 1 || period_of_second_period == 1) {

					Integer days_passed = get_days_passed(complex_cross_name);

					if (days_passed != null) {
						subindicator_above_below_sma_for_days_complex_name = snr
								.get_name_of_days_above(days_passed, true);

						complexes_detected_on_this_day_id
								.add(getInitial_stub()
										+ "_"
										+ subindicator_above_below_sma_for_days_complex_name);

					}
				}

			}
			return true;
		}
	}

	private ArrayList<String> getlist_of_Pairs_already_processed() {
		return list_of_pairs_already_processed;
	}

	public ResolveCross(Integer day_id_being_processed, String initial_stub,
			ComplexBuySellLists cbsl,
			HashMap<String, ComplexChange> complex_change_map,
			TreeMap<Integer, Double> all_available_indicator_values) {
		this.list_of_pairs_already_processed = new ArrayList<String>();
		this.day_id_being_processed = day_id_being_processed;
		this.initial_stub = initial_stub;
		this.complexes_detected_on_this_day_id = new HashSet<String>();
		this.cbsl = cbsl;
		this.complex_change_map = complex_change_map;
		this.all_available_indicator_values = all_available_indicator_values;
	}

}
