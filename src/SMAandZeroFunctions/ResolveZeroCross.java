package SMAandZeroFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import Indicators.ComplexChange;
import RedisAccessObject.ComplexBuySellLists;

public class ResolveZeroCross {

	private ArrayList<String> complexes_detected_on_this_day_id;

	public ArrayList<String> getComplexes_detected_on_this_day_id() {
		return complexes_detected_on_this_day_id;
	}

	public void setComplexes_detected_on_this_day_id(
			ArrayList<String> complexes_detected_on_this_day_id) {
		this.complexes_detected_on_this_day_id = complexes_detected_on_this_day_id;
	}

	/**
	 * local -> calculated.
	 * 
	 * 
	 */
	private Boolean cross_detected;

	private Boolean getCross_detected() {
		return cross_detected;
	}

	/**
	 * 
	 * 
	 * 
	 */
	private String initial_stub;

	private String getInitial_stub() {
		return initial_stub;
	}

	/**
	 * the main complex change map -> passed in.
	 * 
	 */
	private HashMap<String, ComplexChange> complex_change_map;

	private HashMap<String, ComplexChange> getComplex_change_map() {
		return complex_change_map;
	}

	/***
	 * the complex buy sell lists -> passed in.
	 * 
	 * 
	 */
	private ComplexBuySellLists complex_buy_sell_lists_of_signal_line_and_zero_line;

	private ComplexBuySellLists getComplex_buy_sell_lists_of_signal_line_and_zero_line() {
		return complex_buy_sell_lists_of_signal_line_and_zero_line;
	}

	/**
	 * all available indicator values -> passed in
	 * 
	 * 
	 * 
	 */
	private TreeMap<Integer, Double> all_available_indicator_values;

	private TreeMap<Integer, Double> getAll_available_indicator_values() {
		return all_available_indicator_values;
	}

	/**
	 * the day id being processed -> passed in.
	 * 
	 */
	private Integer current_day_id;

	private Integer getCurrent_day_id() {
		return current_day_id;
	}

	/**
	 * passed in.
	 * 
	 */
	private Double average_of_all_smas;

	/**
	 * passed in.
	 * 
	 */
	private Double previous_day_value;

	/**
	 * 
	 * passed in.
	 * 
	 */
	private Double current_value;

	/**
	 * 
	 * 1 -> crosses above. -1 -> crosses below.
	 * 
	 */
	private Integer crosses_above_or_below_zero;

	public Integer getCrosses_above_or_below_zero() {
		return crosses_above_or_below_zero;
	}

	/**
	 * basically which one to use for the finding of when was the last time it
	 * crossed which wya.
	 * 
	 * i.e to use :
	 * 
	 * subindicator_goes_above_zero_line OR subindicator_goes_below_zero_line
	 * 
	 * to find like how many days have passed since it.
	 * 
	 */
	private String name_of_subindicator_stub_to_lookup_in_cbsl;

	public String getName_of_subindicator_stub_to_lookup_in_cbsl() {
		return name_of_subindicator_stub_to_lookup_in_cbsl;
	}

	public ResolveZeroCross(
			Double previous_day_value,
			Double current_value,
			Double average_of_all_smas,
			Integer current_day_id,
			TreeMap<Integer, Double> all_available_indicator_values,
			ComplexBuySellLists complex_buy_sell_lists_of_signal_line_and_zero_line,
			HashMap<String, ComplexChange> complex_change_map,
			String initial_stub) {
		this.previous_day_value = previous_day_value;
		this.current_value = current_value;
		this.average_of_all_smas = average_of_all_smas;
		this.all_available_indicator_values = all_available_indicator_values;
		this.complex_buy_sell_lists_of_signal_line_and_zero_line = complex_buy_sell_lists_of_signal_line_and_zero_line;
		this.complex_change_map = complex_change_map;
		this.cross_detected = false;
		this.initial_stub = initial_stub;
		this.current_day_id = current_day_id;
		setComplexes_detected_on_this_day_id(new ArrayList<String>());
	}

	public void resolve_cross() {

		System.out.println("the previous day value is:"
				+ this.previous_day_value);
		System.out.println("the current value is:" + this.current_value);
		System.out.println("average of all the smas is :"
				+ this.average_of_all_smas);

		if (this.previous_day_value < 0 && this.current_value > 0) {
			crosses_above_or_below_zero = 1;
			name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_below_zero_line";
			this.cross_detected = true;
			System.out.println("the cross was detected is true.");
			System.out.println("the cross was above the zero line.");
			System.out.println("figuring how many days below zero line.");
			System.out.println(name_of_subindicator_stub_to_lookup_in_cbsl);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (this.previous_day_value > 0 && this.current_value < 0) {
			crosses_above_or_below_zero = -1;
			this.cross_detected = true;
			name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_above_zero_line";

			System.out.println("the cross was detected is true.");
			System.out.println("the cross was below the zero line.");
			System.out.println("figuring how many days above zero line.");
			System.out.println(name_of_subindicator_stub_to_lookup_in_cbsl);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (this.current_value > 0) {
			crosses_above_or_below_zero = 1;
			name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_above_zero_line";

			System.out.println("just the current value is above zero.");
			System.out.println("the cross was detected is false.");
			System.out.println("the value was above the zero line.");
			System.out.println("figuring how many days ABOVE zero line.");
			System.out.println(name_of_subindicator_stub_to_lookup_in_cbsl);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (this.current_value < 0) {
			crosses_above_or_below_zero = -1;
			name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_below_zero_line";

			System.out.println("JUST the current value is below zero.");
			System.out.println("the cross was detected is false.");
			System.out.println("the value was below the zero line.");
			System.out.println("figuring how many days BELOW zero line.");
			System.out.println(name_of_subindicator_stub_to_lookup_in_cbsl);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// it is zero.
			// now take the average of the all the other smas and see if they
			// are above or below zero.
			// if still zero, consider as greater.
			System.out.println("hit zero dawg.");
			if (average_of_all_smas > 0 || average_of_all_smas == 0) {
				System.out.println("hit pos");
				crosses_above_or_below_zero = 1;
				name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_above_zero_line";
			} else {
				System.out.println("hit neg.");
				crosses_above_or_below_zero = -1;
				name_of_subindicator_stub_to_lookup_in_cbsl = "subindicator_goes_below_zero_line";
			}

		}

		name_of_subindicator_stub_to_lookup_in_cbsl = getInitial_stub()
				+ StockConstants.seperator
				+ name_of_subindicator_stub_to_lookup_in_cbsl;

		System.out
				.println("the final name of subindicator stub to lookup in cbsl.");
		System.out.println(name_of_subindicator_stub_to_lookup_in_cbsl);

		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * finally generate the complex names on this day.
	 * 
	 * two types:
	 * 
	 * a. above/below zero line for days.
	 * 
	 * b. goes above/below zero line.
	 * 
	 */
	public void resolve_days_above_or_below_zero_line_and_cross() {
		// System.out.println("initial stub inside zero cross is:");
		// System.out.println(getInitial_stub());
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		Integer classified_days_passed = get_days_passed(getName_of_subindicator_stub_to_lookup_in_cbsl());
		// now if there is a cross
		String above_or_below = null;

		if (getCrosses_above_or_below_zero() == 1) {
			above_or_below = StockConstants.above;
		} else {
			above_or_below = StockConstants.below;
		}

		if (getCross_detected()) {

			getComplexes_detected_on_this_day_id().add(
					getInitial_stub() + StockConstants.subindicator
							+ StockConstants.seperator + StockConstants.goes
							+ StockConstants.seperator + above_or_below
							+ StockConstants.seperator
							+ StockConstants.zero_line);

			// doing this because suppose the stock crosses above, then we say
			// that it has been below
			// for x days, sow we switch the above and below, before adding the
			// complex regarding
			// for how many days .
			if (classified_days_passed != null) {
				if (above_or_below.equals("above")) {
					above_or_below = "below";
				} else {
					above_or_below = "above";
				}

				getComplexes_detected_on_this_day_id()
						.add(getInitial_stub()

								+ StockConstants.subindicator
								+ StockConstants.seperator
								+ above_or_below
								+ StockConstants.seperator
								+ StockConstants.zero_line
								+ StockConstants.seperator
								+ StockConstants.for_string
								+ StockConstants.period_in_integer_period_in_words_map
										.get(classified_days_passed));
			}
		} else {
			// now template is subindicator_above/below_zero_line_for_n_sessions
			// remove trailing s if necessary.
			// System.out.println("these are the classified days passed:"
			// + classified_days_passed);
			// System.out.println(classified_days_passed);
			if (classified_days_passed != null) {
				getComplexes_detected_on_this_day_id()
						.add(getInitial_stub()

								+ StockConstants.subindicator
								+ StockConstants.seperator
								+ above_or_below
								+ StockConstants.seperator
								+ StockConstants.zero_line
								+ StockConstants.seperator
								+ StockConstants.for_string
								+ StockConstants.period_in_integer_period_in_words_map
										.get(classified_days_passed));
				/**
				System.out
						.println("the days above or below zero line--> final name becomes.");
				System.out.println(getInitial_stub()

						+ StockConstants.subindicator
						+ StockConstants.seperator
						+ above_or_below
						+ StockConstants.seperator
						+ StockConstants.zero_line
						+ StockConstants.seperator
						+ StockConstants.for_string
						+ StockConstants.period_in_integer_period_in_words_map
								.get(classified_days_passed));
				try {
					System.in.read();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				**/
			} else {
				//System.out
				//		.println("classified days passed was null so , no complex was added.");
			}
		}

	}

	public Integer get_days_passed(String opposite_name) {
		// so basically we have to find when was the last time the opposite
		// crossed.
		// if it exists, otherwise.
		// there can also be realtime nausea.
		// so if there was a cross in the past then how do i assess it now
		// will have to somehow update this crap.
		// check in both the cbsl and the complex change map.
		// System.out.println("opposite name is:" + opposite_name);

		LinkedHashMap<String, ArrayList<Integer>> buy_sell_lists_for_opposite_name = getComplex_buy_sell_lists_of_signal_line_and_zero_line()
				.getBuy_sell_lists_by_complex_name().get(opposite_name);
		// get the last date.
		Integer buy_lists_last_date = null;

		// get the buy list for it and get the last date.
		ArrayList<Integer> buy_dates = buy_sell_lists_for_opposite_name
				.get(StockConstants.complex_values_buy_key_name);
		if (!buy_dates.isEmpty()) {
			buy_lists_last_date = buy_dates.get(buy_dates.size() - 1);
		}

		// now get the last date from the complex change map.
		// basically this map holds all the values that are being calculated in
		// teh current calculation
		// especially in the indicatorsignal line calculation after the sma
		// calculations are done for
		// each day in the rolling queue we add all the complexes that are got,
		// to the complex change map.
		// then we pass in the complex change map into this resolve cross class
		if (complex_change_map.get(opposite_name) != null) {
			System.out.println("the complex change map contains it");
			System.out.println(opposite_name);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// then we get the last date in it.
			ArrayList<Integer> complex_change_map_day_ids_for_this_complex = complex_change_map
					.get(opposite_name).getDay_ids();

			System.out
					.println("these are the day ids on which this complex was encountered in past.");
			System.out.println(complex_change_map_day_ids_for_this_complex);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Integer last_day_in_complex_change_map = complex_change_map_day_ids_for_this_complex
					.get(complex_change_map_day_ids_for_this_complex.size() - 1);

			System.out.println("the last day is:"
					+ last_day_in_complex_change_map);

			if (buy_lists_last_date != null) {
				if (buy_lists_last_date < last_day_in_complex_change_map) {
					buy_lists_last_date = last_day_in_complex_change_map;
				}
			} else {
				buy_lists_last_date = last_day_in_complex_change_map;
			}
		}

		// if you cannot find a point where there has been a cross ignore it.
		if (buy_lists_last_date != null) {
			// now calculate the number of days since.
			// we should get real number of sessions
			// not just minus.
			// in both cases,
			// we are calculating days since.
			// so we need to get a headmap.
			// of the indicator values linkedhashmap between these two day ids.
			Integer days_passed = CompressUncompress
					.get_total_number_of_actual_sessions_between_two_day_ids(
							getCurrent_day_id(), buy_lists_last_date,
							getAll_available_indicator_values());
			System.out
					.println("these are the day ids themselves, just to confirm.");
			System.out.println(getAll_available_indicator_values().keySet());
			System.out.println("days passed become:" + days_passed);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// now we make the names of the complexes.
			// find closest day to this out of all the periods.
			// only if more than one day has passed then.

			return CompressUncompress
					.find_nearest(
							days_passed.doubleValue(),
							CompressUncompress
									.integer_array_to_double_array(StockConstants.standard_deviation_subindicator_periods))
					.intValue();

		}
		return null;
	}

}
