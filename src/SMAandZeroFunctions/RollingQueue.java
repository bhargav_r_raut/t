package SMAandZeroFunctions;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.elasticsearch.common.recycler.Recycler.V;
import org.javatuples.Triplet;

import CommonUtils.CompressUncompress;

public class RollingQueue {

	// ///////////////////////////////////////////////////////////////////////////////////
	// METHODS TO BE OVERRIDDEN.
	// both methods MUST SET the last day id processed at their respective ends.
	// ////////////////////////////////////////////////////////////////////////////////////

	/**
	 * this is fired when the rolling queue is instantiated. OVERRIDDING CLASS
	 * MUST CALL SET_LAST_DAY_ID_PROCESSED at its end.
	 * 
	 */
	public void get_existing_results_map() {

	}

	/**
	 * this method should be overridden in subclasses OVERRIDDING CLASS MUST
	 * CALL SET_LAST_DAY_ID_PROCESSED at its end.
	 * 
	 */
	public void post_process(Integer day_id, Integer last_day_id_processed) {

	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////

	// BASIC ROLLING QUEUE STUFF

	// /////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * to track so that we can get the previous day easily.\ when we track
	 * crosses.
	 * 
	 */
	private Integer last_day_id_in_results;

	public Integer getLast_day_id_in_results() {
		return last_day_id_in_results;
	}

	public void setLast_day_id_in_results(Integer last_day_id_in_results) {
		this.last_day_id_in_results = last_day_id_in_results;
	}

	/**
	 * structure :
	 * 
	 * key => period.
	 * value => triplet : 
	 * a => current total of elements in the queue. -> updated whenever a new element is added or removed from the
	 * queue,
	 * initial value = 0;
	 * 
	 * 
	 * b => current total number of elements in the queue ->updated whenever a
	 * new element is added or removed from the queue, initial value is = 0
	 * 
	 * c => queue itself. -> linkedlist of doubles.
	 */
	private static LinkedHashMap<Integer, Triplet<Double, Integer, Queue<Double>>> rolling_queue =
			new LinkedHashMap<Integer, Triplet<Double, Integer, Queue<Double>>>();

	private static LinkedHashMap<Integer, Triplet<Double, Integer, Queue<Double>>> getRolling_queue() {
		return rolling_queue;
	}

	/***
	 * these are the final results this is done for convenience of looking up
	 * the sma period easily .
	 * 
	 * their structure is as follows:
	 * 
	 * key -> day id
	 * 
	 * value => key -> sma_period value -> sma_calculation_object.
	 * 
	 * this hashmap should be populated in the class which overrides
	 * 
	 * it should be called inside both the
	 * 
	 * post_process
	 * 
	 * and
	 * 
	 * get_existing_results.
	 * 
	 */
	private LinkedHashMap<Integer, LinkedHashMap<Integer, SmaCalculationResultObject>> results;

	public LinkedHashMap<Integer, LinkedHashMap<Integer, SmaCalculationResultObject>> getResults() {
		return results;
	}

	public void setResults(
			LinkedHashMap<Integer, LinkedHashMap<Integer, SmaCalculationResultObject>> results) {
		this.results = results;
	}

	private Integer[] periods_for_which_rolling_averages_have_to_be_calculated;

	private Integer[] getPeriods_for_which_rolling_averages_have_to_be_calculated() {
		return periods_for_which_rolling_averages_have_to_be_calculated;
	}

	private void setPeriods_for_which_rolling_averages_have_to_be_calculated(
			Integer[] periods_for_which_rolling_averages_have_to_be_calculated) {
		this.periods_for_which_rolling_averages_have_to_be_calculated = periods_for_which_rolling_averages_have_to_be_calculated;
	}

	/**
	 * 
	 * 
	 * 
	 * @param periods_for_which_rolling_averages_have_to_be_calculated
	 */
	public RollingQueue(
			Integer[] periods_for_which_rolling_averages_have_to_be_calculated) {

		setPeriods_for_which_rolling_averages_have_to_be_calculated(periods_for_which_rolling_averages_have_to_be_calculated);
		for (Integer period : periods_for_which_rolling_averages_have_to_be_calculated) {
			rolling_queue.put(period,
					new Triplet<Double, Integer, Queue<Double>>(0.0, 0,
							new LinkedList<Double>()));
		}
		setLast_day_id_in_results(null);
		setResults(new LinkedHashMap<Integer, LinkedHashMap<Integer, SmaCalculationResultObject>>());
		/***
		 * this is a hook overridden by the extending class.
		 * 
		 */
		get_existing_results_map();

	}

	/**
	 * adds the given double value to all the queues in the rolling queue
	 * linkedhashmap.
	 * 
	 * day id -> the day id whose double value is being added to the queue.
	 * 
	 * @param value
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void add_to_all_queues(Integer day_id, Double value) {
		getResults().put(day_id,
				new LinkedHashMap<Integer, SmaCalculationResultObject>());
		for (Integer period : getPeriods_for_which_rolling_averages_have_to_be_calculated()) {
			add_to_queue(period, value, day_id);
		}
		;
		/**
		 * this is a hook overridden by the extending class.
		 * 
		 */
		post_process(day_id, getLast_day_id_in_results());

	}

	/**
	 * algorithm is as follows:
	 * 
	 * add value to queue update total = total + value update count = count + 1
	 * 
	 * if count > period
	 * 
	 * update count = count - 1
	 * 
	 * remove_from_queue - -update_total = total - first_value_from_queue -
	 * remove() calculate average - post to pre_results.
	 * 
	 * else dont do anything. end
	 * 
	 * @param period
	 * @param value
	 * @throws IOException
	 */
	public void add_to_queue(Integer period, Double value, Integer day_id) {
		Integer multiplier = 1;

		value = CompressUncompress.round(value, 2);
		System.out.println("indicator value to add is: " + value);
		System.out.println("the day id is:" + day_id);

		getRolling_queue().get(period).getValue2().add(value);
		Double curr_total = get_current_total_of_queue(period);
		Integer curr_count = get_current_count(period);

		System.out.println("period is:" + period);
		System.out.println("curr total is:" + curr_total);
		System.out.println("curr count is:" + curr_count);

		Double new_total = update_queue_total(curr_total, multiplier, value,
				period);

		Integer new_count = update_queue_count(curr_count, multiplier, period);

		System.out.println("period is:" + period);
		System.out.println("curr total after incr is:" + new_total);
		System.out.println("curr count after incr is:" + new_count);

		// if zero was added.
		// just decrement the count , i.e remove from queue.
		//

		multiplier = -1;
		if (new_count >= period) {
			// cumulatively resets the counts and the totals.

			if (new_count == period) {
				multiplier = 0;
			}

			System.out.println("multiplier is:" + multiplier);

			Double removed_from_queue = remove_from_queue(period, multiplier);

			Integer new_count_i = update_queue_count(new_count, multiplier,
					period);

			Double new_total_i = null;
		//	if (period.equals(1) && value.equals(0.0)) {
		//		set_new_total(1, 0.0);
		//		update_queue_average_direct(0.0, period, day_id);
		//	} else {
				new_total_i = update_queue_total(new_total, multiplier,
						removed_from_queue, period);

				update_queue_average(new_count_i, new_total_i, period, day_id);
	//		}

			System.out.println("now the new total is: "
					+ get_current_total_of_queue(period));
			System.out.println("now the new count is:"
					+ get_current_count(period));

			System.out.println("the queue is :"
					+ getRolling_queue().get(period).getValue2());

			System.out.println("ends here.");

		}

		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * returns the element that it pops from the head of the queue
	 * 
	 * @param period
	 * @return
	 */
	private Double remove_from_queue(Integer period, Integer multiplier) {
		if (multiplier == 0) {
			return getRolling_queue().get(period).getValue2().peek();
		}
		return getRolling_queue().get(period).getValue2().remove();
	}

	// ///////////////////

	private Double get_current_total_of_queue(Integer period) {
		return getRolling_queue().get(period).getValue0();
	}

	private Integer get_current_count(Integer period) {
		return getRolling_queue().get(period).getValue1();
	}

	// ///////////////////

	private Double update_queue_total(Double curr_total, Integer add_or_remove,
			Double value_to_add, Integer period) {

		if (value_to_add.equals(0.0) || add_or_remove.equals(0)) {

		} else {
			// System.out.println("normal");
			if (add_or_remove > 0) {
				curr_total = curr_total + value_to_add;
			} else {
				curr_total = curr_total = value_to_add;
			}
		}

		set_new_total(period, curr_total);

		return curr_total;
	}

	private void set_new_total(Integer period, Double new_total) {
		Triplet<Double, Integer, Queue<Double>> tr = getRolling_queue().get(
				period).setAt0(new_total);
		getRolling_queue().put(period, tr);
	}

	// ////////////////////

	private Integer update_queue_count(Integer curr_count,
			Integer add_or_remove, Integer period) {
		Integer new_count = curr_count + (1 * add_or_remove);
		set_new_count(period, new_count);
		return new_count;
	}

	private void set_new_count(Integer period, Integer new_count) {
		Triplet<Double, Integer, Queue<Double>> tr = getRolling_queue().get(
				period).setAt1(new_count);
		getRolling_queue().put(period, tr);
	}

	// //////////////////
	private void update_queue_average(Integer curr_count, Double curr_total,
			Integer period, Integer day_id) {

		Double average = curr_total / curr_count.doubleValue();
		System.out.println("average calculated as:" + average);
		this.results.get(day_id).put(period,
				new SmaCalculationResultObject(period, average));
	}

	private void update_queue_average_direct(Double avg, Integer period,
			Integer day_id) {
		this.results.get(day_id).put(period,
				new SmaCalculationResultObject(period, avg));
	}

	// /custom comparator
	public static LinkedHashMap<Integer, SmaCalculationResultObject> get_sorted_map(
			LinkedHashMap<Integer, SmaCalculationResultObject> ln) {

		List<Map.Entry<Integer, SmaCalculationResultObject>> list = new LinkedList<>(
				ln.entrySet());

		Collections
				.sort(list,
						new Comparator<Map.Entry<Integer, SmaCalculationResultObject>>() {
							@Override
							public int compare(
									Map.Entry<Integer, SmaCalculationResultObject> o1,
									Map.Entry<Integer, SmaCalculationResultObject> o2) {

								if (o1.getValue().getAverage() > o2.getValue()
										.getAverage()) {
									// sma_calc_object.rank--;

									return -1;

								} else if (o1.getValue().getAverage()
										.equals(o2.getValue().getAverage())) {

									if (o1.getValue().period > o2.getValue().period) {
										// sma_calc_object.rank--;
										return -1;
									} else {
										// this.rank--;
										return 1;
									}
								} else {

									// this.rank--;
									return 1;
								}

							}
						});

		LinkedHashMap<Integer, SmaCalculationResultObject> result = new LinkedHashMap<Integer, SmaCalculationResultObject>();

		for (Map.Entry<Integer, SmaCalculationResultObject> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

}
