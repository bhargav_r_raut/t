package SMAandZeroFunctions;

import yahoo_finance_historical_scraper.StockConstants;

import com.sun.tools.jdi.LinkedHashMap;

public class SmaCalculationResultObject implements Comparable {

	/**
	 * this is set on each smacarr object so that it can be used
	 * 
	 * 
	 */
	private Double previous_day_value_for_this_sma;

	public void setPrevious_day_value_for_this_sma(
			Double previous_day_value_for_this_sma) {
		this.previous_day_value_for_this_sma = previous_day_value_for_this_sma;
	}

	public Double getPrevious_day_value_for_this_sma() {
		return previous_day_value_for_this_sma;
	}

	/**
	 * 
	 * the combination of the rank on the previous day and current day. example
	 * : 1_2
	 * 
	 */
	public String rank_combination;

	public String getRank_combination() {
		return rank_combination;
	}

	public void setRank_combination(String rank_combination) {
		this.rank_combination = rank_combination;
	}

	public Integer rank_combination_binary_search_integer_code;

	public Integer getRank_combination_binary_search_integer_code() {

		return StockConstants.cross_detection_string_pairs_for_binary_search_string_as_key
				.get(getRank_combination());

	}

	public Integer period;

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Integer rank;

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getRank() {
		return rank;
	}

	public Double average;

	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}

	/**
	 * @param period
	 * @param average
	 */
	public SmaCalculationResultObject(Integer period, Double average) {
		super();
		this.period = period;
		this.average = average;
		this.rank = 0;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		SmaCalculationResultObject sma_calc_object = (SmaCalculationResultObject) o;
		// so how do we sort it?
		// by the descending order of the value.

		if (this.average > sma_calc_object.getAverage()) {
			// sma_calc_object.rank--;
			return -1;

		} else if (this.average == sma_calc_object.getAverage()) {
			if (this.period > sma_calc_object.period) {
				// sma_calc_object.rank--;
				return -1;
			} else {
				// this.rank--;
				return 1;
			}
		} else {
			// this.rank--;
			return 1;
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return (" ||| period is:" + period + " ||| actual value is: " + average
				+ "||| rank is:" + rank + " ||| rank combination is:"
				+ rank_combination + " ||| rank combination binary is:" + rank_combination_binary_search_integer_code);

	}

}
