package SMAandZeroFunctions;

import java.io.IOException;

import yahoo_finance_historical_scraper.StockConstants;

public class SmaNameResolver {

	private String name;
	private String opposite_name;

	public String getOpposite_name() {

		return opposite_name;

	}

	public String get_name_of_days_above(Integer days_passed,
			Boolean there_was_a_cross) {
		
		String name_to_return = null;

		// this is the case where one has just crossed above the other.
		// in this case it is the above that has been above.
		// if subinidcator crossed above
		// then it was below.
		
		
		
		
		//this crosser check is to decide the above or below
		//since in the subindicators we have either subindicator above or below a particular sma for n days.
		//so if subindicator has crossed above, then it was below for n days.
		if (this.crosser == 1) {
			name_to_return = StockConstants.subindicator.replace("_","")
					+ StockConstants.seperator + "below"
					+ StockConstants.seperator + crossed.toString()
					+ StockConstants.seperator + "day_sma_for"
					+ StockConstants.period_in_integer_period_in_words_map.get(days_passed);
			
			
		// and if subindicator was crossed then it was above for n days.
		} else {
			name_to_return = StockConstants.subindicator.replace("_","")
					+ StockConstants.seperator + "above"
					+ StockConstants.seperator + crosser.toString()
					+ StockConstants.seperator + "day_sma_for"
							+ StockConstants.period_in_integer_period_in_words_map.get(days_passed);
		
			
		}

		if (there_was_a_cross) {
			// just return the name.
			return name_to_return;
		} else {
			//obviously if one was above the other, basically there is no cross.
			//just we called one of them the crosser for purpose of convenience.
			//to generate the opposite name to calculate the number of days 
			//in the resolvecross class.
			
			
			//in the inital part above, we make the names from the point of view of a cross.
			//so there it becomes opposite.
			
			//but if there was not cross, we need to put it back by reversing what was done above.
			
			if (name_to_return.contains("above")) {
				return name_to_return.replace("above", "below");
			} else {
				return name_to_return.replace("below", "above");
			}

		}
	}

	public String getName() {
		return name;
	}

	/***
	 * the period which crossed above another period.
	 * 
	 */
	public Integer crosser;

	/**
	 * the period which was crossed.
	 * 
	 */
	public Integer crossed;

	/**
	 * constructor for sma crosses above another sma.
	 * 
	 * @param crosser
	 * @param crossed
	 */
	public SmaNameResolver(Integer crosser, Integer crossed) {
		
		
		
		
		this.crossed = crossed;
		this.crosser = crosser;

		/**
		 * the subindicator itself was crossed.
		 * 
		 */
		if (crossed == 1) {
			if (crosser == 9) {
				// subindicator goes below signal line.
				this.name = StockConstants.subindicator.replace("_","")
						+ "_goes_below_signal_line";
				this.opposite_name = StockConstants.subindicator.replace("_","")
						+ "_goes_above_signal_line";
			} else {
				this.name = StockConstants.subindicator.replace("_","") + "_"
						+ crosser.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","");
				this.opposite_name = StockConstants.subindicator.replace("_","")
						+ "_crosses_above_" 
						+ crosser.toString() + "_session_sma";
			}
		}
		/***
		 * the signal line was crossed.
		 * 
		 */
		else if (crossed == 9) {
			// subindicator goes above signal line.
			if (crosser == 1) {
				this.name = StockConstants.subindicator.replace("_","")
						+ "_goes_above_signal_line";
				this.opposite_name = StockConstants.subindicator.replace("_","")
						+ "_goes_below_signal_line";
			} else {
				this.name = StockConstants.subindicator.replace("_","") + "_"
						+ crosser.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","") + "_"
						+ crossed.toString() + "_session_sma";

				this.opposite_name = StockConstants.subindicator.replace("_","") + "_"
						+ crossed.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","") + "_"
						+ crosser.toString() + "_session_sma";

			}
		}
		/**
		 * something else was crossed.
		 * 
		 */
		else {
			if (crosser == 1) {
				this.name = StockConstants.subindicator.replace("_","") + "_crosses_above_"
						
						+ crossed.toString() + "_session_sma";
				this.opposite_name = StockConstants.subindicator.replace("_","") + "_"
						+ crossed.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","");
			} else {
				this.name = StockConstants.subindicator.replace("_","") + "_"
						+ crosser.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","") + "_"
						+ crossed.toString() + "_session_sma";
				this.opposite_name = StockConstants.subindicator.replace("_","") + "_"
						+ crossed.toString() + "_session_sma_crosses_above_"
						+ StockConstants.subindicator.replace("_","") + "_"
						+ crosser.toString() + "_session_sma";
			}

		}

	}

}
