package SMAandZeroFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.json.JSONException;

import CommonUtils.FloatGlitch;
import Indicators.ComplexChange;
import Titan.GroovyStatements;

import yahoo_finance_historical_scraper.StockConstants;

public class Test {

	private TreeMap<Integer, Double> all_available_indicator_values;

	public TreeMap<Integer, Double> getAll_available_indicator_values() {
		return all_available_indicator_values;
	}

	public ArrayList<Integer> getRequired_day_ids() {
		return required_day_ids;
	}

	public ArrayList<Integer> getAll_day_ids() {
		return all_day_ids;
	}

	public String getInitial_stub() {
		return initial_stub;
	}

	private ArrayList<Integer> required_day_ids;

	private ArrayList<Integer> all_day_ids;

	private final Integer total_days_for_test = 1000;
	private final Integer start_day = 10;
	private final String initial_stub = "AC.PA_Accor S.A._CAC_stochastic_"
			+ "oscillator_k_indicator_period_start_14_3_period_end";
	private GregorianCalendar start_cal;
	private GregorianCalendar end_cal;

	public void set_required_day_ids_and_all_day_ids() {
		this.required_day_ids = new ArrayList<Integer>(
				this.all_available_indicator_values.keySet());
		this.all_day_ids = this.required_day_ids;
	}

	public static Double get_random_value(Double min, Double max, Random r) {
		return min + (max - min) * r.nextDouble();
	}

	public static void main(String[] args) throws JSONException, IOException {
		GroovyStatements.initializeGroovyStatements(null);
		// RiseAndFallFunctionsTest t = new RiseAndFallFunctionsTest("basic",null,null);
		// RiseAndFallFunctionsTest t = new RiseAndFallFunctionsTest("saa",1,3);
		// t.test_generation_of_cross_pairs();
		// System.exit(1);
		// all the below tests check each condition prescribed in the
		// Sma Name Resolver.

		Test zer = new Test("test_zero_cross", null, null);

		System.exit(1);
		// one crossed by nine.
		Test t1 = new Test("test_sma_crosses", 1, 9);

		System.in.read();
		// one crossed by something other than 9
		Test t2 = new Test("test_sma_crosses", 1, 200);

		// nine crossed by 1
		Test t3 = new Test("test_sma_crosses", 9, 1);

		// nine crossed by something other than 1
		Test t4 = new Test("test_sma_crosses", 9, 200);

		// something crossed by 1
		Test t5 = new Test("test_sma_crosses", 200, 1);

		// something crossed by something other than 1.
		Test t6 = new Test("test_sma_crosses", 200, 9);

	}
	
	public Test(){
		
	}
	
	public Test(Double min, Double max){
		prepare_test_values_for_basic_functions_tests(min,max);
	}

	/**
	 * 
	 * 
	 * 
	 * @param type
	 *            - test type,
	 * 
	 *            basic, sma cross, zero cross, days passed sma, days passed
	 *            zero,
	 * 
	 * @param crosser
	 * @param crossed
	 */
	public Test(String type, Integer crossed, Integer crosser) {
		start_timer();
		if (type.equals("basic")) {
			prepare_test_values_for_basic_functions_tests(-100.34,112.23);
			try {
				test_basic_functioning_of_indicator_signalline_class();
			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (type.equals("test_sma_crosses")) {
			prepare_test_values_for_sma_cross(crosser, crossed);
			System.out.println(this.all_available_indicator_values);
			try {
				IndicatorSignalLineAndSmaCalculations insl = new IndicatorSignalLineAndSmaCalculations(
						this.all_available_indicator_values,
						this.required_day_ids, this.all_day_ids,
						this.initial_stub,
						StockConstants.periods_for_sma_calculations);
				// so the complex change map should have the desired complex
				// change at the desired point.
				SmaNameResolver snr = new SmaNameResolver(crosser, crossed);
				// so we should have the snr.name -> in the complex change map,
				// at the right place,
				// the place should be after the crossover.

				if (insl.getComplex_changes_map().get(snr.getName()) != null) {
					ComplexChange cc = insl.getComplex_changes_map().get(
							snr.getName());
					if (cc == null) {
						System.out.println("complex change itself was null");
						System.out.println("test failed");
					} else {
						System.out
								.println("these are the complex change values");
						System.out.println(cc.getComplex_values());
						System.out.println("these are teh day ids.");
						cc.getDay_ids();
					}

				} else {
					System.out.println("test failed");
					for (Map.Entry<String, ComplexChange> entry : insl
							.getComplex_changes_map().entrySet()) {
						System.out.println("complex name:" + entry.getKey());
						System.out.println("day idds:"
								+ entry.getValue().getDay_ids());
						System.out.println("\n");
					}
				}

			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (type.equals("test_zero_cross")) {
			prepare_test_values_for_zero_cross();
			try {
				test_rolling_queue_basic_function();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			try {
				IndicatorSignalLineAndSmaCalculations insl = new IndicatorSignalLineAndSmaCalculations(
						all_available_indicator_values, required_day_ids,
						all_day_ids, this.initial_stub,
						StockConstants.periods_for_sma_calculations);
			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			**/
			
		}

		end_timer();

	}

	public void start_timer() {
		this.start_cal = new GregorianCalendar();
		System.out.println("started");
	}

	public void end_timer() {
		System.out.println("ended");
		this.end_cal = new GregorianCalendar();
		System.out.println("took "
				+ (this.end_cal.getTimeInMillis() - this.start_cal
						.getTimeInMillis()));
	}

	public void prepare_test_values_for_zero_cross() {

		double low = 20.23;
		double max = 100.23;

		this.all_available_indicator_values = new TreeMap<Integer, Double>();

		Random r = new Random();

		// so first we do 3 positives, then we switch to negative,
		// then we do 5 negatives then we switch to positive.
		Integer[] intervals = { 3, 3, 3, 3, 6, 14, 12, 2, 3, 4, 5, 3, 6, 9, 7,
				17, 34, 5, 6, 7, 8, 9 };
		Integer start_multiple = 1;
		Integer counter = 0;
		for (int i = 0; i < intervals.length; i++) {

			start_multiple = start_multiple * -1;
			// System.out.println("start multiple was:" + start_multiple);

			Integer interval = intervals[i];
			// System.out.println("interval is:" + interval);

			for (int k = 0; k < interval; k++) {
				// System.out.println("k is: " + k);
				if (i == 3) {
					this.all_available_indicator_values.put(counter, 0.0);
				} else {
					this.all_available_indicator_values.put(
							counter,
							get_random_value(low * start_multiple, max
									* start_multiple, r));
				}

				counter++;
			}

		}

		 System.out.println(this.all_available_indicator_values);

		set_required_day_ids_and_all_day_ids();

	}

	/**
	 * crosser -> the sma that crossed above another sma.
	 * 
	 * crossed -> the sma that was crossed.
	 * 
	 * if the crosser is larger, then we have to make the values suddenly fall.
	 * if the crosser is smaller, then we have to make the values suddenly rise.
	 * until then we keep the values, between a very narrow random range.
	 * 
	 * after the maximum of the period of the crosser or crossed is passed, we
	 * make the required change, we choose a disproportianetely higher or lower
	 * but sufficiently narrow random range.
	 * 
	 * 
	 * 
	 */
	public void prepare_test_values_for_sma_cross(Integer crosser,
			Integer crossed) {

		double min_low = 23.00;
		double max_low = 25.00;

		double min_high = 223.00;
		double max_high = 237.00;

		this.all_available_indicator_values = new TreeMap<Integer, Double>();

		Random r = new Random();

		if (crosser > crossed) {

			// start with the higher range.
			// for twice the number of days as the crosser period.
			// when that has elaspsed, drop the range.
			for (int i = 0; i < crosser * 2; i++) {

				this.all_available_indicator_values.put(i,
						get_random_value(min_high, max_high, r));
			}
			// now after this is done, again for the same number of days as
			// above, use the min_low
			for (int i = crosser * 2; i < crosser * 4; i++) {
				this.all_available_indicator_values.put(i,
						get_random_value(min_low, max_low, r));
			}

		} else {

			// start with the lower range.
			// for twice the number of days
			for (int i = 0; i < crosser * 2; i++) {
				this.all_available_indicator_values.put(i,
						get_random_value(min_low, max_low, r));
			}
			// now after this is done, again for the same number of days as
			// above, use the min_low
			for (int i = crosser * 2; i < crosser * 4; i++) {
				this.all_available_indicator_values.put(i,
						get_random_value(min_high, max_high, r));
			}
		}

		set_required_day_ids_and_all_day_ids();

	}

	/***
	 * 
	 * 
	 * first test the sorting of the sorted map.
	 * 
	 * 
	 */
	public void test_sorting() {
		LinkedHashMap<Integer, SmaCalculationResultObject> test_map = new LinkedHashMap<Integer, SmaCalculationResultObject>();

		LinkedHashMap<Integer, SmaCalculationResultObject> test_map_equality = new LinkedHashMap<Integer, SmaCalculationResultObject>();

		Random r = new Random();
		for (Integer sma_period : StockConstants.periods_for_sma_calculations) {
			test_map.put(sma_period, new SmaCalculationResultObject(sma_period,
					Test.get_random_value(-122.2, 121.22, r)));
		}

		test_map_equality.put(9, new SmaCalculationResultObject(9, 12.2));
		test_map_equality.put(25, new SmaCalculationResultObject(25, 12.2));
		test_map_equality.put(12, new SmaCalculationResultObject(12, 12.2));

		System.out.println("this is the map:");
		System.out.println(test_map);

		System.out.println("this is the map after sorting");
		System.out.println(RollingQueue.get_sorted_map(test_map));
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("this is the map with the equality:");
		System.out.println(test_map_equality.keySet());
		System.out.println("this is the equality map sorted");
		System.out.println(RollingQueue.get_sorted_map(test_map_equality)
				.keySet());

		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * then test, whether cross pairs are generated properly in stock constants.
	 * 
	 */
	public void test_generation_of_cross_pairs() {
		System.out
				.println(StockConstants.cross_detection_string_pairs_for_binary_search_string_as_key);

		System.out
				.println(StockConstants.which_rank_pattern_crosses_above_which_as_string);

		System.out
				.println(StockConstants.which_rank_pattern_crosses_above_which);
	}

	/***
	 * 
	 * then test update rank function.
	 * 
	 * 
	 */

	/**
	 * 
	 * check that the new sorted and rank updated map has really changed and
	 * been put into the results.
	 * 
	 * 
	 */

	/***
	 * 
	 * now test zero cross.
	 * 
	 * 
	 */

	/***
	 * 
	 * now test difference between subindicator and signal line.
	 * 
	 * 
	 */

	/***
	 * 
	 * finally test resolve cross - and resolve name.
	 * 
	 */

	/**
	 * test loading of existing results.
	 * 
	 * 
	 */

	/**
	 * 
	 * 
	 * 
	 * 
	 **/

	/****
	 * *************************************************************************
	 * ************
	 * 
	 * TEST BASIC FUNCTIONING.
	 * 
	 * FOR ROLLING QUEUE
	 * 
	 * AND FULL INDICATORSIGNALLINE CLASS. basically just ensuring no npe's and
	 * any other major flaws.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * *************************************************************************
	 * ************
	 */

	public void prepare_test_values_for_basic_functions_tests(Double min, Double max) {

		
		Random r = new Random();

		this.all_available_indicator_values = new TreeMap<Integer, Double>();

		for (int i = this.start_day; i < this.total_days_for_test
				+ this.start_day; i++) {
			this.all_available_indicator_values.put(i,
					FloatGlitch.drop_decimals(get_random_value(min, max, r)));
		}

		set_required_day_ids_and_all_day_ids();

		// System.out.println(this.all_available_indicator_values);
	}

	public void test_rolling_queue_basic_function() throws JSONException,
			IOException {
		RollingQueue rq = new RollingQueue(
				StockConstants.periods_for_sma_calculations);
		for (Map.Entry<Integer, Double> entry : this.all_available_indicator_values
				.entrySet()) {
			rq.add_to_all_queues(entry.getKey(), entry.getValue());
		}
		GroovyStatements.initializeGroovyStatements(null);
		System.out.println(GroovyStatements.getInstance().gson.toJson(rq
				.getResults()));
	}

	public void test_basic_functioning_of_indicator_signalline_class()
			throws JSONException, IOException {
		IndicatorSignalLineAndSmaCalculations insl = new IndicatorSignalLineAndSmaCalculations(
				this.all_available_indicator_values, this.required_day_ids,
				this.all_day_ids, this.initial_stub,
				StockConstants.periods_for_sma_calculations);
	}

}
