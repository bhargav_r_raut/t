import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;

import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.script.ScriptService.ScriptType;

import ComplexProcessor.Question;
import ExchangeBuilder.BulkPayLoad;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.PairArray;
import IdAndShardResolver.IdAndShardResolver;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;

public class SandBox {

	public static void main(String[] args) throws Exception {
		/**
		Entity e = new Entity("test", 0);
		
		e.getPrice_map_internal().size();
		System.in.read();
		
		
		ObjectMapper mapper = new ObjectMapper();
		StopLossAndPriceChangeUpdateRequest sr = new StopLossAndPriceChangeUpdateRequest();
		sr.setArrs_and_start_indices(new HashMap<String,Integer>(){{
			put("hello",1);
		}});
		System.out.println(mapper.writeValueAsString(sr));
		System.in.read();
		GregorianCalendar start_cal = new GregorianCalendar();

		int[] sparse_arr = new int[4];
		sparse_arr[0] = 6;
		sparse_arr[1] = 10;
		sparse_arr[2] = 14;
		sparse_arr[3] = 15;
		int[] read = get_arr(sparse_arr, 0, null);
		String comb = "";
		for (int i = 0; i < read.length; i++) {
			comb += String.valueOf(read[i]) + " , ";
		}
		// elasticsearch_bulk_size();
		System.out.println("init list generation results:");
		System.out.println(comb);

		int[] sarr = new int[3];
		sarr[0] = 19;
		sarr[1] = 22;
		sarr[2] = 30;

		read = get_arr(sarr, 0, read);

		comb = "";
		for (int i = 0; i < read.length; i++) {
			comb += String.valueOf(read[i]) + " , ";
		}
		// elasticsearch_bulk_size();
		System.out.println("after init list generation results:");
		System.out.println(comb);

		GregorianCalendar end_cal = new GregorianCalendar();

		System.out.println("time taken:"
				+ (end_cal.getTimeInMillis() - start_cal.getTimeInMillis()));
		**/
		int[] k = new int[1095000];
		for(int i = 0; i < 1095000; i++){
			
				k[i] = 99999;
			
			
		}
		PairArray.get_compressed_int_array(k, 0);
	}

	
	
	
	public static void elasticsearch_bulk_size()
			throws JsonGenerationException, JsonMappingException, IOException {
		final ArrayList<Integer> test_list = new ArrayList<Integer>();
		Random r = new Random();
		for (int i = 0; i < 10; i++) {
			test_list.add(r.nextInt(10));
		}
		Collections.sort(test_list);

		BulkRequestBuilder brb = new BulkRequestBuilder(
				RemoteEs.getInstance());

		UpdateRequest ureq = new UpdateRequest("tradegenie_titan", "bs",
				"blame");
		ureq.script("update_bs_script", ScriptType.FILE,
				new HashMap<String, Object>() {
					{
						put("sparse_arr", ArrayUtils.toPrimitive(test_list
								.toArray(new Integer[test_list.size()])));
					}
				});
		ureq.scriptedUpsert(true);
		ureq.upsert(new HashMap<String, Object>());
		brb.add(ureq);

		BulkResponse resp = brb.execute().actionGet();
		BulkItemResponse[] bir = resp.getItems();
		for(int i=0; i < bir.length; i++){
			BulkItemResponse biri = bir[i];
			
		}
	}

	/****
	 * 
	 * 
	 * 
	 * 
	 * @throws Exception
	 */
	public static void test_completion_suggester_memory() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		EntityIndex ei = new EntityIndex("test", 1);
		ei.initialize_remote_bulk_processor();
		HashMap<String, String[]> high_low_amounts = IdAndShardResolver
				.get_n_day_high_low_subindicator_names(null, null);
		Random r = new Random();
		Integer counter = 0;
		for (int i = 0; i < 10000; i++) {
			String index_name = "ask";
			String substring = "ask";
			
			for (Map.Entry<String, String[]> entry : high_low_amounts
					.entrySet()) {
				Question question = new Question(substring + " "
						+ entry.getValue()[0]);
				question.getSuggest().put("input",
						substring + " " + entry.getValue()[0]);
				question.getSuggest().put("weight", r.nextInt(10));
				question.getSuggest().put("payload",
						new HashMap<String, Object>() {
							{
								put("oil", new String[] { "0.24","1.2","0.30" });
							}
						});
				IndexRequestBuilder irb = LocalEs.getInstance()
						.prepareIndex("tradegenie_titan", index_name);
				irb.setSource(mapper.writeValueAsBytes(question));
				BulkPayLoad blk = new BulkPayLoad("test", counter.toString());
				ei.getRemote_Bulk_processor().add(irb.request(), blk);
				counter++;
			}
			System.out.println("doing :" + counter);
		}

		ei.getRemote_Bulk_processor().awaitClose(1000, TimeUnit.SECONDS);

	}

	public static void tm_ex() {
		for (int t = 0; t < 1000; t++) {
			TreeMap<Integer, Integer> first_map = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> second_map = new TreeMap<Integer, Integer>();

			for (int i = 0; i < 6000; i++) {
				if (i % 2 == 0) {
					first_map.put(i, i);
				} else {
					second_map.put(i, i);
				}
			}

			for (Map.Entry<Integer, Integer> entry : first_map.entrySet()) {
				int l = second_map.higherKey((entry.getKey()));
			}
		}
	}

	public static void comp_ex() {
		int[] arr_c_one = PairArray.get_random_test_array(3000, 1, 1, 3000, 0);
		int[] arr_c_two = PairArray.get_random_test_array(3000, 1, 1, 3000, 0);
		for (int i = 0; i < 1000; i++) {
			int[] arr_d_one = PairArray
					.get_decompressed_array_from_compressed_array(arr_c_one,
							3000, 1);
			int[] arr_d_two = PairArray
					.get_decompressed_array_from_compressed_array(arr_c_two,
							3000, 1);
			for (int ii = 0; ii < arr_d_one.length; ii++) {
				int j = arr_d_two[ii];
			}
		}
	}

	/****
	 * TESTED TAKES THE GIVEN UNCOMPRESSED ARRAY AND COMPRESSES IT DEPENDING ON
	 * ITS TYPE.
	 * 
	 * 
	 * @param data
	 * @param type
	 *            : 0 uncompressed, 1 compressed
	 * @return
	 */
	public static int[] get_compressed_int_array(int[] data) {

		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());

		int[] compressed = new int[data.length + 1024];

		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);

		System.out.println("compressed from " + data.length * 4 / 1024
				+ "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");

		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		System.out.println("invoked compression of sorted array:");
		return compressed;

	}

	/****
	 * given a compressed array, and its type,(whether unsorted or sorted)
	 * returns a decompressed array.
	 * 
	 * 
	 * @param compressed_arr
	 * @param expected_length_of_decompressed_array
	 * @param type
	 * @return
	 */
	public static int[] get_decompressed_array_from_compressed_array(
			int[] compressed_arr, int expected_length_of_decompressed_array) {

		int[] recovered = new int[expected_length_of_decompressed_array];

		// sorted
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		recovered = new int[expected_length_of_decompressed_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed_arr, new IntWrapper(0),
				compressed_arr.length, recovered, recoffset);
		System.out
				.println("invoking decompression of sorted array and returned \n");

		return recovered;

	}

	public static int[] get_arr(int[] sparse_arr, Integer buy_or_sell,
			int[] sorted_compressible_arr) {

		int max_day_id = sparse_arr[sparse_arr.length - 1];
		int init_fill = 0;
		if (sorted_compressible_arr == null) {
			sorted_compressible_arr = new int[max_day_id + 1];
		} else {
			init_fill = sorted_compressible_arr[sorted_compressible_arr.length - 1];
			sorted_compressible_arr = Arrays.copyOf(sorted_compressible_arr,
					sparse_arr[sparse_arr.length - 1] + 1);
		}

		int index = 0;

		for (int day_id : sparse_arr) {
			//System.out.println("day id is:" + day_id);

			// int prev_day_id = index - 1 < 0 ? 0 : sparse_arr[index - 1];
			if (sorted_compressible_arr[sorted_compressible_arr.length - 1] != 0) {
				//System.out.println("last element is not zero");
			}
			int prev_day_id = index == 0 ? init_fill : sparse_arr[index - 1];

			//System.out.println("index is :" + index);
			//System.out.println("prev day id is:" + prev_day_id);

			int fill_with = buy_or_sell.intValue() == 0 ? prev_day_id : day_id;

			//System.out.println("fill with is:" + fill_with);

			int s = (index == 0) ? prev_day_id : prev_day_id + 1;
			for (int i = s; i < day_id; i++) {
				//System.out.println("filling:" + i);
				sorted_compressible_arr[i] = sorted_compressible_arr[i] != 0 ? sorted_compressible_arr[i]
						: fill_with;
			}

			sorted_compressible_arr[day_id] = day_id;

			index++;
		}

		return sorted_compressible_arr;
	}

}
