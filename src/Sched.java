import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import Indicators.JedisPopper;

import yahoo_finance_historical_scraper.StockConstants;

public class Sched {
	
	
	private static CountDownLatch messageReceivedLatch = new CountDownLatch(1);
	private static CountDownLatch closeProgram = new CountDownLatch(1);
	public static ScheduledFuture<?> t;
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		//System.out.println("hello");
		//System.exit(1);
		//sched();
		//test_internal_shutdown();
		//System.out.println("this should be seen" +
		//		", now sleeping for 10 seconds before triggering the shutdown");
		//Thread.sleep(5000);
		//messageReceivedLatch.countDown();
		//closeProgram.await();
		BigDecimal bd = new BigDecimal("nil");
		System.out.println(bd.toString());
	
	}
	
	public static void test_internal_shutdown(){
		
		//so while starting we have to set the main program to stop
		//then we have to set the jedis poller to proceed.
		//then we go ahead with below.
		
		//beyond that we have to put the main program into an endless while loop,  at 
		//its end, continuously polling the redis key for signal for the main program.
		
		//if the scheduled executor service has an exception anywhere it logs, and tells the main
		//program to continue.
		
		//if that fails, i get an email.
		
		
		final ScheduledExecutorService scheduledExecutorService =
		        Executors.newScheduledThreadPool(StockConstants.total_threads_to_use_for_jedis_popper);
		t = scheduledExecutorService.scheduleAtFixedRate(new RunnableTest(scheduledExecutorService), StockConstants.initial_delay_for_jedis_popper_schedule_executor,
		StockConstants.schedule_for_jedis_popper_schedule_executor,
	    StockConstants.timeunit_for_jedis_popper_schedule_executor);
		
	}
	
	public static void sched(){
		final ScheduledExecutorService scheduledExecutorService =
		        Executors.newScheduledThreadPool(5);
		
		t = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {	
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("hello");
				if(messageReceivedLatch.getCount() == 0){
					System.out.println("the latch has been counted, exiting.");
					scheduledExecutorService.shutdown();
					closeProgram.countDown();
				}
			}
		}, StockConstants.initial_delay_for_jedis_popper_schedule_executor,
		StockConstants.schedule_for_jedis_popper_schedule_executor,
	    StockConstants.timeunit_for_jedis_popper_schedule_executor);
		
		
		
	}
	
	public static void schedule_popper(){
		
		
		
		
		
	}
	
	public static void scheduled_task_thread(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("hello");
				try {
					ScheduledExecutorService scheduledExecutorService =
					        Executors.newScheduledThreadPool(5);
					
					ScheduledFuture t = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {	
						@Override
						public void run() {
							// TODO Auto-generated method stub
							System.out.println("hello");
							
						}
					}, 1, 1, TimeUnit.SECONDS);
					
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	

}
