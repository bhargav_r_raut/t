import static org.msgpack.template.Templates.TString;
import static org.msgpack.template.Templates.tList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.unpacker.Unpacker;

import com.squareup.okhttp.Response;
import com.tradegenie.tests.BaseTest;
import com.tradegenie.tests.ESTestObject;
import com.tradegenie.tests.ShScript;

import DailyOperationsLogging.LoggingDailyOperations;
import ExchangeBuilder.CentralSingleton;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import dailyDownload.AppendCompanyInformation;
import dailyDownload.IndiceComponents;
import elasticSearchNative.RemoteEs;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.ComponentScraper;
import yahoo_finance_historical_scraper.StockConstants;

public class SimplyRun {
	/**
	 * 0:subindicator_rises_for_2_sessions 1:subindicator_shows_up_up_pattern
	 * 2:subindicator_goes_above_critical_line 3:subindicator_3_session_high
	 * 4:subindicator_shows_up_down_pattern
	 * 5:subindicator_above_signal_line_for_1_session
	 * 6:subindicator_above_zero_line_for_1_session
	 * 7:subindicator_rate_of_change_amount_rising_for_2_sessions
	 * 8:subindicator_0.25_percent_above_signal_line
	 * 9:subindicator_bullish_divergence 10:subindicator_above_5_session_sma
	 * 11:subindicator_sessions_above_5_session_sma 12:subindicator_value
	 * 13:subindicator_5_session_sma_crosses_above_subindicator_9_session_sma
	 * 14:subindicator_closes_up_for_the_week
	 * 15:subindicator_rises_by_0.25_percent_in_1_session
	 * 
	 * @throws Exception
	 */

	protected static void generate_company_types() throws Exception {
		IndiceComponents comp = IndiceComponents.getInstance();
		for (int i = 2; i < 3; i++) {
			ComponentScraper scr = ComponentScraper.getInstance(i);
			// System.out.println(scr.getIndex_names_and_stock_names());
			// System.in.read();
			ArrayList<String> stocknames = scr.getIndex_names_and_stock_names().get(StockConstants.indices[i]);

			Integer counter = 0;
			ArrayList<String> spair = new ArrayList<String>();

			for (String name : stocknames) {

				spair.add(name);

				counter++;
				if (counter == 2) {
					// System.out.println("entered counter as two:" + counter);
					comp.addstockToIndexInMap(StockConstants.indices_main[i], spair.get(0), spair.get(1));

					counter = 0;
					spair = new ArrayList<String>();
					LoggingDailyOperations.indices_and_components.put(StockConstants.indices_main[i], spair);

				}
			}

			// System.out.println(comp.getIndice_components());
			// System.out.println(comp.getIndice_components().get(StockConstants.indices_main[i]));
			// System.in.read();

			AppendCompanyInformation api = new AppendCompanyInformation(scr.getCompany_information(),
					StockConstants.indices_main[i], null, null);
			api.addStockInformationToGraph();
		}
	}

	public static void test_messagepack() {
		String hash_name = "test_hash";
		String hash_key_name = "test_hash_key";
		MessagePack mpack = new MessagePack();
		Template<List<String>> listTmpl = tList(TString);
		List<String> ls = new ArrayList<String>(Arrays.asList("doggy", "catty"));

		try {
			byte[] wr = mpack.write(ls);

			try (Jedis jedis = RedisManager.getInstance().getJedis()) {
				jedis.hset(hash_name.getBytes(), hash_key_name.getBytes(), wr);
				byte[] wrbyte = jedis.hget(hash_name.getBytes(), hash_key_name.getBytes());
				ByteArrayInputStream in = new ByteArrayInputStream(wrbyte);
				Unpacker unpacker = mpack.createUnpacker(in);
				List<String> dstList = unpacker.read(listTmpl);

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected static Double get_rounded_price_change(Double price_change) {

		ArrayList<Double> rounded_number = new ArrayList<Double>();

		rounded_number.add(Math.abs(5.0 * (Math.round(price_change / 5.0))));
		rounded_number.add(Math.abs(2.0 * (Math.round(price_change / 2.0))));
		Collections.sort(rounded_number);
		if (Arrays.binarySearch(StockConstants.rise_or_fall_amounts_for_price_change_percentage_diff,
				price_change) >= 0) {
			return price_change;
		} else if (Arrays.binarySearch(StockConstants.rise_or_fall_amounts_for_price_change_percentage_diff,
				rounded_number.get(0)) >= 0) {
			return rounded_number.get(0);
		} else {
			return rounded_number.get(1);
		}

	}

	
	/**
	**/

	protected static Double find_nearest(Double search, Double[] arr) {

		Double closest = null;
		for (Double item : arr) {
			if ((closest == null) || (Math.abs(search - closest) > Math.abs(item - search))) {
				closest = item;
			}
		}
		System.out.println(closest);
		return closest;
	}

	public static String prepare_string(String input_string) {

		input_string = input_string.toLowerCase().trim().replaceAll("\\s([Pp][Ll][Cc]|[Gg]roup|[Cc]o|[Ii]nc)", " ")
				.replaceAll("^\\s|^\\n|^\\t|^\\r", "").replaceAll("\\s$|\\n$|\\t$|\\r$", "").replaceAll("\\.|\\,", "");
		return input_string;

	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		//ReadWriteTextToFile.getInstance().getTextFromUrl("http://10.255.255.1/helo");
		CentralSingleton.getInstance().initializeSingleton();
		System.out.println(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey());
	}

	public static void read_es_test_object() {
		try {

			ObjectMapper mapper = new ObjectMapper();

			SearchResponse sr = RemoteEs.getInstance()
					.prepareSearch("tradegenie_titan")
					.setTypes(StockConstants.ESTypes_test)
					.setQuery(QueryBuilders.matchAllQuery())
					.setFetchSource(true)
					.execute().actionGet();

			for (SearchHit hit : sr.getHits().getHits()) {

				//System.out.println("test_document id:");
				//System.out.println(hit.getId());
				if (hit.getSource().containsKey("session_id")) {
					ESTestObject et = mapper.readValue(hit.getSourceAsString(), ESTestObject.class);

					TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
					};

					HashMap<String, String> failed_entities = 
							mapper.readValue(et.getStringified_object(), typeRef);

					///////////////////////////
					//
					//////////////////////////

					SearchResponse response = RemoteEs.getInstance()
							.prepareSearch("tradegenie_titan")
							.setTypes(StockConstants.ESTypes_error_log)
							.setQuery(QueryBuilders.matchAllQuery())
							.setFetchSource(true)
							.setSize(600)
							.execute().actionGet();

					for (SearchHit hitt : response.getHits().getHits()) {

						String unique_name = (String) hitt.getSource()
								.get("entity_unique_name");
						
						if(failed_entities.containsKey(unique_name)){
							System.out.println("it was there in failed entities.");
						}
						else{
							System.out.println("it was not there.");
						}
						
					}
					
					if(failed_entities.size() == response.getHits().hits().length){
						System.out.println("works");
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void write_es_test_object() {
		try {
			ObjectMapper mapper = new ObjectMapper();

			BaseTest bs = new BaseTest();

			bs.setUpBeforeClass();

			bs.setUp();

			ESTestObject es_test_object = new ESTestObject(10l,
					mapper.writeValueAsString(new HashMap<String, String>() {
						{
							put("test", "test");
						}
					}));

			IndexResponse in_r = RemoteEs.getInstance().prepareIndex().setIndex("tradegenie_titan")
					.setType(StockConstants.ESTypes_test).setSource(mapper.writeValueAsString(es_test_object)).execute()
					.actionGet();

			System.out.println(in_r.getId());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}
