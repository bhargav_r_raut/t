

import java.util.HashMap;

public class StopLossAndPriceChangeUpdateRequest {
	
	private HashMap<String,Integer> arrs_and_start_indices;

	public HashMap<String, Integer> getArrs_and_start_indices() {
		return arrs_and_start_indices;
	}

	public void setArrs_and_start_indices(
			HashMap<String, Integer> arrs_and_start_indices) {
		this.arrs_and_start_indices = arrs_and_start_indices;
	}
	
	/***
	 * 
	 * for jackson.
	 */
	public StopLossAndPriceChangeUpdateRequest(){
		
	}
}
