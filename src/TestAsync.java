import java.util.GregorianCalendar;
import java.util.Random;

import org.apache.cassandra.cli.CliParser.newColumnFamily_return;
import org.apache.commons.math3.stat.inference.TTest;


public class TestAsync {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random r = new Random();
		double[] sample1 = new double[3000];
		for(int i=0; i < 3000; i++){
			sample1[i] = r.nextDouble();
		}
		double[] sample2 = new double[1500];
		for(int i=0; i < 400; i++){
			sample2[i] = r.nextDouble();

		}
		GregorianCalendar start = new GregorianCalendar();
		for(int i=0; i < 10000; i++){
			Double t = new TTest().homoscedasticT(sample1, sample2);
		}
		GregorianCalendar end = new GregorianCalendar();
		System.out.println("for 1000 iterations it took: " + (end.getTimeInMillis() - start.getTimeInMillis()));
	}

}
