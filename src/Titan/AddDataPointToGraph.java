package Titan;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

import yahoo_finance_historical_scraper.StockConstants;

import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.TransactionalGraph;



public class AddDataPointToGraph {

	// get a threadpoolexecutor instance.

	private ThreadPoolExecutor TpoolExec;
	private TransactionalGraph g;
	private TitanGraph tg;
	public TitanGraph getTg() {
		return tg;
	}

	public void setTg(TitanGraph tg) {
		this.tg = tg;
	}

	private ThreadPoolRejectHandler handler;
	private CompletionService<String> TpoolCompl;
	private int totalTasksadded;
	private boolean use_transactional_graph;
	
	/**
	 * if you pass in threads and queue length as null,
	 * it will be assigned from stockconstants.
	 * 
	 * @param threads
	 * @param queue_length
	 */
	public AddDataPointToGraph(Integer threads, Integer queue_length){
		if(threads!=null && queue_length!=null){
			
		}
		else{
			threads = StockConstants.number_of_threads_for_complex_calculation_thread_pool;
			System.out.println("total threads which will be used are:" + threads);
			queue_length = StockConstants.maximum_queue_size_for_complex_calculation_thread_pool_executor;
		}
		this.TpoolExec = new BlockingExecutor(threads, queue_length);
		this.TpoolCompl = new ExecutorCompletionService<String>(this.TpoolExec);
	}
	
	public AddDataPointToGraph(TitanGraph g, boolean use_transactional_graph) {
		
		//BlockingQueue q = new ArrayBlockingQueue<>(1);
		
		this.TpoolExec = new ThreadPoolExecutor(2, 3, 1000L,
				TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
				new SimpleThreadFactory(), new ThreadPoolRejectHandler(1));
		 
		 // this.TpoolExec = new BlockingExecutor(2, 2);
		
		
		if (use_transactional_graph) {
			System.out.println("we are using transactional graph.");
			this.g = g.newTransaction();
			
		} else {
			System.out.println("we are using single threads");
			this.tg = g;
		}
		this.handler = (ThreadPoolRejectHandler) this.TpoolExec
				.getRejectedExecutionHandler();
		this.TpoolCompl = new ExecutorCompletionService<String>(this.TpoolExec);
		this.totalTasksadded = 0;
		this.use_transactional_graph = use_transactional_graph;
	}

	public ThreadPoolExecutor getTpoolExec() {
		return TpoolExec;
	}

	public void setTpoolExec(ThreadPoolExecutor tpoolExec) {
		TpoolExec = tpoolExec;
	}

	public TransactionalGraph getG() {
		return g;
	}

	public CompletionService<String> getTpoolCompl() {
		return TpoolCompl;
	}

	public void setTpoolCompl(CompletionService<String> tpoolCompl) {
		TpoolCompl = tpoolCompl;
	}
	
	public void addtask(){
		TpoolCompl.submit(new DummyTask());
	}
	
	public void CreateVertex(Map<String, Object> m,String specific_graph_job,JSONObject additional_params) {
		if(use_transactional_graph){
		TpoolCompl.submit(new TitanWorkerThread(g, TpoolExec, m,
				specific_graph_job, use_transactional_graph,additional_params));
		}
		else{
			TpoolCompl.submit(new TitanWorkerThread(tg, TpoolExec, m,
					specific_graph_job, use_transactional_graph,additional_params));
		}
		this.setTotalTasksadded(this.getTotalTasksadded() + 1);
	}

	public int getTotalTasksadded() {
		return totalTasksadded;
	}

	public void setTotalTasksadded(int totalTasksadded) {
		this.totalTasksadded = totalTasksadded;
	}

}
