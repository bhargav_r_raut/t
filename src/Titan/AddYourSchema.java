package Titan;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.thinkaurelius.titan.core.EdgeLabel;
import com.thinkaurelius.titan.core.Multiplicity;
import com.thinkaurelius.titan.core.Order;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.VertexLabel;
import com.thinkaurelius.titan.core.attribute.Decimal;
import com.thinkaurelius.titan.core.attribute.Precision;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.thinkaurelius.titan.core.schema.TitanSchemaElement;
import com.tinkerpop.blueprints.Direction;

import dailyDownload.StockSchemaGenerator;



public class AddYourSchema implements TitanConstants{
	private TitanGraph g;
	private ConcurrentHashMap<String,PropertyKey> cpk;
	public AddYourSchema(TitanGraph g){
		this.g = g;
		this.cpk = new ConcurrentHashMap<String, PropertyKey>();
	}
	
	public void change_your_schema() throws IOException{
		
		TitanManagement mgmt = this.g.getManagementSystem();
		File fin = new File("schema_changes.txt");
		
		FileInputStream fis = new FileInputStream(fin);
		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
		Pattern p = Pattern.compile("([A-Za-z\\-_]+),([A-Za-z\\-_]+)");
		
		String line = null;
		while ((line = br.readLine()) != null) {
			Matcher m = p.matcher(line);
			while(m.find()){
				System.out.println("change this--->" + m.group(1) + "with " + m.group(2));
				if(mgmt.containsRelationType(m.group(1))){
					System.out.println("done correctly");
					System.out.println(mgmt.getPropertyKey(m.group(1)));
					System.out.println(mgmt.containsRelationType(m.group(2)));
					TitanSchemaElement oldname = mgmt.getPropertyKey(m.group(1));
					mgmt.changeName(oldname,"booboo");
				}
				else{
					System.out.println("failed");
					System.out.println("this ---> " + m.group(1) + "does not exist.");
				}
			}
		}	
		br.close();
		mgmt.commit();
		g.commit();
		//FileWriter writer = new FileWriter(new File("schema_changes.txt"), false);
		//writer.append("");
		//writer.close();
		
	}
	
	public void add_labels(){
		TitanGraph graph = this.g;
		System.out.println("graph opened successfully");
		TitanGraphSchema schema = new TitanGraphSchema(SEARCH_INDEX_NAME);
		TitanManagement mgmt = graph.getManagementSystem();
		
		
		VertexLabel indicator = schema.create_vertex_label("indicator", mgmt);
		VertexLabel subindicator = schema.create_vertex_label("subindicator", mgmt);
		VertexLabel entity_indicator_subindicator_complex = 
				schema.create_vertex_label("entity_indicator_subindicator_complex", mgmt);
		VertexLabel registry = schema.create_vertex_label("registry", mgmt);
		VertexLabel common_day_id_vertex_label = 
				schema.create_vertex_label("common_day_id_vertex_label", mgmt);
		
		EdgeLabel edgelabel = schema.create_edge_label("day_connector", false, null, mgmt, null);
		EdgeLabel monitor_with = schema.create_edge_label("monitor_indicator_with",
				true, null, mgmt, null);
		EdgeLabel cross_with = schema.create_edge_label("cross_indicator_with",
					true, null, mgmt, null);
		EdgeLabel applicable_to_subindicator = schema.create_edge_label("applicable_to_subindicator",
					false, null, mgmt, null);
		EdgeLabel group = schema.create_edge_label("subindicator_group",
				true, null, mgmt, null);
		EdgeLabel sub_group = schema.create_edge_label("subindicator_subgroup",
				true, null, mgmt, null);
		EdgeLabel opposite_group = schema.create_edge_label("subindicator_opposite_group",
				true, null, mgmt, null);
		EdgeLabel opposite_subgroup = schema.create_edge_label("subindicator_opposite_subgroup",
				true, null, mgmt, null);
		EdgeLabel opposite_specific_subindicator = schema
				.create_edge_label("subindicator_opposite_specific_subindicator",
				true, null, mgmt, null);
		EdgeLabel entity_parent_indicator = 
				schema.create_edge_label("entity_parent_indicator", false, Multiplicity.SIMPLE, mgmt,
						null);
		EdgeLabel entity_parent_subindicator = 
				schema.create_edge_label("entity_parent_subindicator", false, Multiplicity.SIMPLE, mgmt, null);
		EdgeLabel complex = 
				schema.create_edge_label("complex", false, Multiplicity.MULTI, mgmt, null);
		EdgeLabel common_day_id_edge_label = 
				schema.create_edge_label("common_day_id_edge_label",
						false, Multiplicity.MULTI, mgmt, null);
		
		mgmt.commit();
	}
	
	
	
	
	public void update_schema() throws Exception{
		TitanGraph graph = this.g;
		System.out.println("graph opened successfully");
		TitanGraphSchema schema = new TitanGraphSchema(SEARCH_INDEX_NAME);
		TitanManagement mgmt = graph.getManagementSystem();
		
		
			/**
			 * dummy key created here.
			 * 
			 * 
			 */
			
			schema.create_key("multi_key",
					"Vertex",
					true,
					false,
					false,
					String.class,
					true,
					null,
					graph, mgmt, this.cpk);
		
			/**
			 * dummy key creation ends.
			 * 
			 * 
			 */
		
			schema.create_key("close", "Vertex", true, false, true, Decimal.class,false,null, graph,mgmt,this.cpk);
			schema.create_key("epoches", "Vertex", true, false, true, Long.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("stockName", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("open", "Vertex", true, false, true, Decimal.class,true, null, graph,mgmt,this.cpk);
			schema.create_key("high", "Vertex", true, false, true, Decimal.class,true, null, graph,mgmt,this.cpk);
			schema.create_key("low", "Vertex", true, false, true, Decimal.class,true, null, graph,mgmt,this.cpk);
			schema.create_key("actual_close", "Vertex", true, false, true, Decimal.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("volume", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);	
			schema.create_key("entityType", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("dates", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("timezoneString","Vertex",true,false,true,String.class,true, null, graph,mgmt,this.cpk);
			schema.create_key("indice", "Vertex", true, false, true, String.class,true, null, graph,mgmt,this.cpk);
			schema.create_key("stockFullName", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("tableName", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("unique_identifier", "Vertex", true, true, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("unique_identifier", "Edge", true, true, true, String.class, true, null, graph, mgmt, this.cpk);
			schema.create_key("day_of_month", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("day_of_week", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("month", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("week_of_month", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("week_of_year", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("month_of_year", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("quarter_of_year", "Vertex", true, false, true, Integer.class,true,null, graph,mgmt,this.cpk);
			schema.create_key("half_of_year", "Vertex", true, false, true, String.class,true,null, graph,mgmt,this.cpk);
			
			/*
			 * 
			 * this is also stored as a comma seperated string.
			 * 
			 */
			schema.create_key("company_description", "Vertex", true, false, true, String.class, true,null, graph,mgmt,this.cpk);
			/**
			 * this is also stored as a comma seperated list.
			 * otherwise take the only workd 
			 * 
			 */
			schema.create_key("industry", "Vertex", true, false, true, String.class, true,null, graph,mgmt,this.cpk);
			/*
			 * this is stored as a comma seperated string, 
			 * split by commas to get the individual sectors.
			 * 
			 */
			schema.create_key("sector", "Vertex", true, false, true, String.class, true, null, graph,mgmt,this.cpk);
			schema.create_key("full_time_employees", "Vertex", true, false, true, Integer.class, true, null, graph,mgmt,this.cpk);
			schema.create_key("daily_indicators_json_string", "Vertex", true, false, false, String.class, true, null, graph,mgmt,this.cpk);
			schema.create_key("ohlcv_type", "Vertex", true, false, true, String.class, true, null, graph,mgmt,this.cpk);
			/*
			 * the total number of whole days since the epoch.
			 * 
			 */
			schema.create_key("day_id", "Vertex", true,
					false, true,
					Integer.class, true, null, g, mgmt, this.cpk);
			
			
		

			
			/**
			 * 
			 * day connector edge label.
			 * edge label names are always defined in their respective classes.
			 * 
			 */
			EdgeLabel edgelabel = schema.create_edge_label("day_connector", false, Multiplicity.MULTI, mgmt, null);
			
			
			
			/**
			 * 
			 * day connector property keys.
			 * what all should be stored on the day connector?
			 * 
			 *  1. profit/loss in percentage terms.
			 *  2. days
			 *     #create index individually for these two and also for them together. 
			 *  3. profit/loss binary terms
			 * 
			 */
			schema.create_key("days_back", "Edge", true, false, true, Integer.class, true, edgelabel, graph, mgmt, this.cpk);
			schema.create_key("price_change_percentage", "Edge", true, false, true, Precision.class, true, edgelabel, graph, mgmt, cpk);
			//1->profit, -1->loss, 0->no change
			schema.create_key("profit_or_loss", "Edge", true, false, true, Integer.class,
					true, edgelabel, graph, mgmt, this.cpk);
			
			//System.out.println("now starting to create the stock specific schema.");
			StockSchemaGenerator stock_sch_gen =
					new StockSchemaGenerator(edgelabel, graph, mgmt, schema, cpk);
			//System.out.println("inspect the propertykeys:");
			//System.out.println(this.cpk.toString());
			//System.in.read();
			
			/**
			 * create vertex centric indices for the edges based on these labels.
			 * 
			 * 
			 * 
			 */
			schema.create_vertex_centric_index_on_edgelabel(mgmt, "days_connector_on_days", "days_back",
					new PropertyKey[]{this.cpk.get("days_back")},
					Direction.BOTH, Order.ASC, edgelabel);
			
			schema.create_vertex_centric_index_on_edgelabel(mgmt,
					"days_connector_on_price_change_percentage",
					"price_change_percentage", 
					new PropertyKey[]{this.cpk.get("price_change_percentage")},
					Direction.BOTH, Order.ASC, edgelabel);
			
			schema.create_vertex_centric_index_on_edgelabel(mgmt,
					"days_connector_on_profit_or_loss",
					"profit_or_loss", 
					new PropertyKey[]{this.cpk.get("profit_or_loss")},
					Direction.BOTH, Order.DEFAULT, edgelabel);
			 /**
			  * INDICATOR SCHEMA
			  */
			
			/**
			 * INDICATOR LABEL AND ITS PROPERTIES.
			 * 
			 */
			VertexLabel indicator = schema.create_vertex_label("indicator", mgmt);
			PropertyKey indicator_name = schema.create_key("indicator_name",
					"Vertex",
					true,
					true,
					true,
					String.class,
					true,
					indicator, graph, mgmt, this.cpk);
			PropertyKey indicator_periods = schema.create_key("periods", "Vertex", 
					true, false, true, String.class, true, indicator, graph, mgmt, this.cpk);
			PropertyKey indicator_ohlcv_groups = schema.create_key("ohlcv_groups",
					"Vertex", true, false, false, String.class, true, indicator, graph, mgmt, this.cpk);
			PropertyKey indicator_critical_line_limits = schema.create_key("critical_line_limits",
					"Vertex", true, false, false, Precision.class, true, indicator, graph, mgmt,
					this.cpk);
			//the monitor with and cross with are edge labels which point to particular other indicators.
			EdgeLabel monitor_with = schema.create_edge_label("monitor_indicator_with",
				true, null, mgmt, null);
			EdgeLabel cross_with = schema.create_edge_label("cross_indicator_with",
					true, null, mgmt, null);
			EdgeLabel applicable_to_subindicator = schema.create_edge_label("applicable_to_subindicator",
					false, null, mgmt, null);
			PropertyKey when_was_this_indicator_applied_to_this_subindicator = schema.
					create_key("when_was_indicator_applied_to_subindicator",
							"Edge", true, false,
							true,
							Long.class,
							true,
							applicable_to_subindicator, graph, mgmt, this.cpk);
			
			/**
			 * 
			 * subindicator label and its properties.
			 * 
			 * 
			 */
			
			VertexLabel subindicator = schema.create_vertex_label("subindicator", mgmt);
			PropertyKey subindicator_name = schema.create_key("subindicator_name",
					"Vertex",true, false,
					true, String.class,true,
					subindicator, graph, mgmt,
					this.cpk);
			
				/**
				 * 
				 * subindicator group.
				 */
				//each subindicator is associated with others in groups
				//each group has an id, which corresponds to the index number
				//in the arraylist of the subindicator object self_groups_arraylist.
				//it has affixed to it the timestamp.
				EdgeLabel group = schema.create_edge_label("subindicator_group",
						true, null, mgmt, null);
				PropertyKey group_id = schema.
						create_key("subindicator_group_id",
								"Edge", true, false,
								true, Integer.class,
								true,
								null, graph, mgmt, this.cpk);
				PropertyKey hex_id = schema.create_key("component_id",
						"Edge", true, false,
						true, Integer.class, true,
						null, graph, mgmt, this.cpk);
				/**
				 * ends.
				 * 
				 */
				
				
				
				/**
				 * subindicator sub group
				 * 
				 */
				EdgeLabel sub_group = schema.create_edge_label("subindicator_subgroup",
						true, null, mgmt, null);
				PropertyKey sub_group_id = schema
						.create_key("subindicator_subgroup_id",
								"Edge", true,
								false, true, Integer.class,
								true,
								sub_group, graph, mgmt, this.cpk);
				//group id applies to subindicator subgroup edge as well.
				/**
				 * ends
				 * 
				 */
				
				
				
				/**
				 * opposite group
				 * 
				 */
				EdgeLabel opposite_group = schema.create_edge_label("subindicator_opposite_group",
						true, null, mgmt, null);
				PropertyKey opposite_group_id = schema
						.create_key("subindicator_opposite_group_id",
								"Edge", true,
								false, true, Integer.class,
								true,
								opposite_group, graph, mgmt, this.cpk);
				//.
				/**
				 * ends
				 * 
				 */
				
				
				
				/**
				 * opposite subgroup.
				 * 
				 */
				EdgeLabel opposite_subgroup = schema.create_edge_label("subindicator_opposite_subgroup",
						true, null, mgmt, null);
				PropertyKey opposite_subgroup_id = schema
						.create_key("subindicator_opposite_subgroup_id",
								"Edge", true,
								false, true, Integer.class,
								true,
								opposite_subgroup, graph, mgmt, this.cpk);
				
				//id of the sub-group to which it is opposite.
				//id of opposite group to which this subgroup belongs(it is the index in teh array)
				/**
				 * ends
				 * 
				 */
				
				
				
				/**
				 * subindicator opposite specific subindicator.
				 * 
				 * 
				 */
				EdgeLabel opposite_specific_subindicator = schema
						.create_edge_label("subindicator_opposite_specific_subindicator",
						true, null, mgmt, null);
				PropertyKey opposite_specific_subindicator_id = schema
						.create_key("subindicator_opposite_specific_subindicator_id",
								"Edge", true,
								false, true, Integer.class,
								true,
								opposite_specific_subindicator, graph, mgmt, this.cpk);
				
				//id of its parent opposite group.
				//id of the sub-group to which it is opposite.
				//id of opposite group to which the parent opposite subgroup belongs(it is the index in teh array)
				
				/**
				 * ends
				 * 
				 */
				
				
				
			VertexLabel entity_indicator_subindicator_complex = 
					schema.create_vertex_label("entity_indicator_subindicator_complex", mgmt);
			PropertyKey entity_indicator_subindicator_name = 
					schema.create_key("entity_indicator_subindicator_unique_name",
							"Vertex",
						true,
						false,
						true,
						String.class,
						true,
						entity_indicator_subindicator_complex,
						graph, mgmt, this.cpk);
			
			PropertyKey entity_indicator_subindicator_stockName = 
					schema.create_key("entity_indicator_subindicator_stockName_name",
							"Vertex",
						true,
						false,
						true,
						String.class,
						true,
						entity_indicator_subindicator_complex,
						graph, mgmt, this.cpk);
			
			PropertyKey entity_indicator_subindicator_stockFullName = 
					schema.create_key("entity_indicator_subindicator_stockFullName_name",
							"Vertex",
						true,
						false,
						true,
						String.class,
						true,
						entity_indicator_subindicator_complex,
						graph, mgmt, this.cpk);
			
			PropertyKey entity_indicator_subindicator_indice = 
					schema.create_key("entity_indicator_subindicator_indice_name",
							"Vertex",
						true,
						false,
						true,
						String.class,
						true,
						entity_indicator_subindicator_complex,
						graph, mgmt, this.cpk);
			
			
			
			PropertyKey elasticsearch_mapping = schema.create_key("entity_es_object_id",
					"Vertex",
					true,
					false,
					true,
					String.class,
					true,
					entity_indicator_subindicator_complex, graph, mgmt,
					this.cpk);
			
			EdgeLabel entity_parent_indicator = 
					schema.create_edge_label("entity_parent_indicator", false, Multiplicity.SIMPLE, mgmt,
							null);
			EdgeLabel entity_parent_subindicator = 
					schema.create_edge_label("entity_parent_subindicator", false, Multiplicity.SIMPLE, mgmt, null);
			
			/**
			 * add mapping for actual calculation of the subindicators.
			 * 					
			 * 
			 * 				 complex_name : x , complex_value: y, 
			 * 					complex_day_id : same as vertex day id.
			 * stock vertex =============================================> complex_vertex
			 * 
			 */
			
			EdgeLabel complex = 
					schema.create_edge_label("complex", false, Multiplicity.MULTI, mgmt, null);
			
			PropertyKey complex_name = 
					schema.create_key("complex_name",
							"Edge",
							true,
							false,
							true,
							String.class,
							true,
							complex,
							graph, mgmt,
							this.cpk);
			
			
			
			PropertyKey complex_value = 
					schema.create_key("complex_value",
							"Edge",
							true,
							false,
							true,
							String.class,
							true,
							complex,
							graph, mgmt,
							this.cpk);
			
			PropertyKey complex_day_id =
					schema.create_key("complex_day_id",
							"Edge",
							true,
							false,
							true,
							String.class,
							true,
							complex,
							graph, mgmt,
							this.cpk);
			
			schema.create_vertex_centric_index_on_edgelabel(mgmt, "complex_day_id_index",
					"complex_day_id",
					new PropertyKey[]{this.cpk.get("complex_day_id")},
					Direction.BOTH,
					Order.ASC,
					complex);
			
			//schema.create_vertex_centric_index_on_edgelabel(mgmt, "days_connector_on_days", "days_back",
			//		new PropertyKey[]{this.cpk.get("days_back")},
			//		Direction.BOTH, Order.ASC, edgelabel);
			
			/**
			 * complex vertex mapping ends.
			 * -----------------------------------
			 * 
			 * ENDS.
			 * 
			 * ---------------------------------
			 */
			
			
			VertexLabel registry = schema.create_vertex_label("registry", mgmt);
			
			PropertyKey hash_string = 
					schema.create_key("registry_component_md5_hash",
							"Vertex",
							true,
							false,
							true,
							String.class,
							true,
							registry,
							graph, mgmt, this.cpk);
			
			PropertyKey counter = 
					schema.create_key("registry_counter",
							"Vertex",
							true,
							false,
							true,
							Integer.class,
							true,
							registry,
							graph, mgmt, this.cpk);
			
			PropertyKey component_jsonified = 
					schema.create_key("registry_component_jsonified",
							"Vertex",true,false,true,String.class,true,registry,graph,mgmt,this.cpk);
			
					
			/***
			 * EDGE FROM STOCK VERTEX TO COMMON DAY ID VERTEX.
			 * ALSO THE COMMON DAY ID VERTEX.
			 * 
			 */
			VertexLabel common_day_id_vertex_label = 
					schema.create_vertex_label("common_day_id_vertex_label", mgmt);
				
			
			
			PropertyKey common_day_id = 
					schema.create_key("common_day_id",
							"Vertex",
							true,
							false,
							true,
							Integer.class,
							true,
							null,
							graph, mgmt, this.cpk);
			
			EdgeLabel common_day_id_edge_label = 
					schema.create_edge_label("common_day_id_edge_label",
							false, Multiplicity.MULTI, mgmt, null);
			
			PropertyKey common_day_id_stockName = 
					schema.create_key("common_day_id_stockName",
							"Edge",
							false,
							false,
							true,
							String.class,
							true,
							common_day_id_edge_label,
							graph, mgmt, this.cpk);
			
			PropertyKey common_day_id_stockFullName = 
					schema.create_key("common_day_id_stockFullName",
							"Edge",
							false,
							false,
							true,
							String.class,
							true,
							common_day_id_edge_label,
							graph, mgmt, this.cpk);
			
			PropertyKey common_day_id_indice = 
					schema.create_key("common_day_id_indice",
							"Edge",
							false,
							false,
							true,
							String.class,
							true,
							common_day_id_edge_label,
							graph, mgmt, this.cpk);
			
			
			
			
			 /*
			 * build mixed indices here.
			 * query the hashmap for the key that you are looking for and add it to the requisite composite index
			 */
			
			/*
			mgmt.buildIndex("close_and_open", Vertex.class).
			addKey(cpk.get("close"),com.thinkaurelius.titan.core.schema.Parameter.of("mapped-name","close")).
			addKey(cpk.get("open"),com.thinkaurelius.titan.core.schema.Parameter.of("mapped-name", "open")).
			addKey(cpk.get("stockName")).
			addKey(cpk.get("stockFullName")).
			buildMixedIndex(TitanConstants.SEARCH_INDEX_NAME);			
			
			*/
			
			mgmt.commit();
			
			
			
			
			
			
		
		
		
	}
	
	/*
	 * 
	 * 
	public ArrayList<Integer> day_of_month;
	public ArrayList<String> day_of_week;
	public ArrayList<String> month;
	public ArrayList<Integer> week_of_month;
	public ArrayList<Integer> week_of_year;
	public ArrayList<Integer> month_of_year;
	public ArrayList<Integer> quarter_of_year;
	public ArrayList<String> half_of_year;
	
	 * 
	 * 
	 * 
	 * 
	 */
	
	
}
