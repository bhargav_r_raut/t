package Titan;

import java.util.concurrent.Callable;

public class DummyTask implements Callable {

	public DummyTask(){
		System.out.println("came to the constructor");
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		//System.out.println("now sleeping from thread: " + Thread.currentThread().getId());
		Thread.sleep(2000);
		return null;
	}

}
