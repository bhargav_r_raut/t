package Titan;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import DailyOperationsLogging.LoggingDailyOperations;
import Titan.MultiThreadable;
import Titan.PrepareObjectArrayListForMultiThreading;

import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import dailyDownload.MultiThreadGraphAddJob;

public class ExecuteMultiThreadedJob {
	
	private ArrayList<? extends MultiThreadable> object_array;
	private TitanGraph g;
	private TitanManagement mgmt;
	private String groovy_statements_function;
	private Boolean use_transactional_graph;
	private ArrayList<JSONObject> additional_params_additional;
	private PrepareObjectArrayListForMultiThreading prm;
	private ArrayList<TItanWorkerThreadResultObject> res;
	private static final String identifier_for_log = "Execute_Multi_Thread_Graph_Add_Job";
	
	public ArrayList<TItanWorkerThreadResultObject> getRes() {
		return res;
	}

	public void setRes(ArrayList<TItanWorkerThreadResultObject> res) {
		this.res = res;
	}

	public PrepareObjectArrayListForMultiThreading getPrm() {
		return prm;
	}

	public void setPrm(PrepareObjectArrayListForMultiThreading prm) {
		this.prm = prm;
	}

	public Boolean getUse_transactional_graph() {
		return use_transactional_graph;
	}

	public void setUse_transactional_graph(Boolean use_transactional_graph) {
		this.use_transactional_graph = use_transactional_graph;
	}

	public ArrayList<JSONObject> getAdditional_params_additional() {
		return additional_params_additional;
	}

	public void setAdditional_params_additional(
			ArrayList<JSONObject> additional_params_additional) {
		this.additional_params_additional = additional_params_additional;
	}

	public String getGroovy_statements_function() {
		return groovy_statements_function;
	}

	public void setGroovy_statements_function(String groovy_statements_function) {
		this.groovy_statements_function = groovy_statements_function;
	}

	public ArrayList<? extends MultiThreadable> getObject_array() {
		return object_array;
	}

	public void setObject_array(ArrayList<? extends MultiThreadable> object_array) {
		this.object_array = object_array;
	}

	public TitanGraph getG() {
		return g;
	}

	public void setG(TitanGraph g) {
		this.g = g;
	}

	public TitanManagement getMgmt() {
		return mgmt;
	}

	public void setMgmt(TitanManagement mgmt) {
		this.mgmt = mgmt;
	}


	/**
	 * JUST INVOKE CONSTRUCTOR, NOTHING ELSE NEEDS TO BE DONE.
	 * GET RESULTS OF THE MULTITHREADED JOB BY CALLING getRes
	 * 
	 * 
	 * JSONOBJECT TYPE IS org.json.jsonobject.
	 *  
	 *
	 * 
	 * 
	 * @param object_array
	 * @param g
	 * @param mgmt
	 * @param groovy_statements_function
	 * @param use_transactional_graph
	 * @param additional_params_additional
	 * @throws JSONException 
	 */
	public ExecuteMultiThreadedJob(
			ArrayList<? extends MultiThreadable> object_array, TitanGraph g,
			TitanManagement mgmt, String groovy_statements_function,
			Boolean use_transactional_graph,
			ArrayList<JSONObject> additional_params_additional){
		this.object_array = object_array;
		this.g = g;
		this.mgmt = mgmt;
		this.groovy_statements_function = groovy_statements_function;
		this.use_transactional_graph = use_transactional_graph;
		this.additional_params_additional = additional_params_additional;
		setRes(do_job());
	}

	public ArrayList<TItanWorkerThreadResultObject> do_job(){
		
		try {
			PrepareObjectArrayListForMultiThreading prm = 
					new PrepareObjectArrayListForMultiThreading(getObject_array(),
					getAdditional_params_additional(), getGroovy_statements_function(), false);
			try {
				
				
				MultiThreadGraphAddJob mlt = 
				new MultiThreadGraphAddJob(prm.req_hm,prm.additional_params,
					getG(),getMgmt(),prm.use_transactional_graph,prm.groovy_function);
				
				g.commit();
				return mlt.getResults_array();
				
			} catch (ClassNotFoundException | IllegalArgumentException
					| IllegalAccessException | InterruptedException
					| ExecutionException e) {
				// TODO Auto-generated catch block
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e,
						identifier_for_log, null);
				e.printStackTrace();
				return null;
			}
		} catch (IllegalArgumentException | IllegalAccessException | JSONException e1) {
			// TODO Auto-generated catch block
			LoggingDailyOperations.get_instance().add_to_log("exception",
					e1,
					identifier_for_log, null);
			e1.printStackTrace();
			return null;
		}
	}

}
