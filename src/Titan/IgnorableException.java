package Titan;

public class IgnorableException extends Exception{

	public IgnorableException(String message){
		super(message);
	}

}
