package Titan;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

public class MultiThreadable {
	
	public HashMap<String,Object> prepare_for_multithread() throws IllegalArgumentException, IllegalAccessException{
		HashMap<String,Object> hm = new HashMap<String, Object>();
		Field[] fields_list = this.getClass().getFields();
		for(Field f: fields_list){
			hm.put(f.getName(), f.get(this));
		}
		return hm;
	}
	
	public JSONObject prepare_jsonobject_from_private_fields() throws IllegalArgumentException, IllegalAccessException, JSONException{
		HashMap<String,Object> hm = new HashMap<String, Object>();
		Field[] fields_list = this.getClass().getFields();
		for(Field f: fields_list){
			hm.put(f.getName(), f.get(this));
		}
		
		JSONObject j_object = new JSONObject();
		for(Field f:this.getClass().getDeclaredFields()){
			if (hm.containsKey(f.getName())){
				//this is a public field.
			}
			else{
				f.setAccessible(true);
				j_object.put(f.getName(), f.get(this));
			}
		}
		return j_object;
	}
}
