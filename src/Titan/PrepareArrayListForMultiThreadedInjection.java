package Titan;

import java.util.ArrayList;
import java.util.HashMap;

public class PrepareArrayListForMultiThreadedInjection {

		private ArrayList<HashMap<String,Object>> full_list;
		
		/**
		 * @return the full_list
		 */
		public ArrayList<HashMap<String,Object>> getFull_list() {
			return full_list;
		}

		/**
		 * @param full_list the full_list to set
		 */
		public void setFull_list(ArrayList<HashMap<String,Object>> full_list) {
			this.full_list = full_list;
		}
	
		public HashMap<String,Object> return_empty_hashmap(){
			return new HashMap<String,Object>();
		}
	
		public PrepareArrayListForMultiThreadedInjection(){
			setFull_list(new ArrayList<HashMap<String,Object>>());
		}
}
