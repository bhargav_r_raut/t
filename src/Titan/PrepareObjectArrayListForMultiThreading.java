package Titan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class PrepareObjectArrayListForMultiThreading {
	public ArrayList<? extends MultiThreadable> ar;
	public ArrayList<HashMap<String,Object>> req_hm;
	public ArrayList<JSONObject> additional_params_additional;
	public ArrayList<JSONObject> additional_params;
	public Boolean use_transactional_graph;
	public String groovy_function;
	
	/**
	 * @param ar
	 * @param additional_params_additional: the additional params that you want to add for each 
	 * item in the arraylist, remember that all the private/protected fields of the class are automatically
	 * added to the additional params, so additional_params_additional is just a helper method to add anything
	 * external.
	 * @param use transactional graph : whether to use transactional graph or not.
	 * @param groovy_function: the string name of the groovy function that the multithreaded job is to execute.
	 * @throws JSONException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public PrepareObjectArrayListForMultiThreading(
			ArrayList<? extends MultiThreadable> ar,
			ArrayList<JSONObject> additional_params_additonal,
			String groovy_function,
			Boolean use_transactional_graph) throws IllegalArgumentException, IllegalAccessException, JSONException {
		this.ar = ar;
		this.req_hm = new ArrayList<HashMap<String,Object>>();
		this.additional_params = new ArrayList<JSONObject>();
		this.additional_params_additional = additional_params_additonal;
		this.use_transactional_graph = use_transactional_graph;
		this.groovy_function = groovy_function;
		prepare_arraylist();
	}
	
	public void prepare_arraylist() throws IllegalArgumentException, IllegalAccessException, JSONException
	{
		for(MultiThreadable mr: ar){
			this.req_hm.add(mr.prepare_for_multithread());
			this.additional_params.add(mr.prepare_jsonobject_from_private_fields());
		}
		
		//suppose the additional params additional has something, then add it to the requisite thing in the 
		//additional params arraylist.
		Integer additional_params_counter = 0;
		for( JSONObject obj:  additional_params_additional){
			Iterator iterator = obj.keys();
			while(iterator.hasNext()){
				String key = (String) iterator.next();
				this.additional_params.get(additional_params_counter).put(key, obj.get(key));
			}
			additional_params_counter++;
		}
		
		
	}
	
}
