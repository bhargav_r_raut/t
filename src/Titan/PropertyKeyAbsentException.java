package Titan;


public class PropertyKeyAbsentException extends Exception {

	public PropertyKeyAbsentException(String message){
		super(message);
	}

	
}
