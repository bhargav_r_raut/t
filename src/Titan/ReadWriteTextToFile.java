package Titan;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tradegenie.tests.DeChunkResponse;

import ExchangeBuilder.Entity;
import ExchangeBuilder.LogObject;
import RemoteComms.Job;
import RemoteComms.JobStatusResponse;
import RemoteComms.StopLossAndPriceChangeUpdateRequest;
import yahoo_finance_historical_scraper.StockConstants;

public class ReadWriteTextToFile {
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	public static ReadWriteTextToFile instance;
	private String fileName;
	private static ObjectMapper mapper;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public static ReadWriteTextToFile getInstance() {
		if (instance == null) {
			instance = new ReadWriteTextToFile();
		}
		return instance;
	}

	public ReadWriteTextToFile() {
		this.mapper = new ObjectMapper();
	}

	/**
	 * 
	 * THE PATH IS JUST THE FILENAME WITH EXTENSION IF THE FILE IS PLACED IN THE
	 * ROOT FOLDER OF THE PROJECT. the encoding is placed in teh titan constants
	 * file, as encoding.
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public void generate_filename(String input_variable) {
		this.fileName = String.valueOf(new GregorianCalendar().getTimeInMillis() + "__" + input_variable);
	}

	public static void writeFile(String fileName, String contents) throws IOException {

		FileUtils.writeStringToFile(new File(fileName), contents, false);
	}

	public static void writeFilefile(File file, String contents) throws IOException {
		FileUtils.writeStringToFile(file, contents, true);
	}

	public static BufferedReader stringToBufferedReader(String str) {

		// convert String into InputStream
		InputStream is = new ByteArrayInputStream(str.getBytes());

		// read it with BufferedReader
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		return br;
	}

	public static JobStatusResponse make_stop_loss_and_price_change_update_request(HashMap<String, Entity> entity_map,
			String job_id) throws Exception {

		StopLossAndPriceChangeUpdateRequest req = new StopLossAndPriceChangeUpdateRequest();
		req.setArrs_names_and_values(new HashMap<String, Integer>());
		for (Map.Entry<String, Entity> entry : entity_map.entrySet()) {
			req.getArrs_names_and_values()
					.putAll(entry.getValue().getStop_loss_and_price_change_arrays_with_first_modified_chunk_id());
		}
		req.setJob_id(job_id);
		return remote_job_request(mapper.writeValueAsString(req),
				StockConstants.EntityIndex_price_change_and_stop_loss_update_remote_url);
	}

	public static JobStatusResponse make_status_request(String job_id) throws Exception {
		Job req = new Job();
		req.setJob_id(job_id);
		return remote_job_request(mapper.writeValueAsString(req), StockConstants.EntityIndex_job_status_remote_url);
	}

	public static JobStatusResponse make_cancel_job_request(String job_id) throws Exception {

		Job req = new Job();
		req.setJob_id(job_id);
		return remote_job_request(mapper.writeValueAsString(req), StockConstants.EntityIndex_cancle_job_remote_url);

	}

	public static Boolean clear_remote_singleton_request(String url) throws Exception {
		LogObject lgd = new LogObject(ReadWriteTextToFile.class);
		lgd.add_to_info_packet("url", url);
		lgd.commit_debug_log("init:clear remote singleton request");
		OkHttpClient ok_http = new OkHttpClient();
		Request request = new Request.Builder().url(url).get().build();
		Response response = ok_http.newCall(request).execute();

		String response_string = response.body().string();
		
		System.out.println("response in ReadWriteTextToFile:" + response_string);
		
		if (response_string == null || response_string.isEmpty()) {
			return null;
		}

		JSONObject jsr = new JSONObject(response_string);

		return jsr.getBoolean("result");
	}

	/**
	 * @param req
	 *            : serialized job request.
	 * @param url
	 *            : the url to make the request to
	 * @description: this is the central method which actually makes the request
	 *               to the remote api and returns a jobstatusresponse it is
	 *               called by all the methods that talk to the remote api.
	 * 
	 **/
	public static JobStatusResponse remote_job_request(String req, String url) throws Exception {

		LogObject lgd = new LogObject(ReadWriteTextToFile.class);
		lgd.add_to_info_packet("url", url);
		lgd.add_to_info_packet("req", req);
		lgd.commit_debug_log("init:remote job");
		OkHttpClient ok_http = new OkHttpClient();
		RequestBody body = RequestBody.create(JSON, req);

		Request request = new Request.Builder().url(url).post(body).build();

		Response response = ok_http.newCall(request).execute();
		String response_string = response.body().string();
		System.out.println("response in ReadWriteTextToFile:" + response_string);
		if (response_string == null || response_string.isEmpty()) {
			return null;
		}
		JobStatusResponse jsr = mapper.readValue(response_string, JobStatusResponse.class);

		return jsr;
	}

	public static DeChunkResponse remote_dechunk_test_response(String req, String url)
			throws org.codehaus.jackson.JsonParseException, org.codehaus.jackson.map.JsonMappingException, IOException {
		LogObject lgd = new LogObject(ReadWriteTextToFile.class);
		lgd.add_to_info_packet("url", url);
		lgd.add_to_info_packet("req", req);
		lgd.commit_debug_log("init:remote job");
		OkHttpClient ok_http = new OkHttpClient();
		RequestBody body = RequestBody.create(JSON, req);

		Request request = new Request.Builder().url(url).post(body).build();

		Response response = ok_http.newCall(request).execute();
		String response_string = response.body().string();
		System.out.println("response in ReadWriteTextToFile:" + response_string);
		if (response_string == null || response_string.isEmpty()) {
			return null;
		}
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		DeChunkResponse jsr = mapper.readValue(response_string, DeChunkResponse.class);

		return jsr;
	}

	public static JobStatusResponse cancel_all_jobs() throws Exception {
		Job req = new Job();
		return remote_job_request(mapper.writeValueAsString(req),
				StockConstants.EntityIndex_cancel_all_jobs_remote_url);
	}

	public static JobStatusResponse get_semaphore_count() throws Exception {
		Job req = new Job();
		return remote_job_request(mapper.writeValueAsString(req),
				StockConstants.EntityIndex_get_semaphore_count_remote_url);
	}

	public static JobStatusResponse get_pending_jobs_count() throws Exception {
		Job req = new Job();
		return remote_job_request(mapper.writeValueAsString(req),
				StockConstants.EntityIndex_get_pending_jobs_remote_url);
	}

	public static Response getTextFromUrl(String url) throws IOException {

		OkHttpClient ok_http_client = new OkHttpClient();
		ok_http_client.setConnectTimeout(StockConstants.ok_http_connect_timeout, StockConstants.ok_http_tu); // connect
																												// timeout
		ok_http_client.setReadTimeout(StockConstants.ok_http_read_timeout, StockConstants.ok_http_tu); // socket
																										// timeout

		LogObject lgd = new LogObject(ReadWriteTextToFile.class);
		lgd.add_to_info_packet("url requested", url);
		lgd.commit_debug_log("logged");

		Builder request = new Request.Builder().url(url);

		request.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64)"
				+ " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.132 Safari/537.36");
		request.addHeader("Connection", "keep-alive");
		request.addHeader("Referer", "https://s.yimg.com/rq/darla/2-8-7/html/r-sf.html");
		request.addHeader("Accept-Language", "en-US,en;q=0.8");
		request.addHeader("Content-Encoding", "gzip,deflate,sdch");
		request.addHeader("Accept", "text/*; charset=utf-8");

		request = request.get();

		Request request_main = request.build();
		Response response = ok_http_client.newCall(request_main).execute();

		return response;

	}

	public static Response forex_request(String url) throws IOException {

		LogObject lgd = new LogObject(ReadWriteTextToFile.class);
		lgd.add_to_info_packet("url requested", url);
		lgd.commit_debug_log("logged");

		OkHttpClient ok_http_client = new OkHttpClient();
		ok_http_client.setConnectTimeout(StockConstants.ok_http_connect_timeout, TimeUnit.SECONDS); // connect
																									// timeout
		ok_http_client.setReadTimeout(StockConstants.ok_http_read_timeout, TimeUnit.SECONDS); // socket
																								// timeout
		Builder request = new Request.Builder().url(url);
		request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		request.addHeader("Accept-Encoding", "gzip, deflate, sdch");
		request.addHeader("Cache-Control", "max-age=0");
		request.addHeader("Connection", "keep-alive");
		request.addHeader("Host", "api.fixer.io");
		request.addHeader("User-Agent",
				"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36");
		request = request.get();
		Request request_main = request.build();
		Response response = ok_http_client.newCall(request_main).execute();

		return response;
	}

	public static void pretty_write_jsonobject_to_file(JSONObject json_object, String fileName) {
		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		Object o;
		try {
			o = mapper.readValue(json_object.toString(), Object.class);
			ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
			writer.writeValue(new File(fileName), o);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String get_base64_representation(String input) {

		final byte[] authBytes = input.getBytes(StandardCharsets.UTF_8);
		final String encoded = Base64.getEncoder().encodeToString(authBytes);
		return encoded;
	}

	public static Boolean file_exists(String file_name_with_path) {
		File varTmpDir = new File(file_name_with_path);
		return varTmpDir.exists();
	}

}
