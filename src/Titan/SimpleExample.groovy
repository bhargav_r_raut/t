package Titan

import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.gremlin.groovy.Gremlin



class SimpleExample {
	
	static {
		Gremlin.load()
	}

	public static Map<Vertex, Integer> eigenvectorRank(Graph g) {
		Map<Vertex,Integer> m = [:]; int c = 0
		g.V.as('x').out.groupCount(m).loop('x') {c++ < 1000}.iterate()
		return m
	}
	
	public void getExistingComponents(Graph g){
			  
	  def m = g.V("stockName",CONTAINS,"USD").map();
	  println m.toList()[0].timezoneString;
	  	
	}
	
	//structure for this query is as follows:
	/*
	 * 
	 * get all vertices of this index -->
	 * for stocks --> 
	 * NASDAQ
	 * DAX
	 * CAC
	 * FTSE
	 * NIFTY
	 * for oil ---> DME.
	 * for gold and silver ---> LME
	 * for forex ----> ECB.
	 * 
	 * each vertex is a record in this table.
	 * now we have a property called stockName
	 * get this name
	 * iterate each of them, and find the last date for which we have a value in the data.
	 * 
	 * 
	 */
	
	 public void queryIndices(Graph g,String indice_name){
		 def a = g.V("tableName",indice_name);
	 }
	
}
