package Titan;

import org.json.JSONObject;

public class TItanWorkerThreadResultObject {
	
	private JSONObject success_object;
	private JSONObject failure_object;
	
	public JSONObject get_failed_object() {
		return failure_object;
	}

	public void set_failed_object(JSONObject failed_object) {
		this.failure_object = failed_object;
	}

	public TItanWorkerThreadResultObject(){
		this.success_object = new JSONObject();
		this.failure_object = new JSONObject();
	}

	/**
	 * @return the success_object
	 */
	public JSONObject get_success_object() {
		return success_object;
	}

	/**
	 * @param success_object the success_object to set
	 */
	public void set_success_object(JSONObject object) {
		this.success_object = object;
	}
	
	
}
