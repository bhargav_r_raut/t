package Titan;

import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;

import dailyDownload.GraphHolder;

public class TitanBaseConfig implements TitanConstants {

	private String conf_file_path;

	public TitanBaseConfig(String conf_file_path) {
		this.conf_file_path = conf_file_path;
	}

	public TitanGraph get_graph() throws Exception {
		TitanGraph graph = null;
		
		graph = TitanFactory.open(this.conf_file_path);
		
		//GraphHolder.initializeGraphHolder(graph);

		System.out.println("updating schema changes if any");
		AddYourSchema ads = new AddYourSchema(graph);
		
		ads.update_schema();
		
		
		//NOW CHECK IF WE HAVE ENOUGH COMMON DAY IDS.
		
		
		
		System.out.println("updating of schema changes is completed");

		return graph;

	}

}
