package Titan;

import java.nio.charset.*;
import static java.nio.charset.StandardCharsets.UTF_8;




public interface TitanConstants {
	static final String GRAPH_NAME = "trade_genie";
	static final String SEARCH_INDEX_DIRECTORY = "root@192.168.0.5:/home/titan-0.5.0-hadoop2";
	static final String SEARCH_INDEX_NAME = "search";
	static final String STORAGE_BACKEND = "cassandra";
	static final String STORAGE_INDEX_BACKEND = "elasticsearch";
	static final String V_STOCKS_IN_DATABASE = "stocks_in_database"; 
	static final String DATE_STRING_FORMAT_IN_DATABASE = "yyyy-MM-dd HH:mm:ss";
	static final String DATE_STRING_FORMAT_FOR_QUERY = "yyyy-MM-dd HH:mm:ss z";
	static final int GLOBAL_TRANSACTIONS_LIMIT = 2000;
	static final String VERTEX_INDEX_NAME = "Vsearch";
	static final String EDGE_INDEX_NAME = "Esearch";
	//read timeout in milliseconds.
	static final Integer OPEN_URL_TIMEOUT = 40000;
	public static final Charset ENCODING = UTF_8; 
	
	
}
