package Titan;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import com.thinkaurelius.titan.core.Cardinality;
import com.thinkaurelius.titan.core.EdgeLabel;
import com.thinkaurelius.titan.core.Multiplicity;
import com.thinkaurelius.titan.core.Order;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.VertexLabel;
import com.thinkaurelius.titan.core.schema.ConsistencyModifier;
import com.thinkaurelius.titan.core.schema.EdgeLabelMaker;
import com.thinkaurelius.titan.core.schema.PropertyKeyMaker;
import com.thinkaurelius.titan.core.schema.TitanGraphIndex;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.thinkaurelius.titan.core.schema.TitanSchemaType;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
//import com.thinkaurelius.titan.core.KeyMaker;
//import com.thinkaurelius.titan.core.TitanKey;
//import com.thinkaurelius.titan.core.TitanType;

public class TitanGraphSchema implements TitanConstants {
	// so we will basically define what kind of schema we want.
	// create property keys.
	// create labels.
	private String INDEX_NAME;

	public TitanGraphSchema(String INDEX_NAME) {
		this.INDEX_NAME = INDEX_NAME;
	}

	public PropertyKey create_key(String name, String VertexOrEdge,
			boolean BackEndIndex, boolean Unique, boolean single,
			Class<?> dataType, boolean create_composite_index,
			TitanSchemaType index_only, TitanGraph g, TitanManagement mgmt,
			ConcurrentHashMap<String, PropertyKey> madeKeys) throws Exception {

		// first check if key already exists, in which case we just return it.
		System.out.println("we are currently processing this name" + name);

		TitanGraphIndex vindex = mgmt.getGraphIndex(VERTEX_INDEX_NAME);
		PropertyKey madeKey = null;

		if (mgmt.containsRelationType(name)) {
			System.out.println("the key " + name + " already exists");
		} else {
			System.out.println("now creating key" + name);
			PropertyKeyMaker mkr = mgmt.makePropertyKey(name);
			mkr.dataType(dataType);
			if (single) {
				mkr.cardinality(Cardinality.SINGLE);
			} else {
				mkr.cardinality(Cardinality.SET);
			}
			madeKey = mkr.make();
			if (Unique) {
				TitanManagement.IndexBuilder index;
				if (VertexOrEdge.startsWith("V")) {
					index = mgmt.buildIndex("unique_by_" + name, Vertex.class)
							.addKey(madeKey).unique();

				} else {
					index = mgmt.buildIndex("unique_by_" + name, Edge.class)
							.addKey(madeKey).unique();
				}

				if (index_only != null) {
					TitanGraphIndex idx = index
							.buildCompositeIndex();
					mgmt.setConsistency(idx, ConsistencyModifier.LOCK);
				} else {
					TitanGraphIndex idx = index.buildCompositeIndex();
					mgmt.setConsistency(idx, ConsistencyModifier.LOCK);
				}

			} else {
				if (create_composite_index) {
					
					if (VertexOrEdge.startsWith("V")) {
						if (index_only != null) {
							
							mgmt.buildIndex(name + "_composite_index",
									Vertex.class).addKey(madeKey)
									.buildCompositeIndex();

						} else {
							mgmt.buildIndex(name + "_composit_index",
									Vertex.class).addKey(madeKey)
									.buildCompositeIndex();

						}
					} else {
						if (index_only != null) {

							mgmt.buildIndex(name + "_composite_index",
									Edge.class).addKey(madeKey)
									.buildCompositeIndex();

						} else {
							mgmt.buildIndex(name + "_composit_index",
									Edge.class).addKey(madeKey)
									.buildCompositeIndex();

						}
					}

				}
			}

			madeKeys.put(name, madeKey);
		}
		// mgmt.commit();
		// GraphHolder.g.commit();
		return madeKey;
	}

	public VertexLabel create_vertex_label(String name, TitanManagement mgmt) {
		VertexLabel madeLabel = null;
		if (mgmt.containsVertexLabel(name)) {
			System.out.println("this vertex label--->" + name
					+ " already exists");
		} else {
			System.out.println("now creating vertex label--->" + name);
			madeLabel = mgmt.makeVertexLabel(name).make();
		
		}
		// mgmt.commit();
		// GraphHolder.g.commit();
		return madeLabel;
	}

	public EdgeLabel create_edge_label(String name, Boolean unidirected,
			Multiplicity multi, TitanManagement mgmt, PropertyKey signature) {

		if (mgmt.containsRelationType(name)) {
			System.out.println("this edge label already exists--->" + name);
			return null;
		} else {
			System.out.println("now creating edge label--->" + name);
			EdgeLabelMaker mkr = mgmt.makeEdgeLabel(name);

			if (unidirected) {
				mkr.unidirected();
			}
			if (multi != null) {
				mkr.multiplicity(multi);
			}
			if (signature != null) {
				mkr.signature(signature);
			}
			EdgeLabel e = mkr.make();
			// mgmt.commit();
			// GraphHolder.g.commit();
			return e;

		}

	}

	public void create_vertex_centric_index_on_edgelabel(TitanManagement mgmt,
			String index_name, String edge_name_on_which_index_was_built,
			PropertyKey[] sortkey_array, Direction dir, Order ord,
			EdgeLabel edgelabel) {
		try {
			if (mgmt.containsRelationIndex(
					mgmt.getRelationType(edge_name_on_which_index_was_built),
					index_name)) {
				System.out.println("this vertex centric index already exists.");
			} else {
				System.out.println("created vertex centric index");
				mgmt.buildEdgeIndex(edgelabel, index_name, dir, ord,
						sortkey_array);
				// GraphHolder.g.commit();
				// mgmt.commit();
			}
		} catch (Exception e) {

		}

	}

}
