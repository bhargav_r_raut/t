package Titan;

import java.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class TitanThreadedExecutor {
	
	private int  corePoolSize;
	private int  maxPoolSize;
	private long keepAliveTime;
	private ThreadPoolExecutor texecutor;
	
	public TitanThreadedExecutor(int corePoolSize,
            int maximumPoolSize,
            long keepAliveTime,
            TimeUnit unit,
            BlockingQueue<Runnable> workQueue,
            ThreadFactory threadFactory,
            ThreadPoolRejectHandler handler){
			this.corePoolSize = corePoolSize;
			this.keepAliveTime = keepAliveTime;
			this.maxPoolSize = maximumPoolSize;
			this.texecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
					keepAliveTime, unit, workQueue);
			this.texecutor.setRejectedExecutionHandler(handler);
	}

	public ThreadPoolExecutor getTexecutor() {
		return texecutor;
	}

	
	

}
