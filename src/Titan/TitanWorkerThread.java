package Titan;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadPoolExecutor;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.json.JSONException;
import org.json.JSONObject;

import DailyOperationsLogging.LoggingDailyOperations;

import com.google.gson.Gson;
import com.thinkaurelius.titan.core.TitanException;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.TransactionalGraph;
import com.tinkerpop.blueprints.Vertex;

import elasticSearchNative.ClientInstance;
import elasticSearchNative.FailedToAddToElasticSearch;
import elasticSearchNative.elasticSearchConstants;

public class TitanWorkerThread implements Callable {

	private ThreadPoolExecutor TpoolExec;
	private TransactionalGraph tg;
	private TitanGraph g;
	private Map<String, Object> vm;
	private String type = null;
	private boolean use_transactional_graph;
	private JSONObject additional_params;
	private TItanWorkerThreadResultObject result;

	// CONSTRUCTOR FOR USING TO CREATE A NEW VERTEX.
	public TitanWorkerThread(Object g, ThreadPoolExecutor tex,
			Map<String, Object> vm, String type,
			boolean use_transactional_graph, JSONObject additional_params) {
		this.TpoolExec = tex;
		if (use_transactional_graph) {
			this.tg = (TransactionalGraph) g;
		} else {
			this.g = (TitanGraph) g;
		}

		this.vm = vm;
		this.type = type;
		this.use_transactional_graph = use_transactional_graph;
		this.additional_params = additional_params;
		this.result = new TItanWorkerThreadResultObject();

	}

	// SEPERATE CONSTRUCTOR CAN BE USED FOR SENDING A QUERY.

	public Map<String, Object> getVm() {
		return vm;
	}

	public void setVm(Map<String, Object> vm) {
		this.vm = vm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void FunctionSwitch() throws FailedToAddToElasticSearch,
			JSONException {

		switch (getType()) {
		case "create_vertex":
			CreateVertex();
			break;

		case "add_sector_industry_information":
			AddSectorIndustryInformation();
			break;

		case "draw_edges":
			draw_edges();
			break;

		case "add_combined_vertices":
			add_combined_vertices();
			break;
		case "add_registry_objects":
			add_registry_objects();
			break;

		case "draw_common_day_connector_edge":
			draw_common_day_connector_edge();
			break;

		case "update_property_on_common_day_connector_edge":
			update_property_on_common_day_connector_edge();
			break;

		default:
			break;
		}

	}

	public void update_property_on_common_day_connector_edge()
			throws JSONException {

		String edge_id = (String) getAdditional_params().getString("edge_id");
		Float price_change_percentage = (Float) getAdditional_params().get(
				"price_change_percentage");
		Integer days_back = (Integer) getAdditional_params()
				.getInt("days_back");
		Integer profit_or_loss = (Integer) getAdditional_params().getInt(
				"profit_or_loss");
		String stock_combination = (String) getAdditional_params().getString(
				"stock_combination");
		String stockName = (String) getAdditional_params().getString(
				"stockName");
		String stockFullName = (String) getAdditional_params().getString(
				"stockFullName");
		String indice = (String) getAdditional_params().getString("indice");
		Integer day_id_from = getAdditional_params().getInt("day_id_from");
		Integer day_id_to = getAdditional_params().getInt("day_id_to");

		GroovyStatements.getInstance()
				.update_property_on_common_day_connector_edge(
						price_change_percentage, days_back, stock_combination,
						profit_or_loss, edge_id, stockName, stockFullName,
						indice, day_id_from, day_id_to);

	}

	public void add_registry_objects() throws JSONException {
		String registry_component_md5_hash = (String) getAdditional_params()
				.getString("registry_component_md5_hash");
		String registry_component_jsonified = (String) getAdditional_params()
				.getString("registry_component_jsonified");
		Integer registry_counter = (Integer) getAdditional_params().getInt(
				"registry_counter");
		try {
			GroovyStatements.getInstance().add_registry_objects(
					registry_component_md5_hash, registry_counter,
					registry_component_jsonified);
		} catch (Exception e) {
			e.printStackTrace();
			result.get_failed_object().put("registry_component_jsonified",
					registry_component_jsonified);
			result.get_failed_object().put("exception", e);
		}
	}

	/**
	 * all private fields of classes which extend multithreadable are accessible
	 * via the getadditional_params which is basically a jsonobjec.t
	 * 
	 * @throws JSONException
	 * 
	 */
	public void add_combined_vertices() throws JSONException {

		HashMap<String, Object> vertex_properties_map = new HashMap<String, Object>();

		String vertex_label = "entity_indicator_subindicator_complex";

		vertex_properties_map.put(
				"entity_indicator_subindicator_stockName_name",
				(String) getAdditional_params().getString("stockName"));
		vertex_properties_map.put(
				"entity_indicator_subindicator_stockFullName_name",
				(String) getAdditional_params().getString("stockFullName"));
		vertex_properties_map.put("entity_indicator_subindicator_indice_name",
				(String) getAdditional_params().getString("indice"));
		vertex_properties_map.put("entity_indicator_subindicator_unique_name",
				(String) getAdditional_params().get("combined_vertex_name"));
		vertex_properties_map.put("entity_es_object_id",
				(String) getAdditional_params().getString("es_object_id"));
		String entity_parent_indicator_label = "entity_parent_indicator";
		String entity_parent_subindicator_label = "entity_parent_subindicator";
		Long indicator_vertex_id = (Long) getAdditional_params().get(
				"indicator_vertex_id");
		Long subindicator_vertex_id = (Long) getAdditional_params().get(
				"subindicator_vertex_id");

		try {
			GroovyStatements.getInstance().add_combined_vertex_and_edges(
					vertex_label, vertex_properties_map, indicator_vertex_id,
					subindicator_vertex_id, entity_parent_indicator_label,
					entity_parent_subindicator_label);

		} catch (Exception e) {

			result.get_failed_object().put(
					"combined_name",
					vertex_properties_map
							.get("entity_indicator_subindicator_unique_name"));
			result.get_failed_object().put("exception", e);
		}

	}

	/**
	 * draw common day connector edge. any exception is logged inside
	 * groovystatements directly to the loggingdailyoperations error log. no
	 * other specific logging is employted here.
	 * 
	 * @throws JSONException
	 * 
	 * 
	 * 
	 */
	public void draw_common_day_connector_edge() throws JSONException {
		GroovyStatements gs = GroovyStatements.getInstance();
		Vertex root_vertex = (Vertex) getAdditional_params().get("root_vertex");
		Vertex older_destination_vertex = (Vertex) getAdditional_params().get(
				"older_destination_vertex");
		String edgeLabelName = (String) getAdditional_params().get(
				"edgeLabelName");
		gs.draw_common_day_connector_edge(root_vertex,
				older_destination_vertex, edgeLabelName);
	}

	/**
	 * update the profit_percentage, profit_or_loss and days back for a
	 * particular stock onto the common day connector edges.
	 */
	public void update_common_day_connector_edge_with_stock_properties() {

	}

	/**
	 * 
	 * the old function to draw the day connector edges between individual
	 * historical points of each stock. this is now never used, and is defunct.
	 * 
	 * @throws JSONException
	 * 
	 * 
	 * 
	 */
	public void draw_edges() throws JSONException {
		GroovyStatements gs = GroovyStatements.getInstance();
		Vertex root_vertex = (Vertex) getAdditional_params().get("root_vertex");
		Vertex older_destination_vertex = (Vertex) getAdditional_params().get(
				"older_destination_vertex");
		String edgeLabelName = (String) getAdditional_params().get(
				"edgeLabelName");
		// System.out.println("the edgelabelname is --->" + edgeLabelName);
		// System.out.println("the root vertex is --->" +
		// root_vertex.getLongId());
		// System.out.println("the older destination vertex is--->" +
		// older_destination_vertex.getLongId());

		gs.draw_edge(getVm(), root_vertex, older_destination_vertex,
				edgeLabelName);

		// result.get_success_object().put("edge_map", getVm());
		// result.get_success_object().put("root_vertex", root_vertex.getId());
		// result.get_success_object().put("destination_vertex",
		// older_destination_vertex.getId());
		// result.get_success_object().put("edgeLabelName", edgeLabelName);

	}

	/**
	 * here could not implement transactional graph because wanted to use the
	 * same function inside the groovystatement both are using the
	 * non=transactional single threaded for the moment.
	 * 
	 * 
	 * 
	 */
	public void AddSectorIndustryInformation() {
		GroovyStatements gs = GroovyStatements.getInstance();

		if (use_transactional_graph) {
			System.out
					.println("inside the worker thread, we are using transactional graph.");
			// pass in tg.

			gs.addSectorIndustryInformation(getAdditional_params(), getVm(),
					result, "add_sector_industry_information");

		} else {
			System.out
					.println("inside the worker thread we are using single threads.");
			// pass in t

			gs.addSectorIndustryInformation(getAdditional_params(), getVm(),
					result, "add_sector_industry_information");

		}
	}

	public void CreateVertex() throws FailedToAddToElasticSearch {
		GroovyStatements gs = GroovyStatements.getInstance();
		if (use_transactional_graph) {
			System.out
					.println("inside the worker thread, we are using transactional graph.");
			try {
				gs.addPropertyTransactional(tg, getVm());
			} catch (VertexMapDoesNotHaveUniqueIdentifier e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			try {
				gs.addProperty(getVm(), true, "add_daily_stock_price");

				/*
				 * //add to elasticsearch
				 * if(additional_params.getBoolean("elasticsearch")){
				 * if(add_to_elasticsearch()) {
				 * 
				 * } else{ throw new
				 * FailedToAddToElasticSearch(getVm().toString()); } } else{
				 * //System
				 * .out.println("there was no elasticsearch boolean parameter");
				 * //System.exit(0); }
				 */

				result.get_success_object().put("vertex_map", getVm());
			} catch (Exception e) {
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e, (String) getVm().get("unique_identifier"), null);
			}

		}
	}

	@Override
	public String call() throws FailedToAddToElasticSearch, JSONException {

		// TODO Auto-generated method stub
		FunctionSwitch();
		Gson gson = new Gson();
		return gson.toJson(result);
	}

	/**
	 * @return the additional_params
	 */
	public JSONObject getAdditional_params() {
		return additional_params;
	}

	/**
	 * @param additional_params
	 *            the additional_params to set
	 */
	public void setAdditional_params(JSONObject additional_params) {
		this.additional_params = additional_params;
	}

	public boolean add_to_elasticsearch() throws ElasticsearchException,
			JSONException {
		Client client = ClientInstance.getInstance().getCl();
		Gson gson = new Gson();
		if (getVm().get("stockName") == "silver") {
			ClientInstance.getInstance().inc_at();
		}
		String json = gson.toJson(getVm());

		// String dog = additional_params.getString("dog");
		IndexResponse response = client
				.prepareIndex(elasticSearchConstants.index_name,
						additional_params.getString("document_type"))
				.setSource(json).execute().actionGet();
		System.out.println("this is the created response--->"
				+ response.toString());
		return response.isCreated();
		// return true;
	}

}
