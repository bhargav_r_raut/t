import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Random;

import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexResponse;

import Titan.GroovyStatements;
import elasticSearchNative.LocalEs;

public class UnsortedCompressionRatio {

	public static int[] unsorted_compressed_array = null;

	/**
	 * @param args
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public static void main(String[] args) throws JsonGenerationException,
			JsonMappingException, IOException {
		// TODO Auto-generated method stub
		// unsortedExample();
		// make_big_map();
		// make_in_memory_stocks_pairs_map();
		// GroovyStatements.initializeGroovyStatements(null);
		// chop_and_compare();
		// unsortedExample();
		// make_in_memory_stocks_pairs_map();
		// sortedExample();
		//array_lookup_speed();
		/// build_concurrent_hashmap();
		GregorianCalendar start_cal = new GregorianCalendar();
		//measure_codec_init();
		//check_actual_memory_usage();
		check_compression_with_empty_points();
		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println(end_cal.getTimeInMillis() - start_cal.getTimeInMillis());
	}
	
	
	public static void check_compression_with_empty_points(){
		Random rz = new Random();
		int[] x = new int[3600*300];
		for(int i=0; i < 10; i++){
			x[i] = rz.nextInt();
		}
		System.out.println(x[12]);
		get_compressed_int_array(x, x.length);
		
	}
	
	public static void check_speeds(){
		Random r = new Random();
		int[] k = new int[3000];
		for(int i=0; i < k.length; i++){
			k[i] = r.nextInt(3600*300);
		}
		GregorianCalendar start_cal = new GregorianCalendar();
		for(int i=0; i <10000; i++){
			int rint = r.nextInt(300);
			//if(k[rint] > rint*3600){
			if(k[rint] > (3600*30)){	
			}else{
				
			}
		}
		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println("lookup time: " + (end_cal.getTimeInMillis() - start_cal.getTimeInMillis()));
	}
	
	public static void check_actual_memory_usage(){
		HashMap<String,int[]> test  = new HashMap<String, int[]>();
		for(int i=0; i < 600; i++){
			test.put(String.valueOf(i), get_compressed_int_array(build_sample_array(3600*300),
					3600*300));
			for(int k=0; k < 18; k++ ){
				test.put(String.valueOf(k) +"_" + String.valueOf(i), get_compressed_int_array(build_sample_array(3000),3000));
			}
			get_memory_stats();
		}
	}
	
	public static ArrayList<Double> get_300_random_values() {
		Random r = new Random();
		ArrayList<Double> ra = new ArrayList<Double>();
		for (int i = 0; i < 300; i++) {
			ra.add(r.nextDouble());
		}
		return ra;
	}

	public static void get_memory_stats() {
		// Get current size of heap in bytes
		long heapSize = Runtime.getRuntime().totalMemory();

		// Get maximum size of heap in bytes. The heap cannot grow beyond this
		// size.
		// Any attempt will result in an OutOfMemoryException.
		long heapMaxSize = Runtime.getRuntime().maxMemory();

		// Get amount of free memory within the heap in bytes. This size will
		// increase
		// after garbage collection and decrease as new objects are created.
		long heapFreeSize = Runtime.getRuntime().freeMemory();

		System.out.println("heapsize:" + heapSize + " heap max:" + heapMaxSize
				+ " free:" + heapFreeSize);
	}

	public static void make_big_map() {
		get_memory_stats();
		HashMap<String, ArrayList<Double>> dq = new HashMap<String, ArrayList<Double>>();
		for (int i = 0; i < 1095000; i++) {
			System.out.println(i);
			dq.put(String.valueOf(i), get_300_random_values());
		}
		get_memory_stats();
	}

	public static void print_array_contents(int[] arr, String arr_name) {
		String sarr = "";
		for (int i = 0; i < arr.length; i++) {
			sarr = sarr + String.valueOf(arr[i]);
		}
		System.out.println("array name:" + arr_name);
		System.out.println("arr values:" + sarr);
	}

	public static int[][] chunkArray(int[] array, int chunkSize) {
		int numOfChunks = (int) Math.ceil((double) array.length / chunkSize);
		int[][] output = new int[numOfChunks][];

		for (int i = 0; i < numOfChunks; ++i) {
			int start = i * chunkSize;
			int length = Math.min(array.length - start, chunkSize);

			int[] temp = new int[length];
			System.arraycopy(array, start, temp, 0, length);
			output[i] = temp;
		}

		return output;
	}

	public static int[] merge(int[][] arrays) {
		// Count the number of arrays passed for merging and the total size of
		// resulting array
		int arrCount = 0;
		int count = 0;
		for (int[] array : arrays) {
			arrCount++;
			count += array.length;
		}
		System.out.println("Arrays passed for merging : " + arrCount);
		System.out.println("Array size of resultig array : " + count);

		// Create new array and copy all array contents
		int[] mergedArray = new int[count];
		int start = 0;
		for (int[] array : arrays) {
			System.arraycopy(array, 0, mergedArray, start, array.length);
			start += array.length;
		}
		return mergedArray;
	}

	public static void test_es_index(final int[] arr) {

		try {
			IndexResponse inr = LocalEs.getInstance()
					.prepareIndex("pairs", "pair")
					.setSource(new HashMap<String, Object>() {
						{
							put("pair_name",
									GroovyStatements.getInstance().objmapper
											.writeValueAsString(arr));
						}
					}).execute().actionGet();
		} catch (ElasticsearchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void chop_and_compare() {

		int size_of_initial_array = 100000;
		int[] initial_raw_array = build_sample_array(size_of_initial_array);
		int[] compressed_array = get_compressed_int_array(initial_raw_array,
				size_of_initial_array);
		int[] decompressed_array = get_uncompressed_integer_array(
				compressed_array, size_of_initial_array);

		if (Arrays.equals(initial_raw_array, decompressed_array)) {
			System.out
					.println("the raw array and basic decompressed array are equal");
		}

		int compressed_array_length = compressed_array.length;

		int[][] chunks = chunkArray(compressed_array,
				compressed_array_length / 20);

		int[] reconstructed_array = merge(chunks);

		if (Arrays.equals(reconstructed_array, compressed_array)) {
			System.out.println("reconstruction success");
		}

		int[] reconst_decompressed_array = get_uncompressed_integer_array(
				reconstructed_array, size_of_initial_array);

		if (Arrays.equals(reconst_decompressed_array, initial_raw_array)) {
			System.out.println("all works");
		}

		for (int[] chunk : chunks) {
			System.out.println("now indexing:");
			print_array_contents(chunk, "chunk");
			test_es_index(chunk);
		}

	}

	public static void build_concurrent_hashmap() {
		
		// we will have a total of 350*18 sorted int[]
		// each will have 1090000 items.
		// and each will have
		for (int i = 0; i < 350; i++) {
			System.out.println("now doing stock:" + String.valueOf(i));
			String key = null;
			int[] value = null;
			for (int sorted_map = 0; sorted_map < 1; sorted_map++) {
				key = String.valueOf(i) + "_" + String.valueOf(sorted_map);
				value = get_compressed_sorted_int_array(PQ.total_pairs);
				GroovyStatements.getInstance().cmap.put(key, value);
			}
			for (int unsorted_map = 1; unsorted_map < 2; unsorted_map++) {
				key = String.valueOf(i) + "_" + String.valueOf(unsorted_map);
				GroovyStatements.getInstance().cmap.put(key, get_compressed_int_array(null, 3650*300));
			}
		}
		get_memory_stats();
		//now we see the speed of 
		//1000 lookups. 
		//3000 lookups inside that.
	
		
	}

	public static void array_lookup_speed() {
		int[] k = new int[3600 * 300];
		for (int i = 0; i < k.length; i++) {
			k[i] = i;
		}

		Random r = new Random();
		GregorianCalendar start_cal = new GregorianCalendar();
		for (int i = 0; i < 100000; i++) {
			int l = k[r.nextInt(3600 * 299)];
		}
		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println(end_cal.getTimeInMillis()
				- start_cal.getTimeInMillis());
	}

	public static void make_in_memory_stocks_pairs_map()
			throws JsonGenerationException, JsonMappingException, IOException {
		// start from zero,
		// stop loss is exit based.
		// so if you exited on a particular day, then you could have exited for
		// a sorted set of days.
		// and if you had exited on any other particular day.
		// entry -> day id : exit day id-> is it interrupted by a 2 percent
		// fall.
		// ok then the problem is trailing.
		// we are only interested in how much profit we got and when
		//

		get_memory_stats();
		GroovyStatements.initializeGroovyStatements(null);
		int size_of_initial_array = 300 * 3600;
		HashMap<String, int[]> dq = new HashMap<String, int[]>();
		System.out.println("putting 350 unsorted things");
		for (int i = 0; i < 350; i++) {
			dq.put(String.valueOf(i),
					get_compressed_int_array(
							build_sample_array(size_of_initial_array),
							size_of_initial_array));
		}
		get_memory_stats();
		System.out.println("putting: " + (350 * 6) + " sorted arrays");
		for (int i = 350; i < (350 + (350 * 6)); i++) {
			dq.put(String.valueOf(i),
					get_compressed_sorted_int_array(size_of_initial_array));
		}
		get_memory_stats();
		System.in.read();

		Random r = new Random();
		GregorianCalendar cal = new GregorianCalendar();
		for (int i = 0; i < 1000; i++) {
			// get a random stock
			Integer random_key = r.nextInt(349);
			// uncompress its pairs.
			int[] uncompressed = get_uncompressed_integer_array(
					dq.get(String.valueOf(random_key)), size_of_initial_array);
			// calculate a histogram.
			for (int pair = 0; pair < 3000; pair++) {
				// uncompressed[r.nextInt(89999)];
				Integer k = uncompressed[r.nextInt(size_of_initial_array)];
			}
		}
		GregorianCalendar end_cal = new GregorianCalendar();
		System.out.println("total time taken is:"
				+ (end_cal.getTimeInMillis() - cal.getTimeInMillis()));
	}

	public static int[] get_uncompressed_integer_array(int[] compressed,
			int size_of_initial_array) {

		IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
		int[] recovered = new int[size_of_initial_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed, new IntWrapper(0), compressed.length,
				recovered, recoffset);
		return recovered;
	}
	
	public static void measure_codec_init(){
		Random r = new Random();
		for(int i=0; i < 60000; i++){
			r.nextInt(3600*365);
		}
	}

	public static int[] build_sample_array(int size_of_initial_array) {
		Random r = new Random();
		int[] data = null;
		if (unsorted_compressed_array == null) {
			final int N = size_of_initial_array;
			data = new int[N];
			// initialize the data (most will be small
			for (int k = 0; k < N; k += 1)
				data[k] = 10000 + r.nextInt(3000);
			// throw some larger values
			for (int k = 0; k < N; k += 5)
				data[k] = 1000;
			unsorted_compressed_array = data;
		}
		else{
			data = unsorted_compressed_array;
		}
		return data;
	}

	public static int[] get_compressed_sorted_int_array(
			int size_of_initial_array) {
		Random r = new Random();
		int[] data = new int[size_of_initial_array];
		// System.out.println("Compressing " + data.length +
		// " integers in one go");

		for (int k = 0; k < data.length; ++k)
			data[k] = 5000;

		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());

		int[] compressed = new int[data.length + 1024];

		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);

		 System.out.println("compressed from " + data.length * 4 / 1024
		 + "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");

		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		return compressed;
	}

	public static int[] get_uncompressed_sorted_int_array(int[] compressed,Integer size_of_basic_uncompressed_array) {
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		int[] recovered = new int[size_of_basic_uncompressed_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed, new IntWrapper(0), compressed.length,
				recovered, recoffset);
		
		return recovered;
	}


	
	public static int[] get_compressed_int_array(int[] data,
			int size_of_initial_array) {
		//data = build_sample_array(size_of_initial_array);
		final int N = size_of_initial_array;
		int[] compressed = new int[N + 1024];// could need more
		IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());

		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);

		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		System.out.println("compressed unsorted integers from " + data.length
				* 4 / 1024 + "KB to " + outputoffset.intValue() * 4 / 1024
				+ "KB");
		return compressed;
	}

	public static void sortedExample() {

		int[] data = new int[300 * 3650];
		System.out
				.println("Compressing " + data.length + " integers in one go");
		// data should be sorted for best
		// results
		for (int k = 0; k < data.length; ++k)
			data[k] = k;
		// Very important: the data is in sorted order!!! If not, you
		// will get very poor compression with IntegratedBinaryPacking,
		// you should use another CODEC.

		// next we compose a CODEC. Most of the processing
		// will be done with binary packing, and leftovers will
		// be processed using variable byte
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		// output vector should be large enough...
		int[] compressed = new int[data.length + 1024];
		// compressed might not be large enough in some cases
		// if you get java.lang.ArrayIndexOutOfBoundsException, try
		// allocating more memory

		/**
		 * 
		 * compressing
		 * 
		 */
		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);
		// got it!
		// inputoffset should be at data.length but outputoffset tells
		// us where we are...
		System.out.println("compressed from " + data.length * 4 / 1024
				+ "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");
		// we can repack the data: (optional)
		compressed = Arrays.copyOf(compressed, outputoffset.intValue());

		/**
		 * 
		 * now uncompressing
		 * 
		 */
		int[] recovered = new int[data.length];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed, new IntWrapper(0), compressed.length,
				recovered, recoffset);
		if (Arrays.equals(data, recovered))
			System.out.println("data is recovered without loss");
		else
			throw new RuntimeException("bug"); // could use assert
		System.out.println();
	}

	/**
	 * This is an example to show you can compress unsorted integers as long as
	 * most are small.
	 */
	public static void unsortedExample() {
		final int N = 300 * 3600;
		int[] data = new int[N];
		// initialize the data (most will be small
		for (int k = 0; k < N; k += 1)
			data[k] = 3;
		// throw some larger values
		for (int k = 0; k < N; k += 5)
			data[k] = 100;
		for (int k = 0; k < N; k += 533)
			data[k] = 10000;
		int[] compressed = new int[N + 1024];// could need more
		IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
		// compressing
		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed, outputoffset);
		System.out.println("compressed unsorted integers from " + data.length
				* 4 / 1024 + "KB to " + outputoffset.intValue() * 4 / 1024
				+ "KB");
		// we can repack the data: (optional)
		compressed = Arrays.copyOf(compressed, outputoffset.intValue());

		int[] recovered = new int[N];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed, new IntWrapper(0), compressed.length,
				recovered, recoffset);
		if (Arrays.equals(data, recovered))
			System.out.println("data is recovered without loss");
		else
			throw new RuntimeException("bug"); // could use assert
		System.out.println();

	}

}

/***
 * 
 * no need to generate pairs. as pairs are calculated they can be added.
 * initially empty int arrays can be built for each stock of said capacity. they
 * will also be built for the stop losses. total of 7 arrays per stock.
 * 
 * these can be serialized and stored into redis, and read back when the program
 * starts, should be stored as an attribute of the entity itself. check this
 * issue
 * 
 * the entire entity has to also be stored in redis.
 * 
 * and also implement the entire idea written yesterday. minimum time for all
 * this: 10 days.
 * 
 * minimum time for testing the complex generation - rest of month.
 * 
 * jruby, like, recently done stuff, sorted sets caching, news integration,
 * front end => one month?
 * 
 * 
 * when a pair change is calculated, then its index is to be calculated and
 * stored into the array. the array is first decompressed and then restored. if
 * a particular pair gives a 2,3,5 percent change, then calculate all the
 * indices of those things that can be affected, and then we store in the
 * respective index of that array.
 * 
 * 
 * 
 * 
 ***/

