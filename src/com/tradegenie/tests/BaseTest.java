package com.tradegenie.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.LogObject;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class BaseTest {

	public static String[] TEST_indices_to_test;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/***
		 * for test.
		 */
		StockConstants.single_machine_tests = true;
		
		StockConstants.test = true;
		
		LogObject lgd = new LogObject(BaseTest.class);
		
		if (ShScript.start_all_servers()) {
			lgd.commit_debug_log("started all servers at beginning of test");
		} else {
			lgd.commit_error_log("failed to initialize servers", new Exception("failed to initialize servers"));
		}
		
		

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 * 
	 *             recreate the indexes at the end of each test.
	 * 
	 * 
	 * 
	 */
	@Before
	public void setUp() throws Exception {
		StockConstants.test_methods.clear();
		ShScript.start_wifi();
		System.out.println("flushing indices");
		FlushIndicesAndRedis comode = new FlushIndicesAndRedis();
		comode.flush();
		System.out.println("finished flushing");
		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
	}

	/**
	 * @throws java.lang.Exception
	 * 
	 *             make the centralsingleton null at the end of each test. this
	 *             is because some test, simulate errors in centralsingleton
	 *             creation, and in that case the singleton gets created, but
	 *             none of its data is filled. so we encounter problems in other
	 *             tests
	 * 
	 */
	@After
	public void tearDown() throws Exception {
		CentralSingleton.destroySingleton();
		TestSingleton.destroySingleton();
	}

	public static HashMap<String, Integer> iterate_and_return_indices_to_test(String[] test_indices_to_test) {

		TEST_indices_to_test = test_indices_to_test;
		HashMap<String, Integer> exchange_name_and_counters_to_test = new HashMap<String, Integer>();
		Integer counter = 0;

		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			for (String s : TEST_indices_to_test) {

				if (entry.getKey().equals(s)) {

					exchange_name_and_counters_to_test.put(s, counter);

				}
			}
			counter++;
		}

		return exchange_name_and_counters_to_test;
	}

	public void store_es_entity_index_state(String entity_index) {

	}

	public Boolean compare_states() {

		return null;
	}

	/**
	 * @throws Exception
	 *             *
	 * 
	 */
	public static void print_failed_entities_by_index() throws Exception {

		SearchResponse response = RemoteEs.getInstance().prepareSearch("tradegenie_titan")
				.setTypes(StockConstants.ESTypes_error_log)
				.setQuery(QueryBuilders.matchAllQuery())
				.setFetchSource(true)
				.setSize(500)
				.execute().actionGet();

		CentralSingleton.initializeSingleton();

		/***
		 * key -> name of exchange value -> arraylist of entites that were not
		 * possible to download.
		 */
		HashMap<String, ArrayList<String>> exchange_name_to_missing_entities = new HashMap<String, ArrayList<String>>();

		for (SearchHit hit : response.getHits().getHits()) {

			String entity_unique_name = (String) hit.getSource().get("entity_unique_name");

			for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {

				//System.out.println(entry.getKey());
				if (entity_unique_name.contains(entry.getKey())) {
					if (exchange_name_to_missing_entities.containsKey(entry.getKey())) {
						exchange_name_to_missing_entities.get(entry.getKey()).add(entity_unique_name);
					} else {
						exchange_name_to_missing_entities.put(entry.getKey(),
								new ArrayList<String>(Arrays.asList(entity_unique_name)));
					}
				}
			}
		}

		System.out.println(CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter()
				.writeValueAsString(exchange_name_to_missing_entities));
	}

}
