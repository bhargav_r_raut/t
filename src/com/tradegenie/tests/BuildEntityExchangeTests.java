package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.MetricsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.junit.Test;

import DataProvider.ForexRequest;
import DataProvider.QuandlRequest;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.LogObject;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class BuildEntityExchangeTests extends BaseTest {

	// setting constants for how many entities to expect in
	public static HashMap<String, Object> entities_to_expect_in_each_index;

	@Test
	public void test_that_remote_url_calls_are_not_made_anywhere() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_all_exchanges();
		assertEquals("remote urls should not have been called", false,
				TestSingleton.getTsingle().getTest_expected_values_map().containsKey("url_called"));
		CountResponse cr = LocalEs.getInstance().prepareCount("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();
		assertEquals("there should be at least one entity in the entity type", true, cr.getCount() > 0);
	}

	@Test
	public void test_entity_count_in_each_index() throws Exception {

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_all_exchanges(); //
		// here we hard code the total number of entities that are expected in
		// each entityindex.
		HashMap<String, Integer> exchange_info = iterate_and_return_indices_to_test(
				new String[] { StockConstants.name_for_nasdaq, StockConstants.name_for_dax, StockConstants.name_for_cac,
						StockConstants.name_for_ftse, StockConstants.name_for_nifty, StockConstants.name_for_forex,
						StockConstants.name_for_oil, StockConstants.name_for_metals });
		for (Map.Entry<String, Integer> entry : exchange_info.entrySet()) {
			CountResponse cr = LocalEs.getInstance().prepareCount("tradegenie_titan").setTypes("entity")
					.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
							FilterBuilders.termFilter("indice", entry.getKey())))
					.execute().actionGet();
			if (entry.getKey().equals(StockConstants.name_for_nasdaq)) {
				assertEquals("nasdaq should have 106 elements", (long) 106, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_dax)) {
				assertEquals("dax should have 30 elements", (long) 30, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_cac)) {
				assertEquals("cac should have 42 elements", (long) 42, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_ftse)) {
				assertEquals("ftse should have 98 elements", (long) 98, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_nifty)) {
				assertEquals("nifty should have 50 elements", (long) 50, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_forex)) {
				assertEquals("forex should have 10 pairs", (long) 18, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_oil)) {
				assertEquals("oil should have 1 entity", (long) 1, cr.getCount());
			} else if (entry.getKey().equals(StockConstants.name_for_metals)) {
				assertEquals("metals should have 1 entity", (long) 1, cr.getCount());
			}
		}

	}

	@Test
	public void test_all_entity_names_are_unique() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_all_exchanges();
		MetricsAggregationBuilder aggregation = AggregationBuilders.cardinality("agg").field("unique_name");
		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.matchAllQuery()).addAggregation(aggregation).execute().actionGet();
		Cardinality agg = resp.getAggregations().get("agg");
		long total_unique_entities = agg.getValue();
		assertEquals("total unique entities should be 346", (long) 346, total_unique_entities);
	}

	@Test
	public void test_entities_have_unique_data_provider_codes() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_all_exchanges();
		HashMap<String, Integer> index_info = iterate_and_return_indices_to_test(
				new String[] { StockConstants.name_for_nasdaq, StockConstants.name_for_dax, StockConstants.name_for_cac,
						StockConstants.name_for_ftse, StockConstants.name_for_nifty, StockConstants.name_for_oil,
						StockConstants.name_for_metals });
		MetricsAggregationBuilder aggregation = AggregationBuilders.cardinality("agg").field("dataProviderCode");
		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termsFilter("indice",
								new String[] { StockConstants.name_for_nasdaq, StockConstants.name_for_dax,
										StockConstants.name_for_cac, StockConstants.name_for_ftse,
										StockConstants.name_for_nifty, StockConstants.name_for_oil,
										StockConstants.name_for_metals })))
				.addAggregation(aggregation).execute().actionGet();
		Cardinality agg = resp.getAggregations().get("agg");

		assertEquals("there should be 328 data provier codes, because we exclude forex, "
				+ "which does not have quandl codes", (long) 328, agg.getValue());

	}

	/***
	 * instantiates a new exchangepackagecoordinator builds the oil entity
	 * index. then downloads the oil entity index upto the current date. then
	 * waits for the bulk processor to close. asserts that there are no
	 * exceptions.
	 * 
	 * @throws Exception
	 * 
	 **/

	@Test
	public void test_job_id_sent_and_received_is_same() throws Exception {

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, Integer> exchange_name_and_counters_to_test = iterate_and_return_indices_to_test(
				new String[] { StockConstants.name_for_oil });
		for (Map.Entry<String, Integer> entry : exchange_name_and_counters_to_test.entrySet()) {

			erp.build_entityExchange(entry.getValue(), entry.getKey());
			EntityIndex ei = erp.download_entityExchange(entry.getKey(), entry.getValue());
			assertNotNull("job id received should not be null", ei.getPrice_and_stop_loss_arr_update_job_id_received());
			assertEquals("job id sent and received should be the same",
					ei.getPrice_and_stop_loss_arr_update_job_id_sent(),
					ei.getPrice_and_stop_loss_arr_update_job_id_received());
			Boolean exceptions_found = LogObject.get_errors();
			assertEquals("should find no errors", false, exceptions_found);

		}
		Boolean exceptions_found = LogObject.get_errors();
		assertEquals("should find no errors", false, exceptions_found);

	}


}
