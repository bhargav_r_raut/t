package com.tradegenie.tests;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.json.JSONObject;
import org.junit.Test;

import Indicators.Indicator;
import ExchangeBuilder.CentralSingleton;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class CentralSingletonTests extends DownloadEntityExchangeTests {

	/**
	 * @Test public void test_builds_indicator_map() throws Exception {
	 *       RemoteEs.getInstance().admin().indices().prepareRefresh(
	 *       "tradegenie_titan").execute().actionGet();
	 *       CentralSingleton.initializeSingleton(); assertTrue(
	 *       "the indicator map is populated",
	 *       CentralSingleton.indicators_hashmap.size() > 0); }
	 * 
	 * @Test public void test_loads_indicators_from_es() throws Exception {
	 *       RemoteEs.getInstance().admin().indices().
	 *       prepareRefresh("tradegenie_titan").execute().actionGet();
	 *       CentralSingleton.initializeSingleton(); assertTrue(
	 *       "the indicator map is populated",
	 *       CentralSingleton.indicators_hashmap.size() > 0);
	 *       ConcurrentHashMap<String, List<Indicator>> init_indicators_map =
	 *       new CentralSingleton().indicators_hashmap; CountResponse cr =
	 *       RemoteEs.getInstance().prepareCount("tradegenie_titan").setTypes(
	 *       "indicator")
	 *       .setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();
	 *       Long indicators_count = cr.getCount();
	 *       CentralSingleton.destroySingleton(); System.out.println(
	 *       "destroyed the singleton --------------------"); // now it should
	 *       load them from the remote database.
	 *       CentralSingleton.initializeSingleton();
	 * 
	 *       for (Map.Entry<String, List<Indicator>> e :
	 *       CentralSingleton.indicators_hashmap.entrySet()) { assertTrue(
	 *       "indicator present in earlier map,"
	 *       ,init_indicators_map.containsKey(e.getKey())); } CountResponse cr2
	 *       = RemoteEs.getInstance().prepareCount("tradegenie_titan").setTypes(
	 *       "indicator")
	 *       .setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();
	 *       Long indicators_count2 = cr.getCount(); assertTrue(
	 *       "initial and later counts are equal, implying that indicators were not recommitted"
	 *       , indicators_count.longValue() == cr2.getCount()); }
	 **/

	/***
	 * first commits to es, then loads from
	 * 
	 * @throws Exception
	 */

	@Test
	public void test_builds_all_indicators() throws Exception {
		RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
		CentralSingleton.initializeSingleton();
		org.json.JSONObject indicators_jsonfile_read = new org.json.JSONObject(ReadWriteTextToFile.getInstance()
				.readFile(StockConstants.indicators_jsontextfile, TitanConstants.ENCODING));
		Iterator<String> indicators = indicators_jsonfile_read.keys();
		while (indicators.hasNext()) {
			String indicator_name = indicators.next();
			JSONObject indicator_object = indicators_jsonfile_read.getJSONObject(indicator_name);
			HashMap<String, List<Integer>> stub_names_and_periods = CentralSingleton
					.get_indicator_periods_from_json(indicator_name, indicator_object);
			List<Indicator> indicator_list = CentralSingleton.indicators_hashmap.get(indicator_name);
			HashMap<String, List<Integer>> actual_stub_names_and_periods = new HashMap<String, List<Integer>>();
			for (Indicator i : indicator_list) {
				actual_stub_names_and_periods.put(i.getIndicator_and_period_name(), i.getPeriods());
			}
			for (Map.Entry<String, List<Integer>> entry : stub_names_and_periods.entrySet()) {
				assertTrue(entry.getKey() + ":stub is present in the indicators hashmap",
						actual_stub_names_and_periods.containsKey(entry.getKey()));
				assertTrue(entry.getKey() + ":stub periods are same as indicators hashmap",
						actual_stub_names_and_periods.get(entry.getKey())
						.equals(entry.getValue()));
			}
		}
	}

}
