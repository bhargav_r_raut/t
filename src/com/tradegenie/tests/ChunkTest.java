package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.TestMethod;
import ExchangeBuilder.TestMethodsExecutor;
import Titan.ReadWriteTextToFile;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

/***
 * the old_arrays part is redundant, because on redownloading, 
 * the initial parts of all the pair changes arrays, are changed because of the retrospective
 * calculation of the price changes.
 * older pairs are also affected.
 * so old_arrays cannot be used to compare the starting parts of the arrays on remote.
 * 
 * 
 * @author bhargav
 *
 */
public class ChunkTest extends DownloadEntityExchangeTests {

	// so we modify all the arrays to be sent into predefined random arrays
	// then we chunk them
	// then we dechunk them
	// reconstruct them and see whether they are the same as whatever was sent.
	// we also do this in a two step test, to see how it will work, when there
	// is a redownload.
	public static final String test_basic_chunk_dechunk = "test_basic_chunk_dechunk";
	public static final String test_should_skip_part_after_update_to_remote = "test_should_skip_part_after_update_to_remote";

	// @Test
	public void debug_remote() throws JsonParseException, JsonMappingException, IOException {
		DeChunk de = new DeChunk();
		de.setArray_name("oil_Amex Oil Index_oil_index_open_open");
		de.setExpected_digest("7559e6f43f60469df81b76bb548ddba36a49db4da3d56738576681410c716641");
		de.setRequired_large_array_index(725400);
		ObjectMapper mapper = new ObjectMapper();
		ReadWriteTextToFile.remote_dechunk_test_response(mapper.writeValueAsString(de),
				"http://localhost:8080/SpringApp/dechunk");
	}
	
	
	public void multi_step_chunk_dechunk_test() throws Exception {
		CentralSingleton.initializeSingleton();
		// StockConstants.make_dummy_request = true;
		StockConstants.define_custom_end_date = true;
		/// StockConstants.test_methods.put(test_basic_chunk_dechunk, new
		/// TestMethod(test_basic_chunk_dechunk,
		// new
		/// ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_basic_chunk_dechunk))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));
		// now we need to save the entitites to es, and restart the download.
		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		HashMap<String, int[]> old_arrays = new HashMap<String, int[]>();
		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			e.getValue().commit_entity_index_to_elasticsearch(1);
			for (Map.Entry<String, Entity> entry : e.getValue().getEntity_map().entrySet()) {
				ArrayList<String> entity_arrs = new ArrayList<String>(Arrays.asList(
						entry.getValue().getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
						entry.getValue().getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));

				for (String arr : entity_arrs) {
					old_arrays.put(arr,
							PairArray.get_decompressed_arr(arr, 0,
									CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
											* StockConstants.Entity_total_pairs_for_a_day));
				}

			}
		}

		// store the arrays.
		// compare them to the new arrays that are created.

		// we wanna store the digests of all the arrays initially exists.
		// then we wanna store their lengths.
		// finally we wanna assert that the new arrays are all longer and have
		// different digests

		// now we start the download again
		// we dont call teardown here, because it will destroy the
		// centralsingleton,
		// and with it the arrays already in its query_time_lookup_maps.
		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		StockConstants.define_custom_end_date = false;
		// StockConstants.test_methods.put(test_basic_chunk_dechunk, new
		// TestMethod(test_basic_chunk_dechunk,
		// new
		// ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_basic_chunk_dechunk))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), true, false, true);

		HashMap<String, String> arr_name_and_digest = new HashMap<String, String>();

		for (Map.Entry<String, EntityIndex> entry : redownloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// first check the open_open and close_close

				ArrayList<String> entity_arrs = new ArrayList<String>(
						Arrays.asList(e.getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
								e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));

				for (String arr_name : entity_arrs) {

					Integer latest_existing_pair_in_array = null;
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();

					CountResponse cr = RemoteEs.getInstance().prepareCount("tradegenie_titan").setTypes("arrays")
							.setQuery(QueryBuilders.termQuery("arr_name", arr_name)).execute().actionGet();

					SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
							.setFetchSource(true).setQuery(QueryBuilders.termQuery("arr_name", arr_name))
							.addSort("latest_newly_added_pair_index", SortOrder.DESC).setSize(1).execute().actionGet();

					assertEquals("there are hits", true, sr.getHits().getHits().length > 0);

					for (SearchHit hit : sr.getHits().getHits()) {

						latest_existing_pair_in_array = ((Integer) hit.getSource()
								.get("latest_newly_added_pair_index"));
					}

					int[] existing_local_cs_arr = PairArray.get_decompressed_arr(arr_name, 0,
							CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
									* StockConstants.Entity_total_pairs_for_a_day);

					// debug_chunk_by_chunk(existing_local_cs_arr, arr_name,
					// old_arrays.get(arr_name));

					existing_local_cs_arr = Arrays.copyOfRange(existing_local_cs_arr, 0,
							latest_existing_pair_in_array + 1);

					// now digest it.
					String arr_as_string = DeChunkResponse.convertToString(existing_local_cs_arr);
					String digested_array = DigestUtils.sha256Hex(arr_as_string);
					arr_name_and_digest.put(arr_name, digested_array);
					// now make the request.
					DeChunk de = new DeChunk(digested_array, arr_name, latest_existing_pair_in_array);
					DeChunkResponse dcr = ReadWriteTextToFile.getInstance().remote_dechunk_test_response(
							CentralSingleton.objmapper.writeValueAsString(de), StockConstants.ChunkTest_dechunk_url);
					assertEquals("the returned comparison was:" + dcr.getDigest_comparison(), true,
							dcr.getDigest_comparison());
					// }
				}
			}
		}

		// now take each of these entities, and check whether the remote is the
		// same as
		// whatever we have here.
		tearDown();
		setUp();
		Boolean clear_singleton_resp = ReadWriteTextToFile.getInstance()
				.clear_remote_singleton_request(StockConstants.Clear_singleton_url);
		assertTrue("singleton was cleared", clear_singleton_resp);
		compare_multi_step_results_with_single_step(arr_name_and_digest);
	}

	public void debug_chunk_by_chunk(int[] existing_array, String existing_array_name, int[] old_array) {
		SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan")
				.setQuery(QueryBuilders.termQuery("arr_name", existing_array_name))
				.addSort("start_index", SortOrder.ASC).setSize(100).setFetchSource(true).execute().actionGet();
		for (SearchHit hit : sr.getHits().getHits()) {
			ArrayList<Integer> compressed_arrlist = (ArrayList<Integer>) hit.sourceAsMap().get("arr");
			Integer[] wrapperArr = compressed_arrlist.toArray(new Integer[compressed_arrlist.size()]);
			int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
			int start_index = (int) hit.getSource().get("start_index");
			// decompress it and check if it is the same as the current array in
			// those indices.
			int[] decomp_arr = PairArray.get_decompressed_array_from_compressed_array(compressed_arr, 10000, 0);
			int[] existing_arr_slice = Arrays.copyOfRange(existing_array, start_index, start_index + 10000);
			int[] old_arr_slice = Arrays.copyOfRange(old_array, start_index, start_index + 10000);

			if (!Arrays.equals(decomp_arr, existing_arr_slice)) {
				System.out.println("at start index: " + start_index + " there is inequality ");
				for (int i = 0; i < old_arr_slice.length; i++) {
					if (existing_arr_slice[i] != decomp_arr[i]) {
						System.out.println("at index, there is inequality:" + i);
						System.out.println("old slice: " + decomp_arr[i]);
						System.out.println("existing slice: " + existing_arr_slice[i]);
						System.exit(1);
					}
				}
			} else {
				System.out.println("start index :" + start_index + " is ok(existing vs es)");
			}

			if (!Arrays.equals(old_arr_slice, existing_arr_slice)) {
				System.out.println("at start index: " + start_index + " there is inequality ");
				for (int i = 0; i < old_arr_slice.length; i++) {
					if (existing_arr_slice[i] != old_arr_slice[i]) {
						// System.out.println("at index, there is inequality:" +
						// i);
						// System.out.println("old slice: " + old_arr_slice[i]);
						// System.out.println("existing slice: " +
						// existing_arr_slice[i]);
						// System.exit(1);
					}
				}
			} else {
				System.out.println("start index :" + start_index + " is ok(existing vs old_arr)");
			}
		}
	}

	public void compare_multi_step_results_with_single_step(HashMap<String, String> multi_step_arr_digest_map)
			throws Exception {
		CentralSingleton.initializeSingleton();

		// StockConstants.test_methods.put(test_basic_chunk_dechunk, new
		// TestMethod(test_basic_chunk_dechunk,
		// new
		// ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_basic_chunk_dechunk))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> entry : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// first check the open_open and close_close

				ArrayList<String> entity_arrs = new ArrayList<String>(
						Arrays.asList(e.getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
								e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));

				for (String arr_name : entity_arrs) {
					// if (arr_name.contains("open_open") ||
					// arr_name.contains("close_close")) {
					Integer latest_existing_pair_in_array = null;
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
							.setFetchSource(true).setQuery(QueryBuilders.termQuery("arr_name", arr_name))
							.addSort("latest_newly_added_pair_index", SortOrder.DESC).setSize(1).execute().actionGet();
					// System.out.println("the name of the array is:" +
					// arr_name);
					assertEquals("there are hits", true, sr.getHits().getHits().length > 0);

					for (SearchHit hit : sr.getHits().getHits()) {
						/// System.out.println("this is the source of the hit");
						// System.out.println(hit.getSource().toString());
						latest_existing_pair_in_array = ((Integer) hit.getSource()
								.get("latest_newly_added_pair_index"));
					}

					int[] existing_local_cs_arr = PairArray.get_decompressed_arr(arr_name, 0,
							CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
									* StockConstants.Entity_total_pairs_for_a_day);

					existing_local_cs_arr = Arrays.copyOfRange(existing_local_cs_arr, 0,
							latest_existing_pair_in_array + 1);

					// now digest it.
					String arr_as_string = DeChunkResponse.convertToString(existing_local_cs_arr);
					String digested_array = DigestUtils.sha256Hex(arr_as_string);
					// now make the request.
					DeChunk de = new DeChunk(digested_array, arr_name, latest_existing_pair_in_array);
					DeChunkResponse dcr = ReadWriteTextToFile.getInstance().remote_dechunk_test_response(
							CentralSingleton.objmapper.writeValueAsString(de), StockConstants.ChunkTest_dechunk_url);
					assertEquals("the returned comparison was:" + dcr.getDigest_comparison(), true,
							dcr.getDigest_comparison());
					assertEquals("the multi and single step digests are the same",
							multi_step_arr_digest_map.get(arr_name), digested_array);
				}
			}
		}

	}

	/*****
	 * what this tests is to download new prices, and then send them to remote
	 * then we do a simple dechunk request, and compare.
	 * 
	 * two step test would imply, to download some parts, then send to remote,
	 * then redownload and send again, and then ask for the dechunk.
	 * 
	 * we also have to test
	 * 
	 * @throws Exception
	 *             for
	 */

	// @Test
	public void single_step_chunk_dechunk_test() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.make_dummy_request = true;
		StockConstants.test_methods.put(test_basic_chunk_dechunk, new TestMethod(test_basic_chunk_dechunk,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_basic_chunk_dechunk))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> entry : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// first check the open_open and close_close

				ArrayList<String> entity_arrs = new ArrayList<String>(
						Arrays.asList(e.getUnique_name() + "_" + StockConstants.Entity_open_open_map_suffix,
								e.getUnique_name() + "_" + StockConstants.Entity_close_close_map_suffix));

				for (String arr_name : entity_arrs) {
					// if (arr_name.contains("open_open") ||
					// arr_name.contains("close_close")) {
					Integer latest_existing_pair_in_array = null;
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
							.setFetchSource(true).setQuery(QueryBuilders.termQuery("arr_name", arr_name)).setSize(1)
							.addSort(SortBuilders.fieldSort("latest_newly_added_pair_index")).execute().actionGet();
					System.out.println("the name of the array is:" + arr_name);
					assertEquals("there are hits", true, sr.getHits().getHits().length > 0);

					for (SearchHit hit : sr.getHits().getHits()) {
						System.out.println("this is the source of the hit");
						System.out.println(hit.getSource().toString());
						latest_existing_pair_in_array = ((Integer) hit.getSource()
								.get("latest_newly_added_pair_index"));
					}

					int[] existing_local_cs_arr = PairArray.get_decompressed_arr(arr_name, 0,
							CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
									* StockConstants.Entity_total_pairs_for_a_day);

					existing_local_cs_arr = Arrays.copyOfRange(existing_local_cs_arr, 0,
							latest_existing_pair_in_array + 1);

					// now digest it.
					String arr_as_string = DeChunkResponse.convertToString(existing_local_cs_arr);
					String digested_array = DigestUtils.sha256Hex(arr_as_string);
					// now make the request.
					DeChunk de = new DeChunk(digested_array, arr_name, latest_existing_pair_in_array);
					DeChunkResponse dcr = ReadWriteTextToFile.getInstance().remote_dechunk_test_response(
							CentralSingleton.objmapper.writeValueAsString(de), StockConstants.ChunkTest_dechunk_url);
					assertEquals("the returned comparison was:" + dcr.getDigest_comparison(), true,
							dcr.getDigest_comparison());
					// }
				}
			}
		}

	}

	
	/****
	 * what this test checks is as follows
	 * 
	 * first download then send a dechunk request, this will populate the remote
	 * singletons.
	 * 
	 * then redownload. then send another dechunk request this should update the
	 * remote singletons.
	 * 
	 * the arrays should be all equal.
	 * 
	 * @throws Exception
	 */
	//@Test
	public void multi_dechunk_test() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.define_custom_end_date = true;
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));
		// now we need to save the entitites to es, and restart the download.
		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);
		// now do a dechunk request.
		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			e.getValue().commit_entity_index_to_elasticsearch(1);
		}
		do_dechunk_request(initially_downloaded_exchanges, new ArrayList<String>(Arrays
				.asList(StockConstants.Entity_open_open_map_suffix, StockConstants.Entity_close_close_map_suffix)));
		// now we redownload.
		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		StockConstants.define_custom_end_date = false;
		// StockConstants.test_methods.put(test_basic_chunk_dechunk, new
		// TestMethod(test_basic_chunk_dechunk,
		// new
		// ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_basic_chunk_dechunk))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_to_remote,
				new TestMethod(test_should_skip_part_after_update_to_remote, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.test_should_skip_part_after_update_to_remote))));

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), true, false, true);
		// now repeat the dechunk request.
		do_dechunk_request(redownloaded_exchanges, new ArrayList<String>(Arrays
				.asList(StockConstants.Entity_open_open_map_suffix, StockConstants.Entity_close_close_map_suffix)));

	}

	@Test
	public void multi_step_dechunk_for_small_arrays() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.define_custom_end_date = true;
		StockConstants.make_dummy_request = true;
		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);
		HashMap<String, int[]> old_arrays = new HashMap<String, int[]>();
		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			e.getValue().commit_entity_index_to_elasticsearch(1);
			for (Map.Entry<String, Entity> entry : e.getValue().getEntity_map().entrySet()) {
				ArrayList<String> entity_arrs = new ArrayList<String>(Arrays.asList(
						entry.getValue().getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_week,
						entry.getValue().getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_month));

				for (String arr : entity_arrs) {
					old_arrays.put(arr,
							PairArray.get_decompressed_arr(arr, 1,
									CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
											* StockConstants.Entity_total_pairs_for_a_day));
				}

			}
		}
		System.out.println("--------------doing first run through--------------------------");
		do_dechunk_request_for_small_arrays(initially_downloaded_exchanges, new ArrayList<String>(Arrays
				.asList(StockConstants.Entity_last_day_of_prev_week, StockConstants.Entity_last_day_of_prev_month)));

		/// now start the download again
		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		StockConstants.define_custom_end_date = false;
		StockConstants.make_dummy_request = true;
		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), true, false, true);
		System.out.println("-------------doing second run through-----------------");
		do_dechunk_request_for_small_arrays(redownloaded_exchanges, new ArrayList<String>(Arrays
				.asList(StockConstants.Entity_last_day_of_prev_week, StockConstants.Entity_last_day_of_prev_month)));

	}
	
	/****
	 * for a given map of exchanges, will do the dechunk request, and compare
	 * the returned response to the arrays that exist in
	 * it currently tests only the BIG ARRAYS,Not the small arrays.
	 * 
	 * @param exchanges
	 * @param req_arr_suffixes:
	 *            suffixes for the arrays which need to be comapred.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 * @throws IOException
	 */
	public void do_dechunk_request(HashMap<String, EntityIndex> exchanges,
			ArrayList<String> req_arr_suffixes)
			throws JsonParseException, JsonMappingException, JsonGenerationException, IOException {
		for (Map.Entry<String, EntityIndex> entry : exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// first check the open_open and close_close

				ArrayList<String> entity_arrs = new ArrayList<String>();

				for (String suffix : req_arr_suffixes) {
					entity_arrs.add(e.getUnique_name() + "_" + suffix);
				}

				for (String arr_name : entity_arrs) {
					// if (arr_name.contains("open_open") ||
					// arr_name.contains("close_close")) {
					Integer latest_existing_pair_in_array = null;
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					// System.out.println("sleeping after the refresh for array
					// : " + arr_name);
					// Thread.sleep(5000);
					CountResponse cr = RemoteEs.getInstance().prepareCount("tradegenie_titan").setTypes("arrays")
							.setQuery(QueryBuilders.termQuery("arr_name", arr_name)).execute().actionGet();
					assertEquals("there are hits", true, cr.getCount() > 0);
					
					
					/*****
					 * one part will have to be devoted to testing the 
					 * two large arrays like
					 * open_open , and , close_close
					 */
					
					SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
							.setFetchSource(true).setQuery(QueryBuilders.termQuery("arr_name", arr_name))
							.addSort("latest_newly_added_pair_index", SortOrder.DESC).setSize(1).execute().actionGet();
					
					for (SearchHit hit : sr.getHits().getHits()) {
						latest_existing_pair_in_array = ((Integer) hit.getSource()
								.get("latest_newly_added_pair_index"));
					}

					int[] existing_local_cs_arr = PairArray.get_decompressed_arr(arr_name, 0,
							CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
									* StockConstants.Entity_total_pairs_for_a_day);

				

					// debug_chunk_by_chunk(existing_local_cs_arr, arr_name,
					// old_arrays.get(arr_name));
					
					//here if it has a latest_existing_pair_in_array, then copyofrange
					//otherwise leave as is.
					existing_local_cs_arr = Arrays.copyOfRange(existing_local_cs_arr, 0,
							latest_existing_pair_in_array + 1);

					// now digest it.
					String arr_as_string = DeChunkResponse.convertToString(existing_local_cs_arr);
					String digested_array = DigestUtils.sha256Hex(arr_as_string);
					// arr_name_and_digest.put(arr_name, digested_array);
					// now make the request.
					DeChunk de = new DeChunk(digested_array, arr_name, latest_existing_pair_in_array);
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					DeChunkResponse dcr = ReadWriteTextToFile.getInstance().remote_dechunk_test_response(
							CentralSingleton.objmapper.writeValueAsString(de), StockConstants.ChunkTest_dechunk_url);
					assertEquals("the returned comparison was:" + dcr.getDigest_comparison(), true,
							dcr.getDigest_comparison());
					// }
				}
			}
		}

	}
	

	public void do_dechunk_request_for_small_arrays(HashMap<String, EntityIndex> exchanges,
			ArrayList<String> req_arr_suffixes)
			throws JsonParseException, JsonMappingException, JsonGenerationException, IOException{
		for (Map.Entry<String, EntityIndex> entry : exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// first check the open_open and close_close

				ArrayList<String> entity_arrs = new ArrayList<String>();

				for (String suffix : req_arr_suffixes) {
					entity_arrs.add(e.getUnique_name() + "_" + suffix);
				}

				for (String arr_name : entity_arrs) {
					// if (arr_name.contains("open_open") ||
					// arr_name.contains("close_close")) {
					Integer latest_existing_pair_in_array = null;
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					// System.out.println("sleeping after the refresh for array
					// : " + arr_name);
					// Thread.sleep(5000);
					CountResponse cr = RemoteEs.getInstance().prepareCount("tradegenie_titan").setTypes("arrays")
							.setQuery(QueryBuilders.termQuery("arr_name", arr_name)).execute().actionGet();
					assertEquals("there are hits", true, cr.getCount() > 0);
					
					
					/*****
					 * one part will have to be devoted to testing the 
					 * two large arrays like
					 * open_open , and , close_close
					 */
					
					SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
							.setFetchSource(true).setQuery(QueryBuilders.termQuery("arr_name", arr_name))
							.setSize(1).execute().actionGet();
					
					
					int[] existing_local_cs_arr = PairArray.get_decompressed_arr(arr_name, 1,
							CentralSingleton.getInstance().gmt_datestring_to_day_id.size()
									);

				

					// debug_chunk_by_chunk(existing_local_cs_arr, arr_name,
					// old_arrays.get(arr_name));
					
					//here if it has a latest_existing_pair_in_array, then copyofrange
					//otherwise leave as is.
					System.out.println("existing local cs arr name is:" + arr_name  + " and arr is:");
					String arr_debug_string = "";
					for(int i = 0; i < existing_local_cs_arr.length; i++){
						arr_debug_string+=String.valueOf(existing_local_cs_arr[i]);
					}
					System.out.println(arr_debug_string);
					// now digest it.
					String arr_as_string = DeChunkResponse.convertToString(existing_local_cs_arr);
					String digested_array = DigestUtils.sha256Hex(arr_as_string);
					// arr_name_and_digest.put(arr_name, digested_array);
					// now make the request.
					DeChunk de = new DeChunk(digested_array, arr_name);
					RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
					DeChunkResponse dcr = ReadWriteTextToFile.getInstance().remote_dechunk_test_response(
							CentralSingleton.objmapper.writeValueAsString(de), StockConstants.ChunkTest_dechunk_url);
					//so the only way to test this would be to get the array from remotees.
					//and then decompress it and compare against the 
					
					assertEquals("the returned comparison was:" + dcr.getDigest_comparison(), true,
							dcr.getDigest_comparison());
					
					
					// }
				}
			}
		}
	}
	
	/***
	 * 
	 * need to test if it correctly updates retrospectively added pairs, first_chunk_id, last_chunk_id
	 */
	
}
