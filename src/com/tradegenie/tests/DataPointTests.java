package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.ExceptionStringGenerator;
import ExchangeBuilder.PercentageIntegerCorrelate;
import yahoo_finance_historical_scraper.StockConstants;

public class DataPointTests {
		
	@Rule
	public final ExpectedException expected_exception = ExpectedException.none();
	
	@Test
	public void test_datapoint_calculate_difference_from_end_day_dp() {

		/***
		 * first we test the basic correlate function, all the end datapoints
		 * values are just 100 above the start one.
		 * 
		 */
		DataPoint current_dp = new DataPoint();
		current_dp.setClose("100.10");
		current_dp.setOpen("200.10");
		current_dp.setHigh("330.10");
		current_dp.setLow("10.10");

		DataPoint end_dp = new DataPoint();
		end_dp.setClose("10000.10");
		end_dp.setOpen("20000.10");
		end_dp.setHigh("33000.10");
		end_dp.setLow("1000.10");

		PercentageIntegerCorrelate pc = current_dp.calculate_differences_from_end_day_datapoint(end_dp, true);
		ObjectMapper mapper = new ObjectMapper();
		
		assertEquals((Integer)98951, pc.getOpen_open_price_change_correlate());
		assertEquals((Integer)98901, pc.getClose_close_price_change_correlate());
		assertEquals((Integer)98970, pc.getHigh_high_price_change_correlate());
		assertEquals((Integer)98020, pc.getLow_low_price_change_correlate());
	
	}
	
	@Test
	public void number_format_exception_thrown_if_value_is_less_than_or_equal_to_zero(){
		DataPoint dp = new DataPoint();
		
		expected_exception.expect(NumberFormatException.class);
		expected_exception.expectMessage(ExceptionStringGenerator
				.generate_exception_string(StockConstants.datapoint_price_less_than_or_equal_to_zero_template,
						new HashMap<String,String>(){{
							put(StockConstants.datapoint_PRICE,"close_price");
							put(StockConstants.datapoint_VALUE,"0.00");
						}}));
		
		dp.setClose("0.00");
		
	}

}
