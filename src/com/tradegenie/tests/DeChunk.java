package com.tradegenie.tests;

public class DeChunk {
	/****
	 * used in tests, the expected digest of the array, sent from the home
	 * computer. used in dechunkresponse, which checks if the array is same as
	 * what we expect it to be at home.
	 * 
	 */
	private String expected_digest;

	public String getExpected_digest() {
		return expected_digest;
	}

	public void setExpected_digest(String expected_digest) {
		this.expected_digest = expected_digest;
	}

	private int[] dechunked_large_array;

	public int[] getDechunked_large_array() {
		return dechunked_large_array;
	}

	public void setDechunked_large_array(int[] dechunked_large_array) {
		this.dechunked_large_array = dechunked_large_array;
	}

	/***
	 * the chunk id from which to dechunk
	 * 
	 */
	private Integer dechunk_from;

	public Integer getDechunk_from() {
		return dechunk_from;
	}

	public void setDechunk_from(Integer dechunk_from) {
		this.dechunk_from = dechunk_from;
	}

	/***
	 * the chunk id upto which and inclusive to dechunk.
	 */
	private Integer dechunk_to;

	public Integer getDechunk_to() {
		return dechunk_to;
	}

	public void setDechunk_to(Integer dechunk_to) {
		this.dechunk_to = dechunk_to;
	}

	/***
	 * the name of the array for which you need to dechunk.
	 * 
	 */
	private String array_name;

	public String getArray_name() {
		return array_name;
	}

	public void setArray_name(String array_name) {
		this.array_name = array_name;
	}

	/***
	 * index in the large open_open or close_close arrays which we need. this is
	 * optional parameter.
	 */
	private Integer required_large_array_index;

	public Integer getRequired_large_array_index() {
		return required_large_array_index;
	}

	public void setRequired_large_array_index(Integer required_large_array_index) {
		this.required_large_array_index = required_large_array_index;
	}

	private Integer current_large_array_size;

	public Integer getCurrent_large_array_size() {
		return current_large_array_size;
	}

	public void setCurrent_large_array_size(Integer current_large_array_size) {
		this.current_large_array_size = current_large_array_size;
	}

	private Integer latest_newly_added_pair_index;

	public Integer getLatest_newly_added_pair_index() {
		return latest_newly_added_pair_index;
	}

	public void setLatest_newly_added_pair_index(Integer latest_newly_added_pair_index) {
		this.latest_newly_added_pair_index = latest_newly_added_pair_index;
	}

	public DeChunk() {
		
	}

	/***
	 * used in tests and makes sense to use everywhere else as well.
	 * 
	 * @param array_name
	 * @param required_large_array_index
	 */
	public DeChunk(String array_name, Integer required_large_array_index) {
		this.array_name = array_name;
		this.required_large_array_index = required_large_array_index;
	}

	/***
	 * 
	 * USE THIS CONTRCUTOR FOR TESTS, SINCE IT PROVIDES THE EXPECTED_DIGEST
	 * STRING.
	 * 
	 */

	public DeChunk(String expected_digest, String array_name, Integer required_large_array_index) {
		this.expected_digest = expected_digest;
		this.array_name = array_name;
		this.required_large_array_index = required_large_array_index;
	}
	
	/****
	 * use this constructor for testing the sorted arrays 
	 * 1) last_day_month_week array
	 * 2) bsl and tsl arrays.
	 * 
	 * @param expected_digest
	 * @param array_name
	 */
	public DeChunk(String expected_digest, String array_name){
		this.expected_digest = expected_digest;
		this.array_name = array_name;
	}


	/***
	 * constructor used for loading the tsl and bsl arrays from the db into the
	 * centralsingleton.
	 * 
	 * @param array_name
	 */
	public DeChunk(String array_name) {
		this.array_name = array_name;
	}

}
