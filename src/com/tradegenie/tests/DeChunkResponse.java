package com.tradegenie.tests;

public class DeChunkResponse {
	private String array_name;
	private String expected_digest;
	private Boolean digest_comparison;
	
	public Boolean getDigest_comparison() {
		return digest_comparison;
	}

	public void setDigest_comparison(Boolean digest_comparison) {
		this.digest_comparison = digest_comparison;
	}

	public String getArray_name() {
		return array_name;
	}

	public void setArray_name(String array_name) {
		this.array_name = array_name;
	}

	public String getExpected_digest() {
		return expected_digest;
	}

	public void setExpected_digest(String expected_digest) {
		this.expected_digest = expected_digest;
	}

	

	public DeChunkResponse(String array_name, String expected_digest){
		this.array_name = array_name;
		this.expected_digest = expected_digest;
	}
	
	public DeChunkResponse(){
		
	}

	final protected static char[] encoding = "0123456789ABCDEF".toCharArray();
    public static String convertToString(int[] arr) {
        char[] encodedChars = new char[arr.length * 4 * 2];
        for (int i = 0; i < arr.length; i++) {
            int v = arr[i];
            int idx = i * 4 * 2;
            for (int j = 0; j < 8; j++) {
                encodedChars[idx + j] = encoding[(v >>> ((7-j)*4)) & 0x0F];
            }
        }
        return new String(encodedChars);
    }
	
}
