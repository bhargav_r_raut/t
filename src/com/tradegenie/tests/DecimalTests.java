package com.tradegenie.tests;

import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;

import org.junit.Test;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.oscillators.AroonDownIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.AroonUpIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.StochasticOscillatorDIndicator;
import eu.verdelhan.ta4j.indicators.oscillators.StochasticOscillatorKIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.AccelerationDecelerationIndicator;
import eu.verdelhan.ta4j.indicators.trackers.RSIIndicator;

public class DecimalTests {
	
	/**
	 * to initialize a decimal with a non-existent value, 
	 * 
	 */
	//@Test
	public void null_test(){
		Decimal d = Decimal.valueOf("NaN");
		assertTrue("it should not be a number",d.isNaN());
	}
	
	//this test only has to pass wihtout throwing some exception.
	//so there is no assertion.
	//@Test
	public void tick_list_of_nulls(){
		TimeSeries ts = new TimeSeries();
		for(int i = 0; i < 100; i ++){
			ZonedDateTime zdt = ZonedDateTime.now().plusDays(i);
			Tick t = new Tick(zdt, Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"));
			
			ts.addTick(t);
		}
		
		//now with these timeseries, we want to calculate some indicators.
		AccelerationDecelerationIndicator adi = new AccelerationDecelerationIndicator(ts,
				5,14);
		for(int i = 0; i  < 100; i++){
			System.out.println(String.valueOf(i) + ":" + adi.getValue(i));
		}
	}
	
	@Test
	public void tick_list_of_nulls_with_aroon_up(){
		TimeSeries ts = new TimeSeries();
		for(int i = 0; i < 100; i ++){
			ZonedDateTime zdt = ZonedDateTime.now().plusDays(i);
			Tick t = new Tick(zdt, Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"),
					Decimal.valueOf("NaN"));
			
			ts.addTick(t);
		}
		
		//now with these timeseries, we want to calculate some indicators.
		RSIIndicator rsi = new RSIIndicator(new ClosePriceIndicator(ts), 10);
		AroonUpIndicator aru = new AroonUpIndicator(ts, 25);
		AroonDownIndicator ard = new AroonDownIndicator(ts, 25);
		AccelerationDecelerationIndicator adi = new AccelerationDecelerationIndicator(ts);
		StochasticOscillatorKIndicator stk = new StochasticOscillatorKIndicator(ts, 5);
		StochasticOscillatorDIndicator std = new StochasticOscillatorDIndicator(stk);
		for(int i = 0; i  < 100; i++){
			System.out.println(String.valueOf(i) + ":" + rsi.getValue(i));
		}
	}
	
}
