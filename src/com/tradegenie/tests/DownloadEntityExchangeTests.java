package com.tradegenie.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.TestMethod;
import ExchangeBuilder.TestMethodsExecutor;
import Titan.ReadWriteTextToFile;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class DownloadEntityExchangeTests extends BaseTest {

	// these are the markers that are declared into the test_methods of
	// stockconstants before
	// each test.
	// MARKERS
	public static final String stop_es_before_initializing_bulk_processor = "stop_es_before_initializing_bulk_processor";
	public static final String skip_shutdown_bulk_processors = "skip_shutdown_bulk_processors";
	public static final String when_es_empty_check_that_entity_linked_list_is_empty = "when_es_empty_check_that_entity_linked_list_is_empty";
	public static final String when_es_empty_latest_existing_day_id_should_be_zero = "when_es_empty_latest_existing_day_id_should_be_zero";
	public static final String WHEN_ES_EMPTY_check_request_made_and_start_and_end_dates = "check_request_made_and_start_and_end_dates_set";
	public static final String skip_after_enitty_download_hook = "dont_do_after_entity_download_hook";
	public static final String skip_after_entity_index_download_hook = "skip_after_entity_index_download_hook";
	public static final String add_datapoint_to_entity_method_triggered = "add_datapoint_to_entity_should_not_execute_if_request_response_cycle_fails";
	public static final String after_download_entity_hook_method_triggered = "after_download_entity_hook_should_not_execute_if_request_response_cycle_fails";
	public static final String throw_exception_from_make_request = "throw_exception_from_make_request";
	public static final String simulate_non_200_response_code = "simulate_non_200_response_code";
	public static final String simulate_invalid_json_response = "simulate_invalid_json_response";
	// json faulty exception simulations
	public static final String simulate_forex_date_not_in_response = "simulate_forex_date_not_in_response";
	public static final String simulate_forex_rates_key_not_in_response = "simulate_forex_rates_key_not_in_response";
	public static final String simulate_forex_symbol_not_in_response = "simulate_forex_symbol_not_in_response";
	public static final String simulate_quandl_empty_response = "simulate_quandl_empty_response";
	public static final String simulate_quandl_dataset_key_absent = "simulate_quandl_dataset_key_absent";
	public static final String simulate_quandl_data_key_absent_inside_dataset = "simulate_quandl_data_key_absent_inside_dataset_key";
	// number format exception simulations.
	public static final String simulate_number_format_exception_high = "simulate_number_format_exception_high";
	public static final String simulate_number_format_exception_low = "simulate_number_format_exception_low";
	public static final String simulate_number_format_exception_open = "simulate_number_format_exception_open";
	public static final String simulate_number_format_exception_close = "simulate_number_format_exception_close";
	public static final String simulate_number_format_exception_volume = "simulate_number_format_exception_volume";

	// force redownload simulations.
	public static final String simulate_force_redownload = "simulate_force_redownload";

	// simulate expected day id is absent
	public static final String simulate_expected_day_id_is_absent = "simulate_expected_day_id_is_absent";

	// should skip after prune.
	public static final String skip_after_prune_price_map_internal = "skip_after_prune_price_map_internal";

	// simulate force redownload when existing price becomes zero.
	public static final String simulate_force_redownload_due_close_zero = "simulate_force_redownload_due_to_close_zero";

	// simulate force redownload when existing datapoint changes.
	public static final String simulate_force_redownload_due_to_changed_existing_datapoint = "simulate_force_redownload_due_to_changed_existing_datapoint";

	// simulate newer invalid datapoints are not added if price is zero.
	public static final String simulate_new_datatpoint_not_added_when_it_is_invalid = "simulate_new_datapoint_not_added_when_it_is_invalid";

	@Rule
	public final ExpectedException expected_exception = ExpectedException.none();

	@Override
	public void setUp() throws Exception {
		// TODO Auto-generated method stub
		ShScript.start_all_servers();
		super.setUp();
	}

	public Long get_info_exceptions_in_es(String exception_classification, String entity_name, String exception_level) {
		CountResponse sr = RemoteEs.getInstance().prepareCount("tradegenie_titan")

				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.andFilter(
								FilterBuilders.termFilter(StockConstants.EsTypes_error_log_exception_classification,
										exception_classification),
						FilterBuilders.termFilter(StockConstants.ESTypes_error_log_entity_unique_name, entity_name),
						FilterBuilders.termFilter(StockConstants.EsTypes_error_log_exception_type,
								StockConstants.EsTypes_error_log_exception_levels.get(exception_level)))))
				.execute().actionGet();
		return sr.getCount();
	}

	public HashMap<String, EntityIndex> initially_downloaded() throws Exception {
		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
		LocalEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
		return initially_downloaded_exchanges;
	}
	
	
	// @Test
	public void when_bulk_processor_shutdown_returns_false_no_further_exchanges_should_be_processed() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		erp.build_all_exchanges();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = new EntityIndex(entry.getKey(), i);
			// after download shuts the processors and if they dont shut, it //
			// throws this exception.
			// so we add a test_method that skips the call to shutdown
			// //processors. // this is not a test in the real sense of the
			// word, but it // simulates what happens // if for whatever reason
			// the bulk
			// processors are not set to true // for shutdown // when the entity
			// index is initialized they are also initialized to // false.
			StockConstants.test_methods.put(skip_shutdown_bulk_processors, new TestMethod(skip_shutdown_bulk_processors,
					new ArrayList<String>(Arrays.asList(TestMethodsExecutor.dont_shutdown_bulk_processors))));

			expected_exception.expect(Exception.class);
			expected_exception.expectMessage(StockConstants.EntityIndex_failed_to_shut_bulk_processors);
			ei.after_download_entity_index();
			i++;
		}
	}

	// before download entity exchange tests.

	// @Test
	public void initialize_bulk_processor_when_es_is_down_should_fail() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		StockConstants.test_methods.put(stop_es_before_initializing_bulk_processor,
				new TestMethod(stop_es_before_initializing_bulk_processor, new ArrayList<String>(Arrays
						.asList(TestMethodsExecutor.stop_remote_es_server, TestMethodsExecutor.stop_local_es_server))));

		erp.build_all_exchanges();
		expected_exception.expect(NoNodeAvailableException.class);
		erp.download_all_exchanges();

	}

	// @Test
	public void all_entities_from_es_should_get_loaded() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_entity_indices = erp.build_all_exchanges();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = new EntityIndex(entry.getKey(), i);
			ei.before_download_entity_index();

			EntityIndex built_index = built_entity_indices.get(ei.getIndex_name());

			for (Map.Entry<String, Entity> b_entry : built_index.getEntity_map().entrySet()) {
				assertTrue("this entity " + b_entry.getKey() + " should have been built in the pre_download_phase",
						ei.getEntity_map().containsKey(b_entry.getKey()));
			}
			i++;
		}
	}

	// @Test
	public void when_no_data_in_ES_then_ALL_entities_es_linked_list_should_be_empty() throws Exception {
		StockConstants.test_methods.put(when_es_empty_check_that_entity_linked_list_is_empty, new TestMethod(
				when_es_empty_check_that_entity_linked_list_is_empty,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.check_entity_es_linked_list_is_empty))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_entity_indices = erp.build_all_exchanges();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = new EntityIndex(entry.getKey(), i);
			ei.before_download_entity_index(); // the assertion is through
			// testsingleton. // inside the test method the
			for (Map.Entry<String, Entity> eim : ei.getEntity_map().entrySet()) {
				assertTrue("the entity's es linked list is empty", TestSingleton.getTsingle()
						.getTest_expected_values_map().containsKey(eim.getValue().getUnique_name()));
			}
			i++;
		}

	}

	// @Test
	public void when_no_data_in_es__latest_existing_day_id_should_be_zero() throws Exception {

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_entity_indices = erp.build_all_exchanges();
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = new EntityIndex(entry.getKey(), i);
			ei.before_download_entity_index(); //
			// the assertion is through testsingleton. // as long as this entity
			// is
			// present inside the testsingleton, means // it has passed // the
			// test.
			for (Map.Entry<String, Entity> eim : ei.getEntity_map().entrySet()) {
				assertTrue("the entity's es latest existing day id should be zero",
						eim.getValue().getLatest_existing_day_id() == 0);
				assertTrue("the entity's request start datestring should be",
						eim.getValue().getRequest_start_date()
								.equals(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstEntry().getValue()
										.getDateString()));
				assertTrue("the entity's request start day id should be",
						eim.getValue()
								.getRequest_start_day_id() == CentralSingleton.getInstance().gmt_day_id_to_dataPoint
										.firstKey());

				DateTime dt = new DateTime(DateTimeZone.forID("GMT"));
				dt = dt.plusDays(2);
				String expected_end_date_string = String.valueOf(dt.getYear()) + "-"
						+ StringUtils.leftPad(String.valueOf(dt.getMonthOfYear()), 2, "0") + "-"
						+ StringUtils.leftPad(String.valueOf(dt.getDayOfMonth()), 2, "0");

				assertTrue("the entity's request end datestrings should be present day + 2 days",
						eim.getValue().getRequest_end_date().equals(expected_end_date_string));
			}
			i++;
		}

	}

	/*******
	 * QUANDL REQUEST TESTS:
	 * 
	 * the same tests should also be applied to forex. and they have to
	 * reapplied once there is some data in the database they have to be
	 * reapplied when trying to do force_redownload.
	 * 
	 * @throws Exception
	 */

	// @Test
	public void test_all_entities_send_requests() throws Exception {
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> ei_map = erp.build_all_exchanges();
		StockConstants.test_methods.put(WHEN_ES_EMPTY_check_request_made_and_start_and_end_dates, new TestMethod(
				WHEN_ES_EMPTY_check_request_made_and_start_and_end_dates,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.check_quandl_request_made_when_es_is_empty))));
		StockConstants.test_methods.put(skip_after_enitty_download_hook, new TestMethod(skip_after_enitty_download_hook,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.skip_after_download_entity_hook))));
		StockConstants.test_methods.put(skip_after_entity_index_download_hook, new TestMethod(
				skip_after_entity_index_download_hook,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.skip_after_download_entity_index_hook))));

		erp.download_all_exchanges();
		HashMap<String, TestSingletonValue> test_results_map = TestSingleton.getTsingle().getTest_expected_values_map();

		for (Map.Entry<String, EntityIndex> entry : ei_map.entrySet()) {

			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {

				assertTrue("the built entity should have been attempted to be downloaded",
						test_results_map.get(entity_entry.getKey()).getRequest_made());

				assertTrue("the entity start and end date should have been correctly set",
						(test_results_map.get(entity_entry.getKey()).getRequest_start_date() != null)
								&& (test_results_map.get(entity_entry.getKey()).getRequest_end_date() != null));

			}

		}

	}

	/*****
	 * TODO:: 1.test_method has to be added to after_download_entity_hook, to
	 * check that it is not triggered. 2.forex testing has to be seperated from
	 * this method 3.differences a.request response cycle should have completed.
	 * b.no exceptions should be there for any of the forex. c.assert that the
	 * response code is detected as not 200.
	 * 
	 * 4.when initially there is noting, but after some time there is something,
	 * same as above, but in that case, after_download_entity_hook should fire,
	 * and so should the add_data_point_to_entity
	 * 
	 * 5.when there is first something in the result object and then there is a
	 * non 200 then behaviour should be like that described in below function.
	 * 
	 * 6.currently only building forex, so it is only doing forex, but now make
	 * it build all and seperate forex download and post download eval into a
	 * seperate function.
	 * 
	 * we simulate a request failure and expect to see an info exception in the
	 * log we also do not expect any code in the add data_point to entity to
	 * execute we also expect that the size of the datapoint_map should not
	 * change, i.e no new datapoints should be added. we also expect that
	 * request_exception is set to true so that the after_download_entity hook
	 * should not fire. there should be no entities in the entity map since they
	 * should all have been filtered.
	 * 
	 * @throws Exception
	 * 
	 */
	public HashMap<String, EntityIndex> when_request_response_cycle_is_not_successfull(
			ExchangeBuilderPackageCoordinator erp, HashMap<String, EntityIndex> built_exchanges,
			String exception_classification) throws Exception {

		StockConstants.test_methods.put(add_datapoint_to_entity_method_triggered,
				new TestMethod(add_datapoint_to_entity_method_triggered,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.method_triggered))));

		StockConstants.test_methods.put(after_download_entity_hook_method_triggered,
				new TestMethod(after_download_entity_hook_method_triggered,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.method_triggered))));

		HashMap<String, EntityIndex> downloaded_exchanges = new LinkedHashMap<String, EntityIndex>();

		Integer i = 0;
		for (Map.Entry<String, String> ei_names : StockConstants.exchange_builder_constants.entrySet()) {
			if (built_exchanges.containsKey(ei_names.getKey())) {
				EntityIndex ei = erp.download_entityExchange(ei_names.getKey(), i);
				downloaded_exchanges.put(ei.getIndex_name(), ei);
			}
			i++;
		}
		// basically we only download the built exchanges.

		for (Map.Entry<String, EntityIndex> entry : downloaded_exchanges.entrySet()) {

			assertTrue("the entity map should be empty", entry.getValue().getEntity_map().size() == 0);

			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();

				assertTrue("the request should not have been successfull completed",
						!e.getRb().getRequest_response_successfull());

				assertTrue("there should be no datapoints in the price map internal",
						e.getPrice_map_internal().size() == 0);

				assertTrue("the add datapoint to entity method should not have been triggered",
						!TestSingleton.getTsingle().getTest_expected_values_map()
								.get(e.getUnique_name()).methods_triggered
										.contains(add_datapoint_to_entity_method_triggered));

				assertTrue("the after entity download hook should not have been triggered",
						!TestSingleton.getTsingle().getTest_expected_values_map()
								.get(e.getUnique_name()).methods_triggered
										.contains(after_download_entity_hook_method_triggered));

				// now check that an info exception exists with request failed
				// as such.

				Long exceptions = get_info_exceptions_in_es(exception_classification, e.getUnique_name(),
						StockConstants.EsTypes_error_log_exception_type_info);

				assertEquals(
						"there should be one info level exception with request failed as its classification for this entity",
						(Long) 1L, exceptions);

			}
		}

		/****
		 * remove these test methods so that they dont affect subsequent parts
		 * of the test in case those parts dont need these methods.
		 * 
		 */
		StockConstants.test_methods.remove(add_datapoint_to_entity_method_triggered);
		StockConstants.test_methods.remove(after_download_entity_hook_method_triggered);

		return downloaded_exchanges;

	}

	// @Test
	public void no_wifi_should_satisfy_request_failed_conditions() throws Exception {
		ShScript.stop_wifi();
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = erp.build_all_exchanges();
		when_request_response_cycle_is_not_successfull(erp, built_exchanges, StockConstants.request_failed);
		ShScript.start_wifi();
	}

	// @Test
	public void timeout_should_satisfy_request_failed_conditions() throws Exception {
		ShScript.set_request_variables_to_simulate_timeout();
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = erp.build_all_exchanges();
		when_request_response_cycle_is_not_successfull(erp, built_exchanges, StockConstants.request_failed);
		ShScript.reset_request_timeout_variables();
	}

	// @Test
	public void exception_in_make_request_function_should_satisfy_request_failed_conditions() throws Exception {
		StockConstants.test_methods.put(throw_exception_from_make_request,
				new TestMethod(throw_exception_from_make_request,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.throw_exception))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = erp.build_all_exchanges();
		when_request_response_cycle_is_not_successfull(erp, built_exchanges, StockConstants.request_failed);
	}

	// @Test
	public void invalid_json_response() throws Exception {
		StockConstants.test_methods.put(simulate_invalid_json_response, new TestMethod(simulate_invalid_json_response,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.invalid_json_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = erp.build_all_exchanges();
		when_request_response_cycle_is_not_successfull(erp, built_exchanges,
				StockConstants.response_json_parse_exception);
	}

	// @Test
	public void non_200_response_code() throws Exception {
		StockConstants.test_methods.put(simulate_non_200_response_code, new TestMethod(simulate_non_200_response_code,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.non_200_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			EntityIndex ei = erp.build_entityExchange(i, e.getKey());
			built_exchanges.put(ei.getIndex_name(), ei);
			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.response_code_not_200);

		for (Map.Entry<String, EntityIndex> entry : downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> ee : entry.getValue().getEntity_map().entrySet()) {
				assertTrue("the response code is not 200", ee.getValue().getRb().getResponse_code() != 200);
			}
		}

	}

	// @Test
	public void test_forex_no_dates_key_in_response() throws Exception {
		StockConstants.test_methods.put(simulate_forex_date_not_in_response,
				new TestMethod(simulate_forex_date_not_in_response,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.forex_date_not_in_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}
			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.forex_date_not_found_in_response);
	}

	// @Test
	public void test_forex_no_rates_key_in_response() throws Exception {
		StockConstants.test_methods.put(simulate_forex_rates_key_not_in_response,
				new TestMethod(simulate_forex_rates_key_not_in_response,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.forex_rates_key_not_in_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}
			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.forex_rates_not_found_in_response);
	}

	// @Test
	public void test_forex_no_symbol_key_in_rates() throws Exception {

		StockConstants.test_methods.put(simulate_forex_symbol_not_in_response,
				new TestMethod(simulate_forex_symbol_not_in_response,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.forex_symbol_not_in_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}

			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.forex_symbol_not_found_in_response);

	}

	// @Test
	public void test_quandl_empty_result_object() throws Exception {
		StockConstants.test_methods.put(simulate_quandl_empty_response, new TestMethod(simulate_quandl_empty_response,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.quandl_empty_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (!e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}

			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.quandl_empty_response);
	}

	// @Test
	public void test_quandl_dataset_key_missing() throws Exception {
		StockConstants.test_methods.put(simulate_quandl_dataset_key_absent,
				new TestMethod(simulate_quandl_dataset_key_absent,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.quandl_dataset_key_not_in_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (!e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}

			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.quandl_dataset_key_not_found_in_response);
	}

	// @Test
	public void test_quandl_data_key_missing() throws Exception {
		StockConstants.test_methods.put(simulate_quandl_data_key_absent_inside_dataset,
				new TestMethod(simulate_quandl_data_key_absent_inside_dataset, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.quandl_data_key_not_in_dataset_key_in_response))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (!e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}

			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.quandl_data_key_not_found_in_dataset_key);
	}

	/****
	 * number format is also info level exception.
	 * 
	 * @throws Exception
	 */
	// @Test
	public void close_number_format_exception() throws Exception {
		StockConstants.test_methods.put(simulate_number_format_exception_close,
				new TestMethod(simulate_number_format_exception_close,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.close_number_format_exception))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (!e.getKey().equals(StockConstants.name_for_forex)) {
				EntityIndex ei = erp.build_entityExchange(i, e.getKey());
				built_exchanges.put(ei.getIndex_name(), ei);
			}

			i++;
		}

		HashMap<String, EntityIndex> downloaded_exchanges = when_request_response_cycle_is_not_successfull(erp,
				built_exchanges, StockConstants.close_numberformat_exception);

	}

	/***
	 * loads the given entity indices from elasticsearch.
	 * 
	 * @param exchanges_to_load
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, EntityIndex> load_entity_indices_from_es(ArrayList<String> exchanges_to_load)
			throws Exception {
		HashMap<String, EntityIndex> loaded_indices = new HashMap<String, EntityIndex>();
		if (exchanges_to_load == null || exchanges_to_load.size() == 0) {
			for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
				exchanges_to_load.add(e.getKey());
			}
		}
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (exchanges_to_load.contains(e.getKey())) {
				EntityIndex ei = new EntityIndex(e.getKey(), i);
				ei.build_entity_index_from_elasticsearch();
				loaded_indices.put(ei.getIndex_name(), ei);
			}
			i++;
		}
		i = 0;
		return loaded_indices;
	}

	/***
	 * builds the exchanges specified in the input argument
	 * 
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, EntityIndex> build_exchanges(List<String> list, Boolean build_from_es) throws Exception {
		StockConstants.test_methods.put(skip_after_enitty_download_hook, new TestMethod(skip_after_enitty_download_hook,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.skip_after_download_entity_hook))));
		StockConstants.test_methods.put(skip_after_entity_index_download_hook, new TestMethod(
				skip_after_entity_index_download_hook,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.skip_after_download_entity_index_hook))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();

		if (list == null || list.size() == 0) {
			for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
				list.add(e.getKey());
			}
		}

		// some problem at this level.

		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;

		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (list.contains(e.getKey())) {
				EntityIndex ei = null;
				if (build_from_es) {
					ei = new EntityIndex(e.getKey(), i);
					ei.build_entity_index_from_elasticsearch();
				} else {
					ei = erp.build_entityExchange(i, e.getKey());
				}
				built_exchanges.put(ei.getIndex_name(), ei);
			}
			i++;
		}

		return built_exchanges;
	}

	/***
	 * 
	 * @param built_exchanges
	 *            : these exchanges will be downloaded
	 * @param exchanges_to_be_downloaded:
	 *            can be null, in which case the first argument better not be
	 *            null.
	 * 
	 * @return : the downloaded_exchanges.
	 * @throws Exception
	 */
	public HashMap<String, EntityIndex> download_exchanges(HashMap<String, EntityIndex> built_exchanges)
			throws Exception {

		for (Map.Entry<String, EntityIndex> built_entry : built_exchanges.entrySet()) {
			EntityIndex ei = built_entry.getValue();
			System.out.println("doing download entity index");
			ei.download_entity_index();
		}

		return built_exchanges;
	}

	/***
	 * 
	 * @param exchanges_to_download:
	 *            list of exchanges to be downloaded.
	 * @param build_from_es:
	 *            true if you want to load the entity initially from es,
	 *            false if you want to build the entity fresh, using wikipedia and all
	 * @param skip_before_download_entity_and_entity_index_fns:
	 *            true if you want the program to skip the
	 *            after_entity_download_hook as well as the
	 *            after_entity_index_download hooks
	 * 
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, EntityIndex> build_and_download_all_entities(ArrayList<String> exchanges_to_download,
			Boolean build_from_es,
			Boolean skip_after_download_entity,
			Boolean skip_after_download_entity_index) throws Exception {

		if (skip_after_download_entity) {
			StockConstants.test_methods.put(skip_after_enitty_download_hook,
					new TestMethod(skip_after_enitty_download_hook,
							new ArrayList<String>(Arrays.asList(TestMethodsExecutor.skip_after_download_entity_hook))));
		} else {
			StockConstants.test_methods.remove(skip_after_enitty_download_hook);
		}
		
		if(skip_after_download_entity_index){
			StockConstants.test_methods.put(skip_after_entity_index_download_hook,
					new TestMethod(skip_after_entity_index_download_hook,
		new ArrayList<String>(Arrays.asList(TestMethodsExecutor
				.skip_after_download_entity_index_hook))));
		}
		else{
			StockConstants.test_methods.remove(skip_after_entity_index_download_hook);
		}

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();

		if (exchanges_to_download == null || exchanges_to_download.size() == 0) {
			for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
				exchanges_to_download.add(e.getKey());
			}
		}

		// System.out.println("initially exchanges to download are");
		// System.out.println(exchanges_to_download);

		HashMap<String, EntityIndex> built_exchanges = new LinkedHashMap<String, EntityIndex>();
		Integer i = 0;
		for (Map.Entry<String, String> e : StockConstants.exchange_builder_constants.entrySet()) {
			if (exchanges_to_download.contains(e.getKey())) {
				EntityIndex ei = null;
				if (build_from_es) {
					ei = new EntityIndex(e.getKey(), i);
					ei.build_entity_index_from_elasticsearch();
				} else {
					ei = erp.build_entityExchange(i, e.getKey());
				}
				built_exchanges.put(ei.getIndex_name(), ei);
			}
			i++;
		}
		i = 0;
		// now download all these exchanges, and commit them.

		for (Map.Entry<String, EntityIndex> built_entry : built_exchanges.entrySet()) {
			EntityIndex ei = built_entry.getValue();
			ei.download_entity_index();
		}
		System.out.println("build exchanges keyset is");
		System.out.println(built_exchanges.keySet());
		return built_exchanges;
	}

	// @Test
	public void downloaded_data_points_should_be_same_when_loaded_from_es() throws Exception {
		CentralSingleton.initializeSingleton();
		HashMap<String, EntityIndex> initially_downloaded_exchanges = 
				build_and_download_all_entities(
				new ArrayList<String>(
						Arrays.asList(new String[] { StockConstants.name_for_oil, StockConstants.name_for_forex })),
				false, true,true);
		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			e.getValue().commit_entity_index_to_elasticsearch(1);
		}
		// commmit is complete.
		// now we have to reload from the db.
		HashMap<String, EntityIndex> exchanges_loaded_from_db = load_entity_indices_from_es(new ArrayList<String>(
				Arrays.asList(new String[] { StockConstants.name_for_oil, StockConstants.name_for_forex })));

		tearDown();
		setUp();

		// System.out.println("SHOULD REDOWNLOAD FROM
		// HERE---------------------------------");

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(
						Arrays.asList(new String[] { StockConstants.name_for_oil, StockConstants.name_for_forex })),
				false, true,true);

		for (Map.Entry<String, EntityIndex> ei_entry : redownloaded_exchanges.entrySet()) {
			if (exchanges_loaded_from_db.containsKey(ei_entry.getKey())) {
				assertEquals("exchange is equal:" + ei_entry.getKey(), true,
						exchanges_loaded_from_db.get(ei_entry.getKey()).equals_and_not_empty(ei_entry.getValue()));
			} else {
				fail("database did not have exchange : " + ei_entry.getKey());
			}
		}
		// need to also check if the individual datapoints both are not null
	}

	public void latest_day_id_and_start_end_request_date_tests() throws Exception {
		// need to check all the eventualities.
		// so first we want to build all exchanges.
		// then we want to check the start date.
		CentralSingleton.initializeSingleton();
		HashMap<String, EntityIndex> built_exchanges = build_exchanges(
				Arrays.asList(new String[] { StockConstants.name_for_oil }), false);

		// now rebuild the exchanges from elasticsearch
		// expected end date
		DateTime dt = new DateTime(DateTimeZone.forID("GMT"));
		dt = dt.plusDays(2);
		String initial_end_date_string_for_request = String.valueOf(dt.getYear()) + "-"
				+ StringUtils.leftPad(String.valueOf(dt.getMonthOfYear()), 2, "0") + "-"
				+ StringUtils.leftPad(String.valueOf(dt.getDayOfMonth()), 2, "0");

		String initial_start_date_string_for_request = CentralSingleton.getInstance().gmt_day_id_to_dataPoint
				.firstEntry().getValue().getDateString();

		Integer initial_latest_existing_day_id = CentralSingleton.getInstance().gmt_day_id_to_dataPoint.firstKey();

		for (Map.Entry<String, EntityIndex> entry : built_exchanges.entrySet()) {
			entry.getValue().build_entity_index_from_elasticsearch();
			// now assert that each of its entities, has the correct latest
			// existing day , ids,
			// as well as start and end request dates.
			for (Map.Entry<String, Entity> entity_entry : entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				assertEquals(initial_latest_existing_day_id, e.getLatest_existing_day_id());
				assertEquals(initial_start_date_string_for_request, e.getRequest_start_date());
				assertEquals(initial_end_date_string_for_request, e.getRequest_end_date());
			}

		}

		built_exchanges = download_exchanges(built_exchanges);
		for (Map.Entry<String, EntityIndex> ei_entry : built_exchanges.entrySet()) {
			ei_entry.getValue().commit_entity_index_to_elasticsearch(1);
		}

		HashMap<String, EntityIndex> rebuilt_exchanges = build_exchanges(
				Arrays.asList(new String[] { StockConstants.name_for_oil }), true);
		// now check the values.
		assertTrue("rebuilt exchanges exist", rebuilt_exchanges.size() > 0);
		for (Map.Entry<String, EntityIndex> ei_entry : rebuilt_exchanges.entrySet()) {
			assertTrue("rebuilt exchange has entities", ei_entry.getValue().getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> ei_entity : ei_entry.getValue().getEntity_map().entrySet()) {
				Entity e = ei_entity.getValue();

				assertTrue("start request date is not equal to the earlier one",
						!e.getRequest_start_date().equals(initial_start_date_string_for_request));
				assertTrue("end request date is equal to the earlier one.",
						e.getRequest_end_date().equals(initial_end_date_string_for_request));

				assertTrue("the latest existing day id is greater than the earlier one,",
						e.getLatest_existing_day_id() > initial_latest_existing_day_id);
			}
		}

	}

	/***
	 * this tests the situation where 1)initially we have downloaded the entity
	 * and committed it to elasticsearch 2)after that we now try to download it
	 * again, and we find that of the last ten days before the required date,
	 * this time some of those days are missing from the response ,this should
	 * provoke the entity to get filtered out.
	 * 
	 * @throws Exception
	 */

	public void test_expected_day_id_absent() throws Exception {
		CentralSingleton.initializeSingleton();
		// so to do this we have to first download the entity, then rebuild it,
		// then we have to redownload it, but this time, we have to detect that
		// some prices
		// have changed.
		// how to change the data that is received
		// and what should happen after that?
		// the entity should get filtered out.
		HashMap<String, EntityIndex> initially_downloaded_exchanges = 
				build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[]
						{ StockConstants.name_for_oil })), false, true,true);
		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> em : e.getValue().getEntity_map().entrySet()) {
				assertEquals("force redownload should be false", false, em.getValue().getForce_redownload());
			}
			e.getValue().commit_entity_index_to_elasticsearch(1);
		}

		// now we will have to download them again.
		// but this time
		StockConstants.test_methods.put(simulate_expected_day_id_is_absent,
				new TestMethod(simulate_expected_day_id_is_absent,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.expected_day_id_missing))));
						// now we have to download exchanges.

		// now call build and download again.
		// this time we dont skip the after download entity hook.
		// but why, we can skip it.
		// the force redownload is expected in the requestBase phase.
		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] 
						{ StockConstants.name_for_oil })), true, false,false);

		// now we have to check that there are no entites in the entity map,
		// left.
		// and that all of them have the filter set to true.
		for (Map.Entry<String, EntityIndex> ei_entry : redownloaded_exchanges.entrySet()) {
			EntityIndex ei = ei_entry.getValue();
			assertTrue(ei.exchange_is_empty());
		}

	}

	// DO FROM 4 - 5
	public void test_new_invalid_datapoint_not_added_to_price_map() throws Exception {

	}

	// PASSES
	public void test_force_redownload_due_to_invalid_datapoint() throws Exception {
		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = 
				build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] 
						{ StockConstants.name_for_oil })), false, true,true);

		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> em : e.getValue().getEntity_map().entrySet()) {
				assertEquals("force redownload should be false", false, em.getValue().getForce_redownload());
			}
			e.getValue().commit_entity_index_to_elasticsearch(1);
		}

		StockConstants.test_methods.put(simulate_force_redownload_due_close_zero,
				new TestMethod(simulate_force_redownload_due_close_zero,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.close_is_zero))));

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(
						new String[] { StockConstants.name_for_oil })), true, true,true);

		for (Map.Entry<String, EntityIndex> ei_entry : redownloaded_exchanges.entrySet()) {
			EntityIndex ei = ei_entry.getValue();
			assertTrue(ei.getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> entity_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				assertTrue(entity_entry.getValue().getPrice_map_internal().size() > 0);
				assertTrue(entity_entry.getValue().getForce_redownload());
			}
		}

	}

	// PASSES
	public void test_force_redownload_due_to_changed_existing_datapoint() throws Exception {

		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[]
						{ StockConstants.name_for_oil })), false, true, true);

		for (Map.Entry<String, EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> em : e.getValue().getEntity_map().entrySet()) {
				assertEquals("force redownload should be false", false, em.getValue().getForce_redownload());
			}
			e.getValue().commit_entity_index_to_elasticsearch(1);
		}

		StockConstants.test_methods.put(simulate_force_redownload_due_to_changed_existing_datapoint,
				new TestMethod(simulate_force_redownload_due_to_changed_existing_datapoint,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.changed_existing_datapoint))));
		System.out.println("---------------------------- STARTING REDOWNLOAD");
		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[]
						{ StockConstants.name_for_oil })), true, true,true);

		for (Map.Entry<String, EntityIndex> ei_entry : redownloaded_exchanges.entrySet()) {
			EntityIndex ei = ei_entry.getValue();
			assertTrue(ei.getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> entity_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				assertTrue(entity_entry.getValue().getPrice_map_internal().size() > 0);
				assertTrue(entity_entry.getValue().getForce_redownload());
			}
		}

	}


	public void test_does_not_add_new_invalid_datapoints_but_adds_valid_datapoints() throws Exception {

		CentralSingleton.initializeSingleton();

		StockConstants.test_methods.put(simulate_new_datatpoint_not_added_when_it_is_invalid, new TestMethod(
				simulate_new_datatpoint_not_added_when_it_is_invalid,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.
						new_datapoint_not_added_due_to_invalidity))));
		//System.out.println("---------------------------- STARTING REDOWNLOAD");
		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(
						new String[] { StockConstants.name_for_oil })), false, true, true);

		for (Map.Entry<String, EntityIndex> ei_entry : redownloaded_exchanges.entrySet()) {
			EntityIndex ei = ei_entry.getValue();
			// assert that the entiy map size is less by the required amount.
			for (Map.Entry<String, Entity> entity_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				TestSingletonValue tv = TestSingleton.getTsingle()
						.get_singleton_value(new TestSingletonValue(entity_entry.getValue()));
				//System.out.println(tv.getdatapoints_invalidated_randomly());
				//System.out.println(tv.getTotal_datapoints_encountered());
				Integer expected_count = tv.getTotal_datapoints_encountered().intValue()
						- tv.getdatapoints_invalidated_randomly().intValue();
				assertEquals("the price map has all datapoints other than those invalidated", (Integer) expected_count,
						(Integer) entity_entry.getValue().getPrice_map_internal().size());
			}
		}

	}
	
	

	/****
	 * public void simulate_force_redownload() throws Exception {
	 * CentralSingleton.initializeSingleton(); // so to do this we have to first
	 * download the entity, then rebuild it, // then we have to redownload it,
	 * but this time, we have to detect that // some prices // have changed. //
	 * how to change the data that is received // and what should happen after
	 * that? // the entity should get filtered out. HashMap<String, EntityIndex>
	 * initially_downloaded_exchanges = build_and_download_all_entities( new
	 * ArrayList<String>(Arrays.asList(new String[] {
	 * StockConstants.name_for_oil })), false, true); for (Map.Entry<String,
	 * EntityIndex> e : initially_downloaded_exchanges.entrySet()) {
	 * e.getValue().commit_entity_index_to_elasticsearch(1); }
	 * 
	 * // now we will have to download them again. // but this time
	 * StockConstants.test_methods.put(simulate_force_redownload, new
	 * TestMethod(simulate_force_redownload, new ArrayList
	 * <String>(Arrays.asList(TestMethodsExecutor.force_redownload)))); // now
	 * we have to download exchanges.
	 * 
	 * // now call build and download again.
	 * 
	 * HashMap<String, EntityIndex> redownloaded_exchanges =
	 * build_and_download_all_entities( new ArrayList<String>(Arrays.asList(new
	 * String[] { StockConstants.name_for_oil })), true, false);
	 * 
	 * // now we have to check that there are no entites in the entity map, //
	 * left. // and that all of them have the filter set to true. for
	 * (Map.Entry<String, EntityIndex> ei_entry :
	 * redownloaded_exchanges.entrySet()) { EntityIndex ei =
	 * ei_entry.getValue(); assertTrue(ei.exchange_is_empty()); }
	 * 
	 * }
	 **/
	
	/**
	 * @throws Exception *
	 * 
	 * 
	 * 
	 */
	@Test
	public void test_build_and_then_download() throws Exception{
		StockConstants.define_custom_end_date = true;
		StockConstants.minus_days_from_current_datetime = 200;
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		initially_downloaded_exchanges = null;
		CentralSingleton.destroySingleton();
		
		StockConstants.define_custom_end_date = true;
		CentralSingleton.initializeSingleton();
		initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), true, false, true);;
		
	}
	
	// now time to create a ui to visualize this stuff.
	

}
