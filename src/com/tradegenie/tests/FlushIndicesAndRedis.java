package com.tradegenie.tests;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.client.Client;

import ExchangeBuilder.LogObject;
import Indicators.RedisManager;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;

public class FlushIndicesAndRedis {
	public static void main(String[] args) {

	}

	public static void flush() {
		LogObject lgd = new LogObject(FlushIndicesAndRedis.class);
		recreate_index(LocalEs.getInstance());
		recreate_index(RemoteEs.getInstance());
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String redis_flush_response = jedis.flushAll();

			if (redis_flush_response.equals(StockConstants.redis_ok_response)) {
				lgd.commit_debug_log("redis: flushed");
			} else {
				lgd.commit_error_log("redis: flush failed", new Exception("failed to flush redis"));
			}
		} catch (Exception e) {
			lgd.commit_error_log("flush failed", e);
		}
	}

	public static void recreate_index(Client client) {
		LogObject lgd = new LogObject(FlushIndicesAndRedis.class);
		lgd.add_to_info_packet("status", "recreate index for:" + client.settings().get("cluster.name"));
		IndicesExistsResponse res = client.admin().indices().prepareExists("tradegenie_titan").execute().actionGet();
		if (res.isExists()) {
			DeleteIndexRequest delres = new DeleteIndexRequest("tradegenie_titan");
			DeleteIndexResponse resp = client.admin().indices().delete(delres).actionGet();
			if (resp.isAcknowledged()) {
				lgd.commit_debug_log("deleted index success");
				ShScript.create_index(client);
			}
		}
		else{
			lgd.commit_debug_log("index didnt exist so creating it.");
			ShScript.create_index(client);
		}
			
	}

}
