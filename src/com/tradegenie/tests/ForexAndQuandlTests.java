package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.junit.Test;

import DataProvider.ForexRequest;
import DataProvider.QuandlRequest;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.LogObject;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class ForexAndQuandlTests extends BaseTest {

	/*******
	 * TXN -> this symbol has a date where open is null. this function tests,
	 * that no exception is encountered while setting the datapoints for this
	 * entity.
	 * 
	 * two things are tested. a) open is null at least one. b) no exception is
	 * encountered(this is not explicitly tested, just that the test should
	 * pass.)
	 * 
	 * 
	 * @throws Exception
	 */

	/****
	 * build entity index from elasticsearch
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public EntityIndex build_entity_index_from_es(String exchange_name, Integer exchange_number) throws Exception {

		EntityIndex ei = new EntityIndex(exchange_name, exchange_number);

		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity").setSize(500)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termFilter("indice", ei.getIndex_name())))
				.execute()

				.actionGet();

		LogObject lg_1 = new LogObject(ForexAndQuandlTests.class);

		lg_1.add_to_info_packet("total_entities_in_es_from_entity_index", resp.getHits().getHits().length);
		if (resp.getHits().getHits().length == 0) {
			lg_1.commit_error_log("no entites found for entity index",
					new Exception("no entities found in es for this entity index"));
			throw new Exception("no entities found in es for this entity index");
		}

		lg_1.commit_info_log("init:build each entity");

		for (SearchHit hit : resp.getHits().getHits()) {
			// now parse the hit as an entity.
			String source_string = hit.getSourceAsString();
			@SuppressWarnings("static-access")
			Entity e = CentralSingleton.getInstance().objmapper.readValue(source_string, Entity.class);
			e.setEntity_es_id(hit.getId());
			e.after_build_entity_hook();
			ei.getEntity_map().put(e.getUnique_name(), e);
			ei.getEid_to_euname().put(e.getEntity_es_id(), e.getUnique_name());
		}

		LogObject lg_3 = new LogObject(ForexAndQuandlTests.class);
		lg_3.commit_info_log("completed:build each entity");

		return ei;

	}

	/****
	 * returns all the exceptions.
	 * 
	 * 
	 */
	public SearchResponse get_all_exceptions() {
		return RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes(StockConstants.ESTypes_error_log)
				.setQuery(QueryBuilders.matchAllQuery()).setFetchSource(true).execute().actionGet();
	}

	/****
	 * returns the number of info level exceptions.
	 * 
	 * @return
	 */
	public Integer count_info_exceptions() {
		SearchResponse resp = RemoteEs.getInstance().prepareSearch("tradegenie_titan")
				.setTypes(StockConstants.ESTypes_error_log)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termFilter(StockConstants.EsTypes_error_log_exception_type,

								StockConstants.EsTypes_error_log_exception_levels
										.get(StockConstants.EsTypes_error_log_exception_type_info)

		))).setFetchSource(true).setSize(10000)
				.addAggregation(AggregationBuilders.cardinality("unique_entity_exceptions").field("entity_unique_name"))
				.execute().actionGet();

		Cardinality agg = resp.getAggregations().get("unique_entity_exceptions");
		Long value = agg.getValue();
		return value.intValue();
	}

	/****
	 * returns the number of error exceptions.
	 * 
	 * @return
	 */
	public Integer count_error_exceptions() {
		return RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes(StockConstants.ESTypes_error_log)
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termFilter(StockConstants.EsTypes_error_log_exception_type,
								StockConstants.EsTypes_error_log_exception_levels
										.get(StockConstants.EsTypes_error_log_exception_type_error))))
				.setFetchSource(true).setSize(10000).execute().actionGet().getHits().getHits().length;
	}

	/****
	 * counts the total number of entities built.
	 * 
	 * 
	 */
	public Integer count_total_built_entities(SearchResponse sr) {
		return sr.getHits().getHits().length;
	}

	/****
	 * builds all the exchanges and returns the following:
	 * 
	 * @param dates
	 *            -> optional list of dates to pass in.
	 * 
	 * @param exchange_names
	 *            -> string array of exchange optional- these are used only in
	 *            the filter step, where we return the searchresponse, so
	 *            basically we build all the exchanges and then return only
	 *            those which are provided in the optional exchange_names
	 *            argument.
	 * 
	 * @return [] 0 -> Exchange Builder Package Coordinator, 1 -> Search
	 *         Response (contains all the entity definitions from es = the ones
	 *         that were built.), 2 -> dates (ArrayList<String>()), 3 ->
	 *         start_date(dates.get(0)), 4 -> end_date(dates.get(0))
	 * @throws Exception
	 */
	public ArrayList<Object> build_all_exchanges(ArrayList<String> dates, String[] exchange_names) throws Exception {

		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();

		dates = dates == null ? new ArrayList<String>(CentralSingleton.getInstance().gmt_datestring_to_day_id.keySet())
				: dates;

		final String start_date = dates.get(0);

		final String end_date = dates.get(dates.size() - 1);

		erp.build_all_exchanges();

		exchange_names = exchange_names == null
				? new String[] { StockConstants.name_for_nasdaq, StockConstants.name_for_dax,
						StockConstants.name_for_cac, StockConstants.name_for_ftse, StockConstants.name_for_nifty,
						StockConstants.name_for_oil, StockConstants.name_for_metals, StockConstants.name_for_forex }
				: exchange_names;

		SearchResponse resp = LocalEs.getInstance().prepareSearch("tradegenie_titan").setTypes("entity")
				.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),
						FilterBuilders.termsFilter("indice", exchange_names)))
				.addFields(new String[] { "symbol", "fullName", "dataProviderCode", "indice", "unique_name" })
				.setSize(500).execute().actionGet();

		return new ArrayList<Object>(Arrays.asList(new Object[] { erp, resp, dates, start_date, end_date }));

	}

	/***
	 * this function uses all the params returned from the
	 * built_entity_exchanges so we dont need to worry about optional params
	 * here, those are only specified once in the build_all_exchanges
	 * 
	 * @param sr
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> download_all_built_entities(ArrayList<Object> build_exchange_results)
			throws Exception {

		ExchangeBuilderPackageCoordinator erp = (ExchangeBuilderPackageCoordinator) build_exchange_results.get(0);
		SearchResponse resp = (SearchResponse) build_exchange_results.get(1);
		final ArrayList<String> dates = (ArrayList<String>) build_exchange_results.get(2);
		final String start_date = (String) build_exchange_results.get(3);
		final String end_date = (String) build_exchange_results.get(4);

		HashMap<String, String> failed_entities = new HashMap<String, String>();

		for (final SearchHit hit : resp.getHits().hits()) {

			Entity e = new Entity(hit.field("indice").getValue().toString(), 0);

			/****
			 * have to do this manually because we need to get the entity unique
			 * name in the exception logs.
			 * 
			 */

			e.setFullName(hit.field("fullName").getValue().toString());
			e.setsymbol(hit.field("symbol").getValue().toString());
			e.setUnique_name();
			e.setEntity_es_id(hit.getId());
			e.setPrice_map_internal(new LinkedList<Map<Integer, DataPoint>>());
			e.setLatest_existing_day_id(e.calculate_latest_existing_day_id());

			if (hit.field("indice").getValue().toString().equals(StockConstants.name_for_forex)) {

				ForexRequest frex = new ForexRequest(hit.field("symbol").getValue().toString(),
						hit.field("fullName").getValue().toString(), new LinkedHashMap<String, Object>() {
							{
								put(StockConstants.Entity_forex_request_dates, dates);
							}
						}, e);

				frex.run();
				// reassign the entity.
				e = frex.getE();

			} else {
				QuandlRequest qd = new QuandlRequest(hit.field("symbol").getValue().toString(),
						hit.field("fullName").getValue().toString(), new LinkedHashMap<String, Object>() {
							{
								put(StockConstants.dataProvider_start_date, start_date);
								put(StockConstants.dataProvider_end_date, end_date);
								put(StockConstants.dataProvider_dataset_code,
										hit.field("dataProviderCode").getValue().toString());
							}
						}, e);
				qd.run();
				e = qd.getE();
				// reassign the entity.
			}
			e.commit_entity_to_elasticsearch(e.getEntity_es_id(), 1);
			if (e.getPrice_map_internal().size() == 0) {
				failed_entities.put(e.getUnique_name(), "");
			}

		}

		return failed_entities;

	}

	/****
	 * turning off the wifi before downloading should not crash the whole
	 * program but it should produce as many info exceptions as there are
	 * entities. it should not produce any error exceptions.
	 */
	// @Test
	public void turning_off_local_wifi_should_produce_info_exceptions_only() {
		try {
			ArrayList<Object> exchanges_built = build_all_exchanges(null, null);
			ShScript.stop_wifi();
			HashMap<String, String> failed_entities = download_all_built_entities(exchanges_built);
			ShScript.start_wifi();
			Integer total_built_entities = count_total_built_entities((SearchResponse) exchanges_built.get(1));
			assertEquals("total info exceptions are equal to the number of built entities", true,
					total_built_entities.intValue() == (count_info_exceptions()));
			assertEquals("total error exceptions are zero", true, count_error_exceptions() == 0);
			assertEquals("logobject.geterrors should return false", false, LogObject.get_errors());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * we expect to see one info level exception and one datapoint for the valid
	 * date for all the forex entities.
	 * 
	 * invalid dates for forex are old dates like 1990 - 05 - 17
	 * 
	 * 
	 * valid date is the date of the complication like : 2010 - 05 - 17
	 * 
	 * future dates will return the latest available date. eg : 2022 - 05 - 17,
	 * 
	 * 
	 * expect one info level exception, expect one datapoint for each entity,
	 * since we get valid date only one, and the future date returns the latest
	 * avialble date which we have not specified as date for download and so
	 * there is expected to be only one datapoint
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	// @Test
	public void asking_for_one_non_existent_and_one_valid_forex_date_and_one_future_date() throws Exception {

		/***
		 * whenever we want to provide a custom list of exchange names, we have
		 * to initialize the central singleton first, because otherwise the
		 * stockconstants.name_for_index are not yet initialized.
		 * 
		 */
		CentralSingleton.initializeSingleton();

		ArrayList<String> dates = new ArrayList<String>(Arrays.asList("1990-05-17", "2010-05-17", "2022-05-17"));

		String[] exchange_names = new String[] { StockConstants.name_for_forex };

		ArrayList<Object> exchanges_built = build_all_exchanges(dates, exchange_names);

		HashMap<String, String> failed_entities = download_all_built_entities(exchanges_built);
		// count the info exceptions
		Integer total_built_entities = count_total_built_entities((SearchResponse) exchanges_built.get(1));
		assertEquals("total info exceptions are equal to the number of built entities", true,
				total_built_entities.intValue() == (count_info_exceptions()));
		assertEquals("total error exceptions are zero", true, count_error_exceptions() == 0);
		// check that for each entity that is downloaded there are two
		// datapoints.
		// get all datapoints for each entity.
		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {

			for (String exchange_name : exchange_names) {

				if (entry.getKey().equals(exchange_name)) {

					EntityIndex ei = build_entity_index_from_es(exchange_name, i);

					for (Map.Entry<String, Entity> ent : ei.getEntity_map().entrySet()) {
						// System.out.println("total datapoints are:" +
						// ent.getValue().getPrice_map_internal().size());
						assertEquals("there should be two datapoints for entity:" + ent.getValue().getFullName(), 1,
								ent.getValue().getPrice_map_internal().size());

					}

				}

			}

			i++;
		}

		assertEquals("logobject.geterrors should return false", false, LogObject.get_errors());

	}

	/***
	 * we ask for an entity whose name is not valid. HERE We use the name
	 * "doggi" we also ask for three other valid entities.
	 * 
	 * this should produce one info level exception. the valid entities should
	 * have datapoints.
	 * 
	 * So for this test we build an arbitary entity exchange say -> oil.
	 * 
	 * we then while doing download_built_entities, we pass in another fake
	 * entity. we pass this in by putting it into the options hashmap(last
	 * element of the objects arraylist that is returned by
	 * {@link #build_all_exchanges(ArrayList, String[])})
	 * 
	 * then we check that we get only an info exception and that the oil entity
	 * has datapoints.
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	// @Test
	public void asking_for_non_existent_entity_from_quandl() throws Exception {

		CentralSingleton.initializeSingleton();
		Entity dummy_entity = new Entity(StockConstants.name_for_oil, 5);
		dummy_entity.setsymbol("fake1234");
		dummy_entity.setFullName("fake1234_full");
		dummy_entity.setUnique_name();
		// we set its dataprovider code, so that it doesnt ask for the same
		// entity tiwce.
		dummy_entity.setDataProviderCode(dummy_entity.getUnique_name());
		/***
		 * there are 346 entities built each time, so we give this id 347
		 */
		dummy_entity.commit_entity_to_elasticsearch("E-347", 0);

		String[] exchange_names = new String[] { StockConstants.name_for_oil };

		ArrayList<Object> entities_built = build_all_exchanges(null, exchange_names);

		HashMap<String, String> failed_entities = download_all_built_entities(entities_built);

		assertEquals("total built entities are two", (Integer) 2,
				count_total_built_entities((SearchResponse) entities_built.get(1)));

		assertEquals("total info exceptions is equal to 1", (Integer) 1,
				(count_info_exceptions()));

		assertEquals("total error exceptions are zero", true, count_error_exceptions() == 0);

		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {

			for (String exchange_name : exchange_names) {

				if (entry.getKey().equals(exchange_name)) {

					EntityIndex ei = build_entity_index_from_es(exchange_name, i);

					for (Map.Entry<String, Entity> ent : ei.getEntity_map().entrySet()) {
						if (ent.getValue().getUnique_name().equals(dummy_entity.getUnique_name())) {
							assertEquals("fake entity has no dataPoints", true,
									ent.getValue().getPrice_map_internal().size() == 0);
						} else {
							assertEquals("oil entity should have dataPoints", true,
									ent.getValue().getPrice_map_internal().size() > 0);
						}
					}

				}

			}

			i++;
		}

		assertEquals("logobject.geterrors should return false", false, LogObject.get_errors());

	}

	/*****
	 * we set the ip addresses for quandl and forex as follows: 10.255.255.1
	 * this is non routable and should return a timeout.
	 * 
	 * @throws Exception
	 */
	@Test
	public void timeout_error_should_produce_info_level_error() throws Exception {

		CentralSingleton.initializeSingleton();

		StockConstants.forex_request_template = StockConstants.forex_request_template
				.replace(StockConstants.forex_root_url, StockConstants.timeout_simulate_url);
		StockConstants.quandl_request_template = StockConstants.quandl_request_template
				.replace(StockConstants.quandl_root_url, StockConstants.timeout_simulate_url);
		StockConstants.ok_http_tu = TimeUnit.MILLISECONDS;
		StockConstants.ok_http_read_timeout = 1;
		StockConstants.ok_http_connect_timeout = 1;

		ArrayList<Object> built_entities = build_all_exchanges(null, null);
		HashMap<String, String> failed_entities = download_all_built_entities(built_entities);
		// assert that the total info exceptions are equal to the total built
		// entities.
		Integer total_built_entities = count_total_built_entities((SearchResponse) built_entities.get(1));
		assertEquals("total info exceptions are equal to the number of built entities", true,
				total_built_entities.intValue() == (count_info_exceptions()));
		assertEquals("total error exceptions are zero", true, count_error_exceptions() == 0);
		assertEquals("logobject get errors should return false", false, LogObject.get_errors());
		// we want to check that all entities have no datapoints

		Integer i = 0;
		for (Map.Entry<String, String> entry : StockConstants.exchange_builder_constants.entrySet()) {

			EntityIndex ei = build_entity_index_from_es(entry.getKey(), i);

			for (Map.Entry<String, Entity> ent : ei.getEntity_map().entrySet()) {
				assertEquals("no datapoints should exist for entity:" + ent.getValue().getUnique_name(), (Integer) 0,
						(Integer) ent.getValue().getPrice_map_internal().size());
			}

			i++;
		}

	}

	/****
	 * there is no situation where an exception is thrown. so while downloading
	 * entities, there should only be info level logs, and there should be no
	 * logs with "issue" == "Exception while trying to download entity."
	 * 
	 * at the end print out grouped by index those entities which could not be
	 * downloaded and ensure that there the response code confirms to a url
	 * invalid.
	 * 
	 * @throws Exception
	 */
	// @Test
	public void downloading_all_entities_should_produce_only_info_level_exceptions() throws Exception {
		ArrayList<Object> entities_built = build_all_exchanges(null, null);
		HashMap<String, String> failed_entities = download_all_built_entities(entities_built);
		assertEquals("total error exceptions are zero", true, count_error_exceptions() == 0);
		assertEquals("logobject geterrors returns false", false, LogObject.get_errors());
	}

}
