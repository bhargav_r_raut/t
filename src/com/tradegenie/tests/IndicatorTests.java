package com.tradegenie.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.dataPointIndicator;
import Indicators.CalculateIndicators;
import Indicators.Indicator;
import elasticSearchNative.RemoteEs;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.trackers.AccelerationDecelerationIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class IndicatorTests extends DownloadEntityExchangeTests {

	// @Test
	public void test_save_indicator() throws Exception {
		RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
		CentralSingleton.initializeSingleton();
		Indicator i = new Indicator();
		i.setIndicator_name("hello");
		i.setIndicator_and_period_name("hello_there");
		i.setId("a");
		i.setPeriods(new ArrayList<Integer>(Arrays.asList(new Integer[] { 1, 3 })));
		assertTrue("indicator_ is saved", i.save());
	}

	// @Test
	public void test_load_indicator_from_es() throws Exception {
		RemoteEs.getInstance().admin().indices().prepareRefresh("tradegenie_titan").execute().actionGet();
		CentralSingleton.destroySingleton();
		CentralSingleton.initializeSingleton();
		Indicator i = new Indicator();
		i.setIndicator_name("hello");
		i.setIndicator_and_period_name("hello_there");
		i.setId("a");
		i.setPeriods(new ArrayList<Integer>(Arrays.asList(new Integer[] { 1, 3 })));
		assertTrue("indicator_ is saved", i.save());
		Indicator loaded = new Indicator();
		assertTrue("indicator loads frmo es", loaded.get_by_name("hello_there"));
	}

	// @Test
	public void test_creates_ticks_equal_to_datapoints() throws Exception {

		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			entity_index.getValue().before_calculate_entity_index_indicators();
		}

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entry : entity_index.getValue().getEntity_map().entrySet()) {

				TimeSeries ts = entry.getValue().getTs();

				// assert True that the size of the timesereis is
				// the
				// same as the price map internal.
				System.out.println("max tick count:" + ts.getTickCount());
				System.out.println("datapoint count:" + entry.getValue().getPrice_map_internal().size());
				assertTrue("ticks are equal to datapoints:",
						ts.getTickCount() == entry.getValue().getPrice_map_internal().size());

			}
		}

	}

	/***
	 * here we convert every single datapoint to have all its ohlcv as null we
	 * then calculate indicators and check that none of hte datapoints have any
	 * indicators on them. the issue was that aroon up, aroon down have a value
	 * of 0 for each and every day and RSI has a value of 0 for the first day
	 * and then a value of 100 for every successive day. please refer to notes
	 * written in
	 * {@link CalculateIndicators#generate_and_set_indicator_values(eu.verdelhan.ta4j.Indicator, String)}
	 * , where i explain that in our case, this problem will never occur. here i
	 * have blocked out these three names, and throw an exception in case any
	 * other indicator is found to be set
	 * 
	 * this test is subject to additional issues suppose i add more indicators
	 * and they turn out also to be generatin values for null situations, this
	 * test will fail. Point to be borne in mind.
	 * 
	 * @throws Exception
	 */
	// @Test
	public void test_adds_dpi_only_if_other_than_NaN() throws Exception {

		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);
		/// System.out.println("finished initial download");

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entry : entity_index.getValue().getEntity_map().entrySet()) {
				JSONObject indicators_jsonfile = CentralSingleton.getInstance().indicators_jsonfile;
				Iterator<String> keys_iterator = indicators_jsonfile.keys();
				Entity entity = entry.getValue();
				// modify the entity to make ohlcv on every datapoint as null.
				DataPoint null_dp = new DataPoint();
				null_dp.setClose(null);
				null_dp.setOpen(null);
				null_dp.setHigh(null);
				null_dp.setLow(null);
				null_dp.setVolume(null);
				null_dp.setIndicator_values_on_datapoint(new HashMap<String, dataPointIndicator>());
				for (Map.Entry<Integer, DataPoint> ent : entity.getPrice_map_internal().entrySet()) {
					ent.setValue(null_dp);
				}
			}
		}

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			entity_index.getValue().before_calculate_entity_index_indicators();
		}

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entry : entity_index.getValue().getEntity_map().entrySet()) {
				JSONObject indicators_jsonfile = CentralSingleton.getInstance().indicators_jsonfile;
				Iterator<String> keys_iterator = indicators_jsonfile.keys();
				Entity entity = entry.getValue();

				CalculateIndicators cind = new CalculateIndicators(entity);

				while (keys_iterator.hasNext()) {
					String indicator_name = keys_iterator.next();

					JSONObject indicator_object = indicators_jsonfile.getJSONObject(indicator_name);

					JSONArray ohlcv_groups_jsonarray = indicator_object
							.getJSONArray(StockConstants.indicator_ohlcv_groups_key_name);

					for (int i = 0; i < ohlcv_groups_jsonarray.length(); i++) {
						String indicator_ohlcv_group = ohlcv_groups_jsonarray.getString(i);

						if (entity.getOhlcv_type().equals(indicator_ohlcv_group)) {

							cind.setIndicator_name(indicator_name);
							cind.calculate_required_indicator();
							entry.setValue(cind.getE());

							// take the entity and ensure that not a single day
							// even has
							// a datapoint indicator.
							for (Map.Entry<Integer, DataPoint> dp_ent : entry.getValue().getPrice_map_internal()
									.entrySet()) {
								// System.out.println("the datapoint is null");
								// System.out.println(dp_ent.getValue() ==
								// null);
								// System.out.println("the indicator map is
								// null");
								// System.out.println(dp_ent.getValue().getIndicator_values_on_datapoint()
								// == null);
								// assertTrue("aroon up, aroon down, and RSI are
								// the only ones which have values",
								// dp_ent.getValue().getIndicator_values_on_datapoint().keySet().size()
								// == 3);
								// basically block out the correct names for
								// rsi, aroon up, and aroon down, and this test
								// will pass.

								Set<String> indicators_on_datapoint = dp_ent.getValue()
										.getIndicator_values_on_datapoint().keySet();
								for (String n : indicators_on_datapoint) {
									if (n.equals("aroon_up_period_start_25_period_end")) {
										// System.out.println("aroon expectedly
										// fails");
									} else if (n.equals("aroon_down_period_start_25_period_end")) {
										// System.out.println("aroon down
										// expectedly fails");
									} else if (n.equals("relative_strength_indicator_period_start_14_period_end")) {
										// System.out.println("rsi expectedly
										// fails");
									} else {

										throw new Exception(
												"test failed, there was an indicator other than the banned three on this datapoint by name:"
														+ n);

									}
								}
							}

						}
					}

				}

			}
		}
	}

	// test that all indicators with an applicable ohlcv group are
	// calculated for the entity.
	@Test
	public void calculates_indicator_only_if_sufficient_days() throws Exception {

		CentralSingleton.initializeSingleton();

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			entity_index.getValue().before_calculate_entity_index_indicators();
		}

		for (Map.Entry<String, EntityIndex> entity_index : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entry : entity_index.getValue().getEntity_map().entrySet()) {
				JSONObject indicators_jsonfile = CentralSingleton.getInstance().indicators_jsonfile;
				Iterator<String> keys_iterator = indicators_jsonfile.keys();
				Entity entity = entry.getValue();
				CalculateIndicators cind = new CalculateIndicators(entity);

				while (keys_iterator.hasNext()) {

					String indicator_name = keys_iterator.next();

					JSONObject indicator_object = indicators_jsonfile.getJSONObject(indicator_name);

					JSONArray ohlcv_groups_jsonarray = indicator_object
							.getJSONArray(StockConstants.indicator_ohlcv_groups_key_name);

					for (int i = 0; i < ohlcv_groups_jsonarray.length(); i++) {
						String indicator_ohlcv_group = ohlcv_groups_jsonarray.getString(i);

						if (entity.getOhlcv_type().equals(indicator_ohlcv_group)) {

							cind.setIndicator_name(indicator_name);
							cind.calculate_required_indicator();
							entry.setValue(cind.getE());

							List<Indicator> indicator_list = CentralSingleton.indicators_hashmap.get(indicator_name);

							for (Indicator ind : indicator_list) {

								Integer max_required_period = Collections.max(ind.getPeriods());
								// now the entity should have this indicator
								// on every
								// datapoint that has these many periods.
								// and not present on all preceeding
								// datapoints.

								Integer period = null;
								// System.out.println("frist day id:" +
								// entity.getPrice_map_internal().firstKey());
								// System.out.println("indicator name:" +
								// ind.getIndicator_name() + " indi + period
								// name:" +
								// ind.getIndicator_and_period_name());
								for (Map.Entry<Integer, DataPoint> idp : entity.getPrice_map_internal().entrySet()) {
									period = entity.period_for_indicator_calculation(idp.getKey());

									DataPoint dp = idp.getValue();
									HashMap<String, dataPointIndicator> dp_map = dp.getIndicator_values_on_datapoint();

									if (period < max_required_period) {
										assertTrue("the indicator is not set on dps before" + " dayid: " + idp.getKey(),
												!dp.getIndicator_values_on_datapoint()
														.containsKey(ind.getIndicator_and_period_name()));
									}
									if (period >= max_required_period) {
										// System.out.println("indicator
										// values on datapoint");
										// System.out.println(dp.getIndicator_values_on_datapoint().keySet());
										assertTrue("the indicator is set on dayid" + " dayid: " + idp.getKey(),
												dp.getIndicator_values_on_datapoint()
														.containsKey(ind.getIndicator_and_period_name()));
									}

								}

							}

						}
					}

				}
			}
		}

	}

}
