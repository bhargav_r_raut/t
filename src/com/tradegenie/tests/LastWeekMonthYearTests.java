package com.tradegenie.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.MetricsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.junit.Test;

import DataProvider.ForexRequest;
import DataProvider.QuandlRequest;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.LogObject;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.PairArray;
import eu.verdelhan.ta4j.indicators.simple.PreviousPriceIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class LastWeekMonthYearTests extends DownloadEntityExchangeTests {

	/***
	 * in this test, we enable make dummy request. as a result a price is given
	 * to every single day in the calendar. so last day month, week , year
	 * should be possible to predict and test.
	 * 
	 * @throws Exception
	 */
	
	public void single_step_calendar_based_test() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.make_dummy_request = true;
		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> exchange_entry : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> entity_entry : exchange_entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				String request_end_date = e.getRequest_end_date();
				Integer request_end_day_id = CentralSingleton.getInstance().gmt_datestring_to_day_id
						.get(request_end_date);
				Integer prev_week_id = null;
				Integer current_week_id = null;
				Integer prev_month_id = null;
				Integer current_month_id = null;
				Integer last_day_of_previous_week = null;
				Integer last_day_of_previous_month = null;

				// get the 3600 length array which stores for each
				// day the day id of the last day of the prev week
				int[] prev_week_arr = PairArray.get_decompressed_arr(
						e.getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_week, 1,
						CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

				// get the 3600 length array which stores for each
				// day the day id of the last day of the prev month
				int[] prev_month_arr = PairArray.get_decompressed_arr(
						e.getUnique_name() + "_" + StockConstants.Entity_last_day_of_prev_month, 1,
						CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size());

				for (Map.Entry<Integer, DataPoint> calendar_entry : CentralSingleton
						.getInstance().gmt_day_id_to_dataPoint.entrySet()) {

					Integer day_id = calendar_entry.getKey();
					if (day_id <= request_end_day_id) {
						// so lets first check that it correctly sets previous
						// week
						// and previous month
						// System.out.println("day id is:" + day_id);
						// System.out.println("current week id is:" +
						// current_week_id);
						// System.out.println("prev week id is:" +
						// prev_week_id);
						// System.out.println("current month id is:" +
						// current_month_id);
						// System.out.println("prev month id is:" +
						// prev_month_id);

						if (current_week_id != null && calendar_entry.getValue().getWeek_of_year() != current_week_id) {
							// first make the previous
							// System.out.println("current week id is not null
							// and the week id"
							// + " of the current day is not the " +
							// "same as the stored current_week_id");
							prev_week_id = current_week_id;
							// System.out.println("so prev week id becomes:" +
							// prev_week_id);
							last_day_of_previous_week = day_id - 1;
						}

						if (current_month_id != null
								&& calendar_entry.getValue().getMonth_of_year_integer() != current_month_id) {
							// first make the previous
							// System.out.println("current month id is not null
							// and the month id"
							// + " of the current day is not the " +
							// "same as the stored current_month_id");
							prev_month_id = current_month_id;
							// System.out.println("so prev week id becomes:" +
							// prev_month_id);
							last_day_of_previous_month = day_id - 1;
						}

						current_week_id = calendar_entry.getValue().getWeek_of_year();
						// System.out.println("finally at the end setting the
						// current week id as:" + current_week_id);
						current_month_id = calendar_entry.getValue().getMonth_of_year_integer();
						// System.out.println("finally at the end setting the
						// current month id as:" + current_week_id);

						// until the second new week, the last_day should read
						// -1
						// after that it should change regularly with the week.
						if (prev_week_id == null) {
							assertEquals("when there is no previous week," + " the last day of prev week should be -1",
									-1, prev_week_arr[day_id]);
						} else {
							// if there is a previous week id then at the
							// requisite
							// point in the array
							// it should be that prev week id.
							assertEquals(
									"when there is a previous week," + " the "
											+ "last day of the prev week should be as per calculation.",
									(int) last_day_of_previous_week, (int) prev_week_arr[day_id]);

						}

						if (prev_month_id == null) {
							assertEquals(
									"when there is no previous month," + " the last day of prev month should be -1", -1,
									prev_month_arr[day_id]);
						} else {
							assertEquals(
									"when there is a previous month," + " the "
											+ "last day of the prev month should be as per calculation.",
									(int) last_day_of_previous_month, (int) prev_month_arr[day_id]);
						}
					}

				}
			}
		}
	}

	

}
