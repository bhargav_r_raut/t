package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;

import org.elasticsearch.action.count.CountRequest;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.junit.Test;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.LogObject;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

public class LogObjectTest extends BaseTest {
	
	
	@Test
	public void should_not_detect_exception() {
		Boolean exceptions_found = LogObject.get_errors();
		assertEquals("should find no errors", false, exceptions_found);
	}
	
	@Test
	public void get_errors_should_return_true_if_es_is_down(){
		
	}
	
	
	@Test
	public void should_detect_exception() throws Exception {
		CentralSingleton.initializeSingleton();
		LogObject lge = new LogObject(getClass());
		lge.commit_error_log("dummy error", new Throwable("dummy error"));
		Boolean exceptions_found = LogObject.get_errors();
		assertEquals("should find errors", true, exceptions_found);
	}
	
	
	@Test
	public void should_find_info_exception_but_get_exceptions_should_return_false() throws Exception {
		CentralSingleton.initializeSingleton();
		LogObject lge = new LogObject(getClass());
		lge.add_to_info_packet(StockConstants.EsTypes_error_log_exception_type,
				StockConstants.EsTypes_error_log_exception_levels
						.get(StockConstants.EsTypes_error_log_exception_type_info));
		lge.commit_error_log("dummy error two", new Throwable("dummy error 2"));

		// check that there is something in the error log.
		CountResponse cr;
		CountRequest crr = new CountRequest("tradegenie_titan").types("error_log");
		cr = RemoteEs.getInstance().count(crr).actionGet();
		assertEquals("should find one document in the index", 1l, cr.getCount());

		//this document should have an exception type of info.
		SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("error_log")
				.setQuery(QueryBuilders.matchAllQuery()).setFetchSource(true).setSize(100).execute().actionGet();

		for (SearchHit hit : sr.getHits().getHits()) {
			assertEquals("info type exception should exist",
					hit.getSource().get(StockConstants.EsTypes_error_log_exception_type),
					StockConstants.EsTypes_error_log_exception_levels
							.get(StockConstants.EsTypes_error_log_exception_type_info));
		}

		// but whatever is there, does not have an error level(4).
		Boolean exceptions_found = LogObject.get_errors();
		assertEquals("should not find errors", false, exceptions_found);

	}

}
