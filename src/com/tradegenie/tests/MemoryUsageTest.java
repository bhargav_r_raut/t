package com.tradegenie.tests;

import java.util.HashMap;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.PairArray;
import yahoo_finance_historical_scraper.StockConstants;

public class MemoryUsageTest {

	//see by compressing just the sorted array and leaving uncompressed the unsorted array
	//what happens.
	//we have 325 entities.
	public static final Integer total_entities = 325;
	
	
	public static void main(String[] args){
		try {
			test_memory_requirement();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void test_memory_requirement() throws Exception{
		CentralSingleton.initializeSingleton();
		HashMap<String, int[]> holder_map = new HashMap<String,int[]>();
		for(int i = 0 ; i < total_entities; i++){
			String name = String.valueOf(i);
			holder_map.put(name, PairArray.get_random_test_array(CentralSingleton.gmt_day_id_to_dataPoint.size()*StockConstants.Entity_total_pairs_for_a_day,
					0, 0, 9999, -9999));
			System.out.println("after :" + name + " arrays heap is:" );
			PairArray.print_heap_stats();
			if(i == 324){
				System.out.println("sleeping");
				Thread.sleep(100000);
			}
		}
	}
	
}
