package com.tradegenie.tests;

import org.junit.Test;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.PercentageIntegerCorrelate;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

public class PercentageIntegerCorrelateTests {

	/****
	 * test passes.
	 * 
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@Test
	public void test_generation_of_open_and_close_stop_loss_arrays()
			throws JsonGenerationException, JsonMappingException, IOException {

		DataPoint current_dp = new DataPoint();

		current_dp.setClose("1");
		current_dp.setOpen("1");
		current_dp.setHigh("1");
		current_dp.setLow("1");

		DataPoint end_dp = new DataPoint();

		// we will check for close change of 3%
		end_dp.setClose("1.03");
		end_dp.setOpen("0.95");
		end_dp.setHigh("330.10");
		end_dp.setLow("1000.10");

		PercentageIntegerCorrelate pc = current_dp.calculate_differences_from_end_day_datapoint(end_dp, true);

		ObjectMapper mapper = new ObjectMapper();

		assertEquals(true, pc.getClose_close_applicalbe_stop_losses()[3]);
		for (int i = 0; i < pc.getClose_close_applicalbe_stop_losses().length; i++) {
			if (i != 3 && i != 4) {
				assertEquals(false, pc.getClose_close_applicalbe_stop_losses()[i]);
			}
		}

		assertEquals(true, pc.getOpen_open_applicable_stop_losses()[0]);
		assertEquals(true, pc.getOpen_open_applicable_stop_losses()[1]);
		assertEquals(true, pc.getOpen_open_applicable_stop_losses()[2]);

		for (int i = 0; i < pc.getOpen_open_applicable_stop_losses().length; i++) {
			if (i > 2) {
				assertEquals(false, pc.getOpen_open_applicable_stop_losses()[i]);
			}
		}
	}

	@Test
	public void test_price_change_applicable_to_tsl_amount() {

		PercentageIntegerCorrelate pc = new PercentageIntegerCorrelate(52, -32, null, null);

		// testing OPEN.

		assertEquals("this tsl is not applicable because sign is same", false,
				pc.trigger_tsl_move("open_open", 2));
		assertEquals("this tsl is applicable", true, pc.trigger_tsl_move("open_open", -2));
		assertEquals("this tsl is applicable", true, pc.trigger_tsl_move("open_open", -5));
		assertEquals("this tsl is not applicable", false, pc.trigger_tsl_move("open_open", -6));

		// CLOSE HAS A NEGATIVE PRICE CHANGE SO WE CHECK THAT NOW.
		assertEquals("this tsl is not applicable because sign is same", false,
				pc.trigger_tsl_move("close_close", -2));
		assertEquals("this tsl is applicable", true, pc.trigger_tsl_move("close_close", 2));
		assertEquals("this tsl is applicable", true, pc.trigger_tsl_move("close_close", 3));
		assertEquals("this tsl is not applicable", false, pc.trigger_tsl_move("close_close", 6));

	}

	@Test
	public void test_same_sign_as() {
		assertEquals("should be same sign", true, PercentageIntegerCorrelate.same_sign_as(-1, -1));
		assertEquals("should not be same sign", false, PercentageIntegerCorrelate.same_sign_as(1, -1));
		assertEquals("should not be same sign", false, PercentageIntegerCorrelate.same_sign_as(0, -1));
		assertEquals("should be same sign", true, PercentageIntegerCorrelate.same_sign_as(0, 0));
	}

	@Test
	public void test_other_pc_is_greater() {
		PercentageIntegerCorrelate pc1 = new PercentageIntegerCorrelate(20, 20, null, null);
		PercentageIntegerCorrelate pc2 = new PercentageIntegerCorrelate(30, 20, null, null);
		assertEquals("pc2 open should be greater than pc1", true, pc1.other_pc_is_greater("open_open", pc2));
		assertEquals("pc2 close should not be greater than pc1", false, pc1.other_pc_is_greater("close_close", pc2));
	}

	@Test
	public void test_vectore_compared_to_tsl_amount() throws Exception {
		PercentageIntegerCorrelate vector = new PercentageIntegerCorrelate(-30, 10, null, null);
		assertEquals("vector should trigger tsl, so returns -1", new Integer(-1),
				(Integer) vector.vector_compared_to_tsl_amount(-2, "open_open"));
		assertEquals("vector should not trigger tsl, so returns 1", new Integer(1),
				(Integer) vector.vector_compared_to_tsl_amount(-5, "open_open"));
	}

}
