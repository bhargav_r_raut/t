package com.tradegenie.tests;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.ExchangeBuilderPackageCoordinator;
import ExchangeBuilder.LogObject;
import ExchangeBuilder.TestMethod;
import ExchangeBuilder.TestMethodsExecutor;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;

/****
 * does not need the remote server to be online.
 * 
 * 
 * @author bhargav
 *
 */
public class RemoteConnectionToggleTests extends BaseTest {
	
	public static final String stop_local_es_before_build_entity_exchange_but_after_checking_exceptions = 
			"stop_local_es_before_build_entity_exchange_but_after_checking_exceptions";
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Override
	public void setUp() throws Exception {
		ShScript.start_all_servers();
		System.out.println("flushing indices");
		FlushIndicesAndRedis comode = new FlushIndicesAndRedis();
		comode.flush();
		System.out.println("finished flushing");
	}

	@Test
	public void should_detect_exception_if_remote_es_is_offline() {
		// knock of remote es.

		assertEquals("should stop remote es", true, ShScript.do_es(false, false));
		// the get exceptions call should return true
		Boolean exceptions_found = LogObject.get_errors();
		assertEquals("should find errors", true, exceptions_found);
		// now restart the remote es, so that we can go forward with the rest of
		// the tests.
		assertEquals("remote es should have restarted", true, ShScript.do_es(false, true));
	}

	@Test
	public void test_initialize_central_singleton_without_remote_redis_and_remote_es() throws InterruptedException {
		// knock of remote es.
		assertEquals("should stop remote es", true, ShScript.do_es(false, false));
		// knock off remote redis.
		ShScript.stop_remote_redis();
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		assertEquals("should show exception", true, LogObject.get_errors());
		// start them again.
		assertEquals("should start remote es again", true, ShScript.do_es(false, true));
		assertEquals("should start remote redis again", true, ShScript.start_remote_redis());
	}

	@Test
	public void build_entity_exchange_should_return_null() {
		// knock of remote es
		assertEquals("should stop remote es", true, ShScript.do_es(false, false));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();

		try {
			EntityIndex ei = erp.build_entityExchange(0, "");
			assertEquals("build entity exchange should return null in the absence of the remote servers", null, ei);
		} catch (Exception e) {
			e.printStackTrace();
			fail("exception thrown trying to build exchange");
		} finally {
			// start it again.
			assertEquals("should start remote es again", true, ShScript.do_es(false, true));
		}
	}

	@Test
	public void download_entity_exchange_should_return_null() {
		// knock of remote es
		assertEquals("should stop remote es", true, ShScript.do_es(false, false));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();
		try {
			EntityIndex ei = erp.download_entityExchange("", 0);
			assertEquals("download entity exchange should return null in the absence of the remote servers", null, ei);
		} catch (Exception e) {
			e.printStackTrace();
			fail("exception thrown trying to download exchange");
		} finally {
			// knock of remote es
			assertEquals("should start remote es again", true, ShScript.do_es(false, true));
		}
	}

	@Test
	public void shutdown_remote_server_before_build_entity_exchange() throws Exception {

		StockConstants.test_methods.put("stop_local_es_before_build_entity_exchange_but_after_checking_exceptions",
				new TestMethod(
						"stop_local_es_before_build_entity_exchange_but_after_checking_exceptions",
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.stop_local_es_server))));
		ExchangeBuilderPackageCoordinator erp = new ExchangeBuilderPackageCoordinator();

		LogObject lgd = new LogObject(RemoteConnectionToggleTests.class);
		if (StockConstants.exchange_builder_constants == null) {
			lgd.commit_debug_log("the exchange builder constants are null");
		} else {
			lgd.commit_debug_log("the constants are nuot null");
		}

		HashMap<String, Integer> exchange_name_and_counters_to_test = iterate_and_return_indices_to_test(
				new String[] { StockConstants.name_for_oil });

		for (Map.Entry<String, Integer> entry : exchange_name_and_counters_to_test.entrySet()) {
			exception.expect(NoNodeAvailableException.class);
			erp.build_entityExchange(entry.getValue(), entry.getKey());

		}
	}

}
