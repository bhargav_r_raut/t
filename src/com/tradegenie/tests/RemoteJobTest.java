/**
 * 
 */
package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import yahoo_finance_historical_scraper.StockConstants;
import ExchangeBuilder.EntityIndex;
import RemoteComms.Job;
import RemoteComms.JobStatusResponse;
import Titan.ReadWriteTextToFile;

/**
 * @author aditya
 * REMOTE SERVER MUST BE ONLINE FOR THESE TESTS
 * 
 */
public class RemoteJobTest extends BaseTest {
	private final ObjectMapper mapper = new ObjectMapper();

	/***
	 * sends an empty job to the api, and tests that authentication takes place.
	 * 
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */

	//@Test
	public void test_should_auth() throws Exception {
		Job j = new Job();
		j.setJob_id("auth");
		// set up an entity index and then send it , with no entities.
		String req = mapper.writeValueAsString(j);
		JobStatusResponse jsr = ReadWriteTextToFile
				.getInstance()
				.remote_job_request(
						req,
						StockConstants.EntityIndex_price_change_and_stop_loss_update_remote_url);
		assertEquals("response should contain job id", "auth", jsr.getJob_id());
		
		JobStatusResponse jsr2 = ReadWriteTextToFile.getInstance()
				.make_status_request("auth");
		
		assertEquals("job should be successfull","success",jsr2.getStatus_response());
		
		JobStatusResponse jsr3 = ReadWriteTextToFile.getInstance()
				.make_status_request("auth");
		
		assertEquals("job should be absent","absent",jsr3.getStatus_response());
		
	}
	
	
	//@Test
	public void test_auth_should_fail() throws Exception {

		Job j = new Job("rot");
		j.setJob_id("fail");
		String req = mapper.writeValueAsString(j);
		JobStatusResponse jsr = ReadWriteTextToFile
				.getInstance()
				.remote_job_request(
						req,
						StockConstants.EntityIndex_price_change_and_stop_loss_update_remote_url);
		assertEquals("response should be null", null, jsr);
	}


	/*****
	 * NEXT FIVE TESTS ARE ONE UNIT.
	 * 
	 * 
	 * 
	 * 
	 * @throws Exception
	 */
	//@Test
	public void test_setup_long_running_job() throws Exception {
		Job j = new Job();
		j.setJob_id("long_running");
		// set up an entity index and then send it , with no entities.
		String req = mapper.writeValueAsString(j);
		JobStatusResponse jsr = ReadWriteTextToFile
				.getInstance()
				.remote_job_request(
						req,
						StockConstants.EntityIndex_price_change_and_stop_loss_update_remote_url);
		assertEquals("response should contain job id of long running job",
				"long_running", jsr.getJob_id());
		
		
		JobStatusResponse jsr2 = ReadWriteTextToFile.getInstance()
				.make_status_request("long_running");
		assertEquals("job should be pending", "pending",
				jsr2.getStatus_response());
	
		JobStatusResponse jsr3 = ReadWriteTextToFile.getInstance()
				.make_cancel_job_request("long_running");
		assertEquals("cancel response should be true", true,
				jsr3.getCancel_response());
	
		
		JobStatusResponse jsr5 = ReadWriteTextToFile.getInstance()
				.make_status_request("long_running");
		assertEquals("job should be absent", "absent", jsr5.getStatus_response());
		
		
		JobStatusResponse jsr6 = ReadWriteTextToFile.getInstance()
				.make_status_request("long_running");
		assertEquals("semaphore count should equal 1", (Integer)1, jsr6.getSemaphore_count());
		assertEquals("pending jobs should equal 0",(Integer)0,jsr6.getPending_jobs());
		
	}
	
	


	
	
	/********
	 * NEXT TWO TESTS ARE ONE UNIT.
	 * 
	 * 
	 * @throws Exception
	 */
	
	
	//@Test
	public void cancel_all_jobs() throws Exception {
		Job j = new Job();
		j.setJob_id("long_running");
		// set up an entity index and then send it , with no entities.
		String req = mapper.writeValueAsString(j);
		JobStatusResponse jsr = ReadWriteTextToFile
				.getInstance()
				.remote_job_request(
						req,
						StockConstants.EntityIndex_price_change_and_stop_loss_update_remote_url);
		assertEquals("response should contain job id of long running job",
				"long_running", jsr.getJob_id());
	
		
		JobStatusResponse jsr2 = ReadWriteTextToFile.getInstance()
				.cancel_all_jobs();
		assertEquals("pending jobs should be zero", (Integer)0, jsr2.getPending_jobs());
		assertEquals("semaphore count should be one",(Integer)1,jsr2.getSemaphore_count());
	
	}
		
	@Test
	public void test_clear_central_singleton() throws Exception{
		Boolean resp = ReadWriteTextToFile.getInstance()
		.clear_remote_singleton_request(StockConstants.Clear_singleton_url);
		assertEquals("it clears the singleton",true,resp);
	}

}
