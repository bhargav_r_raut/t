package com.tradegenie.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.cluster.stats.ClusterStatsRequest;
import org.elasticsearch.action.admin.cluster.stats.ClusterStatsResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.json.JSONException;
import org.json.JSONObject;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import ExchangeBuilder.LogObject;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;

public class ShScript {
	private String command;

	public ShScript(String command) {
		this.command = command;
	}

	/****
	 * 
	 * @return true if wifi is online, false if offline
	 */
	public static Boolean wifi_status() {
		try {
			ReadWriteTextToFile.getInstance().getTextFromUrl("http://www.google.com");
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return false;
		}
	}

	/***
	 * 
	 * stops the wifi.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public static Boolean stop_wifi() {

		LogObject lg_d = new LogObject(ShScript.class);

		try {

			Boolean ret_val = do_local(StockConstants.local_wifi_stop_script, "");

			do {
				System.out.println("waiting 1 second for wifi to stop");
				Thread.sleep(1000);
			} while (wifi_status());

			lg_d.commit_debug_log("stop wifi returns: " + ret_val);
			return ret_val;

		} catch (Exception e) {
			lg_d.commit_error_log("stop wifi failed", e);
			return false;

		}

	}

	/***
	 * 
	 * start the wifi
	 * 
	 * @return
	 */
	public static Boolean start_wifi() {

		LogObject lg_d = new LogObject(ShScript.class);

		try {

			Boolean ret_val = do_local(StockConstants.local_wifi_start_script, "");

			do {
				System.out.println("waiting 1 second for wifi to start");
				Thread.sleep(1000);
			} while (!(wifi_status()));

			lg_d.commit_debug_log("start wifi returns: " + ret_val);
			return ret_val;

		} catch (Exception e) {
			lg_d.commit_error_log("start wifi failed", e);
			return false;

		}

	}

	public static void set_request_variables_to_simulate_timeout() {
		StockConstants.forex_request_template = StockConstants.forex_request_template
				.replace(StockConstants.forex_root_url, StockConstants.timeout_simulate_url);
		StockConstants.quandl_request_template = StockConstants.quandl_request_template
				.replace(StockConstants.quandl_root_url, StockConstants.timeout_simulate_url);
		StockConstants.ok_http_tu = TimeUnit.MILLISECONDS;
		StockConstants.ok_http_read_timeout = 1;
		StockConstants.ok_http_connect_timeout = 1;
	}

	public static void reset_request_timeout_variables() {
		StockConstants.forex_request_template = StockConstants.forex_request_template
				.replace(StockConstants.timeout_simulate_url, StockConstants.forex_root_url);
		StockConstants.quandl_request_template = StockConstants.quandl_request_template
				.replace(StockConstants.timeout_simulate_url, StockConstants.quandl_root_url);
		StockConstants.ok_http_tu = TimeUnit.SECONDS;
		StockConstants.ok_http_read_timeout = 5;
		StockConstants.ok_http_connect_timeout = 5;
	}

	public static Boolean create_index(Client client) {
		LogObject lgd1 = new LogObject(ShScript.class);
		lgd1.commit_debug_log("init: create index");
		try {
			IndicesExistsRequest ir = new IndicesExistsRequest("tradegenie_titan");
			IndicesExistsResponse resp = client.admin().indices().exists(ir).actionGet(3000);
			if (resp.isExists()) {
				LogObject lgd2 = new LogObject(ShScript.class);
				lgd2.commit_debug_log("index exists");
				return true;
			} else {
				CreateIndexRequest crq = new CreateIndexRequest("tradegenie_titan");
				@SuppressWarnings("static-access")
				String file = ReadWriteTextToFile.getInstance().readFile(StockConstants.es_mapping_json_file,
						TitanConstants.ENCODING);
				JSONObject index_definition = new JSONObject(file);
				JSONObject mappings = index_definition.getJSONObject("mappings");
				JSONObject settings = index_definition.getJSONObject("settings");
				Iterator<String> types_iterator = mappings.keys();
				while (types_iterator.hasNext()) {
					String type = types_iterator.next();
					JSONObject type_definition = mappings.getJSONObject(type);
					crq.mapping(type, type_definition.toString());
				}
				crq.settings(settings.toString());
				CreateIndexResponse cir = client.admin().indices().create(crq).actionGet();
				LogObject lgd2 = new LogObject(ShScript.class);
				lgd2.commit_debug_log("index create resp:" + String.valueOf(cir.isAcknowledged()));
				return cir.isAcknowledged();
			}
		} catch (Exception e) {
			LogObject lge = new LogObject(ShScript.class);
			lge.commit_error_log("failed to make index", e);
			return false;
		}
	}

	public static Boolean delete_index(Client client) {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.add_to_info_packet("status", "started index delete action for client:" 
		+ client.settings().get("cluster.name"));
		try {
			DeleteIndexResponse del_resp = client.admin().indices().delete(new DeleteIndexRequest("tradegenie_titan"))
					.actionGet();
			lgd.commit_debug_log("delete response:" + del_resp.isAcknowledged());
			return del_resp.isAcknowledged();
		} catch (Exception e) {
			lgd.commit_error_log("delete index failed", e);
			return false;
		}
	}

	public static Boolean stop_all_servers() {
		LogObject lgl = new LogObject(ShScript.class);
		lgl.commit_debug_log("stopping all servers");
		Boolean redis_stopped = stop_remote_redis();
		Boolean stop_local_es = do_es(true, false);
		Boolean stop_remote_es = do_es(false, false);
		return redis_stopped && stop_local_es && stop_remote_es;
	}

	public static Boolean start_all_servers() {

		LogObject lgl = new LogObject(ShScript.class);
		if (check_if_es_stopped(LocalEs.getInstance())) {
			if (do_es(true, true)) {

			} else {
				return false;
			}
		} else {
			lgl.commit_debug_log("LOCAL ES ALREADY RUNNING");
		}

		LogObject lgr = new LogObject(ShScript.class);
		// System.out.println("before check if es stopped");
		if (check_if_es_stopped(RemoteEs.getInstance())) {
			if (do_es(false, true)) {

			} else {
				// System.out.println("returning false from do_es");
				return false;
			}
		} else {
			lgr.commit_debug_log("REMOTE ES ALREADY RUNNING");
		}

		// starting redis.
		// if nothing has returned false till now, this is the last step of the
		// jump.
		return start_remote_redis();
	}

	public static void main(String[] args) throws IOException, JSONException, InterruptedException {
		start_all_servers();
	}

	public static Boolean start_remote_redis() {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.commit_debug_log("init: start remote redis");
		if (StockConstants.single_machine_tests) {
			return do_local(StockConstants.remote_redis_start_script, StockConstants.redis_path_remote);
		} else {
			return do_remote_ssh(StockConstants.remote_redis_start_script, new HashMap<String, Integer>() {
				{
					put(StockConstants.redis_process_running, 0);
				}
			});
		}

	}

	public static Boolean stop_remote_redis() {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.commit_debug_log("init: stop remote redis");
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String s = jedis.shutdown();
			if (s == null) {
				return true;
			} else {
				return false;
			}
		}

		/***
		 * if (StockConstants.single_machine_tests) { Boolean stop_redis =
		 * do_local(StockConstants.remote_redis_stop_script, "");
		 * System.out.println("stop redis is:" + stop_redis); try {
		 * Thread.sleep(1000); Boolean check_if_redis_stopped =
		 * check_if_redis_stopped(); return stop_redis &&
		 * check_if_redis_stopped; } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); return false; }
		 * 
		 * } else { try { return
		 * do_remote_ssh(StockConstants.remote_redis_stop_script, null) &&
		 * check_if_redis_stopped(); } catch (Exception e) { return false; } }
		 **/
	}

	public static Boolean check_if_redis_stopped() throws InterruptedException {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.add_to_info_packet("status", "stopping redis");
		Thread.sleep(1000);
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			System.out.println("jedis info:" + jedis.info());
			lgd.commit_error_log("redis still active", new Exception("redis failed to shutdown"));
			return false;
		} catch (Exception e) {
			lgd.commit_debug_log("redis successfully shutdown");
			return true;
		}
	}

	public static Boolean check_if_redis_started() {
		LogObject lgd = new LogObject(ShScript.class);

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			lgd.commit_debug_log("redis started successfully");
			return true;
		} catch (Exception e) {
			lgd.commit_error_log("redis failed to start", new Exception(e));
			return false;
		}
	}

	/***
	 * 
	 * 
	 * @param local
	 *            : true : local, false:remote
	 * @param start_or_sto
	 *            : true : start, false:stop
	 * @return
	 * @throws InterruptedException
	 */
	public static Boolean do_es(Boolean local, Boolean start_or_stop) {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.add_to_info_packet("do es:", local ? "local" : "remote");
		lgd.add_to_info_packet("do:", start_or_stop ? "start" : "stop");
		lgd.commit_debug_log("init");
		if (local) {
			if (start_or_stop) {
				Boolean start_es = do_local(StockConstants.local_es_start_script, StockConstants.es_path_local);

				return check_if_es_started_and_make_index_if_needed(LocalEs.getInstance()) && start_es;
			} else {
				Boolean stop_es = do_local(StockConstants.local_es_stop_script, "");
				return stop_es && check_if_es_stopped(LocalEs.getInstance()) && stop_es;
			}
		} else {
			if (StockConstants.single_machine_tests) {
				if (start_or_stop) {
					Boolean start_es = do_local(StockConstants.remote_es_start_script, StockConstants.es_path_remote);

					return check_if_es_started_and_make_index_if_needed(RemoteEs.getInstance()) && start_es;
				} else {
					Boolean stop_es = do_local(StockConstants.remote_es_stop_script, "");
					return stop_es && check_if_es_stopped(RemoteEs.getInstance()) && stop_es;
				}
			} else {
				if (start_or_stop) {

					Boolean start_es = do_remote_ssh(StockConstants.remote_es_start_script, null);
					// System.out.println("starting individual check");
					Boolean index_exists_and_es_started = check_if_es_started_and_make_index_if_needed(
							RemoteEs.getInstance());
					// System.out.println("finishing individual check");
					if (start_es) {
						// System.out.println("came past start es");
						if (index_exists_and_es_started) {
							// System.out.println("came past index exists
							// check");
							return true;
						}
					}
					// System.out.println("its false");
					return false;
				} else {
					Boolean stop_es = do_remote_ssh(StockConstants.remote_es_stop_script, null);
					return stop_es && check_if_es_stopped(RemoteEs.getInstance());
				}
			}
		}

	}

	/***
	 * 
	 * @param local_or_remote
	 *            : local: true, remote;false
	 * @return
	 * @throws InterruptedException
	 */
	public static Boolean check_if_es_started_and_make_index_if_needed(Client client) {
		// System.out.println("entered check if es started");
		String client_name = client.settings().get("cluster.name");
		LogObject lgd = new LogObject(ShScript.class);
		lgd.commit_debug_log("init: check if es started" + client_name + ": "
				+ "sleeping for 5 seconds to allow server to start");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Integer wait_counter = 0;
		Boolean es_started = false;
		while (true) {
			try {
				ClusterHealthResponse resp = client.admin().cluster().health(new ClusterHealthRequest()).actionGet();
				LogObject lg_d1 = new LogObject(ShScript.class);
				lg_d1.add_to_info_packet("client name:", client_name);
				lg_d1.add_to_info_packet("unassigned shards", resp.getUnassignedShards());
				lg_d1.add_to_info_packet("initializing shards", resp.getInitializingShards());
				lg_d1.commit_debug_log("cluster health");
				if (resp.getInitializingShards() > 0) {
					LogObject lg_d = new LogObject(ShScript.class);
					lg_d.commit_debug_log("sleeping");
					Thread.sleep(10000);
				} else {
					LogObject lg_d2 = new LogObject(ShScript.class);
					lg_d2.commit_debug_log("es started");
					es_started = true;
					break;
				}

				if (wait_counter > 10) {
					LogObject lge = new LogObject(ShScript.class);
					lge.commit_error_log("es_start_exception", new Throwable("exceeded wait"));
					break;
				}
				wait_counter++;
			} catch (Exception e) {
				LogObject lge1 = new LogObject(ShScript.class);
				lge1.commit_error_log("es start exception", e);
				break;
			}
		}

		return es_started && create_index(client);
	}

	public static Boolean delete_index(Boolean local_or_remote) {
		return null;
	}

	public static Boolean check_if_es_stopped(Client client) {
		LogObject lgd = new LogObject(ShScript.class);
		lgd.commit_debug_log("init: check if es stopped:sleeping for 5 seconds.");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ClusterHealthResponse resp = client.admin().cluster().health(new ClusterHealthRequest()).actionGet();
			LogObject lgd1 = new LogObject(ShScript.class);
			lgd1.commit_debug_log("es stopped: failed");
			return false;
		} catch (NoNodeAvailableException ne) {
			LogObject lgd1 = new LogObject(ShScript.class);
			lgd1.commit_debug_log("es stopped:success");
			return true;
		} catch (Exception e) {
			LogObject lgd1 = new LogObject(ShScript.class);
			lgd1.commit_error_log("error while checking if es stopped", e);
			return false;
		}

	}

	public static Boolean do_local(String script, String arg) {

		LogObject lgd = new LogObject(ShScript.class);
		lgd.add_to_info_packet("script", script);
		lgd.commit_debug_log("init: es do_local");

		try {
			/***
			 * the script will contain the arg in case of single machine tests.
			 * because the script will have the argument already appended as if
			 * it has been done for the case where we have an actual remote
			 * server.
			 * 
			 */
			LogObject lgdd = new LogObject(ShScript.class);

			if (arg != "" && script.contains(" " + arg)) {

				lgdd.add_to_info_packet("script before", script);
				int spacePos = script.indexOf(" ");
				script = script.substring(0, spacePos);
				lgdd.add_to_info_packet("script after", script);

			}

			String[] cmd = { "bash", script, arg };
			lgdd.add_to_info_packet("command", cmd[0] + " " + cmd[1] + " " + cmd[2]);
			lgdd.commit_debug_log("debug");

			Process p = Runtime.getRuntime().exec(cmd);
			// System.out.println(output(p.getInputStream()));
			// System.out.println(output(p.getErrorStream()));
			p.waitFor();

			return true;
		} catch (Exception e) {
			LogObject lge = new LogObject(ShScript.class);
			lge.commit_error_log("do local:exception", e);
			return false;
		}
	}

	private static String output(InputStream inputStream) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + System.getProperty("line.separator"));
			}
		} finally {
			br.close();
		}
		return sb.toString();
	}

	public static Boolean do_remote_ssh(String script, HashMap<String, Integer> options) {
		LogObject lgd1 = new LogObject(ShScript.class);
		LogObject lgd = new LogObject(ShScript.class);
		Integer response_code = null;
		Integer exitStatus = null;
		if (options != null) {
			// set it to the error so that it fails by default.
			response_code = 1;
		}
		Session session = null;
		ChannelExec channelExec = null;

		lgd1.commit_debug_log("init: sh_script");
		JSch jsch = new JSch();
		String user = StockConstants.remote_user;
		String host = StockConstants.elasticsearch_remote_host_name;
		int port = StockConstants.remote_ssh_port;
		String privateKey = StockConstants.private_key_file;
		String known_hosts = StockConstants.known_hosts_file;
		try {
			jsch.addIdentity(privateKey);
			jsch.setKnownHosts(known_hosts);

			session = jsch.getSession(user, host, port);

			LogObject lgd2 = new LogObject(ShScript.class);
			lgd2.commit_debug_log("session created.");
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setTimeout(StockConstants.ssh_timeout);
			session.connect();

			// create the excution channel over the session
			channelExec = (ChannelExec) session.openChannel("exec");

			// Gets an InputStream for this channel. All data arriving in as
			// messages from the remote side can be read from this stream.
			InputStream in = channelExec.getInputStream();

			// Set the command that you want to execute
			// In our case its the remote shell script
			channelExec.setCommand("bash " + script);

			// Execute the command
			channelExec.connect();

			// Read the output from the input stream we set above
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;

			// Read each line from the buffered reader and add it to result list
			// You can also simple print the result here

			Integer line_counter = 0;
			LogObject lgd3 = new LogObject(ShScript.class);

			while ((line = reader.readLine()) != null) {
				lgd3.add_to_info_packet("line:" + line_counter, line);
				if (options != null) {
					if (options.containsKey(line)) {
						response_code = options.get(line);
					}
				}
				line_counter++;
			}

			lgd3.commit_debug_log("lines");

			// retrieve the exit status of the remote command corresponding to
			// this channel
			exitStatus = channelExec.getExitStatus();

			// Safely disconnect channel and disconnect session. If not done
			// then it may cause resource leak
			channelExec.disconnect();
			session.disconnect();
		} catch (JSchException | IOException e) {
			LogObject lge = new LogObject(ShScript.class);
			lge.commit_error_log("error doing remote script", e);
			if (session != null) {
				session.disconnect();

			}
			if (channelExec != null) {
				channelExec.disconnect();
			}

			// System.out.println("returning false");

			return false;
		}

		if (exitStatus == null) {

			lgd.commit_error_log("err: sh_script : " + exitStatus, new Exception("no exit code"));
			return false;
		} else if (exitStatus > 0) {

			lgd.commit_error_log("err: sh_script" + exitStatus, new Exception("error exit code"));
			return false;
		} else {
			if (response_code != null && response_code == 1) {
				lgd.commit_error_log("remote script error.", new Exception("remote script failed."));
				return false;
			}
			lgd.commit_debug_log("end: sh_script - complete :  with response code:" + response_code);
			return true;
		}

	}

}
