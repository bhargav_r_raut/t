package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.junit.Test;

import ExchangeBuilder.LogObject;
import Indicators.RedisManager;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import redis.clients.jedis.Jedis;

public class ShScriptTest extends BaseTest{
	
	
	@Test
	public void test_stop_all_servers_then_restart() throws InterruptedException {
		ShScript sh_script = new ShScript(null);
		Boolean stop_all_servers = sh_script.stop_all_servers();
		Thread.sleep(5000);
		
		Boolean start_all_servers = sh_script.start_all_servers();
		Thread.sleep(5000);
		Boolean stop_again = sh_script.stop_all_servers();
		Thread.sleep(5000);
		Boolean start_again = sh_script.start_all_servers();
		LogObject lgd = new LogObject(ShScriptTest.class);
		lgd.add_to_info_packet("first stop", stop_all_servers);
		lgd.add_to_info_packet("first restart", start_all_servers);
		lgd.add_to_info_packet("second stop", stop_again);
		lgd.add_to_info_packet("second restart", start_again);
		lgd.commit_debug_log("report first test");
		assertEquals("All servers should stop and restart stop and restart", 
				true,
				stop_all_servers && start_all_servers && stop_again && start_again);
		
		
	}
	
	
	@Test
	public void test_local_es() {
		// get cluster name
		IndicesExistsResponse ir = LocalEs.getInstance().admin().indices().prepareExists("tradegenie_titan").execute()
				.actionGet();
		assertEquals("index should exist locally",true,ir.isExists());

	}

	@Test
	public void test_remote_es() {
		// get cluster name
		IndicesExistsResponse ir = RemoteEs.getInstance().admin().indices().prepareExists("tradegenie_titan").execute()
				.actionGet();
		assertEquals("index should exist remote",true,ir.isExists());
	}

	@Test
	public void test_remote_redis() {
		String info = null;
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			info = jedis.info();
		}
		catch(Exception e){
			
		}
		assertNotNull("redis should return info string", info);
	}
	
}
