package com.tradegenie.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.junit.Test;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.TestMethod;
import ExchangeBuilder.TestMethodsExecutor;
import elasticSearchNative.RemoteEs;
import yahoo_finance_historical_scraper.StockConstants;

/***
 * a positive stop loss means a short trade has hit a stop loss a negative stop
 * loss means a long trade has hit a stop loss
 ***/
public class StopLossTests extends DownloadEntityExchangeTests {

	/**********
	 * how stop loss works and what tests need to be done:
	 * 
	 * PRE TEST: 1. test generate_index_to_day_id_mapping function.
	 * 
	 * BASIC FUNCTION TESTS.
	 * 
	 * 
	 * 1.test decompression of unsorted array 2.test generate_price_map_in_range
	 * function generates only those days which are numerically n days minus the
	 * end day id. 3.test get_int_array_index_given_start_day_and_end_day_id for
	 * numerically being within the 300 day boundary. 4.PriceChangeInteger
	 * correlate test conversion of price changes to integers 5.stop loss
	 * calculations inside pricechangeinteger correlate 6.test getting hashmaps
	 * of all the stop_loss arrays, trailing and otherwise. 7.test
	 * modify_sorted_array function -> to set the stat stop losses.
	 * 7a.conversion of index in large array -> small array 7b.test getting
	 * applicable trailings stop loss amounts for a bsl 7c.check that after
	 * modify function, then combined_stop_loss_hashmap is returned modified.
	 * 8.test getting single day changes for stop loss(trailing stop loss tests)
	 * 9.actual real data tests, for stop loss.
	 * 
	 */
	public static final String test_generate_price_map_internal_and_pair_array_index_calculation = "test_generate_price_map_internal_and_pair_array_index_calculation";
	public static final String test_should_skip_part_after_update_stop_loss = "test_should_skip_part_after_update_stop_loss";

	public static final String test_calculate_stat_stop_loss = "test_calculate_stat_stop_loss";
	public static final String test_calculate_basic_trailing_stop_loss = "test_calculate_trailing_stop_loss";
	public static final String test_calculate_highest_price_trailing_stop_loss = "test_calculate_highest_price_trailing_stop_loss";
	public static final String test_calculate_multiple_sub_tsl_falls_trigger_tsl = "test_calculate_multiple_sub_tsl_falls_trigger_tsl";

	public static final String test_calculated_tsl_as_bsl = "test_calculate_bsl_as_tsl";
	public static final String test_calculate_tsl_threshold = "test_calculate_tsl_threshold";
	public static final String test_tsl_first_run = "test_calculate_tsl_first_run";
	public static final String test_tsl_second_run = "test_calcualte_tsl_second_run";
	/****
	 * 
	 * stat stop loss multiple runs
	 * 
	 */
	public static final String test_calculate_stat_stop_loss_first_run = "test_calculate_stat_stop_loss_first_run";
	public static final String normalize_price_map_internal_before_stat_stop_loss_second_run = "normalize_price_map_internal_before_stat_stop_loss_second_run";
	
	/***
	 * WHAT IS BEING DONE HERE?
	 * 
	 * 
	 * 
	 * @throws Exception
	 */
	public void test_generate_price_map_in_range() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.test_methods.put(test_generate_price_map_internal_and_pair_array_index_calculation,
				new TestMethod(test_generate_price_map_internal_and_pair_array_index_calculation, new ArrayList<String>(
						Arrays.asList(TestMethodsExecutor.store_start_and_end_day_ids_of_price_map_internal))));
		StockConstants.test_methods.put(test_should_skip_part_after_update_stop_loss,
				new TestMethod(test_should_skip_part_after_update_stop_loss,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.stop_after_update_stop_loss))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);
		// we pass in false for whether to skip both after_entity and
		// after_entity_index download hooks
		// but we do want to skip the after_entity_index_download hook
		// so we add that here followiing.
		for (Map.Entry<String, EntityIndex> ei_entry : initially_downloaded_exchanges.entrySet()) {
			for (Map.Entry<String, Entity> e_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				Entity e = e_entry.getValue();
				TestSingletonValue v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
				TreeMap<Integer, DataPoint> pm_internal = e.getPrice_map_internal();
				DataPoint start_dp = v.getPrice_map_in_range_start_day_datapoint();
				DataPoint end_dp = v.getPrice_map_in_range_end_day_datapoint();
				Integer start_day_id = v.getStart_day_id();
				Integer end_day_id = v.getEnd_day_id();

				/// now check the difference between the two.
				assertTrue("difference between start and end day " + "id is less than entity total pairs for a day",
						(end_day_id - start_day_id) < StockConstants.Entity_total_pairs_for_a_day);
				PairArray pa = v.getP();
				assertEquals("the index in the large arr should be the mathematical index",
						(Integer) ((start_day_id * (StockConstants.Entity_total_pairs_for_a_day - 1)) + end_day_id - 1),
						pa.getPair_difference_array_index());
				assertEquals("the index in small arr should be equal to the start day id", start_day_id,
						pa.getStart_day_id_based_index());
			}
		}

	}

	/****
	 * given a day id of 0, we want this to produce the index in the unsorted
	 * map, of the first pair for day id 1 this makes perfect sense, the array
	 * should still remain sorted . because. the pairs are built as such
	 * 
	 * @throws Exception
	 * 
	 */

	public void test_get_filler_id_for_index() throws Exception {
		CentralSingleton.initializeSingleton();
		for (int i = 0; i < CentralSingleton.getInstance().gmt_day_id_to_dataPoint.size(); i++) {
			Integer filler_id = PairArray.get_filler_id_for_sorted_map(i);
			Integer expected_filled_id = PairArray.get_int_array_index_given_start_day_id_and_difference(i + 1, 1)
					.getPair_difference_array_index();
			assertEquals(
					"the filler id should be equal to" + " the actual index for the first pair in the unsorted map",
					expected_filled_id, filler_id);
		}
	}

	public void runner() {
		System.out.println(PairArray.get_filler_id_for_sorted_map(2041));
	}

	/****
	 * @working for each of the stop loss amounts a certain day id has had its
	 *          close price modified. we need to ignore these day ids, while
	 *          checking if the stop losses were correctly set for any given day
	 *          id. while some of them may be applicable, we have ignored them
	 *          all while testing.
	 * 
	 *          so for example when checking the stop losses for -2, we ignore
	 *          the day ids for -3,-5,5,3,2
	 * 
	 * 
	 * @param simulated_stop_loss_day_ids
	 * @param current_stop_loss_amount
	 * @return
	 */
	public ArrayList<Integer> day_ids_to_block(TreeMap<Integer, Integer> simulated_stop_loss_day_ids,
			Integer current_stop_loss_amount) {

		ArrayList<Integer> i = new ArrayList<Integer>();

		for (Map.Entry<Integer, Integer> entry : simulated_stop_loss_day_ids.entrySet()) {

			if (!entry.getKey().equals(current_stop_loss_amount)) {
				i.add(entry.getValue());
			}

		}

		return i;
	}

	/*****
	 * @working : This function is only to be used for stat_stop_loss. it also
	 *          assumes that the order in which the stop losses were stimulated
	 *          is: [5,3,2,-2,-3,-5] these stop loss amounts are HARD-CODED in
	 *          this function. -------- details: given a stop loss amount, and a
	 *          day id where the stop loss was simulated this function will
	 *          check if the generated large array index stored in the stat stop
	 *          loss array, is the same as what can be calculated. it will
	 *          assert this.
	 * 
	 * @param day_id_where_this_stop_loss_was_simulated
	 * @param e
	 * @param stop_loss_amount
	 * @param stat_stop_loss_array
	 * @return : in the end it returs a hashmap of the following structure: key:
	 *         start_day_id to which the stop loss amount is applicable value:
	 *         the index in the large array, where the stop loss is triggered.
	 *         the value is the same for all the keys obviously.
	 */
	public HashMap<Integer, Integer> get_stat_stop_loss_applicable_day_ids(
			Integer day_id_where_this_stop_loss_was_simulated, Entity e, Integer stop_loss_amount,
			int[] stat_stop_loss_array) {
		// this is to be returned.
		HashMap<Integer, Integer> stat_stop_loss_applicable_day_ids_and_large_array_index = new HashMap<Integer, Integer>();

		SortedMap<Integer, DataPoint> all_dps_less_than_this_day_id = e.getPrice_map_internal().subMap(
				day_id_where_this_stop_loss_was_simulated - StockConstants.Entity_total_pairs_for_a_day,
				day_id_where_this_stop_loss_was_simulated);
		TestSingletonValue v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));

		String name_of_relevant_stat_stop_loss_array = PairArray
				.get_name_of_relevant_stat_stop_loss_map(e.getUnique_name(), "close_close", stop_loss_amount);
		// System.out.println("name of relevant stat stop loss is:"
		// + name_of_relevant_stat_stop_loss_array);

		for (Integer start_day_id : all_dps_less_than_this_day_id.keySet()) {

			/***
			 * these are the day ids, where the price was changed(for the other
			 * stop losses) All except the changed day id of the current stop
			 * loss change is included.
			 * 
			 * 
			 */
			ArrayList<Integer> day_ids = day_ids_to_block(v.getDay_ids_where_dummy_stat_stop_losses_were_simulated(),
					stop_loss_amount);

			/****
			 * as long as these above day ids dont have the start_day_id itself,
			 * in which case the price change will be faulty, so we ignore that
			 * case
			 * 
			 * || day_ids.contains(day_id_where_this_stop_loss_was_simulated)
			 */
			if ((!(day_ids.contains(start_day_id)))) {

				PairArray pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
						day_id_where_this_stop_loss_was_simulated, e);
				/***
				 * if the stop_loss_amount
				 * 
				 */
				if (stop_loss_amount > 0) {
					if (start_day_id < v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(5)) {
						pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
								v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(5), e);
					} else if (start_day_id < v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(3)) {
						pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
								v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(3), e);
					}
				}

				System.out.println("stat stop loss array is:");
				System.out.println(stat_stop_loss_array.length);
				System.out.println("start day id is:");
				System.out.println(start_day_id);
				HashMap<String, Integer> actual_start_day_end_day_id = PairArray
						.pair_differences_index_start_and_end_day_id(stat_stop_loss_array[start_day_id]);

				stat_stop_loss_applicable_day_ids_and_large_array_index.put(start_day_id,
						pa.getPair_difference_array_index().intValue());

				assertEquals(
						"start day id: " + start_day_id + " end day id:" + day_id_where_this_stop_loss_was_simulated
								+ " should be corret pair"
								+ "derived start and end day id based on the index in the stat_stop_loss_Array:"
								+ actual_start_day_end_day_id.get("start_day_id") + " end day id:"
								+ actual_start_day_end_day_id.get("end_day_id"),
						pa.getPair_difference_array_index().intValue(), stat_stop_loss_array[start_day_id]);
			}
		}

		return stat_stop_loss_applicable_day_ids_and_large_array_index;

	}

	/***
	 * PASSES PURPOSE: to test that stat stop losses are correctly set.
	 * EXPLANATION OF TEST: please see comments above each line.
	 * 
	 * @throws Exception
	 */
	//@Test
	public void test_sets_stat_stop_loss_correctly() throws Exception {

		CentralSingleton.initializeSingleton();

		/***
		 * this method modifies the price map internal. it is called in
		 * Entity#update_stop_loss_open_close_arrays() it calls the method
		 * TestMethodsExecutor#modify_price_map_internal_for_stat_stop_loss_test
		 * read about what that does in the method itself.
		 */
		StockConstants.test_methods.put(test_calculate_stat_stop_loss, new TestMethod(test_calculate_stat_stop_loss,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_stat_stop_loss_calculation))));

		/***
		 * this method is called in Entity#after_download_entity_hook what it
		 * does is that it causes that method to exit before : 1)update last day
		 * of week month and year 2)update the price change and stop loss arrays
		 * to es. so basically after calculating the stop losses we are not
		 * allowing them to be uploaded to the remote server, but keeping them
		 * in the local CentralSingleton.
		 */
		StockConstants.test_methods.put(test_should_skip_part_after_update_stop_loss,
				new TestMethod(test_should_skip_part_after_update_stop_loss,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.stop_after_update_stop_loss))));
		/****
		 * this method does the following: here the following is set: 1) false
		 * -> build from es : means that we want to do a totally fresh download
		 * 2) false -> skip_after_entity_download_hook : means we dont want to
		 * skip the after entity download hook 3) true -> skip
		 * after_entity_index_download_hook : means we want to skip the after
		 * entity index download hook : this line is quite meaningless because
		 * the after_entity_download hoook currently does nothing .
		 */
		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> ei_entry : initially_downloaded_exchanges.entrySet()) {
			assertTrue("there are entities", ei_entry.getValue().getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> e_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				Entity e = e_entry.getValue();
				/****
				 * returns a hashmap of all the stat_stop_loss arrays from the
				 * centralsingleton.
				 * 
				 */
				HashMap<String, int[]> hm_of_all_stat_stop_losses = PairArray
						.get_hashmap_of_all_stat_stop_loss_arrays(e.getUnique_name());

				/***
				 * this assertion is frivolous
				 * 
				 */
				assertTrue("there are stat stop losess", hm_of_all_stat_stop_losses.size() > 0);

				TestSingletonValue v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));

				/****
				 * while doing the modification of the price map interanl, we
				 * have stored in the test singleton value, which day each stop
				 * loss was simulated at.
				 * 
				 */
				assertTrue("day ids when dummy stop losses were simulated exists",
						v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().size() > 0);

				/****
				 * now just iterating this above map.
				 * 
				 */
				for (Map.Entry<Integer, Integer> stop_loss_day_ids : v
						.getDay_ids_where_dummy_stat_stop_losses_were_simulated().entrySet()) {

					Integer day_id_where_this_stop_loss_was_simulated = stop_loss_day_ids.getValue();
					Integer stop_loss_amount = stop_loss_day_ids.getKey();

					/****
					 * same as generate_price_map_in_range function from
					 * Entity#generate_price_map_in_range
					 * 
					 * basically we just want all the day_ids for which a pair
					 * can be made with this(day id on which stop loss is
					 * simulated)
					 */
					SortedMap<Integer, DataPoint> all_dps_less_than_this_day_id = e.getPrice_map_internal()
							.subMap(day_id_where_this_stop_loss_was_simulated
									- StockConstants.Entity_total_pairs_for_a_day,
							day_id_where_this_stop_loss_was_simulated);

					/***
					 * get the name of the the stop loss array -> based on which
					 * stop loss we are dealing with.
					 */
					String name_of_relevant_stat_stop_loss_array = PairArray.get_name_of_relevant_stat_stop_loss_map(
							e.getUnique_name(), "close_close", stop_loss_amount);

					/***
					 * 
					 * take that respective stop loss array.
					 */
					int[] stat_stop_loss_array = hm_of_all_stat_stop_losses.get(name_of_relevant_stat_stop_loss_array);

					for (Integer start_day_id : all_dps_less_than_this_day_id.keySet()) {

						/***
						 * e.g:
						 * 
						 * day id: 435 : price 105 day id: 440 : price 103 day
						 * id: 445 : price 102
						 * 
						 * assuming that we are testing for day id 445, all day
						 * ids, other than 435, and 440 will have the pair of
						 * themselves -> 445 as the stop loss. however for the
						 * other two day ids, the price change will not be
						 * triggering a stop loss. eg: 445 - 435 is not going to
						 * trigger a stop loss of 2% because the price change is
						 * not 2%. so we block out all the day ids of other stop
						 * losses, while doing the assertions.
						 * 
						 * this function basically blocks the day_ids where the
						 * stop loss was simulated for each stop loss other than
						 * the present stop loss so it will even block out day
						 * ids, in the front, ie in case of the above example,
						 * the negative stop losses are set after the positive
						 * ones, and so the blocked day ids will have those day
						 * ids also. however that wont make any difference
						 * because we only check the day ids of all pairs before
						 * this day id.
						 */
						ArrayList<Integer> day_ids = day_ids_to_block(
								v.getDay_ids_where_dummy_stat_stop_losses_were_simulated(), stop_loss_amount);

						/****
						 * as long as these above day ids dont have the
						 * start_day_id itself, in which case the price change
						 * will be faulty, so we ignore that case
						 * 
						 * day id: 435 : price 105 day id: 440 : price 103 day
						 * id: 445 : price 102
						 * 
						 * for any day earlier than 435, a 2%, or 3% stop loss
						 * is going to be triggered at 435, in addition to a 5%
						 * stop loss also being triggered there.
						 * 
						 * so for all the day ids before 435, the pair ending
						 * with 435, will also be applicable for 3% and 2% stop
						 * loss trigger.
						 * 
						 * that is why we first see if the day id is less than
						 * the day where 5% stop loss was triggered. if yes,
						 * then we take the pair of start_day_id - day_id where
						 * 5% was triggered as the applicable stop loss index
						 * 
						 * next, we consider day ids including and after the day
						 * id where 5% was triggered we ignore the day the 5%
						 * stop loss was triggered(it will be blocked, in the
						 * above function) now for any day after uptil where the
						 * 3% stop loss is triggered, even the 2% stop loss will
						 * be triggered at the same day where the 3% stop loss
						 * is triggered. so that too is handled similarly.
						 * 
						 * 
						 * we basically want to generate a PairArray instance
						 * given the start and end day ids. here the start day
						 * id is being iterated from above(its just a normal day
						 * id) and the end day id ,is where it should have
						 * detected that the stop loss is hit. so now we have
						 * this pair_array instance. from that we can get the
						 * actual index in the large array.
						 * 
						 * and we now compare it with what has been stored in
						 * the stat stop loss array at the index of the
						 * start_day_id.
						 * 
						 */
						if ((!(day_ids.contains(start_day_id)))) {

							PairArray pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
									day_id_where_this_stop_loss_was_simulated, e);
							/***
							 * this deals with overlap conditions. consider the
							 * same example set above
							 * 
							 * 
							 */
							if (stop_loss_amount > 0) {
								if (start_day_id < v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(5)) {
									pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
											v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(5), e);
								} else if (start_day_id < v.getDay_ids_where_dummy_stat_stop_losses_were_simulated()
										.get(3)) {
									pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
											v.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get(3), e);
								}
							}
							HashMap<String, Integer> actual_start_day_end_day_id = PairArray
									.pair_differences_index_start_and_end_day_id(stat_stop_loss_array[start_day_id]);

							assertEquals(
									"start day id: " + start_day_id + " end day id:"
											+ day_id_where_this_stop_loss_was_simulated + " should be corret pair"
											+ "derived start and end day id based on the index in the stat_stop_loss_Array:"
											+ actual_start_day_end_day_id.get("start_day_id") + " end day id:"
											+ actual_start_day_end_day_id.get("end_day_id"),
									pa.getPair_difference_array_index().intValue(), stat_stop_loss_array[start_day_id]);
						}
					}

				}
			}
		}

	}

	/****
	 * function to get from the remote es, a sorted array, given an array name.
	 * 
	 * will return the decompressed array that is required.
	 * 
	 * 
	 * @param array_name
	 * @return
	 */
	public int[] get_sorted_array_from_remote_es(String array_name) {
		SearchResponse sr = RemoteEs.getInstance().prepareSearch("tradegenie_titan").setTypes("arrays")
				.setFetchSource(true).setSize(1).setQuery(QueryBuilders.termQuery("arr_name", array_name)).execute()
				.actionGet();
		int[] decompressed_arr = null;
		for (SearchHit hit : sr.getHits().getHits()) {
			System.out.println("the array name in question is:");
			System.out.println(array_name);

			ArrayList<Integer> arr = (ArrayList<Integer>) hit.sourceAsMap().get("arr");
			Integer[] wrapperArr = arr.toArray(new Integer[arr.size()]);
			int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
			;
			// the size of this array is always the number of days in the
			// gmt_day_id_to_datestring_map.
			decompressed_arr = PairArray.get_decompressed_array_from_compressed_array(compressed_arr,
					CentralSingleton.gmt_day_id_to_dataPoint.size(), 1);
			// now the decompressed array,
		}
		return decompressed_arr;
	}

	



	/****
	 * this test checks that the threshold to make the trailing stop loss follow
	 * the price is triggered correctly and then whether subsequently the tsl is
	 * triggered as expected.
	 * THIS IS THE MAIN TSL_BASIC_TEST, IGNORE ALL THE OTHERS.
	 * @throws Exception
	 * 
	 * 
	 */
	//@Test
	public void test_trailing_stop_loss_threshold() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.test_methods.put(test_calculate_tsl_threshold, new TestMethod(test_calculate_tsl_threshold,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_tsl_threshold))));

		StockConstants.test_methods.put(test_should_skip_part_after_update_stop_loss,
				new TestMethod(test_should_skip_part_after_update_stop_loss,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.stop_after_update_stop_loss))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> ei_entry : initially_downloaded_exchanges.entrySet()) {
			assertTrue("there are entities", ei_entry.getValue().getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> e_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				Entity e = e_entry.getValue();
				HashMap<String, int[]> hm_of_all_trailing_stop_losses = PairArray
						.get_hashmap_of_all_trailing_stop_loss_arrays(e.getUnique_name(), new HashMap<String, int[]>());
				TestSingletonValue v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
				for (Map.Entry<Integer, Integer> stop_loss_day_ids : v
						.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().entrySet()) {

					Integer day_id_where_this_stop_loss_was_simulated = stop_loss_day_ids.getValue();
					Integer stop_loss_amount = stop_loss_day_ids.getKey();
					SortedMap<Integer, DataPoint> all_dps_less_than_this_day_id = e.getPrice_map_internal()
							.subMap(day_id_where_this_stop_loss_was_simulated
									- StockConstants.Entity_total_pairs_for_a_day,
							day_id_where_this_stop_loss_was_simulated);
					String name_of_relevant_trailing_stop_loss_array = PairArray
							.get_name_of_relevant_trailing_stop_loss_arr(e.getUnique_name(), "close_close",
									stop_loss_amount, 2);
					System.out.println("name of relevant tsl arr is:" + name_of_relevant_trailing_stop_loss_array);
					int[] trailing_stop_loss_array = hm_of_all_trailing_stop_losses
							.get(name_of_relevant_trailing_stop_loss_array);
					for (Integer start_day_id : all_dps_less_than_this_day_id.keySet()) {
						//ignore this start_day_id as this was the id on which we simulated
						//the move in the direction opposite to the stop loss.
						if (start_day_id != 1457) {
							PairArray pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
									day_id_where_this_stop_loss_was_simulated, e);
							HashMap<String, Integer> actual_start_day_end_day_id = PairArray
									.pair_differences_index_start_and_end_day_id(
											trailing_stop_loss_array[start_day_id]);

							assertEquals("start day id: " + start_day_id + " end day id:"
									+ day_id_where_this_stop_loss_was_simulated + " should be corret pair"
									+ "actual existing pair start id:" + actual_start_day_end_day_id.get("start_day_id")
									+ " end day id:" + actual_start_day_end_day_id.get("end_day_id"),
									pa.getPair_difference_array_index().intValue(),
									trailing_stop_loss_array[start_day_id]);
							// System.out.println("worked for start day id:" +
							// start_day_id);
						}
					}
				}
			}
		}

	}
	
	/***
	 *
	 * PASSES EXPLANATION IS UNDER RELEVANT LINE IN THE FUNCTION.
	 * 
	 * @throws Exception
	 */
	//@Test
	public void test_stat_stop_loss_on_two_runs() throws Exception {
		CentralSingleton.initializeSingleton();

		StockConstants.define_custom_end_date = true;
		StockConstants.minus_days_from_current_datetime = 200;

		StockConstants.test_methods.put(test_calculate_stat_stop_loss_first_run,
				new TestMethod(test_calculate_stat_stop_loss_first_run,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_stat_stop_loss_first_run))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		/****
		 * this hashmap is structured as follows: entity-unique-name => {
		 * stop_loss_amount => { start_day_id => large index pair } }
		 * 
		 * it is built after running the first download run. it is utilized
		 * after the second download run, to verify that each entity, each stat
		 * stop loss stays the same.
		 * 
		 */
		HashMap<String, int[]> stop_loss_arrays = new HashMap<String, int[]>();

		for (Map.Entry<String, EntityIndex> ex_entry : initially_downloaded_exchanges.entrySet()) {
			ex_entry.getValue().commit_entity_index_to_elasticsearch(1);
			for (Map.Entry<String, Entity> entity_entry : ex_entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// now get the point at which each stop loss was set.
				// then get the relevant array from the remote es.
				// then check that it is set correctly
				TestSingletonValue tv = new TestSingletonValue(e);
				tv = TestSingleton.getTsingle().get_singleton_value(tv);
				// tv.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get
				// just store the old length of the stop loss arrays.
				// and

				for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
					Integer stop_loss_amount = StockConstants.Entity_stop_loss_amounts[i];
					String stat_stop_loss_arr_name = PairArray.get_name_of_relevant_stat_stop_loss_map(
							e.getUnique_name(), "close_close", stop_loss_amount);
					int[] stat_stop_loss_arr = get_sorted_array_from_remote_es(stat_stop_loss_arr_name);
					stop_loss_arrays.put(stat_stop_loss_arr_name, stat_stop_loss_arr);
				}

			}

		}

		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		StockConstants.define_custom_end_date = false;

		// now we redownload the remaining datapoints, and run the thing just as
		// before.
		// StockConstants.test_methods.put(test_calculate_stat_stop_loss_first_run,
		// new TestMethod(test_calculate_stat_stop_loss_first_run,
		// new
		// ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_stat_stop_loss_first_run))));

		StockConstants.test_methods.put(normalize_price_map_internal_before_stat_stop_loss_second_run, new TestMethod(
				normalize_price_map_internal_before_stat_stop_loss_second_run, new ArrayList<String>(Arrays
						.asList(TestMethodsExecutor.normalize_price_map_internal_before_stat_stop_loss_second_run))));

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> ex_entry : redownloaded_exchanges.entrySet()) {
			// now get all the similar arrs.
			for (Map.Entry<String, Entity> entity_entry : ex_entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
					Integer stop_loss_amount = StockConstants.Entity_stop_loss_amounts[i];
					String stat_stop_loss_arr_name = PairArray.get_name_of_relevant_stat_stop_loss_map(
							e.getUnique_name(), "close_close", stop_loss_amount);
					int[] stat_stop_loss_arr = get_sorted_array_from_remote_es(stat_stop_loss_arr_name);

					assertTrue("arr name is:" + stat_stop_loss_arr_name,
							Arrays.equals(stat_stop_loss_arr, stop_loss_arrays.get(stat_stop_loss_arr_name)));
					System.out.println("the test passed successfully with array:" + stat_stop_loss_arr_name);
				}

			}

		}

	}
	
	
	/***
	 * two run tsl test.
	 * @throws Exception 
	 * 
	 */
	@Test
	public void test_trailing_stop_loss_on_two_runs() throws Exception {
		
		CentralSingleton.initializeSingleton();

		StockConstants.define_custom_end_date = true;
		StockConstants.minus_days_from_current_datetime = 200;

		StockConstants.test_methods.put(test_tsl_first_run,
				new TestMethod(test_tsl_first_run,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor
								.test_tsl_first_run))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);
		
		HashMap<String, int[]> stop_loss_arrays = new HashMap<String, int[]>();

		for (Map.Entry<String, EntityIndex> ex_entry : initially_downloaded_exchanges.entrySet()) {
			ex_entry.getValue().commit_entity_index_to_elasticsearch(1);
			for (Map.Entry<String, Entity> entity_entry : ex_entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				// now get the point at which each stop loss was set.
				// then get the relevant array from the remote es.
				// then check that it is set correctly
				TestSingletonValue tv = new TestSingletonValue(e);
				tv = TestSingleton.getTsingle().get_singleton_value(tv);
				// tv.getDay_ids_where_dummy_stat_stop_losses_were_simulated().get
				// just store the old length of the stop loss arrays.
				// and

				for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
					Integer stop_loss_amount = StockConstants.Entity_stop_loss_amounts[i];
					String tsl_arr_name = PairArray.get_name_of_relevant_trailing_stop_loss_arr(
							e.getUnique_name(), "close_close", stop_loss_amount,100000);
					int[] tsl_arr = get_sorted_array_from_remote_es(tsl_arr_name);
					stop_loss_arrays.put(tsl_arr_name, tsl_arr);
				}

			}

		}

		TestSingleton.destroySingleton();
		TestSingleton.initialize_test_singleton();
		StockConstants.define_custom_end_date = false;

		StockConstants.test_methods.put(test_tsl_second_run,
				new TestMethod(
				test_tsl_second_run, new ArrayList<String>(Arrays
						.asList(TestMethodsExecutor
								.test_tsl_second_run))));

		HashMap<String, EntityIndex> redownloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> ex_entry : redownloaded_exchanges.entrySet()) {
			// now get all the similar arrs.
			for (Map.Entry<String, Entity> entity_entry : ex_entry.getValue().getEntity_map().entrySet()) {
				Entity e = entity_entry.getValue();
				for (int i = 0; i < StockConstants.Entity_stop_loss_amounts.length; i++) {
					Integer stop_loss_amount = StockConstants.Entity_stop_loss_amounts[i];
					String tsl_arr_name = PairArray.get_name_of_relevant_trailing_stop_loss_arr(
							e.getUnique_name(), "close_close", stop_loss_amount,100000);
					int[] tsl_arr = get_sorted_array_from_remote_es(tsl_arr_name);
					
					assertTrue("arr name is:" + tsl_arr_name,
							Arrays.equals(tsl_arr, stop_loss_arrays.get(tsl_arr_name)));
					System.out.println("the test passed successfully with array:" 
							+ tsl_arr_name);
				}

			}

		}
		
		
		
	}

	//@Test
	public void test_trailing_stop_loss_behaves_as_bsl() throws Exception {
		CentralSingleton.initializeSingleton();
		StockConstants.test_methods.put(test_calculated_tsl_as_bsl, new TestMethod(test_calculated_tsl_as_bsl,
				new ArrayList<String>(Arrays.asList(TestMethodsExecutor.test_tsl_as_bsl))));

		StockConstants.test_methods.put(test_should_skip_part_after_update_stop_loss,
				new TestMethod(test_should_skip_part_after_update_stop_loss,
						new ArrayList<String>(Arrays.asList(TestMethodsExecutor.stop_after_update_stop_loss))));

		HashMap<String, EntityIndex> initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), false, false, true);

		for (Map.Entry<String, EntityIndex> ei_entry : initially_downloaded_exchanges.entrySet()) {
			assertTrue("there are entities", ei_entry.getValue().getEntity_map().size() > 0);
			for (Map.Entry<String, Entity> e_entry : ei_entry.getValue().getEntity_map().entrySet()) {
				Entity e = e_entry.getValue();
				HashMap<String, int[]> hm_of_all_trailing_stop_losses = PairArray
						.get_hashmap_of_all_trailing_stop_loss_arrays(e.getUnique_name(), new HashMap<String, int[]>());
				TestSingletonValue v = TestSingleton.getTsingle().get_singleton_value(new TestSingletonValue(e));
				for (Map.Entry<Integer, Integer> stop_loss_day_ids : v
						.getDay_ids_where_dummy_trailing_stop_losses_were_simulated().entrySet()) {

					Integer day_id_where_this_stop_loss_was_simulated = stop_loss_day_ids.getValue();
					Integer stop_loss_amount = stop_loss_day_ids.getKey();
					SortedMap<Integer, DataPoint> all_dps_less_than_this_day_id = e.getPrice_map_internal()
							.subMap(day_id_where_this_stop_loss_was_simulated
									- StockConstants.Entity_total_pairs_for_a_day,
							day_id_where_this_stop_loss_was_simulated);
					String name_of_relevant_trailing_stop_loss_array = PairArray
							.get_name_of_relevant_trailing_stop_loss_arr(e.getUnique_name(), "close_close",
									stop_loss_amount, 2);
					System.out.println("name of relevant tsl arr is:" + name_of_relevant_trailing_stop_loss_array);
					int[] trailing_stop_loss_array = hm_of_all_trailing_stop_losses
							.get(name_of_relevant_trailing_stop_loss_array);
					for (Integer start_day_id : all_dps_less_than_this_day_id.keySet()) {
						// basically the tsl array should have the day_id_where
						// tsl
						// was generated as the
						PairArray pa = PairArray.get_int_array_index_given_start_and_end_day_id(start_day_id,
								day_id_where_this_stop_loss_was_simulated, e);
						HashMap<String, Integer> actual_start_day_end_day_id = PairArray
								.pair_differences_index_start_and_end_day_id(trailing_stop_loss_array[start_day_id]);

						assertEquals("start day id: " + start_day_id + " end day id:"
								+ day_id_where_this_stop_loss_was_simulated + " should be corret pair"
								+ "actual existing pair start id:" + actual_start_day_end_day_id.get("start_day_id")
								+ " end day id:" + actual_start_day_end_day_id.get("end_day_id"),
								pa.getPair_difference_array_index().intValue(), trailing_stop_loss_array[start_day_id]);
						// System.out.println("worked for start day id:" +
						// start_day_id);
					}
				}
			}
		}
	}
}
