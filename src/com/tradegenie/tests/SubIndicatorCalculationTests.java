package com.tradegenie.tests;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import subindicators.RiseFallAmount;


public class SubIndicatorCalculationTests {

	@Test
	public void test_find_nearest(){
		Integer change = 1000;
		Map.Entry<Integer, String> nearest = RiseFallAmount.find_nearest(change, RiseFallAmount.rise_fall_amounts);

		assertTrue("nearest to 1000 should be 1000",nearest.getKey().equals(1000));
		
		change = 620;
		nearest = RiseFallAmount.find_nearest(change, RiseFallAmount.rise_fall_amounts);
		assertTrue("nearest to 620 should be 600",nearest.getKey().equals(600));
		
	
	}
	
}
