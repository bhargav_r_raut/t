package com.tradegenie.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.cassandra.utils.OutputHandler.SystemOutput;
import org.junit.Test;

import ComplexProcessor.Result;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import ExchangeBuilder.dataPointIndicator;
import Indicators.TickMapIndicator;
import elasticSearchNative.LocalEs;
import elasticSearchNative.RemoteEs;
import eu.verdelhan.ta4j.indicators.statistics.StandardDeviationIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;
import subindicators.Complex;
import subindicators.ForTheMonthWeekYearQuarter;
import subindicators.IndicatorValueChangeStandardDeviation;
import subindicators.NDayHighLow;
import subindicators.Pattern;
import subindicators.RiseFallAmount;
import subindicators.SmaCross;
import subindicators.StandardDeviation;
import subindicators.SubIndicatorCalculation;
import subindicators.TImeBasedSubindicators;
import yahoo_finance_historical_scraper.StockConstants;

public class SubIndicatorTests extends DownloadEntityExchangeTests {

	

	/****
	 * 
	 * @throws Exception
	 */
	// @Test
	public void test_n_day_high_low_basic() throws Exception {

		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				Integer first_day_id = e.getPrice_map_internal().firstKey();
				Integer ninth_day_id = first_day_id + 8;
				Integer ninth_day_set_at = null;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					DataPoint d = e.getPrice_map_internal().get(day_id);
					for (String indicator_name : d.getIndicator_values_on_datapoint().keySet()) {
						if (day_id < ninth_day_id) {
							index_entry.getValue().getEntity_map().get(entity_name).getPrice_map_internal().get(day_id)
									.getIndicator_values_on_datapoint().get(indicator_name)
									.setIndicator_integer_value(1);
							System.out.println("day id:" + day_id + " value:" + 1);
						} else if (day_id >= ninth_day_id && ninth_day_set_at == null) {
							index_entry.getValue().getEntity_map().get(entity_name).getPrice_map_internal().get(day_id)
									.getIndicator_values_on_datapoint().get(indicator_name)
									.setIndicator_integer_value(9);
							ninth_day_set_at = day_id;
							System.out.println("day id:" + day_id + " value:" + 9);
						} else if (day_id >= ninth_day_id && ninth_day_set_at != null) {
							index_entry.getValue().getEntity_map().get(entity_name).getPrice_map_internal().get(day_id)
									.getIndicator_values_on_datapoint().get(indicator_name)
									.setIndicator_integer_value(1);
							if (day_id < 20) {
								System.out.println("day id:" + day_id + " value:" + 1);
							}
						}
					}

				}

				NDayHighLow nd = new NDayHighLow();
				nd.calculate(e);

				// now get the results and assert that things are as you said
				// they would be
				Boolean two_day_low_detected = false;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					if (day_id == ninth_day_set_at) {
						if (nd.getResults().containsKey(day_id)) {
							List<Complex> complexes_at_day_id = nd.getResults().get(day_id);
							HashMap<String, Complex> complexes_by_subindicator_name = new HashMap<String, Complex>();
							for (Complex c : complexes_at_day_id) {
								complexes_by_subindicator_name.put(c.getSubindicator_name(), c);
							}

							assertTrue("9th day high", complexes_by_subindicator_name.containsKey("9 day high"));
							assertTrue("7 day high", complexes_by_subindicator_name.containsKey("1 week high"));
							assertTrue("3 day high", complexes_by_subindicator_name.containsKey("3 day high"));
							System.out.println("high passed.");
						}
					} else if (day_id > ninth_day_set_at) {
						// if it is the third day after the ninth day set at
						// then it becomes the 3 day low.
						if (nd.getResults().containsKey(day_id)) {
							List<Complex> complexes_at_day_id = nd.getResults().get(day_id);
							HashMap<String, Complex> complexes_by_subindicator_name = new HashMap<String, Complex>();
							for (Complex c : complexes_at_day_id) {
								complexes_by_subindicator_name.put(c.getSubindicator_name(), c);
							}
							if (day_id == 10) {
								// this should be the 3 day low, but only the
								// first occurence.
								assertTrue("3 day low", complexes_by_subindicator_name.containsKey("3 day low"));
								assertTrue("1 week low", complexes_by_subindicator_name.containsKey("1 week low"));
								assertTrue("9 day low", complexes_by_subindicator_name.containsKey("9 day low"));
								assertTrue("contains only these three keys",
										complexes_by_subindicator_name.size() == 3);
								// System.out.println("low passed on day id: " +
								// day_id);
								/// two_day_low_detected = true;
								System.out.println("day id 10 passed");
							}
							if (day_id == 11) {
								assertFalse("3 day low", complexes_by_subindicator_name.containsKey("3 day low"));
								assertTrue("1 week low", complexes_by_subindicator_name.containsKey("1 week low"));
								assertTrue("9 day low", complexes_by_subindicator_name.containsKey("9 day low"));
								System.out.println("day id 11 passed");
							}
						}
					}
				}
			}
		}
	}

	@Test
	public void test_n_day_high_low_id_generation() {
		NDayHighLow nd = new NDayHighLow();
		HashMap<String, String> names_to_ids = nd.get_subindicator_names_to_ids();
		ArrayList<Integer> high_low = new ArrayList<Integer>(Arrays.asList(1, -1));
		for (Integer period : NDayHighLow.custom_periods_map.keySet()) {
			for (Integer high_or_low : high_low) {
				HashMap<String, Object> arguments = new HashMap<String, Object>();
				arguments.put("high_or_low", high_or_low);
				arguments.put("period", period);
				String subindicator_name = nd.get_subindicator_name(arguments);
				String subindicator_id = nd.get_subindicator_id(arguments);
				// now we should compare with names to ids.
				System.out.println("period:" + period + "high low:" + high_or_low);
				assertTrue("the name exists: " + subindicator_name,
						names_to_ids.containsKey(subindicator_name));
				System.out.println("the id should be:" + names_to_ids.get(subindicator_name));
				assertTrue("the id is equal:" + subindicator_id,
						names_to_ids.get(subindicator_name).equals(subindicator_id));
			}
		}
	}

	// @Test
	public void test_subindicator_rise_fall_amounts() throws Exception {
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			// setting the required dummy values.
			// we cannot know if the entity will have any given two day ids
			// successively
			// present in its price map internal.
			// so first have to find which period is available
			// for eg.
			// take each period in the custom periods from Ndayhighlow
			// then add that period to the first available day id in this entity
			// then see if that day id (after adding) is present as a day id in
			// the price map internal.
			// the make that day-id (referred as second_Day_id) close price to 2
			// and make the first day ids close to 1
			// store the day id , period, at which the change is to be detected.

			/***
			 * this map stores which day we have used as a second day id and
			 * what period that second day id was.
			 * 
			 * key -> entity name
			 * 
			 * value -> { "period" => whatever, "second_day_id" => whatever. }
			 * 
			 */
			HashMap<String, HashMap<String, Integer>> entity_changed_day_id_and_period = new HashMap<String, HashMap<String, Integer>>();

			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {

				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				Integer first_day_id = e.getPrice_map_internal().firstKey();

				Integer second_day_id = null;

				for (Map.Entry<Integer, String> period_entry : RiseFallAmount.periods_map.entrySet()) {

					second_day_id = first_day_id + period_entry.getKey();

					if (e.getPrice_map_internal().containsKey(second_day_id)) {
						e.getPrice_map_internal().get(first_day_id).setClose("2");
						e.getPrice_map_internal().get(second_day_id).setClose("1");
						HashMap<String, Integer> period_and_second_day_id = new HashMap<String, Integer>();
						period_and_second_day_id.put("period", period_entry.getKey());
						period_and_second_day_id.put("second_day_id", second_day_id);
						entity_changed_day_id_and_period.put(e.getUnique_name(), period_and_second_day_id);
						break;
					}

				}
			}

			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {

				if (entity_changed_day_id_and_period.containsKey(entity_name)) {

					System.out.println("--------------------------------------------------------");
					System.out.println("period entry and second day map is:");

					System.out.println(entity_changed_day_id_and_period);

					Integer period = entity_changed_day_id_and_period.get(entity_name).get("period");
					Integer day_id_on_which_changed_expected = entity_changed_day_id_and_period.get(entity_name)
							.get("second_day_id");
					Entity e = index_entry.getValue().getEntity_map().get(entity_name);
					RiseFallAmount rf = new RiseFallAmount();
					rf.calculate(e);
					assertTrue("complex calculated on second day id:" + day_id_on_which_changed_expected,
							rf.getResults().containsKey(day_id_on_which_changed_expected));
					List<Complex> complexes = rf.getResults().get(day_id_on_which_changed_expected);
					Complex c = complexes.get(0);

					System.out.println(c.toString());
					assertTrue("falls by 100 percent exists", c.getSubindicator_name()
							.equals("falls by forty percent in " + RiseFallAmount.periods_map.get(period)));
				}
			}
		}
	}

	// @Test
	public void test_subindicator_rise_fall_amounts_positive() throws Exception {
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			// setting the required dummy values.
			// we cannot know if the entity will have any given two day ids
			// successively
			// present in its price map internal.
			// so first have to find which period is available
			// for eg.
			// take each period in the custom periods from Ndayhighlow
			// then add that period to the first available day id in this entity
			// then see if that day id (after adding) is present as a day id in
			// the price map internal.
			// the make that day-id (referred as second_Day_id) close price to 2
			// and make the first day ids close to 1
			// store the day id , period, at which the change is to be detected.

			/***
			 * this map stores which day we have used as a second day id and
			 * what period that second day id was.
			 * 
			 * key -> entity name
			 * 
			 * value -> { "period" => whatever, "second_day_id" => whatever. }
			 * 
			 */
			HashMap<String, HashMap<String, Integer>> entity_changed_day_id_and_period = new HashMap<String, HashMap<String, Integer>>();

			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {

				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				Integer first_day_id = e.getPrice_map_internal().firstKey();

				Integer second_day_id = null;

				for (Map.Entry<Integer, String> period_entry : RiseFallAmount.periods_map.entrySet()) {

					second_day_id = first_day_id + period_entry.getKey();

					if (e.getPrice_map_internal().containsKey(second_day_id)) {
						e.getPrice_map_internal().get(first_day_id).setClose("1");
						e.getPrice_map_internal().get(second_day_id).setClose("2");
						HashMap<String, Integer> period_and_second_day_id = new HashMap<String, Integer>();
						period_and_second_day_id.put("period", period_entry.getKey());
						period_and_second_day_id.put("second_day_id", second_day_id);
						entity_changed_day_id_and_period.put(e.getUnique_name(), period_and_second_day_id);
						break;
					}

				}
			}

			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {

				if (entity_changed_day_id_and_period.containsKey(entity_name)) {

					System.out.println("--------------------------------------------------------");
					System.out.println("period entry and second day map is:");

					System.out.println(entity_changed_day_id_and_period);

					Integer period = entity_changed_day_id_and_period.get(entity_name).get("period");
					Integer day_id_on_which_changed_expected = entity_changed_day_id_and_period.get(entity_name)
							.get("second_day_id");
					Entity e = index_entry.getValue().getEntity_map().get(entity_name);
					RiseFallAmount rf = new RiseFallAmount();
					rf.calculate(e);
					assertTrue("complex calculated on second day id:" + day_id_on_which_changed_expected,
							rf.getResults().containsKey(day_id_on_which_changed_expected));
					List<Complex> complexes = rf.getResults().get(day_id_on_which_changed_expected);
					Complex c = complexes.get(0);

					System.out.println(c.toString());
					assertTrue("rises by 100 percent exists", c.getSubindicator_name()
							.equals("rises by hundred percent in " + RiseFallAmount.periods_map.get(period)));
				}
			}
		}
	}

	public void test_subindicator_rise_fall_amounts_id_generation() {
		RiseFallAmount rise_fall_subindicator = new RiseFallAmount();
		HashMap<String, String> subindicator_names_to_ids = rise_fall_subindicator.get_subindicator_names_to_ids();
		Integer counter = 0;
		ArrayList<Integer> rises_or_falls = new ArrayList<Integer>(Arrays.asList(0, 1));
		for (Integer i : rises_or_falls) {
			for (Map.Entry<Integer, String> rise_fall_entry : rise_fall_subindicator.rise_fall_amounts.entrySet()) {

				for (Integer period : RiseFallAmount.periods_map.keySet()) {

					HashMap<String, Object> args = new HashMap<String, Object>();

					args.put("percentage_change", rise_fall_entry.getKey());
					args.put("period", period);

					String subindicator_id = rise_fall_subindicator.get_subindicator_id(args);
					String subindicator_name = rise_fall_subindicator.get_subindicator_name(args);

					System.out.println("subindicator name is: " + subindicator_name);
					assertTrue("the subindicator name exists",
							subindicator_names_to_ids.containsKey(subindicator_name));

					assertTrue("the id is equivalent",
							subindicator_names_to_ids.get(subindicator_name).equals(subindicator_id));

					counter++;
				}
			}
		}
	}

	// @Test
	public void test_pattern_subindicator() throws Exception {
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {
			/// this map is the same for all entities.
			// it is just written to again and again inside the following loop.
			// each time the same things are written.
			HashMap<Integer, String> expected_day_ids_and_patterns = new HashMap<Integer, String>();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				Integer counter = 0;

				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					switch (counter) {
					case 0:
						e.getPrice_map_internal().get(day_id).setClose("10");
						break;
					case 1:
						e.getPrice_map_internal().get(day_id).setClose("11");
						break;
					case 2:
						// should show up_up pattern
						e.getPrice_map_internal().get(day_id).setClose("12");
						String subindicator_name = null;
						expected_day_ids_and_patterns.put(day_id,
								Pattern.permutations_map.get("11")[Pattern.permutations_map.get("11").length - 1]);
						break;
					case 3:
						// should show up_up_down pattern.
						e.getPrice_map_internal().get(day_id).setClose("11");
						expected_day_ids_and_patterns.put(day_id,
								Pattern.permutations_map.get("112")[Pattern.permutations_map.get("112").length - 1]);
					default:
						break;
					}
					counter++;
				}
			}

			//
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				Pattern ptn = new Pattern();
				ptn.calculate(e);
				for (Map.Entry<Integer, String> expected_change : expected_day_ids_and_patterns.entrySet()) {
					Integer day_id = expected_change.getKey();
					assertTrue("complex exists on day_id:" + expected_change.getKey(),
							ptn.getResults().containsKey(day_id));
					List<Complex> complexes = ptn.getResults().get(day_id);
					if (day_id == 2) {
						System.out.println(complexes.get(0).toString());
						assertTrue("pattern exists",
								complexes.get(0).getSubindicator_name().equals(expected_change.getValue()));
					} else if (day_id == 3) {
						System.out.println(complexes.get(1).toString());
						assertTrue("pattern exists",
								complexes.get(1).getSubindicator_name().equals(expected_change.getValue()));
					}
				}
			}

		}
	}

	/****
	 * 
	 * id generation doesnt need to be tested neither does name generation =>
	 * these are being directly set from a hashmap.
	 * 
	 */
	public void test_pattern_subindicator_id_generation() {

	}

	/****
	 * {0={} 1={} 2={3=1.0000} 3={3=1.0000} 4={3=1.0000} 7={3=1.0000, 7=1.0000}
	 * 8={3=1.0000, 7=1.0000, 9=1.0000} 9={3=1.3333, 7=1.1429, 9=1.1250}
	 * 10={3=1.6667, 7=1.2857, 9=1.2222} 11={3=2.0000, 7=1.4286, 9=1.3333}
	 * 14={3=1.6667, 7=1.4286, 9=1.3333, 14=1.2727} 15={3=1.3333, 7=1.4286,
	 * 9=1.3333, 14=1.2500} 16={3=1.0000, 7=1.4286, 9=1.3333, 14=1.2308}
	 * 17={3=1.0000, 7=1.4286, 9=1.3333, 14=1.2143} 18={3=1.0000, 7=1.2857,
	 * 9=1.3333, 14=1.2143}}
	 *
	 * 
	 * its processing it like this because:
	 * 
	 * 1. oil is only applicable to close 2. it does produce crosses for other
	 * subindicators. 3. close comes before open, high, low and volumen in the
	 * indicators list. 4. add a global indicators_applicable to all the
	 * subindicators.
	 * 
	 *
	 *
	 * @throws Exception
	 */
	public void test_sma_cross_subindicator() throws Exception {
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			HashMap<Integer, String> expected_day_ids_and_subindicators = new HashMap<Integer, String>();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				for (Integer day_id : e.getPrice_map_internal().keySet()) {

					if (e.datapoints_upto_current_day_id(day_id, true) <= 7) {
						// set close as 1
						e.getPrice_map_internal().get(day_id).setClose("1");

					} else if (e.datapoints_upto_current_day_id(day_id, true) <= 10) {
						// set close as 2
						e.getPrice_map_internal().get(day_id).setClose("2");

					} else {
						e.getPrice_map_internal().get(day_id).setClose("1");
						if (day_id == 15) {
							expected_day_ids_and_subindicators.put(day_id, "7 day sma crosses above 3  day sma");

						}
					}
				}
			}

			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				SmaCross sma_cross = new SmaCross();

				sma_cross.calculate(e);

				System.out.println("the results of sma cross are:");

				System.out.println(sma_cross.getResults().keySet());

				for (Map.Entry<Integer, String> expected_change : expected_day_ids_and_subindicators.entrySet()) {
					Integer day_id = expected_change.getKey();

					assertTrue("complex exists on day_id:" + expected_change.getKey(),
							sma_cross.getResults().containsKey(day_id));
					List<Complex> complexes = sma_cross.getResults().get(day_id);

					assertTrue("complex exists",
							complexes.get(0).getSubindicator_name().equals(expected_change.getValue()));

					for (int i = 0; i < complexes.size(); i++) {

						if (i > 0) {
							Complex c = complexes.get(i);
							assertFalse("complex is present only once",
									complexes.get(0).getSubindicator_name().equals(c.getSubindicator_name()));
						}

					}

					System.out.println("assert works");
				}

			}
		}
	}

	// @Test
	public void test_sma_cross_subindicator_id_generation() {
		SmaCross sma = new SmaCross();
		HashMap<String, String> subindicator_names_to_ids = SmaCross.get_subindicator_names_to_ids();
		for (Map.Entry<Integer, String> period_entry : SmaCross.custom_periods_map.entrySet()) {
			HashMap<String, Object> args = new HashMap<String, Object>();
			for (Map.Entry<Integer, String> period_entry_two : SmaCross.custom_periods_map.entrySet()) {
				// if 1 crosses above 3
				if (!period_entry.equals(period_entry_two)) {
					Integer crosser = period_entry.getKey();
					Integer crossed = period_entry_two.getKey();
					args.put("crosser", crosser);
					args.put("crossed", crossed);
					String subindicator_id = sma.get_subindicator_id(args);
					// the name is
					String subindicator_name = crosser + " day sma crosses above " + crossed + " day sma";

					System.out.println("subindicator id calculated is:" + subindicator_id);
					System.out.println("subindicator name calculated is:" + subindicator_name);
					assertTrue("the name :" + subindicator_name + " exists ",
							subindicator_names_to_ids.containsKey(subindicator_name));
					assertTrue("the id:" + subindicator_id + " is equal ",
							subindicator_id.equals(subindicator_names_to_ids.get(subindicator_name)));
				}
			}
		}
	}

	/***
	 * indicator:close std_dev:0.5000 mean:1.5000 difference:-0.5000 day_id:507
	 * indicator_value:1.0000 ratio:-1000.0000 expected to fall by one standard
	 * deviation. ---------------------------- indicator:close std_dev:0.5000
	 * mean:1.5000 difference:0.5000 day_id:508 indicator_value:2.0000
	 * ratio:1000.0000 expected to rise by 1 standard deviation.
	 * 
	 */
	// @Test
	public void test_standard_deviation_subindicator() throws Exception {

		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			HashMap<Integer, String> expected_day_ids_and_subindicators = new HashMap<Integer, String>();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				/***
				 * 
				 * modify the day ids alternately if the day_id is even then set
				 * close to 1.00 if the day id is odd then set close to 2.00
				 * 
				 */
				Integer counter = 0;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					if (counter % 2 == 0) {
						e.getPrice_map_internal().get(day_id).setClose("1.00");
					} else {
						e.getPrice_map_internal().get(day_id).setClose("2.00");
					}
					counter++;
				}

				/***
				 * 
				 * 
				 * now figure out what is the standard deviation at that stage.
				 * 
				 * 
				 */

			}
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				StandardDeviation std_dev_subindicator = new StandardDeviation();
				std_dev_subindicator.calculate(e);

				List<Complex> complexes_on_day_id_507 = std_dev_subindicator.getResults().get(507);

				String subindicator_name_on_507 = complexes_on_day_id_507.get(0).getSubindicator_name();

				String subindicator_id_on_507 = complexes_on_day_id_507.get(0).getSubindicator_id();

				assertTrue("day id 507 has subindicator fall by 1 standard deviation",
						subindicator_name_on_507.equals("falls by 1 standard deviation"));

				System.out.println("the subindicator id on 507 is: " + subindicator_id_on_507);

				assertTrue("day id 507 has id", subindicator_id_on_507
						.equals(SubIndicatorCalculation.subindicator_prefix_map.get("StandardDeviation") + "28"));

				List<Complex> complexes_on_day_id_508 = std_dev_subindicator.getResults().get(508);

				String subindicator_name_on_508 = complexes_on_day_id_508.get(0).getSubindicator_name();

				assertTrue("day id 508 has subindicator fall by 1 standard deviation",
						subindicator_name_on_508.equals("rises by 1 standard deviation"));

				String subindicator_id_on_508 = complexes_on_day_id_508.get(0).getSubindicator_id();
				assertTrue("day id 508 has id", subindicator_id_on_508
						.equals(SubIndicatorCalculation.subindicator_prefix_map.get("StandardDeviation") + "9"));
			}
		}

	}

	// @Test
	public void test_standard_deviation_id_generation() {
		StandardDeviation stddev = new StandardDeviation();
		HashMap<String, String> subindicator_names_to_ids = StandardDeviation.subindicator_names_to_ids();
		Integer counter = 0;
		ArrayList<Integer> rises_or_falls = new ArrayList<Integer>(Arrays.asList(1, -1));
		for (Integer i : rises_or_falls) {
			for (Map.Entry<Integer, String> std_dev_entry : stddev.standard_deviations.entrySet()) {

				HashMap<String, Object> args = new HashMap<String, Object>();
				args.put("standard_deviation_amount_and_name", std_dev_entry);
				args.put("rises_or_falls", i);
				String subindicator_id = stddev.get_subindicator_id(args);
				String subindicator_name = stddev.get_subindicator_name(args);

				System.out.println("subindicator name is: " + subindicator_name);
				assertTrue("the subindicator name exists", subindicator_names_to_ids.containsKey(subindicator_name));

				assertTrue("the id is equivalent",
						subindicator_names_to_ids.get(subindicator_name).equals(subindicator_id));

				counter++;
			}
		}
	}

	// finish at night.
	// @Test
	public void test_indicator_value_change_standard_deviation_subindicator_id_generation() throws Exception {

		IndicatorValueChangeStandardDeviation insd = new IndicatorValueChangeStandardDeviation();
		HashMap<String, String> nids = insd.get_subindicator_names_to_ids();

		System.out.println(insd.get_subindicator_names_to_ids());
		for (Map.Entry<String, String> name_id : insd.get_subindicator_names_to_ids().entrySet()) {
			if (name_id.getKey().startsWith("2 day change rises by")) {
				System.out.println(name_id.getKey());
			}
		}

		// now for each period and each and every standard deviation.
		for (Integer period : IndicatorValueChangeStandardDeviation.custom_periods_map.keySet()) {
			ArrayList<Integer> rises_or_falls = new ArrayList<Integer>(Arrays.asList(1, -1));
			for (Integer rf : rises_or_falls) {

				for (Map.Entry<Integer, String> standard_deviations : StandardDeviation.standard_deviations
						.entrySet()) {

					HashMap<String, Object> arguments = new HashMap<String, Object>();

					arguments.put("standard_deviation_amount_and_name", standard_deviations);
					arguments.put("rises_or_falls", rf);
					arguments.put("period", period);

					String subindicator_id = insd.get_subindicator_id(arguments);
					String subindicator_name = insd.get_subindicator_name(arguments);

					assertTrue("the name :" + subindicator_name + " exists ", nids.containsKey(subindicator_name));
					System.out.println("the expected id is:" + nids.get(subindicator_name));
					assertTrue("the id is equivalent for:" + subindicator_name + "id :" + subindicator_id,
							nids.get(subindicator_name).equals(subindicator_id));

				}
			}
		}

	}

	/****
	 * @throws Exception
	 */
	// @Test
	public void test_indicator_changes_calculated_correctly() throws Exception {

		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			HashMap<Integer, String> expected_day_ids_and_subindicators = new HashMap<Integer, String>();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);

				/***
				 * 
				 * modify the day ids alternately if the day_id is even then set
				 * close to 1.00 if the day id is odd then set close to 2.00
				 * 
				 */
				Integer counter = 1;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					e.getPrice_map_internal().get(day_id).setClose(counter.toString());
					// if the counter has become 3, make it to 0
					if (counter.equals(3)) {
						counter = 0;
					}
					// and the increment makes it 1 again.
					// so the values become : 1,2,3,1,2,3,1,2,3,1,2,3
					counter++;
				}

				/***
				 * 
				 * 
				 * now figure out what is the standard deviation at that stage.
				 * 
				 * 
				 */

			}
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().before_calculate_entity_index_complexes();
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				// System.out.println(e.getPrice_map_internal().keySet());
				IndicatorValueChangeStandardDeviation indicator_value_change_std_dev_subindicator = new IndicatorValueChangeStandardDeviation();
				indicator_value_change_std_dev_subindicator.calculate(e);

				HashMap<String, TreeMap<Integer, dataPointIndicator>> indicator_price_changes_map = indicator_value_change_std_dev_subindicator
						.getIndicator_changes_hashmap_for_test();

				for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {

					ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

					/****
					 * iterate each applicable indicator cum period name.
					 * 
					 * 
					 */
					for (final String indicator_cum_period_name : indicator_cum_period_names) {

						for (Integer period : StandardDeviation.custom_periods_map.keySet()) {

							if (period > StandardDeviation.only_consider_periods_greater_than) {

								TreeMap<Integer, dataPointIndicator> idp =

								indicator_price_changes_map.get(indicator_cum_period_name + "-" + period.toString());

								TickMapIndicator mean_tm_indicator = (TickMapIndicator) indicator_value_change_std_dev_subindicator
										.getStandard_deviation_and_mean_indicators()
										.get(indicator_cum_period_name + "-" + period.toString() + "-mean");

								TickMapIndicator stddev_tm_indicator = (TickMapIndicator) indicator_value_change_std_dev_subindicator
										.getStandard_deviation_and_mean_indicators().get(indicator_cum_period_name + "-"
												+ period.toString() + "-standard-deviation");

								StandardDeviationIndicator stddev_indicator = (StandardDeviationIndicator) indicator_value_change_std_dev_subindicator
										.getStandard_deviation_and_mean_indicators().get(indicator_cum_period_name + "-"
												+ period.toString() + "-standard-deviation-indicator");

								SMAIndicator sma_indicator = (SMAIndicator) indicator_value_change_std_dev_subindicator
										.getStandard_deviation_and_mean_indicators()
										.get(indicator_cum_period_name + "-" + period.toString() + "-mean-indicator");

								if (indicator_name.equals("close")) {
									// System.out.println("period is:" +
									// period);

									Integer counter = 0;
									for (Map.Entry<Integer, dataPointIndicator> kk : idp.entrySet()) {

										if (period.equals(3)) {

											if (counter.equals(20)) {

												// System.out.println("the
												// trimmed standard deviation");

												// System.out
												/// .println(
												// stddev_indicator
												// .getValue(stddev_tm_indicator
												// .getTick_to_timeseries_index_map()
												// .get(kk.getKey()))
												// .toString().substring(0,
												// 15));

												// System.out.println("mean
												// trimmed string is:");

												// System.out
												// .println(
												// sma_indicator
												// .getValue(mean_tm_indicator
												// .getTick_to_timeseries_index_map()
												// .get(kk.getKey()))
												// .toString().substring(0,
												// 15));

												assertTrue("standard deviation as per expectation on 20",
														stddev_indicator
																.getValue(stddev_tm_indicator
																		.getTick_to_timeseries_index_map()
																		.get(kk.getKey()))
																.toString().substring(0, 15).equals("0.7853534524986"));

												assertTrue("mean as per expectation on 20", sma_indicator
														.getValue(mean_tm_indicator.getTick_to_timeseries_index_map()
																.get(kk.getKey()))
														.toString().substring(0, 15).equals("0.0476190476190"));

												HashMap<String, Object> calc_values = indicator_value_change_std_dev_subindicator
														.get_nearest_standard_deviation_entry(sma_indicator,
																mean_tm_indicator, stddev_indicator,
																stddev_tm_indicator, idp, kk.getKey());

												Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) calc_values
														.get("nearest_standard_deviation");

												BigDecimal ratio_as_percentage = (BigDecimal) calc_values
														.get("ratio_as_percentage");

												// System.out.println("nearest
												// standard deviation:" +
												// nearest_standard_deviation.toString());

												// System.out.println("ratio as
												// percentage:" +
												// ratio_as_percentage);

												// on day id :

												// System.out.println("the day
												// id:" + kk.getKey());

												// System.out.println("indicator
												// cum period name:");
												// System.out.println(indicator_cum_period_name);

												// System.exit(1);

												// so comp
											} else if (counter.equals(301)) {

												// System.out.println("the
												// trimmed standard deviation");

												// System.out.println(stddev_indicator.getValue(stddev_tm_indicator
												// .getTick_to_timeseries_index_map().get(kk.getKey())).toString().substring(0,
												// 15));

												// System.out.println("mean
												// trimmed string is:");

												// System.out.println(sma_indicator.getValue(mean_tm_indicator
												// .getTick_to_timeseries_index_map().get(kk.getKey())).toString().substring(0,
												// 1));

												assertTrue("standard deviation as per expectation on 301",
														stddev_indicator
																.getValue(stddev_tm_indicator
																		.getTick_to_timeseries_index_map()
																		.get(kk.getKey()))
																.toString().substring(0, 15).equals("0.8124038404635"));

												assertTrue("mean as per expectation on 301", sma_indicator
														.getValue(mean_tm_indicator.getTick_to_timeseries_index_map()
																.get(kk.getKey()))
														.toString().substring(0, 1).equals("0"));

											} else if (counter.equals(400)) {

												/// ***
												/**
												 * System.out.println(
												 * "the trimmed standard deviation"
												 * );
												 * 
												 * System.out .println(
												 * stddev_indicator
												 * .getValue(stddev_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * () .get(kk.getKey()))
												 * .toString().substring(0,
												 * 15));
												 * 
												 * System.out.println(
												 * "mean trimmed string is:");
												 * 
												 * System.out .println(
												 * sma_indicator
												 * .getValue(mean_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * () .get(kk.getKey()))
												 * .toString().substring(0, 1));
												 * 
												 * System.out.println(
												 * "mean is: ");
												 * System.out.println(
												 * sma_indicator.getValue(
												 * mean_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * ().get(kk.getKey())));
												 * 
												 ***/

												assertTrue("standard deviation as per expectation on 400",
														stddev_indicator
																.getValue(stddev_tm_indicator
																		.getTick_to_timeseries_index_map()
																		.get(kk.getKey()))
																.toString().substring(0, 15).equals("0.8144459602835"));

												assertTrue("mean as per expectation on 400", sma_indicator
														.getValue(mean_tm_indicator.getTick_to_timeseries_index_map()
																.get(kk.getKey()))
														.toString().substring(0, 15).equals("-0.003333333333"));
											} else if (counter.equals(419)) {

												/***
												 * 
												 * System.out.println(
												 * "the trimmed standard deviation"
												 * );
												 * 
												 * System.out .println(
												 * stddev_indicator
												 * .getValue(stddev_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * () .get(kk.getKey()))
												 * .toString().substring(0,
												 * 15));
												 * 
												 * System.out.println(
												 * "mean trimmed string is:");
												 * 
												 * System.out .println(
												 * sma_indicator
												 * .getValue(mean_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * () .get(kk.getKey()))
												 * .toString().substring(0,
												 * 15));
												 * 
												 * System.out.println(
												 * "trimmed string on 419");
												 * 
												 * System.out .println(
												 * sma_indicator
												 * .getValue(mean_tm_indicator
												 * .getTick_to_timeseries_index_map
												 * () .get(kk.getKey()))
												 * .toString().substring(0,
												 * 15));
												 * 
												 ***/

												assertTrue("standard deviation as per expectation on 419",
														stddev_indicator
																.getValue(stddev_tm_indicator
																		.getTick_to_timeseries_index_map()
																		.get(kk.getKey()))
																.toString().substring(0, 15).equals("0.8164693639214"));

												assertTrue("mean as per expectation on 419", sma_indicator
														.getValue(mean_tm_indicator.getTick_to_timeseries_index_map()
																.get(kk.getKey()))
														.toString().substring(0, 15).equals("-0.006666666666"));
											}
										}

										/****
										 * System.out.println(counter + "," +
										 * kk.getValue().
										 * getIndicator_integer_value() + "," +
										 * kk.getKey() + "," +
										 * sma_indicator.getValue(
										 * mean_tm_indicator
										 * .getTick_to_timeseries_index_map().
										 * get(kk.getKey())) + "," +
										 * stddev_indicator.getValue(
										 * stddev_tm_indicator
										 * .getTick_to_timeseries_index_map().
										 * get(kk.getKey())));
										 ***/

										counter++;

									}

									// please print out the mean on day id 28.

									// System.exit(1);
								}

								for (Integer day_id : e.getPrice_map_internal().keySet()) {

									// if the day_id - period exists
									// and if the inidicator is there on that
									// day id
									// then the entry should be there, otherwise
									// not
									if (e.getPrice_map_internal().containsKey(day_id - period)) {

										// also check if it contains the
										// indicator for both days.

										if (e.getPrice_map_internal().get(day_id).getIndicator_values_on_datapoint()
												.containsKey(indicator_cum_period_name)
												&& e.getPrice_map_internal().get(day_id - period)
														.getIndicator_values_on_datapoint()
														.containsKey(indicator_cum_period_name)) {
											// System.out.println(
											// "the day id is:" + day_id + " and
											// the period is:" + period);
											// System.out.println("day id period
											// is:" + (day_id - period));
											// System.out
											// .println("indicator cum period
											// name:" +
											// indicator_cum_period_name);
											assertTrue("entry for this period for this indicator",
													idp.containsKey(day_id));
										}

										else {
											assertFalse("no entry for this period for this indicator",
													idp.containsKey(day_id));
										}
									} else {
										assertFalse("no entry for this period for this indicator",
												idp.containsKey(day_id));
									}

								}

							}

						}

					}

				}

				/// expect it to have the required complex on the required day
				/// id.

				HashMap<Integer, List<Complex>> results = indicator_value_change_std_dev_subindicator.getResults();

				List<Complex> day_n_complex = results.get(49);
				HashMap<String, String> all_arrays = new HashMap<String, String>();
				for (Complex c : day_n_complex) {
					// if the indicator cum period name is the one with close.
					// then we should have this.
					if (c.getIndicator_cum_period_name().equals("close_period_start_1_period_end")) {
						// then we should get
						// close_1_close
						// for this day id,
						// there can be more than one.
						all_arrays.put(c.getSubindicator_name(), c.getIndicator_cum_period_name());
					}
				}

				// expect all arrays to contain, that one.
				assertTrue("the standard deviation has been detected",
						all_arrays.containsKey("3 day change rises by 1 standard deviation"));
				System.out.println("came past assert");
			}
		}

	}

	/****
	 * used by the test_time_based_subindicators function only.
	 * 
	 * 
	 * 
	 * @param key
	 * @param day_id
	 * @param hm
	 */
	public void add_to_map(String key, Integer day_id, HashMap<String, ArrayList<Integer>> hm) {
		if (!hm.containsKey(key)) {
			hm.put(key, new ArrayList<Integer>());
		}
		hm.get(key).add(day_id);
	}

	// @Test
	public void test_time_based_subindicators() throws Exception {

		/***
		 * iterate all the datapoints from the centralsingleton
		 * 
		 * for each datapoint add to a hash which should have the following
		 * keys: the keys have to be the exact names of the complexes that we
		 * create.
		 * 
		 * 
		 * "monday" => [array of day ids] "tuesday" "wednesday" "thursday"
		 * "friday" "saturday" "sunday" -------- "first monday of the month"
		 * "second monday" "third monday" "fourth monday" .. "sixth monday"
		 * "last monday"
		 * 
		 * to understand last -> if you go to a first weekday of anything, go to
		 * the last key, and add the day id of the first_weekday that you get -
		 * 7.
		 * 
		 * ------------- "1-55 monday of year" "1-55 tuesday of year"
		 * -----------------
		 * 
		 * 
		 * 
		 * ------------------------------------------- after we are done
		 * creating this hash. , run the timebasedsubindicators , then get the
		 * complexes generated, and from that generate a hash similar to the one
		 * above. the two should match for every key. this will test that all
		 * complexes are being correctly created.
		 * ========================================
		 * 
		 * how to test the subindicator id assignment.
		 * 
		 */
		HashMap<String, ArrayList<Integer>> expected_subindicators_hashmap = new HashMap<String, ArrayList<Integer>>();

		HashMap<String, Integer> expected_subindicator_names_to_ids = TImeBasedSubindicators
				.generate_expected_subindicators();
		;

		CentralSingleton.initializeSingleton();

		/****
		 * 
		 * we set this to 1 everytime a first of month is detected thereafter we
		 * increment it by 1, everytime that day id is detected.
		 * 
		 */
		ConcurrentHashMap<String, Integer> day_of_month_counts = new ConcurrentHashMap<String, Integer>() {
			{
				put("Monday", 0);
				put("Tuesday", 0);
				put("Wednesday", 0);
				put("Thursday", 0);
				put("Friday", 0);
				put("Saturday", 0);
				put("Sunday", 0);
			}
		};

		HashMap<String, Integer> day_of_quarter_counts = new HashMap<String, Integer>() {
			{
				put("Monday", 0);
				put("Tuesday", 0);
				put("Wednesday", 0);
				put("Thursday", 0);
				put("Friday", 0);
				put("Saturday", 0);
				put("Sunday", 0);
			}
		};

		HashMap<String, Integer> day_of_year_counts = new HashMap<String, Integer>() {
			{
				put("Monday", 0);
				put("Tuesday", 0);
				put("Wednesday", 0);
				put("Thursday", 0);
				put("Friday", 0);
				put("Saturday", 0);
				put("Sunday", 0);
			}
		};

		Boolean process = false;
		for (Map.Entry<Integer, DataPoint> entry : CentralSingleton.getInstance().gmt_day_id_to_dataPoint.entrySet()) {

			DataPoint dp = entry.getValue();
			Integer current_day_id = entry.getKey();
			// want to check that we are on the first of jan, before starting
			// our processing
			// because this way we will start on the first day of the year, so
			// all calculations will be simpler.
			if (dp.getDay_of_month() == 1 && dp.getMonth_of_year_integer() == 1) {
				process = true;
			}

			if (process) {

				/**
				 * day_of_week.
				 */
				add_to_map(dp.getDay_of_week(), current_day_id, expected_subindicators_hashmap);

				/**
				 * month of year
				 * 
				 */
				add_to_map(dp.getMonth_of_year(), current_day_id, expected_subindicators_hashmap);

				/***
				 * quarter of year
				 * 
				 * 
				 */
				add_to_map(dp.getQuarter_of_year() + " quarter", current_day_id, expected_subindicators_hashmap);

				/**
				 * year itself.
				 * 
				 * 
				 */
				add_to_map(dp.getYear().toString(), current_day_id, expected_subindicators_hashmap);

				/**
				 * first -> last monday of the month
				 * 
				 * 
				 * 
				 * ---------------- if we get a first of the month -> then we
				 * can also check for quarter. if its a quarter month, then we
				 * can declare it as the first of the quarter,and start
				 * incremeting from there onwards
				 * 
				 * for last of quarter. the same 7 days ago, becomes last of
				 * quarter.
				 * 
				 * --------------- so when we get first of month, we also check
				 * year if its jan then declare as first of jan and start
				 * counting from there for last year, the same 7 days ago,
				 * becomes last of year.
				 * 
				 * 
				 */
				// if we minus seven days, do we get a different month?
				Integer seven_days_ago = current_day_id - 7;

				// first check if the day id even exists,
				if (CentralSingleton.gmt_day_id_to_dataPoint.containsKey(seven_days_ago)) {
					if (!CentralSingleton.gmt_day_id_to_dataPoint.get(seven_days_ago).getMonth_of_year()
							.equals(dp.getMonth_of_year())) {

						// this is only true for the week.
						// it doesnt mean that that is for the year
						// for everytime the month changes
						// i am treating it as a first of the year
						// that is not right, unless it is january.

						// it means that this is the first of the month.
						add_to_map("first " + dp.getDay_of_week() + " of the month", current_day_id,
								expected_subindicators_hashmap);

						// IF THIS IS FIRST, THEN THE 7 DAYS AGO, SHOULD BE
						// ADDED TO LAST

						add_to_map("last " + dp.getDay_of_week() + " of the month", seven_days_ago,
								expected_subindicators_hashmap);

						day_of_month_counts.put(dp.getDay_of_week(), 1);

						/***
						 * process for quarter
						 * 
						 */
						if (dp.getMonth_of_year_integer() == 1 || dp.getMonth_of_year_integer() == 4
								|| dp.getMonth_of_year_integer() == 7 || dp.getMonth_of_year_integer() == 10) {
							day_of_quarter_counts.put(dp.getDay_of_week(), 1);

							// it is the first for quarter
							// and the previous is last for quarter
							// for the name we have to do the ordinal shit.
							add_to_map(
									TImeBasedSubindicators.ordinal(1) + " " + dp.getDay_of_week() + " of the quarter",
									current_day_id, expected_subindicators_hashmap);

							// IF THIS IS FIRST, THEN THE 7 DAYS AGO, SHOULD BE
							// ADDED TO LAST

							add_to_map("last " + dp.getDay_of_week() + " of the quarter", seven_days_ago,
									expected_subindicators_hashmap);

						} else {
							Integer current_day_of_quarter_count = day_of_quarter_counts.get(dp.getDay_of_week());

							current_day_of_quarter_count = current_day_of_quarter_count + 1;
							day_of_quarter_counts.put(dp.getDay_of_week(), current_day_of_quarter_count);

							add_to_map(
									TImeBasedSubindicators.ordinal(current_day_of_quarter_count) + " "
											+ dp.getDay_of_week() + " of the quarter",
									current_day_id, expected_subindicators_hashmap);
						}

						/**
						 * 
						 * process for year
						 * 
						 */
						if (dp.getMonth_of_year_integer() == 1) {
							// if the month is 1 and this is the first time this
							// day was encountered, then it is the first for the
							// year

							day_of_year_counts.put(dp.getDay_of_week(), 1);
							// it is the first for quarter
							// and the previous is last for quarter
							// for the name we have to do the ordinal shit.
							add_to_map(TImeBasedSubindicators.ordinal(1) + " " + dp.getDay_of_week() + " of the year",
									current_day_id, expected_subindicators_hashmap);

							// IF THIS IS FIRST, THEN THE 7 DAYS AGO, SHOULD BE
							// ADDED TO LAST
							add_to_map("last " + dp.getDay_of_week() + " of the year", seven_days_ago,
									expected_subindicators_hashmap);

						} else {
							Integer current_day_of_year_count = day_of_year_counts.get(dp.getDay_of_week());
							current_day_of_year_count++;
							day_of_year_counts.put(dp.getDay_of_week(), current_day_of_year_count);

							add_to_map(
									TImeBasedSubindicators.ordinal(current_day_of_year_count) + " "
											+ dp.getDay_of_week() + " of the year",
									current_day_id, expected_subindicators_hashmap);
						}

					} else {
						// we are inside the same month.
						Integer current_day_of_month_count = day_of_month_counts.get(dp.getDay_of_week());
						current_day_of_month_count++;
						day_of_month_counts.put(dp.getDay_of_week(), current_day_of_month_count);
						// get the conversion
						if (current_day_of_month_count == 2) {
							add_to_map("second " + dp.getDay_of_week() + " of the month", current_day_id,
									expected_subindicators_hashmap);
						} else if (current_day_of_month_count == 3) {
							add_to_map("third " + dp.getDay_of_week() + " of the month", current_day_id,
									expected_subindicators_hashmap);

						} else if (current_day_of_month_count == 4) {
							add_to_map("fourth " + dp.getDay_of_week() + " of the month", current_day_id,
									expected_subindicators_hashmap);

						} else if (current_day_of_month_count == 5) {
							add_to_map("fifth " + dp.getDay_of_week() + " of the month", current_day_id,
									expected_subindicators_hashmap);

						} else if (current_day_of_month_count == 6) {
							add_to_map("sixth " + dp.getDay_of_week() + " of the month", current_day_id,
									expected_subindicators_hashmap);

						}

						/****
						 * 
						 * now for the quarter
						 * 
						 */

						// first increment the counts
						// then use the ordinal
						Integer current_day_of_quarter_count = day_of_quarter_counts.get(dp.getDay_of_week());

						current_day_of_quarter_count = current_day_of_quarter_count + 1;
						day_of_quarter_counts.put(dp.getDay_of_week(), current_day_of_quarter_count);

						add_to_map(TImeBasedSubindicators.ordinal(current_day_of_quarter_count) + " "
								+ dp.getDay_of_week() + " of the quarter", current_day_id,
								expected_subindicators_hashmap);

						/*****
						 * 
						 * now for the year
						 * 
						 * 
						 */
						Integer current_day_of_year_count = day_of_year_counts.get(dp.getDay_of_week());
						current_day_of_year_count++;
						day_of_year_counts.put(dp.getDay_of_week(), current_day_of_year_count);

						add_to_map(TImeBasedSubindicators.ordinal(current_day_of_year_count) + " " + dp.getDay_of_week()
								+ " of the year", current_day_id, expected_subindicators_hashmap);
					}

				}

			}

		}

		// so now the idea is to cross check this against the complexes that we
		// get
		// by running the timebased subindicators.
		TImeBasedSubindicators ts = new TImeBasedSubindicators();
		ts.calculate(null);

		HashMap<String, ArrayList<Integer>> generated_complexes_hashmap = new HashMap<String, ArrayList<Integer>>();
		HashMap<String, String> generated_complexes_ids = new HashMap<String, String>();
		/***
		 * now iterate the complexes that were generated and build a similar map
		 * from them. get the subindicator
		 * 
		 */
		// System.out.println("the size of the results is:" +
		// ts.getResults().size());

		for (Map.Entry<Integer, List<Complex>> complex_entry : ts.getResults().entrySet()) {
			Integer day_id = complex_entry.getKey();
			// System.out.println("day id is:" + day_id);
			for (Complex c : complex_entry.getValue()) {
				// System.out.println("complex subindicator name is: " +
				// c.getSubindicator_name());
				if (generated_complexes_hashmap.containsKey(c.getSubindicator_name())) {
					// System.out.println("key was already there");
					generated_complexes_hashmap.get(c.getSubindicator_name()).add(day_id);
				} else {
					// System.out.println("put for the first time.");
					generated_complexes_hashmap.put(c.getSubindicator_name(),
							new ArrayList<Integer>(Arrays.asList(day_id)));
					generated_complexes_ids.put(c.getSubindicator_name(), c.getSubindicator_id());
				}
			}
		}

		// System.out.println(generated_complexes_hashmap.keySet());

		/***
		 * now compare everything in the expected map with the generated map.
		 * 
		 */
		for (Map.Entry<String, ArrayList<Integer>> expected_entry : expected_subindicators_hashmap.entrySet()) {

			String subindicator_name = expected_entry.getKey();

			System.out.println("testing subindicator name:" + subindicator_name);

			assertTrue(subindicator_name + " exists ", generated_complexes_hashmap.containsKey(subindicator_name));

			ArrayList<Integer> generated_complex_day_ids = generated_complexes_hashmap.get(subindicator_name);

			for (Integer day_id_ : expected_entry.getValue()) {

				System.out.println("subindicator exists is:" + subindicator_name + " testing on day:" + day_id_);

				System.out.println("the datestring on that day is:");

				System.out.println(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.get(day_id_).getDateString());

				assertTrue(day_id_.toString() + " exists", generated_complex_day_ids.contains(day_id_));

			}

			// now check that the generated id matches for the generated name.
			// check that the complex id is the same as
			String expected_id = SubIndicatorCalculation.subindicator_prefix_map.get("TimeBasedSubindicators")
					+ expected_subindicator_names_to_ids.get(subindicator_name).toString();
			assertTrue("the subindicator name" + subindicator_name + " is not null:",
					expected_subindicator_names_to_ids.containsKey(subindicator_name));
			System.out.println("subindicator name is:" + subindicator_name);
			assertTrue("the id:" + expected_id + " and  " + generated_complexes_ids.get(subindicator_name)
					+ "  are equal ", expected_id.equals(generated_complexes_ids.get(subindicator_name)));

		}

	}

	/***
	 * will return "week", "month", "year", "quarter" will return null if its
	 * none of these.
	 * 
	 * if the given datapoint is either "first" or "last" trading time
	 * unit(week,month,year,quarter)
	 * 
	 * 
	 * 
	 * you have to pass in "first" or "last" , depending on what your'e looking
	 * for.
	 * 
	 * @param dp
	 * @param e
	 * @param day_id
	 * @param first_or_last
	 * @return
	 */
	private ArrayList<String> is_trading_day(DataPoint dp, Entity e, Integer day_id, String first_or_last) {

		Entry<Integer, DataPoint> compared_ent = null;

		// if the previous datapoint is of another week of the year.
		if (first_or_last.equals("first")) {
			// System.out.println("went for lower entry");
			compared_ent = e.getPrice_map_internal().lowerEntry(day_id);
		} else {
			// System.out.println("went for higher entry");
			compared_ent = e.getPrice_map_internal().higherEntry(day_id);
		}

		ArrayList<String> ret_values = new ArrayList<String>();

		if (compared_ent != null) {
			DataPoint compared_dp = compared_ent.getValue();

			if (!compared_dp.getWeek_of_year().equals(dp.getWeek_of_year())) {

				ret_values.add("week");
			}
			if (!compared_dp.getMonth_of_year().equals(dp.getMonth_of_year())) {
				ret_values.add("month");
			}
			if (!compared_dp.getQuarter_of_year().equals(dp.getQuarter_of_year())) {
				ret_values.add("quarter");
			}
			if (!compared_dp.getYear().equals(dp.getYear())) {
				ret_values.add("year");
			}
		}

		// add for 2010-05-17 : that it is the first day of the week
		if (first_or_last.equals("first") && dp.getDateString().equals("2010-05-17")) {
			ret_values.add("week");
		}

		return ret_values;
	}

	/****
	 * used in for the week month year specs only.
	 * 
	 * @param day_id
	 * @param entity_name
	 * @param exp_subindicators_hashmap
	 * @param expected_subindicators
	 */
	private void add_expected_subindicators(Integer day_id, String entity_name,
			HashMap<String, TreeMap<Integer, ArrayList<String>>> exp_subindicators_hashmap,
			ArrayList<String> expected_subindicators) {

		if (!exp_subindicators_hashmap.containsKey(entity_name)) {
			exp_subindicators_hashmap.put(entity_name, new TreeMap<Integer, ArrayList<String>>());
		}

		if (exp_subindicators_hashmap.get(entity_name).containsKey(day_id)) {
			exp_subindicators_hashmap.get(entity_name).get(day_id).addAll(expected_subindicators);
		} else {
			exp_subindicators_hashmap.get(entity_name).put(day_id, expected_subindicators);
		}
	}

	/****
	 * this test makes the first trading day of a time unit close value as 2.00
	 * then it sees if that day is also the last trading day of something, if
	 * yes, then it skips that day while testing. if no, then it continues to
	 * the next day.
	 * 
	 * if on the other hand a day is only the last trading day of something,
	 * then it sets that to 0.10
	 * 
	 * so basically we only test:
	 * 
	 * first trading day -> last_trading_day (falls for the
	 * (week/month/quarter/year))
	 * 
	 * 
	 * the test also simultaneously tests, for detection of subindicators like
	 * "first_trading_day_of..." and "last_trading_day_of..."
	 * 
	 * read additional comments inside the code.
	 * 
	 * 
	 * 
	 * @throws Exception
	 * 
	 */
	// @Test
	public void for_the_week_month_year_tests() throws Exception {
		HashMap<String, String> subindicator_ids_map = new HashMap<String, String>() {
			{

				put("first trading day of the week", "0");
				put("first trading day of the month", "1");
				put("first trading day of the quarter", "2");
				put("first trading day of the year", "3");

				put("last trading day of the week", "4");
				put("last trading day of the month", "5");
				put("last trading day of the quarter", "6");
				put("last trading day of the year", "7");

				put("rises for the week", "8");
				put("rises for the month", "9");
				put("rises for the quarter", "10");
				put("rises for the year", "11");

				put("falls for the week", "12");
				put("falls for the month", "13");
				put("falls for the quarter", "14");
				put("falls for the year", "15");

			}
		};
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		HashMap<String, TreeMap<Integer, ArrayList<String>>> expected_subindicators = new HashMap<String, TreeMap<Integer, ArrayList<String>>>();
		HashMap<String, ArrayList<Integer>> days_to_skip_while_testing_for_entity = new HashMap<String, ArrayList<Integer>>();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				expected_subindicators.put(entity_name, new TreeMap<Integer, ArrayList<String>>());
				days_to_skip_while_testing_for_entity.put(e.getUnique_name(), new ArrayList<Integer>());
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					DataPoint dp = e.getPrice_map_internal().get(day_id);

					/***
					 * diagnose if it is the first trading day of the week.
					 * 
					 */
					ArrayList<String> first_day_of = is_trading_day(dp, e, day_id, "first");
					ArrayList<String> last_day_of = is_trading_day(dp, e, day_id, "last");

					/****
					 * 
					 * if something is the first day of anything, make its value
					 * 2.00
					 * 
					 * 
					 * 
					 */
					if (first_day_of != null) {

						for (String t : first_day_of) {
							if (t.equals("week")) {
								/// make it 1
								e.getPrice_map_internal().get(day_id).setClose("2.00");
								add_expected_subindicators(day_id, entity_name, expected_subindicators,
										new ArrayList<String>(Arrays.asList("first trading day of the week")));
							} else if (t.equals("month")) {
								/// make it 2
								e.getPrice_map_internal().get(day_id).setClose("2.00");
								add_expected_subindicators(day_id, entity_name, expected_subindicators,
										new ArrayList<String>(Arrays.asList("first trading day of the month")));
								// e.getPrice_map_internal().get(day_id).setClose("2.00");
							} else if (t.equals("quarter")) {
								// make it 1
								e.getPrice_map_internal().get(day_id).setClose("2.00");
								add_expected_subindicators(day_id, entity_name, expected_subindicators,
										new ArrayList<String>(Arrays.asList("first trading day of the quarter")));
								// e.getPrice_map_internal().get(day_id).setClose("2.00");
							} else if (t.equals("year")) {
								// make it 2
								e.getPrice_map_internal().get(day_id).setClose("2.00");
								add_expected_subindicators(day_id, entity_name, expected_subindicators,
										new ArrayList<String>(Arrays.asList("first trading day of the year")));
								/// e.getPrice_map_internal().get(day_id).setClose("2.00");
							}
						}
					}

					/***
					 * any day which is both the first trading day of somethin g
					 * and the last trading day of something else, is skipped
					 * while testing
					 * 
					 * this is because in the above block we set a day id close
					 * as 2.00 if its the first trading day of the week
					 * 
					 * now suppose its also the last trading day of the month.
					 * then it will be reset to 0.10 below.
					 * 
					 * as a result, when we test , we will expect it to actually
					 * fall for the month, but it will instead show a rise.
					 * 
					 * 
					 * hence such day ids are added to skip ids.
					 * 
					 * 
					 * 
					 * 
					 */
					if (!first_day_of.isEmpty() && !last_day_of.isEmpty()) {
						days_to_skip_while_testing_for_entity.get(e.getUnique_name()).add(day_id);
					}

					/**************
					 * 
					 * FOR LAST TRADING DAY. only set the last trading day
					 * values if the first trading day is empty.
					 * 
					 * otherwise let the last trading values be whatever they
					 * are
					 * 
					 * those day_ids, will be skipped while doing the test
					 * 
					 * So if a given day id is only a last_trading day for any
					 * number of time units , then it will be set to 0.10
					 * 
					 * so the test actually on tests for
					 * "falls for the ...time_unit"
					 * 
					 * 
					 **************/
					if (first_day_of.isEmpty()) {
						if (last_day_of != null) {
							// the thing will always fall for the last day.

							for (String t : last_day_of) {
								if (t.equals("week")) {
									/// make it 1
									// expect falls for the week.
									e.getPrice_map_internal().get(day_id).setClose("0.10");
									add_expected_subindicators(day_id, entity_name, expected_subindicators,
											new ArrayList<String>(Arrays.asList("last trading day of the week",
													"falls for the week")));
									// e.getPrice_map_internal().get(day_id).setClose("0.10");
								} else if (t.equals("month")) {
									/// make it 2
									// expect falls for the month.
									e.getPrice_map_internal().get(day_id).setClose("0.10");
									add_expected_subindicators(day_id, entity_name, expected_subindicators,
											new ArrayList<String>(Arrays.asList("last trading day of the month",
													"falls for the month")));
									// e.getPrice_map_internal().get(day_id).setClose("1.00");
								} else if (t.equals("quarter")) {
									// make it 1
									// expect doesnt change for the quarter
									e.getPrice_map_internal().get(day_id).setClose("0.10");
									add_expected_subindicators(day_id, entity_name, expected_subindicators,
											new ArrayList<String>(Arrays.asList("last trading day of the quarter",
													"falls for the quarter")));
									// e.getPrice_map_internal().get(day_id).setClose("1.00");
								} else if (t.equals("year")) {
									// make it 2
									// expect rises for the year
									e.getPrice_map_internal().get(day_id).setClose("0.10");
									add_expected_subindicators(day_id, entity_name, expected_subindicators,
											new ArrayList<String>(Arrays.asList("last trading day of the year",
													"falls for the year")));
									// e.getPrice_map_internal().get(day_id).setClose("4.00");
								}
							}
						}
					}
				}
			}
		}

		/***
		 *
		 */

		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {

				index_entry.getValue().calculate_entity_index_indicators();
				index_entry.getValue().before_calculate_entity_index_complexes();
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				ForTheMonthWeekYearQuarter fw = new ForTheMonthWeekYearQuarter();
				fw.calculate(e);

				HashMap<Integer, List<Complex>> results = fw.getResults();

				TreeMap<Integer, ArrayList<String>> expected_subindicators_for_entity = expected_subindicators
						.get(entity_name);

				for (Map.Entry<Integer, ArrayList<String>> entity_entry_s : expected_subindicators_for_entity
						.entrySet()) {
					Integer day_id_for_s = entity_entry_s.getKey();
					ArrayList<String> subindicators_expected_for_close = entity_entry_s.getValue();

					ArrayList<String> subindicators_calculated = new ArrayList<String>();
					ArrayList<String> subindicators_calculated_ids = new ArrayList<String>();
					/**
					 * // these days are skipped why? // we start on 2010-05-17
					 * // so for the first day, it is actually also the first
					 * day of the week // but because we dont have a previous
					 * dp, it wont be calcualted // so we ignore that while
					 * testing(day id 0) // then comes 14 -> this one is the
					 * last day of the month, but // because we dont have the
					 * first day of the month, so we dont get // that , so we
					 * ignore it. // then 44 -> this one is the last day of the
					 * quarter -> same reason // 228 -> last day of year -> same
					 * reason.
					 ***/
					ArrayList<Integer> day_ids_to_skip = new ArrayList<Integer>(Arrays.asList(0, 14, 44, 228));

					day_ids_to_skip.addAll(days_to_skip_while_testing_for_entity.get(e.getUnique_name()));

					if (!day_ids_to_skip.contains(day_id_for_s)) {

						for (Complex c : results.get(day_id_for_s)) {
							/***
							 * only add those subindicators which are detected
							 * for close -> since we have only modified close.
							 */
							if (c.getIndicator_cum_period_name().equals("close_period_start_1_period_end")) {
								subindicators_calculated.add(c.getSubindicator_name());
								subindicators_calculated_ids.add(c.getSubindicator_id());
							}
						}

						assertTrue("day id exists", results.containsKey(day_id_for_s));

						if (subindicators_calculated.size() > 0) {
							for (String s : subindicators_expected_for_close) {
								System.out.println("checking subinidcator: " + s + " on day id:" + day_id_for_s);

								assertTrue("subindicator " + s + " exists on day id: " + day_id_for_s,
										subindicators_calculated.contains(s));

								Integer index_of_subindicator_in_array = subindicators_calculated.indexOf(s);
								String calculated_subindicator_id = subindicators_calculated_ids
										.get(index_of_subindicator_in_array);

								String expected_id_of_subindicator = SubIndicatorCalculation.subindicator_prefix_map
										.get("ForTheMonthWeekYearQuarter") + subindicator_ids_map.get(s);

								assertTrue("testing id : " + expected_id_of_subindicator,
										calculated_subindicator_id.equals(expected_id_of_subindicator));

							}

						}

						assertTrue("the sizes are the same",
								(subindicators_calculated.size() == subindicators_expected_for_close.size()));
					}

				}
			}
		}

	}

}
