package com.tradegenie.tests;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;


/***
 * A Singleton that can be used in the lifetime of tests to hold values.
 * It also has a hashmap<string,object> called expected values to store arbitary data.
 * @author bhargav
 *
 */
public class TestSingleton {
	
	

	private AtomicInteger counter;
	
	public AtomicInteger getCounter() {
		return counter;
	}

	public void setCounter(AtomicInteger counter) {
		this.counter = counter;
	}

	public static TestSingleton tsingle;
	public static TestSingleton getTsingle() {
		return tsingle;
	}

	public static void setTsingle(TestSingleton tsingle) {
		TestSingleton.tsingle = tsingle;
	}

	public static TestSingleton initialize_test_singleton(){
		if(tsingle == null){
			tsingle = new TestSingleton();
			tsingle.setTest_expected_values_map(new HashMap<String,TestSingletonValue>());
			tsingle.setCounter(new AtomicInteger(0));
	
		}
		return tsingle;
	}
	
	private HashMap<String,TestSingletonValue> test_expected_values_map;
	
	public HashMap<String, TestSingletonValue> getTest_expected_values_map() {
		return test_expected_values_map;
	}

	public void setTest_expected_values_map(
			HashMap<String, TestSingletonValue> test_expected_values_map) {
		this.test_expected_values_map = test_expected_values_map;
	}
	
	public synchronized void put_if_absent(TestSingletonValue v){
		if(getTest_expected_values_map().containsKey(v.getName())){
			
		}
		else{
			getTest_expected_values_map().put(v.getName(), v);
		}
	}
	
	public synchronized TestSingletonValue get_singleton_value(TestSingletonValue v){
		put_if_absent(v);
		return getTest_expected_values_map().get(v.getName());
	}
	
	public synchronized TestSingletonValue put_singleton_value(TestSingletonValue v){
		getTest_expected_values_map().put(v.getName(), v);
		return v;
	}

	public TestSingleton(){
		this.test_expected_values_map = new HashMap<String, TestSingletonValue>();
	}
	
	public static synchronized void destroySingleton() throws Exception {
		tsingle = null;
	}
}
