package com.tradegenie.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.PairArray;
import ExchangeBuilder.PercentageIntegerCorrelate;

public class TestSingletonValue {

	/***
	 * for trailing stop losses the day id where we need to check the simulation
	 * is one day after the day id where the price change is done
	 * key -> trailing stop loss amount
	 * value -> day id
	 */
	private TreeMap<Integer, Integer> day_ids_where_dummy_trailing_stop_losses_were_simulated;

	public TreeMap<Integer, Integer> getDay_ids_where_dummy_trailing_stop_losses_were_simulated() {
		return day_ids_where_dummy_trailing_stop_losses_were_simulated;
	}

	public void setDay_ids_where_dummy_trailing_stop_losses_were_simulated(
			TreeMap<Integer, Integer> day_ids_where_dummy_trailing_stop_losses_were_simulated) {
		this.day_ids_where_dummy_trailing_stop_losses_were_simulated = day_ids_where_dummy_trailing_stop_losses_were_simulated;
	}

	/***
	 * for the stat stop loss test need to store at which day id we have stored
	 * a difference in price as equal to the requisite stop loss.
	 * 
	 * key -> stop_loss_amount[-5,-3,-2,2,3,5] value -> day id where the close
	 * price was made equal to this amount, so that a stop loss should be
	 * triggered relative to this end_day_id, for any start_day_id less than it.
	 * 
	 */
	private TreeMap<Integer, Integer> day_ids_where_dummy_stat_stop_losses_were_simulated;

	public TreeMap<Integer, Integer> getDay_ids_where_dummy_stat_stop_losses_were_simulated() {
		return day_ids_where_dummy_stat_stop_losses_were_simulated;
	}

	public void setDay_ids_where_dummy_stat_stop_losses_were_simulated(
			TreeMap<Integer, Integer> day_ids_where_dummy_stat_stop_losses_were_simulated) {
		this.day_ids_where_dummy_stat_stop_losses_were_simulated = day_ids_where_dummy_stat_stop_losses_were_simulated;
	}

	private AtomicInteger datapoints_invalidated_randomly;

	public AtomicInteger getdatapoints_invalidated_randomly() {
		return datapoints_invalidated_randomly;
	}

	public void setdatapoints_invalidated_randomly(AtomicInteger counter) {
		this.datapoints_invalidated_randomly = counter;
	}

	private AtomicInteger total_datapoints_encountered;

	public AtomicInteger getTotal_datapoints_encountered() {
		return total_datapoints_encountered;
	}

	public void setTotal_datapoints_encountered(AtomicInteger total_datapoints_encountered) {
		this.total_datapoints_encountered = total_datapoints_encountered;
	}

	public ArrayList<String> methods_triggered;

	public ArrayList<String> getMethods_triggered() {
		return methods_triggered;
	}

	public void setMethods_triggered(ArrayList<String> methods_triggered) {
		this.methods_triggered = methods_triggered;
	}

	private Boolean request_made;

	public Boolean getRequest_made() {
		return request_made;
	}

	public void setRequest_made(Boolean request_made) {
		this.request_made = request_made;
	}

	private String response_string;
	private Integer response_code;
	private String request_start_date;
	private String request_end_date;
	private Boolean entity_es_linked_list_empty;
	private String entity_unique_name;

	/***
	 * the following are used in the stoploss tests.
	 */

	private Integer start_day_id;

	public Integer getStart_day_id() {
		return start_day_id;
	}

	public void setStart_day_id(Integer start_day_id) {
		this.start_day_id = start_day_id;
	}

	private Integer end_day_id;

	public Integer getEnd_day_id() {
		return end_day_id;
	}

	public void setEnd_day_id(Integer end_day_id) {
		this.end_day_id = end_day_id;
	}

	private DataPoint price_map_in_range_start_day_datapoint;

	public DataPoint getPrice_map_in_range_start_day_datapoint() {
		return price_map_in_range_start_day_datapoint;
	}

	public void setPrice_map_in_range_start_day_datapoint(DataPoint price_map_in_range_start_day_datapoint) {
		this.price_map_in_range_start_day_datapoint = price_map_in_range_start_day_datapoint;
	}

	public DataPoint getPrice_map_in_range_end_day_datapoint() {
		return price_map_in_range_end_day_datapoint;
	}

	public void setPrice_map_in_range_end_day_datapoint(DataPoint price_map_in_range_end_day_datapoint) {
		this.price_map_in_range_end_day_datapoint = price_map_in_range_end_day_datapoint;
	}

	private DataPoint price_map_in_range_end_day_datapoint;
	private PairArray p;
	private Integer price_map_in_range_size;

	public Integer getPrice_map_in_range_size() {
		return price_map_in_range_size;
	}

	public void setPrice_map_in_range_size(Integer price_map_in_range_size) {
		this.price_map_in_range_size = price_map_in_range_size;
	}

	public PairArray getP() {
		return p;
	}

	public void setP(PairArray p) {
		this.p = p;
	}

	private PercentageIntegerCorrelate pc_correlate;

	public PercentageIntegerCorrelate getPc_correlate() {
		return pc_correlate;
	}

	public void setPc_correlate(PercentageIntegerCorrelate pc_correlate) {
		this.pc_correlate = pc_correlate;
	}

	/***
	 * end the variables used in teh stop loss tests.
	 * 
	 * @return
	 */

	public String getName() {
		return entity_unique_name;
	}

	public void setName(String name) {
		this.entity_unique_name = name;
	}

	public Boolean getEntity_es_linked_list_empty() {
		return entity_es_linked_list_empty;
	}

	public void setEntity_es_linked_list_empty(Boolean entity_es_linked_list_empty) {
		this.entity_es_linked_list_empty = entity_es_linked_list_empty;
	}

	private Entity e;

	public String getResponse_string() {
		return response_string;
	}

	public void setResponse_string(String response_string) {
		this.response_string = response_string;
	}

	public Integer getResponse_code() {
		return response_code;
	}

	public void setResponse_code(Integer response_code) {
		this.response_code = response_code;
	}

	public String getRequest_start_date() {
		return request_start_date;
	}

	public void setRequest_start_date(String request_start_date) {
		this.request_start_date = request_start_date;
	}

	public String getRequest_end_date() {
		return request_end_date;
	}

	public void setRequest_end_date(String request_end_date) {
		this.request_end_date = request_end_date;
	}

	public Entity getE() {
		return e;
	}

	public void setE(Entity e) {
		this.e = e;
	}

	public TestSingletonValue(Entity e) {
		this.e = e;
		this.entity_unique_name = e.getUnique_name();
		this.methods_triggered = new ArrayList<String>();
		this.datapoints_invalidated_randomly = new AtomicInteger();
		this.total_datapoints_encountered = new AtomicInteger();
		this.day_ids_where_dummy_stat_stop_losses_were_simulated = new TreeMap<Integer, Integer>();
		this.day_ids_where_dummy_trailing_stop_losses_were_simulated = new TreeMap<Integer, Integer>();
	}

	@Override
	public String toString() {
		try {
			String pretty_print = CentralSingleton.getInstance().objmapper.defaultPrettyPrintingWriter()
					.writeValueAsString(this);
			return pretty_print;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

}
