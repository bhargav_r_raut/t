package com.tradegenie.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.script.ScriptService.ScriptType;
import org.junit.Test;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.EntityIndex;
import elasticSearchNative.RemoteEs;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;
import subindicators.SubIndicatorCalculation;
import yahoo_finance_historical_scraper.StockConstants;

public class UpdateBSArrayTests extends DownloadEntityExchangeTests {

	private static int[] get_decompressed_array_from_compressed_array(ArrayList<Integer> compressed_list,
			int expected_length_of_decompressed_array) {

		int[] compressed_arr = new int[compressed_list.size()];

		for (int i = 0; i < compressed_list.size(); i++) {
			compressed_arr[i] = compressed_list.get(i);
		}

		int[] recovered = new int[expected_length_of_decompressed_array + 1];

		// sorted
		IntegratedIntegerCODEC codec = new IntegratedComposition(new IntegratedBinaryPacking(),
				new IntegratedVariableByte());
		recovered = new int[expected_length_of_decompressed_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed_arr, new IntWrapper(0), compressed_arr.length, recovered, recoffset);
		// System.out
		// .println("invoking decompression of sorted array and returned \n");

		return recovered;

	}

	private UpdateResponse do_update_request(final ArrayList<Integer> arr, String complex_id) {
		UpdateRequest ureq = new UpdateRequest("tradegenie_titan", "bs", complex_id);
		ureq.script("update_bs_script", ScriptType.FILE, new HashMap<String, Object>() {
			{
				put("sparse_arr", ArrayUtils.toPrimitive(arr.toArray(new Integer[arr.size()])));
			}
		});
		ureq.scriptedUpsert(true);
		ureq.upsert(new HashMap<String, Object>());
		UpdateResponse uresp = RemoteEs.getInstance().update(ureq).actionGet();
		return uresp;
	}

	private ArrayList<Integer> unique_arraylist(ArrayList<Integer> arr) {

		Set<Integer> unique_buy_array = new HashSet<Integer>();

		if (arr != null) {

			for (int a : arr) {
				System.out.println("integer is:" + a);
				unique_buy_array.add(a);
			}

			ArrayList<Integer> reassembled_array = new ArrayList<Integer>(unique_buy_array);

			return reassembled_array;
		}

		return null;
	}

	private ArrayList<Integer> get_reassembled_array(String complex_id) {
		// now what kind of array should have been created?
		// that we have to evaluate.
		// so we need to be able to get the decompressed array back.
		GetResponse document = RemoteEs.getInstance().prepareGet("tradegenie_titan", "bs", complex_id)
				.setFetchSource(true).get();

		ArrayList<Integer> buy_array = (ArrayList<Integer>) document.getSource().get("buy");
		ArrayList<Integer> sell_array = (ArrayList<Integer>) document.getSource().get("sell");
		int decompressed_length = (int) document.getSource().get("decompressed_length");

		System.out.println("buy array is:");
		System.out.println(buy_array);

		System.out.println("sell array is:");
		System.out.println(sell_array);

		System.out.println("decompressed length is:" + decompressed_length);

		// okay so lets see if it decompresses to what we need.
		int[] decompressed_buy_array = get_decompressed_array_from_compressed_array(buy_array, decompressed_length);

		ArrayList<Integer> reassembled_array = new ArrayList<Integer>();
		for (int i = 0; i < decompressed_buy_array.length; i++) {
			if (!reassembled_array.contains(decompressed_buy_array[i])) {
				reassembled_array.add(decompressed_buy_array[i]);
			}
		}

		// = unique_arraylist(new
		// ArrayList(Arrays.asList(decompressed_buy_array)));

		return reassembled_array;
	}

	// @Test
	public void test_bug_in_zero_buy_arr() {
		ArrayList<Integer> arr = new ArrayList<Integer>(Arrays.asList(3, 7, 9, 11, 15, 17, 21, 23, 25, 29, 31, 35, 37,
				39, 43, 45, 49, 51, 53, 57, 59, 63, 65, 67, 71, 73, 77, 79, 81, 85, 87, 91, 93, 95, 99, 101, 105, 107,
				109, 113, 115, 119, 121, 123, 127, 129, 133, 135, 137, 141, 143, 147, 149, 151, 155, 157, 161, 163, 165,
				169, 171, 175, 177, 179, 183, 185, 189, 191, 193, 197, 199, 203, 205, 207, 211, 213, 217, 219, 221, 225,
				227, 231, 233, 235, 239, 241, 245, 247, 249, 253, 255, 259, 261, 263, 267, 269, 273, 275, 277, 281, 283,
				287, 289, 291, 295, 297, 301, 303, 305, 309, 311, 315, 317, 319, 323, 325, 329, 331, 333, 337, 339, 344,
				346, 350, 352, 354, 358, 360, 364, 366, 368, 372, 374, 378, 380, 382, 386, 388, 392, 394, 396, 400, 402,
				406, 408, 410, 414, 416, 420, 422, 424, 428, 430, 434, 436, 438, 442, 444, 448, 450, 452, 456, 458, 462,
				464, 466, 470, 472, 476, 478, 480, 484, 486, 490, 492, 494, 498, 500, 504, 506, 508, 512, 514, 518, 520,
				522, 526, 528, 532, 534, 536, 540, 542, 546, 548, 550, 554, 556, 560, 562, 564, 568, 570, 574, 576, 578,
				582, 584, 589, 591, 596, 598, 602, 604, 606, 610, 612, 616, 618, 620, 624, 626, 630, 632, 634, 638, 640,
				644, 646, 648, 652, 654, 658, 660, 662, 666, 668, 672, 674, 676, 680, 682, 686, 688, 693, 695, 697, 701,
				703, 707, 709, 711, 715, 717, 721, 723, 725, 729, 731, 735, 737, 739, 743, 745, 749, 751, 753, 757, 759,
				763, 765, 767, 771, 773, 777, 779, 781, 785, 787, 791, 793, 795, 799, 801, 805, 807, 809, 813, 815, 819,
				821, 823, 827, 829, 833, 835, 837, 841, 843, 847, 849, 851, 855, 857, 861, 863, 865, 869, 871, 875, 877,
				879, 883, 885, 889, 891, 893, 897, 899, 903, 905, 907, 911, 913, 917, 919, 921, 925, 927, 931, 933, 935,
				939, 941, 945, 947, 949, 954, 956, 961, 963, 967, 969, 973, 975, 977, 981, 983, 987, 989, 991, 995, 997,
				1001, 1003, 1005, 1009, 1011, 1015, 1017, 1019, 1023, 1025, 1029, 1031, 1033, 1037, 1039, 1043, 1045,
				1050, 1052, 1054, 1058, 1060, 1064, 1066, 1068, 1072, 1074, 1078, 1080, 1082, 1086, 1088, 1092, 1094,
				1096, 1100, 1102, 1106, 1108, 1110, 1114, 1116, 1120, 1122, 1124, 1128, 1130, 1134, 1136, 1138, 1142,
				1144, 1148, 1150, 1152, 1156, 1158, 1162, 1164, 1166, 1170, 1172, 1176, 1178, 1180, 1184, 1186, 1190,
				1192, 1194, 1198, 1200, 1204, 1206, 1208, 1212, 1214, 1218, 1220, 1222, 1226, 1228, 1232, 1234, 1236,
				1240, 1242, 1246, 1248, 1250, 1254, 1256, 1260, 1262, 1264, 1268, 1270, 1274, 1276, 1278, 1282, 1284,
				1288, 1290, 1292, 1296, 1298, 1302, 1304, 1306, 1310, 1312, 1316, 1320, 1324, 1327, 1331, 1333, 1337,
				1339, 1341, 1345, 1347, 1351, 1353, 1355, 1359, 1361, 1365, 1367, 1369, 1373, 1375, 1379, 1381, 1383,
				1387, 1389, 1393, 1395, 1397, 1401, 1403, 1407, 1409, 1411, 1415, 1417, 1421, 1423, 1425, 1429, 1431,
				1436, 1438, 1442, 1444, 1446, 1450, 1452, 1456, 1458, 1460, 1464, 1466, 1470, 1472, 1474, 1478, 1480,
				1484, 1486, 1488, 1492, 1494, 1498, 1500, 1502, 1506, 1508, 1512, 1514, 1516, 1520, 1522, 1526, 1528,
				1530, 1534, 1536, 1540, 1542, 1544, 1548, 1550, 1554, 1556, 1558, 1562, 1564, 1568, 1570, 1572, 1576,
				1578, 1582, 1584, 1586, 1590, 1592, 1596, 1598, 1600, 1604, 1606, 1610, 1612, 1614, 1618, 1620, 1624,
				1626, 1628, 1632, 1634, 1638, 1640, 1642, 1646, 1648, 1652, 1654, 1656, 1660, 1662, 1666, 1668, 1670,
				1674, 1676, 1680, 1682, 1687, 1689, 1694, 1696, 1698, 1702, 1704, 1708, 1710, 1712, 1716, 1718, 1722,
				1724, 1726, 1730, 1732, 1736, 1738, 1740, 1744, 1746, 1750, 1752, 1754, 1758, 1760, 1764, 1766, 1768,
				1772, 1774, 1778, 1780, 1782, 1786, 1788, 1792, 1794, 1796, 1800, 1802, 1806, 1808, 1810, 1814, 1816,
				1820, 1822, 1824, 1828, 1830, 1834, 1836, 1838, 1842, 1844, 1848, 1850, 1852, 1856, 1858, 1862, 1864,
				1866, 1870, 1872, 1876, 1878, 1880, 1884, 1886, 1890, 1892, 1894, 1898, 1900, 1904, 1906, 1908, 1912,
				1914, 1918, 1920, 1922, 1926, 1928, 1932, 1934, 1936, 1940, 1942, 1946, 1948, 1950, 1954, 1956, 1960,
				1962, 1964, 1968, 1970, 1974, 1976, 1978, 1982, 1984, 1988, 1990, 1992, 1996, 1998, 2002, 2004, 2006,
				2010, 2012, 2016, 2018, 2020, 2024, 2026, 2030, 2032, 2034, 2038, 2040, 2044, 2046, 2051, 2053, 2058,
				2060, 2062, 2066, 2068, 2072, 2074, 2076, 2080, 2082, 2086, 2088, 2090, 2094, 2096, 2100, 2102, 2104,
				2108, 2110, 2114, 2116, 2118, 2122, 2124, 2128, 2130, 2132, 2136, 2138, 2142, 2144, 2146, 2150, 2152,
				2156, 2158, 2160, 2164, 2166, 2170, 2172, 2174, 2178, 2180, 2184, 2186, 2188, 2192, 2194, 2198, 2200,
				2202, 2206, 2208, 2212, 2214, 2216, 2220, 2222, 2226, 2228, 2230, 2234, 2236, 2240, 2242, 2244, 2248,
				2250, 2254, 2256, 2258, 2262, 2264, 2268, 2270, 2272, 2276, 2278, 2282, 2284, 2286, 2290, 2292, 2296,
				2298, 2300, 2304, 2306, 2310, 2312, 2314, 2318, 2320, 2324, 2326, 2328, 2332, 2334, 2338, 2340, 2342,
				2346, 2348, 2352, 2354, 2356, 2360, 2362, 2366, 2368, 2370, 2374, 2376, 2380, 2382, 2384, 2388, 2390,
				2394, 2396, 2398, 2402, 2404, 2408, 2410, 2412, 2417, 2419, 2424, 2426, 2430, 2432, 2436, 2438, 2440,
				2444, 2446, 2450, 2452, 2454, 2458, 2460, 2464, 2466, 2468, 2472, 2474, 2478, 2480, 2482, 2486, 2488,
				2492, 2494, 2496, 2500, 2502, 2506, 2508, 2510, 2514, 2516, 2520, 2522, 2527, 2529, 2531, 2535, 2537,
				2541, 2543, 2545, 2549, 2551, 2555, 2557, 2559, 2563, 2565, 2569, 2571, 2573, 2577, 2579, 2583, 2585,
				2587, 2591, 2593, 2597, 2599, 2601, 2605, 2607, 2611, 2613, 2615, 2619, 2621, 2625, 2627, 2629, 2633,
				2635, 2639, 2641, 2643, 2647, 2649, 2653, 2655, 2657, 2661, 2663, 2667, 2669, 2671, 2675, 2677, 2681,
				2683, 2685, 2689, 2691, 2695, 2697, 2699, 2703, 2705, 2709, 2711, 2713, 2717, 2719, 2723, 2725, 2727,
				2731, 2733, 2737, 2739, 2741, 2745, 2747, 2751, 2753, 2755, 2759, 2761, 2765, 2767, 2769, 2773, 2775,
				2780, 2782, 2787, 2789, 2793, 2795, 2797, 2801, 2803, 2807, 2809, 2811, 2815, 2817, 2821, 2823, 2825,
				2829, 2831, 2835, 2837, 2839, 2843, 2845, 2849, 2851, 2853, 2857, 2859, 2863, 2865, 2867, 2871, 2873));

		String complex_id = "test_complex";
		UpdateResponse response = do_update_request(arr, complex_id);
		assertTrue("new buy sell document is created", response.isCreated());

		ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

		System.out.println(reassembled_array.subList(0, 10));

		assertTrue("the array on reassembling is equal", arr.equals(reassembled_array));

	}

	/****
	 * 
	 * test update of an array with day ids, when none exists.
	 * 
	 */
	// @Test
	public void creates_the_array_if_it_doesnt_exist() {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			if (i % 2 == 0) {
				arr.add(i);
			}
		}

		System.out.println("array is:");
		System.out.println(arr);

		String complex_id = "test_complex";
		UpdateResponse response = do_update_request(arr, complex_id);
		assertTrue("new buy sell document is created", response.isCreated());

		ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

		assertTrue("the array on reassembling is equal", arr.equals(reassembled_array));

	}

	/***
	 * 
	 * test update of an array with day ids, when it already exists.
	 * 
	 * 
	 */
	// @Test
	public void update_array_test() {
		// first create an
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			if (i % 2 == 0) {
				arr.add(i);
			}
		}

		System.out.println("array ONE is:");
		System.out.println(arr);

		String complex_id = "test_complex";
		UpdateResponse response = do_update_request(arr, complex_id);
		assertTrue("new buy sell document is created", response.isCreated());

		// now make another array which starts from 5
		// and see if it updates correctly.

		ArrayList<Integer> arr2 = new ArrayList<Integer>();
		for (int i = 5; i < 15; i++) {
			if (i % 2 == 0) {
				arr2.add(i);
			}
		}

		System.out.println("array TWO is:");
		System.out.println(arr2);

		UpdateResponse response_two = do_update_request(arr2, complex_id);
		assertTrue("new buy sell document is updated", !response_two.isCreated());

		// now get the array,and check that it contains what you want.
		ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

		System.out.println("reassembled array is: ");
		System.out.println(reassembled_array);
		// now add all and chec..

		ArrayList<Integer> combined_array = new ArrayList<Integer>();
		combined_array.addAll(arr);
		combined_array.addAll(arr2);
		combined_array = unique_arraylist(combined_array);

		assertTrue("the arrays are equal", combined_array.equals(reassembled_array));

	}

	//@Test
	public void test_runs_complexes_individual_run() throws Exception {
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {

			HashMap<String, ArrayList<Integer>> up_down_up_pattern_days = new HashMap<String, ArrayList<Integer>>();

			// 1,2,1,2,1,2
			// whereever there is 2, and the counter is >= 3
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				up_down_up_pattern_days.put(index_entry.getKey(), new ArrayList<Integer>());
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				Integer counter = 0;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					if (counter % 2 == 0) {
						e.getPrice_map_internal().get(day_id).setClose("1.00");
					} else {
						e.getPrice_map_internal().get(day_id).setClose("2.00");
						if (counter >= 3) {
							if (up_down_up_pattern_days.containsKey(entity_name)) {
								up_down_up_pattern_days.get(entity_name).add(day_id);
							} else {
								up_down_up_pattern_days.put(entity_name, new ArrayList<Integer>(Arrays.asList(day_id)));
							}
						}
					}
					counter++;
				}
			}
			System.out.println("the up down up pattern days are:");
			System.out.println(up_down_up_pattern_days);
			// modify every entity, similar to the subindicator calculation
			// tests
			// evaluate that the buy sell arrays are correctly set for that
			// subindicator.
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().calculate_entity_index_complexes();

			// so now we have to get that subindicator id that we are looking
			// for.
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				String entity_es_id = index_entry.getValue().getEntity_map().get(entity_name).getEntity_es_id();
				String indicator_id = CentralSingleton.stub_hashmap.get("close_period_start_1_period_end").getId();
				String subindicator_id = SubIndicatorCalculation.subindicator_prefix_map.get("Pattern") + "6";

				String complex_id = entity_es_id + indicator_id + subindicator_id;

				// now get the buy array for this complex.

				// unique it.
				

				ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

				// then check the arraylist we created in
				// up_down_up_pattern_days for
				// this entity.
				ArrayList<Integer> up_down_days_for_current_entity = up_down_up_pattern_days.get(entity_name);

				// we have to search for complex:

				// E-0I16_0sb6

			

				int comp_counter = 0;



				System.out.println("reassembled array length is:" + reassembled_array.size());

				List<Integer> last_ten_days_from_es = reassembled_array.subList(reassembled_array.size() - 10,
						reassembled_array.size() - 1);

				System.out.println(last_ten_days_from_es);

				System.out.println("stored last ten days array size is:" + up_down_days_for_current_entity.size());

				List<Integer> last_ten_days_stored = up_down_days_for_current_entity.subList(
						up_down_days_for_current_entity.size() - 10, up_down_days_for_current_entity.size() - 1);

				System.out.println(last_ten_days_stored);

				// they should match.
				assertTrue("reassembled array equals the expected array",
						reassembled_array.equals(up_down_days_for_current_entity));

			}
		}
	}

	//@Test
	public void test_runs_complexes_two_runs() throws Exception {

		/***
		 * FIRST RUN. ADD THE PART W
		 */
		StockConstants.define_custom_end_date = true;
		StockConstants.minus_days_from_current_datetime = 200;
		HashMap<String, EntityIndex> initially_downloaded_exchanges = initially_downloaded();
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {
			HashMap<String, ArrayList<Integer>> up_down_up_pattern_days = new HashMap<String, ArrayList<Integer>>();

			
			/******
			 * 
			 * modify the datapoints so that we can simulate a pattern.
			 */
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				up_down_up_pattern_days.put(index_entry.getKey(), new ArrayList<Integer>());
				
				Integer counter = 0;
				
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					if (counter % 2 == 0) {
						e.getPrice_map_internal().get(day_id).setClose("1.00");
					} else {
						e.getPrice_map_internal().get(day_id).setClose("2.00");
						if (counter >= 3) {
							if (up_down_up_pattern_days.containsKey(entity_name)) {
								up_down_up_pattern_days.get(entity_name).add(day_id);
							} else {
								up_down_up_pattern_days.put(entity_name, new ArrayList<Integer>(Arrays.asList(day_id)));
							}
						}
					}
					counter++;
				}
			}
			
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().calculate_entity_index_complexes();
			index_entry.getValue().commit_entity_index_to_elasticsearch(1);
			
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				String entity_es_id = index_entry.getValue().getEntity_map().get(entity_name).getEntity_es_id();
				String indicator_id = CentralSingleton.stub_hashmap.get("close_period_start_1_period_end").getId();
				String subindicator_id = SubIndicatorCalculation.subindicator_prefix_map.get("Pattern") + "6";

				String complex_id = entity_es_id + indicator_id + subindicator_id;
				System.out.println("complex id is:" + complex_id);
				// now get the buy array for this complex.

				// unique it.
				
				ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

				// then check the arraylist we created in
				// up_down_up_pattern_days for
				// this entity.
				ArrayList<Integer> up_down_days_for_current_entity = up_down_up_pattern_days.get(entity_name);

				System.out.println("reassembled array length is:" + reassembled_array.size());

				List<Integer> last_ten_days_from_es = reassembled_array.subList(reassembled_array.size() - 10,
						reassembled_array.size() - 1);

				System.out.println(last_ten_days_from_es);

				System.out.println("stored last ten days array size is:" + up_down_days_for_current_entity.size());

				List<Integer> last_ten_days_stored = up_down_days_for_current_entity.subList(
						up_down_days_for_current_entity.size() - 10, up_down_days_for_current_entity.size() - 1);

				System.out.println(last_ten_days_stored);

				// they should match.
				assertTrue("reassembled array equals the expected array",
						reassembled_array.equals(up_down_days_for_current_entity));

			}

			
		}
		
		//System.exit(1);
		
		/***
		 * SECOND RUN.
		 * 
		 */
		initially_downloaded_exchanges = null;
		CentralSingleton.destroySingleton();
		CentralSingleton.initializeSingleton();
		StockConstants.define_custom_end_date = false;
		initially_downloaded_exchanges = build_and_download_all_entities(
				new ArrayList<String>(Arrays.asList(new String[] { StockConstants.name_for_oil })), true, false, true);;
		for (Map.Entry<String, EntityIndex> index_entry : initially_downloaded_exchanges.entrySet()) {
			HashMap<String, ArrayList<Integer>> up_down_up_pattern_days = new HashMap<String, ArrayList<Integer>>();

			/******
			 * 
			 * modify the datapoints so that we can simulate a pattern.
			 */
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				
				Entity e = index_entry.getValue().getEntity_map().get(entity_name);
				up_down_up_pattern_days.put(index_entry.getKey(), new ArrayList<Integer>());

				Integer counter = 0;
				
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					if (counter % 2 == 0) {
						e.getPrice_map_internal().get(day_id).setClose("1.00");
					} else {
						e.getPrice_map_internal().get(day_id).setClose("2.00");
						if (counter >= 3) {
							if (up_down_up_pattern_days.containsKey(entity_name)) {
								up_down_up_pattern_days.get(entity_name).add(day_id);
							} else {
								up_down_up_pattern_days.put(entity_name, new ArrayList<Integer>(Arrays.asList(day_id)));
							}
						}
					}
					
					counter++;
				}
			}
			
			index_entry.getValue().calculate_entity_index_indicators();
			index_entry.getValue().calculate_entity_index_complexes();
			index_entry.getValue().commit_entity_index_to_elasticsearch(1);
			
			for (String entity_name : index_entry.getValue().getEntity_map().keySet()) {
				String entity_es_id = index_entry.getValue().getEntity_map().get(entity_name).getEntity_es_id();
				String indicator_id = CentralSingleton.stub_hashmap.get("close_period_start_1_period_end").getId();
				String subindicator_id = SubIndicatorCalculation.subindicator_prefix_map.get("Pattern") + "6";

				String complex_id = entity_es_id + indicator_id + subindicator_id;

				// now get the buy array for this complex.

				// unique it.
				

				ArrayList<Integer> reassembled_array = get_reassembled_array(complex_id);

				// then check the arraylist we created in
				// up_down_up_pattern_days for
				// this entity.
				ArrayList<Integer> up_down_days_for_current_entity = up_down_up_pattern_days.get(entity_name);

				// we have to search for complex:

				// E-0I16_0sb6

			

				int comp_counter = 0;

				for (Integer a : reassembled_array) {

					// if
					// (!up_down_days_for_current_entity.get(comp_counter).equals(a))
					// {

					// }

					comp_counter++;
				}

				System.out.println("reassembled array length is:" + reassembled_array.size());

				List<Integer> last_ten_days_from_es = reassembled_array.subList(reassembled_array.size() - 10,
						reassembled_array.size() - 1);

				System.out.println(last_ten_days_from_es);

				System.out.println("stored last ten days array size is:" + up_down_days_for_current_entity.size());

				List<Integer> last_ten_days_stored = up_down_days_for_current_entity.subList(
						up_down_days_for_current_entity.size() - 10, up_down_days_for_current_entity.size() - 1);

				System.out.println(last_ten_days_stored);

				// they should match.
				assertTrue("reassembled array equals the expected array",
						reassembled_array.equals(up_down_days_for_current_entity));

			}
			
		}
	}

}
