package dailyDownload;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.script.ScriptService;
import org.elasticsearch.search.SearchHit;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Tuple;
import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.CheckPriceAberrations;
import yahoo_finance_historical_scraper.DayConnectorEdge;
import yahoo_finance_historical_scraper.HistoricalStock;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.StockOperations;
import Indicators.RedisManager;
import Titan.ExecuteMultiThreadedJob;
import Titan.GroovyStatements;
import Titan.PropertyKeyAbsentException;
import Titan.TItanWorkerThreadResultObject;
import Titan.TitanConstants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import elasticSearchNative.BulkLoader;
import elasticSearchNative.LocalEs;
import elasticSearchNative.IBulkLoader;

public class AddHistoricalStockQueryResultsToDatabase implements
		TitanConstants, IBulkLoader {

	/**
	 * we need to build a final hashmap of all the new dates that were added for
	 * each stock. Triplet<stockName,stockFullName,Indice>,
	 * Pair<day_id_of_earliest_day,day_id_of_latest_day>
	 * 
	 * this serves as the input for the class which builds the requests for
	 * jedis popper.
	 * 
	 */
	HashMap<String, HashMap<String, Object>> dates_successfully_added_to_es_map = new HashMap<String, HashMap<String, Object>>();

	/**
	 * 
	 * only close -> c1 close + open,high,low -> c2 close + open,high,low,volume
	 * -> c3 close + volume -> c4
	 * 
	 * 
	 * @param m
	 * @return
	 */
	private static final String identifier_for_log = "add_historical_stock_query_results_to_database";
	private final Integer bulk_items_amount = 2000;
	private BulkRequestBuilder brq;
	private ArrayList<Map<String, Object>> index_request_list;
	private BulkLoader bulkloader;

	public ArrayList<Map<String, Object>> getIndex_request_list() {
		return index_request_list;
	}

	public void setIndex_request_list(
			ArrayList<Map<String, Object>> index_request_list) {
		this.index_request_list = index_request_list;
	}

	public BulkRequestBuilder getBrq() {
		return brq;
	}

	public void setBrq(BulkRequestBuilder brq) {
		this.brq = brq;
	}

	private ArrayList<TItanWorkerThreadResultObject> results_array;

	public ArrayList<TItanWorkerThreadResultObject> getResults_array() {
		return results_array;
	}

	public void setResults_array(
			ArrayList<TItanWorkerThreadResultObject> results_array) {
		this.results_array = results_array;
	}

	private Gson gson;

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	public Map<String, Object> add_ohlcv_type_details(Map<String, Object> m) {
		String ohlcv = "c1";

		if (m.containsKey("volume")) {
			ohlcv = "c4";
		}
		if (m.containsKey("open") && m.containsKey("high")
				&& m.containsKey("low")) {
			ohlcv = "c2";
			if (m.containsKey("volume")) {
				ohlcv = "c3";
			}
		}

		m.put("ohlcv_type", ohlcv);

		return m;
	}

	public Map<String, Object> addKeyValuePairToMap(Map<String, Object> m,
			String key, Object value, TitanManagement mgmt,
			ArrayList<String> propertyKeyList)
			throws PropertyKeyAbsentException {

		m.put(key, value);

		/*
		 * if (propertyKeyList.contains(key)) { m.put(key, value); } else { if
		 * (mgmt.containsRelationType(key)) { propertyKeyList.add(key);
		 * m.put(key, value); } else { throw new
		 * PropertyKeyAbsentException("the key  " + key + " was absent."); } }
		 */

		return m;
	}

	public AddHistoricalStockQueryResultsToDatabase(QueryResult qr,
			TitanGraph g, TitanManagement mgmt, boolean use_transactional_graph)
			throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InterruptedException, ExecutionException {

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				"add_historical_stock_query_results_to_database",
				"hashmap of stock vertices to be added:"
						+ qr.getHm().toString());

		// AddDataPointToGraph adg = new AddDataPointToGraph(g,
		// use_transactional_graph);
		this.bulkloader = new BulkLoader(
				200,
				"tradegenie_titan",
				StockConstants.elasticsearch_mapping_name_for_historical_data_point);

		int total_transactions_done = 0;

		Class cls = Class.forName(HistoricalStock.class.getName());

		Field fieldlist[] = cls.getDeclaredFields();

		ArrayList<String> propertyKeyList = new ArrayList<String>();

		IndiceComponents components = IndiceComponents.getInstance();

		setResults_array(new ArrayList<TItanWorkerThreadResultObject>());
		setGson(new Gson());

		

		for (HistoricalStock stock : qr.getHm().values()) {
			// checks the stock for aberrations, and if there is an aberration,
			// the next conditoin,
			// means that the stock will not be inserted.
			CheckPriceAberrations chkpr = new CheckPriceAberrations(stock);
			
			
			
			if (stock.getclose().size() > 0
					&& !LoggingDailyOperations.get_instance().has_exception()) {
				// System.out.println("the last date and unique identifer of the stock object is: "
				// + stock.print_last_date_and_unique_identifier());
				// add it to the indice components
				// check if indice exists, and if yes, then add stock name and
				// fullname to it in the map.
				
				//modulate the stock open,close,high,low to fix, floatglitch.
				//volume is not fixed in order to 
				System.out.println("now calling fix float glitch on stock");
				stock.fix_float_glitch();
				System.out.println("ended calling fix float glitch.");
				try {
					System.in.read();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				components.addstockToIndexInMap(stock.getIndice(),
						stock.getStockName(), stock.getStockFullName());
				// now we have to iterate over each field, if null dont add it
				// to
				// map,
				// otherwise get its value for the object and add to the map.
				int calendar_entry = 0;

				for (Calendar cal : stock.getDates()) {
					// System.out.println("current calendar of this stock is" +
					// calendar_entry);
					// the map is the property name , followed by its value.
					Map<String, Object> m = new HashMap<String, Object>();

					for (Field f : fieldlist) {
						// if(f.getName() == "indice"){
						// System.out.println("the name of the field is ---> "
						// + f.getName());
						// System.out.println("the value is--->" +
						// f.get(stock));
						// }

						if (f.get(stock) != null) {

							if (f.get(stock) instanceof String
									|| f.get(stock) instanceof Integer
									|| f.get(stock) instanceof Double
									|| f.get(stock) instanceof Float
									|| f.get(stock) instanceof Long) {

								try {

									m = addKeyValuePairToMap(m, f.getName(),
											f.get(stock), mgmt, propertyKeyList);

								} catch (PropertyKeyAbsentException e) {
									// TODO Auto-generated catch block
									LoggingDailyOperations
											.get_instance()
											.add_to_log(
													"exception",
													e,
													stock.getUnique_identifier()
															.get(0), null);
								}

							} else if (f.get(stock) instanceof ArrayList<?>) {
								ArrayList<?> l = (ArrayList<?>) f.get(stock);
								// System.out.println("it was an instance of---> "
								// +
								// f.get(stock).getClass());

								if (l.isEmpty()) {
									// System.out.println("it was empty proof--->"
									// + l.size());
								} else {
									// System.out.println("it was not empty, so"
									// + " now we check if calendar");
									// see if its a calendar, in which case we
									// have to
									if (l.get(calendar_entry) instanceof Calendar) {
										// System.out.println("it is a calendar");
										Calendar caln = (Calendar) l
												.get(calendar_entry);
										String calendar_rep = new CalendarFunctions(
												caln)
												.getFullDateWithoutTimeZone();
										// System.out.println("calendar representation is -->"
										// + "" + calendar_rep);
										try {
											m = addKeyValuePairToMap(m,
													f.getName(), calendar_rep,
													mgmt, propertyKeyList);
										} catch (PropertyKeyAbsentException e) {
											// TODO Auto-generated catch block
											LoggingDailyOperations
													.get_instance()
													.add_to_log(
															"exception",
															e,
															stock.getUnique_identifier()
																	.get(0),
															null);
										}
									} else {
										try {
										
											m = addKeyValuePairToMap(m,
													f.getName(),
													l.get(calendar_entry),
													mgmt, propertyKeyList);
										} catch (PropertyKeyAbsentException e) {
											// TODO Auto-generated catch block
											LoggingDailyOperations
													.get_instance()
													.add_to_log(
															"exception",
															e,
															stock.getUnique_identifier()
																	.get(0),
															null);
										}
									}
								}
							} else {
								// all other types can be added here later.
							}

						}

						// here you have to submit the map to the worker thread.
						// so here we do the submit new vertex thingy.

					}

					JSONObject additional_params = new JSONObject();
					/*
					 * try { additional_params .put("document_type",
					 * "class_yahoo_finance_historical_scraper_HistoricalStock"
					 * ); additional_params.put("elasticsearch",
					 * elasticSearchConstants.add_to_elasticsearch); } catch
					 * (JSONException e1) { // TODO Auto-generated catch block
					 * LoggingDailyOperations.get_instance().add_to_log(
					 * "exception", e1, stock.getUnique_identifier().get(0),
					 * null); }
					 */

					// System.out.println("this is the map that will be added.");
					// for(Map.Entry<String, Object> entry : m.entrySet()){
					// System.out.println("key-->" + entry.getKey().toString());
					// System.out.println("value--->" +
					// entry.getValue().toString());
					// }
					// try {
					// System.in.read();
					// } catch (IOException e1) {
					// TODO Auto-generated catch block
					// e1.printStackTrace();
					// }
					// add_historical_stock_to_es(add_ohlcv_type_details(m));
					Map<String, Object> map_with_ohlcv_details = add_ohlcv_type_details(m);
					Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = this.bulkloader
							.add_item_to_bulk_loader(map_with_ohlcv_details);
					if (response != null) {
						process_response(response);
					}
					// adg.CreateVertex(add_ohlcv_type_details(m),
					// "create_vertex", additional_params);
					total_transactions_done++;
					/*
					 * if (use_transactional_graph) { System.out .println(
					 * "user transactional graph is true, and here are the " +
					 * "total transactions done--->" + total_transactions_done);
					 * if (total_transactions_done > GLOBAL_TRANSACTIONS_LIMIT)
					 * { System.out.println("crossed transaction limit --->" +
					 * total_transactions_done);
					 * 
					 * for (int i = 0; i < total_transactions_done; i++) {
					 * System.out .println(
					 * "now doing thread completion inside the crossed limit--->"
					 * + String.valueOf(i)); try { String result =
					 * adg.getTpoolCompl().take() .get(); } catch
					 * (ExecutionException e) { LoggingDailyOperations
					 * .get_instance() .add_to_log( "exception", e,
					 * stock.getUnique_identifier() .get(0), null); } // now
					 * after this shutdown the pool, and commit // the // graph.
					 * } adg.getTpoolExec().shutdown(); // now commit.
					 * adg.getG().commit(); // now create a new pool. adg =
					 * null; total_transactions_done = 0; adg = new
					 * AddDataPointToGraph(g, use_transactional_graph); //
					 * System.out //
					 * .println("commited this transaction and regen thread pool"
					 * ); //
					 * System.out.println("now going to get inside again."); } }
					 */

					calendar_entry++;
				}

			} else {

			}

		}

		// index_documents_and_flush_bulk();
		Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = this.bulkloader
				.index_documents_and_flush_bulk();
		if (response != null) {
			process_response(response);
		}
		
		
		
		/*
		 * for (int i = 0; i < total_transactions_done; i++) {
		 * 
		 * try { String result = adg.getTpoolCompl().take().get();
		 * TItanWorkerThreadResultObject thread_result = getGson()
		 * .fromJson(result, TItanWorkerThreadResultObject.class);
		 * getResults_array().add(thread_result);
		 * 
		 * if (i % 250 == 0) {
		 * 
		 * LoggingDailyOperations.get_instance().add_to_log( "info", null,
		 * "add_historical_stock_query_results_to_database",
		 * "adding the stock vertex operation number: " + String.valueOf(i) +
		 * " and the result is:" + result);
		 * 
		 * }
		 * 
		 * } catch (Exception e) {
		 * LoggingDailyOperations.get_instance().add_to_log("exception", e,
		 * "error while collecting results from stock add job.", null); }
		 * 
		 * }
		 * 
		 * adg.getTpoolExec().shutdown();
		 * 
		 * if (use_transactional_graph) { adg.getG().commit(); } else {
		 * adg.getTg().commit(); }
		 */
		if (LoggingDailyOperations.get_instance().has_exception()) {

			LoggingDailyOperations.get_instance()
					.exit_without_graph_and_log_before_exiting();
		} else {

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"now starting to update the stock pair changes to es");
			
			if(StockConstants.update_stock_pair_changes_to_es){
				update_stock_pair_changes_to_es(this.bulkloader);
			}

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"finished updating the stock pair changes to es.");

			/***
			 * now after this we can send the calls to start the complex
			 * calculations by jedis popper
			 * 
			 * 
			 * 
			 */
			try {
				//System.out.println(dates_successfully_added_to_es_map);
				if(StockConstants.prepare_complexes){
					
					PrepareComplexes prep = new PrepareComplexes(dates_successfully_added_to_es_map);
				}
			} catch (Exception e) {
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e, identifier_for_log,
						null);
			}
			
			//whatever happens we have to reset the jedis schedule keys.
			//tell the redis poller scheduler to stop.
			
			
			//if(!srkc.getProceed()){
			//	LoggingDailyOperations.get_instance().exit_without_graph_and_log_before_exiting();
			//}
			// System.out.println("skippin drawing day connector edges.");
			/*
			 * try { LoggingDailyOperations .get_instance() .add_to_log( "info",
			 * null, "add_historical_stock_query_results_to_database",
			 * "starting to draw the day connector edges for this index");
			 * draw_day_connector_edges(g, mgmt); if (use_transactional_graph) {
			 * adg.getG().commit(); } else { adg.getTg().commit(); } } catch
			 * (Exception e) { // TODO Auto-generated catch block
			 * e.printStackTrace();
			 * LoggingDailyOperations.get_instance().add_to_log("exception", e,
			 * "drawing day connector edges", null); }
			 */
		}
	}

	public void draw_day_connector_edges(TitanGraph g, TitanManagement mgmt)
			throws JSONException {
		// get the result array and for each vertex that was su
		// ccessfully added, we go ahead and draw the edges
		Integer counter = 0;
		ArrayList<DayConnectorEdge> dmain = new ArrayList<DayConnectorEdge>();
		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				"add_historical_stock_query_results_to_database",
				"total things in the TItanWorkerThreadResultObject from adding"
						+ " the stock vertices are:"
						+ getResults_array().size());
		Integer total_edges_to_draw = getResults_array().size() * 365;
		for (TItanWorkerThreadResultObject tr : getResults_array()) {

			Map<String, Object> vertex_map = (Map<String, Object>) tr
					.get_success_object().get("vertex_map");

			for (DayConnectorEdge e : GroovyStatements
					.getInstance()
					.generate_stock_wise_updates_for_common_day_connector_edges(
							(Map<String, Object>) vertex_map.get("map"))) {

				dmain.add(e);
			}

			if (counter % 250 == 0) {
				LoggingDailyOperations.get_instance().add_to_log(
						"info",
						null,
						"add_historical_stock_query_results_to_database",
						"adding day connector edge objects to arraylist completed :"
								+ counter * 365 + "of approximately :"
								+ total_edges_to_draw);
			}

			counter++;
		}

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				"add_historical_stock_query_results_to_database",
				"finished creating the array of day connector edges, its size is :"
						+ dmain.size());

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null,
						"add_historical_stock_query_results_to_database",
						"starting the multi threaded graph add job to draw the day connector edges");

		ExecuteMultiThreadedJob exm = new ExecuteMultiThreadedJob(dmain, g,
				mgmt, "update_property_on_common_day_connector_edge", false,
				new ArrayList<org.json.JSONObject>());

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null,
						"add_historical_stock_query_results_to_database",
						"finished the multi threaded graph add job to draw the day connector edges");

	}

	/**
	 * this function iterates over all the individual stocks in the
	 * dailyoperations logs and for each day id it calculates the difference for
	 * the last 300 days, then it finds the relevant pair and pushes it to the
	 * bulk update script.
	 * 
	 * if there is a failure anywhere in the function then it returns false, so
	 * that we dont go ahead with the next part of calculating the complexes
	 * 
	 * otherwise we return true, so that we go ahead and calculate the
	 * complexes.
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Boolean update_stock_pair_changes_to_es(BulkLoader bulkloader) {
		try {
			ArrayList<HashMap<String, Object>> stock_pair_changes = new ArrayList<HashMap<String, Object>>();
			bulkloader.setBrq(new BulkRequestBuilder(LocalEs
					.getInstance()));
			Integer test_counter = 0;

			Boolean failure_in_updating_stock_pair_changes = false;

			try (Jedis jedis = RedisManager.getInstance().getJedis()) {

				ObjectMapper m = new ObjectMapper();

				for (Map.Entry<String, StockOperations> entry : LoggingDailyOperations
						.get_instance().getIndividual_stock_operations()
						.entrySet()) {

					StockOperations ops = entry.getValue();
					// System.out.println("now doing this stock operation:");
					// System.out.println(ops.getStockFullName());

					ArrayList<String> day_ids_added_to_es = ops
							.getDates_successfully_added_to_db();

					final String stockName = ops.getStockName();
					final String indice = ops.getIndice();
					final String stockFullName = ops.getStockFullName();
					final String stock_id = stockName + "_" + stockFullName
							+ "_" + indice;

					String name_of_set = "stock_" + stockName + "_" + indice
							+ "_ohlcv_values";

					TreeMap tm = new TreeMap();

					Set<Tuple> ohlcv_maps_for_this_stock = jedis
							.zrangeWithScores(name_of_set, 0l, -1l);

					// System.out
					// .println("now getting the ohlcv values for this stock from redis.");

					for (Tuple tup : ohlcv_maps_for_this_stock) {
						Double day_id = tup.getScore();
						String ohlcv_map = tup.getElement();
						HashMap<String, Object> ohlcv_map_el = (HashMap<String, Object>) JSON
								.parse(ohlcv_map);
						// System.out
						// .println("day id in redis is:" + day_id
						// + " ohlcv map in redis on that day is:"
						// + ohlcv_map);
						Integer day_id_i = day_id.intValue();
						tm.put(day_id_i, (Double) ohlcv_map_el.get("close"));
					}

					// for each day id compose an array of stock pairs that have
					// to
					// be updated.
					// we need to get all
					// System.out
					// .println("now processing all the day ids added to just now..");
					// System.out.println("day ids added to es were:" +
					// day_ids_added_to_es);
					for (String day_id : day_ids_added_to_es) {

						// System.out.println("day id is:" + day_id);
						Integer day_id_i = Integer.parseInt(day_id);
						NavigableMap<Integer, Double> desc_nvm = tm.headMap(
								day_id_i, true).descendingMap();

						// System.out
						// .println("the navigable map upto this day is after descent.:");

						// NavigableMap<Integer, Double> desc_nvm = nvm
						// .descendingMap();

						// System.out.println(test_json);

						ArrayList<Integer> all_day_ids_inclusive_given_day = new ArrayList<Integer>(
								desc_nvm.keySet());

						ArrayList<Double> all_days_inclusive_given_day_close_values = new ArrayList<Double>(
								desc_nvm.values());

						Double close_on_given_day = all_days_inclusive_given_day_close_values
								.get(0);
						// System.out.println("close value on day in question is:"
						// + close_on_given_day);
						// try {
						// System.in.read();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						int i = 1;

						Integer while_counter = null;

						if (all_day_ids_inclusive_given_day.size() > StockConstants.total_pairs_for_any_given_days) {
							while_counter = StockConstants.total_pairs_for_any_given_days;
						} else {
							while_counter = all_day_ids_inclusive_given_day
									.size();
						}

						while (i < while_counter) {
							// System.out.println("now doing the first day:" +
							// i);

							String pair_name = all_day_ids_inclusive_given_day
									.get(i) + "_" + day_id;
							 System.out.println("pair name is:" + pair_name);

							final Double price_change = (close_on_given_day - 
									all_days_inclusive_given_day_close_values
									.get(i)) / (close_on_given_day) * 100.0;
							// System.out.println("price change is:" +
							// price_change);

							Integer profit_or_loss = 1;
							if (price_change <= 0.0) {
								profit_or_loss = -1;
							}
							final Integer profit_or_loss_final = profit_or_loss;
							// System.out.println("profit or loss is:"
							// + profit_or_loss);
							
							final Double value_at_start_day = all_days_inclusive_given_day_close_values
									.get(i); 
									
							final Double value_at_end_day = close_on_given_day;

							// now get the es id of this pair from the redis
							// hash.
							// then add to bulk update loader.
							String es_id_for_update = jedis.hget("pair_es_ids",
									pair_name);
							// System.out.println("es id for update is:"
							// + es_id_for_update);

							if (es_id_for_update != null) {
								// balls to it, doesnt exist.
								HashMap<String, Object> urequest_map = new HashMap<String, Object>() {
									{
										put("pair_stock_change",
												new HashMap<String, Object>() {
													{
														put("stock_ops_name",
																stock_id);
														put("price_change_percentage",
																price_change);
														put("profit_or_loss",
																profit_or_loss_final);
														put("value_at_start_day",value_at_start_day);
														put("value_at_end_day",value_at_end_day);
													}
												});
									}
								};

								UpdateRequest urequest = new UpdateRequest(
										"tradegenie_titan", "pairs",
										es_id_for_update).script(
										"update_stock_pair_change",
										ScriptService.ScriptType.FILE,
										urequest_map);
								Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = bulkloader
										.add_update_item_to_es(urequest,
												urequest_map);
								test_counter++;
								System.out.println("total updates done:"
										+ test_counter);
								if (response != null) {
									for (HashMap<String, Object> failed_object : response
											.getValue1()) {
										ops.getPair_change_failed_to_update_to_es()
												.add(this.gson
														.toJson(failed_object));
										LoggingDailyOperations
												.get_instance()
												.add_to_log(
														"exception",
														new Exception(
																"failed to update a pair stock change into es,"
																		+ " check logs"),
														identifier_for_log,
														null);
										failure_in_updating_stock_pair_changes = true;
									}
									for (HashMap<String, Object> success_object : response
											.getValue0()) {
										// ops.getPairs_successfully_updated_to_es().add(this.gson.toJson(success_object));
									}
									// System.out
									// .println("hopefully there havent been any errors before this"
									// + "point");

								}
							}

							i++;
						}

					}

				}
				Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = bulkloader
						.index_documents_and_flush_bulk();
				if (response != null) {

					for (HashMap<String, Object> failed_object : response
							.getValue1()) {

						Map<String, Object> map = (Map<String, Object>) failed_object
								.get("item");
						Map<String, Object> pair_stock_change = (Map<String, Object>) map
								.get("pair_stock_change");
						String ops_name = (String) pair_stock_change
								.get("stock_ops_name");

						LoggingDailyOperations.get_instance()
								.getIndividual_stock_operations().get(ops_name)
								.getPair_change_failed_to_update_to_es()
								.add(this.gson.toJson(failed_object));

						LoggingDailyOperations.get_instance().add_to_log(
								"exception",
								new Exception(
										"failed to update a pair stock change into es(residue type) : "
												+ failed_object.toString()),
								identifier_for_log, null);
						failure_in_updating_stock_pair_changes = true;
					}
					/**
					 * for(HashMap<String,Object> success_object :
					 * response.getValue0()){ Map<String,Object> map =
					 * (Map<String,Object>)success_object.get("item");
					 * Map<String,Object> pair_stock_change =
					 * (Map<String,Object>) map.get("pair_stock_change"); String
					 * ops_name =
					 * (String)pair_stock_change.get("stock_ops_name");
					 * 
					 * LoggingDailyOperations.get_instance()
					 * .getIndividual_stock_operations()
					 * .get(ops_name).getPairs_successfully_updated_to_es
					 * ().add(this .gson.toJson(success_object));
					 * 
					 * }
					 **/

				}
			}

			if (failure_in_updating_stock_pair_changes) {
				return false;
			}
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * this function is used to extract the stock details from the es bulk
	 * response map. it is used to generate a map that is then used to update
	 * the new ohlcv values into redis.
	 * 
	 * @param success_object
	 * @return
	 */
	public Map<String, Object> get_individual_map_details_from_bulk_loader_response(
			Map<String, Object> success_object) {

		Map<String, Object> historical_data_point = (Map<String, Object>) success_object
				.get("item");

		final Integer day_id = (Integer) historical_data_point.get("day_id");

		String stockName = (String) historical_data_point.get("stockName");
		String stockFullName = (String) historical_data_point
				.get("stockFullName");
		String indice = (String) historical_data_point.get("indice");
		String dates = (String) historical_data_point.get("dates");
		final String stockoperations_name = stockName + "_" + stockFullName
				+ "_" + indice;

		final Double open = (Double) historical_data_point.get("open");
		final Double close = (Double) historical_data_point.get("close");
		final Double high = (Double) historical_data_point.get("high");
		final Double low = (Double) historical_data_point.get("low");
		final Long volume = (Long) historical_data_point.get("volume");
		final String month = (String) historical_data_point.get("month");
		final Integer week_of_month = (Integer) historical_data_point
				.get("week_of_month");
		final String day_of_week = (String) historical_data_point
				.get("day_of_week");
		final Integer quarter_of_year = (Integer) historical_data_point
				.get("quarter_of_year");

		/**
		 * deriving year
		 * 
		 */
		Long milliseconds_since_epoch = day_id.longValue() * 86400000;
		TimeZone gmt_timezone = TimeZone.getTimeZone("GMT");
		GregorianCalendar calendar = new GregorianCalendar(gmt_timezone);
		calendar.setTimeInMillis(milliseconds_since_epoch);
		final Integer year = calendar.get(Calendar.YEAR);

		Map<String, Object> ohlcv_map = new HashMap<String, Object>() {
			{

				put("open", open);
				put("close", close);
				put("high", high);
				put("low", low);
				put("volume", volume);
				put("month", month);
				put("week_of_month", week_of_month);
				put("day_of_week", day_of_week);
				put("quarter", quarter_of_year);
				put("year", year);
				put("day_id", day_id);
			}
		};

		// System.out.println("inner shit.");
		// System.out.println(ohlcv_map);
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return ohlcv_map;
	}

	public void populate_hm_for_complex_calculation(
			Map<String, Object> historical_data_point) {

		final String stockName = (String) historical_data_point
				.get("stockName");
		final String stockFullName = (String) historical_data_point
				.get("stockFullName");
		final String indice = (String) historical_data_point.get("indice");
		final Integer day_id = (Integer) historical_data_point.get("day_id");
		final String ohlcv_type = (String) historical_data_point
				.get("ohlcv_type");
		final String combined_name = stockName + "_" + stockFullName + "_"
				+ indice;
		
		//System.out.println("day id outside:" + day_id);
		//System.out.println("ohlcv_type outside" + ohlcv_type);
		
		
		if (dates_successfully_added_to_es_map.containsKey(combined_name)) {
			//System.out.println("this is the hashmap thing:" + dates_successfully_added_to_es_map.get(combined_name));
			
			Integer existing_day_id_min = (Integer) dates_successfully_added_to_es_map.get(combined_name)
					.get("day_id_min");
			Integer existing_day_id_max = (Integer) dates_successfully_added_to_es_map.get(combined_name)
					.get("day_id_max");
			
			//System.out.println("existing day id min is:" + existing_day_id_min);
			//System.out.println("existing day id max is:" + existing_day_id_max);
			//System.out.println("day id being tested is:" + day_id);
			
			if (existing_day_id_min > day_id) {
				dates_successfully_added_to_es_map.get(combined_name).put(
						"day_id_min", day_id);
			} else {
				
				if (existing_day_id_max < day_id) {
					dates_successfully_added_to_es_map.get(combined_name).put(
							"day_id_max", day_id);
				}
			}

		} else {

			dates_successfully_added_to_es_map.put(combined_name,
					new HashMap<String, Object>() {
						{
							put("day_id_min", day_id);
							put("day_id_max", day_id);
							put("stockName", stockName);
							put("stockFullName", stockFullName);
							put("indice", indice);
							put("ohlcv_type", ohlcv_type);
						}
					});

		}

	}

	/**
	 * update ohlcv objects of newly dowloaded historical data points into
	 * redis.
	 * 
	 * 
	 */
	@Override
	public void process_response(
			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response) {
		// TODO Auto-generated method stub
		// only update the successfully put maps into redis.
		ArrayList<HashMap<String, Object>> objects_successfully_added_to_es = response
				.getValue0();
		ArrayList<HashMap<String, Object>> objects_failed_to_add_to_es = response
				.getValue1();
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Pipeline jedis_pipe = jedis.pipelined();

			for (HashMap<String, Object> historical_data_point : objects_successfully_added_to_es) {
				 
				Map<String, Object> ohlcv_map = get_individual_map_details_from_bulk_loader_response(historical_data_point);
				ObjectMapper m = new ObjectMapper();

				
				/**
				 * item
				 * 
				 * 
				 */

				Map<String, Object> actual_hist_data_point = (Map<String, Object>) historical_data_point
						.get("item");
				
				/***
				 * here populating the dates successfully added to es hm, for
				 * use at the end of this class if there are no errors anywhere,
				 * so as to send the requests for complex calculation.
				 * 
				 * 
				 */
				
				populate_hm_for_complex_calculation(actual_hist_data_point);


				/**
				 * 
				 * 
				 * 
				 * 
				 */

				String stockName = (String) actual_hist_data_point
						.get("stockName");
				String indice = (String) actual_hist_data_point.get("indice");
				String stockFullName = (String) actual_hist_data_point
						.get("stockFullName");
				final Integer day_id = (Integer) actual_hist_data_point
						.get("day_id");

				LoggingDailyOperations
						.get_instance()
						.getIndividual_stock_operations()
						.get(stockName + "_" + stockFullName + "_" + indice)
						.addDate_successfully_added_to_db(
								String.valueOf(day_id));

				final String map_as_string = m.writeValueAsString(ohlcv_map);

				jedis_pipe.zadd("stock_" + stockName + "_" + indice
						+ "_ohlcv_values", new HashMap<String, Double>() {
					{
						put(map_as_string, day_id.doubleValue());
					}
				});

			}

			jedis_pipe.sync();
			// now iterate and generate the pair documents.
			// we have to check whether such a pair already exists.
			// get its id and update it.

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
			LoggingDailyOperations.get_instance()
					.exit_without_graph_and_log_before_exiting();
		}

		for (HashMap<String, Object> failure_object : objects_failed_to_add_to_es) {
			// we need to log failure for them.
			Map<String, Object> historical_data_point = (Map<String, Object>) failure_object
					.get("item");

			String stockName = (String) historical_data_point.get("stockName");
			String stockFullName = (String) historical_data_point
					.get("stockFullName");
			String indice = (String) historical_data_point.get("indice");
			String dates = (String) historical_data_point.get("dates");
			org.json.simple.JSONObject failure_reason_object = new org.json.simple.JSONObject();
			failure_reason_object.put("failure_reason",
					failure_object.get("error_cause"));
			failure_reason_object.put("full_map", historical_data_point);

			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception(
							"failed to add following historical data point to es: "
									+ failure_object.toString()),
					identifier_for_log, null);
			LoggingDailyOperations.get_instance()
					.getIndividual_stock_operations()
					.get(stockName + "_" + stockFullName + "_" + indice)
					.getDates_failed_in_database_adding()
					.put(dates, failure_reason_object);

		}

	}

	/**
	 * functions to generate the pair documents in es.
	 * 
	 */
	public String get_id_of_existing_pair_document(String pair_name) {

		String id = null;

		SearchResponse sr = LocalEs.getInstance()
				.prepareSearch("tradegenie_titan").setTypes("pairs")
				.setQuery(termQuery("pair_name", pair_name)).execute()
				.actionGet();

		for (SearchHit hit : sr.getHits().hits()) {

			id = hit.getId();
		}

		return id;

	}

}
