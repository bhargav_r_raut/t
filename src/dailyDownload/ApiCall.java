package dailyDownload;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.dom4j.DocumentException;

import oil.OilScraper;
import metals.MetalsScraper;
import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.StockOperations;
import Titan.TitanBaseConfig;
import Titan.TitanConstants;

import com.thinkaurelius.titan.core.TitanException;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import forex.ForexTest;
import forex.HistoricalQuery;
import genericIndicatorFunctions.HistoricalStockObjectDoesNotContainClose;
import genericIndicatorFunctions.PrepareStockForIndicatorCalculations;
import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.HistoricalStock;
import yahoo_finance_historical_scraper.InputListSizeInsufficient;
import yahoo_finance_historical_scraper.StockConstants;
import yahoo_finance_historical_scraper.StockScraper;

public class ApiCall extends ForexTest {

	@Override
	public HistoricalQuery generateHistoricalQuery(ArrayList<Object> queryString) {
		// TODO Auto-generated method stub
		return super.generateHistoricalQuery(queryString);
	}

	public void scrapeEntity(CreateQueryStringFromDbFields qst, TitanGraph g,
			TitanManagement mgmt, Integer exchange_counter)
			throws ParseException, IllegalArgumentException,
			IllegalAccessException, InputListSizeInsufficient,
			ClassNotFoundException, InterruptedException, ExecutionException,
			MalformedURLException, DocumentException,
			ForexDocumentEmptyException {

		switch (exchange_counter) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:

			StockScraper scraper = new StockScraper();
			QueryResult qr = scraper.scrapeMetal(qst.getQueryString());
			if (!LoggingDailyOperations.get_instance().has_exception()) {
				try {
					AddHistoricalStockQueryResultsToDatabase addToDb = new AddHistoricalStockQueryResultsToDatabase(
							qr, g, mgmt, StockConstants.use_transactional_graph);
				} catch (Exception e) {
					e.printStackTrace();
					LoggingDailyOperations
							.get_instance()
							.add_to_log(
									"exception",
									e,
									"catching all possible exceptions in api call - all stocks",
									null);
				}
			}

			break;

		case 6:
			MetalsScraper mscraper = new MetalsScraper();

			try {
				QueryResult qrm = mscraper.scrapeMetal(qst.getQueryString());
				if (!LoggingDailyOperations.get_instance().has_exception()) {
					AddHistoricalStockQueryResultsToDatabase addToDb = new AddHistoricalStockQueryResultsToDatabase(
							qrm, g, mgmt,
							StockConstants.use_transactional_graph);
				}
			} catch (Exception e) {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								e,
								"catching all possible exceptions in api call - metals",
								null);
			}

			break;

		case 5:
			OilScraper oscraper = new OilScraper();

			try {
				QueryResult orm = oscraper.scrapeMetal(qst.getQueryString());
				if (!LoggingDailyOperations.get_instance().has_exception()) {
					AddHistoricalStockQueryResultsToDatabase addToDb = new AddHistoricalStockQueryResultsToDatabase(
							orm, g, mgmt,
							StockConstants.use_transactional_graph);
				}
			} catch (Exception e) {
				e.printStackTrace();
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e,
						"catching all possible exceptions in api call - oil",
						null);
			}
			break;
		case 7:
			ForexTest ftscraper = new ForexTest();
			QueryResult frm = new QueryResult();

			try {

				frm.setHm(ftscraper.get_forex_values(qst.getQueryString()));
				if (!LoggingDailyOperations.get_instance().has_exception()) {
					AddHistoricalStockQueryResultsToDatabase addToDb = new AddHistoricalStockQueryResultsToDatabase(
							frm, g, mgmt,
							StockConstants.use_transactional_graph);
				}
			} catch (Exception e) {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								e,
								"catching all possible exceptions in api call - forex test.",
								null);
			}
			break;

		default:

			break;

		}

	}

	public void calculateIndicatorsFromQueryString(
			CreateQueryStringFromDbFields qst, TitanGraph g,
			TitanManagement mgmt, Integer exchange_counter)
			throws HistoricalStockObjectDoesNotContainClose {
		/*
		Integer counter = 0;
		HistoricalQuery query = generateHistoricalQuery(qst.getQueryString());
		String indiceName = query.getIndice_names().get(0);
		for (String stockName : query.getForex_names()) {
			String stockFullName = query.getStockFullName().get(counter);
			Calendar start_date = query.getStart_dates().get(counter);
			Calendar end_date = query.getEnd_dates().get(counter);
			CreateRequiredIndicatorsHashMap cr_ind = new CreateRequiredIndicatorsHashMap(
					stockName, stockFullName, indiceName, start_date, end_date);
			PrepareStockForIndicatorCalculations prep_stock = new PrepareStockForIndicatorCalculations(
					stockName, stockFullName, indiceName,
					cr_ind.getRequired_indicators(), start_date.getTimeZone(),
					start_date, end_date, StockConstants.entity_type[counter]);

			counter++;
		}
		*/
		
		ConcurrentHashMap<String, StockOperations> stocks_processed = 
				LoggingDailyOperations.get_instance().getIndividual_stock_operations();
		
		for(Map.Entry<String, StockOperations> entry : stocks_processed.entrySet()){
			
			String stockName = entry.getValue().getStockName();
			String stockFullName = entry.getValue().getStockFullName();
			String indiceName = entry.getValue().getIndice();
			
			//first check whether we have indicators and subindicator complexes generated for this stock or not.
			//if they are not generated, then we have to generate them,
			//how do i do that?
			
			//then for each date, calculate those complexes.
			
			for(String date: entry.getValue().getDates_successfully_added_to_db()){
				
			}
			
		}
		
	}
	


}
