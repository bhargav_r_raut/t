package dailyDownload;

import static org.msgpack.template.Templates.TString;
import static org.msgpack.template.Templates.tList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.thrift.meta_data.SetMetaData;
import org.elasticsearch.action.index.IndexResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.unpacker.Unpacker;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import yahoo_finance_historical_scraper.WikipediaScraper;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.RedisManager;
import Titan.PrepareArrayListForMultiThreadedInjection;

import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import elasticSearchNative.LocalEs;

public class AppendCompanyInformation {
	private HashMap<String, JSONObject> company_information;
	private HashMap<String, String> stocks_with_some_historical_data;
	private String indice;
	private TitanGraph g;
	private TitanManagement mgmt;
	private MessagePack mpack;
	private Template<List<String>> listTmpl;

	public MessagePack getMpack() {
		return mpack;
	}

	public void setMpack(MessagePack mpack) {
		this.mpack = mpack;
	}

	public Template<List<String>> getListTmpl() {
		return listTmpl;
	}

	public void setListTmpl(Template<List<String>> listTmpl) {
		this.listTmpl = listTmpl;
	}

	public TitanManagement getMgmt() {
		return mgmt;
	}

	public void setMgmt(TitanManagement mgmt) {
		this.mgmt = mgmt;
	}

	/**
	 * @return the company_information
	 */
	public HashMap<String, JSONObject> getCompany_information() {
		return company_information;
	}

	/**
	 * @param company_information
	 *            the company_information to set
	 */
	public void setCompany_information(
			HashMap<String, JSONObject> company_information) {
		this.company_information = company_information;
	}

	/**
	 * @return the stocks_with_some_historical_data
	 */
	public HashMap<String, String> getStocks_with_some_historical_data() {
		return stocks_with_some_historical_data;
	}

	/**
	 * @param stocks_with_some_historical_data
	 *            the stocks_with_some_historical_data to set
	 */
	public void setStocks_with_some_historical_data(
			HashMap<String, String> stocks_with_some_historical_data) {
		this.stocks_with_some_historical_data = stocks_with_some_historical_data;
	}

	/**
	 * @return the indice
	 */
	public String getIndice() {
		return indice;
	}

	/**
	 * @param indice
	 *            the indice to set
	 */
	public void setIndice(String indice) {
		this.indice = indice;
	}

	public TitanGraph getG() {
		return g;
	}

	public void setG(TitanGraph g) {
		this.g = g;
	}

	/**
	 * indice components is set in the
	 * add_historical_stock_query_Results_to_database it contains all the stocks
	 * in this indice for which we got some results.
	 * 
	 * now we use it to create the 'got stocks' just get the stocks which had
	 * some historical data for this indice.
	 * 
	 * 
	 * @param company_information
	 * @param indice
	 * @param g
	 * @param mgmt
	 */
	public AppendCompanyInformation(
			HashMap<String, JSONObject> company_information, String indice,
			TitanGraph g, TitanManagement mgmt) {
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				"append_company_information",
				"adding company information for indice:" + indice);
		setCompany_information(company_information);
		IndiceComponents indcomp = IndiceComponents.getInstance();
		HashMap<String, String> got_stocks = indcomp.getIndice_components()
				.get(indice);
		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				"append_company_information",
				"the indice components for the indice were:"
						+ got_stocks.toString());
		setStocks_with_some_historical_data(got_stocks);
		setIndice(indice);
		setG(g);
		setMgmt(mgmt);
		setMpack(new MessagePack());
		setListTmpl(tList(TString));
	}

	public String capitalize_first_letter_of_string(String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1);
	}

	public List<String> get_companies_list_from_msgpack(byte[] bytes,
			MessagePack mpack, Template<List<String>> listTmpl)
			throws IOException {

		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		Unpacker unpacker = mpack.createUnpacker(in);
		List<String> dstList = unpacker.read(listTmpl);

		return dstList;
	}

	public void add_company_to_zone(String stockName, String stockFullName,
			String indiceName, String zone) throws IOException {
		zone = capitalize_first_letter_of_string(WikipediaScraper
				.prepare_string(zone));
		String stockstring = stockName + "_" + stockFullName + "_" + indiceName;
		// so we need to have country, indice centric and global keys for each
		// zone.
		// so each zone is a hash, it has many keys
		// basically each key holds a messagepacked compressed list of the
		// companies.
		// telecom
		
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			ArrayList<String> synonyms = StockConstants.indice_and_country
					.get(indiceName);
			synonyms.add(indiceName);
			synonyms.add(StockConstants.all_companies_name);
			
			for (String synonym : synonyms) {
				if (jedis.hexists(synonym, zone)) {
					List<String> existing_companies_in_zone = get_companies_list_from_msgpack(
							jedis.hget(synonym.getBytes(), zone.getBytes()),
							getMpack(), getListTmpl());
					if (existing_companies_in_zone.contains(stockstring)) {

					} else {
						existing_companies_in_zone.add(stockstring);
						byte[] barr = getMpack().write(
								existing_companies_in_zone);
						jedis.hset(synonym.getBytes(), zone.getBytes(), barr);
					}
				} else {
					List<String> companies_to_add_to_zone = new ArrayList<String>(
							Arrays.asList(stockstring));
					byte[] barr = getMpack().write(companies_to_add_to_zone);
					jedis.hset(synonym.getBytes(), zone.getBytes(), barr);
				}

			}

		}
	}

	public void addStockInformationToGraph() throws JSONException,
			ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InterruptedException, ExecutionException,
			NoStocksInThisIndexFound {
		System.out.println("now entered add stock info to graph, watch output.");
		try {
			System.in.read();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// System.out.println("these are the stocks with the historical data");
		// System.out.println(getStocks_with_some_historical_data().toString());

		PrepareArrayListForMultiThreadedInjection pr = new PrepareArrayListForMultiThreadedInjection();
		ArrayList<JSONObject> additional_params = new ArrayList<JSONObject>();
		if (getStocks_with_some_historical_data() == null) {
			 throw new NoStocksInThisIndexFound("indice is --->" + getIndice()
			 + "no stocks found.");
		} else {
			System.out
					.println("these are the stocks with soem historical data");
			for (Map.Entry<String, String> entry : getStocks_with_some_historical_data()
					.entrySet()) {
				System.out.println(entry.getKey() + " " + entry.getValue());
			}
			for (Map.Entry<String, String> entry : getStocks_with_some_historical_data()
					.entrySet()) {
				// we have to create a vertex with tablename as the indice,and
				// it has a
				// stockname and a stockfullname, and also a
				// here the stockobject is the json object which contains this
				// companies information.
				// that is accessed in the hashmap called company information.
				// System.out.println(entry.getKey());
				// System.out.println(entry.getValue());
				// System.out.println(getCompany_information().toString());
				// System.out.println("endnew here");
				// System.out.println("this is the information map.");
				// System.out
				// .println(getCompany_information().get(entry.getKey()));

				HashMap<String, Object> prm = pr.return_empty_hashmap();
				JSONObject additional_params_local = new JSONObject();

				/*
				 * if(getIndice().contains("forex")){
				 * System.out.println((String)entry.getKey());
				 * System.out.println((String)entry.getValue()); String
				 * stockName = entry.getKey().split("/")[1].trim(); String
				 * stockFullName = entry.getValue().split("/")[1].trim();
				 * getCompany_information().get(stockName).put(
				 * "unique_identifier", entry.getKey() + "_" + stockFullName +
				 * "_" + getIndice()); Iterator itr =
				 * getCompany_information().get(stockName) .keys(); while
				 * (itr.hasNext()) { String key = (String) itr.next();
				 * prm.put(key, getCompany_information().get(stockName)
				 * .get(key)); } prm.put("stockName", stockName);
				 * prm.put("stockFullName", stockFullName); prm.put("tableName",
				 * getIndice()); additional_params_local.add(stockName);
				 * additional_params_local.add(stockFullName);
				 * additional_params_local.add(getIndice()); } else{
				 */
				getCompany_information().get(entry.getKey()).put(
						"unique_identifier",
						entry.getKey() + "_" + entry.getValue() + "_"
								+ getIndice());
				Iterator itr = getCompany_information().get(entry.getKey())
						.keys();
				while (itr.hasNext()) {
					String key = (String) itr.next();
					prm.put(key, getCompany_information().get(entry.getKey())
							.get(key));
				}
				prm.put("stockName", entry.getKey());
				prm.put("stockFullName", entry.getValue());
				prm.put("tableName", getIndice());
				additional_params_local.put("stockName", entry.getKey());
				additional_params_local.put("stockFullName", entry.getValue());
				additional_params_local.put("tableName", getIndice());

				// }

				// System.out.println("thei is the entry key and value-->"
				// + entry.getKey() + "  -->" + entry.getValue());
				pr.getFull_list().add(prm);
				additional_params.add(additional_params_local);

			}
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					"append_company_information",
					"now starting multithread graph add job for adding company information, total "
							+ "elements in the list were:"
							+ pr.getFull_list().size());
			
			for (HashMap<String, Object> hm : pr.getFull_list()) {
				// IndexRequest ind = new IndexRequest("tradegenie_titan",
				// "stock_details");
				// ind.source(hm);
				System.out.println("this is the hashmap going in:" + hm);
				
				try {
					IndexResponse res = LocalEs.getInstance()
							.prepareIndex("tradegenie_titan", "stock_details")
							.setSource(hm).execute().actionGet();
					if (res.getId() == null) {
						LoggingDailyOperations
								.get_instance()
								.getIndividual_stock_operations()
								.get(hm.get("stockName") + "_"
										+ hm.get("stockFullName") + "_"
										+ hm.get("tableName"))
								.setFailed_to_add_company_information(true);
					} else {
						System.out
								.println("successfully added es information to graph..");
						LoggingDailyOperations
								.get_instance()
								.getIndividual_stock_operations()
								.get(hm.get("stockName") + "_"
										+ hm.get("stockFullName") + "_"
										+ hm.get("tableName"))
								.setFailed_to_add_company_information(false);
						String[] zones = (String[]) hm.get("zones");
						for (String zone : zones) {
							add_company_to_zone((String) hm.get("stockName"),
									(String) hm.get("stockFullName"),
									(String) hm.get("tableName"), zone);
						}
						

					}
				} catch (Exception e) {
					LoggingDailyOperations
							.get_instance()
							.getIndividual_stock_operations()
							.get(hm.get("stockName") + "_"
									+ hm.get("stockFullName") + "_"
									+ hm.get("tableName"))
							.setFailed_to_add_company_information(true);
				}

			}

			// MultiThreadGraphAddJob mt = new MultiThreadGraphAddJob(
			// pr.getFull_list(), additional_params, getG(),
			// getMgmt(), false, "add_sector_industry_information");
			LoggingDailyOperations
					.get_instance()
					.add_to_log("info", null, "append_company_information",
							"finished multi thread graph add job of adding elements to the list.");
		}
	}
}
