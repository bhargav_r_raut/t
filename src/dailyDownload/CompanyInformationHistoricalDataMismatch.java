package dailyDownload;

public class CompanyInformationHistoricalDataMismatch extends Exception {
	private String message;
	public CompanyInformationHistoricalDataMismatch(String message){
		super(message);
	}
}
