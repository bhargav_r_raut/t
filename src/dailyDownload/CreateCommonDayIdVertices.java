package dailyDownload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import yahoo_finance_historical_scraper.CommonDayConnectorEdge;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import Titan.ExecuteMultiThreadedJob;
import Titan.GroovyStatements;

import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Vertex;

public class CreateCommonDayIdVertices {

	private GregorianCalendar start_from;
	private GregorianCalendar end_till;
	private static final String identifier_for_log = "create_common_day_id_vertice";

	public GregorianCalendar getStart_from() {
		return start_from;
	}

	public void setStart_from(GregorianCalendar start_from) {
		this.start_from = start_from;
	}

	public GregorianCalendar getEnd_till() {
		return end_till;
	}

	public void setEnd_till(GregorianCalendar end_till) {
		this.end_till = end_till;
	}

	private TitanGraph g;

	/**
	 * just pass in the graph, it will automatically calculate all the results.
	 * no need to call any functions.
	 * 
	 * start from 2007 first january at 12.00 am.
	 * 
	 * ends at 2020 first january at 12.00 am.
	 * 
	 * @param start_from
	 * @param end_till
	 * @param g
	 */
	public CreateCommonDayIdVertices(TitanGraph g, TitanManagement mgmt) {

		GregorianCalendar start_common_day_ids_from = new GregorianCalendar();
		start_common_day_ids_from.setTimeZone(TimeZone.getTimeZone("UTC"));
		start_common_day_ids_from.set(2007, 0, 1, 0, 0, 0);

		GregorianCalendar end_common_day_ids_from = new GregorianCalendar();
		end_common_day_ids_from.setTimeZone(TimeZone.getTimeZone("UTC"));
		end_common_day_ids_from.set(2016, 0, 1, 0, 0, 0);

		setStart_from(start_common_day_ids_from);
		setEnd_till(end_common_day_ids_from);
		create_common_day_id_vertices(g, mgmt);
	}

	public void create_common_day_id_vertices(TitanGraph g, TitanManagement mgmt) {

		Boolean proceed = GroovyStatements.getInstance()
				.check_if_common_day_ids_exist();

		if (proceed && !LoggingDailyOperations.get_instance().has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"now starting to create common day id vertices.");

			Long start_time = getStart_from().getTimeInMillis();
			double start_day_id = start_time / (86400000.0);
			Integer start_id = (int) start_day_id;

			/***
			 * CAUTION CHANGE THIS LATER.
			 * 
			 * 
			 */

			// start_id = 18200;

			/***
			 * end caution.
			 * 
			 * 
			 */

			Long end_time = getEnd_till().getTimeInMillis();
			double end_day_id = end_time / (86400000.0);
			Integer end_id = (int) end_day_id;

			System.out.println("start common day id is:" + start_id
					+ "end id is:" + end_id);
			System.out.println("difference is:" + (end_id - start_id));
			//try {
			//	System.in.read();
			//} catch (IOException e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
			
			ArrayList<Long> common_day_vertex_ids = new ArrayList<Long>();
			for (int i = start_id; i < end_id; i++) {
				TitanVertex v = g
						.addVertexWithLabel("common_day_id_vertex_label");
				v.setProperty("common_day_id", i);
				common_day_vertex_ids.add((Long)v.getId());
				g.commit();
				System.out.println("created day id--->" + (i - start_id));
			}
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"now finished creating common day id vertices."
							+ "and now gonig to draw the edges between them.");

			//draw_day_connector_edges_between_common_day_ids(start_id, end_id,
			//		g, mgmt,common_day_vertex_ids);
		}

	}

	public void draw_day_connector_edges_between_common_day_ids(
			Integer start_id, Integer end_id, TitanGraph g, 
			TitanManagement mgmt, ArrayList<Long> common_day_vertex_ids) {
		ArrayList<CommonDayConnectorEdge> cmain = new ArrayList<CommonDayConnectorEdge>();
		for (int i = 0; i < end_id - start_id; i++) {
			
			if(i > 0){
				
				Integer sublist_index_start = 0;
				
				if(i > StockConstants.total_days_to_connect_by_day_connector){
					sublist_index_start = i - StockConstants.total_days_to_connect_by_day_connector;
				}
				System.out.println("sublist index start:" + sublist_index_start);
				System.out.println("sublist index end:" + i);
				
				//now you take a sublist of the array.
				List<Long> destination_vertex_sublist = common_day_vertex_ids.subList(sublist_index_start, i);
				Vertex root_vertex = g.getVertex(common_day_vertex_ids.get(common_day_vertex_ids.size()-1));
				for(Long destination_vertex_id : destination_vertex_sublist){
					CommonDayConnectorEdge cde = 
							new CommonDayConnectorEdge(root_vertex, g.getVertex(destination_vertex_id));
					
					cmain.add(cde);
				}
			}
			else{
				//no point in drawing any edges.
			}
			
			//ArrayList<CommonDayConnectorEdge> cde = GroovyStatements
			//		.getInstance()
			//		.generate_common_day_id_day_connector_edges(i);
			//cmain.addAll(cde);
			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"finished generating the edges for :" + (i));
		}
		
		
		System.out.println("total edges in array are:" + cmain.size());
		//try {
		//	System.in.read();
		//} catch (IOException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		ArrayList<org.json.JSONObject> arr = new ArrayList<org.json.JSONObject>();
		ExecuteMultiThreadedJob exm = new ExecuteMultiThreadedJob(cmain, g,
				mgmt, "draw_common_day_connector_edge", false, arr);

	}

}
