package dailyDownload;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.StockOperations;
import Titan.TitanConstants;

/**
 * @author aditya
 *
 */
public class CreateQueryStringFromDbFields implements TitanConstants{
	

	private Calendar dbdate;
	public Calendar getDbdate() {
		return dbdate;
	}


	public void setDbdate(Calendar dbdate) {
		this.dbdate = dbdate;
	}


	private ArrayList<Object> queryString;
	/*
	 * arraylist with elements in this order.
	 * 
	 * stockname
	 * start date - calendar object
	 * end date - calendar object
	 * indicename
	 * stockfullname
	 * 
	 * 
	 * 
	 */
	public CreateQueryStringFromDbFields(){
		this.queryString = new ArrayList<Object>();
	}
	
	
	public void appendStockName(String stockName){
		
		this.getQueryString().add(stockName);
	
	}
	
	public void appendIndiceName(String indiceName){
		this.getQueryString().add(indiceName);
	}
	
	public void appendStockFullName(String stockFullName){
		this.getQueryString().add(stockFullName);
	}
	
	
	/**
	 * @param dates : this is a single date either from the database or a custom date
	 * defined in the titan constants file format as date_string_format_in_database
	 * @param timezoneString: this is the timezone id , that is stored under the propertykey
	 * of timezoneString.
	 * @throws ParseException
	 */
	public void appendDbDate(String dates,
			String timezoneString) throws ParseException{
		
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_STRING_FORMAT_IN_DATABASE);
		sdf.setTimeZone(TimeZone.getTimeZone(timezoneString));
		Date d = sdf.parse(dates);
		Calendar cal = new GregorianCalendar();
		cal.setTime(d);
		cal.setTimeZone(TimeZone.getTimeZone(timezoneString));
		this.dbdate = cal;
		this.getQueryString().add(cal);
		
	}
	
	/**
	 * @param timezoneString : it must be the timezone from the database 
	 * propertykey defined as timezoneString
	 *  of the element that you 
	 * want to get the querystring representation of .
	 */
	public void appendTodayDate(
			String timezoneString){
		
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timezoneString));
		this.getQueryString().add(cal);	
		
	}
	
	public ArrayList<Object> getQueryString() {
		return queryString;
	}


	public void setQueryString(ArrayList<Object> queryString) {
		this.queryString = queryString;
	}
	
	public void add_query_string_data_to_loggins_operations(){
		Integer counter = 0;
		String stockName = null;
		String stockFullName = null;
		String indice = null;
		String unique_name = null;
		GregorianCalendar start_date = null;
		GregorianCalendar end_date = null;
		for(Object o : getQueryString()){
			if(counter.equals(0)){
				//unique_name = (String)o;
				stockName = (String)o;
			}
			else if(counter.equals(1)){
				start_date = (GregorianCalendar)o;
			}
			else if(counter.equals(2)){
				end_date = (GregorianCalendar)o;
			}
			else if(counter.equals(3)){
				//unique_name = unique_name + "_" + (String)o;
				indice = (String)o;
				
			}
			else if(counter.equals(4)){
				stockFullName = (String)o;
				
				//add start and end dates to logging operations.
		        unique_name = stockName + "_" + stockFullName + "_" + indice;
		        //System.out.println("this is the unique name.");
		        //System.out.println(unique_name);
		        
		        //System.out.println("this is the hashmap of the individual stock operations at the moment");
		        //System.out.println(LoggingDailyOperations.get_instance().individual_stock_operations.keySet().toString());
		        
		        
				LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations().
				get(unique_name).setQuery_start_date(start_date);
				
				LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations().
				get(unique_name).setQuery_end_date(end_date);
				
				//reset
				counter = -1;
				unique_name = null;
				stockFullName = null;
				stockName = null;
				start_date = null;
				end_date = null;
			}
			counter ++;
		}
	}

	
	
}
