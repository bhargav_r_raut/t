package dailyDownload;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import Titan.GroovyStatements;

public class CreateRequiredIndicatorsHashMap {
	
	private HashMap<Long,ArrayList<String>> required_indicators;

	public HashMap<Long, ArrayList<String>> getRequired_indicators() {
		
		return required_indicators;
	}
	public void setRequired_indicators(
			HashMap<Long, ArrayList<String>> required_indicators) {
		this.required_indicators = required_indicators;
	}
	
	public CreateRequiredIndicatorsHashMap(String stockName, String stockFullName, String indiceName,
			Calendar start_date, Calendar end_date) {
		this.required_indicators = GroovyStatements.getInstance().
				indicators_to_be_calculated_for_stock(stockName, stockFullName, indiceName, start_date, end_date);
		
	}
		
	
}
