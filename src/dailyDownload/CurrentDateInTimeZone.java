package dailyDownload;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import singletonClass.CalendarFunctions;

public class CurrentDateInTimeZone {
	
	private String timezoneString;
	
	public CurrentDateInTimeZone(String timezoneString){
		this.timezoneString = timezoneString;
	}
	
	public String getCurrentDateInThatTimeZone(){
		return new CalendarFunctions(new GregorianCalendar
				(TimeZone.getTimeZone(this.timezoneString)))
		.getFullDateWithoutTimeZone();
	}
}
