package dailyDownload;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;

import Titan.MultiThreadable;
import elasticSearchNative.RemoteEs;

public class DogTest extends MultiThreadable {
	public static boolean compare_two_double_arrays(ArrayList<Double> arr1, ArrayList<Double>arr2){
		if(arr1.size() != arr2.size()){
			System.out.println("size was different.");
			return false;
		}
		
		for(Double ob: arr1){
			if(!arr2.contains(ob)){
				return false;
			}
		}
		
		return true;
	}
	public static void main(String[] args){
		SearchResponse sr = RemoteEs.getInstance()
				.prepareSearch("tradegenie_titan").setTypes("indicator")
				.setQuery(QueryBuilders.
						termQuery("indicator_and_period_name", "single_ema_indicator_period_start_12_period_end"))
				.setFetchSource(true).execute().actionGet();
	}
}
