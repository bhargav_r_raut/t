package dailyDownload;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.ScriptService;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.javatuples.Pair;
import org.json.JSONException;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import yahoo_finance_historical_scraper.ComponentScraper;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LogOnShutdown;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.JedisPopper;
import Indicators.RedisManager;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;
import Titan.TitanBaseConfig;
import Titan.TitanConstants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import elasticSearchNative.BulkLoader;
import elasticSearchNative.ClientInstance;
import elasticSearchNative.LocalEs;
import elasticSearchNative.IBulkLoader;

public class Download implements TitanConstants, IBulkLoader {
	public static final String identifier_for_log = "Download.java";
	public static HashMap<String, ArrayList<String>> last_reliable_dates_map;

	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static void clear_graph(TitanGraph g) {
		Iterator<Vertex> vertices = g.getVertices().iterator();
		while (vertices.hasNext()) {
			Vertex v = vertices.next();
			g.removeVertex(v);
			g.commit();
		}

		Iterator<Edge> edges = g.getEdges().iterator();
		while (edges.hasNext()) {
			Edge e = edges.next();
			g.removeEdge(e);
			g.commit();
		}
	}

	public Pair<Long, String> add_new_session_to_elasticsearch() {
		Map<String, Object> new_session = new LinkedHashMap<String, Object>();
		Long new_session_time = new GregorianCalendar().getTimeInMillis();
		new_session.put("session_start_epoch", new_session_time);
		new_session.put("last_reliable_end_dates", new ArrayList<String>());
		IndexResponse indexresp = LocalEs.getInstance()
				.prepareIndex("tradegenie_titan", "session_info")
				.setSource(new_session).execute().actionGet();

		if (indexresp.isCreated()) {
			Pair<Long, String> p = new Pair<Long, String>(new_session_time,
					indexresp.getId());
			return p;
		}

		return new Pair<Long, String>(null, null);

	}

	public void build_last_reliable_date_hashmap() throws IOException {
		this.last_reliable_dates_map = new HashMap<String, ArrayList<String>>();
		// .internalBuilder(
		// new SearchSourceBuilder()
		// ReadWriteTextToFile
		// .getInstance()
		// .readFile(
		// StockConstants.path_to_saved_queries
		// +
		// StockConstants.name_of_file_holding_saved_query_for_last_successfully_session,
		// TitanConstants.ENCODING))
		SearchRequestBuilder requestBuilder = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("session_info")
				.setQuery(
						QueryBuilders
								.termQuery("session_successfully_ended", 1))
				.setFetchSource(new String[] { "last_reliable_end_dates" },
						null).addSort("session_start_epoch", SortOrder.DESC)
				.setSize(1);

		SearchResponse response = requestBuilder.execute().actionGet();

		for (SearchHit hit : response.getHits()) {
			// System.out.println("got a hit.");
			// Map<String, SearchHitField> fields = hit.getFields();
			// System.out.println(fields);
			// SearchHitField sf = fields.get("last_reliable_end_dates");
			List<Object> last_reliable_dates = (List<Object>) hit.getSource()
					.get("last_reliable_end_dates");
			// System.out.println("these are the last reliable dates:" +
			// last_reliable_dates);
			for (Object o : last_reliable_dates) {
				// /this is basically a map of strings and the
				// stockdetails.
				Map<String, Object> sm = (Map<String, Object>) o;
				final String stockName = (String) sm.get("stockName");
				final String stockFullName = (String) sm.get("stockFullName");
				final String last_reliable_end_Date = (String) sm
						.get("last_reliable_end_date");
				last_reliable_dates_map
						.put(stockName + "_" + stockFullName,
								new ArrayList<String>(
										Arrays.asList(new String[] { last_reliable_end_Date })));

			}
		}

		// System.out.println("this is the last reliable dates arraylist");
		// System.out.println(last_reliable_dates_map);
		// System.in.read();

	}

	/**
	 * 
	 * look for it in the map. if it is not there. then use the default start
	 * date and use the default timezonestring.
	 * 
	 * 
	 * 
	 * @param stockName
	 * @param stockFullName
	 * @return 0.last date -> as string representation 1.
	 * 
	 */
	public static ArrayList<String> get_last_date_and_time(String stockName,
			String stockFullName, Integer exchange_counter) {
		ArrayList<String> last_date_and_time = new ArrayList<String>();

		if (last_reliable_dates_map
				.containsKey(stockName + "_" + stockFullName)) {
			return new ArrayList<String>(Arrays.asList(new String[] {
					last_reliable_dates_map
							.get(stockName + "_" + stockFullName).get(0),
					StockConstants.default_timezoneStrings[exchange_counter] }));

		} else {
			// System.out.println("didnt get this one:" + stockName);
			return new ArrayList<String>(Arrays.asList(new String[] {
					StockConstants.default_start_dates[exchange_counter],
					StockConstants.default_timezoneStrings[exchange_counter] }));
		}

		/**
		 * try { XContentBuilder builder = jsonBuilder().startObject()
		 * .startObject("query").startObject("filtered")
		 * .startObject("query").field("match_all").startObject()
		 * .endObject().endObject().startObject("filter")
		 * .startObject("bool").startArray("must").startObject()
		 * .startObject("term").field("stockName").value(stockName)
		 * .endObject().endObject().startObject().startObject("term")
		 * .field("stockFullName").value(stockFullName).endObject()
		 * .endObject().endArray().endObject().endObject().endObject()
		 * .endObject().endObject();
		 * 
		 * //XContentBuilder builder = jsonBuilder().startObject() //
		 * .startObject("query").field("match_all").endObject() // .endObject();
		 * 
		 * SearchRequestBuilder requestBuilder = ElasticClientHolder
		 * .getInstance().prepareSearch("tradegenie_titan")
		 * .setTypes("historical_data_point").setSource(builder)
		 * .addField("dates"
		 * ).addField("timezoneString").addField("epoches").setSize(1)
		 * .addSort("epoches", SortOrder.DESC);
		 * 
		 * //SearchRequestBuilder requestBuilder = ElasticClientHolder //
		 * .getInstance() // .prepareSearch("tradegenie_titan") //
		 * .setTypes("session_info") // .setSource(builder) //
		 * .setFetchSource(new String[] { "last_reliable_end_dates" }, // new
		 * String[] {}).addSort("epoches", SortOrder.DESC) // .setSize(1);
		 * 
		 * SearchResponse response = requestBuilder.execute().actionGet();
		 * 
		 * for (SearchHit hit : response.getHits().hits()) {
		 * last_date_and_time.add(hit.field("dates").value().toString());
		 * last_date_and_time.add(hit.field("timezoneString").value()
		 * .toString());
		 * last_date_and_time.add(hit.field("epoches").toString()); }
		 * 
		 * //if (response.getHits().hits().length > 0) {
		 * 
		 * // for (SearchHit hit : response.getHits()) { // Map<String,
		 * SearchHitField> fields = hit.getFields(); // SearchHitField sf =
		 * fields.get("last_reliable_end_dates"); // List<Object>
		 * last_reliable_dates = sf.getValues(); // for (Object o :
		 * last_reliable_dates) { // /this is basically a map of strings and the
		 * // stockdetails.
		 * 
		 * // } // } //} else {
		 * 
		 * //}
		 * 
		 * // System.out.println("stockName:" + stockName + " dates :" + //
		 * last_date_and_time.toString());
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * return last_date_and_time;
		 **/
	}

	public static Integer get_day_id(Long epoch) {
		double number_of_days = epoch / (86400000.0);
		return (int) number_of_days;
	}

	public static GregorianCalendar convert_day_id_to_calendar(Integer day_id) {
		Long epoch = day_id.longValue() * 86400000;
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(epoch);
		return calendar;
	}

	public static Integer get_quarter_from_date(GregorianCalendar d) {
		Integer quarter = 1;

		if (d.get(d.MONTH) <= 3) {

		} else if (d.get(d.MONTH) > 3 && d.get(d.MONTH) <= 6) {
			quarter = 2;
		} else if (d.get(d.MONTH) > 6 && d.get(d.MONTH) <= 9) {
			quarter = 3;
		} else {
			quarter = 4;
		}

		return quarter;
	}

	public Long get_count_of_es_pair_docs() {
		CountResponse response = LocalEs.getInstance()
				.prepareCount("tradegenie_titan").setTypes("pairs")
				.setQuery(matchAllQuery()).execute().actionGet();
		return response.getCount();
	}

	public void check_or_create_pair_documents() {
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log, "starting to check total pairs in es");
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			Pipeline pipe = jedis.pipelined();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log, "entered to count the pairs in es");

			Long length_of_keys_in_redis_hash = jedis.hlen("pair_es_ids");
			if (get_count_of_es_pair_docs().equals(
					StockConstants.total_pairs_that_should_exist_in_es)
					&& length_of_keys_in_redis_hash
							.equals(StockConstants.total_pairs_that_should_exist_in_es)) {
				LoggingDailyOperations.get_instance().add_to_log("info", null,
						identifier_for_log,
						"pairs are equal to expected amount.");
			} else {

				LoggingDailyOperations
						.get_instance()
						.add_to_log("info", null, identifier_for_log,
								"pairs are less than expected amount, now deleting the pairs docs from es.");

				DeleteByQueryResponse del_response = LocalEs
						.getInstance().prepareDeleteByQuery("tradegenie_titan")
						.setTypes("pairs").setQuery(matchAllQuery()).execute()
						.actionGet();

				if (del_response.status().toString().equals("OK")) {

				} else {
					LoggingDailyOperations
							.get_instance()
							.add_to_log(
									"exception",
									new Exception(
											"es gave something other than ok while trying to delete pair"
													+ " docs because there were insufficient pair docs, here is the response:"
													+ del_response.status()
															.toString()),
									identifier_for_log, null);
					LoggingDailyOperations.get_instance()
							.exit_without_graph_and_log_before_exiting();
				}

				LoggingDailyOperations.get_instance().add_to_log(
						"info",
						null,
						identifier_for_log,
						"finished deleting , here is the delete response: "
								+ del_response.status().toString());

				LoggingDailyOperations.get_instance().add_to_log("info", null,
						identifier_for_log,
						"now flushing the redis es pair ids");

				if (jedis.exists("pair_es_ids")) {
					Long redis_del_response = jedis.del("pair_es_ids");

					if (redis_del_response != 1) {
						LoggingDailyOperations
								.get_instance()
								.add_to_log(
										"exception",
										new Exception(
												"redis gave a false for the deleting of the pair es ids key"),
										identifier_for_log, null);
					}

					LoggingDailyOperations.get_instance().add_to_log(
							"info",
							null,
							identifier_for_log,
							"flushed redis of es pair ids here is the respons: "
									+ String.valueOf(redis_del_response));
				} else {
					LoggingDailyOperations
							.get_instance()
							.add_to_log("info", null, identifier_for_log,
									"redis key of pair_es_ids did not exist, so no need to delete.");
				}
				BulkLoader bulkLoader = new BulkLoader(3000,
						"tradegenie_titan", "pairs");
				// start day ids from 2006 go upto 2016.
				GregorianCalendar cal_start = new GregorianCalendar(2006, 0, 1);
				GregorianCalendar cal_end = new GregorianCalendar(2016, 0, 1);
				Long start_epoch = cal_start.getTimeInMillis();
				Long end_epoch = cal_end.getTimeInMillis();
				Integer start_day_id = get_day_id(start_epoch);
				Integer end_day_id = get_day_id(end_epoch);
				Integer total_done = 0;

				for (int i = end_day_id; i > start_day_id; i--) {

					Integer internal_counter = 0;
					for (int j = i - 1; j > i - 300; j--) {
						internal_counter++;
						total_done++;
						GregorianCalendar start_cal = convert_day_id_to_calendar(j);

						GregorianCalendar end_cal = convert_day_id_to_calendar(i);

						/**
						 * pair name
						 */
						String pair_name = String.valueOf(j) + "_"
								+ String.valueOf(i);

						/**
						 * pair difference
						 * 
						 */
						Integer pair_difference = i - j;

						/**
						 * create tpair.
						 * 
						 */

						TPair tpair = new TPair(pair_name, pair_difference);

						/**
						 * 
						 * month combination.
						 */
						String month_combination = new SimpleDateFormat("MMM")
								.format(start_cal.getTime())
								+ "_"
								+ new SimpleDateFormat("MMM").format(end_cal
										.getTime());
						String pair_month_start = new SimpleDateFormat("MMM")
								.format(start_cal.getTime());
						String pair_month_end = new SimpleDateFormat("MMM")
								.format(end_cal.getTime());
						HashMap<String, Object> month = new HashMap<String, Object>();
						month.put("start_month", pair_month_start);
						month.put("end_month", pair_month_end);
						month.put("month_combination", month_combination);
						tpair.getMonth().add(month);

						/**
						 * 
						 * week of month combination.
						 */
						Integer start_week = start_cal
								.get(GregorianCalendar.WEEK_OF_MONTH);
						Integer end_week = end_cal
								.get(GregorianCalendar.WEEK_OF_MONTH);
						String week_combination = start_week.toString() + "_"
								+ end_week.toString();
						HashMap<String, Object> week_of_month = new HashMap<String, Object>();
						week_of_month.put("start_week", start_week);
						week_of_month.put("end_week", end_week);
						week_of_month.put("week_combination", week_combination);
						tpair.getWeek_of_month().add(week_of_month);

						/**
						 * 
						 * day of week combination.
						 * 
						 */
						String start_day_of_week = new SimpleDateFormat("EEEE")
								.format(start_cal.getTime());
						String end_day_of_week = new SimpleDateFormat("EEEE")
								.format(end_cal.getTime());
						String day_of_week_combination = start_day_of_week
								.toString() + "_" + end_day_of_week.toString();
						HashMap<String, Object> day_of_week = new HashMap<String, Object>();
						day_of_week.put("start_day", start_day_of_week);
						day_of_week.put("end_day", end_day_of_week);
						day_of_week.put("day_combination",
								day_of_week_combination);
						tpair.getDay_of_week().add(day_of_week);
						/**
						 * quarter
						 * 
						 * 
						 */
						Integer start_quarter = get_quarter_from_date(start_cal);
						Integer end_quarter = get_quarter_from_date(end_cal);
						String quarter_combination = start_quarter.toString()
								+ "_" + end_quarter.toString();
						HashMap<String, Object> quarter = new HashMap<String, Object>();
						quarter.put("start_quarter", start_quarter);
						quarter.put("end_quarter", end_quarter);
						quarter.put("quarter_combination", quarter_combination);
						tpair.getQuarter().add(quarter);
						/**
						 * 
						 * 
						 * year
						 * 
						 */
						Integer start_year = start_cal.get(Calendar.YEAR);
						Integer end_year = end_cal.get(Calendar.YEAR);
						String year_combination = start_year.toString() + "_"
								+ end_year.toString();
						HashMap<String, Object> year = new HashMap<String, Object>();
						year.put("start_year", start_year);
						year.put("end_year", end_year);
						year.put("year_combination", year_combination);
						tpair.getYear().add(year);
						// System.out.println(tpair.get_map_representation());
						/**
						 * day id start and day id end.
						 * 
						 */

						Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> 
						bulk_add_response = bulkLoader
								.add_item_to_bulk_loader(tpair
										.get_map_representation());

						if (bulk_add_response != null) {
							process_response(bulk_add_response);
							for (HashMap<String, Object> success_item : bulk_add_response
									.getValue0()) {
								String id = (String) success_item.get("id");
								Map<String, Object> item_vertex_map = (Map<String, Object>) success_item
										.get("item");
								String success_pair_name = (String) item_vertex_map
										.get("pair_name");
								pipe.hset("pair_es_ids", success_pair_name, id);
								if (total_done % 10000 == 0) {
									pipe.sync();
								}
							}

						}
					}

					// System.out.println("now finished i:" + i +
					// " total done for i:" + internal_counter +
					// " total done all:" + total_done);

				}

				//
				// get rid of the residue.
				Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> final_bulk_add_response = bulkLoader
						.index_documents_and_flush_bulk();
				if (final_bulk_add_response != null) {
					process_response(final_bulk_add_response);
					for (HashMap<String, Object> success_item : final_bulk_add_response
							.getValue0()) {
						String id = (String) success_item.get("id");
						Map<String, Object> item_vertex_map = (Map<String, Object>) success_item
								.get("item");
						String success_pair_name = (String) item_vertex_map
								.get("pair_name");
						pipe.hset("pair_es_ids", success_pair_name, id);
						if (total_done % 10000 == 0) {
							pipe.sync();
						}
					}

				}
				pipe.sync();

				LoggingDailyOperations.get_instance().add_to_log("info", null,
						identifier_for_log,
						"finished adding the pair documents to es and redis.");

				Thread.sleep(1000);

				Long recheck_length_of_keys_in_redis_hash = jedis
						.hlen("pair_es_ids");

				if (get_count_of_es_pair_docs().equals(
						StockConstants.total_pairs_that_should_exist_in_es)
						&& recheck_length_of_keys_in_redis_hash
								.equals(StockConstants.total_pairs_that_should_exist_in_es)) {
					LoggingDailyOperations.get_instance().add_to_log(
							"info",
							null,
							identifier_for_log,
							"the total count of pairs in es and redis are equal to the expectation"
									+ "after their additoin.");
				} else {
					LoggingDailyOperations
							.get_instance()
							.add_to_log(
									"info",
									new Exception(
											"did not find expected es and redis counts for the pairs,"
													+ "after their additions: + counts are: es"
													+ get_count_of_es_pair_docs()
													+ " redis :"
													+ recheck_length_of_keys_in_redis_hash),
									identifier_for_log, null);

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
		}

	}

	public void scrape_exchanges(TitanGraph g, TitanManagement mgmt){
		if (!LoggingDailyOperations.get_instance().has_exception()) {

			ApiCall api = new ApiCall();
			ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();
			Integer exchange_counter = 0;

			for (String indiceName : StockConstants.indices_main) {
				// System.out.println("exchange counter is :" +
				// exchange_counter);
				if (exchange_counter == 7) {

					try {

						LoggingDailyOperations.get_instance().add_to_log(
								"info",
								null,
								"daily_download",
								"doing exchange --->"
										+ String.valueOf(exchange_counter));

						LoggingDailyOperations.get_instance().add_to_log(
								"info",
								null,
								"daily_download",
								"starting component scraping of exchange--->"
										+ String.valueOf(exchange_counter));

						ComponentScraper csr = ComponentScraper
								.getInstance(exchange_counter);
						HashMap<String, ArrayList<String>> index_names_and_stock_names = csr
								.getIndex_names_and_stock_names();

						if (!LoggingDailyOperations.get_instance()
								.has_exception()) {
							HashMap<String, JSONObject> company_information = csr
									.getCompany_information();

							CreateQueryStringFromDbFields qst = new CreateQueryStringFromDbFields();
							String start_date = StockConstants.default_start_dates[exchange_counter];
							String timezoneString = StockConstants.default_timezoneStrings[exchange_counter];
							CurrentDateInTimeZone cd = new CurrentDateInTimeZone(
									timezoneString);
							String end_date = cd.getCurrentDateInThatTimeZone();

							/****
							 * 
							 * get rid of this line afterwards.
							 * 
							 */
							end_date = "2015-08-02 23:34:43.878";
							// System.out.println(end_date);
							// System.in.read();

							// results =
							// (ArrayList)searchExchange(indiceName);

							results = (ArrayList) GroovyStatements
									.getInstance().searchElasticExchange(
											indiceName);
							// System.out.println("this is what is stored in elasticsearch.");
							// System.out.println(results);
							// System.in.read();
							// results contain all the elements or stocks
							// that
							// already
							// exist.

							if (results.size() == 0) {

								for (ArrayList<String> arraylist : index_names_and_stock_names
										.values()) {
									Integer name_counter = 0;
									for (String stock : arraylist) {
										if (name_counter == 0) {
											qst.appendStockName(stock);
										} else if (name_counter == 1) {
											qst.appendDbDate(start_date,
													timezoneString);
											qst.appendDbDate(end_date,
													timezoneString);
											// System.out.println("this is the indice name-->"
											// + indiceName);
											qst.appendIndiceName(indiceName);
											qst.appendStockFullName(stock);
										}

										name_counter++;
										if (name_counter > 1) {
											name_counter = 0;
										}
									}
								}
							} else {
								ArrayList<String> stockNames_already_existing_in_graph = new ArrayList<String>();

								for (HashMap<String, String> hm : results) {

									for (Map.Entry entry : hm.entrySet()) {
										stockNames_already_existing_in_graph
												.add((String) entry.getValue());
									}
								}

								// System.out.println("these are the stockNAMES ALREADY EXISTING IN es.");
								// System.out.println(stockNames_already_existing_in_graph);

								// System.in.read();

								// System.out.println("these are the index names and stock names, scraped by component scraper.");
								// System.out.println(index_names_and_stock_names.values());
								Boolean query_last_date = true;
								for (ArrayList<String> arraylist : index_names_and_stock_names
										.values()) {
									Integer name_counter = 0;

									for (String stock : arraylist) {

										if (name_counter == 0) {

											qst.appendStockName(stock);

											if (stockNames_already_existing_in_graph
													.contains(stock)) {

												query_last_date = true;

											} else {
												/**
												 * setting this to always be
												 * true. so that we always enter
												 * the query last date part of
												 * the loop as per our new
												 * stuff.
												 */
												query_last_date = true;
											}

										} else if (name_counter == 1) {

											ArrayList<String> dateFindings = new ArrayList<String>();

											if (query_last_date) {
												dateFindings = get_last_date_and_time(

														(String) qst
																.getQueryString()
																.get(qst.getQueryString()
																		.size() - 1),
														stock, exchange_counter);
												// System.out.println("these are the datefindings:");
												// System.out.println(dateFindings);
												// System.in.read();
												if (dateFindings.size() == 0) {
													LoggingDailyOperations
															.get_instance()
															.add_to_log(
																	"exception",
																	new TimeZoneStringInvalid(
																			"stock was existing in database"
																					+ "but we could not find any last date for "
																					+ "it, name of stock is--->"
																					+ stock
																					+ ", exchange counter is-->"
																					+ exchange_counter),
																	"daily_download",
																	null);
													throw new TimeZoneStringInvalid(
															"stock was existing in database"
																	+ "but we could not find any last date for "
																	+ "it, name of stock is--->"
																	+ stock
																	+ ", exchange counter is-->"
																	+ exchange_counter);
												} else if (!timezoneString
														.contains(dateFindings
																.get(1))) {
													LoggingDailyOperations
															.get_instance()
															.add_to_log(
																	"exception",
																	new TimeZoneStringInvalid(
																			"stock was existing in database"
																					+ "but there was a timezonestring mismatch."
																					+ "it, name of stock is--->"
																					+ stock
																					+ ", exchange counter is-->"
																					+ exchange_counter),
																	"daily_download",
																	null);
													throw new TimeZoneStringInvalid(
															"the timezone from the graph does not match the timezone that we have statically got.");
												} else {

													qst.appendDbDate(
															dateFindings.get(0),
															timezoneString);
												}
											} else {
												qst.appendDbDate(start_date,
														timezoneString);
											}

											qst.appendDbDate(end_date,
													timezoneString);
											qst.appendIndiceName(indiceName);
											qst.appendStockFullName(stock);
										}

										name_counter++;
										if (name_counter > 1) {
											name_counter = 0;
										}
									}
								}

							}

							if (!LoggingDailyOperations.get_instance()
									.has_exception()) {

								qst.add_query_string_data_to_loggins_operations();

								LoggingDailyOperations
										.get_instance()
										.add_to_log("info", null,
												"daily_download",
												"now starting to get historical data and insert into graph.");
								if (!LoggingDailyOperations.get_instance()
										.has_exception()) {

									api.scrapeEntity(qst, g, mgmt,
											exchange_counter);

									LoggingDailyOperations.get_instance()
											.add_to_log(
													"info",
													null,
													"daily_download",
													"finished api call for index :"
															+ indiceName);
								}

							}

							if (!LoggingDailyOperations.get_instance()
									.has_exception()) {
								AppendCompanyInformation ap_ci = new AppendCompanyInformation(
										company_information, indiceName, g,
										mgmt);
								ap_ci.addStockInformationToGraph();
							}
							else{
								System.out.println("there is an exception, attempting to enter" +
										" append company information ");
								System.in.read();
							}

						}

						LoggingDailyOperations.get_instance().add_to_log(
								"info",
								null,
								"daily_download",
								"finished full graph commit for " + "index--->"
										+ exchange_counter);

					} catch (Exception e) {
						LoggingDailyOperations.get_instance().add_to_log(
								"exception", e, "daily_download", null);
					}

				}
				exchange_counter++;
			}
		}
	}
	
	
	public void run(Client client, Gson gson, Boolean test) {

		/**
		 * 
		 * check the values of the two jedis keys. both are expected to be null
		 * 
		 * 
		 */
		SchedulerRedisKeysControl srkc = new SchedulerRedisKeysControl(1);
		if (!srkc.getProceed()) {
			LoggingDailyOperations.get_instance()
					.exit_without_graph_and_log_before_exiting();
		}

		/**
		 * start the scheduled executor service.
		 * 
		 * 
		 */
		final ScheduledExecutorService scheduledExecutorService = Executors
				.newScheduledThreadPool(StockConstants.total_threads_to_use_for_jedis_popper);
		ScheduledFuture<?> l = scheduledExecutorService
				.scheduleAtFixedRate(
						new JedisPopper(scheduledExecutorService, test),
						StockConstants.initial_delay_for_jedis_popper_schedule_executor,
						StockConstants.schedule_for_jedis_popper_schedule_executor,
						StockConstants.timeunit_for_jedis_popper_schedule_executor);

		/**
		 * proceed.
		 * 
		 */

		// Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		// Gson gson = null;

		/*
		 * try {
		 * 
		 * client = ClientInstance.getInstance().getCl();
		 * GroovyStatements.initializeGroovyStatements(g, mgmt); gson = new
		 * GsonBuilder().setPrettyPrinting().create();
		 * 
		 * } catch (Exception e) { e.printStackTrace();
		 * LoggingDailyOperations.get_instance().add_to_log("exception", e,
		 * "init", null);
		 * 
		 * return; }
		 */

		Long new_session_time = null;
		String id_of_new_session_doc = null;
		try {
			check_or_create_pair_documents();
			build_last_reliable_date_hashmap();
			Pair<Long, String> p = add_new_session_to_elasticsearch();
			new_session_time = p.getValue0();
			if (new_session_time == null) {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								new Exception(
										"could not add the new session to elasticsearch/"),
								identifier_for_log, null);
			} else {
				id_of_new_session_doc = p.getValue1();
				// System.out.println("this is the id of the new session");
				// System.out.println(id_of_new_session_doc);
				// System.in.read();
			}

		} catch (Exception e) {
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
		}

		scrape_exchanges(g, mgmt);
		/**
		 * send the stop signal to the jedis poller.
		 * 
		 * 
		 */
		SchedulerRedisKeysControl srkc2 = new SchedulerRedisKeysControl(2);

		/**
		 * 
		 * proceed into endless while loop. waiting for the signal for the main
		 * program to proceed.
		 * 
		 */
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			while (!jedis
					.get(StockConstants.redis_key_name_for_main_program_wait_controller)
					.equals(StockConstants.proceed_signal)) {
				// wait.
				// System.out.println(jedis.get(StockConstants.redis_key_name_for_main_program_wait_controller));
				try {
					System.out.println("waiting.");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		/**
		 * check for exceptions that have occurred after the session started. if
		 * none, then update the latest date on each stock into session info.
		 * 
		 */

		check_exceptions_and_update_last_reliable_dates(new_session_time,
				id_of_new_session_doc);

		/**
		 * 
		 * now set everything back to the start values.
		 * 
		 */
		SchedulerRedisKeysControl srkc3 = new SchedulerRedisKeysControl(4);

	}

	public static void update_latest_reliable_dates_for_each_stock(
			String id_of_latest_session) {

		ArrayList<HashMap<String, Object>> latest_reliable_last_dates = new ArrayList<HashMap<String, Object>>();

		SearchResponse sr = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("historical_data_point")
				.setQuery(QueryBuilders.matchAllQuery())
				.addAggregation(
						AggregationBuilders
								.terms("each_stock")
								.field("stockName")
								.size(0)
								.subAggregation(
										AggregationBuilders
												.terms("last_date")
												.field("dates")
												.order(Terms.Order.aggregation(
														"max_date", false))
												.size(1)
												.subAggregation(
														AggregationBuilders
																.max("max_date")
																.field("epoches")))
								.subAggregation(
										AggregationBuilders.terms(
												"stockFullName").field(
												"stockFullName")))

				.execute().actionGet();

		// sr is here your SearchResponse object
		Terms genders = sr.getAggregations().get("each_stock");

		// For each entry

		for (Terms.Bucket entry : genders.getBuckets()) {
			// System.out.println(entry.getKey()); // Term
			// System.out.println(entry.getDocCount()); // Doc count
			String stockName_n = entry.getKey();
			String latest_date_d = null;
			String stockFullName_n = null;

			Terms latest_date = entry.getAggregations().get("last_date");
			Terms stockFullNameterms = entry.getAggregations().get(
					"stockFullName");
			for (Terms.Bucket latest_date_entry : latest_date.getBuckets()) {
				latest_date_d = latest_date_entry.getKey();
			}
			for (Terms.Bucket stockFullNameentry : stockFullNameterms
					.getBuckets()) {
				stockFullName_n = stockFullNameentry.getKey();
			}

			final String stockName = stockName_n;
			final String stockFullName = stockFullName_n;
			final String last_reliable_end_date = latest_date_d;

			latest_reliable_last_dates.add(new HashMap<String, Object>() {
				{
					put("stockName", stockName);
					put("stockFullName", stockFullName);
					put("last_reliable_end_date", last_reliable_end_date);
				}
			});
		}

		HashMap<String, Object> urequest_map = new LinkedHashMap<String, Object>();
		urequest_map.put("list_of_latest_reliable_dates",
				latest_reliable_last_dates);

		// now issue the update request.
		UpdateRequest urequest = new UpdateRequest("tradegenie_titan",
				"session_info", id_of_latest_session).script(
				StockConstants.name_of_script_for_updating_session_info,
				ScriptService.ScriptType.FILE, urequest_map);

		try {
			UpdateResponse uresp = LocalEs.getInstance()
					.update(urequest).get();
			uresp.isCreated();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * try { XContentBuilder builder = jsonBuilder().startObject()
		 * .startObject("query").startObject("filtered")
		 * .startObject("query").field("match_all").startObject()
		 * .endObject().endObject().startObject("filter")
		 * .startObject("bool").startArray("must").startObject()
		 * .startObject("term").field("stockName").value(stockName)
		 * .endObject().endObject().startObject().startObject("term")
		 * .field("stockFullName").value(stockFullName).endObject()
		 * .endObject().endArray().endObject().endObject().endObject()
		 * .endObject().endObject();
		 * 
		 * //XContentBuilder builder = jsonBuilder().startObject() //
		 * .startObject("query").field("match_all").endObject() // .endObject();
		 * 
		 * SearchRequestBuilder requestBuilder = ElasticClientHolder
		 * .getInstance().prepareSearch("tradegenie_titan")
		 * .setTypes("historical_data_point").setSource(builder)
		 * .addField("dates"
		 * ).addField("timezoneString").addField("epoches").setSize(1)
		 * .addSort("epoches", SortOrder.DESC);
		 * 
		 * //SearchRequestBuilder requestBuilder = ElasticClientHolder //
		 * .getInstance() // .prepareSearch("tradegenie_titan") //
		 * .setTypes("session_info") // .setSource(builder) //
		 * .setFetchSource(new String[] { "last_reliable_end_dates" }, // new
		 * String[] {}).addSort("epoches", SortOrder.DESC) // .setSize(1);
		 * 
		 * SearchResponse response = requestBuilder.execute().actionGet();
		 * 
		 * for (SearchHit hit : response.getHits().hits()) {
		 * last_date_and_time.add(hit.field("dates").value().toString());
		 * last_date_and_time.add(hit.field("timezoneString").value()
		 * .toString());
		 * last_date_and_time.add(hit.field("epoches").toString()); }
		 * 
		 * //if (response.getHits().hits().length > 0) {
		 * 
		 * // for (SearchHit hit : response.getHits()) { // Map<String,
		 * SearchHitField> fields = hit.getFields(); // SearchHitField sf =
		 * fields.get("last_reliable_end_dates"); // List<Object>
		 * last_reliable_dates = sf.getValues(); // for (Object o :
		 * last_reliable_dates) { // /this is basically a map of strings and the
		 * // stockdetails.
		 * 
		 * // } // } //} else {
		 * 
		 * //}
		 * 
		 * // System.out.println("stockName:" + stockName + " dates :" + //
		 * last_date_and_time.toString());
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * return last_date_and_time;
		 **/
	}

	public static void check_exceptions_and_update_last_reliable_dates(
			Long new_session_time, String new_session_id) {
		try {
			Long latest_error_time = null;
			Map<String, Object> exception = null;
			SearchResponse resp = LocalEs
					.getInstance()
					.prepareSearch("tradegenie_titan")
					.setTypes("complex_calculation_error")
					.setSource(
							ReadWriteTextToFile
									.getInstance()
									.readFile(
											StockConstants.path_to_saved_queries
													+ StockConstants.name_of_file_holding_saved_query_for_latest_error,
											TitanConstants.ENCODING))
					.addSort("error_time", SortOrder.DESC)
					.setFetchSource(new String[] { "error_time", "exception" },
							new String[] {}).setSize(1).execute().actionGet();

			for (SearchHit hit : resp.getHits().getHits()) {
				latest_error_time = (Long) hit.sourceAsMap().get("error_time");
				exception = (Map<String, Object>) hit.sourceAsMap().get(
						"exception");
			}

			// System.out.println("latest error time is:" + latest_error_time);
			// System.out.println("exception is:" + exception);
			// System.in.read();

			if (latest_error_time == null) {
				update_latest_reliable_dates_for_each_stock(new_session_id);
			} else if (latest_error_time < new_session_time) {
				// we go ahead and update the new latest reliable dates.
				// now go one exchange at a time, get all the stocks in it, and
				// then
				// get the last date for each of those stocks
				update_latest_reliable_dates_for_each_stock(new_session_id);
			} else {
				// /System.out.println("this exception occured later.");
				// /System.out.println(exception);
			}

		} catch (ElasticsearchException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IllegalArgumentException,
			IllegalAccessException, JSONException, InterruptedException {

		LogOnShutdown lsd = new LogOnShutdown();
		lsd.attachShutDownHook();

		Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;
		GroovyStatements gs = null;
		Gson gson = null;

		try {

			client = ClientInstance.getInstance().getCl();
			GroovyStatements.initializeGroovyStatements(g, mgmt);
			gson = new GsonBuilder().setPrettyPrinting().create();

		} catch (Exception e) {
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}

		//new Download().run(client, gson, false);
		
		new Download().scrape_exchanges(g, mgmt);
		
		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, "daily_download-BEGIN",
						"#########################----------begin-------##############################");

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						"daily_download-EXIT",
						"-------------------------exiting daily_download, after shutting down graph.---------------------");
		LoggingDailyOperations.get_instance().log_on_exit();
	}

	/**
	 * here this response processing is for the pair items.
	 * 
	 */
	@Override
	public void process_response(
			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response) {

		// TODO Auto-generated method stub
		for (HashMap<String, Object> failed_response_object : response
				.getValue1()) {
			LoggingDailyOperations.get_instance().pairs_failed_to_add_to_es
					.add(failed_response_object);
			LoggingDailyOperations.get_instance().add_to_log("exception",
					new Exception("failed to add pair to es, check logs."),
					identifier_for_log, null);
		}

		// LoggingDailyOperations.get_instance().add_to_log("info",
		// null,
		// identifier_for_log,
		// "successfully added pair count of:" + response.getValue0().size());

		// LoggingDailyOperations.get_instance()
		// .exit_without_graph_and_log_before_exiting();

	}

}
