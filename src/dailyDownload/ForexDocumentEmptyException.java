package dailyDownload;

public class ForexDocumentEmptyException extends Exception{
	public ForexDocumentEmptyException(String message){
		super(message);
	}
}
