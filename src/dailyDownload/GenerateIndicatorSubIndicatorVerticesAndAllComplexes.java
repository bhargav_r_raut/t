package dailyDownload;

import java.io.IOException;
import java.util.GregorianCalendar;

import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.PrepareIndicatorsAndSubIndicatorsJSONFiles;
import Indicators.RedisManager;
import Titan.GroovyStatements;
import Titan.TitanBaseConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import elasticSearchNative.ClientInstance;
import elasticSearchNative.LocalEs;
import genericIndicatorFunctions.CreateDailyIndicatorsJsonString;
import genericIndicatorFunctions.GenerateSummaryOfNewlyAddedIndicators;

import static org.elasticsearch.index.query.FilterBuilders.*;
import static org.elasticsearch.index.query.QueryBuilders.*;

public class GenerateIndicatorSubIndicatorVerticesAndAllComplexes {

	/**
	 * flushes redis
	 * 
	 * runs make index
	 * 
	 * runs the prepare indicator subindicator json strings
	 * 
	 * 
	 */
	public static Integer fresh_run_setup() {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {

			jedis.flushAll();

			new PrepareIndicatorsAndSubIndicatorsJSONFiles();

			String[] cmd = new String[] {
					"/bin/sh",
					StockConstants.path_to_elasticsearch_config
							+ StockConstants.name_of_elasticsearch_index_maker_sh_script };

			Process pr = Runtime.getRuntime().exec(cmd);

			Integer i = pr.waitFor();

			return i;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public static Integer at_end_of_setup() throws IOException, InterruptedException{
		System.out.println("running script to backup everything at the end of setup.");
		String[] cmd = new String[] {
				"/bin/sh",
				StockConstants.path_to_elasticsearch_config
						+ StockConstants.name_of_sh_file_to_run_at_end_of_setup };

		Process pr = Runtime.getRuntime().exec(cmd);

		Integer i = pr.waitFor();

		return i;
	}
	
	public static Integer at_start_of_test() throws IOException, InterruptedException{
		System.out.println("running restoration script at start of test.");
		String[] cmd = new String[] {
				"/bin/sh",
				StockConstants.path_to_elasticsearch_config
						+ StockConstants.name_of_sh_file_to_run_at_start_of_test_prepare_complexes };

		Process pr = Runtime.getRuntime().exec(cmd);

		Integer i = pr.waitFor();

		return i;
	}
	
	
	
	
	
	

	public static void delete_historical_data_points_stock_details_and_session_info_indices() {

		Client client = LocalEs.getInstance();

		System.out.println("deleting the historical data point index");

		DeleteByQueryResponse historical_data_point_delete_response = client
				.prepareDeleteByQuery("tradegenie_titan")
				.setTypes("historical_data_point").setQuery(matchAllQuery())
				.execute().actionGet();
		if (historical_data_point_delete_response.status()
				.equals(RestStatus.OK)) {
			System.out.println("deleted the historical stock datapoint index");
		} else {
			System.out
					.println("failed to delete the historical stock datapoint index");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("deleting the stock_details index");
		DeleteByQueryResponse stock_details_delete_response = client
				.prepareDeleteByQuery("tradegenie_titan")
				.setTypes("stock_details").setQuery(matchAllQuery()).execute()
				.actionGet();

		if (stock_details_delete_response.status().equals(RestStatus.OK)) {
			System.out.println("deleted the stock details index");
		} else {
			System.out.println("failed to delete the stock details index");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("deleting the session info index");
		DeleteByQueryResponse session_info_delete_resposne = client
				.prepareDeleteByQuery("tradegenie_titan")
				.setTypes("session_info")
				.setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();

		if (session_info_delete_resposne.status().equals(RestStatus.OK)) {
			System.out.println("deleted the session info index");
		} else {
			System.out.println("failed to delete the session info index");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	public static void generate_subindicator_indicator_vertices() {

		if (fresh_run_setup() != 0) {
			return;
		}

		StockConstants.update_stock_pair_changes_to_es = true;
		StockConstants.prepare_complexes = false;
		StockConstants.is_test = true;
		

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, "daily_download-BEGIN",
						"#########################----------begin-------##############################");

		Client client = null;

		TitanGraph g = null;
		TitanManagement mgmt = null;

		Gson gson = null;

		try {
			client = LocalEs.getInstance();
			// cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			// g = cfg.get_graph();

			// mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g);

			// CreateCommonDayIdVertices cid = new
			// CreateCommonDayIdVertices(g,mgmt);

			// gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"daily_download", "finished initialization corretly");

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}

		new Download().run(client, gson, true);

		// System.out.println("finished the first download run through.");
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		
		/**
		 * this calls a redis flush somewhere in the middle, and thats why we have to 
		 * do another run of download.
		 */
		CreateDailyIndicatorsJsonString crud = new CreateDailyIndicatorsJsonString(
				g, gson);
		
		
		System.out.println("finished creating the indicator subindicators.");
		System.out.println("now entering the summary creation.");

		delete_historical_data_points_stock_details_and_session_info_indices();
		
		new Download().run(client, gson, true);
		
		
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			System.out.println("saving the redis database at the end of the whole process with response:" + jedis.save());
		}
		
		//now store the redis 
		
		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						"daily_download-EXIT",
						"-------------------------exiting daily_download, after shutting down graph.---------------------");
		LoggingDailyOperations.get_instance().log_on_exit();

	}

	/***
	 * 
	 * so first run generate_subindicator_indicator_vertices - this generates
	 * all the redis stuff it flushes the current redis , and remakes the index.
	 * 
	 * then run test_prepare_complexes to test the complex creation.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		//generate_subindicator_indicator_vertices();
		// main_daily_function();
		// GregorianCalendar cal = new GregorianCalendar();

		test_prepare_complexes();

		// /GregorianCalendar end_cal = new GregorianCalendar();

		// System.out.println("took" + (end_cal.getTimeInMillis() -
		// cal.getTimeInMillis()));

	}

	public static void test_prepare_complexes() {

		StockConstants.update_stock_pair_changes_to_es = false;
		StockConstants.prepare_complexes = true;
		GroovyStatements.initializeGroovyStatements(null);
		//new RefreshChangedRedisLists().refresh_changed_redis_lists();
		
		//try {
		//	System.out.println("stopped");
		//	System.in.read();
		//} catch (Exception e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		
		Client client = null;

		client = LocalEs.getInstance();

		delete_historical_data_points_stock_details_and_session_info_indices();

		main_daily_function_test(client);
	}

	/**
	 * simply does same as main daily function but here es client is passed in.
	 * used for the test function above.
	 * 
	 */
	public static void main_daily_function_test(Client client) {

		/**
		 * 
		 * 
		 * STEP ONE: START THE SCHEDULED POLLER FOR JEDIS POPPER.
		 * 
		 * 
		 * 
		 */
		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, "daily_download-BEGIN",
						"#########################----------begin-------##############################");

		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		GroovyStatements gs = null;
		Gson gson = null;

		try {

			// cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			// g = cfg.get_graph();

			// mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g);

			// CreateCommonDayIdVertices cid = new
			// CreateCommonDayIdVertices(g,mgmt);

			// gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"daily_download", "finished initialization corretly");

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}

		new Download().run(client, gson, false);

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						"daily_download-EXIT",
						"-------------------------exiting daily_download, after shutting down graph.---------------------");
		LoggingDailyOperations.get_instance().log_on_exit();

	}

	/**
	 * simply runs daily download.
	 * 
	 */
	public static void main_daily_function() {

		/**
		 * 
		 * 
		 * STEP ONE: START THE SCHEDULED POLLER FOR JEDIS POPPER.
		 * 
		 * 
		 * 
		 */
		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, "daily_download-BEGIN",
						"#########################----------begin-------##############################");

		Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		GroovyStatements gs = null;
		Gson gson = null;

		try {
			client = LocalEs.getInstance();
			// cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			// g = cfg.get_graph();

			// mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g, mgmt);

			// CreateCommonDayIdVertices cid = new
			// CreateCommonDayIdVertices(g,mgmt);

			// gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"daily_download", "finished initialization corretly");

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);

			return;
		}

		new Download().run(client, gson, false);

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						"daily_download-EXIT",
						"-------------------------exiting daily_download, after shutting down graph.---------------------");
		LoggingDailyOperations.get_instance().log_on_exit();

	}

}
