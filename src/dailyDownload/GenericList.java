package dailyDownload;

import java.util.ArrayList;
import java.util.List;

public class GenericList<T> extends ArrayList<T> {
	private Class<T> genericType;
	
	public GenericList(List<T> l){
		
	}
	
	public GenericList(Class<T> c) {
		this.genericType = c;
	}

	public Class<T> getGenericType() {
		return genericType;
	}
}
