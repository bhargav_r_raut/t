package dailyDownload;

import com.thinkaurelius.titan.core.TitanGraph;

public class GraphHolder {
	public static GraphHolder instance;
	public static TitanGraph g;
	
	public static synchronized void initializeGraphHolder(TitanGraph g){
		if(instance == null){
			
			instance = new GraphHolder(g);
		}
	}

	public static synchronized GraphHolder getInstance(){
		return instance;
	}
	
	
	public GraphHolder(TitanGraph g){
		this.g = g;
	}
}
