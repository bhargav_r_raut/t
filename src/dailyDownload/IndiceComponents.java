package dailyDownload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.cassandra.thrift.Cassandra.AsyncProcessor.get;

import Titan.GroovyStatements;
import antlr.collections.List;

import com.tinkerpop.blueprints.Graph;

public class IndiceComponents {
	
	private HashMap<String, HashMap<String, String>> indice_components =
			new HashMap<String, HashMap<String, String>>();

	
	private static IndiceComponents instance;
	
	public static synchronized IndiceComponents getInstance(){
		if(instance == null){
			instance = new IndiceComponents();
		}
		return instance;
	}
	
	public HashMap<String, HashMap<String, String>> getIndice_components() {
		return indice_components;
	}

	public IndiceComponents() {
		this.indice_components = new HashMap<String, HashMap<String, String>>();
	}

	public void addstockToIndexInMap(String index, String stockName,
			String stockFullName) {
		if(getIndice_components().containsKey(index)){
			
		}
		else{
			getIndice_components().put(index, new HashMap<String, String>());
		}
		HashMap currentHashMap = getIndice_components().get(index);
		currentHashMap.put(stockName, stockFullName);
		getIndice_components().put(index, currentHashMap);
	}

	public HashMap<String,String> yieldUndoneComponents(Graph g, String index) {
		// first get the current components
		// then compare and add new ones.
		GroovyStatements gs = GroovyStatements.getInstance();
		ArrayList<HashMap<String, String>> current_components_of_index = (ArrayList) gs
				.searchExchange(index);
		// now remove these from the components hashmap that you have got just
		// now.
		for (HashMap<String, String> hs : current_components_of_index){
			for(Map.Entry entry : hs.entrySet()){
				getIndice_components().get(index).remove(entry.getKey());
			}
		}
		
		//now the indicecomponents hash for this index contains whatever new components are there,
		//which we do not have in the graph.
		return getIndice_components().get(index);

	}
	
	//yield current components as hashmap of hashmaps
	

}
