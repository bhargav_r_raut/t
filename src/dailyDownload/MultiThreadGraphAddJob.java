package dailyDownload;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;

import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.HistoricalStock;
import DailyOperationsLogging.LoggingDailyOperations;
import Titan.AddDataPointToGraph;
import Titan.PropertyKeyAbsentException;
import Titan.TItanWorkerThreadResultObject;
import Titan.TitanConstants;
import Titan.TitanThreadedExecutor;

import com.google.gson.Gson;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

public class MultiThreadGraphAddJob {
	private ArrayList<TItanWorkerThreadResultObject> results_array;
	private transient Gson gson;
	private static final String identifier_for_log = "multi_thread_graph_add_job";
	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	public Map<String, Object> addKeyValuePairToMap(Map<String, Object> m,
			String key, Object value, TitanManagement mgmt,
			ArrayList<String> propertyKeyList)
			throws PropertyKeyAbsentException {
		// System.out.println("the property key list contains.");
		// System.out.println(propertyKeyList.toString());
		if (propertyKeyList.contains(key)) {
			m.put(key, value);
		} else {
			if (mgmt.containsRelationType(key)) {
				propertyKeyList.add(key);

				m.put(key, value);
			} else {
				throw new PropertyKeyAbsentException("the key  " + key
						+ " was absent.");
			}
		}

		return m;
	}

	public MultiThreadGraphAddJob(ArrayList<HashMap<String, Object>> hm,
			ArrayList<JSONObject> additional_params, TitanGraph g,
			TitanManagement mgmt, boolean use_transactional_graph,
			String groovy_function) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException,
			InterruptedException, ExecutionException {

		this.results_array = new ArrayList<TItanWorkerThreadResultObject>();
		this.gson = new Gson();
		AddDataPointToGraph adg = new AddDataPointToGraph(g,
				use_transactional_graph);

		int total_transactions_done = 0;

		ArrayList<String> propertyKeyList = new ArrayList<String>();
		Integer outer_counter = 0;
		for (HashMap<String, Object> stock : hm) {
			// here we have to check if the stock has any values or not.
			// if (stock.keySet().size() > 0) {

			int calendar_entry = 0;

			Map<String, Object> m = new HashMap<String, Object>();

			for (Map.Entry<String, Object> sm : stock.entrySet()) {
				// if(f.getName() == "indice"){
				// System.out.println("the name of the field is ---> "
				// + f.getName());
				// System.out.println("the value is--->" +
				// f.get(stock));
				// }

				if (sm.getValue() != null) {

					if (sm.getValue() instanceof String
							|| sm.getValue() instanceof Integer
							|| sm.getValue() instanceof Double
							|| sm.getValue() instanceof Float
							|| sm.getValue() instanceof Long) {

						try {

							m = addKeyValuePairToMap(m, sm.getKey(),
									sm.getValue(), mgmt, propertyKeyList);

						} catch (PropertyKeyAbsentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							LoggingDailyOperations.get_instance()
							.add_to_log("exception", e,
									identifier_for_log, null);
						}

					} else if (sm.getValue() instanceof ArrayList<?>) {
						ArrayList<?> l = (ArrayList<?>) sm.getValue();
						// System.out.println("it was an instance of---> "
						// +
						// f.get(stock).getClass());

						if (l.isEmpty()) {
							// System.out.println("it was empty proof--->"
							// + l.size());
						} else {
							// System.out.println("it was not empty, so"
							// + " now we check if calendar");
							// see if its a calendar, in which case we
							// have to
							if (l.get(calendar_entry) instanceof Calendar) {
								// System.out.println("it is a calendar");
								Calendar caln = (Calendar) l
										.get(calendar_entry);
								String calendar_rep = new CalendarFunctions(
										caln).getFullDateWithoutTimeZone();
								// System.out.println("calendar representation is -->"
								// + "" + calendar_rep);
								try {
									m = addKeyValuePairToMap(m, sm.getKey(),
											calendar_rep, mgmt, propertyKeyList);
								} catch (PropertyKeyAbsentException e) {
									// TODO Auto-generated catch block
									LoggingDailyOperations
											.get_instance()
											.add_to_log(
													"exception",
													e,
													"inside multithread graph add job",
													null);
								}
							} else {
								try {
									m = addKeyValuePairToMap(m, sm.getKey(),
											l.get(calendar_entry), mgmt,
											propertyKeyList);
								} catch (PropertyKeyAbsentException e) {
									// TODO Auto-generated catch block
									LoggingDailyOperations
											.get_instance()
											.add_to_log(
													"exception",
													e,
													"inside multithread graph add job",
													null);
								}
							}
						}
					} else {
						// all other types can be added here later.
					}

				}

				// here you have to submit the map to the worker thread.
				// so here we do the submit new vertex thingy.

			}

			adg.CreateVertex(m, groovy_function,
					additional_params.get(outer_counter));
			total_transactions_done++;

			if (use_transactional_graph) {
				if (total_transactions_done > TitanConstants.GLOBAL_TRANSACTIONS_LIMIT) {

					for (int i = 0; i < total_transactions_done; i++) {

						try {
							String result = adg.getTpoolCompl().take().get();
						} catch (ExecutionException e) {

						}
						// now after this shutdown the pool, and commit
						// the
						// graph.
					}
					adg.getTpoolExec().shutdown();
					// now commit.
					adg.getG().commit();
					// now create a new pool.
					adg = null;
					total_transactions_done = 0;
					adg = new AddDataPointToGraph(g, use_transactional_graph);
					// System.out
					// .println("commited this transaction and regen thread pool");
					// System.out.println("now going to get inside again.");
				}
			}

			// } else {

			// }
			outer_counter++;
		}

		for (int i = 0; i < total_transactions_done; i++) {

			try {

				String result = adg.getTpoolCompl().take().get();
				getResults_array().add(
						getGson().fromJson(result,
								TItanWorkerThreadResultObject.class));
				if (i % 250 == 0) {

					LoggingDailyOperations.get_instance().add_to_log(
							"info",
							null,
							"multi_threaded_graph_add_job",
							"finished adding result number: "
									+ String.valueOf(i) + "" +
											" and the result object is: " + result);

				}

			} catch (Exception e) {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								e,
								"inside multithread graph add job, collecting results.",
								null);
			}
			// now after this shutdown the pool, and commit the
			// graph.
		}

		adg.getTpoolExec().shutdown();

		if (use_transactional_graph) {
			adg.getG().commit();
		} else {
			adg.getTg().commit();
		}

	}

	/**
	 * @return the results_array
	 */
	public ArrayList<TItanWorkerThreadResultObject> getResults_array() {
		return results_array;
	}

	/**
	 * @param results_array
	 *            the results_array to set
	 */
	public void setResults_array(
			ArrayList<TItanWorkerThreadResultObject> results_array) {
		this.results_array = results_array;
	}

}
