package dailyDownload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yahoo_finance_historical_scraper.StockConstants;
import Indicators.ComplexCalculationRequest;
import Titan.AddDataPointToGraph;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import elasticSearchNative.LocalEs;

public class PrepareComplexes {

	public static void main(String[] args) throws Exception {
		HashMap<String, HashMap<String, Object>> hm = new HashMap<String, HashMap<String, Object>>();
		hm.put("test", new HashMap<String, Object>() {
			{
				put("ohlcv_type", "c2");
			}
		});
		new PrepareComplexes(hm);
	}

	private AddDataPointToGraph adg;
	private org.json.JSONObject indicators_jsonfile;
	private org.json.JSONObject first_subindicator_from_each_group;
	private HashMap<String, HashMap<String, Object>> hashmap_of_stocks_successfully_added_to_es;

	public AddDataPointToGraph getAdg() {
		return adg;
	}

	public void setAdg(AddDataPointToGraph adg) {
		this.adg = adg;
	}

	public HashMap<String, HashMap<String, Object>> getHashmap_of_stocks_successfully_added_to_es() {
		return hashmap_of_stocks_successfully_added_to_es;
	}

	public void setHashmap_of_stocks_successfully_added_to_es(
			HashMap<String, HashMap<String, Object>> hashmap_of_stocks_successfully_added_to_es) {
		this.hashmap_of_stocks_successfully_added_to_es = hashmap_of_stocks_successfully_added_to_es;
	}

	public org.json.JSONObject getIndicators_jsonfile() {
		return indicators_jsonfile;
	}

	public void setIndicators_jsonfile() {
		try {
			org.json.JSONObject indicators_jsonfile_read = new org.json.JSONObject(
					ReadWriteTextToFile.getInstance().readFile(
							StockConstants.indicators_jsontextfile,
							TitanConstants.ENCODING));
			this.indicators_jsonfile = indicators_jsonfile_read;
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * read the indicators jsonstring read the file which contains the first
	 * subindicator from each group store the hashmap of successfully downloaded
	 * stocks with min and max day ids.
	 * 
	 * @throws JSONException
	 * 
	 * 
	 * 
	 */
	public PrepareComplexes(
			HashMap<String, HashMap<String, Object>> stocks_succssfully_added_to_es)
			throws Exception {

		setIndicators_jsonfile();
		setHashmap_of_stocks_successfully_added_to_es(stocks_succssfully_added_to_es);
		generate_and_send_complexes();

	}

	public ArrayList<ArrayList<Integer>> generate_arraylist_of_periods_from_jsonarray(
			JSONArray jsonarray) throws JSONException {
		ArrayList<ArrayList<Integer>> periods = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < jsonarray.length(); i++) {
			ArrayList<Integer> stat_list = new ArrayList<Integer>();
			JSONArray arr = jsonarray.getJSONArray(i);
			for (int k = 0; k < arr.length(); k++) {
				stat_list.add(arr.getInt(k));
			}
			periods.add(stat_list);
		}

		return periods;
	}

	public ArrayList<String> get_period_names_from_time_frames(
			JSONArray time_frames) throws JSONException {

		ArrayList<ArrayList<Integer>> periods = generate_arraylist_of_periods_from_jsonarray(time_frames);
		ArrayList<String> period_names = new ArrayList<String>();
		Integer outer_counter = 0;
		for (ArrayList<Integer> list_a : periods) {
			// System.out.println("outer list is --->" + list_a.toString() +
			// "outer counter is=-=>"
			// + outer_counter);
			// System.out.println("press to continue");
			// System.in.read();
			Integer inner_counter = 0;
			for (ArrayList<Integer> list_b : periods) {
				// System.out.println("inner list is--->" +
				// list_b.toString() +
				// "inner counter is" +
				// inner_counter);
				if (outer_counter < inner_counter) {
					for (Integer list_a_interger : list_a) {
						for (Integer list_b_integer : list_b) {
							period_names.add("period_start_"
									+ String.valueOf(list_a_interger) + "_"
									+ String.valueOf(list_b_integer)
									+ "_period_end");
							// System.out.println("now added to period_names--->"
							// +
							// String.valueOf(list_a_interger)
							// + "_day_" + String.valueOf(list_b_integer) +
							// "_day");
							// System.out.println("press to continue");
							// System.in.read();

						}
					}
				} else {
					// now mix the two lists.

				}

				inner_counter++;
			}
			outer_counter++;
		}

		if (period_names.isEmpty()) {
			// in this case we have to take each thing in the periods array
			// ,
			// and make one
			// name out of it.
			// this is the case where there is only one determinant in the
			// periods.
			// System.out.println("period names was empty--->");

			if (periods.size() == 1) {
				// System.out.println("periods size is 1");
				for (Integer period : periods.get(0)) {
					period_names.add("period_start_" + String.valueOf(period)
							+ "_period_end");
				}
				// System.out
				// .println("period names after adding for size 1 is0-->");
				// System.out.println(period_names.toString());
				// System.out.println("press to continue");
				// System.in.read();
			}
		}

		return period_names;

	}
		
	/**
	 * this def is used only in test 
	 * it queries the elasticsearch "complex" index for a random 
	 * document and then gets the stock name and full name from it.
	 * this is because in test mode when we make the pairs, it makes only the pairs
	 * for one stock and we dont know in advance which one, so we need to know which one it 
	 * took, so we do this.
	 * 
	 */
	private ArrayList<String> get_stock_details_for_test(){
		ArrayList<String> ar = new ArrayList<String>();
		SearchResponse sr = 
				LocalEs.getInstance()
		.prepareSearch("tradegenie_titan")
		.setTypes("complex")
		.setQuery(QueryBuilders.matchAllQuery())
		.setSize(1)
		.addFields(new String[]{"stockName","indicator_name"})
		.execute().actionGet();
		
		SearchHit[] srh = sr.getHits().getHits();
		
		for(SearchHit hit: srh){
			ar.add((String)hit.field("stockName").value());
			ar.add((String)hit.field("indicator_name").value());
		}
		
		return ar;
	}

	public void generate_and_send_complexes() throws JSONException,
			InterruptedException {

		/**
		 * iterating one stock at a time. threadpool created only once at
		 * starting.
		 * 
		 * 
		 */
		setAdg(new AddDataPointToGraph(null, null));

		for (Map.Entry<String, HashMap<String, Object>> entry : getHashmap_of_stocks_successfully_added_to_es()
				.entrySet()) {

			HashMap<String, Object> stock_info = entry.getValue();
			String ohlcv_type = (String) stock_info.get("ohlcv_type");
			String stockName = (String) stock_info.get("stockName");
			String stockFullName = (String) stock_info.get("stockFullName");
			String indice = (String) stock_info.get("indice");
			Integer day_id_from = (Integer) stock_info.get("day_id_min");
			Integer day_id_to = (Integer) stock_info.get("day_id_max");

			System.out.println("now doing prepare complex for:" + stockName);
			// try {
			// System.in.read();
			// } catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			/**
			 * this arraylist contains all the complex calculation requests
			 * added to the threadpool. it is used to log errors in case
			 * something goes wrong in the execution. although that is already
			 * being totally handled inside the complex caculation itself, so
			 * this is for errors in the threadpool itself, basically something
			 * weird happening. it also serves to count the total number of
			 * futures that need to be iterated.
			 * 
			 */

			ArrayList<ComplexCalculationRequest> cerr_al = new ArrayList<ComplexCalculationRequest>();
			Iterator<String> indicators_iterator = getIndicators_jsonfile()
					.keys();

			while (indicators_iterator.hasNext()) {

				ArrayList<String> complexes_of_this_indicator = new ArrayList<String>();

				String indicator_name = indicators_iterator.next();
				// System.out.println("indicator name is:" + indicator_name);

				JSONObject indicator_object = getIndicators_jsonfile()
						.getJSONObject(indicator_name);
				JSONArray applicable_ohlcv_groups = indicator_object
						.getJSONArray("ohlcv_group");
				JSONArray time_frames = indicator_object
						.getJSONArray("time_frames");
				JSONArray names_of_subindicators_not_applicable_to_indicator = indicator_object
						.getJSONArray("not_applicable_to");
				ArrayList<String> periods = get_period_names_from_time_frames(time_frames);
				// now for each period, generate the names of the
				// complex.
				if (periods.isEmpty()) {
					periods.add(StockConstants.name_to_use_when_no_periods);
				}

				// System.out.println("name of indicator is:" + indicator_name);
				// System.out.println("the ohlcv_group of indicator is:"
				// + applicable_ohlcv_groups);
				// System.out.println("the time frames are:" + time_frames);
				// System.out.println("the names not applicable are:"
				// + names_of_subindicators_not_applicable_to_indicator);
				// System.out.println("press enter to continue");
				// System.out.println("periods of this indicator are:" +
				// periods);

				// try {
				// System.in.read();
				// } catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				Boolean proceed = false;
				for (int g = 0; g < applicable_ohlcv_groups.length(); g++) {
					// System.out.println("we are checking the applicable ohlcv :"
					// + applicable_ohlcv_groups.get(g));

					if (applicable_ohlcv_groups.get(g).equals(ohlcv_type)) {
						// System.out
						// .println("and we are getting it equal to stock");
						proceed = true;
					}
				}

				/**
				 * indicator is applicable to stock
				 */
				if (proceed) {
					// System.out.println("the proceed is true.");

					/**
					 * 
					 * for each of the subindicators in the subindicator groups
					 * check if it doesnt exist in the not applicable
					 * subindicators and if not, then for each indicator period,
					 * construct the name of the subindicator indicator stock
					 * complex then push it to the thread pool.
					 * 
					 */
					for (String subindicator_in_question : StockConstants.first_subindicator_from_each_group_with_index
							.keySet()) {

						/**
						 * 
						 * CAUTION REMOVE THIS EQUALS BULLSHIT remove the entire
						 * first if condition.
						 * 
						 * 
						 */

						if (StockConstants.is_test) {

							// find the relevant stock from redis, or
							// elasticsearch since we cannot know in advance which was the first
							// one that it had chosen for making pairs.
							//so we query the pairs index for any random document.
							//then we get teh stockname from that.
							//and also the name of the indicator.
							//the subindicator in question is always this default one.
							
							System.out.println("making query for stockName inside prepare complexes");
							ArrayList<String> test_stock_data = get_stock_details_for_test();
							String stockName_for_test = test_stock_data.get(0);
							String indicatorName_for_test = test_stock_data.get(1);
							System.out.println("got this stock and indicator name:");
							System.out.println("stockName:" + stockName_for_test);
							System.out.println("indicator name for test:" + indicatorName_for_test);
							try {
								System.in.read();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if (subindicator_in_question
									.equals("subindicator_rises_by_0.25_percent_in_1_session")
									&& indicator_name
											.equals(indicatorName_for_test)
									&& stockName.equals(stockName_for_test)) {

								// check if the groovystatements not applicable
								// to
								// has this object
								// if not then go ahead.
								if (!GroovyStatements.getInstance().indicators_jsonfile
										.getJSONObject(indicator_name)
										.getJSONObject(
												StockConstants.not_applicable_to_object)
										.has(subindicator_in_question)) {

									for (String period : periods) {
										complexes_of_this_indicator
												.add(stockName
														+ "_"
														+ stockFullName
														+ "_"
														+ indice
														+ "_"
														+ indicator_name
														+ "_"
														+ period
														+ "_"
														+ subindicator_in_question);
									}

								}
							}
						}
						// if not testing, and real program.
						else {
							if (!GroovyStatements.getInstance().indicators_jsonfile
									.getJSONObject(indicator_name)
									.getJSONObject(
											StockConstants.not_applicable_to_object)
									.has(subindicator_in_question)) {

								for (String period : periods) {
									complexes_of_this_indicator.add(stockName
											+ "_" + stockFullName + "_"
											+ indice + "_" + indicator_name
											+ "_" + period + "_"
											+ subindicator_in_question);
								}

							}
						}

					}

					// now we add all of them to the threadpool.
					// now add all of them to the threadpool.
					// then clear it or something and do the next one.
					for (String complex_name : complexes_of_this_indicator) {
						String subindicator_in_question = get_subindicator_name_from_complex_name(complex_name);
						ComplexCalculationRequest cerr = new ComplexCalculationRequest(
								stockName, stockFullName, indice, complex_name,
								indicator_name, subindicator_in_question, null,
								null, day_id_from, day_id_to);
						cerr_al.add(cerr);

						getAdg().getTpoolCompl().submit(cerr);

					}

				}

			}

			// now we finished iterating all the indicators for this stock
			// after now we have to iterate the responses.
			for (ComplexCalculationRequest cerr : cerr_al) {
				try {
					Future<String> res = getAdg().getTpoolCompl().take();
					res.get();
				} catch (InterruptedException | ExecutionException e) {
					// this method will log a complex calculation request.
					cerr.build_complex_calculation_error_from_request(
							e,
							"e"
									+ "ither interrupted exception, or execution exception, other than what is already being caught"
									+ "complex calculation request, so probably threadpool issues.");

				}
			}

		}

		// after iterating all the stocks we shutdown the threadpool executor.

		getAdg().getTpoolExec().shutdown();
		Boolean termination_status = getAdg()
				.getTpoolExec()
				.awaitTermination(
						StockConstants.number_of_seconds_to_wait_for_thread_pool_executor_to_shutdown_after_complex_processing,
						TimeUnit.SECONDS);

	}

	public String get_subindicator_name_from_complex_name(String complex_name) {
		Integer last_occurrence = complex_name.lastIndexOf("period_end_");
		Integer chop_point = last_occurrence + 11;
		String subindicator_name = complex_name.substring(chop_point);
		// System.out.println("subindicator name is:" + subindicator_name);
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		return subindicator_name;
	}

}
