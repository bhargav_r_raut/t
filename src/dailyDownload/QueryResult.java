package dailyDownload;

import java.util.HashMap;

import yahoo_finance_historical_scraper.HistoricalStock;

public class QueryResult {
	private HashMap<String, HistoricalStock> hm;
	
	public QueryResult(){
		
	}

	public HashMap<String, HistoricalStock> getHm() {
		return hm;
	}

	public void setHm(HashMap<String, HistoricalStock> hm) {
		this.hm = hm;
	}
	
	
}
