package dailyDownload;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.reflect.TypeToken;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import Indicators.RedisManager;
import Titan.GroovyStatements;


public class RefreshChangedRedisLists {

	
	
	public static Integer get_list_length(){
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			return(jedis.llen(StockConstants.name_of_redis_list_for_complex_objects).intValue());
		}
	}
	
	public static void refresh_changed_redis_lists(){
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			Integer list_length = get_list_length();
			System.out.println("the list length is:" + list_length);
			for(int i=0; i < list_length; i++){
				String map_string = jedis.rpop(StockConstants.name_of_redis_list_for_complex_objects);
				java.lang.reflect.Type t = 
						new TypeToken<Map<String,Object>>() {}.getType();
				///System.out.println(map_string);
				Map<String,Object> m = GroovyStatements.getInstance().gson.fromJson(map_string, t);
				String complex_name = (String)m.get("complex_name");
				System.out.println("this is the complex name:" + complex_name);
				//try {
				//	System.in.read();
				//} catch (IOException e) {
					// TODO Auto-generated catch block
				//	e.printStackTrace();
				//}
				
				if (complex_name.contains("subindicator_value")) {
					LinkedHashMap<Integer, Double> ln = new LinkedHashMap<Integer, Double>();
					jedis.hset(complex_name, "complex_values",
							GroovyStatements.getInstance().gson.toJson(ln));
					System.out.println("reset subindicator value");
				} else if (complex_name
						.contains("subindicator_n_day_stddev")) {
					//System.out.println("it is n day stddev change.");
					//try {
					//	System.in.read();
					//} catch (IOException e) {
						// TODO Auto-generated catch block
					//	e.printStackTrace();
					//}
					Map<Integer, Map<String, Double>> tm = new TreeMap<Integer, Map<String, Double>>();
					jedis.hset(complex_name.getBytes(),
							("complex_values").getBytes(),
							(GroovyStatements.getInstance().gson.toJson(tm)).getBytes());
					System.out.println("reset n day stddev");
				} else {
					int[] empty_placeholder_int_array = new int[0];
					byte[] compressed_byte_array = CompressUncompress
							.compress_integer_array_to_byte_array(empty_placeholder_int_array);
					jedis.hset(complex_name.getBytes(),
							(StockConstants.complex_values_buy_key_name)
									.getBytes(), compressed_byte_array);
					jedis.hset(complex_name.getBytes(),
							(StockConstants.complex_values_sell_key_name)
									.getBytes(), compressed_byte_array);
					System.out.println("reset buy and sells");
				}
			}
			
			System.out.println("the list length is:" + get_list_length());
			jedis.set(StockConstants.redis_key_name_for_main_program_wait_controller, StockConstants.stop_signal);
			jedis.set(StockConstants.redis_key_name_for_jedis_poller_controller, StockConstants.proceed_signal);
		}
		
		
	}
	
	

}
