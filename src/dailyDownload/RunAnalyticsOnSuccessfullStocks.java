package dailyDownload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tictactec.ta.lib.Core;

public class RunAnalyticsOnSuccessfullStocks {

	private HashMap<String, String> stocks_with_some_historical_data;
	private String indice;
	private TitanGraph g;
	private TitanManagement mgmt;

	public HashMap<String, String> getStocks_with_some_historical_data() {
		return stocks_with_some_historical_data;
	}

	public void setStocks_with_some_historical_data(
			HashMap<String, String> stocks_with_some_historical_data) {
		this.stocks_with_some_historical_data = stocks_with_some_historical_data;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public TitanGraph getG() {
		return g;
	}

	public void setG(TitanGraph g) {
		this.g = g;
	}

	public TitanManagement getMgmt() {
		return mgmt;
	}

	public void setMgmt(TitanManagement mgmt) {
		this.mgmt = mgmt;
	}

	public RunAnalyticsOnSuccessfullStocks(String indice, TitanGraph g,
			TitanManagement mgmt) {
		IndiceComponents indcomp = IndiceComponents.getInstance();
		HashMap<String, String> got_stocks = indcomp.getIndice_components()
				.get(indice);
		setStocks_with_some_historical_data(got_stocks);
		setIndice(indice);
		setG(g);
		setMgmt(mgmt);
	}

	/*list of daily indicator properties
	 * 
	 * 1.absolute change
	 * 2.percentage change -
	 * 3.1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, upto 3 months.day forward change -> by edges, 
	 * the edge should hold other useful time data like,week, month, year, quarter etc, as well 
	 * as amount based labels, as well as polarity labels - up, or down.
	 * 4.connection to a static date, for stocks we do this for close prices only.
	 * 5. 
	 * 
	 * 
	 * 
	 */
	
}
