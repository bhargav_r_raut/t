package dailyDownload;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.io.IOException;

import org.elasticsearch.action.count.CountResponse;
import org.javatuples.Pair;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import Indicators.RedisManager;
import elasticSearchNative.LocalEs;

public class SchedulerRedisKeysControl {
	private static final String identifier_for_log = "Scheduler_Reids_keys_controller";
	private Integer caller;
	private Boolean proceed;

	public Boolean getProceed() {
		return proceed;
	}

	public void setProceed(Boolean proceed) {
		this.proceed = proceed;
	}

	public Integer getCaller() {
		return caller;
	}

	public void setCaller(Integer caller) {
		this.caller = caller;
	}

	/**
	 * we need to have two or three control configurations.
	 * 
	 * caller
	 * 
	 * 1.at the start of the program we want to set the main program to wait.
	 * and the scheduler to proceed - only if the keys are null. otherwise we
	 * dont go forwards and send alert email. it means something went wrong the
	 * earlier time
	 * 
	 * 
	 * 2.at the end of the prepare complexes we need to tell jedis poller to
	 * stop
	 * 
	 * 
	 * 3.at the end of jedis poller controller we need to tell the main program
	 * to continue.
	 * 
	 * 
	 * 4.at the end of the main program we need to reset keys to the start
	 * program values.
	 * 
	 * 
	 */
	public SchedulerRedisKeysControl(Integer caller) {
		setCaller(caller);
		setProceed(modulate_keys());

		switch (caller) {
		case 1:

			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"status of setting start keys at start of program:"
							+ getProceed());

			break;

		case 2:

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"status of setting jedis poller to stop:" + getProceed());

			break;

		case 4:

			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"status of setting start keys at end of program:"
							+ getProceed());

			break;

		default:
			break;
		}

		System.out
				.println("now about to go out of the schedulerediskeyswitch thing.");

		//try {
		//	System.in.read();
		//} catch (IOException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
	}

	public Pair<String, String> get_current_keys() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String poller_scheduler_controller_status = jedis
					.get(StockConstants.redis_key_name_for_jedis_poller_controller);
			String main_program_controller_status = jedis
					.get(StockConstants.redis_key_name_for_main_program_wait_controller);
			return new Pair<String, String>(poller_scheduler_controller_status,
					main_program_controller_status);
		} catch (Exception e) {
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
			return null;
		}
	}

	public Boolean set_keys_to_new_program_starting_values() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			jedis.set(
					StockConstants.redis_key_name_for_jedis_poller_controller,
					StockConstants.proceed_signal);
			jedis.set(
					StockConstants.redis_key_name_for_main_program_wait_controller,
					StockConstants.stop_signal);
			return true;
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
			return false;
		}
	}

	public Boolean set_key_to_value(String key, String value) {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			jedis.set(key, value);
			//System.out.println("set key: " + key + " to value: " + value);
			return true;
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
			return false;
		}
	}

	public Boolean modulate_keys() {
		System.out.println("entered modulate keys.");
		Pair<String, String> current_keys = get_current_keys();
		//System.out.println("these are the current keys:"
		//		+ current_keys.getValue0() + " and  "
		//		+ current_keys.getValue1());
		if (current_keys != null) {

			if (caller.equals(1)) {
				// it is the starting of the program.
				// if both are null
				// set both of them to start.
				//System.out.println("entered that caller is 1");
				if (current_keys.getValue0() == null
						&& current_keys.getValue1() == null) {
					// this is the first time.
					// check that there is nothing in the historical data point
					// index.
					// only then return true.
					// otherwise return false with error.
					//System.out.println("both keys got as null");
					CountResponse countresp = LocalEs
							.getInstance()
							.prepareCount("tradegenie_titan")
							.setQuery(
									termQuery("_type", "historical_data_point"))
							.execute().actionGet();
					//System.out.println("the countresponse get count was:"
					//		+ countresp.getCount());
					if (countresp.getCount() == 0l) {
						// set the keys to start program values.
						return (set_keys_to_new_program_starting_values());

					} else {

						LoggingDailyOperations
								.get_instance()
								.add_to_log(
										"exception",
										new Exception(
												"the controller keys were null but there was something in the historical data point index."),
										identifier_for_log, null);
						return false;
					}
				} else {
					if (current_keys.getValue0().equals(
							StockConstants.proceed_signal)
							&& current_keys.getValue1().equals(
									StockConstants.stop_signal)) {
						// its fine.
						System.out
								.println("start values are expected start values so returning true");
						return true;
					} else {
						// there is something wrong.
						// abort the program and return
						LoggingDailyOperations
								.get_instance()
								.add_to_log(
										"exception",
										new Exception(
												"at start of program the keys were not both as expected, but neither were they null."
														+ " they were: jedis scheduler key status : "
														+ current_keys
																.getValue0()
														+ " and "
														+ " main program status: "
														+ current_keys
																.getValue1()),
										identifier_for_log, null);
						return false;
					}
				}

			} else if (caller.equals(2)) {

				return (set_key_to_value(
						StockConstants.redis_key_name_for_jedis_poller_controller,
						StockConstants.stop_signal));
			} else if (caller.equals(3)) {
				// never happens.

				// return (set_key_to_value(
				// StockConstants.redis_key_name_for_main_program_wait_controller,
				// StockConstants.proceed_signal));
			} else if (caller.equals(4)) {

				return (set_keys_to_new_program_starting_values());
			}

		}
		return null;
	}

}
