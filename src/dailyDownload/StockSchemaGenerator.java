package dailyDownload;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.elasticsearch.client.Client;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.LoggingDailyOperations;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;
import Titan.TitanBaseConfig;
import Titan.TitanConstants;
import Titan.TitanGraphSchema;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.EdgeLabel;
import com.thinkaurelius.titan.core.Order;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.attribute.Precision;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Direction;

import elasticSearchNative.ClientInstance;

public class StockSchemaGenerator {

	private static final String identifier_for_log = "StockSchemaGenerator";
	private EdgeLabel common_day_id_day_connector_edge_label;

	public EdgeLabel getCommon_day_id_day_connector_edge_label() {
		return common_day_id_day_connector_edge_label;
	}

	public void setCommon_day_id_day_connector_edge_label(
			EdgeLabel common_day_id_day_connector_edge_label) {
		this.common_day_id_day_connector_edge_label = common_day_id_day_connector_edge_label;
	}

	private TitanGraph g;

	public TitanGraph getG() {
		return g;
	}

	public void setG(TitanGraph g) {
		this.g = g;
	}

	private TitanManagement mgmt;

	public TitanManagement getMgmt() {
		return mgmt;
	}

	public void setMgmt(TitanManagement mgmt) {
		this.mgmt = mgmt;
	}

	private TitanGraphSchema schema;

	public TitanGraphSchema getSchema() {
		return schema;
	}

	public void setSchema(TitanGraphSchema schema) {
		this.schema = schema;
	}

	/**
	 * constructor for generating the stock specific schemas to be called inside
	 * addyourschema. no need to call any additional functions, constructor
	 * calls necessary functions.
	 * 
	 * @param common_day_id_day_connector_edge_label
	 * @param g
	 * @param mgmt
	 * @param schema
	 */
	public StockSchemaGenerator(
			EdgeLabel common_day_id_day_connector_edge_label, TitanGraph g,
			TitanManagement mgmt, TitanGraphSchema schema,
			ConcurrentHashMap<String, PropertyKey> cpk) {
		setCommon_day_id_day_connector_edge_label(common_day_id_day_connector_edge_label);
		setG(g);
		setMgmt(mgmt);
		setSchema(schema);
		
		try {
			generate_schema(cpk);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

	}

	/**
	 * constructor for populating the stocks list json file.
	 * 
	 */
	public StockSchemaGenerator(Client client, TitanBaseConfig cfg, TitanGraph g, TitanManagement mgmt
			, TitanGraphSchema schema) {
		populate_stocks_list(client,cfg,g,mgmt,schema);
		g.shutdown();
	}

	public static JSONObject get_current_stocks_list() throws IOException,
			ParseException {

		String json_stocks_list = ReadWriteTextToFile.getInstance().readFile(
				"Stocks_List.json", TitanConstants.ENCODING);
		JSONParser parser = new JSONParser();
		JSONObject current_stocks_list = (JSONObject) parser
				.parse(json_stocks_list);
		return current_stocks_list;
	}

	public void populate_stocks_list(Client client, TitanBaseConfig cfg, TitanGraph g, TitanManagement mgmt
			, TitanGraphSchema schema) {
		String json_stocks_list;
		try {

			JSONObject current_stocks_list = get_current_stocks_list();

			for (int i = 0; i < StockConstants.indices_main.length; i++) {

				String exchange = StockConstants.indices_main[i];
				List<HashMap<String, String>> stocks_in_exchange = GroovyStatements
						.getInstance().searchExchange(exchange);
				if (!stocks_in_exchange.isEmpty()) {
					for (HashMap<String, String> hm : stocks_in_exchange) {
						current_stocks_list.put(hm.get("symbol"),
								hm.get("fullName"));
					}
				}

			}
			
			LoggingDailyOperations.get_instance().add_to_log("info",
					null,
					identifier_for_log,
					"added following things to stocks list file:" + current_stocks_list.keySet().toString());
			
			ReadWriteTextToFile.getInstance().writeFile("Stocks_List.json",
					current_stocks_list.toJSONString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception",
					e, identifier_for_log, null);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LoggingDailyOperations.get_instance().add_to_log("exception",
					e, identifier_for_log, null);
		}

	}

	public void generate_schema(ConcurrentHashMap<String,PropertyKey> cpk) throws IOException, ParseException {

		JSONObject current_stocks_list = get_current_stocks_list();
		Set<Object> keys = current_stocks_list.keySet();
		for (Object key : keys) {
			String stock_combination = key.toString() + "_"
					+ current_stocks_list.get(key);

			try {
				schema.create_key("days_back_" + stock_combination, "Edge",
						true, false, true, Integer.class, true,
						common_day_id_day_connector_edge_label, g, mgmt,
						cpk);
				//create the vertex centric indice for the days back of this company..
				schema.create_vertex_centric_index_on_edgelabel(mgmt, "days_back_vertex_centric_index_" + stock_combination,
						"days_back_" + stock_combination,
						new PropertyKey[]{cpk.get("days_back_" + stock_combination)}, Direction.BOTH, Order.ASC,
						common_day_id_day_connector_edge_label);
				
				
				schema.create_key("price_change_percentage_"
						+ stock_combination, "Edge", true, false, true,
						Precision.class, true,
						common_day_id_day_connector_edge_label, g, mgmt, cpk);
				//create the vertex centric indice for the price_change_percentage
				schema.create_vertex_centric_index_on_edgelabel(mgmt, "price_change_percentage_vertex_centric_index_"
				+ stock_combination,
						"price_change_percentage_" + stock_combination,
						new PropertyKey[]{cpk.get("price_change_percentage_" + stock_combination)},
						Direction.BOTH, Order.ASC,
						common_day_id_day_connector_edge_label);
				
				
				// 1->profit, -1->loss, 0->no change
				schema.create_key("profit_or_loss_" + stock_combination,
						"Edge", true, false, true, Integer.class, true,
						common_day_id_day_connector_edge_label, g, mgmt,
						cpk);
				schema.create_vertex_centric_index_on_edgelabel(mgmt, "profit_or_loss_vertex_centric_index_" 
						+ stock_combination,
						"profit_or_loss_" + stock_combination,
						new PropertyKey[]{cpk.get("profit_or_loss_" + stock_combination)},
						Direction.BOTH, Order.ASC,
						common_day_id_day_connector_edge_label);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
