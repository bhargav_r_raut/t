package dailyDownload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TPair {

	private String pair_name;
	private Integer pair_difference;
	private ArrayList<HashMap<String,Object>> year;
	private ArrayList<HashMap<String,Object>> quarter;
	private ArrayList<HashMap<String,Object>> month;
	private ArrayList<HashMap<String,Object>> week_of_month;
	private ArrayList<HashMap<String,Object>> day_of_week;
	private ArrayList<HashMap<String,Object>> pair_stock_changes;
	private Map<String,Object> m;
	
	public String getPair_name() {
		return pair_name;
	}


	public void setPair_name(String pair_name) {
		this.pair_name = pair_name;
	}


	public Integer getPair_difference() {
		return pair_difference;
	}



	public void setPair_difference(Integer pair_difference) {
		this.pair_difference = pair_difference;
	}


	
	public ArrayList<HashMap<String, Object>> getYear() {
		return year;
	}



	public void setYear(ArrayList<HashMap<String, Object>> year) {
		this.year = year;
	}



	public ArrayList<HashMap<String, Object>> getQuarter() {
		return quarter;
	}



	public void setQuarter(ArrayList<HashMap<String, Object>> quarter) {
		this.quarter = quarter;
	}



	public ArrayList<HashMap<String, Object>> getMonth() {
		return month;
	}



	public void setMonth(ArrayList<HashMap<String, Object>> month) {
		this.month = month;
	}



	public ArrayList<HashMap<String, Object>> getWeek_of_month() {
		return week_of_month;
	}



	public void setWeek_of_month(ArrayList<HashMap<String, Object>> week_of_month) {
		this.week_of_month = week_of_month;
	}



	public ArrayList<HashMap<String, Object>> getDay_of_week() {
		return day_of_week;
	}



	public void setDay_of_week(ArrayList<HashMap<String, Object>> day_of_week) {
		this.day_of_week = day_of_week;
	}



	public ArrayList<HashMap<String, Object>> getPair_stock_changes() {
		return pair_stock_changes;
	}



	public void setPair_stock_changes(
			ArrayList<HashMap<String, Object>> pair_stock_changes) {
		this.pair_stock_changes = pair_stock_changes;
	}



	public TPair(String pair_name, Integer pair_difference){
		this.pair_difference = pair_difference;
		this.pair_name = pair_name;
		this.year = new ArrayList<HashMap<String,Object>>();
		this.month = new ArrayList<HashMap<String,Object>>();
		this.quarter = new ArrayList<HashMap<String,Object>>();
		this.week_of_month = new ArrayList<HashMap<String,Object>>();
		this.day_of_week = new ArrayList<HashMap<String,Object>>();
		this.pair_stock_changes = new ArrayList<HashMap<String,Object>>();
		this.m = new HashMap<String, Object>();
	}
	
	
	public Map<String,Object> get_map_representation(){
		this.m.put("pair_difference", this.pair_difference);
		this.m.put("pair_name", this.pair_name);
		this.m.put("year", this.year);
		this.m.put("month", this.month);
		this.m.put("quarter", this.quarter);
		this.m.put("week_of_month", this.week_of_month);
		this.m.put("day_of_week", this.day_of_week);
		this.m.put("pair_stock_changes", this.pair_stock_changes);
		return this.m;
	}
	

}
