package dailyDownload;

public class TimeZoneStringInvalid extends Exception {
	public TimeZoneStringInvalid(String message){
		super(message);
	}
}
