package elasticSearchNative;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class BaseElasticHttpRequest {
	public static final MediaType JSON = MediaType
			.parse("application/json; charset=utf-8");
	public String base_url = "http://192.168.10.5:9200";

	public String getBase_url() {
		return base_url;
	}

	public void setBase_url(String base_url) {
		this.base_url = base_url;
	}

	/**
	 * 
	 * get , put, post, delete
	 * 
	 * @return
	 */
	private String request_type;
	private String json_request;
	private ArrayList<String> sub_paths;
	private HashMap<String, Object> query_params;
	private Response response;
	private String response_body;

	public String getResponse_body() {
		return response_body;
	}

	public void setResponse_body(String response_body) {
		this.response_body = response_body;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public HashMap<String, Object> getQuery_params() {
		return query_params;
	}

	public void setQuery_params(HashMap<String, Object> query_params) {
		this.query_params = query_params;
	}

	private String final_url;

	public String getFinal_url() {
		return final_url;
	}

	public void setFinal_url(String final_url) {
		this.final_url = final_url;
	}

	public ArrayList<String> getSub_paths() {
		return sub_paths;
	}

	public void setSub_paths(ArrayList<String> sub_paths) {
		this.sub_paths = sub_paths;
	}

	public String getRequest_type() {
		return request_type;
	}

	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}

	public String getJson_request() {
		return json_request;
	}

	public void setJson_request(String json_request) {
		this.json_request = json_request;
	}

	public void build_url() {
		String final_url = base_url;
		for (String path : getSub_paths()) {

			final_url = final_url + "/" + path;
		}

		Iterator it = getQuery_params().entrySet().iterator();
		String query = "";
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();

			try {
				query += URLEncoder.encode(String.valueOf(pairs.getKey()),
						"utf-8")
						+ "="
						+ URLEncoder.encode(String.valueOf(pairs.getValue()),
								"utf-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (query != "") {
			setFinal_url(final_url + "?" + query);
		} else {
			setFinal_url(final_url);
		}

	}

	public BaseElasticHttpRequest(String request_type, String json_request,
			ArrayList<String> sub_paths,
			HashMap<String, Object> query_parameters) throws IOException {

		setJson_request(json_request);
		setRequest_type(request_type);
		setSub_paths(sub_paths);
		setQuery_params(query_parameters);
		build_url();
		make_request();
	}

	public void make_request() throws IOException {

		OkHttpClient ok_http_client = new OkHttpClient();
		RequestBody body = RequestBody.create(JSON, getJson_request());

		Builder request = new Request.Builder().url(getFinal_url());
		System.out.println(getFinal_url());
		if (request_type.equals("post")) {
			request = request.post(body);
		} else if (request_type.equals("get")) {
			request = request.get();
		} else if (request_type.equals("put")) {
			request = request.put(body);
		} else if (request_type.equals("delete")) {
			request = request.delete();
		}
		
		
		
		Request request_main = request.build();
		System.out.println("body:" + request_main.body());
		Response response = ok_http_client.newCall(request_main).execute();
		setResponse(response);
		if (response.code() <= 400) {
			System.out.println("successfull");
			setResponse_body(response.body().string());
		} else {
			System.out.println(response.code());
			System.out.println("failed");
			System.out.println(response.message());
			System.out.println(response.body());
			setResponse_body(null);
		}
		
	}

}
