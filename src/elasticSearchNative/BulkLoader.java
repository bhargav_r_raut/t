package elasticSearchNative;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.javatuples.Pair;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import ExchangeBuilder.CentralSingleton;
import Indicators.RedisManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BulkLoader {

	private Integer bulk_items_amount;
	private BulkRequestBuilder brq;
	private ArrayList<Map<String, Object>> index_request_list;
	private String index_name;
	private String type;
	private Gson gson;
	private static final String identifier_for_log = "bulk_loader";
	private static BulkLoader bulkloader;

	public String getIndex_name() {
		return index_name;
	}

	public void setIndex_name(String index_name) {
		this.index_name = index_name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getBulk_items_amount() {
		return bulk_items_amount;
	}

	public void setBulk_items_amount(Integer bulk_items_amount) {
		this.bulk_items_amount = bulk_items_amount;
	}

	public BulkRequestBuilder getBrq() {
		return brq;
	}

	public void setBrq(BulkRequestBuilder brq) {
		this.brq = brq;
	}

	public ArrayList<Map<String, Object>> getIndex_request_list() {
		return index_request_list;
	}

	public void setIndex_request_list(
			ArrayList<Map<String, Object>> index_request_list) {
		this.index_request_list = index_request_list;
	}

	/**
	 * @param bulk_items_amount
	 * @param index_name
	 * @param type
	 */
	public BulkLoader(Integer bulk_items_amount, String index_name, String type) {

		this.bulk_items_amount = bulk_items_amount;
		this.index_name = index_name;
		this.type = type;
		this.brq = new BulkRequestBuilder(LocalEs.getInstance());
		this.index_request_list = new ArrayList<Map<String, Object>>();
		this.gson = new GsonBuilder().setPrettyPrinting().create();

	}

	/**
	 * success objects list is element 0 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * id => the id of the document that was updated <br>
	 * <br>
	 * failed objects list is element 1 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * cause => the id of the document that was updated.
	 * 
	 * @param item_source
	 * @return
	 * 
	 */
	public Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> index_documents_and_flush_bulk() {

		// System.out.println("size of list is:" +
		// getIndex_request_list().size());
		if (getBrq().numberOfActions() == 0) {
			System.out
					.println("nothing in the bulk loader, so index documents and flush bulk returns null");
			return null;
		} else {
			
			ArrayList<HashMap<String, Object>> failed_requests = new ArrayList<HashMap<String, Object>>();
			ArrayList<HashMap<String, Object>> successfully_ids = new ArrayList<HashMap<String, Object>>();
			BulkResponse response = getBrq().execute().actionGet();
			Iterator<BulkItemResponse> it = response.iterator();
			Integer counter = 0;
			try(Jedis jedis = RedisManager.getInstance().getJedis()){
				Pipeline pipe = jedis.pipelined();
				while (it.hasNext()) {
					
					final BulkItemResponse resp = it.next();
					String id = resp.getId();
					pipe.set(String.valueOf(counter) + "_key", id);
					counter++;
				}
				pipe.sync();
				System.out.println("did redis.");
			}
			/**
			Iterator<BulkItemResponse> it = response.iterator();
			Integer counter = 0;
			while (it.hasNext()) {
				final BulkItemResponse resp = it.next();
				final Map<String, Object> item_vertex_map = getIndex_request_list()
						.get(counter);
				final String failure_message = resp.getFailureMessage();

				if (resp.isFailed()) {
					failed_requests.add(new HashMap<String, Object>() {
						{
							put("item", item_vertex_map);
							put("error_cause", failure_message);
						}
					});
				} else {
					successfully_ids.add(new HashMap<String, Object>() {
						{
							put("item", item_vertex_map);
							put("id", resp.getId());
						}
					});

				}

				counter++;
			}
			getIndex_request_list().clear();
			Pair p = new Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>>(
					successfully_ids, failed_requests);
			return p;
			**/
			return null;
			
		}
	}

	/**
	 * success objects list is element 0 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * id => the id of the document that was updated <br>
	 * <br>
	 * failed objects list is element 1 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * error_cause => the id of the document that was updated.
	 * 
	 * @param item_source
	 * @return
	 */
	public Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> add_item_to_bulk_loader(
			Map<String, Object> item_source) {

		try {
			ObjectMapper objmap = new ObjectMapper();
			final String json_string = objmap
					.writeValueAsString(item_source);
			if (getBrq().numberOfActions() == bulk_items_amount.intValue()) {

				Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = index_documents_and_flush_bulk();

				setBrq(LocalEs.getInstance().prepareBulk());
				//getIndex_request_list().add(item_source);
				getBrq().add(
						LocalEs.getInstance()
								.prepareIndex(getIndex_name(), getType())
								.setSource(item_source));

				return response;

			} else {

				//getIndex_request_list().add(item_source);
				getBrq().add(
						LocalEs.getInstance()
								.prepareIndex(getIndex_name(), getType())
								.setSource(json_string));
				return null;
			}
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}

	}

	public Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> add_item_to_bulk_loader(
			byte[] item_source, Map<String, Object> item_source_as_map) {

		if (getBrq().numberOfActions() == bulk_items_amount.intValue()) {

			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = index_documents_and_flush_bulk();

			setBrq(LocalEs.getInstance().prepareBulk());
			getIndex_request_list().add(item_source_as_map);
			getBrq().add(
					LocalEs.getInstance()
							.prepareIndex(getIndex_name(), getType())
							.setSource(item_source));

			return response;

		} else {

			getIndex_request_list().add(item_source_as_map);
			getBrq().add(
					LocalEs.getInstance()
							.prepareIndex(getIndex_name(), getType())
							.setSource(item_source));
			return null;
		}

	}

	/**
	 * success objects list is element 0 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * id => the id of the document that was updated <br>
	 * <br>
	 * failed objects list is element 1 of pair: its a list of maps. each map
	 * has the following two elements: <br>
	 * item => the original map of params supplied in the request. <br>
	 * error_cause => the id of the document that was updated.
	 * 
	 * 
	 * 
	 * @param req
	 *            : the es updaterequest.
	 * @param item_source
	 *            : the map of params.
	 * @return
	 */
	public Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> add_update_item_to_es(
			UpdateRequest req, Map<String, Object> item_source) {
		// System.out.println("the brq now has:" + getBrq().numberOfActions());
		// System.out.println("the bulk items amount is:" +
		// bulk_items_amount.intValue());
		if (getBrq().numberOfActions() == bulk_items_amount.intValue()) {
			// System.out.println("went ahead to index and flush.");
			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = index_documents_and_flush_bulk();

			setBrq(LocalEs.getInstance().prepareBulk());
			getIndex_request_list().add(item_source);
			getBrq().add(req);

			return response;

		} else {
			// System.out.println("did nothihg, see this is how the index list looks.");
			getIndex_request_list().add(item_source);
			// System.out.println(getIndex_request_list());
			getBrq().add(req);
			return null;
		}

	}

}
