package elasticSearchNative;

import java.util.concurrent.atomic.AtomicInteger;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class ClientInstance {
	
	private static Client cl;
	
	private AtomicInteger silver_counter;
	
	public static ClientInstance client;
	
	public static ClientInstance getInstance(){
		if(client == null){
			client = new ClientInstance();
			client.setCl(client.return_client());
			return client; 
		}
		else{
			return client;
		}
	}
	
	public Client return_client() {

		final ImmutableSettings.Builder settings = ImmutableSettings
				.settingsBuilder();
		TransportClient transportClient = new TransportClient(settings);
		transportClient = transportClient
				.addTransportAddress(new InetSocketTransportAddress(
						elasticSearchConstants.hostname,
						elasticSearchConstants.port));
		this.silver_counter = new AtomicInteger();
		return transportClient;
	}

	/**
	 * @return the cl
	 */
	public static Client getCl() {
		return cl;
	}

	/**
	 * @param cl the cl to set
	 */
	public static void setCl(Client cl) {
		ClientInstance.cl = cl;
	}
	
	public void inc_at(){
		this.silver_counter.incrementAndGet();
	}
	
	public void get_inc(){
		System.out.println("the total silver count is -->" + this.silver_counter.toString());
	}
}
