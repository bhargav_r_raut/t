package elasticSearchNative;

import java.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.BufferedReader;
import java.util.*;

import org.elasticsearch.*;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.jackson.core.JsonParser;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import static org.elasticsearch.common.xcontent.XContentFactory.*;

public class ElasticSearchFirst {

	public static void main(String[] args) throws ElasticsearchException,
			IOException {
		// TODO Auto-generated method stub
		System.out.println("hello world.");

		// on startup
		String index_name = "tradegenie";
		String documenttype = "statistic";
		int number_of_days_for_search_test = 40;

		boolean search = true;
		Client client = return_client();

		if (search) {
			//GetResponse docresponse = client
					//.prepareGet(index_name, documenttype, "M10").execute()
					//.actionGet();

			List<Integer> search_list = new ArrayList<Integer>();

			Random randomGenerator = new Random();
			for (int idx = 1; idx <= number_of_days_for_search_test; ++idx) {
				int randomInt = randomGenerator.nextInt(3000);
				search_list.add(randomInt);
			}

			SearchResponse response = client.prepareSearch(index_name)
					.setQuery(QueryBuilders.termsQuery("days", search_list)).execute().actionGet();
			// .setPostFilter(FilterBuilders.termsFilter("days",search_list))
			SearchHit[] results = response.getHits().getHits();
			puts("the query took" + Float.toString(response.getTookInMillis()));
			System.out.println("Current results: " + results.length);
			for (SearchHit hit : results) {
				System.out.println("------------------------------");
				puts(Float.toString(hit.getScore()));
				Map<String, Object> result = hit.getSource();
				System.out.println(result);
			}

			// }

		} else {

			puts("now returned client.");
			// delete index
			puts("now deleting existing index");
			delete_existing_index(client, index_name);
			// create index
			puts("now creating neww index with this name.");
			CreateIndexRequestBuilder createIndexRequestBuilder = create_index(
					client, index_name);
			// create mapping.
			puts("now creating mapping.");
			domapping(documenttype, createIndexRequestBuilder);
			// load data.
			puts("now loading bulk data");
			loadbulkdata(client, index_name, documenttype);
			// search data.
		}
	}

	private static void puts(String string) {
		System.out.println(string);
	}

	private static void delete_existing_index(Client client, String index_name) {
		// TODO Auto-generated method stub
		final IndicesExistsResponse res = client.admin().indices()
				.prepareExists(index_name).execute().actionGet();
		if (res.isExists()) {
			puts("the older index exists");
			final DeleteIndexRequestBuilder delIdx = client.admin().indices()
					.prepareDelete(index_name);
			delIdx.execute().actionGet();
		}
	}

	private static void loadbulkdata(Client client, String index_name,
			String documenttype) {
		// TODO Auto-generated method stub
		
		for (int i = 1; i < 20; i++) {
			
			BulkRequestBuilder bulk = client.prepareBulk();
			
			for (int doc_count = (5000*i) - 5001; doc_count < 5000*i; doc_count++) {
				List<Integer> list = new ArrayList<Integer>();
				Random randomGenerator = new Random();
				for (int idx = 1; idx <= 100; ++idx) {
					int randomInt = randomGenerator.nextInt(3000);
					list.add(randomInt);
				}

				puts("creating id number ---->" + Integer.toString(doc_count));

				XContentBuilder builder = null;
				try {
					builder = jsonBuilder().startObject().field("days", list)
							.field("name", Integer.toString(doc_count))
							.endObject();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				bulk.add(client.prepareIndex(index_name, documenttype,
						"M" + Integer.toString(doc_count)).setSource(builder));
			}

			BulkResponse bulkResponse = bulk.execute().actionGet();
			if (bulkResponse.hasFailures()) {
				System.out.println("failures.");
			}

		}

	}

	private static void domapping(String documenttype,
			CreateIndexRequestBuilder createindexrequestbuilder) {
		// TODO Auto-generated method stub
		// create mapping.
		XContentBuilder builder = null;
		try {
			builder = jsonBuilder().startObject().startObject(documenttype)
					.startObject("properties").startObject("days")
					.field("type", "integer").endObject().startObject("name")
					.field("type", "string").endObject().endObject()
					.endObject().endObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// add mapping to index.
		createindexrequestbuilder.addMapping(documenttype, builder).execute()
				.actionGet();

	}

	private static CreateIndexRequestBuilder create_index(Client client,
			String index_name) {
		// TODO Auto-generated method stub
		final CreateIndexRequestBuilder createIndexRequestBuilder = client
				.admin().indices().prepareCreate(index_name);
		return createIndexRequestBuilder;
	}

	private static Client return_client() {

		final ImmutableSettings.Builder settings = ImmutableSettings
				.settingsBuilder();
		TransportClient transportClient = new TransportClient(settings);
		transportClient = transportClient
				.addTransportAddress(new InetSocketTransportAddress(
						"localhost", 9300));
		return transportClient;
	}

}

