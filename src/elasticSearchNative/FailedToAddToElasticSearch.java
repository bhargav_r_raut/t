package elasticSearchNative;

public class FailedToAddToElasticSearch extends Exception {
	private String message;
	public FailedToAddToElasticSearch(String message){
		super(message);
	}
}
