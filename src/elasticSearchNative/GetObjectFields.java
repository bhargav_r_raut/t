package elasticSearchNative;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.json.JSONException;

import ExchangeBuilder.DataPoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetObjectFields {

	/**
	 * @param args
	 * @throws JSONException
	 * @throws JsonProcessingException
	 */
	public static void main(String[] args) throws JSONException,
			JsonProcessingException {
		// TODO Auto-generated method stub

		// INSTANTIATE DESIRED CLASS HERE AND WATCH MAGIC.
		DataPoint obj = new DataPoint();
		;

		Field[] fields = obj.getClass().getDeclaredFields();
		HashMap<String, HashMap<String, Object>> mapping = new LinkedHashMap<String, HashMap<String, Object>>();
		for (int i = 0; i < fields.length; i++) {
			final Field f = fields[i];

			if (f.getAnnotation(JsonIgnore.class) == null && (!f.getType().getName().toLowerCase().contains("linkedlist")) && 
					!f.getType().getName().toLowerCase().contains("map")) {

				mapping.put(fields[i].getName(), new HashMap<String, Object>() {
					{
						String type = f.getType().getName()
								.replace("java.util.", "")
								.replace("java.lang.", "").toLowerCase();

						if (type.contains("arraylist")) {
							ParameterizedType arraylisttype = (ParameterizedType) f
									.getGenericType();
							Class<?> arraylist_generic_type = (Class<?>) arraylisttype
									.getActualTypeArguments()[0];
							type = arraylist_generic_type.getName()
									.replace("java.lang.", "").toLowerCase();

						}

						put("type", type);
						if (f.getType().getName().contains("String")) {
							put("index", "not_analyzed");
						}

					}
				});
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(mapping));
	}

}
