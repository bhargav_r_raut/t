package elasticSearchNative;

import java.util.ArrayList;
import java.util.HashMap;

import org.javatuples.Pair;

public interface IBulkLoader {
	/**
	 * first value of pair : successfully put maps.
	 * second value of pair: failed maps.
	 * 
	 * @param response
	 */
	public abstract void process_response(
			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response,
			ArrayList<Object> other_required_objects);
	
	
}
