package elasticSearchNative;

import genericIndicatorFunctions.Indicator;

import java.util.ArrayList;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import yahoo_finance_historical_scraper.StockConstants;


/***
 * THE HTTP PORT HAS BEEN AND TRANSPORT PORT HAS BEEN BLOCKED OUT IN THE 
 * ELASTICSEARCH.YML FILE IN THE SCRIPTS OF THE elasticsearch-1.4.4 folder on desktop
 * at this point, it seems that the default 9200 is being used for local es.
 * 
 * transport port is being used which also has been blocked out in elasticsearch.yml,
 * but its default is 9300
 * 
 * @author bhargav
 *
 */
public class LocalEs {

	private static Client instance;

	public static Client return_client() {

		final ImmutableSettings.Builder settings = ImmutableSettings
				.settingsBuilder()
				.put("transport.tcp.connect_timeout","3s")
				;
		TransportClient transportClient = new TransportClient(settings);
		transportClient = transportClient
				.addTransportAddress(new InetSocketTransportAddress(
						StockConstants.elasticsearch_local_host_name,
						StockConstants.es_local_transport_port));
		return transportClient;
	}
	
	public static synchronized Client getInstance(){
		
		if(instance == null){
			instance = return_client();
		}
		else{
			
		}
	
		return instance;
		
	}
	


}
