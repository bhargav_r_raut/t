package elasticSearchNative;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class MultiThreadedBulkLoader {

	public static MultiThreadedBulkLoader instance;

	public static void setInstance(MultiThreadedBulkLoader instance) {
		MultiThreadedBulkLoader.instance = instance;
	}

	private final ReentrantLock ret = new ReentrantLock(true);

	private BulkLoader loader;

	public BulkLoader getLoader() {
		return loader;
	}

	public void setLoader(BulkLoader loader) {
		this.loader = loader;
	}

	private ArrayBlockingQueue<List<Object>> arq;

	public ArrayBlockingQueue<List<Object>> getArq() {
		return arq;
	}

	public void setArq(ArrayBlockingQueue<List<Object>> arq) {
		this.arq = arq;
	}

	public static MultiThreadedBulkLoader get_instance() {
		return instance;
	}

	public static void initialize_singleton() {
		instance = new MultiThreadedBulkLoader();
		instance.loader = new BulkLoader(3000, "tradegenie_titan", "complex");
		instance.arq = new ArrayBlockingQueue<List<Object>>(6000);

	}

	public synchronized void add_item(byte[] request,
			Map<String, Object> request_params) {
		getArq().add(Arrays.asList(new Object[] { request, request_params }));
		if (getArq().size() > 1) {
			// add the item to the bulk loader.
			// if the number has been exceeded then the bulk loader will
			// automatically flush.
			getLoader().add_item_to_bulk_loader(request, request_params);
		}
	}

	public synchronized void add_item(Map<String, Object> request_params) {
		getArq().add(Arrays.asList(new Object[] { request_params }));
		if (getArq().size() > 1) {
			// add the item to the bulk loader.
			// if the number has been exceeded then the bulk loader will
			// automatically flush.
			getLoader().add_item_to_bulk_loader(request_params);
		}
	}

}
