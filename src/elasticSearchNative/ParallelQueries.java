package elasticSearchNative;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkProcessor.Listener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import ComplexProcessor.BlockingThreadPoolExecutor;
import ExchangeBuilder.PairArray;
import Indicators.RedisManager;



public class ParallelQueries {

	

	public static final String[] alphabet = { "a", "b", "c", "d", "e", "f",
			"g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
			"t", "u", "v", "w", "x", "y", "z" };

	public static final String[] alphabet_capital = { "A", "B", "C", "D", "E",
			"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
			"S", "T", "U", "V", "W", "X", "Y", "Z" };

	public static AtomicLong millis = new AtomicLong(1l);
	public static AtomicInteger req_counter = new AtomicInteger(1);

	public static String[] build_random_pair_names() {
		Random r = new Random();
		ArrayList<String> pairs = new ArrayList<String>();
		for (int i = 0; i < 2500; i++) {
			Integer start_pair = r.nextInt(3000);
			Integer end_pair = r.nextInt(3350 - start_pair) + start_pair;
			pairs.add(start_pair.toString() + "_" + end_pair.toString());
		}
		return pairs.toArray(new String[pairs.size()]);
	}

	public static HashMap<Integer, Integer[]> get_random_arrays_hashmap() {

		HashMap<Integer, Integer[]> test_hm = new LinkedHashMap<Integer, Integer[]>();
		for (int i = 0; i < 50000; i++) {
			Integer[] comp = generate_random_compressed_sorted_array();
			test_hm.put(i, comp);
			// System.out.println("generating random aray:" +
			// String.valueOf(i));
		}

		return test_hm;

	}

	public static Integer[] generate_random_compressed_sorted_array() {
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			int[] uncompressed_9000_values = new int[3000];
			Random r = new Random();
			// lets say that it changes 500 times.

			HashMap<Integer, Integer> change_points = new HashMap<Integer, Integer>();
			for (int i = 0; i < 1000; i++) {
				change_points.put(r.nextInt(2999), 10);
			}
			Integer prev_val = 0;
			for (int i = 0; i < 3000; i++) {
				if (change_points.containsKey(i)) {
					uncompressed_9000_values[i] = i;
					prev_val = i;
				} else {
					uncompressed_9000_values[i] = prev_val;
				}
			}

			int[] compressed_arr = PairArray.get_compressed_int_array(
					uncompressed_9000_values, 1);

			Integer[] carr = new Integer[compressed_arr.length];

			for (int i = 0; i < compressed_arr.length; i++) {
				carr[i] = compressed_arr[i];
			}

			return carr;
		}

	}

	public static void test_bulk_processor(final Boolean with_shards) {
		BulkProcessor processor = BulkProcessor
				.builder(LocalEs.getInstance(), new Listener() {

					@Override
					public void beforeBulk(long executionId, BulkRequest request) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterBulk(long executionId,
							BulkRequest request, Throwable failure) {
						// TODO Auto-generated method stub
						failure.printStackTrace();
					}

					@Override
					public void afterBulk(long executionId,
							BulkRequest request, BulkResponse response) {
						// TODO Auto-generated method stub
						try (Jedis jedis = RedisManager.getInstance()
								.getJedis()) {
							Pipeline pipe = jedis.pipelined();
							Integer counter = 0;
							for (BulkItemResponse resp : response.getItems()) {
								if (counter % 10 == 0) {
									pipe.lpush("es_ids", resp.getId());
									if (with_shards) {
										HashMap<String, Object> payload = (HashMap<String, Object>) request
												.payloads().get(counter);

										pipe.lpush("es_shards",
												(String) payload
														.get("shard_name"));
									}
								}
								counter++;
							}
							pipe.sync();
						}
					}
				}).setConcurrentRequests(1).setBulkActions(500).build();

		// ExecutorService executor = Executors.newFixedThreadPool(4);
		// IdGenerator snowflake = IdGenerators.newSnowflakeIdGenerator();
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(
				50);
		BlockingThreadPoolExecutor executor = new BlockingThreadPoolExecutor(1,
				5, 5000, TimeUnit.MILLISECONDS, blockingQueue);

		Integer counter = 0;
		Random r = new Random();

		HashMap<Integer, Integer[]> random_compressed_arrays_hashmap = null;
		for (int entity = 0; entity < 325; entity++) {
			for (int indicator = 0; indicator < 25; indicator++) {
				for (int subindicator = 0; subindicator < 25; subindicator++) {
					String subindicator_prefix = "s" + alphabet[subindicator];
					for (int subindicator_index = 0; subindicator_index < 50; subindicator_index++) {
						String entity_name = "E" + String.valueOf(entity);
						String indicator_name = "I" + String.valueOf(indicator);
						String subindicator_name = subindicator_prefix
								+ subindicator_index;

						if (counter % 100000 == 0) {
							System.out
									.println("generating random compressed hashmap, and counter is:"
											+ counter);
							random_compressed_arrays_hashmap = get_random_arrays_hashmap();
						}

						if (counter % 10000 == 0) {
							System.out.println("doing :" + counter);
						}

						Integer[] a = random_compressed_arrays_hashmap.get(r
								.nextInt(49999));

						String shard_name = String.valueOf(r.nextInt(100000));

						if (with_shards) {
							Runnable worker = new MyBulkRunnable(processor,
									entity_name + "-" + indicator_name + "-"
											+ subindicator_name, shard_name, a);
							executor.execute(worker);
						} else {
							Runnable worker = new MyBulkRunnable(processor,
									entity_name + "-" + indicator_name + "-"
											+ subindicator_name, null, a);
							executor.execute(worker);
						}

						counter++;

					}
				}
			}
		}

		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");
		try {
			processor.awaitClose(30l, TimeUnit.SECONDS);
			System.out.println("waiting for processor to close.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void test_parallel_mgets(Boolean with_shards) {
		// we want to fire 10000 mgets, each getting four random documents.
		// we want to get the average return time.
		// we use 4 threads.

		System.out.println("starting mget test.");
		ExecutorService executor = Executors.newFixedThreadPool(4);
		Random r = new Random();

		for (int i = 0; i < 10000; i++) {
			System.out.println("doing nth time:" + String.valueOf(i));
			String ids[] = new String[4];
			String shard_names[] = new String[4];
			for (int l = 0; l < 4; l++) {
				Integer entity_counter = r.nextInt(325);
				Integer indicator_counter = r.nextInt(25);
				Integer subindicator_counter = r.nextInt(25);
				Integer subindicator_inner = r.nextInt(49);

				String id = "E" + entity_counter + "-I" + indicator_counter
						+ "-s" + alphabet[subindicator_counter]
						+ subindicator_inner;

				ids[l] = id;
				shard_names[l] = String.valueOf(entity_counter / 30);
			}
			Runnable worker = new MGETRunnable(ids);
			executor.execute(worker);

		}

		/**
		 * try (Jedis jedis = RedisManager.getInstance().getJedis()) {
		 * 
		 * ArrayList<Response<String>> random_ids = new
		 * ArrayList<Response<String>>(); ArrayList<Response<String>>
		 * random_shard_ids = new ArrayList<Response<String>>(); for (int t = 0;
		 * t < 400; t++) { Pipeline pipe = jedis.pipelined();
		 * System.out.println("did t:" + String.valueOf(t)); for (int i = 0; i <
		 * 100; i++) { long lindex = new Integer(r.nextInt(100000)).longValue();
		 * random_ids.add(pipe.lindex("es_ids", lindex)); if (with_shards) {
		 * random_shard_ids.add(pipe.lindex("es_shards", lindex)); } }
		 * pipe.sync(); }
		 * 
		 * for (int i = 0; i < 10000; i++) { ArrayList<String> ids = new
		 * ArrayList<String>(); String[] shard_ids = new String[4]; for (int j =
		 * i; j < i + 4; j++) { ids.add(random_ids.get(j).get()); if
		 * (with_shards) { shard_ids[j - i] = random_shard_ids.get(j).get(); } }
		 * String[] ids_string = new String[ids.size()]; ids_string =
		 * ids.toArray(ids_string); Runnable worker = new
		 * MGETRunnable(ids_string, bucket); executor.execute(worker);
		 * System.out.println("doing nth time:" + String.valueOf(i)); } }
		 **/
		executor.shutdown();
		// Wait until all threads are finish
		System.out.println("waiting for shutdown.");
		while (!executor.isTerminated()) {

		}
		System.out.println("average time take per request:");
		Long total_time = millis.get();
		Double total_time_d = total_time.doubleValue();
		Integer total_count = req_counter.intValue();
		Double total_count_d = total_count.doubleValue();
		System.out.println(total_time_d / total_count_d);

	}

	public static class MGETRunnable implements Runnable {

		private String[] ids;
		private String[] shard_names;
		

		public MGETRunnable(String[] ids) {
			this.ids = ids;
		}

		public MGETRunnable(String[] ids, String[] shard_names) {
			this.ids = ids;
			this.shard_names = shard_names;
		}

		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			GregorianCalendar start_cal = new GregorianCalendar();
			// System.out.println("making request for ids:");
			StringBuilder sb = new StringBuilder("making request for ids");
			/***
			 * MultiGetRequestBuilder mrget = new MultiGetRequestBuilder(
			 * ElasticClientHolder.getInstance()); Integer counter = 0; for
			 * (String id : this.ids) {
			 * 
			 * sb.append(":" + id + ",");
			 * 
			 * if (this.shard_names == null) { mrget.add(new
			 * Item("tradegenie_titan", "test_type", id)); } else {
			 * mrget.add(new Item("tradegenie_titan", "test_type", id)
			 * .routing(this.shard_names[counter])); } counter++; }
			 * MultiGetResponse mresp = mrget.execute().actionGet(); for
			 * (MultiGetItemResponse resp : mresp.getResponses()) {
			 * System.out.println(resp.getId()); }
			 **/

			SearchResponse resp = LocalEs
					.getInstance()
					.prepareSearch("tradegenie_titan")
					.setQuery(
							QueryBuilders.filteredQuery(
									QueryBuilders.matchAllQuery(),
									FilterBuilders.idsFilter("test_type").ids(
											this.ids))).execute().actionGet();

			

			for (SearchHit hit : resp.getHits().hits()) {
				sb.append(hit.getId());
			}

			System.out.println(sb.toString());

			/**
			 * List<JsonDocument> foundDocs = Observable .just(this.ids[0],
			 * this.ids[1], this.ids[2], this.ids[3]) .flatMap(new Func1<String,
			 * Observable<JsonDocument>>() {
			 * 
			 * @Override public Observable<JsonDocument> call(String id) {
			 *           return bucket.async().get(id); }
			 *           }).toList().toBlocking().single();
			 **/
			GregorianCalendar end_cal = new GregorianCalendar();
			System.out
					.println("took: "
							+ (end_cal.getTimeInMillis() - start_cal
									.getTimeInMillis()));
			millis.addAndGet(end_cal.getTimeInMillis()
					- start_cal.getTimeInMillis());
			req_counter.addAndGet(1);
		}

	}

	/**********************************************************************************************
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * MAIN
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {

		
		/**
		 * DeleteByQueryResponse dqb = new DeleteByQueryRequestBuilder(
		 * ElasticClientHolder.getInstance())
		 * .setIndices("tradegenie_titan").setTypes("test_type")
		 * .setQuery(QueryBuilders.matchAllQuery()).execute().actionGet(); try
		 * (Jedis jedis = RedisManager.getInstance().getJedis()) {
		 * jedis.flushAll(); } test_bulk_processor(false, null, null);
		 **/
		test_parallel_mgets(false);
		// System.exit(1);
		// TODO Auto-generated method stub

	}

	public static class MyBulkRunnable implements Runnable {

		private final BulkProcessor processor;
		private String id;
		private Integer int_id;
		private String shard_name;
		

		public String getShard_name() {
			return shard_name;
		}

		public void setShard_name(String shard_name) {
			this.shard_name = shard_name;
		}

		private String complex_name;

		public String getComplex_name() {
			return complex_name;
		}

		public void setComplex_name(String complex_name) {
			this.complex_name = complex_name;
		}

		private Integer[] compressed_arr;

		public Integer[] getCompressed_arr() {
			return compressed_arr;
		}

		public void setCompressed_arr(Integer[] compressed_arr) {
			this.compressed_arr = compressed_arr;
		}

		public MyBulkRunnable(BulkProcessor processor, String entity_prefix,
				String indicator_prefix, String subindicator_prefix,
				String shard_name) {
			this.processor = processor;
			this.id = entity_prefix + "-" + indicator_prefix + "-"
					+ subindicator_prefix;
			this.shard_name = shard_name;
		}

		public MyBulkRunnable(BulkProcessor processor, Integer int_id) {
			this.processor = processor;
			this.int_id = int_id;
		}

		public MyBulkRunnable(BulkProcessor processor, String id) {
			this.processor = processor;
			this.id = id;
		}

		public MyBulkRunnable(BulkProcessor processor, String id,
				String shard_name) {
			this.processor = processor;
			this.id = id;
			this.shard_name = shard_name;
		}

		public MyBulkRunnable(BulkProcessor processor, String id,
				String shard_name, Integer[] compressd_buy_arr) {
			this.processor = processor;
			this.id = id;
			this.shard_name = shard_name;
			this.compressed_arr = compressd_buy_arr;
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (this.shard_name == null) {
				/**
				 * JsonArray arr = JsonArray.create(); for (int i = 0; i <
				 * getCompressed_arr().length; i++) {
				 * arr.add(getCompressed_arr()[i]); } JsonObject object =
				 * JsonObject.empty().put("array", arr); JsonDocument doc =
				 * JsonDocument.create(this.id, object);
				 * this.bucket.insert(doc);
				 **/

				this.processor.add(new IndexRequest("tradegenie_titan",
						"test_type", this.id)
						.source(new HashMap<String, Object>() {
							{
								put("arr", getCompressed_arr());
							}
						}));

			} else {

				this.processor.add(
						new IndexRequest("tradegenie_titan", "test_type",
								this.id).source(new HashMap<String, Object>() {
							{
								put("arr", getCompressed_arr());
							}
						}).routing(getShard_name()),
						new HashMap<String, Object>() {
							{
								put("shard_name", getShard_name());
							}
						});
			}

		}

	}

	public static class MyRunnable implements Runnable {
		private final String url;

		MyRunnable(String url) {
			this.url = url;
		}

		@Override
		public void run() {

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			 * SearchResponse resp =
			 * ElasticClientHolder.getInstance().prepareSearch("pairs")
			 * .setTypes
			 * ("pair").setQuery(QueryBuilders.filteredQuery(QueryBuilders
			 * .matchAllQuery(), FilterBuilders.termsFilter("pair_name",
			 * build_random_pair_names())))
			 * .addAggregation(AggregationBuilders.avg("average_profit").
			 * script("average_price_change").param("field", "pc"))
			 * .execute().actionGet();
			 * 
			 * System.out.println("took:" + resp.getTookInMillis()); Avg agg =
			 * resp.getAggregations().get("average_profit"); double value =
			 * agg.getValue(); System.out.println("result:" + value);
			 */

			/**
			 * SearchResponse resp =
			 * ElasticClientHolder.getInstance().prepareSearch("pairs")
			 * .setTypes
			 * ("pair").setQuery(QueryBuilders.filteredQuery(QueryBuilders
			 * .matchAllQuery(), FilterBuilders.termsFilter("pair_name",
			 * build_random_pair_names()))) .addAggregation(AggregationBuilders
			 * .histogram("agg") .script("price_change_histogram")
			 * .interval(50)) .execute().actionGet();
			 * 
			 * Histogram agg = resp.getAggregations().get("agg");
			 * 
			 * // For each entry System.out.println("total buckets:" +
			 * agg.getBuckets().size()); for (Histogram.Bucket entry :
			 * agg.getBuckets()) { String key = entry.getKey(); // Key long
			 * docCount = entry.getDocCount(); // Doc count
			 * 
			 * System.out.println("key:" + key + ", doc_count:" + docCount); }
			 **/

		}
	}

}
