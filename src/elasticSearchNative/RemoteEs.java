package elasticSearchNative;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import yahoo_finance_historical_scraper.StockConstants;

public class RemoteEs {
	
	private static Client instance;

	public static Client return_client() {

		final ImmutableSettings.Builder settings = ImmutableSettings
				.settingsBuilder()
				.put("transport.tcp.connect_timeout","3s")
				.put("es.http.timeout","5s")
				.put("cluster.name","elasticsearch_xeon");
				
		TransportClient transportClient = new TransportClient(settings);
		transportClient = transportClient
				.addTransportAddress(new InetSocketTransportAddress(
						StockConstants.elasticsearch_remote_host_name,
						StockConstants.es_remote_transport_port));
				
		return transportClient;
	}
	
	public static synchronized Client getInstance(){
		
		if(instance == null){
			instance = return_client();
			
		}
	
		return instance;
		
	}
	
}
