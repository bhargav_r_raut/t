package elasticSearchNative;

import yahoo_finance_historical_scraper.StockConstants;

public interface elasticSearchConstants {
	
	public static final String index_name = "tradegenie";
	public static final String STRING_TYPE = "string";
	public static final String INTEGER_TYPE = "integer";
	public static final String BOOLEAN_TYPE = "boolean";
	public static final String LONG_TYPE = "long";
	public static final String DOUBLE_TYPE = "double";
	public static final String FLOAT_TYPE = "float";
	public static final String hostname = StockConstants.elasticsearch_remote_host_name;
	public static final Integer port = 9300;
	public static final Boolean add_to_elasticsearch = false;

}
