package forex;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import Titan.GroovyStatements;

import singletonClass.CalendarFunctions;

public class ForexBean {

	private Calendar date;
	private ArrayList<String> currency = new ArrayList<String>();
	private ArrayList<Double> rate = new ArrayList<Double>();

	public ForexBean() {
		this.currency = currency;
		this.rate = rate;
		this.date = date;
	}

	public ArrayList<String> getCurrency() {
		return currency;
	}

	public void setCurrency(ArrayList<String> currency) {
		this.currency = currency;
	}

	public ArrayList<Double> getRate() {
		return rate;
	}

	public void setRate(ArrayList<Double> rate) {
		this.rate = rate;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		// logic for converting it to an epoch.
		this.date = date;
	}

	public void addRate(Double rate_curr) {
		rate.add(rate_curr);
	}

	public void addCurrency(String currString) {
		currency.add(currString);
	}

	public Calendar getTimeAsCalendar(String time) {
		Calendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
		Date d = new Date();
		try {
			d = sdf.parse(time + " 15:00:00");

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		cal.setTime(d);
		cal.setTimeZone(sdf.getTimeZone());
		return cal;

	}

	@Override
	public String toString() {
		// TODO Auto
		HashMap<String, String> output_map = new HashMap<String, String>();
		output_map.put("currency", getCurrency().toString());
		output_map.put("date", new CalendarFunctions(getDate())
				.getFullDateWithoutTimeZoneOnlyDate());
		output_map.put("value", getRate().toString());
		try {
			return GroovyStatements.getInstance().objmapper
					.writerWithDefaultPrettyPrinter().writeValueAsString(
							output_map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
			
		}

	}

}
