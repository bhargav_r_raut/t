package forex;

import DailyOperationsLogging.LoggingDailyOperations;
import Titan.AddDataPointToGraph;
import Titan.SimpleThreadFactory;
import Titan.ThreadPoolRejectHandler;
import Titan.TitanBaseConfig;
import Titan.TitanConstants;
import Titan.TitanGraphSchema;
import Titan.TitanThreadedExecutor;
import Titan.TitanWorkerThread;

import com.thinkaurelius.titan.core.TitanException;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Vertex;

import dailyDownload.ForexDocumentEmptyException;
import forex.MyXmlParser;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;





import javax.xml.parsers.ParserConfigurationException;
/*
 import javax.vecmath.GMatrix;
 import javax.vecmath.GVector;
 */
import javax.xml.stream.XMLStreamException;

import org.apache.tools.ant.taskdefs.Exit;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import singletonClass.CalendarFunctions;
import singletonClass.MySingleton;
import yahoo_finance_historical_scraper.HistoricalStock;

public class ForexTest  implements TitanConstants,forexConstants{
	
	/**
	 * @param args
	 * @throws MalformedURLException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	
	public static final String ENTITY_TYPE = "forex";
	
	private HashMap<String, HistoricalStock> convertForexBeansToRequiredHashMap(
			ArrayList<ForexBean> forex_beans, ArrayList<String> forex_names) {

		HashMap<String, HistoricalStock> hm = new HashMap<String, HistoricalStock>();
		
		for (ForexBean forexBean : forex_beans) {
			// take each currency out of it and place it in the hashmap
			//System.out.println("currencies at date:" + forexBean.getDate().toString());
			//System.out.println(forexBean.getCurrency().toString());
			int currencyIterator = 0;
			for (String currency : forexBean.getCurrency()) {
				if (hm.get(currency) != null) {
					//System.out.println("this currency already exists.");
					// this currency exists already, so we have to just add to
					// its historical
					// stock object.
					HistoricalStock stock = hm.get(currency);
					//System.out.println("we are adding this currency." + currency);
					stock.addDate(forexBean.getDate());
					//System.out.println("we are now adding---->" + new CalendarFunctions(forexBean.getDate()).getFullDate());
					stock.addClose(forexBean.getRate().get(currencyIterator));
					//System.out.println("this is the rate---->" + forexBean.getRate().get(currencyIterator));

				} else {
					
					int shortformindex = Arrays.asList(shortforms).indexOf("EUR/"+currency);
					String longform = Arrays.asList(fullforms).get(shortformindex);
					
					//System.out.println("this currency does not exist, so we add it to the map " + currency);
					HistoricalStock stock = new HistoricalStock("EUR/"+currency,
							TimeZone.getTimeZone("Europe/Berlin"),ENTITY_TYPE,ENTITY_TYPE,longform);
					
					// now we have to start adding stuff to it.
					stock.addDate(forexBean.getDate());
					//System.out.println("we are now adding---->" + new CalendarFunctions(forexBean.getDate()).getFullDate());
					stock.addClose(forexBean.getRate().get(currencyIterator));
					//System.out.println("this is the rate---->" + forexBean.getRate().get(currencyIterator));
					hm.put(currency, stock);
				}

				currencyIterator++;
			}
		}

		return hm;
	}

	public HashMap<String, HistoricalStock> getForexforDates(
			ArrayList<Calendar> start_dates, ArrayList<Calendar> end_dates,
			ArrayList<String> forex_names) throws Exception {
		System.out.println("came to get forex for dates.");
		boolean parseWholeFile = false;
		// the array list will contain the start dates and end dates that we
		// want.
		// this method will return a hashmap containing, the forex_name, another
		// hashmap
		// which will be a historical stock object which will contain the price
		// over the required
		// dates

		// now the first thing to do is to get the oldest date from those
		// required.
		ArrayList<Long> all_dates = new ArrayList<Long>();
		for (Calendar calendar : start_dates) {
			System.out.println(new CalendarFunctions(calendar).getFullDateWithoutTimeZoneOnlyDate());
			all_dates.add(calendar.getTimeInMillis());
		}
		System.in.read();
		// now we have to get minimum value from this.
		Collections.sort(all_dates);
		long earliest_date = all_dates.get(0);
		//System.out.println("the earliest epoch is " + Long.toString(earliest_date));
		
		// now i see whether this is within 60 days of todays date
		Calendar currentDate = new GregorianCalendar();
		long today_minus_sixty = currentDate.getTimeInMillis();
		//System.out.println("the current epoch is -----> " + Long.toString(currentEpoch));
		
		today_minus_sixty = today_minus_sixty - (86400 * 60 * 1000);
		
		//System.out.println("the current epoch minused is ----> " + Long.toString(currentEpoch));
		
		if (earliest_date < today_minus_sixty) {
			// now we have to take the whole xml file.
			//System.out.println("the earliest date is outside of the 60 day mark.");
			parseWholeFile = true;
		}

		String urlString = "";
		
		/***
		 * CAUTION FOR TEST PURPOSE ONLY.
		 * 
		 * SETTING PARSEWHOLEFILE TO FALSE BY DEFAULT. BECAUSE FILE IS
		 * TOO BIG.
		 * 
		 * 
		 * 
		 * 	
		 */
		parseWholeFile = false;

		if (parseWholeFile) {
			// here we have to get a forexbean holding all the values for each
			// currency,
			// starting from the start date.
			//System.out.println("we have to parse the whole file.");
			LoggingDailyOperations.get_instance().add_to_log("info",
					null,
					"forex_test",
					"decided to parse the whole file.");
			urlString = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.xml";
			//urlString = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";
			
		} else {
			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"forex text",
					"decided to parse only last 90 days file.");
			//System.out.println("we dont need to parse the whole file.");
			urlString = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";

		}

		URL baseUrl = new URL(urlString);

		MyXmlParser parser = new MyXmlParser();
		ArrayList<ForexBean> forex_beans = new ArrayList<ForexBean>();
		HashMap<String, ArrayList<Long>> infoMap = new HashMap<String, ArrayList<Long>>();
		int forex_name_index = 0;
		for (String forexName : forex_names) {

			ArrayList<Long> localbounds = new ArrayList<Long>();
			localbounds
					.add(start_dates.get(forex_name_index).getTimeInMillis());
			localbounds.add(end_dates.get(forex_name_index).getTimeInMillis());
			infoMap.put(forexName, localbounds);

			forex_name_index++;
		}
		
		
		HashMap<String, HistoricalStock> hm = new HashMap<String, HistoricalStock>();
		
			//disabled for test purposes.
			
			try {
				Document document = parser.parseUrl(baseUrl);
				
				 if(document == null){
				    	LoggingDailyOperations.get_instance()
				    	.add_to_log("exception",
				    			new ForexDocumentEmptyException("the forex document was empty"),
				    			"forex_test",
				    			null);
				    	throw new ForexDocumentEmptyException("the forex document is null");
				    
				    }
				    else if(document.asXML().trim().length() == 0){
				    	LoggingDailyOperations.get_instance()
				    	.add_to_log("exception",
				    			new ForexDocumentEmptyException("the forex document was empty"),
				    			"forex_test",
				    			null);
				    	throw new ForexDocumentEmptyException("the forex document is empty");
				    }
				    else{
				    	
				    
					//forex_beans = parser.parse_dom_xml_file(document, earliest_date,
					//		infoMap);
					
				    parser.get_test_xml_file();
				    System.in.read();
				    System.out.println("size of beans is : " + forex_beans.size());
					for(ForexBean bean : forex_beans){
						System.out.print(bean.toString());
					}
					hm = convertForexBeansToRequiredHashMap(forex_beans, forex_names);
					return hm;
				    }
			} catch (IOException | ParserConfigurationException | SAXException e) {
				
				// TODO Auto-generated catch block
				LoggingDailyOperations.get_instance()
		    	.add_to_log("exception",
		    			e,
		    			"forex_test",
		    			null);
				throw e;
			
			}
			//File f = new File("forex_history.xml");
		   
		

	}
	
	public HistoricalQuery generateHistoricalQuery(ArrayList<Object> queryString){
		System.out.println("entered generate historical query.");
		HistoricalQuery forexQuery = new HistoricalQuery();
		int counter = 0;
		for(Object s:queryString){
			if(counter==0){
				forexQuery.getForex_names().add((String)s);
			}
			else if(counter==1){
				forexQuery.getStart_dates().add((Calendar)s);
			}
			else if(counter==2){
				forexQuery.getEnd_dates().add((Calendar)s);
			}
			else if(counter==3){
				forexQuery.getIndice_names().add((String)s);
			}
			else if(counter==4){
				forexQuery.getStockFullName().add((String)s);
			}
			counter++;
			
			if(counter>4){
				counter=0;
			}
			
			
		}
		
		return forexQuery;
	}
	
	public HashMap<String, HistoricalStock> get_forex_values(ArrayList<Object> queryString) throws Exception{
		System.out.println("entered get forex values.");
		HistoricalQuery forexQuery = generateHistoricalQuery(queryString);
		
		HashMap<String, HistoricalStock> hm = 
				getForexforDates(forexQuery.getStart_dates(),
						forexQuery.getEnd_dates(),
						forexQuery.getForex_names());
	
		return hm;
	}
	

	public static void puts(String message) {
		System.out.println(message);
	}
	
	
		
		
		
	
}
