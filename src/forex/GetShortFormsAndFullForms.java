package forex;

import static java.nio.charset.StandardCharsets.UTF_8;
import static jodd.jerry.Jerry.jerry;

import java.io.IOException;
import java.util.ArrayList;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import Titan.ReadWriteTextToFile;

public class GetShortFormsAndFullForms {
	
	public void getforms() throws IOException{
		String pageSource = ReadWriteTextToFile.readFile("forex_table.html",UTF_8);
		Jerry doc = jerry(pageSource);
		final ArrayList<String> currency_shortforms = new ArrayList<String>();
		final ArrayList<String> currency_fullnames = new ArrayList<String>();
		//final Pattern p = Pattern.compile("^[A-Z]+$");
		//final Pattern pf = Pattern.compile("\\s+");
		doc.$("#forex-table tr").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				//System.out.println($this.text());
			//	String trimmed_string = $this.text().trim();
			//	Matcher m = p.matcher(trimmed_string);
				
				
				if(index >0){
					//System.out.println(index);
					//System.out.println(trimmed_string);
					$this.$("td").each(new JerryFunction() {
						
						@Override
						public boolean onNode(Jerry $this_inner, int inner_index) {
							// TODO Auto-generated method stub
							if(inner_index == 0){
								//System.out.println("shortform is ===?>" + $this_inner.text());
								currency_shortforms.add("EUR/" + $this_inner.text().trim());
							}
							else if(inner_index == 1){
								currency_fullnames.add("Euro/" + $this_inner.text().trim());
								//System.out.println("fullform ---->" + $this_inner.text());
							}
							return true;
						}
					});
				}
				
				return true;
			}
		});
		
		String[] currency_shortforms_array =  new String[currency_fullnames.size()];
		currency_shortforms_array = currency_shortforms.toArray(currency_shortforms_array);
		
		String[] currency_fullnames_array = new String[currency_shortforms.size()];
		currency_fullnames_array = currency_fullnames.toArray(currency_fullnames_array);
		
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(String s:currency_shortforms){
			sb.append("\"" + s + "\"" +  ",");
		}
		sb.append("}");
		
		String array = sb.toString();
		System.out.println(array);
	}
	
}
