package forex;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import Titan.TitanConstants;

public class HistoricalQuery implements TitanConstants{
	
	private ArrayList<Calendar> start_dates;
	private ArrayList<Calendar> end_dates;
	private ArrayList<String> forex_names;
	private ArrayList<String> indice_names;
	private ArrayList<String> stockFullName;
	
	public ArrayList<Calendar> getStart_dates() {
		return start_dates;
	}



	public void setStart_dates(ArrayList<Calendar> start_dates) {
		this.start_dates = start_dates;
	}



	public ArrayList<Calendar> getEnd_dates() {
		return end_dates;
	}



	public void setEnd_dates(ArrayList<Calendar> end_dates) {
		this.end_dates = end_dates;
	}



	public ArrayList<String> getForex_names() {
		return forex_names;
	}



	public void setForex_names(ArrayList<String> forex_names) {
		this.forex_names = forex_names;
	}


	public HistoricalQuery(){
		this.start_dates = new ArrayList<Calendar>();
		this.end_dates = new ArrayList<Calendar>();
		this.forex_names = new ArrayList<String>();
		this.setIndice_names(new ArrayList<String>());
		this.setStockFullName(new ArrayList<String>());
	}



	public ArrayList<String> getIndice_names() {
		return indice_names;
	}



	public void setIndice_names(ArrayList<String> indice_names) {
		this.indice_names = indice_names;
	}



	/**
	 * @return the stockFullName
	 */
	public ArrayList<String> getStockFullName() {
		return stockFullName;
	}



	/**
	 * @param stockFullName the stockFullName to set
	 */
	public void setStockFullName(ArrayList<String> stockFullName) {
		this.stockFullName = stockFullName;
	}
	
	
	



	
}
