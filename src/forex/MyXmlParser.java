package forex;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.DOMReader;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import DailyOperationsLogging.LoggingDailyOperations;
import Titan.ReadWriteTextToFile;

public class MyXmlParser {

	/**
	 * @param args
	 */

	static final String CUBE = "Cube";
	static final String TIME = "time";
	static final String CURRENCY = "currency";
	static final String RATE = "rate";

	// static final File xml_file = new File("forex_history.xml");

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static void jodd_based_parser(String url) throws IOException{
		String res = ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		Jerry doc = jerry(res);
		System.out.println(doc.text());
		System.in.read();
		
	}

	public static ArrayList<ForexBean> parse_dom_xml_file(
			org.dom4j.Document document, Long earliestDate,
			HashMap<String, ArrayList<Long>> infoMap) {
		 //System.out.println("dom parser");
		 //System.out.println(infoMap.toString());
		ArrayList<ForexBean> forexbeans = new ArrayList<ForexBean>();
		Element root = document.getRootElement();
		 System.out.println(root.asXML());
		for (Iterator i = root.elementIterator("Cube"); i.hasNext();) {
			Element foo = (Element) i.next();
			// do something
			// check if it has children.
			for (Iterator iter = foo.elementIterator(); iter.hasNext();) {
				Element element = (Element) iter.next();

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

				Calendar currCalBeingParsed = new GregorianCalendar();
				//
				Date new_d = new Date();
				try {
					System.out.println("the time value is:" + element.attributeValue("time"));
					try {
						System.in.read();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					new_d = sdf.parse(element.attributeValue("time"));
					sdf.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
					 System.out.println("date is --->" +
					 sdf.parse(element.attributeValue("time") + " 15:00:00"));
					currCalBeingParsed.setTime(new_d);
					currCalBeingParsed.set(Calendar.HOUR_OF_DAY, 15);
					currCalBeingParsed.set(Calendar.MINUTE, 0);
					currCalBeingParsed.set(Calendar.SECOND, 0);
					currCalBeingParsed.setTimeZone(TimeZone
							.getTimeZone("Europe/Berlin"));

					Long currCalBeingParsedlong = currCalBeingParsed
							.getTimeInMillis();
					// first check if this currcal is less than the minimum date
					// that we need.
					if (currCalBeingParsedlong < earliestDate) {
						 System.out.println("the current cal being parsed is less than our earliest date");
					} else {
						// System.out.println("the attribute value si --->" +
						// element.attributeValue("time"));
						// System.out.println(currCalBeingParsed.toString());
						// System.out.println("the day in our calendar object is ---->"
						// + new
						// CalendarFunctions(currCalBeingParsed).getFullDate());
						// System.out.println("the current cal is equal or after our earliest so we parse it.");
						ForexBean bean = new ForexBean();
						bean.setDate(bean.getTimeAsCalendar(element
								.attributeValue("time")));
						// System.out.println("the time on this bean is " +
						// new CalendarFunctions(bean.getDate()).getFullDate()
						// );
						for (Iterator c = element.elementIterator(); c
								.hasNext();) {
							Element innerElement = (Element) c.next();
							// System.out.println("hasnext");
							// get the currency from the hashmap and see if its
							// startdate is
							// ahead of the current date being parsed.
							// also see if its end date is not before the
							// currentdate being parsed.
							// System.out.println("the current currency is ---->"
							// + innerElement.attributeValue("currency"));

							ArrayList<Long> start_end_time = infoMap.get("EUR/"
									+ innerElement.attributeValue("currency"));

							if (start_end_time == null) {
								// System.out
								// .println("its null for this currency.");
							} else {

								// System.out.println("the start end time for this currency is "
								// + start_end_time.toString());

								if (currCalBeingParsedlong < start_end_time
										.get(0)
										|| currCalBeingParsedlong > start_end_time
												.get(1)) {
									// System.out
									// .println("either too early or too late");
									// the current date is before the start date
									// for
									// this stock
									// OR
									// the current date is after the end date
									// for
									// this stock, so
									// we dont do anything.

								} else {
									// System.out.println("added to bean.");
									bean.addCurrency(innerElement
											.attributeValue("currency"));
									bean.addRate(Double
											.parseDouble(innerElement
													.attributeValue("rate")));
								}
							}
						}

						forexbeans.add(bean);

					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					LoggingDailyOperations.get_instance().add_to_log(
							"exception", e, "my_xml_parse", null);
				}
			}

		}

		return forexbeans;
	}

	public org.dom4j.Document parse(File file) throws DocumentException {
		SAXReader reader = new SAXReader();
		org.dom4j.Document document = reader.read(file);
		return document;
	}

	public InputStream get_test_xml_file() {
		String xml_string;
		try {
			xml_string = ReadWriteTextToFile.readFile("forex_history.xml",
					Titan.TitanConstants.ENCODING);
			InputStream ins = new ByteArrayInputStream(xml_string.getBytes());
			return ins;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public org.dom4j.Document parseUrl(URL url) throws DocumentException,
			IOException, ParserConfigurationException, SAXException {
		
		//HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//connection.setRequestMethod("GET");
		//connection.setRequestProperty("Accept", "application/xhtml+xml");
		//InputStream xml = connection.getInputStream();
		
		/***
		 * uncomment all lines above this line, 
		 * and comment out the below line, to come out of test mode.
		 * 
		 */
		InputStream xml = get_test_xml_file();
		/**
		 * end test line.
		 * 
		 * 
		 */
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document w3cDoc = db.parse(xml);
		org.dom4j.io.DOMReader dom4j_reader = new DOMReader();
		org.dom4j.Document document = dom4j_reader.read(w3cDoc);
		
		return document;
	}
}