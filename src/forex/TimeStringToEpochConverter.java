package forex;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeStringToEpochConverter {
	
	
	
	
	public static Date getSomeDate(final String str, final TimeZone tz)
		    throws ParseException {
		  final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  sdf.setTimeZone(tz);
		  return sdf.parse(str);
		}
	
	public long dateToEpoch(String date_str, String tz) throws ParseException
	{
		long epoch = 0;
		int final_epoch = 0;
		
		Date date = getSomeDate(date_str,TimeZone.getTimeZone(tz));
		//System.out.println(date.toGMTString());
		epoch = date.getTime();
		epoch = epoch/1000;
	
		
		return epoch;
	}
}
