package forex;

public interface forexConstants {
String [] shortforms = {
		"EUR/ZAR","EUR/USD","EUR/TRY","EUR/THB","EUR/SGD","EUR/SEK","EUR/RUB","EUR/RON","EUR/PLN",
		"EUR/PHP","EUR/NZD","EUR/NOK","EUR/MYR","EUR/MXN","EUR/LTL","EUR/KRW","EUR/JPY","EUR/ISK",
		"EUR/INR","EUR/ILS","EUR/IDR","EUR/HUF","EUR/HRK","EUR/HKD","EUR/GBP","EUR/DKK",
		"EUR/CZK","EUR/CNY","EUR/CHF","EUR/CAD","EUR/BRL","EUR/BGN","EUR/AUD"};

String [] fullforms = {"Euro/South African rand",
		"Euro/US dollar",
		"Euro/Turkish lira","Euro/Thai baht",
		"Euro/Singapore dollar",
		"Euro/Swedish krona","Euro/Russian rouble",
		"Euro/New Romanian leu 1","Euro/Polish zloty","Euro/Philippine peso",
		"Euro/New Zealand dollar",
		"Euro/Norwegian krone","Euro/Malaysian ringgit",
		"Euro/Mexican peso","Euro/Lithuanian litas",
		"Euro/South Korean won","Euro/Japanese yen",
		"Euro/Icelandic krona",
		"Euro/Indian rupee",
		"Euro/Israeli shekel","Euro/Indonesian rupiah",
		"Euro/Hungarian forint","Euro/Croatian kuna","Euro/Hong Kong dollar",
		"Euro/Pound sterling","Euro/Danish krone","Euro/Czech koruna","Euro/Chinese yuan renminbi",
		"Euro/Swiss franc","Euro/Canadian dollar","Euro/Brasilian real",
		"Euro/Bulgarian lev","Euro/Australian dollar"};


}
