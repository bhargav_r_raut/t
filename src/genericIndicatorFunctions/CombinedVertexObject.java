package genericIndicatorFunctions;

import Titan.MultiThreadable;

public class CombinedVertexObject extends MultiThreadable{

	private String combined_vertex_name;
	private String stockName;
	private String stockFullName;
	private String indice;
	private Long indicator_vertex_id;
	private Long subindicator_vertex_id;
	private String es_object_id;
	
	public String getEs_object_id() {
		return es_object_id;
	}
	public void setEs_object_id(String es_object_id) {
		this.es_object_id = es_object_id;
	}
	public String getCombined_vertex_name() {
		return combined_vertex_name;
	}
	public void setCombined_vertex_name(String combined_vertex_name) {
		this.combined_vertex_name = combined_vertex_name;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getStockFullName() {
		return stockFullName;
	}
	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}
	public String getIndice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public Long getIndicator_vertex_id() {
		return indicator_vertex_id;
	}
	public void setIndicator_vertex_id(Long indicator_vertex_id) {
		this.indicator_vertex_id = indicator_vertex_id;
	}
	public Long getSubindicator_vertex_id() {
		return subindicator_vertex_id;
	}
	public void setSubindicator_vertex_id(Long subindicator_vertex_id) {
		this.subindicator_vertex_id = subindicator_vertex_id;
	}
	
	
	/**
	 * @param combined_vertex_name
	 * @param stockName
	 * @param stockFullName
	 * @param indice
	 * @param indicator_vertex_id
	 * @param subindicator_vertex_id
	 */
	public CombinedVertexObject(String combined_vertex_name, String stockName,
			String stockFullName, String indice, Long indicator_vertex_id,
			Long subindicator_vertex_id) {
		this.combined_vertex_name = combined_vertex_name;
		this.stockName = stockName;
		this.stockFullName = stockFullName;
		this.indice = indice;
		this.indicator_vertex_id = indicator_vertex_id;
		this.subindicator_vertex_id = subindicator_vertex_id;
	}
	
	
	

}
