package genericIndicatorFunctions;

public class CouldNotAddCombinedIndicatorSubIndicatorStockPair extends
		Exception {

	public CouldNotAddCombinedIndicatorSubIndicatorStockPair(String message){
		super(message);
	}

}
