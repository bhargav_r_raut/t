package genericIndicatorFunctions;

public class CouldNotFindSubIndicatorVertexWhileTryingToMakeCombinedPair extends Exception{

	public CouldNotFindSubIndicatorVertexWhileTryingToMakeCombinedPair(String message){
		super(message);
	}

}
