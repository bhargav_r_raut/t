package genericIndicatorFunctions;

import static org.elasticsearch.index.query.FilterBuilders.termFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import DailyOperationsLogging.IndicatorOperations;
import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.SubIndicatorOperations;
import ExchangeBuilder.Entity;
import Indicators.RedisManager;
import Titan.GroovyStatements;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import Titan.VertexMapDoesNotHaveUniqueIdentifier;

import com.google.gson.Gson;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import elasticSearchNative.LocalEs;

public class CreateDailyIndicatorsJsonString {

	private Gson gson;
	private static final String identifier_for_log = "Create_Daily_JSON_String";

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	private LinkedHashMap<String, SubIndicator> assembled_subindicators_LinkedHashMap;

	public LinkedHashMap<String, SubIndicator> getAssembled_subindicators_LinkedHashMap() {
		return assembled_subindicators_LinkedHashMap;
	}

	public void setAssembled_subindicators_LinkedHashMap(
			LinkedHashMap<String, SubIndicator> assembled_subindicators_LinkedHashMap) {
		this.assembled_subindicators_LinkedHashMap = assembled_subindicators_LinkedHashMap;
	}

	/**
	 * first we read the jsonfile into a jsonobject. we do it for both the
	 * indicators and the subindicators. so the next two variables hold those
	 * jsonobjects.
	 * 
	 * 
	 */
	private JSONObject jsonobject_textfile_subindicators;
	private JSONObject jsonobject_textfile_indicators;

	/**
	 * we parse the jsonfile into a LinkedHashMap of subindicators.
	 * 
	 * 
	 */
	private LinkedHashMap<String, SubIndicator> subindicators_LinkedHashMap_after_parsing_jsonString;

	/**
	 * indicators LinkedHashMap, which contains each indicator and all its
	 * attributes, the subindicators prepared in the above variable are added to
	 * each indicator whereever they are applicable. this is the final end
	 * product of this program.
	 * 
	 */
	private LinkedHashMap<String, Indicator> indicators_LinkedHashMap_after_parsing_jsonString;

	/**
	 * 
	 * 
	 * 
	 * 
	 * @return
	 */
	public JSONObject getJsonobject_textfile_subindicators() {
		return jsonobject_textfile_subindicators;
	}

	public void setJsonobject_textfile_subindicators(
			JSONObject jsonobject_textfile_subindicators) {
		this.jsonobject_textfile_subindicators = jsonobject_textfile_subindicators;
	}

	public JSONObject getJsonobject_textfile_indicators() {
		return jsonobject_textfile_indicators;
	}

	public void setJsonobject_textfile_indicators(
			JSONObject jsonobject_textfile_indicators) {
		this.jsonobject_textfile_indicators = jsonobject_textfile_indicators;
	}

	private Boolean update_subindicators;
	private Boolean update_indicators;

	private ArrayList<EsIndicator> newly_added_indicators;

	/*
	 * public static void main(String[] args) throws
	 * ErrorParsingIndicatorsJsonString, IndicatorHasNoDefinition,
	 * SpellingErrorWhileLookingForThisSubIndicator,
	 * PeriodOrOhlcvGroupIsMissing, IndicatorsJsonStringIsFaulty { try {
	 * CreateDailyIndicatorsJsonString crj = new
	 * CreateDailyIndicatorsJsonString(); } catch (JSONException |
	 * VertexMapDoesNotHaveUniqueIdentifier | IOException | ParseException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } }
	 */

	public ArrayList<EsIndicator> getNewly_added_indicators() {
		return newly_added_indicators;
	}

	public void setNewly_added_indicators(
			ArrayList<EsIndicator> newly_added_indicators) {
		this.newly_added_indicators = newly_added_indicators;
	}

	/**
	 * structure of this json object is as follows:
	 * 
	 * key => value
	 * 
	 * 
	 * names_generic_indicators: {simple_names_of_indicators ->
	 * aroon_up=>id,aroon_down=>id....}
	 * 
	 * names_generic_and_sub:{aroon_up_rising=>{id,pair_id},aroon_down_falling=>
	 * {id,pair_id},....}
	 * 
	 * names_sub_indicators: {full list name=>id}
	 * 
	 * generic_indicator_descriptions:{ example_indicator_id :{
	 * indicator_name:name specific_subindicator_names:[] ohlcv group:
	 * boolean_generic_subindicators_applicable:
	 * names_of_specific_sub_indicators_not_applicable:[]
	 * ids_of_opposite_self_indicators:[]
	 * group_ids_of_opposite_self_indicators:[]
	 * ids_of_oppostie_non_self_indicators:[]
	 * group_ids_of_opposite_non_self_indicators:[] self_group_id }}
	 * 
	 * generic_subindicators_description{ example_Subindicator_id:[
	 * subindicator_name: subindicator_opposites_ids:[] subindicator_group_id:
	 * subindicator_opposite_group_ids:[] ] }
	 * 
	 * @throws JSONException
	 * @throws VertexMapDoesNotHaveUniqueIdentifier
	 * @throws IOException
	 * @throws ParseException
	 * @throws ErrorParsingIndicatorsJsonString
	 * @throws IndicatorHasNoDefinition
	 * @throws SpellingErrorWhileLookingForThisSubIndicator
	 * @throws PeriodOrOhlcvGroupIsMissing
	 * @throws IndicatorsJsonStringIsFaulty
	 * @throws InterruptedException
	 * @throws FailedToCommitMd5HashToDb
	 * @throws SubIndicatorsJsonStringIsFaulty
	 * @throws DatabaseSubIndicatorsFileNewerThanServerFile
	 * 
	 * 
	 */

	public CreateDailyIndicatorsJsonString(TitanGraph g, Gson gson,
			String indice_name, Entity optional_new_entity) {

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						identifier_for_log,
						"entered parsing of subindicators_and_indicators_json_string, to add them to the graph");
		this.newly_added_indicators = new ArrayList<EsIndicator>();
		this.gson = gson;

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, identifier_for_log,
						"now deleting the indicator names from redis, since new ones may be added now.");
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			Long del_result = jedis.del("indicator_names");
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}

		/**
		 * 
		 * FIRST SUBINDICATORS.
		 * 
		 */
		try {
			String subindicators_jsonString = new ReadWriteTextToFile()
					.getInstance().readFile(
							StockConstants.subindicators_jsontextfile,
							TitanConstants.ENCODING);

			if (!subindicators_jsonString.isEmpty()) {
				try {
					LoggingDailyOperations
							.get_instance()
							.add_to_log("info", null, identifier_for_log,
									"starting to do initial parsing of the subindicators json file.");
					setJsonobject_textfile_subindicators(new JSONObject(
							subindicators_jsonString));
					subindicators_LinkedHashMap_after_parsing_jsonString = new LinkedHashMap<String, SubIndicator>();
					inspect_json_subindicators_object_of_textfile();
					LoggingDailyOperations
							.get_instance()
							.add_to_log("info", null, identifier_for_log,
									"finished creating the subindicators hashmap after parsing the jsonstring.");

					/***
					 * this hashmap holds the subindicator, for quick referral
					 * when required otherwise.
					 * 
					 */
					setAssembled_subindicators_LinkedHashMap(new LinkedHashMap<String, SubIndicator>());

					/**
					 * NOW INDICATORS.
					 * 
					 * 
					 */
					String indicators_jsonString = new ReadWriteTextToFile()
							.getInstance().readFile(
									StockConstants.indicators_jsontextfile,
									TitanConstants.ENCODING);
					if (!indicators_jsonString.isEmpty()) {
						try {
							LoggingDailyOperations
									.get_instance()
									.add_to_log("info", null,
											identifier_for_log,
											"now starting to do the initial parsing of the indicators jsontextfile.");
							setJsonobject_textfile_indicators(new JSONObject(
									indicators_jsonString));
							indicators_LinkedHashMap_after_parsing_jsonString = new LinkedHashMap<String, Indicator>();
							inspect_json_indicators_object_of_textfile();
							LoggingDailyOperations
									.get_instance()
									.add_to_log(
											"info",
											null,
											identifier_for_log,
											"finished the initial parsing of the indicators jsonfile,"
													+ " and created the indicators linkedhashmap");
						} catch (JSONException je) {
							je.printStackTrace();
							String exception = je.getLocalizedMessage();
							if (exception.contains("Expected")
									&& exception.contains("character")) {
								String problem_character = exception
										.replaceAll("[^\\d]", "");
								Integer problem_char_no = Integer
										.parseInt(problem_character);
								System.out
										.println("this is the problem char no"
												+ problem_char_no);
								LoggingDailyOperations.get_instance()
										.add_to_log("exception", je,
												identifier_for_log, null);
								throw new IndicatorsJsonStringIsFaulty(
										indicators_jsonString.substring(
												problem_char_no - 200,
												problem_char_no)
												+ "this is the error message"
												+ exception);

							} else {
								LoggingDailyOperations.get_instance()
										.add_to_log("exception", je,
												identifier_for_log, null);
								throw new IndicatorsJsonStringIsFaulty(
										exception);
							}

						}
					} else {
						LoggingDailyOperations
								.get_instance()
								.add_to_log(
										"exception",
										new Exception(
												"the indicators json string text file is empty"),
										identifier_for_log, null);
					}

				} catch (JSONException e) {
					LoggingDailyOperations.get_instance().add_to_log(
							"exception", e, identifier_for_log, null);
					throw new SubIndicatorsJsonStringIsFaulty(
							e.getLocalizedMessage());

				}
			} else {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								new Exception(
										"the subindicators_json string_textfile was empty"),
								identifier_for_log, null);
			}
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
		}

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				"now printing the subindicator linked hashmap "
						+ "  that were got by parsing the text files.");

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						identifier_for_log,
						"total subindicators got:"
								+ String.valueOf(subindicators_LinkedHashMap_after_parsing_jsonString
										.entrySet().size()));

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				subindicators_LinkedHashMap_after_parsing_jsonString.keySet()
						.toString());

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				"now printing the indicator linked hashmap "
						+ "  that were got by parsing the text files.");

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						identifier_for_log,
						"total subindicators got:"
								+ String.valueOf(indicators_LinkedHashMap_after_parsing_jsonString
										.entrySet().size()));

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				indicators_LinkedHashMap_after_parsing_jsonString.keySet()
						.toString());

		ArrayList<SubIndicator> changed_subindicators = add_subindicators_to_graph(g);

		ArrayList<Indicator> changed_indicators = add_indicators_to_graph(g);

		/**
		 * 
		 * get rid of everything from the db.
		 * 
		 * 
		 */
		// try(Jedis jedis = RedisManager.getInstance().getJedis()){
		// jedis.flushDB();
		// }

		// ArrayList<Indicator> demo = new ArrayList<Indicator>(
		// indicators_LinkedHashMap_after_parsing_jsonString.values());
		// List<Indicator> demo_slice = demo.subList(0, 4);
		// ArrayList<Indicator> final_demo = new ArrayList<Indicator>();
		// for (Indicator i : demo_slice) {
		// final_demo.add(i);
		// }

		// CreateIndicatorSubIndicatorPairsForEachStock crp_stocks = new
		// CreateIndicatorSubIndicatorPairsForEachStock(
		// g, final_demo, new ArrayList<SubIndicator>(
		// subindicators_LinkedHashMap_after_parsing_jsonString
		// .values()));

		// CreateIndicatorSubIndicatorPairsForEachStock crp_stocks =
		// new CreateIndicatorSubIndicatorPairsForEachStock(
		// g, changed_indicators, changed_subindicators);

		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log, "starting to create the pairs");
		CreateIndicatorSubIndicatorPairsForEachStock crp_stocks = new CreateIndicatorSubIndicatorPairsForEachStock(
				getNewly_added_indicators(),indice_name, optional_new_entity);

		// g.commit();

	}

	public void remove_non_existent_subindicators_from_graph(TitanGraph g,
			LinkedHashMap<String, SubIndicator> subindicators_LinkedHashMap) {
		System.out
				.println("now removing non existent subindicators from graph.");
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"entered to remove disabled subindicators from graph.");
		// collect all the subindicators in teh graph.l
		GroovyStatements.getInstance()
				.remove_all_subindicators_except_these_from_graph(
						subindicators_LinkedHashMap.keySet());
		if (LoggingDailyOperations.has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log("info", null,
					identifier_for_log,
					"exited at end of function:" + new Object() {
					}.getClass().getEnclosingMethod().getName());
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}
	}

	public void remove_non_existent_indicators_from_graph(TitanGraph g,
			LinkedHashMap<String, Indicator> indicators_LinkedHashMap) {
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"entered to remove disabled indidcators from graph.");
		System.out.println("now removing non existent indicators from graph.");
		GroovyStatements.getInstance()
				.remove_all_indicators_except_these_from_graph(
						indicators_LinkedHashMap.keySet());
		if (LoggingDailyOperations.has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"exited with exception,check logs, at end of function:"
							+ new Object() {
							}.getClass().getEnclosingMethod().getName());
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}
	}

	public ArrayList<SubIndicator> add_subindicators_to_graph(TitanGraph g) {

		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log, "entered to add subindicators to graph");

		ArrayList<SubIndicator> failed_subindicators = new ArrayList<SubIndicator>();
		ArrayList<SubIndicator> successfull_subindicators = new ArrayList<SubIndicator>();

		LinkedHashMap<String, Long> subindicator_vertex_ids = new LinkedHashMap<String, Long>();

		// failed to add groups of which subindicator
		ArrayList<SubIndicator> failed_to_add_groups_of = new ArrayList<SubIndicator>();
		ArrayList<SubIndicator> failed_to_add_subgroups_of = new ArrayList<SubIndicator>();
		ArrayList<SubIndicator> failed_to_add_opposite_groups_of = new ArrayList<SubIndicator>();
		ArrayList<SubIndicator> failed_to_add_opposite_subgroups_of = new ArrayList<SubIndicator>();
		ArrayList<SubIndicator> failed_to_add_opposite_specific_groups_of = new ArrayList<SubIndicator>();

		// take all the subindicators in the LinkedHashMap.
		// get all the subindicators from the graph, which are not there int.
		// remove all edges going in and out from them.
		// remove them.
		// same for indicators.
		// do this all first.

		for (Map.Entry<String, SubIndicator> entry : this.subindicators_LinkedHashMap_after_parsing_jsonString
				.entrySet()) {

			SubIndicator subindicator = entry.getValue();
			LoggingDailyOperations
					.get_instance()
					.getSubindicator_operations()
					.put(subindicator.getSubIndicator_name(),
							new SubIndicatorOperations(subindicator));

			System.out.println("now doing subindicator:"
					+ subindicator.getSubIndicator_name());
			subindicator.generate_es_subindicator(failed_subindicators,
					failed_to_add_groups_of, failed_to_add_subgroups_of,
					failed_to_add_opposite_groups_of,
					failed_to_add_opposite_subgroups_of,
					failed_to_add_opposite_specific_groups_of);
			failed_to_add_groups_of = subindicator.getFailed_to_add_groups();
			failed_to_add_opposite_groups_of = subindicator
					.getFailed_to_add_opposite_groups();
			failed_to_add_opposite_specific_groups_of = subindicator
					.getFailed_to_add_opposite_specific_subindicators();
			failed_to_add_opposite_subgroups_of = subindicator
					.getFailed_to_add_opposite_subgroups();
			failed_to_add_subgroups_of = subindicator
					.getFailed_to_add_subgroups();
			// System.out.println(subindicator.getOpposite_groups().toString());

			try {
				/**
				 * for trial purpose only.
				 * 
				 * 
				 */
				LoggingDailyOperations
						.get_instance()
						.getSubindicator_operations()
						.putIfAbsent(subindicator.getSubIndicator_name(),
								new SubIndicatorOperations(subindicator));

				EsSubIndicator existing_subindicator_in_database = search_if_subindicator_exists(
						g, subindicator.getSubIndicator_name());

				

				if (subindicator
						.getEs_subindicator()
						.get_json_representation()
						.equals(existing_subindicator_in_database
								.get_json_representation())) {
					System.out
							.println(subindicator.getSubIndicator_name()
									+ "subindicator is the same and has not changed-- same");
					// System.in.read();
					LoggingDailyOperations.get_instance().add_to_log(
							"info",
							null,
							identifier_for_log,
							"this subindicator already exists unchanged:"
									+ subindicator.getSubIndicator_name());
					LoggingDailyOperations.get_instance()
							.getSubindicator_operations()
							.get(subindicator.getSubIndicator_name())
							.setAlready_present_in_graph_and_same(true);

				} else {
					System.out
							.println("the subindicator is different or does not exist.");
					// System.in.read();
					LoggingDailyOperations.get_instance()
							.getSubindicator_operations()
							.get(subindicator.getSubIndicator_name())
							.setdifferent(true);
					LoggingDailyOperations
							.get_instance()
							.add_to_log(
									"info",
									null,
									identifier_for_log,
									"this "
											+ "subindicator was either different than before or not present before."
											+ subindicator
													.getSubIndicator_name());
					Long subindicator_vertex_id = null;
					// Long subindicator_vertex_id = subindicator
					// .add_subindicator_vertex_to_database(g, gson);
					if (subindicator.index_subindicator_to_es()) {
						LoggingDailyOperations.get_instance().add_to_log(
								"info",
								null,
								identifier_for_log,
								"successfully added subindicator vertex to graph:"
										+ subindicator.getSubIndicator_name());
						LoggingDailyOperations.get_instance()
								.getSubindicator_operations()
								.get(subindicator.getSubIndicator_name())
								.setAdded_vertex_to_graph(true);
						subindicator_vertex_ids.put(
								subindicator.getSubIndicator_name(),
								subindicator_vertex_id);
						successfull_subindicators.add(subindicator);
					}

					// System.out
					// /
					// .println("added this subindicator vertex with name to graph");
					// System.out.println(subindicator.getSubIndicator_name());
				}

				assembled_subindicators_LinkedHashMap.put(
						subindicator.getSubIndicator_name(), subindicator);

			} catch (Exception e) {
				e.printStackTrace();
				failed_subindicators.add(subindicator);
				LoggingDailyOperations.get_instance().add_to_log(
						"exception",
						new Exception("failed to add subindicator:"
								+ subindicator.getSubIndicator_name()
								+ " to graph"), identifier_for_log, null);
				LoggingDailyOperations.get_instance()
						.getSubindicator_operations()
						.get(subindicator.getSubIndicator_name())
						.setFailed_in_any_respect(true);
				LoggingDailyOperations
						.get_instance()
						.getSubindicator_operations()
						.get(subindicator.getSubIndicator_name())
						.addFailed_to_add_what(
								"failed with exception in add subindicators to graph.");
			}

		}
		/*
		 * for (SubIndicator subindicator : successfull_subindicators) {
		 * 
		 * try { LoggingDailyOperations.get_instance().add_to_log( "info", null,
		 * identifier_for_log, "starting to add parameters for subindicator:" +
		 * subindicator.getSubIndicator_name());
		 * subindicator.add_subindicator_params_to_database(g, gson,
		 * subindicator_vertex_ids, successfull_subindicators,
		 * failed_subindicators, failed_to_add_groups_of,
		 * failed_to_add_subgroups_of, failed_to_add_opposite_groups_of,
		 * failed_to_add_opposite_subgroups_of,
		 * failed_to_add_opposite_specific_groups_of);
		 * subindicator.index_subindicator_to_es(); // get the failed to add
		 * things from the subindicator // methods themselves.
		 * failed_to_add_groups_of = subindicator .getFailed_to_add_groups();
		 * failed_to_add_opposite_groups_of = subindicator
		 * .getFailed_to_add_opposite_groups();
		 * failed_to_add_opposite_specific_groups_of = subindicator
		 * .getFailed_to_add_opposite_specific_subindicators();
		 * failed_to_add_opposite_subgroups_of = subindicator
		 * .getFailed_to_add_opposite_subgroups(); failed_to_add_subgroups_of =
		 * subindicator .getFailed_to_add_subgroups(); } catch (Exception e) {
		 * // TODO Auto-generated catch block
		 * LoggingDailyOperations.get_instance().add_to_log("exception", e,
		 * identifier_for_log, null); LoggingDailyOperations.get_instance()
		 * .getSubindicator_operations()
		 * .get(subindicator.getSubIndicator_name())
		 * .setFailed_in_any_respect(true); LoggingDailyOperations
		 * .get_instance() .getSubindicator_operations()
		 * .get(subindicator.getSubIndicator_name()) .addFailed_to_add_what(
		 * "failed while" +
		 * "trying to add paramters of subindicator after adding its basic vertex to graph "
		 * ); e.printStackTrace(); } // } // else{ //
		 * System.out.println("the name is---> " + //
		 * subindicator.getSubIndicator_name());
		 * 
		 * // } }
		 */

		for (SubIndicator s : failed_to_add_groups_of) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.addFailed_to_add_what("failed to add groups");
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception("failed to add groups of subindicator:"
							+ s.getSubIndicator_name()), identifier_for_log,
					null);
		}

		for (SubIndicator s : failed_to_add_subgroups_of) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.addFailed_to_add_what("failed to add subgroups of");
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception("failed to add subgroups of subindicator:"
							+ s.getSubIndicator_name()), identifier_for_log,
					null);
		}

		for (SubIndicator s : failed_to_add_opposite_groups_of) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.addFailed_to_add_what("failed to add opposite groups");
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception(
							"failed to add opposite groups of subindicator:"
									+ s.getSubIndicator_name()),
					identifier_for_log, null);
		}

		for (SubIndicator s : failed_to_add_opposite_subgroups_of) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.setFailed_in_any_respect(true);
			LoggingDailyOperations
					.get_instance()
					.getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.addFailed_to_add_what(
							"failed to add opposite subgroups of");
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception(
							"failed to opposite  subgroups of subindicator:"
									+ s.getSubIndicator_name()),
					identifier_for_log, null);
		}

		for (SubIndicator s : failed_to_add_opposite_specific_groups_of) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.setFailed_in_any_respect(true);
			LoggingDailyOperations
					.get_instance()
					.getSubindicator_operations()
					.get(s.getSubIndicator_name())
					.addFailed_to_add_what(
							"failed to add opposite specific groups of");
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception(
							"failed to add opposite specific groups of subindicator:"
									+ s.getSubIndicator_name()),
					identifier_for_log, null);
		}

		if (LoggingDailyOperations.has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"exited with exception,check logs, at end of function:"
							+ new Object() {
							}.getClass().getEnclosingMethod().getName());

			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}

		for (SubIndicator subindicator : successfull_subindicators) {
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(subindicator.getSubIndicator_name())
					.setAdded_parameters_to_graph(true);
		}

		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, identifier_for_log,
						"successfully added all subindicator vertices and params to graph.");

		return successfull_subindicators;

	}

	public ArrayList<Indicator> add_indicators_to_graph(TitanGraph g) {

		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"now starting to add the indicators to the graph.");
		System.out.println("now adding indicators to the database.");

		ArrayList<Indicator> failed_indicators = new ArrayList<Indicator>();
		ArrayList<Indicator> successfull_indicators = new ArrayList<Indicator>();

		LinkedHashMap<String, Long> indicator_vertex_ids = new LinkedHashMap<String, Long>();

		ArrayList<Indicator> failed_cross_with_edges = new ArrayList<Indicator>();
		ArrayList<Indicator> failed_monitor_with_edges = new ArrayList<Indicator>();

		/*
		 * for(Map.Entry<String, Indicator> entry :
		 * indicators_LinkedHashMap_after_parsing_jsonString.entrySet()){
		 * System.out.println(entry.getValue().toString()); }
		 */

		for (Map.Entry<String, Indicator> entry : indicators_LinkedHashMap_after_parsing_jsonString
				.entrySet()) {

			Indicator indicator = entry.getValue();

			indicator.build_es_indicator();

			// System.out.println("built indicator:");
			// System.out.println(indicator.getEs_indicator().getIndicator_string_representation());
			// try {
			// System.in.read();
			// } catch (IOException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			// }

			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.putIfAbsent(indicator.getIndicator_name(),
							new IndicatorOperations(indicator));
			// System.out.println(indicator.getPeriods());

			String indicator_from_es = get_indicator_from_es(indicator
					.getIndicator_name());
			/*
			 * System.out.println("this is the indicator from es.");
			 * System.out.println(indicator_from_es); try { System.in.read(); }
			 * catch (IOException e2) { // TODO Auto-generated catch block
			 * e2.printStackTrace(); }
			 * System.out.println("this is what we have");
			 * System.out.println(indicator
			 * .getEs_indicator().getIndicator_string_representation()); try {
			 * System.in.read(); } catch (IOException e1) { // TODO
			 * Auto-generated catch block // e1.printStackTrace(); }
			 */

			// Indicator existing_indicator_in_database =
			// search_database_for_indicator(
			// g, indicator.getIndicator_name());
			// System.out.println("after searching for existing here are the periods.");
			// System.out.println(indicator.getPeriods());
			// System.out.println("json indicator is-->" +
			// indicator.toString());
			// System.out.println("databae indicator is --->" +
			// existing_indicator_in_database.toString());
			// System.out.println(existing_indicator_in_database.compare_to_existing_indicator(indicator));

			// if (indicator.toString().equals(
			// existing_indicator_in_database.toString())) {
			if (indicator_from_es.equals(indicator.getEs_indicator()
					.getIndicator_string_representation())) {
				LoggingDailyOperations.get_instance().getIndicator_operations()
						.get(indicator.getIndicator_name())
						.setAlready_present_in_graph_and_same(true);
				LoggingDailyOperations.get_instance().add_to_log(
						"info",
						null,
						identifier_for_log,
						indicator.getIndicator_name()
								+ " already exists in graph unchanged");

				/*****
				 * CAUTION ! CAUTIONS ! CAUTION ! caution remove this after a
				 * while.
				 * 
				 */

				successfull_indicators.add(indicator);

			} else {
				System.out
						.println("indicator is not same as what we have in graph, or does not "
								+ "exist in graph.");
				LoggingDailyOperations.get_instance().getIndicator_operations()
						.get(indicator.getIndicator_name())
						.setdifferent_because_either_absent_or_changed(true);
				try {

					// Long indicator_vertex_id = indicator
					// .add_indicator_to_database(g, gson);

					// indicator_vertex_ids.put(indicator.getIndicator_name(),
					// indicator_vertex_id);
					if (indicator.index_es_indicator()) {
						successfull_indicators.add(indicator);
					} else {
						LoggingDailyOperations.get_instance().add_to_log(
								"exception",
								new Exception("failed to add an indicator"),
								identifier_for_log, null);
						failed_indicators.add(indicator);
					}
				} catch (Exception e) {
					e.printStackTrace();
					LoggingDailyOperations.get_instance().add_to_log(
							"exception", e, identifier_for_log, null);
					LoggingDailyOperations.get_instance()
							.getIndicator_operations()
							.get(indicator.getIndicator_name())
							.setFailed_in_any_respect(true);
					LoggingDailyOperations
							.get_instance()
							.getIndicator_operations()
							.get(indicator.getIndicator_name())
							.addFailed_to_add_what(
									"Abnormal exception : failed with an exception "
											+ "while adding it to the graph or while"
											+ " drawing the edges to the applicable subindicators:"
											+ e.getLocalizedMessage());
					failed_indicators.add(indicator);
				}
			}

		}

		if (LoggingDailyOperations.has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"exited with exception,check logs, at end of function:"
							+ new Object() {
							}.getClass().getEnclosingMethod().getName());
			LoggingDailyOperations.get_instance()
					.exit_without_graph_and_log_before_exiting();
		}

		/*
		 * for (Indicator indicatora : successfull_indicators) {
		 * 
		 * LoggingDailyOperations .get_instance() .getIndicator_operations()
		 * .get(indicatora.getIndicator_name())
		 * .setSuccessfully_added_monitor_with_and_cross_with_edges( true); }
		 */

		for (Indicator s_ind : successfull_indicators) {
			getNewly_added_indicators().add(s_ind.getEs_indicator());
		}
		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				"successfully added all indicator" + " without" + " exception,"
						+ " and now leaving the function");

		return successfull_indicators;

	}

	public void inspect_json_indicators_object_of_textfile()
			throws ErrorParsingIndicatorsJsonString, IndicatorHasNoDefinition,
			SpellingErrorWhileLookingForThisSubIndicator,
			PeriodOrOhlcvGroupIsMissing {

		Iterator<String> indicator_names = getJsonobject_textfile_indicators()
				.keys();

		while (indicator_names.hasNext()) {
			Indicator indicator = new Indicator();
			String indicator_name = indicator_names.next();
			indicator.setIndicator_name(indicator_name);
			System.out.println("now doing indicator--->" + indicator_name);

			indicator
					.apply_all_subindicators_to_indicator(subindicators_LinkedHashMap_after_parsing_jsonString);
			try {
				JSONObject indicator_params = getJsonobject_textfile_indicators()
						.getJSONObject(indicator_name);
				Iterator<String> indicator_param_names = indicator_params
						.keys();

				indicator.setCross_with(new ArrayList<String>());
				indicator.setMonitor_with(new ArrayList<String>());
				indicator.setCritical_line_limits(new ArrayList<Double>());
				/**
				 * ohlcv group, and period is not set. at end if it is null,
				 * then there is an error. because these two are required for
				 * any indicator.
				 * 
				 */

				if (indicator_param_names.hasNext()) {
					// it means that we have to process each individual
					// parameter.
					while (indicator_param_names.hasNext()) {
						String parameter = indicator_param_names.next();
						switch (parameter) {
						case "not_applicable_to":
							JSONArray subindicators_not_applicable_to = indicator_params
									.getJSONArray(parameter);
							/**
							 * first we already applied all the subindicators to
							 * it and then remove those which are not
							 * applicable.
							 */
							ArrayList<String> names_of_subindicators_which_are_not_applicable_to_this_indicator = new ArrayList<String>();
							for (int i = 0; i < subindicators_not_applicable_to
									.length(); i++) {
								names_of_subindicators_which_are_not_applicable_to_this_indicator
										.add(subindicators_not_applicable_to
												.getString(i));
							}
							indicator
									.remove_subindicators(names_of_subindicators_which_are_not_applicable_to_this_indicator);

							break;

						case "time_frames":

							JSONArray periods_holder = indicator_params
									.getJSONArray(parameter);
							ArrayList<ArrayList<Integer>> periods = new ArrayList<ArrayList<Integer>>();
							for (int i = 0; i < periods_holder.length(); i++) {
								ArrayList<Integer> calculation_element_periods = new ArrayList<Integer>();
								JSONArray calculation_element_periods_jsonArray = periods_holder
										.getJSONArray(i);
								for (int j = 0; j < calculation_element_periods_jsonArray
										.length(); j++) {
									calculation_element_periods
											.add(calculation_element_periods_jsonArray
													.getInt(j));
								}
								periods.add(calculation_element_periods);
							}
							indicator.setPeriods(periods);
							break;

						case "cross_with":
							JSONArray cross_with_jsonarray = indicator_params
									.getJSONArray(parameter);

							for (int i = 0; i < cross_with_jsonarray.length(); i++) {
								indicator.addCross_with(cross_with_jsonarray
										.getString(i));
							}

							break;

						case "critical_line_limits":
							JSONArray critical_line_limits_jsonarray = indicator_params
									.getJSONArray(parameter);
							for (int i = 0; i < critical_line_limits_jsonarray
									.length(); i++) {
								indicator
										.addCritical_line_limits(critical_line_limits_jsonarray
												.getDouble(i));
							}

							break;

						case "monitor_with":
							JSONArray monitor_with_jsonarray = indicator_params
									.getJSONArray(parameter);
							for (int i = 0; i < monitor_with_jsonarray.length(); i++) {
								indicator
										.addMonitor_with(monitor_with_jsonarray
												.getString(i));
							}

							break;

						case "ohlcv_group":
							JSONArray ohlcv_group_array = indicator_params
									.getJSONArray(parameter);
							for (int i = 0; i < ohlcv_group_array.length(); i++) {
								indicator.addOhlcv_group(ohlcv_group_array
										.getString(i));
							}
							break;
						default:
							break;
						}
					}

				} else {
					throw new IndicatorHasNoDefinition(indicator_name);
				}
				/**
				 * first check if ohlcv group and period are defined, if not
				 * throw error, otherwise add the parsed indicator to the
				 * LinkedHashMap.
				 * 
				 */

				// System.out.println(indicator.getOhlcv_group().toString());
				// System.out.println(indicator.getPeriods().toString());

				if (indicator.getOhlcv_group() == null
						|| indicator.getPeriods() == null) {
					throw new PeriodOrOhlcvGroupIsMissing(
							indicator.getIndicator_name());
				}

				indicators_LinkedHashMap_after_parsing_jsonString.put(
						indicator.getIndicator_name(), indicator);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				throw new ErrorParsingIndicatorsJsonString(indicator_name);

			}

		}
	}

	public LinkedHashMap<Integer, ArrayList<ArrayList<String>>>
	generate_opposite_sub_groups_and_opposite_specific_groups(
			ArrayList<Integer> opposite_group_nos,
			Integer group_no,
			LinkedHashMap<Integer, ArrayList<String>> subgroups_of_this_group,
			LinkedHashMap<Integer, String> names_correlations,
			LinkedHashMap<Integer, ArrayList<Integer>> lh_all_opposite_groups,
			LinkedHashMap<Integer, LinkedHashMap<Integer, ArrayList<String>>> all_subgroups) {

		System.out.println("the group number is:" + group_no);

		LinkedHashMap<Integer, ArrayList<ArrayList<String>>> opposite_sub_groups_per_subgroup = new LinkedHashMap<Integer, ArrayList<ArrayList<String>>>();

		// now take each opposite group/
		// get its subgroup count.
		// if same as current then
		// create a LinkedHashMap and add it to the respective one.
		ArrayList<Integer> opposite_groups_to_this_group = lh_all_opposite_groups
				.get(group_no);
		//System.out.println("the opposite groups to this group are:"
		//		+ opposite_groups_to_this_group);
		if (opposite_groups_to_this_group.size() > 0) {
			// now suppose that the opposite group has equal number of subgroups
			// as
			// this one.
			// then equate them one for one.
			for (Integer opposite_group_no : opposite_groups_to_this_group) {
				// get the subgroups of this opposite group.
				LinkedHashMap<Integer, ArrayList<String>> subgroups_of_opposite_group = all_subgroups
						.get(opposite_group_no);
				 //System.out.println(subgroups_of_opposite_group);
				 //System.out.println(subgroups_of_this_group);

				if (subgroups_of_opposite_group.values().size() == subgroups_of_this_group
						.values().size()) {
					// for each subgroup of this group.
					//System.out.println("they are equal");
					for (Integer subgroup_number_in_main_group : subgroups_of_this_group
							.keySet()) {
						if (opposite_sub_groups_per_subgroup
								.containsKey(subgroup_number_in_main_group)) {

							opposite_sub_groups_per_subgroup
									.get(subgroup_number_in_main_group)
									.add(subgroups_of_opposite_group
											.get(subgroup_number_in_main_group));
						}

						else {
							opposite_sub_groups_per_subgroup.put(
									subgroup_number_in_main_group,
									new ArrayList<ArrayList<String>>());

							opposite_sub_groups_per_subgroup
									.get(subgroup_number_in_main_group)
									.add(subgroups_of_opposite_group
											.get(subgroup_number_in_main_group));

						}
					}
				} else {
					/**
					 * TODO: THIS IS THE PLACE WHERE FOR THE MOMENT WE JUST
					 * GENERATE AN EMPTY ARRAYLIST<ARRAYLIST<STRING>> if the
					 * counts of the subgroups of the two groups dont match.
					 * 
					 * you can change this logic later for deciding how
					 * subgroups should be pitted against opposite subgroups if
					 * the counts dont match.
					 * 
					 * 
					 * 
					 */
					for (Integer subgroup_number_in_main_group : subgroups_of_this_group
							.keySet()) {
						if (opposite_sub_groups_per_subgroup
								.containsKey(subgroup_number_in_main_group)) {
							opposite_sub_groups_per_subgroup.get(
									subgroup_number_in_main_group).add(
									new ArrayList<String>());
						} else {
							opposite_sub_groups_per_subgroup.put(
									subgroup_number_in_main_group,
									new ArrayList<ArrayList<String>>());
						}
					}
				}

			}

		}
		return opposite_sub_groups_per_subgroup;
	}

	public void iterate_over_sub_groups_and_assemble_each_subindicator(
			Integer group_no,
			LinkedHashMap<Integer, ArrayList<String>> subgroups_of_this_group,
			LinkedHashMap<Integer, String> names_correlations,
			LinkedHashMap<Integer, ArrayList<Integer>> lh_all_opposite_groups,

			LinkedHashMap<Integer, LinkedHashMap<Integer, ArrayList<String>>> all_subgroups,
			LinkedHashMap<Integer, LinkedHashMap<Integer, String>> lh_names_corr_holder) {

		Collection<String> all_subindicators_in_this_group = (Collection<String>) names_correlations
				.values();
		/*
		 * System.out.println("these are the all opposite groups.");
		 * System.out.println(lh_all_opposite_groups.toString());
		 * 
		 * try { System.in.read(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		ArrayList<Integer> opposite_groups_index_numbers = lh_all_opposite_groups
				.get(group_no);

		/**
		 * ArrayList<Integer> opposite_group_nos, Integer group_no,
		 * LinkedHashMap<Integer, ArrayList<String>> subgroups_of_this_group,
		 * LinkedHashMap<Integer, String> names_correlations,
		 * LinkedHashMap<Integer,ArrayList<Integer>> lh_all_opposite_groups,
		 * LinkedHashMap<Integer, LinkedHashMap<Integer, ArrayList<String>>>
		 * all_subgroups, LinkedHashMap<Integer,ArrayList<LinkedHashMap<Integer,
		 * String>>> lh_names_corr_holder
		 * 
		 * 
		 * 
		 * 
		 * 
		 */

		/*
		 * LinkedHashMap<Integer, ArrayList<ArrayList<String>>> opp_subgroups =
		 * generate_opposite_sub_groups_and_opposite_specific_groups(
		 * opposite_groups_index_numbers, group_no, subgroups_of_this_group,
		 * names_correlations, lh_all_opposite_groups, all_subgroups,
		 * lh_names_corr_holder);
		 */

		LinkedHashMap<Integer, ArrayList<ArrayList<String>>> opp_subgroups = 
				generate_opposite_sub_groups_and_opposite_specific_groups(
				opposite_groups_index_numbers, group_no,
				subgroups_of_this_group, names_correlations,
				lh_all_opposite_groups, all_subgroups);

		/**
		 * 
		 * iterate over each subindicator of this group and generate all its
		 * attributes.
		 * 
		 * 
		 * 
		 * 
		 * 
		 */

		for (String subindicator_name : all_subindicators_in_this_group) {
			
			/**
			 * see if the subindicator exists or not, if not then create it.
			 * 
			 * 
			 */
			if (subindicators_LinkedHashMap_after_parsing_jsonString
					.containsKey(subindicator_name)) {
				
			} else {
				subindicators_LinkedHashMap_after_parsing_jsonString.put(
						subindicator_name, new SubIndicator(subindicator_name));
			}
			
			

			/**
			 * add the groups of this subindicator.
			 * 
			 */

			subindicators_LinkedHashMap_after_parsing_jsonString.get(
					subindicator_name).add_self_group(
					new ArrayList<String>(all_subindicators_in_this_group));

			/**
			 * add the opposite groups of this subindicator.
			 * 
			 */

			Integer current_size_of_self_groups = subindicators_LinkedHashMap_after_parsing_jsonString
					.get(subindicator_name).getSelf_groups().size();
			Integer index_of_this_subindicator_in_this_group = subindicators_LinkedHashMap_after_parsing_jsonString
					.get(subindicator_name).getSelf_groups()
					.get(current_size_of_self_groups - 1)
					.indexOf(subindicator_name);

			/**
			 * TODO : here is one place where if the count of the opposite group
			 * does not equal count of this group, then we just add an empty
			 * arraylist as the opposite specific subindcator.
			 * 
			 * 
			 * Add the opposite specific subindicator of this subindicator.
			 * 
			 * 
			 * 
			 * 
			 */
			
			for (Integer index_no : opposite_groups_index_numbers) {
				//System.out.println("processing index no :" + index_no);
				LinkedHashMap<Integer, String> subindicators_of_opposite_group = lh_names_corr_holder
						.get(index_no);

				
				ArrayList<String> ark = new ArrayList<String>(
						subindicators_of_opposite_group.values());
				
				ArrayList<ArrayList<String>> ar1 = new ArrayList<ArrayList<String>>();
				ar1.add(ark);
				if (subindicators_LinkedHashMap_after_parsing_jsonString
						.get(subindicator_name).getOpposite_groups().size() <= group_no) {
					
					subindicators_LinkedHashMap_after_parsing_jsonString
							.get(subindicator_name).getOpposite_groups()
							.add(ar1);
				} else {
					subindicators_LinkedHashMap_after_parsing_jsonString
							.get(subindicator_name).getOpposite_groups()
							.get(group_no).add(ark);
				}

			
			}

			/**
			 * #####################################################
			 * 
			 * 
			 * THIS PART ADDS SELF SUB GROUPS AND OPPOSITE SUB GROUPS THIS PART
			 * USES A FUNCTOIN CALLED ABOVE, WHERE THERE IS THE OTHER PART WHERE
			 * IF THE LENGTH OF THE TWO GROUPS ARE COMPARED.
			 * 
			 * name of this function is --->
			 * generate_opposite_sub_groups_and_opposite_specific_groups
			 * 
			 * 
			 * 
			 * ####################################################3
			 */

			Integer sub_groups_of_this_group_counter = 0;
			Integer sub_group_additional_counter = 0;
			Integer opposite_subgroup_additional_counter = 0;
			// System.out.println(subgroups_of_this_group.toString());
			for (Map.Entry entry : subgroups_of_this_group.entrySet()) {
				
				ArrayList<String> other_members_in_subgroup = (ArrayList<String>) entry
						.getValue();
			
				if (other_members_in_subgroup.contains(subindicator_name)) {
					
					Integer index_of_subindicator_in_subgroup = other_members_in_subgroup
							.indexOf(subindicator_name);
					
					ArrayList<ArrayList<String>> ar = new ArrayList<ArrayList<String>>();

					ar.add(other_members_in_subgroup);

					
					if (sub_group_additional_counter == 0) {
						// add it to that.

						subindicators_LinkedHashMap_after_parsing_jsonString
								.get(subindicator_name).getSelf_sub_groups()
								.add(ar);
						sub_group_additional_counter++;
					} else {

						if (subindicators_LinkedHashMap_after_parsing_jsonString
								.get(subindicator_name)
								.getSelf_sub_groups()
								.get(subindicators_LinkedHashMap_after_parsing_jsonString
										.get(subindicator_name)
										.getSelf_sub_groups().size() - 1)
								.contains(other_members_in_subgroup)) {
							// dont do antyhing.
						} else {
							subindicators_LinkedHashMap_after_parsing_jsonString
									.get(subindicator_name)
									.getSelf_sub_groups()
									.get(subindicators_LinkedHashMap_after_parsing_jsonString
											.get(subindicator_name)
											.getSelf_sub_groups().size() - 1)
									.add(other_members_in_subgroup);
						}
					}

					// /SAME THING AGAIN FOR THE OPPOSITE SUBGROUPS.
					if (opp_subgroups.isEmpty()) {

					} else {
						ArrayList<ArrayList<ArrayList<String>>> or = new ArrayList<ArrayList<ArrayList<String>>>();
						// get the opposite subgroups of this subgroup.
						ArrayList<ArrayList<String>> opposite_sub_groups_of_this_subgroup = opp_subgroups
								.get(sub_groups_of_this_group_counter);

						or.add(opposite_sub_groups_of_this_subgroup);
						/**
						 * here adding the opposite sub groups.
						 * 
						 * 
						 */
						ArrayList<String> opposite_specific_subindicator_names = new ArrayList<String>();
						for (ArrayList<String> s : opposite_sub_groups_of_this_subgroup) {
							// System.out.println(s);
							String specific_opposite = s
									.get(index_of_subindicator_in_subgroup);
							// add this in the opposite specific groups.
							opposite_specific_subindicator_names
									.add(specific_opposite);
						}

						ArrayList<ArrayList<String>> opposite_specific_groups_of_this_subindicator_in_this_group = new ArrayList<ArrayList<String>>();
						opposite_specific_groups_of_this_subindicator_in_this_group
								.add(opposite_specific_subindicator_names);

						// now adding upposite subgroups.
						if (opposite_subgroup_additional_counter == 0) {
							subindicators_LinkedHashMap_after_parsing_jsonString
									.get(subindicator_name)
									.getOpposite_subgroups().add(or);
							// here get the index of the subindicator in the
							// current subgroup, and add the same
							// member of the opposite subgroup as the opposite
							// specific group.

							subindicators_LinkedHashMap_after_parsing_jsonString
									.get(subindicator_name)
									.getOpposite_specific_groups()
									.add(opposite_specific_groups_of_this_subindicator_in_this_group);

							opposite_subgroup_additional_counter++;
						} else {
							// there is already entry for this group number
							subindicators_LinkedHashMap_after_parsing_jsonString
									.get(subindicator_name)
									.getOpposite_subgroups()
									.get(subindicators_LinkedHashMap_after_parsing_jsonString
											.get(subindicator_name)
											.getOpposite_subgroups().size() - 1)
									.add(opposite_sub_groups_of_this_subgroup);

							subindicators_LinkedHashMap_after_parsing_jsonString
									.get(subindicator_name)
									.getOpposite_specific_groups()
									.get(subindicators_LinkedHashMap_after_parsing_jsonString
											.get(subindicator_name)
											.getOpposite_specific_groups()
											.size() - 1)
									.add(opposite_specific_subindicator_names);
						}
					}

				} else {

				}

				sub_groups_of_this_group_counter++;
			}

		}
		
		
	}

	public void inspect_json_subindicators_object_of_textfile()
			throws InterruptedException {

		Iterator<String> jsoniterator = getJsonobject_textfile_subindicators()
				.keys();
		while (jsoniterator.hasNext()) {
			try {
				JSONArray jsonarray = (JSONArray) getJsonobject_textfile_subindicators()
						.get(jsoniterator.next());

				/**
				 * a LinkedHashMap of LinkedHashMaps of the subgroups of each
				 * group. integer [0 -> group number] => LinkedHashMap
				 * <integer(sub-group number -0-) => arraylist<string> sub-group
				 * contents. >, (sub-group number -1-) => arraylist<string>
				 * 
				 * [1 -> group number] => LinkedHashMap ....
				 * 
				 * 
				 */
				LinkedHashMap<Integer, LinkedHashMap<Integer, ArrayList<String>>> all_sub_groups = new LinkedHashMap<Integer, LinkedHashMap<Integer, ArrayList<String>>>();

				/**
				 * simple array of all the group ids that we have.
				 * 
				 * 
				 */
				ArrayList<Integer> group_ids_array = new ArrayList<Integer>();

				/**
				 * an arraylist which contains the opposite groups for each
				 * group. it is built as we go along processing each group.
				 * 
				 */
				ArrayList<ArrayList<Integer>> all_opposite_groups = new ArrayList<ArrayList<Integer>>();
				LinkedHashMap<Integer, ArrayList<Integer>> lh_all_opp_groups = new LinkedHashMap<Integer, ArrayList<Integer>>();

				/**
				 * an arraylist which holds for each group [group 0->
				 * LinkedHashMap<integer,string> -> integer:subindicator index
				 * => string:subindicator name, next groups hashamp.]
				 * 
				 */
				LinkedHashMap<Integer, LinkedHashMap<Integer, String>> lh_names_corr_holder = new LinkedHashMap<Integer, LinkedHashMap<Integer, String>>();

				ArrayList<LinkedHashMap<Integer, String>> names_correlations_holder = new ArrayList<LinkedHashMap<Integer, String>>();

				/**
				 * here we process one group at a time.
				 * 
				 * 
				 */
				for (Integer i = 0; i < jsonarray.length(); i++) {

					JSONObject group = jsonarray.getJSONObject(i);

					if (group.has("id")) {

						Integer group_id = Integer.parseInt(group
								.getString("id"));
						group_ids_array.add(group_id);
						JSONArray subindicators_array = (JSONArray) group
								.get("names");

						/**
						 * building the opposite groups array.
						 * 
						 */
						JSONArray opposite_groups = null;

						try {
							opposite_groups = (JSONArray) group
									.get("opposite_groups");
						} catch (JSONException je) {

						}
						ArrayList<Integer> opposite_groups_holder = new ArrayList<Integer>();
						if (opposite_groups == null) {
							lh_all_opp_groups.put(group_id,
									new ArrayList<Integer>());
							all_opposite_groups.add(new ArrayList<Integer>());
						} else {
							for (int io = 0; io < opposite_groups.length(); io++) {
								opposite_groups_holder.add(opposite_groups
										.getInt(io));
							}
							all_opposite_groups.add(opposite_groups_holder);
							lh_all_opp_groups.put(group_id,
									opposite_groups_holder);
						}

						/**
						 * make the sub groups and add them to a master array.
						 * names correlations is specific for each group. it is
						 * a simple hashamp that is added ot the names
						 * correlations holder.
						 * 
						 */
						int scope = 1;
						if (subindicators_array.length() < 4) {

						} else {
							scope = (int) Math.sqrt((int) subindicators_array
									.length());
						}

						/**
						 * subindicator index in the group -> subindicator name
						 * 
						 * 
						 */
						LinkedHashMap<Integer, String> names_correlations = new LinkedHashMap<Integer, String>();

						/**
						 * this contains the subgroups as
						 * 
						 * 0 -> [0,1,2] i.e the index number of the subgroups .
						 * 
						 * 
						 */
						LinkedHashMap<Integer, ArrayList<Integer>> sub_groups = new LinkedHashMap<Integer, ArrayList<Integer>>();

						/**
						 * now creating names correlations, adding it to names
						 * correlations holder, also createing sub_groups
						 * 
						 */
						for (Integer ii = 0; ii < subindicators_array.length(); ii++) {
							names_correlations.put(ii,
									subindicators_array.getString(ii));
							ArrayList<Integer> sub_groups_holder = new ArrayList<Integer>();
							for (Integer k = ii; k < ii + scope; k++) {
								// if(k == null){
								// System.out.println("k is null");
								// Thread.sleep(100000);
								// }
								sub_groups_holder.add(k);
							}

							sub_groups.put(ii, sub_groups_holder);
						}
						lh_names_corr_holder.put(group_id, names_correlations);
						names_correlations_holder.add(names_correlations);

						/**
						 * now creating sub groups wth names. its the same as
						 * subgroups, just instead of index numbers, here are
						 * names. then adding this LinkedHashMap(sub groups with
						 * names(its only of current group)) to all subgroups
						 * LinkedHashMap.
						 * 
						 */
						LinkedHashMap<Integer, ArrayList<String>> sub_groups_with_names = new LinkedHashMap<Integer, ArrayList<String>>();
						Integer counter = 0;
						for (ArrayList<Integer> holder : sub_groups.values()) {
							ArrayList<String> names_holder = new ArrayList<String>();
							for (Integer ik : holder) {
								if (names_correlations.get(ik) == null) {

								} else {
									names_holder
											.add(names_correlations.get(ik));
								}
							}
							sub_groups_with_names.put(counter, names_holder);
							counter++;
						}

						all_sub_groups.put(group_id, sub_groups_with_names);

					} else {
						// no id, cannot admit

					}

				}

				/**
				 * now again iterate each group at a time. and update the
				 * subindicator_from_text_file LinkedHashMap, i.e process each
				 * subindicator.
				 * 
				 */
				for (Integer current_group : group_ids_array) {

					LinkedHashMap<Integer, ArrayList<String>> subgroups_of_this_group = all_sub_groups
							.get(current_group);

					iterate_over_sub_groups_and_assemble_each_subindicator(
							current_group, subgroups_of_this_group,
							lh_names_corr_holder.get(current_group),
							lh_all_opp_groups, all_sub_groups,
							lh_names_corr_holder);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public String get_indicator_from_es(String indicator_name) {
		String indicator_from_es = null;
		SearchResponse sr = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("indicator")
				.setQuery(
						QueryBuilders.filteredQuery(
								QueryBuilders.matchAllQuery(),
								termFilter("indicator_name", indicator_name)))
				.setSize(1)
				.setFetchSource(
						new String[] { "indicator_name",
								"indicator_numeric_id", "indicator_hex_id",
								"ohlcv_groups", "periods", "monitor_with",
								"cross_with", "applicable_subindicators",
								"critical_line_limits" }, null).execute()
				.actionGet();

		for (SearchHit hit : sr.getHits().hits()) {
			EsIndicator existing_es_indicator = new EsIndicator((String) hit
					.sourceAsMap().get("indicator_name"));

			existing_es_indicator
					.setCritical_line_limits((ArrayList<Double>) hit
							.sourceAsMap().get("critical_line_limits"));

			existing_es_indicator.setIndicator_numeric_id((Integer) hit
					.sourceAsMap().get("indicator_numeric_id"));

			existing_es_indicator.setIndicator_hex_id((String) hit
					.sourceAsMap().get("indicator_hex_id"));

			existing_es_indicator.setOhlcv_groups((ArrayList<String>) hit
					.sourceAsMap().get("ohlcv_groups"));

			existing_es_indicator
					.setApplicable_subindicators((ArrayList<HashMap<String, Object>>) hit
							.sourceAsMap().get("applicable_subindicators"));

			existing_es_indicator
					.setCritical_line_limits((ArrayList<Double>) hit
							.sourceAsMap().get("critical_line_limits"));

			existing_es_indicator
					.setCross_with((ArrayList<HashMap<String, Object>>) hit
							.sourceAsMap().get("cross_with"));

			existing_es_indicator
					.setMonitor_with((ArrayList<HashMap<String, Object>>) hit
							.sourceAsMap().get("monitor_with"));

			existing_es_indicator
					.setPeriods((ArrayList<HashMap<String, Object>>) hit
							.sourceAsMap().get("periods"));

			existing_es_indicator.build_string_representation();

			return existing_es_indicator.getIndicator_string_representation();
		}

		return "";

	}

	public Indicator search_database_for_indicator(TitanGraph g,
			String indicator_name) {

		Indicator existing_indicator_in_database = new Indicator();
		existing_indicator_in_database.setIndicator_name(indicator_name);

		Iterator<Vertex> vertex_iterator = g.query()
				.has("indicator_name", indicator_name).vertices().iterator();
		Vertex indicator_vertex = null;

		Boolean does_not_exist_in_graph = true;

		while (vertex_iterator.hasNext()) {

			does_not_exist_in_graph = false;
			// System.out.println("the vertices were found.");
			indicator_vertex = vertex_iterator.next();

			Object ohlcv_group = indicator_vertex.getProperty("ohlcv_groups");

			if (ohlcv_group == null) {
				existing_indicator_in_database.setOhlcv_group(null);
			} else {
				existing_indicator_in_database
						.setOhlcv_group((ArrayList<String>) ohlcv_group);
			}

			String gson_periods = (String) indicator_vertex
					.getProperty("periods");
			// System.out.println(gson_periods);
			if (gson_periods == null) {
				// System.out.println("gson periods wer enull");
				existing_indicator_in_database.setPeriods(null);
			} else {
				// System.out.println("gson periods were not null");
				existing_indicator_in_database.setPeriods(gson.fromJson(
						gson_periods, ArrayList.class));

			}

			Object critical_line_limits = indicator_vertex
					.getProperty("critical_line_limits");
			if (critical_line_limits == null) {

			} else {
				existing_indicator_in_database
						.setCritical_line_limits((ArrayList<Double>) critical_line_limits);
			}

			Iterator<Edge> applicable_subindicators = indicator_vertex
					.getEdges(Direction.OUT, "applicable_to_subindicator")
					.iterator();
			existing_indicator_in_database
					.setSubindicators_applicable_to_this_indicator(new ArrayList<SubIndicator>());

			// adding all the applicable subindicators.

			ArrayList<Edge> applicable_subindicator_edges = new ArrayList<Edge>();

			while (applicable_subindicators.hasNext()) {
				applicable_subindicator_edges.add(applicable_subindicators
						.next());
			}
			for (Edge e : applicable_subindicator_edges) {
				// System.out.println("applicable subindicators were found.");

				Vertex applicable_subindicator = e.getVertex(Direction.IN);
				// System.out.println("got the vertex on its end.");
				// createing a new subindicator with the name on this vertex.

				// invoking the assemlbe subindicator function to

				SubIndicator existing_subindicator_on_this_vertex = new SubIndicator(
						(String) applicable_subindicator
								.getProperty("subindicator_name"));

				existing_indicator_in_database
						.add_subindicator(getAssembled_subindicators_LinkedHashMap()
								.get(existing_subindicator_on_this_vertex
										.getSubIndicator_name()));

			}

			// adding the monitor with.

			ArrayList<Edge> outgoing_monitor_with_edges = GroovyStatements
					.getInstance().get_outgoing_edge_with_label(
							indicator_vertex, "monitor_indicator_with");
			// System.out.println("monitor with edges");
			// System.out.println(outgoing_monitor_with_edges);
			for (Edge e : outgoing_monitor_with_edges) {
				Vertex monitor_with_indicator = e.getVertex(Direction.IN);
				existing_indicator_in_database
						.addMonitor_with((String) monitor_with_indicator
								.getProperty("indicator_name"));
			}

			// adding the cross with.

			ArrayList<Edge> outgoing_cross_with_edges = GroovyStatements
					.getInstance().get_outgoing_edge_with_label(
							indicator_vertex, "cross_indicator_with");
			// System.out.println("cross with ");
			// System.out.println(outgoing_cross_with_edges);
			for (Edge e : outgoing_cross_with_edges) {
				Vertex cross_with_indicator = e.getVertex(Direction.IN);
				// System.out.println("the id of the in vertex is");
				// System.out.println(cross_with_indicator.getId());
				existing_indicator_in_database
						.addCross_with((String) cross_with_indicator
								.getProperty("indicator_name"));
			}

		}

		LoggingDailyOperations.get_instance().getIndicator_operations()
				.get(existing_indicator_in_database.getIndicator_name())
				.setNot_present_in_graph(does_not_exist_in_graph);

		g.commit();

		return existing_indicator_in_database;
	}

	public EsSubIndicator search_if_subindicator_exists(TitanGraph g,
			String subindicator_name) {

		SearchResponse sr = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("subindicator")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(
						QueryBuilders.termQuery("subindicator_name",
								subindicator_name))
				.setFetchSource(
						new String[] { "subindicator_name",
								"subindicator_numeric_id",
								"subindicator_hex_id", "groups" }, null)
				.setSize(1).execute().actionGet();

		EsSubIndicator db_subindicator = new EsSubIndicator(subindicator_name);
		for (SearchHit hit : sr.getHits().hits()) {
			db_subindicator.setGroups((ArrayList<HashMap<String, Object>>) hit
					.sourceAsMap().get("groups"));
			db_subindicator.setNumeric_id((Integer) hit.sourceAsMap().get(
					"subindicator_numeric_id"));
			db_subindicator.setHex_id((String) hit.sourceAsMap().get(
					"subindicator_hex_id"));
			return db_subindicator;
		}

		return new EsSubIndicator();

		/*
		 * Vertex subindicator_vertex = null; GremlinPipeline gl = new
		 * GremlinPipeline(); gl.start( g.query().has("subindicator_name",
		 * subindicator_name)
		 * .vertices()).as("subindicator").outE().as("edge").inV()
		 * .as("related_subindicator").select();
		 * 
		 * while (gl.hasNext()) { Row row = (Row) gl.next(); subindicator_vertex
		 * = (Vertex) row.getColumn("subindicator"); }
		 * 
		 * if (subindicator_vertex == null) {
		 * LoggingDailyOperations.get_instance().getSubindicator_operations()
		 * .get(subindicator_name).setNot_present_in_graph(true);
		 * LoggingDailyOperations.get_instance().add_to_log( "info", null,
		 * identifier_for_log, "this subindicator does not exist in graph:" +
		 * subindicator_name);
		 * System.out.println("no such subindicator currently exists"); return
		 * new SubIndicator(subindicator_name);
		 * 
		 * } else { LoggingDailyOperations .get_instance() .add_to_log( "info",
		 * null, identifier_for_log,
		 * "subindicator is present in graph, see next log statement to see if it is same or not:"
		 * + subindicator_name);
		 * System.out.println("such a subindicator already exists");
		 * SubIndicator existing_subindicator_in_database = new SubIndicator(
		 * subindicator_name);
		 * existing_subindicator_in_database.assemble_existing_information(g);
		 * return existing_subindicator_in_database; }
		 */
	}

}
