package genericIndicatorFunctions;

import org.elasticsearch.client.Client;

import Titan.GroovyStatements;
import Titan.TitanBaseConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import dailyDownload.CreateCommonDayIdVertices;
import elasticSearchNative.ClientInstance;
import DailyOperationsLogging.LogOnShutdown;
import DailyOperationsLogging.LoggingDailyOperations;

public class CreateIndSubInd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LogOnShutdown lsd = new LogOnShutdown();
		lsd.attachShutDownHook();
		
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				"daily_download-BEGIN", "#########################----------begin-------##############################");
		
		Client client = null;
		TitanBaseConfig cfg = null;
		TitanGraph g = null;
		TitanManagement mgmt = null;

		GroovyStatements gs = null;
		Gson gson = null;

		try {
			client = ClientInstance.getInstance().getCl();
			cfg = new TitanBaseConfig("titan-cassandra-es.properties");
			g = cfg.get_graph();

			mgmt = g.getManagementSystem();
			GroovyStatements.initializeGroovyStatements(g, mgmt);

			CreateCommonDayIdVertices cid = new CreateCommonDayIdVertices(g,mgmt);

			gs = GroovyStatements.getInstance();
			gson = new GsonBuilder().setPrettyPrinting().create();

			LoggingDailyOperations.get_instance().add_to_log("info", null,
					"daily_download", "finished initialization corretly");
			CreateDailyIndicatorsJsonString crud = 
					new CreateDailyIndicatorsJsonString(g, gson);
			System.out.println("came out of the total thing.");
			System.out.println("now entering the summary creation.");
			
			GenerateSummaryOfNewlyAddedIndicators gen_s = new GenerateSummaryOfNewlyAddedIndicators();
			
			g.shutdown();
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					"init", null);
			
			
		}
		
		
	}

}
