package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Collections;

public class CreateIndicatorNamesInLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer [] rangesn = {-10,-9,-8,-7,-6,-5,-4,-3,-2,-1};
		Integer [] rangesp = {1,2,3,4,5,6,7,8,9,10};
		Integer [] days = {1,2,3,4,5};
		String stub = "indicator_range_";
		String fors = "_for_";
		String dayn = "_days";
		ArrayList<String> strings = new ArrayList<String>();
		
		for(Integer range: rangesp){
			
			for(Integer day: days){
				strings.add(stub +  range + fors + String.valueOf(day) + dayn);
			}
		}
		Collections.reverse(strings);
		System.out.println(strings);
	}

}
