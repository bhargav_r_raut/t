package genericIndicatorFunctions;

import static org.elasticsearch.index.query.FilterBuilders.termFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.javatuples.Pair;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import yahoo_finance_historical_scraper.StockConstants;
import CommonUtils.CompressUncompress;
import DailyOperationsLogging.LoggingDailyOperations;
import ExchangeBuilder.Entity;
import Indicators.RedisManager;
import Titan.ExecuteMultiThreadedJob;
import Titan.GroovyStatements;
import Titan.TItanWorkerThreadResultObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import elasticSearchNative.BulkLoader;
import elasticSearchNative.LocalEs;
import elasticSearchNative.IBulkLoader;

/**
 * 
 * we store key - name of complex in it we store its id. then we also store the
 * names of the opposite subgroups. then after all is done we iterate each key,
 * and update in es, the ids of the opposite subgroups
 * 
 * 
 * 
 * this class will search the entire database for indicators.
 * 
 * first we will get a list of indicators and subindicators which have changed.
 * 
 * 
 * it will start with the first indicator(that has changed) then it will get all
 * the applicable subindicators to that indicator. then it will get all the
 * stocks which have a ohlcv group the same as itself.
 * 
 * then it will start searching for nodes which contain the combination of it
 * with the first subindicator.
 * 
 * 
 * @author aditya
 * 
 */
public class CreateIndicatorSubIndicatorPairsForEachStock implements
		IBulkLoader {

	private String indice;
	private Entity e;

	private static final String identifier_for_log = "Create_Indicator_SubIndicator_Pairs_for_Each_stock";
	private BulkLoader bulkloader;
	/**
	 * getters and setters for creating pairs for a new stock.
	 * 
	 * 
	 * private String stockName_of_new_stock; private String
	 * stockFullName_of_new_stock; private String indice_of_new_stock; private
	 * Boolean generate_complexes_for_new_stock;
	 * 
	 * public Boolean getGenerate_complexes_for_new_stock() { return
	 * generate_complexes_for_new_stock; }
	 * 
	 * public void setGenerate_complexes_for_new_stock( Boolean
	 * generate_complexes_for_new_stock) { this.generate_complexes_for_new_stock
	 * = generate_complexes_for_new_stock; }
	 * 
	 * public String getStockName_of_new_stock() { return
	 * stockName_of_new_stock; }
	 * 
	 * public void setStockName_of_new_stock(String stockName_of_new_stock) {
	 * this.stockName_of_new_stock = stockName_of_new_stock; }
	 * 
	 * public String getStockFullName_of_new_stock() { return
	 * stockFullName_of_new_stock; }
	 * 
	 * public void setStockFullName_of_new_stock(String
	 * stockFullName_of_new_stock) { this.stockFullName_of_new_stock =
	 * stockFullName_of_new_stock; }
	 * 
	 * public String getIndice_of_new_stock() { return indice_of_new_stock; }
	 * 
	 * public void setIndice_of_new_stock(String indice_of_new_stock) {
	 * this.indice_of_new_stock = indice_of_new_stock; }
	 * 
	 * /** end getters and setters.
	 * 
	 * 
	 */

	private Gson gson;
	private TitanGraph g;
	private BulkRequestBuilder builder;
	private BulkProcessor bulkprocessor;
	private final Long bulk_items_amount = 2000L;
	private ArrayList<String> ids_of_es_complex_docs;

	private BulkRequestBuilder brq;

	public BulkRequestBuilder getBrq() {
		return brq;
	}

	public void setBrq(BulkRequestBuilder brq) {
		this.brq = brq;
	}

	public ArrayList<String> getIds_of_es_complex_docs() {
		if (this.ids_of_es_complex_docs == null) {
			this.ids_of_es_complex_docs = new ArrayList<String>();
		}
		return this.ids_of_es_complex_docs;
	}

	public void setIds_of_es_complex_docs(
			ArrayList<String> ids_of_es_complex_docs) {
		this.ids_of_es_complex_docs = ids_of_es_complex_docs;
	}

	public void addids_of_es_objects(String id) {
		getIds_of_es_complex_docs().add(id);
	}

	public BulkProcessor getBulkprocessor() {

		if (bulkprocessor == null) {
			this.bulkprocessor = BulkProcessor
					.builder(LocalEs.getInstance(),
							new BulkProcessor.Listener() {

								@Override
								public void afterBulk(long execution_id,
										BulkRequest request,
										BulkResponse response) {
									// TODO Auto-generated method stub
									Long start_item_being_processed = execution_id
											* bulk_items_amount;
									BulkItemResponse[] responses = response
											.getItems();
									for (int i = 0; i < responses.length; i++) {

										if (responses[i].isFailed()) {
											LoggingDailyOperations
													.get_instance()
													.getComplexes_which_failed_to_add_into_es()
													.add(getCombined_vertex_object_arraylist()
															.get(start_item_being_processed
																	.intValue()
																	+ i)
															.getCombined_vertex_name());
											LoggingDailyOperations
													.get_instance()
													.add_to_log(
															"exception",
															new Exception(
																	"failed to add an item to es"),
															identifier_for_log,
															null);
										} else {
											// getIds_of_es_complex_docs().add(
											// responses[i].getId());
										}
									}
									if (!LoggingDailyOperations.get_instance()
											.has_exception()) {
										LoggingDailyOperations
												.get_instance()
												.add_to_log(
														"info",
														null,
														identifier_for_log,
														"completed:"
																+ (start_item_being_processed
																		.intValue() + bulk_items_amount
																		.intValue())
																+ " into es");
									}

								}

								@Override
								public void afterBulk(long arg0,
										BulkRequest arg1, Throwable arg2) {
									// TODO Auto-generated method stub
									arg2.printStackTrace();
									LoggingDailyOperations
											.get_instance()
											.add_to_log(
													"exception",
													new Exception(
															arg2.getLocalizedMessage()),
													identifier_for_log, null);
									LoggingDailyOperations.get_instance()
											.exit_and_log_before_exiting(g);
								}

								@Override
								public void beforeBulk(long arg0,
										BulkRequest arg1) {
									// TODO Auto-generated method stub
									System.out
											.println("these were the number of requests sent to es. bulk");
									System.out.println(arg1.numberOfActions());

								}

							}).setBulkActions(bulk_items_amount.intValue())
					.setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
					.setFlushInterval(TimeValue.timeValueSeconds(100))
					.setConcurrentRequests(1).build();
		}
		return bulkprocessor;
	}

	public BulkRequestBuilder getBuilder() {
		if (this.builder == null) {
			this.builder = LocalEs.getInstance().prepareBulk();

		}
		return builder;
	}

	private ArrayList<Indicator> changed_indicators;
	private ArrayList<SubIndicator> changed_subindicators;

	private HashMap<String, Indicator> changed_indicators_map;
	private HashMap<String, SubIndicator> changed_subindicators_map;

	public HashMap<String, Indicator> getChanged_indicators_map() {
		return changed_indicators_map;
	}

	public void setChanged_indicators_map(
			HashMap<String, Indicator> changed_indicators_map) {
		this.changed_indicators_map = changed_indicators_map;
	}

	public HashMap<String, SubIndicator> getChanged_subindicators_map() {
		return changed_subindicators_map;
	}

	public void setChanged_subindicators_map(
			HashMap<String, SubIndicator> changed_subindicators_map) {
		this.changed_subindicators_map = changed_subindicators_map;
	}

	private ArrayList<Vertex> changed_indicators_vertices;
	private ArrayList<Vertex> changed_subindicators_vertices;
	private JSONObject registry;
	private ArrayList<CombinedVertexObject> combined_vertex_object_arraylist;
	private TreeMap<Double, String> md5_hash;
	private Long md5_hash_holder_vertex_id;
	private ArrayList<String> failed_elasticsearch_additions;
	private HashMap<String, SubIndicator> assembled_subindicators;
	private HashMap<String, String> registry_keys_and_values_hm;

	public HashMap<String, String> getRegistry_keys_and_values_hm() {
		return registry_keys_and_values_hm;
	}

	public void setRegistry_keys_and_values_hm(
			HashMap<String, String> registry_keys_and_values_hm) {
		this.registry_keys_and_values_hm = registry_keys_and_values_hm;
	}

	public HashMap<String, SubIndicator> getAssembled_subindicators() {
		if (this.assembled_subindicators == null) {
			this.assembled_subindicators = new HashMap<String, SubIndicator>();
		}
		return assembled_subindicators;
	}

	public ArrayList<String> getFailed_elasticsearch_additions() {
		if (failed_elasticsearch_additions == null) {
			setFailed_elasticsearch_additions(new ArrayList<String>());
		}
		return failed_elasticsearch_additions;
	}

	public void setFailed_elasticsearch_additions(
			ArrayList<String> failed_elasticsearch_additions) {

		this.failed_elasticsearch_additions = failed_elasticsearch_additions;
	}

	/**
	 * new code for registry counters.
	 * 
	 * 
	 * @return
	 */

	/**
	 * new code for registry counters ends.
	 * 
	 * 
	 * @return
	 */

	public TreeMap<Double, String> getMd5_hash() {
		if (md5_hash == null) {
			Iterator<Vertex> itv = getG().query().has("placeholder", 1)
					.vertices().iterator();
			while (itv.hasNext()) {
				Vertex hash_holder = itv.next();
				md5_hash_holder_vertex_id = (Long) hash_holder.getId();
				String hash_string = (String) hash_holder
						.getProperty("hash_string");
				Gson gson = new Gson();
				TreeMap<Double, String> hm = gson.fromJson(hash_string,
						TreeMap.class);
				setMd5_hash(hm);

				/**
				 * also get the registry_keys_and_values_hashmap_and_set_it.
				 * 
				 * 
				 */
				String registry_keys_and_values = (String) hash_holder
						.getProperty("registry_keys_and_values");
				HashMap<String, String> hkv = gson.fromJson(
						registry_keys_and_values, HashMap.class);
				setRegistry_keys_and_values_hm(hkv);

				return hm;
			}
			setMd5_hash(new TreeMap<Double, String>());
			setRegistry_keys_and_values_hm(new HashMap<String, String>());
			// in this case there is nothing, so return an empty hashmap.
			return new TreeMap<Double, String>();
		}

		return md5_hash;
	}

	public void setMd5_hash(TreeMap<Double, String> md5_hash) {
		this.md5_hash = md5_hash;
	}

	public Boolean recommit_md5_hash_to_db() {

		HashMap<String, Pair<Integer, String>> registry_components = GroovyStatements
				.getInstance().registry_hash;
		ArrayList<RegistryObject> registry_objects_list = new ArrayList<RegistryObject>();

		for (Map.Entry<String, Pair<Integer, String>> entry : registry_components
				.entrySet()) {
			RegistryObject rob = new RegistryObject(entry.getKey(), entry
					.getValue().getValue1(), entry.getValue().getValue0());
			registry_objects_list.add(rob);
		}

		ExecuteMultiThreadedJob ex = new ExecuteMultiThreadedJob(
				registry_objects_list, g, g.getManagementSystem(),
				"add_registry_objects", false,
				new ArrayList<org.json.JSONObject>());

		ArrayList<TItanWorkerThreadResultObject> results = ex.getRes();
		if (results != null) {
			for (TItanWorkerThreadResultObject result : results) {
				if (!(result.get_failed_object().length() == 0)) {
					// remove this from elasticsearch.
					try {
						Exception e = (Exception) result.get_failed_object()
								.get("exception");
						LoggingDailyOperations.get_instance().add_to_log(
								"exception", e, identifier_for_log, null);

					} catch (JSONException e) {
						LoggingDailyOperations.get_instance().add_to_log(
								"exception", e, identifier_for_log, null);
					}

				}
			}
		} else {
			LoggingDailyOperations
					.get_instance()
					.add_to_log(
							"exception",
							new Exception(
									"check the previous exception,"
											+ " there was an error in the multi thread graph add"
											+ " job, so null was returned as the results array."),
							identifier_for_log, null);
		}

		return null;
	}

	private HashMap<String, Vertex> subindicator_vertices_hashmap;

	public HashMap<String, Vertex> getSubindicator_vertices_hashmap() {
		return subindicator_vertices_hashmap;
	}

	public void setSubindicator_vertices_hashmap(
			HashMap<String, Vertex> subindicator_vertices_hashmap) {
		this.subindicator_vertices_hashmap = subindicator_vertices_hashmap;
	}

	public ArrayList<CombinedVertexObject> getCombined_vertex_object_arraylist() {
		return combined_vertex_object_arraylist;
	}

	public void setCombined_vertex_object_arraylist(
			ArrayList<CombinedVertexObject> combined_vertex_object_arraylist) {
		this.combined_vertex_object_arraylist = combined_vertex_object_arraylist;
	}

	public JSONObject getRegistry() throws ParseException {
		if (this.registry == null) {
			Iterator<Vertex> registry_vertex_iterator = g.query()
					.has("registry", "registry").vertices().iterator();
			Vertex registry_vertex = null;
			while (registry_vertex_iterator.hasNext()) {
				registry_vertex = registry_vertex_iterator.next();
			}
			if (registry_vertex == null) {
				setRegistry(new JSONObject());
				return new JSONObject();
			} else {
				String current_registry = (String) registry_vertex
						.getProperty("registry");
				JSONParser parser = new JSONParser();
				setRegistry((JSONObject) parser.parse(current_registry));
				return getRegistry();
			}
		} else {
			return this.registry;
		}

	}

	public void setRegistry(JSONObject registry) {
		this.registry = registry;
	}

	public ArrayList<Vertex> getChanged_subindicators_vertices() {
		return changed_subindicators_vertices;
	}

	public void setChanged_SubIndicators_vertices(
			ArrayList<Vertex> change_subindicators_vertices) {
		this.changed_subindicators_vertices = change_subindicators_vertices;
	}

	public ArrayList<Vertex> getChanged_indicators_vertices() {
		return changed_indicators_vertices;
	}

	public void setChanged_indicators_vertices(
			ArrayList<Vertex> changed_indicators_vertices) {
		this.changed_indicators_vertices = changed_indicators_vertices;
	}

	public TitanGraph getG() {
		return g;
	}

	public void setG(TitanGraph g) {
		this.g = g;
	}

	public ArrayList<Indicator> getChanged_indicators() {
		return changed_indicators;
	}

	public void setChanged_indicators(ArrayList<Indicator> changed_indicators) {
		this.changed_indicators = changed_indicators;
		if (this.changed_indicators_map == null) {
			this.changed_indicators_map = new HashMap<String, Indicator>();
		}
		for (Indicator i : changed_indicators) {
			this.changed_indicators_map.put(i.getIndicator_name(), i);
		}
	}

	public ArrayList<SubIndicator> getChanged_subindicators() {
		return changed_subindicators;
	}

	public void setChanged_subindicators(
			ArrayList<SubIndicator> changed_subindicators) {
		this.changed_subindicators = changed_subindicators;
		if (this.changed_subindicators_map == null) {
			this.changed_subindicators_map = new HashMap<String, SubIndicator>();
		}
		for (SubIndicator s : this.changed_subindicators) {
			this.changed_subindicators_map.put(s.getSubIndicator_name(), s);
		}

	}

	public ArrayList<String> get_changed_subindicators_names() {
		ArrayList<String> changed_subindicator_names = new ArrayList<String>();
		for (SubIndicator subindicator : getChanged_subindicators()) {
			changed_subindicator_names.add(subindicator.getSubIndicator_name());
		}

		return changed_subindicator_names;
	}

	public ArrayList<String> get_changed_indicators_names() {
		ArrayList<String> changed_indicator_names = new ArrayList<String>();
		for (Indicator indicator : getChanged_indicators()) {
			changed_indicator_names.add(indicator.getIndicator_name());
		}

		return changed_indicator_names;
	}

	public ArrayList<String> assemble_subindicators_from_es_indicator(
			EsIndicator es_indicator) {

		ArrayList<String> subindicators_applicable_to_indicator = new ArrayList<String>();

		for (HashMap<String, Object> applicable_subindicator : es_indicator
				.getApplicable_subindicators()) {
			subindicators_applicable_to_indicator
					.add((String) applicable_subindicator
							.get("applicable_subindicator_name"));
		}

		return subindicators_applicable_to_indicator;

	}

	public ArrayList<ArrayList<Integer>> assemble_periods_from_es_indicator(
			EsIndicator es_indicator) {

		ArrayList<ArrayList<Integer>> assembled_periods = new ArrayList<ArrayList<Integer>>();
		for (HashMap<String, Object> period : es_indicator.getPeriods()) {

			assembled_periods.add((ArrayList<Integer>) period
					.get("index_components"));

		}

		return assembled_periods;

	}

	public ArrayList<String> combined_indicator_subindicator_names_for_changed_indicator_new(
			ArrayList<String> changed_subindicator_names,
			EsIndicator es_indicator) {

		try {
			ArrayList<String> pairs = new ArrayList<String>();
			// Indicator indicator = getChanged_indicators_map().get(
			// v.getProperty("indicator_name"));
			// String indicator_name = indicator.getIndicator_name();

			String indicator_name = es_indicator.getIndicator_name();

			// System.out.println("starting to add the pairs.");
			// System.out.println("the indicator name is--->" + indicator_name +
			// "press to continue");
			// System.in.read();
			/**
			 * what if there is no two period modulateros, in that case we would
			 * get nothing, however we need a seperate indicator for each of the
			 * individual periods even if there is only one period modulator.
			 * 
			 */
			ArrayList<ArrayList<Integer>> periods = assemble_periods_from_es_indicator(es_indicator);
			// System.out.println("the current periods look like this---->"
			// + periods.toString());

			// System.in.read();

			ArrayList<String> period_names = new ArrayList<String>();
			Integer outer_counter = 0;
			for (ArrayList<Integer> list_a : periods) {
				// System.out.println("outer list is --->" + list_a.toString() +
				// "outer counter is=-=>"
				// + outer_counter);
				// System.out.println("press to continue");
				// System.in.read();
				Integer inner_counter = 0;
				for (ArrayList<Integer> list_b : periods) {
					// System.out.println("inner list is--->" +
					// list_b.toString() +
					// "inner counter is" +
					// inner_counter);
					if (outer_counter < inner_counter) {
						for (Integer list_a_interger : list_a) {
							for (Integer list_b_integer : list_b) {
								period_names.add("period_start_"
										+ String.valueOf(list_a_interger) + "_"
										+ String.valueOf(list_b_integer)
										+ "_period_end");
								// System.out.println("now added to period_names--->"
								// +
								// String.valueOf(list_a_interger)
								// + "_day_" + String.valueOf(list_b_integer) +
								// "_day");
								// System.out.println("press to continue");
								// System.in.read();

							}
						}
					} else {
						// now mix the two lists.

					}

					inner_counter++;
				}
				outer_counter++;
			}

			if (period_names.isEmpty()) {
				// in this case we have to take each thing in the periods array
				// ,
				// and make one
				// name out of it.
				// this is the case where there is only one determinant in the
				// periods.
				// System.out.println("period names was empty--->");

				if (periods.size() == 1) {
					// System.out.println("periods size is 1");
					for (Integer period : periods.get(0)) {
						period_names.add("period_start_"
								+ String.valueOf(period) + "_period_end");
					}
					// System.out
					// .println("period names after adding for size 1 is0-->");
					// System.out.println(period_names.toString());
					// System.out.println("press to continue");
					// System.in.read();
				}
			}

			ArrayList<String> subindicators_applicable_to_indicator = assemble_subindicators_from_es_indicator(es_indicator);

			for (String period_name : period_names) {
				for (String subindicator_name : subindicators_applicable_to_indicator) {
					if (changed_subindicator_names.contains(subindicator_name)) {
						pairs.add(indicator_name + "_" + period_name + "_"
								+ subindicator_name);

					} else {
						// in case the provided changed subindicator list is
						// completely
						// empty,
						// we add the all the applicable subindicators.
						if (changed_subindicator_names.size() == 0) {
							pairs.add(indicator_name + "_" + period_name + "_"
									+ subindicator_name);
						}
					}
					// System.out
					// .println("just added the final name and it looks like--->"
					// + indicator_name
					// + "_"
					// + period_name
					// + "_"
					// + subindicator.getSubIndicator_name());
				}
			}

			/***
			 * covers condition where there are no period names, eg, open, high,
			 * low close, volume.
			 */
			if (period_names.size() == 0) {
				// System.out.println("periods size is 0");
				// System.out.println("press to continue");
				// System.in.read();
				for (String subindicator_name : subindicators_applicable_to_indicator) {
					if (changed_subindicator_names.contains(subindicator_name)) {
						pairs.add(indicator_name + "_"
								+ StockConstants.name_to_use_when_no_periods
								+ "_" + subindicator_name);
					} else {
						// in case the provided changed subindicator list is
						// completely
						// empty,
						// we add the all the applicable subindicators.
						if (changed_subindicator_names.size() == 0) {
							pairs.add(indicator_name
									+ "_"
									+ StockConstants.name_to_use_when_no_periods
									+ "_" + subindicator_name);
						}
					}
					// System.out.println("in this case we just add the old name--->");
					// System.out.println(indicator_name + "_"
					// + subindicator.getSubIndicator_name());
				}
			}
			return pairs;
		} catch (Exception e) {
			LoggingDailyOperations.get_instance().getIndicator_operations()
					.get(es_indicator.getIndicator_name()).addExceptions(e);
			return new ArrayList<String>();
		}
	}

	/*
	 * public HashMap<String, Indicator> create_changed_indicators_map() {
	 * HashMap<String, Indicator> hm = new HashMap<String, Indicator>();
	 * 
	 * for (Indicator indicator : getChanged_indicators()) {
	 * hm.put(indicator.getIndicator_name(), indicator); } return hm;
	 * 
	 * }
	 * 
	 * public HashMap<String, SubIndicator> create_changed_subindicators_map() {
	 * HashMap<String, SubIndicator> hm = new HashMap<String, SubIndicator>();
	 * 
	 * for (SubIndicator subindicator : getChanged_subindicators()) {
	 * hm.put(subindicator.getSubIndicator_name(), subindicator); } return hm;
	 * 
	 * }
	 */

	public ArrayList<String> combined_indicator_subindicator_names_for_changed_indicator(
			Vertex v, ArrayList<String> changed_subindicator_names)
			throws IOException {
		ArrayList<String> pairs = new ArrayList<String>();
		Indicator indicator = getChanged_indicators_map().get(
				v.getProperty("indicator_name"));
		String indicator_name = indicator.getIndicator_name();

		// System.out.println("starting to add the pairs.");
		// System.out.println("the indicator name is--->" + indicator_name +
		// "press to continue");
		// System.in.read();
		/**
		 * what if there is no two period modulateros, in that case we would get
		 * nothing, however we need a seperate indicator for each of the
		 * individual periods even if there is only one period modulator.
		 * 
		 */
		ArrayList<ArrayList<Integer>> periods = indicator.getPeriods();
		// System.out.println("the current periods look like this---->"
		// + periods.toString());

		// System.in.read();

		ArrayList<String> period_names = new ArrayList<String>();
		Integer outer_counter = 0;
		for (ArrayList<Integer> list_a : periods) {
			// System.out.println("outer list is --->" + list_a.toString() +
			// "outer counter is=-=>"
			// + outer_counter);
			// System.out.println("press to continue");
			// System.in.read();
			Integer inner_counter = 0;
			for (ArrayList<Integer> list_b : periods) {
				// System.out.println("inner list is--->" + list_b.toString() +
				// "inner counter is" +
				// inner_counter);
				if (outer_counter < inner_counter) {
					for (Integer list_a_interger : list_a) {
						for (Integer list_b_integer : list_b) {
							period_names.add("period_start_"
									+ String.valueOf(list_a_interger) + "_"
									+ String.valueOf(list_b_integer)
									+ "_period_end");
							// System.out.println("now added to period_names--->"
							// +
							// String.valueOf(list_a_interger)
							// + "_day_" + String.valueOf(list_b_integer) +
							// "_day");
							// System.out.println("press to continue");
							// System.in.read();

						}
					}
				} else {
					// now mix the two lists.

				}

				inner_counter++;
			}
			outer_counter++;
		}

		if (period_names.isEmpty()) {
			// in this case we have to take each thing in the periods array ,
			// and make one
			// name out of it.
			// this is the case where there is only one determinant in the
			// periods.
			// System.out.println("period names was empty--->");

			if (periods.size() == 1) {
				// System.out.println("periods size is 1");
				for (Integer period : periods.get(0)) {
					period_names.add("period_start_" + String.valueOf(period)
							+ "_period_end");
				}
				// System.out
				// .println("period names after adding for size 1 is0-->");
				// System.out.println(period_names.toString());
				// System.out.println("press to continue");
				// System.in.read();
			}
		}

		for (String period_name : period_names) {
			for (SubIndicator subindicator : indicator
					.getSubindicators_applicable_to_this_indicator()) {
				if (changed_subindicator_names.contains(subindicator
						.getSubIndicator_name())) {
					pairs.add(indicator_name + "_" + period_name + "_"
							+ subindicator.getSubIndicator_name());

				} else {
					// in case the provided changed subindicator list is
					// completely
					// empty,
					// we add the all the applicable subindicators.
					if (changed_subindicator_names.size() == 0) {
						pairs.add(indicator_name + "_" + period_name + "_"
								+ subindicator.getSubIndicator_name());
					}
				}
				// System.out
				// .println("just added the final name and it looks like--->"
				// + indicator_name
				// + "_"
				// + period_name
				// + "_"
				// + subindicator.getSubIndicator_name());
			}
		}

		/***
		 * covers condition where there are no period names, eg, open, high, low
		 * close, volume.
		 */
		if (period_names.size() == 0) {
			// System.out.println("periods size is 0");
			// System.out.println("press to continue");
			// System.in.read();
			for (SubIndicator subindicator : indicator
					.getSubindicators_applicable_to_this_indicator()) {
				if (changed_subindicator_names.contains(subindicator
						.getSubIndicator_name())) {
					pairs.add(indicator_name + "_period_start_null_period_end_"
							+ subindicator.getSubIndicator_name());
				} else {
					// in case the provided changed subindicator list is
					// completely
					// empty,
					// we add the all the applicable subindicators.
					if (changed_subindicator_names.size() == 0) {
						pairs.add(indicator_name
								+ "_period_start_null_period_end_"
								+ subindicator.getSubIndicator_name());
					}
				}
				// System.out.println("in this case we just add the old name--->");
				// System.out.println(indicator_name + "_"
				// + subindicator.getSubIndicator_name());
			}
		}
		return pairs;
	}

	public void process_hashmap_new(HashMap<EsIndicator, ArrayList<String>> hm) {
		for (Map.Entry<EsIndicator, ArrayList<String>> entry : hm.entrySet()) {
			LoggingDailyOperations.get_instance().getIndicator_operations()
					.get(entry.getKey().getIndicator_name())
					.setStarted_processing_for_this_indicator(true);

			// now create the combined indicator subindicator names for this
			// indicator.

			// then get stocks which are applicable to it and proceed.

			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(entry.getKey().getIndicator_name())
					.setFinished_processing_for_this_indicator_without_error(
							true);
		}
	}

	/**
	 * hm -> key: indicator vertex -> value: arraylist of applicable
	 * subindicator names.
	 * 
	 * 
	 * 
	 * @param hm
	 */
	public void process_hashmap(HashMap<Vertex, ArrayList<String>> hm) {
		Integer total_indicators = hm.size();
		Integer counter = 0;
		for (Map.Entry<Vertex, ArrayList<String>> entry : hm.entrySet()) {
			try {
				// System.out.println("this is the entry key--->"
				// + entry.getKey().toString());
				// System.out.println("this is the entry value--->"
				// + entry.getValue().toString());
				LoggingDailyOperations.get_instance().getIndicator_operations()
						.get(entry.getKey().getProperty("indicator_name"))
						.setStarted_processing_for_this_indicator(true);
				get_stocks_list_which_have_same_ohlcv_type_as_changed_indicators(
						entry.getKey(), entry.getValue(), total_indicators,
						counter);
				LoggingDailyOperations
						.get_instance()
						.getIndicator_operations()
						.get(entry.getKey().getProperty("indicator_name"))
						.setFinished_processing_for_this_indicator_without_error(
								true);

			} catch (Exception e) {
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e, identifier_for_log, null);
				LoggingDailyOperations
						.get_instance()
						.getIndicator_operations()
						.get((String) entry.getKey().getProperty(
								"indicator_name"))
						.setFailed_in_any_respect(true);

				LoggingDailyOperations
						.get_instance()
						.getIndicator_operations()
						.get((String) entry.getKey().getProperty(
								"indicator_name")).addExceptions(e);
				LoggingDailyOperations.get_instance()
						.exit_and_log_before_exiting(g);
				// TODO Auto-generated catch block
				/*
				 * System.out.println("was processing indicator--->" +
				 * entry.getKey().getProperty("indicator_name")); System.out
				 * .println("was processing arraylist of subindicator names--->"
				 * + entry.getValue().toString()); e.printStackTrace();
				 * System.exit(1);
				 */
			}
			counter++;
			// break;

		}

	}

	public void modify_combined_vertex_arraylist_to_add_es_object_ids() {
		LoggingDailyOperations
				.get_instance()
				.add_to_log("info", null, identifier_for_log,
						"now entering to modify the combined vertex objects list, to add the es ids.");

		ListIterator<CombinedVertexObject> loc = getCombined_vertex_object_arraylist()
				.listIterator();
		Integer counter = 0;
		if (getCombined_vertex_object_arraylist().size() != getIds_of_es_complex_docs()
				.size()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception("there is a mismatch between the es ids and"
							+ " the combined vertex"
							+ " objects arraylist sizes"
							+ getCombined_vertex_object_arraylist().size()
							+ " and this is the es ids size:"
							+ getIds_of_es_complex_docs().size()),
					identifier_for_log, null);
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}
		while (loc.hasNext()) {
			CombinedVertexObject cob = loc.next();
			cob.setEs_object_id(getIds_of_es_complex_docs().get(counter));
			counter++;
		}

		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"finished modifying the combined vertex object arraylist");

	}

	/***
	 * 
	 * IN USE THIS ONE.
	 * 
	 * 
	 * @param changed_indicators
	 */
	public CreateIndicatorSubIndicatorPairsForEachStock(
			ArrayList<EsIndicator> changed_indicators, String indice_name,
			Entity optional_entity) {
		this.indice = indice_name;
		this.e = optional_entity;
		this.bulkloader = new BulkLoader(2000, "tradegenie_titan", "complex");
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		Integer counter = 0;
		for (EsIndicator es_indicator : changed_indicators) {
			// input_map.put(es_indicator, new ArrayList<String>());
			try {
				LoggingDailyOperations.get_instance().add_to_log(
						"info",
						null,
						identifier_for_log,
						"starting to generate pairs for indicator:"
								+ es_indicator.getIndicator_name());
				get_stocks_list_new(es_indicator, new ArrayList<String>(),
						changed_indicators.size(), counter);
				LoggingDailyOperations.get_instance().add_to_log(
						"info",
						null,
						identifier_for_log,
						"finished creating pairs for indicator:"
								+ es_indicator.getIndicator_name());
				if (StockConstants.is_test) {
					/**
					 * remove this break;
					 * 
					 */
					break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				LoggingDailyOperations
						.get_instance()
						.getIndicator_operations()
						.get(es_indicator.getIndicator_name())
						.setFailed_in_processing_indicator_to_generate_pairs(
								true);
				LoggingDailyOperations.get_instance().getIndicator_operations()
						.get(es_indicator.getIndicator_name()).addExceptions(e);
			}
			counter++;
		}
		/**
		 * if (LoggingDailyOperations.get_instance()
		 * .getComplexes_which_failed_to_add_into_es().size() == 0) {
		 * LoggingDailyOperations.get_instance().add_to_log("info", null,
		 * identifier_for_log, "starting to update the redis hashes.");
		 * update_redis_hashes_with_ids_of_opposite_subgroups();
		 * LoggingDailyOperations.get_instance().add_to_log("info", null,
		 * identifier_for_log, "finished to update the redis hashes."); }
		 **/

		// process_hashmap_new(input_map);

	}

	public CreateIndicatorSubIndicatorPairsForEachStock(TitanGraph g,
			ArrayList<Indicator> changed_indicators,
			ArrayList<SubIndicator> changed_subindicators) {

		setBrq(LocalEs.getInstance().prepareBulk());
		this.bulkloader = new BulkLoader(2000, "tradegenie_titan", "complex");
		// System.out.println("these are the changed indicators coming in.");
		// System.out.println(changed_indicators.toString());
		// try {
		// System.in.read();
		// } catch (IOException e1) {
		// TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		this.gson = new GsonBuilder().setPrettyPrinting().create();
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"entered to create the indicator subindicator stock pairs.");

		setChanged_subindicators(changed_subindicators);
		setChanged_indicators(changed_indicators);
		setG(g);
		setCombined_vertex_object_arraylist(new ArrayList<CombinedVertexObject>());
		setSubindicator_vertices_hashmap(new HashMap<String, Vertex>());

		LoggingDailyOperations
				.get_instance()
				.add_to_log(
						"info",
						null,
						identifier_for_log,
						"getting the subindicator"
								+ "and indicator vertices fo the changed indicators or subindicators.");

		setChanged_SubIndicators_vertices(GroovyStatements.getInstance()
				.get_vertices_with_these_names(
						get_changed_subindicators_names(), "subindicator_name"));
		setChanged_indicators_vertices(GroovyStatements.getInstance()
				.get_vertices_with_these_names(get_changed_indicators_names(),
						"indicator_name"));

		if ((getChanged_indicators_vertices().size() != getChanged_indicators()
				.size())
				|| getChanged_subindicators_vertices().size() != getChanged_subindicators()
						.size()) {
			LoggingDailyOperations
					.get_instance()
					.add_to_log(
							"exception",
							new Exception(
									"did not find all the subindicator vertices or indicator vertices"),
							identifier_for_log, null);
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}

		// System.out.println("the changed subindicator vertices are--->");
		// System.out.println(getChanged_subindicators_vertices().toString());

		// System.out.println("the changed indicator vertices are--->");
		// System.out.println(getChanged_indicators_vertices().toString());

		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"got all the indicator and subindicator vertices");

		// first make one hashmap of all the indicators and keep empty arraylist
		// so that it adds all
		// the subindicators for these changed indicators.
		HashMap<Vertex, ArrayList<String>> hm_all_indicators = new HashMap<Vertex, ArrayList<String>>();
		for (Vertex indicator_vertex : getChanged_indicators_vertices()) {
			hm_all_indicators.put(indicator_vertex, new ArrayList<String>());
		}

		System.out
				.println("now processing all the indicators which have changed");
		LoggingDailyOperations.get_instance().add_to_log("info", null,
				identifier_for_log,
				"now processing the hashmap of all the changed indicators.");
		process_hashmap(hm_all_indicators);

		// do this for any remaining documents which are less than 2000.
		index_documents_and_flush_bulk();
		/**
		 * now adding all the pairs to titan multithreaded.
		 * 
		 * 
		 */

		if (!LoggingDailyOperations.get_instance().has_exception()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"total complexes to be created:"
							+ getCombined_vertex_object_arraylist().size());
			modify_combined_vertex_arraylist_to_add_es_object_ids();
			ExecuteMultiThreadedJob ex = new ExecuteMultiThreadedJob(
					getCombined_vertex_object_arraylist(), g,
					g.getManagementSystem(), "add_combined_vertices", false,
					new ArrayList<org.json.JSONObject>());
			ArrayList<TItanWorkerThreadResultObject> results = ex.getRes();
			if (results != null) {
				for (TItanWorkerThreadResultObject result : results) {
					if (!(result.get_failed_object().length() == 0)) {
						// remove this from elasticsearch.
						try {
							Exception e = (Exception) result
									.get_failed_object().get("exception");
							LoggingDailyOperations.get_instance().add_to_log(
									"exception", e, identifier_for_log, null);
							LoggingDailyOperations.get_instance()
									.addComplex_which_failed_to_add_into_graph(
											(String) result.get_failed_object()
													.get("combined_name"));
						} catch (JSONException e) {
							LoggingDailyOperations.get_instance().add_to_log(
									"exception", e, identifier_for_log, null);
						}

					}
				}
			} else {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"exception",
								new Exception(
										"check the previous exception,"
												+ " there was an error in the multi thread graph add"
												+ " job, so null was returned as the results array."),
								identifier_for_log, null);
			}

		}

		recommit_md5_hash_to_db();
		// now we have to calculate all the newly calculated indicators and
		// subindicators
		// from the start of time till present date.

		// recommit hash string so that we avoid any bullshit.
		// recommit_md5_hash_to_db();

		// now do the multithreaded job.

	}

	public Vertex create_indicator_subindicator_stock_combined_vertex(
			String name, String stockName, String stockFullName, String indice) {
		try {
			Vertex v = g
					.addVertexWithLabel("entity_indicator_subindicator_complex");
			v.setProperty("entity_indicator_subindicator_unique_name", name);
			v.setProperty("entity_indicator_subindicator_stockName_name",
					stockName);
			v.setProperty("entity_indicator_subindicator_stockFullName_name",
					stockFullName);
			v.setProperty("entity_indicator_subindicator_indice_name", indice);
			g.commit();
			return v;
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}

	public Vertex get_subindicator_vertex(String name) {
		ArrayList<Vertex> result = GroovyStatements.getInstance()
				.get_vertices_with_these_names(
						new ArrayList<String>(Arrays.asList(name)),
						"subindicator_name");
		if (result.size() == 0) {
			return null;
		} else {
			return result.get(0);
		}
	}

	public Vertex get_indicator_vertex(String name) {
		Iterator<Vertex> it_v = g.query().has("indicator_name", name)
				.vertices().iterator();
		Vertex indicator_vertex = null;
		while (it_v.hasNext()) {
			indicator_vertex = it_v.next();
		}
		return indicator_vertex;
	}

	public HashMap<Vertex, ArrayList<String>> prepare_subindicators_which_have_changed_for_database_insertion()
			throws SubIndicatorDoesNotHaveAnyIncidentApplicableIndicators {

		HashMap<Vertex, ArrayList<String>> hm = new HashMap<Vertex, ArrayList<String>>();

		for (Vertex subindicator_vertex : getChanged_subindicators_vertices()) {

			String subindicator_name = subindicator_vertex
					.getProperty("subindicator_name");
			Iterator<Edge> applicable_indicator_edges = subindicator_vertex
					.getEdges(Direction.IN, "applicable_to_subindicator")
					.iterator();
			ArrayList<Vertex> applicable_indicator_vertices = new ArrayList<Vertex>();
			while (applicable_indicator_edges.hasNext()) {
				Edge e = applicable_indicator_edges.next();
				applicable_indicator_vertices.add(e.getVertex(Direction.OUT));
			}

			// if the array is empty, throw eerror.

			if (applicable_indicator_vertices.isEmpty()) {
				throw new SubIndicatorDoesNotHaveAnyIncidentApplicableIndicators(
						"was preparing this subindicator"
								+ subindicator_vertex
										.getProperty("subindicator_name")
								+ ""
								+ " to send for creation of the "
								+ "indicator subindicator pairs, but it does not have any incident indicator edges");
			}

			for (Vertex indicator_vertex : applicable_indicator_vertices) {
				Boolean vertex_exists_in_hashmap = false;
				for (Vertex v : hm.keySet()) {
					if (v.getProperty("indicator_name").equals(
							indicator_vertex.getProperty("indicator_name"))) {
						// this vertex already exists in the hashmap.
						vertex_exists_in_hashmap = true;
					}

				}

				if (!vertex_exists_in_hashmap) {
					hm.put(indicator_vertex, new ArrayList<String>());
					hm.get(indicator_vertex).add(subindicator_name);
				}

			}

		}

		return hm;

	}

	public HashMap<String, Object> get_stock_details_for_creating_combined_vertex_object(
			String unique_name) {

		HashMap<String, Object> stock_details = new HashMap<String, Object>();
		SearchResponse sr = LocalEs
				.getInstance()
				.prepareSearch("tradegenie_titan")
				.setTypes("entity")
				.setQuery(
						QueryBuilders.filteredQuery(
								QueryBuilders.matchAllQuery(),
								termFilter("unique_name", unique_name)))
				.setSize(1)
				.addFields(
						new String[] { "fullName", "indice", "symbol",
								"products", "industry" }).execute().actionGet();

		for (SearchHit hit : sr.getHits().hits()) {
			stock_details.put("stockFullName", hit.field("fullName").value()
					.toString());
			stock_details.put("stockName", hit.field("symbol").value()
					.toString());
			stock_details.put("indice", hit.field("indice").value().toString());
			
			ArrayList<Object> industry = new ArrayList<Object>();
			ArrayList<Object> products = new ArrayList<Object>();
				
			if(hit.field("industry") != null){
				industry = (ArrayList<Object>) hit.field(
						"industry").values();
			}
			
			if(hit.field("products") != null){
				products = (ArrayList<Object>) hit.field(
						"products").values();
			}
			
			
			
			
			
			Set<String> business_types = new HashSet<String>();
			for (Object o : industry) {
				business_types.add((String) o);
			}
			for (Object o : products) {
				business_types.add((String) o);
			}
			stock_details.put("business_types", new ArrayList<String>(
					business_types));
		}

		return stock_details;
	}

	public ArrayList<String> get_names_of_opposite_subgroups_for_given_subindicator(
			String subindicator, String complex_name_without_subindicator) {
		// String subindicator_hex_id = DigestUtils.sha256Hex(subindicator);
		ArrayList<String> names_of_opposite_subgroup_components = new ArrayList<String>();
		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			String opposite_subgroup_components_of_subindicator = jedis.hget(
					subindicator, "opposite_subgroup_components");
			if (opposite_subgroup_components_of_subindicator == null) {
				// get it from es and store in redis for future lookup.
				SearchResponse sr = LocalEs
						.getInstance()
						.prepareSearch("tradegenie_titan")
						.setTypes("subindicator")
						.setQuery(
								QueryBuilders.filteredQuery(
										QueryBuilders.matchAllQuery(),
										termFilter("subindicator_name",
												subindicator))).setSize(1)
						.setFetchSource(new String[] { "groups" }, null)
						.execute().actionGet();
				for (SearchHit hit : sr.getHits().hits()) {

					ArrayList<HashMap<String, Object>> groups = (ArrayList<HashMap<String, Object>>) hit
							.getSource().get("groups");

					for (HashMap<String, Object> group : groups) {
						ArrayList<HashMap<String, Object>> subgroups = (ArrayList<HashMap<String, Object>>) group
								.get("subgroups");
						for (HashMap<String, Object> subgroup : subgroups) {
							ArrayList<HashMap<String, Object>> opposite_subgroups = (ArrayList<HashMap<String, Object>>) subgroup
									.get("opposite_subgroups");
							for (HashMap<String, Object> opposite_subgroup : opposite_subgroups) {
								ArrayList<String> opposite_subgroup_components = (ArrayList<String>) opposite_subgroup
										.get("opposite_subgroup_components");
								names_of_opposite_subgroup_components
										.addAll(opposite_subgroup_components);
							}
						}
					}
				}
				Set<String> names_uniqued = new HashSet<String>(
						names_of_opposite_subgroup_components);
				names_of_opposite_subgroup_components = new ArrayList<String>(
						names_uniqued);

				jedis.hset(subindicator, "opposite_subgroup_components",
						this.gson.toJson(names_of_opposite_subgroup_components));
			} else {
				names_of_opposite_subgroup_components = this.gson.fromJson(
						opposite_subgroup_components_of_subindicator,
						ArrayList.class);

			}

			ArrayList<String> opposite_complex_full_names = new ArrayList<String>();
			for (String s : names_of_opposite_subgroup_components) {
				opposite_complex_full_names
						.add(complex_name_without_subindicator + "_" + s);
			}

			return opposite_complex_full_names;

		}

	}

	public String assemble_name_of_opposite_subindicator_for_complex(
			String opposite_subgroup_name, String full_complex_name) {
		String name = null;

		return name;
	}

	public void get_stocks_list_new(EsIndicator changed_indicator,
			ArrayList<String> changed_subindicators, Integer total_indicators,
			Integer current_indicator) {
		ArrayList<String> ohlcv_groups_of_indicator = changed_indicator
				.getOhlcv_groups();
		ArrayList<String> unique_names_with_same_ohlcv_groups = new ArrayList<String>();
		//System.out.println("the ohlcvs are:" + ohlcv_groups_of_indicator);
		//System.out.println("e is:" + e);
		
		try {
			SearchResponse sr = LocalEs
					.getInstance()
					.prepareSearch("tradegenie_titan")
					.setTypes("entity")
					.setQuery(
							QueryBuilders.filtered(
									QueryBuilders.termsQuery("ohlcv_type",
											ohlcv_groups_of_indicator),
									FilterBuilders.boolFilter().must(
											FilterBuilders.termFilter("indice",
													this.indice)))

					)
					.setSize(0)
					.addAggregation(
							AggregationBuilders.terms("stocks")
									.field("unique_name").size(300)).execute()
					.actionGet();
			
			

			Terms stocks = sr.getAggregations().get("stocks");
			//System.out.println("the total aggs were:" + stocks.getBuckets().size());
			//System.in.read();
			for (Terms.Bucket entry : stocks.getBuckets()) {
				if (this.e == null) {
					// add the symbol.
					// stockNames_with_same_ohlcv_group.add(entry.getKey());
					
					unique_names_with_same_ohlcv_groups.add(entry.getKey());
				} else {
					if (this.e.generate_unique_name().equals(entry.getKey())) {
						// we should add the
						unique_names_with_same_ohlcv_groups.add(entry.getKey());
					}
				}

			}

		} catch (Exception e) {
			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(changed_indicator.getIndicator_name())
					.setFailed_to_find_the_stock_details_while_complex_generation(
							true);
		}

		LoggingDailyOperations
				.get_instance()
				.getIndicator_operations()
				.get(changed_indicator.getIndicator_name())
				.setStocks_having_same_ohlcv_as_this_indicator(
						unique_names_with_same_ohlcv_groups);

		if (unique_names_with_same_ohlcv_groups.size() == 0) {
			LoggingDailyOperations.get_instance().add_to_log("exception",
					new Exception("not stocks found with this ohlcv group"),
					identifier_for_log, null);

		}

		ArrayList<String> applicable_subindicators_indicator_paris;

		applicable_subindicators_indicator_paris = combined_indicator_subindicator_names_for_changed_indicator_new(
				changed_subindicators, changed_indicator);

		if (applicable_subindicators_indicator_paris.isEmpty()) {
			LoggingDailyOperations
					.get_instance()
					.add_to_log(
							"exception",
							new Exception(
									"no indicator subindicator pairs found for this indicator:"
											+ changed_indicator
													.getIndicator_name()
											+ " check indicator operations, for error"
											+ "or it is possible that nothing was found."),
							identifier_for_log, null);
			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(changed_indicator.getIndicator_name())
					.setFailed_to_generate_the_indicator_subindicator_pairs(
							true);
		} else {
			for (String stockName_with_same_ohlcv_group : unique_names_with_same_ohlcv_groups) {
				for (String combined_name : applicable_subindicators_indicator_paris) {

					HashMap<String, Object> details_of_stock = get_stock_details_for_creating_combined_vertex_object(stockName_with_same_ohlcv_group);
					String required_vertex_name = (String) details_of_stock
							.get("stockName")
							+ "_"
							+ (String) details_of_stock.get("stockFullName")
							+ "_"
							+ (String) details_of_stock.get("indice")
							+ "_" + combined_name;

					String subindicator_name = combined_name
							.substring(combined_name.indexOf("_subindicator") + 1);

					String complex_name_without_subindicator = required_vertex_name
							.substring(0, required_vertex_name
									.indexOf("_subindicator"));

					ArrayList<String> names_of_opposite_subgroups_subindicators = get_names_of_opposite_subgroups_for_given_subindicator(
							subindicator_name,
							complex_name_without_subindicator);
					/*
					 * try (Jedis jedis = RedisManager.getInstance().getJedis())
					 * { jedis.hset( required_vertex_name,
					 * "opposite_subgroup_components", this.gson
					 * .toJson(names_of_opposite_subgroups_subindicators));
					 */
					EsComplex complex = new EsComplex(required_vertex_name,
							(String) details_of_stock.get("stockName"),
							(String) details_of_stock.get("stockFullName"),
							(String) details_of_stock.get("indice"),
							changed_indicator.getIndicator_name(),
							subindicator_name,
							(ArrayList<String>) details_of_stock
									.get("business_types"));

					ObjectMapper m = new ObjectMapper();
					Map<String, Object> item_source = m.convertValue(complex,
							Map.class);
					Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = this.bulkloader
							.add_item_to_bulk_loader(item_source);
					if (response != null) {
						process_response(response,new ArrayList<Object>());
					}

					// }

				}

				if (StockConstants.is_test) {
					/**
					 * REMOVE THIS BREAk;
					 * 
					 * 
					 */
					break;
				}

			}

			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response = this.bulkloader
					.index_documents_and_flush_bulk();
			if (response != null) {
				process_response(response,new ArrayList<Object>());
			}
		}

	}

	public void update_redis_hashes_with_ids_of_opposite_subgroups() {

		try (Jedis jedis = RedisManager.getInstance().getJedis()) {
			ScanParams params = new ScanParams();
			params.count(20000);
			Integer cursor = 100;
			Integer scan_counter = 0;
			while (cursor != 0) {
				LoggingDailyOperations
						.get_instance()
						.add_to_log(
								"info",
								null,
								identifier_for_log,
								"now finished redis update  :"
										+ scan_counter
										+ " of total of:"
										+ LoggingDailyOperations.get_instance().total_complexes_added);

				ScanResult<String> results = jedis.scan(cursor, params);
				cursor = results.getCursor();
				ArrayList<Response<String>> components = new ArrayList<Response<String>>();
				ArrayList<String> valid_key_names = new ArrayList<String>();
				Pipeline pipe = jedis.pipelined();
				for (String key_name : results.getResult()) {
					/**
					 * make sure that the key is not a
					 * subindicator_indicator_complex that it is not a ohlcv
					 * sorted set and that it is not the pair_es_ids hash.
					 */
					if (!key_name.startsWith("subindicator")
							&& !key_name.startsWith("stock_")
							&& key_name != "pair_es_ids") {
						Response<String> opposite_subgroup_components = pipe
								.hget(key_name, "opposite_subgroup_components");
						components.add(opposite_subgroup_components);
						valid_key_names.add(key_name);
					}
				}
				pipe.sync();

				Integer counter = 0;
				// Pipeline pipe_two = jedis.pipelined();
				HashMap<String, String> hm_for_pipe = new HashMap<String, String>();
				for (Response<String> component_string : components) {

					ArrayList<String> opposite_subgroup_names = this.gson
							.fromJson(component_string.get(), ArrayList.class);

					if (opposite_subgroup_names == null) {
						System.out.println("this complex name came as null:"
								+ valid_key_names.get(counter));
						System.in.read();
					}

					ArrayList<String> opposite_subgroup_es_ids = new ArrayList<String>();
					for (String opposite_subgroup_complex : opposite_subgroup_names) {
						// get the ids and add the recreation to a pipe.
						// System.out.println(opposite_subgroup_complex);

						String es_id_of_opposite_subgroup = jedis.hget(
								opposite_subgroup_complex, "es_id");
						// System.out.println(es_id_of_opposite_subgroup);
						opposite_subgroup_es_ids
								.add(es_id_of_opposite_subgroup);
					}
					String opposite_subgroup_ids_gsonified = this.gson
							.toJson(opposite_subgroup_es_ids);
					hm_for_pipe.put(valid_key_names.get(counter),
							opposite_subgroup_ids_gsonified);

					counter++;
				}
				Pipeline pipe_two = jedis.pipelined();
				for (Map.Entry<String, String> en : hm_for_pipe.entrySet()) {
					pipe_two.hset(en.getKey(),
							"opposite_subgroup_component_ids", en.getValue());
				}
				pipe_two.sync();
				scan_counter += 20000;
			}

		} catch (Exception e) {
			LoggingDailyOperations.get_instance().add_to_log("exception", e,
					identifier_for_log, null);
		}

	}

	/**
	 * 
	 * if we want to add new indicators. then send in each new indicator, with
	 * an empty arraylist for changed subindicators. this will make it for all
	 * applicable subindicators for that indicator.
	 * 
	 * 
	 * reconstruct things as follows:
	 * 
	 * take subindicator make
	 * 
	 * 
	 * 
	 * @param changed_indicator
	 * @param changed_subindicators
	 * @throws CouldNotAddCombinedIndicatorSubIndicatorStockPair
	 * @throws CouldNotFindSubIndicatorVertexWhileTryingToMakeCombinedPair
	 * @throws IOException
	 */
	public void get_stocks_list_which_have_same_ohlcv_type_as_changed_indicators(
			Vertex changed_indicator, ArrayList<String> changed_subindicators,
			Integer total_indicators, Integer current_indicator)
			throws Exception {

		ArrayList<String> ohlcv_groups_of_indicator = changed_indicator
				.getProperty("ohlcv_groups");
		// System.out.println("ohlcv group of indicator--->"
		// + ohlcv_groups_of_indicator);

		if (ohlcv_groups_of_indicator == null) {
			LoggingDailyOperations.get_instance().getIndicator_operations()
					.get(changed_indicator.getProperty("indicator_name"))
					.setFailed_in_any_respect(true);

			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(changed_indicator.getProperty("indicator_name"))
					.addFailed_to_add_what(
							"no ohlcv groups"
									+ "for this indicator, while trying to create its complexes.");

			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception("failed while trying to add complexes,"
							+ " no ohlcv group found for"
							+ " indicator for whom we are trying"),
					identifier_for_log, null);

		}

		LoggingDailyOperations.get_instance().add_to_log(
				"info",
				null,
				identifier_for_log,
				"entered ohlcv group exists and is neither empty nor null for this indicator:"
						+ changed_indicator.getProperty("indicator_name"));

		Set<Vertex> stocks = GroovyStatements.getInstance()
				.get_all_stocks_with_given_ohlcv_groups(
						ohlcv_groups_of_indicator);

		if (stocks.size() == 0) {
			throw new Exception(
					"no stocks found with the ohlcv groups of this indicator,"
							+ ohlcv_groups_of_indicator.toString()
							+ " indicator name is :"
							+ changed_indicator.getProperty("indicator_name"));

		} else {
			ArrayList<String> stockNames_with_same_ohlcv_group = new ArrayList<String>();
			Iterator iter = stocks.iterator();
			while (iter.hasNext()) {
				Vertex vv = (Vertex) iter.next();
				stockNames_with_same_ohlcv_group.add((String) vv
						.getProperty("stockName"));
			}

			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(changed_indicator.getProperty("indicator_name"))
					.setStocks_having_same_ohlcv_as_this_indicator(
							stockNames_with_same_ohlcv_group);
		}

		// System.out.println("now doing indicator:" +
		// changed_indicator.getProperty("indicator_name"));
		// System.out.println("stocks with this ohlcv group---->"
		// + stocks.toString());

		// get all the vertices of the subindicators which are applicable to
		// this indicator.
		// then get the names of all those subindicators.
		// get all the subindicators to which this indicator is applicable and
		// create the name
		// pairs.

		ArrayList<String> applicable_subindicators_indicator_paris = combined_indicator_subindicator_names_for_changed_indicator(
				changed_indicator, changed_subindicators);
		// System.out.println("applicable subindicator and indicator pairs.--->");
		// System.out.println(applicable_subindicators_indicator_paris.toString());

		if (applicable_subindicators_indicator_paris.isEmpty()) {
			throw new Exception("no subindicator indicator"
					+ " pairs could be created for this indicator."
					+ changed_indicator.getProperty("indicator_name"));
		}

		// System.in.read();
		/**
		 * CAUTION FOR DEMO ONLY, SLICING STOCK TO CONTAIN 1 STOCK, TO TEST BULK
		 * ES LOADING.
		 * 
		 * APPLY FILTER TO REMOVE ALL STOCKS EXCEPT THE ONE STOCK WHICH IS NEW,
		 * IN CASE WE ARE INVOKING THE API TO ADD COMPLEXES FOR NEWER STOCKS.
		 * 
		 * 
		 */
		// System.out.println("now processing each of the stocks of this ohlcv grou.");
		Integer total_stocks = stocks.size();
		Integer current_stock = 0;
		for (Vertex stock : stocks) {
			// /we create a vertex with the name, fullname, index name and
			// /assemble the required vertex name, which is the
			// stockName_fullname_indicename_combined_indicator_subindicator_name

			for (String combined_name : applicable_subindicators_indicator_paris) {

				// /
				// System.out.println("now doing stock, and this stock will done for each combined name.--->"
				// + stock.getProperty("stockName"));
				String required_vertex_name = stock.getProperty("stockName")
						+ "_" + stock.getProperty("stockFullName") + "_"
						+ stock.getProperty("indice") + "_" + combined_name;
				// System.out.println("required vertex name is--->"
				// + required_vertex_name);
				// get the subindicator vertex.

				Vertex subindicator_vertex = null;

				String subindicator_name = combined_name
						.substring(combined_name.indexOf("_subindicator") + 1);

				if (getSubindicator_vertices_hashmap().containsKey(
						subindicator_name)) {
					subindicator_vertex = getSubindicator_vertices_hashmap()
							.get(subindicator_name);
				} else {
					subindicator_vertex = get_subindicator_vertex(subindicator_name);
					getSubindicator_vertices_hashmap().put(subindicator_name,
							subindicator_vertex);
				}

				// draw the parent indicator and parent subindicator edges.

				if (subindicator_vertex == null) {
					throw new CouldNotFindSubIndicatorVertexWhileTryingToMakeCombinedPair(
							"this was the required vertex name:"
									+ required_vertex_name
									+ " and this is the subindicator name extracted by regexing the"
									+ " combined name(indicator + subindicator)"
									+ combined_name);
				}

				// System.out
				// .println("subindicator vertex by string substituion is---->"
				// + subindicator_vertex.toString());

				ArrayList<Vertex> it_v = GroovyStatements.getInstance()
						.get_vertices_with_these_names(
								new ArrayList<String>(
										Arrays.asList(required_vertex_name)),
								"entity_indicator_subindicator_unique_name");

				if (it_v.size() > 0) {
					System.out.println("the combined vertex arlready exists.");
					LoggingDailyOperations.get_instance().add_to_log(
							"info",
							null,
							identifier_for_log,
							"this complex is already present in the graph:"
									+ required_vertex_name);
					LoggingDailyOperations
							.get_instance()
							.getIndicator_operations()
							.get(changed_indicator
									.getProperty("indicator_name"))
							.addComplexes_already_present(required_vertex_name);
				} else {

					CombinedVertexObject combined_vertex_object = new CombinedVertexObject(
							required_vertex_name,
							(String) stock.getProperty("stockName"),
							(String) stock.getProperty("stockFullName"),
							(String) stock.getProperty("indice"),
							(Long) changed_indicator.getId(),
							(Long) subindicator_vertex.getId());
					// System.out
					// .println("currently not adding object to save memory.");
					getCombined_vertex_object_arraylist().add(
							combined_vertex_object);

					create_mapping_for_elasticsearch_aggregations(
							subindicator_vertex, required_vertex_name,
							(String) stock.getProperty("stockName"),
							(String) stock.getProperty("stockFullName"),
							(String) stock.getProperty("indice"),
							(String) changed_indicator
									.getProperty("indicator_name"));

				}

			}
			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(changed_indicator.getProperty("indicator_name"))
					.addStocks_processed_for_complex_creation_without_error(
							(String) stock.getProperty("stockName"));
			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					identifier_for_log,
					"now finished stock number:" + current_stock
							+ " of total :" + total_stocks
							+ " and this is indicator:" + current_indicator
							+ " of total indicators :" + total_indicators);

			current_stock++;
		}

	}

	// get a list of stocks which have the same ohlcv group.
	// update all its pairs, if they dont exist, with the new combinations,
	// after that get all the subindicators which have changed, by now there
	// will already be the
	// pairs created for each company, so we just have to update those which
	// connect to this subindicator as a parent subindciator.

	public HashMap<String, Object> derive_indicator_and_periods_from_combined_vertex_name(
			String combined_vertex_name) {

		HashMap<String, Object> derivation = new HashMap<String, Object>();

		return derivation;

	}

	/**
	 * @param combination
	 *            - a jsonified string.
	 * @param combined_vertex_name
	 *            - the combined name of stock + ind + subind
	 * @return
	 */
	public String get_combination_index(String combination,
			String combined_vertex_name) {

		String hash = DigestUtils.md5Hex(combination.toString());

		String new_counter_for_this_hash = GroovyStatements.getInstance()
				.increment_and_get_registry_hash_counter(hash, combination);

		if (new_counter_for_this_hash == null) {
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}

		combined_vertex_name = combined_vertex_name.substring(0,
				combined_vertex_name.lastIndexOf("_period_end") + 11);

		String name_to_return = combined_vertex_name + "_"
				+ new_counter_for_this_hash;

		return name_to_return;

	}

	public String get_component_code_for_specific_opposite_indicator(
			String direct_opposite_subindicator_name,
			String combined_vertex_name) {

		String stub = combined_vertex_name.substring(0,
				combined_vertex_name.lastIndexOf("_period_end") + 11);
		return stub + "_" + direct_opposite_subindicator_name;

	}

	public void create_mapping_for_elasticsearch_aggregations(
			Vertex subindicator_vertex, String combined_vertex_name,
			String stockName, String stockFullName, String indice,
			String indicator_name) {

		SubIndicator subindicator = new SubIndicator(
				(String) subindicator_vertex.getProperty("subindicator_name"));

		if (getAssembled_subindicators().containsKey(
				subindicator.getSubIndicator_name())) {

			subindicator = getAssembled_subindicators().get(
					subindicator.getSubIndicator_name());

		} else {

			subindicator = subindicator.assemble_existing_information(getG());
			getAssembled_subindicators().put(
					subindicator.getSubIndicator_name(), subindicator);
		}

		JSONObject elastic_object = new JSONObject();

		JSONArray groups = new JSONArray();
		JSONArray subgroups = new JSONArray();
		JSONArray sectors = new JSONArray();
		JSONArray industries = new JSONArray();
		JSONArray subgroups_opposite_subgroups = new JSONArray();
		JSONArray groups_opposite_groups = new JSONArray();
		JSONArray groups_subgroups = new JSONArray();
		JSONArray specific_opposite_indicators = new JSONArray();
		Integer full_time_employees = 0;

		Integer group_counter = 0;
		// System.out.println("these are the self groups of the group-->");
		// System.out.println(subindicator.getSelf_groups());

		for (ArrayList<String> group : subindicator.getSelf_groups()) {

			String registry_index_for_this_group = get_combination_index(
					group.toString(), combined_vertex_name);
			// System.out.println("the registry index for the group is-->");
			// System.out.println(registry_index_for_this_group);

			groups.add(registry_index_for_this_group);
			/**
			 * 
			 * now doing opposite groups to group.
			 */

			if (subindicator.getOpposite_groups().size() > 0) {
				ArrayList<ArrayList<String>> opposite_groups_to_this_group = subindicator
						.getOpposite_groups().get(group_counter);

				JSONArray opposite_group_numbers = new JSONArray();
				for (ArrayList<String> opposite_group : opposite_groups_to_this_group) {
					String registry_number_of_opposite_group = get_combination_index(
							opposite_group.toString(), combined_vertex_name);
					opposite_group_numbers
							.add(registry_number_of_opposite_group);

				}

				JSONObject group_and_its_opposites = new JSONObject();
				group_and_its_opposites.put("group_name",
						registry_index_for_this_group);
				group_and_its_opposites.put("opposite_groups",
						opposite_group_numbers);

				groups_opposite_groups.add(group_and_its_opposites);
			}

			/**
			 * now doing subgroups and opposite groups.
			 * 
			 */
			if (subindicator.getSelf_sub_groups().size() > 0) {

				ArrayList<ArrayList<String>> subgroups_of_this_group = subindicator
						.getSelf_sub_groups().get(group_counter);
				Integer subgroup_counter = 0;

				for (ArrayList<String> subgroup : subgroups_of_this_group) {
					String subgroup_registry_index = get_combination_index(
							subgroup.toString(), combined_vertex_name);
					subgroups.add(subgroup_registry_index);
					// System.out.println(subgroup);
					if (subindicator.getOpposite_subgroups().size() >= group_counter + 1) {

						if (subindicator.getOpposite_subgroups()
								.get(group_counter).size() >= subgroup_counter + 1) {
							ArrayList<ArrayList<String>> opposite_groups_of_this_subgroup = subindicator
									.getOpposite_subgroups().get(group_counter)
									.get(subgroup_counter);

							Integer opposite_group_counter = 0;

							JSONArray opposite_groups_to_this_subgroup = new JSONArray();

							for (ArrayList<String> opposite_subgroup : opposite_groups_of_this_subgroup) {
								// System.out.println(opposite_subgroup);
								String opposite_group_registry_index = get_combination_index(
										opposite_subgroup.toString(),
										combined_vertex_name);

								// System.out.println("opposite specific groups.");
								// System.out.println(subindicator
								// .getOpposite_specific_groups());
								// System.out.println("group ==>" +
								// group_counter);
								// System.out.println("subgroup-==>"
								// + subgroup_counter);
								// System.out.println("opposite_group---->"
								// + opposite_group_counter);

								String direct_opposite_indicator = subindicator
										.getOpposite_specific_groups()
										.get(group_counter)
										.get(subgroup_counter)
										.get(opposite_group_counter);
								// String code_of_this_component =
								// get_combination_index(
								// direct_opposite_indicator,
								// combined_vertex_name);

								String code_of_this_component = get_component_code_for_specific_opposite_indicator(
										direct_opposite_indicator,
										combined_vertex_name);

								specific_opposite_indicators
										.add(code_of_this_component);

								JSONObject opposite_subgroup_with_specific_subindicator = new JSONObject();
								opposite_subgroup_with_specific_subindicator
										.put("opposite_subgroup",
												opposite_group_registry_index);
								opposite_subgroup_with_specific_subindicator
										.put("specific_opposite_subindicator",
												code_of_this_component);

								opposite_groups_to_this_subgroup
										.add(opposite_subgroup_with_specific_subindicator);

								opposite_group_counter++;
							}

							JSONObject subgroup_and_its_opposites = new JSONObject();
							subgroup_and_its_opposites.put("subgroup_name",
									subgroup_registry_index);
							subgroup_and_its_opposites.put(
									"opposite_subgroups",
									opposite_groups_to_this_subgroup);

							subgroups_opposite_subgroups
									.add(subgroup_and_its_opposites);
						}

					}

					subgroup_counter++;
				}

				JSONObject group_and_its_subgroups = new JSONObject();
				group_and_its_subgroups.put("group_name",
						registry_index_for_this_group);
				group_and_its_subgroups.put("subgroups", subgroups);

				groups_subgroups.add(group_and_its_subgroups);

			} else {

			}

			group_counter++;

		}

		if (groups.isEmpty()) {
			LoggingDailyOperations.get_instance().add_to_log(
					"exception",
					new Exception("no groups found for this complex:"
							+ combined_vertex_name), identifier_for_log, null);
			LoggingDailyOperations.get_instance().getIndicator_operations()
					.get(indicator_name).setFailed_in_any_respect(true);
			LoggingDailyOperations
					.get_instance()
					.getIndicator_operations()
					.get(indicator_name)
					.addFailed_to_add_what(
							"no group found for this complex of this indicator.:"
									+ combined_vertex_name);
			LoggingDailyOperations.get_instance()
					.exit_and_log_before_exiting(g);
		}
		elastic_object.put("groups", groups);
		elastic_object.put("subgroups", subgroups);
		elastic_object.put("subgroups_opposite_subgroups",
				subgroups_opposite_subgroups);
		elastic_object.put("groups_opposite_groups", groups_opposite_groups);
		elastic_object.put("groups_subgroups", groups_subgroups);
		elastic_object.put("specific_opposite_indicators",
				specific_opposite_indicators);
		elastic_object.put("self_code", combined_vertex_name);

		/**
		 * 
		 * now adding stockname, stockfullname, and indice, and periods and
		 * indicator name and subindicator name
		 */

		elastic_object.put("stockName", stockName);
		elastic_object.put("stockFullName", stockFullName);
		elastic_object.put("subindicator_name",
				subindicator.getSubIndicator_name());
		elastic_object.put("indicator_name", indicator_name);
		elastic_object.put("indice", indice);

		// *** get the relevant periods and add them to an arraylist of period
		ArrayList<Integer> periods_f = new ArrayList<Integer>();
		String period_holder = combined_vertex_name.substring(
				combined_vertex_name.lastIndexOf("period_start_") + 12,
				combined_vertex_name.lastIndexOf("period_end"));
		String[] periods = period_holder.split("_");
		for (int i = 0; i < periods.length; i++) {
			if (periods[i].isEmpty()) {

			} else if (periods[i].equals("null")) {

			} else {
				try {
					periods_f.add(Integer.parseInt(periods[i]));
				} catch (NumberFormatException e) {
					LoggingDailyOperations.get_instance().add_to_log(
							"exception", e, identifier_for_log, null);
					LoggingDailyOperations.get_instance()
							.getIndicator_operations().get(indicator_name)
							.setFailed_in_any_respect(true);

					LoggingDailyOperations
							.get_instance()
							.getIndicator_operations()
							.get(indicator_name)
							.addFailed_to_add_what(
									"failed to derive the period from the combined vertex name.");

					LoggingDailyOperations.get_instance()
							.exit_and_log_before_exiting(g);
				}
			}
		}

		elastic_object.put("periods", periods_f);
		elastic_object.put("combined_vertex_name", combined_vertex_name);
		// elastic_object.put("indicator_name", value);

		/**
		 * now adding sectors industry and pointers.
		 * 
		 * 
		 */

		Vertex company_information_vertex = (Vertex) g.query()
				.has("stockName", stockName)
				.has("stockFullName", stockFullName).has("tableName")
				.vertices().iterator().next();

		// TODO
		// ArrayList<String> possible_pointers =
		// company_information_vertex.getProperty("pointers");

		String sector = (String) company_information_vertex
				.getProperty("sector");
		String industry = (String) company_information_vertex
				.getProperty("industry");

		if (sector != null) {
			String[] sectors_string_array = return_components_sector_industry(sector);
			for (int i = 0; i < sectors_string_array.length; i++) {
				sectors.add(sectors_string_array[i]);
			}
			elastic_object.put("sectors", sectors);
		}

		if (industry != null) {
			String[] industries_string_array = return_components_sector_industry(industry);

			for (int i = 0; i < industries_string_array.length; i++) {
				industries.add(industries_string_array[i]);
			}
			elastic_object.put("industries", industries);
		}

		full_time_employees = (Integer) company_information_vertex
				.getProperty("full_time_employees");
		if (full_time_employees != null) {
			elastic_object.put("employees", full_time_employees);
		}

		elastic_object.put("day_ids", new JSONArray());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		// System.out.println(gson.toJson(elastic_object));
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		/*
		 * getBuilder().add( ElasticClientHolder .getInstance()
		 * .prepareIndex("tradegenie", "subindicator_indicator_stock_complex")
		 * .setSource(elastic_object.toJSONString()));
		 */
		/*
		 * try { System.in.read(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		if (getBrq().numberOfActions() == bulk_items_amount.intValue()) {

			index_documents_and_flush_bulk();

			setBrq(LocalEs.getInstance().prepareBulk());
			getBrq().add(
					LocalEs
							.getInstance()
							.prepareIndex("tradegenie_titan",
									"subindicator_indicator_stock_complex")
							.setSource(elastic_object.toJSONString()));

		} else {
			getBrq().add(
					LocalEs
							.getInstance()
							.prepareIndex("tradegenie_titan",
									"subindicator_indicator_stock_complex")
							.setSource(elastic_object.toJSONString()));
			// System.out.println("last oubject added to the bulk");
			// System.out.println(elastic_object.toJSONString());
		}
		// getBulkprocessor().add(
		// new IndexRequest("tradegenie_titan",
		// "subindicator_indicator_stock_complex")
		// .source(elastic_object.toJSONString()));

		// System.out.println("added to elasticsearch");
	}

	public String[] return_components_sector_industry(String s) {
		String newstring = s.replace("&amp;", ",").replace("&", ",");
		return newstring.split(",");

	}

	public void index_documents_and_flush_bulk() {
		BulkResponse response = getBrq().execute().actionGet();
		Iterator<BulkItemResponse> it = response.iterator();
		while (it.hasNext()) {
			BulkItemResponse resp = it.next();
			if (resp.isFailed()) {

				LoggingDailyOperations.get_instance().add_to_log(
						"exception",
						new Exception(
								"failed to add an item to es, here is message from es:"
										+ resp.getFailureMessage()),
						identifier_for_log, null);
			} else {
				getIds_of_es_complex_docs().add(resp.getId());

			}

		}
	}

	@Override
	public void process_response(
			Pair<ArrayList<HashMap<String, Object>>, ArrayList<HashMap<String, Object>>> response,
			ArrayList<Object> other_required_objects) {
		// TODO Auto-generated method stub
		if (response != null) {
			for (HashMap<String, Object> failed_item : response.getValue1()) {
				Map<String, Object> failed_item_source = (Map<String, Object>) failed_item
						.get("item");
				LoggingDailyOperations.get_instance()
						.getComplexes_which_failed_to_add_into_es()
						.add((String) failed_item_source.get("complex_name"));
				LoggingDailyOperations.get_instance().add_to_log(
						"exception",
						new Exception("failed to add complex to es:"
								+ (String) failed_item_source
										.get("complex_name")
								+ "error cause:"
								+ (String) failed_item_source
										.get("error_cause")),
						identifier_for_log, null);
			}

			for (HashMap<String, Object> successfull_item : response
					.getValue0()) {
				Map<String, Object> successfull_item_source = (Map<String, Object>) successfull_item
						.get("item");
				String complex_name = (String) successfull_item_source
						.get("complex_name");

				try (Jedis jedis = RedisManager.getInstance().getJedis()) {
					jedis.hset(complex_name, "es_id",
							(String) successfull_item.get("id"));
					// now depending on whether it is subindicator value or not
					// , we add either a
					// gsonified empty linkedhashmap or a compressed empty int[]
					if (complex_name
							.contains(StockConstants.subindicator_value)) {
						LinkedHashMap<Integer, Double> ln = new LinkedHashMap<Integer, Double>();
						jedis.hset(complex_name, "complex_values",
								this.gson.toJson(ln));
						// for n day stddev fo change and
						// subindicator_sma_values.
					} else if (complex_name
							.contains(StockConstants.subindicator_n_day_stddev)
							|| complex_name
									.contains(StockConstants.subindicator_sma_values)
							|| complex_name
									.contains(StockConstants.subindicator_n_day_rise_fall_amounts)) {
						Map<Integer, Map<String, Double>> tm = new TreeMap<Integer, Map<String, Double>>();
						jedis.hset(complex_name.getBytes(),
								("complex_values").getBytes(),
								(this.gson.toJson(tm)).getBytes());
					} else {
						int[] empty_placeholder_int_array = new int[0];
						byte[] compressed_byte_array = CompressUncompress
								.compress_integer_array_to_byte_array(empty_placeholder_int_array);
						jedis.hset(complex_name.getBytes(),
								(StockConstants.complex_values_buy_key_name)
										.getBytes(), compressed_byte_array);
						jedis.hset(complex_name.getBytes(),
								(StockConstants.complex_values_sell_key_name)
										.getBytes(), compressed_byte_array);
					}

				} catch (Exception e) {
					e.printStackTrace();
					try {
						System.in.read();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				LoggingDailyOperations.get_instance().total_complexes_added
						.incrementAndGet();
			}
		}
	}
}
