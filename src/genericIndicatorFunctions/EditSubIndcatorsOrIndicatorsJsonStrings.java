package genericIndicatorFunctions;

import Titan.ReadWriteTextToFile;

public class EditSubIndcatorsOrIndicatorsJsonStrings {

	private String path_of_subindicators_json_file;
	private String path_of_indicators_json_file;

	public String getPath_of_subindicators_json_file() {
		return path_of_subindicators_json_file;
	}

	public void setPath_of_subindicators_json_file(
			String path_of_subindicators_json_file) {
		if (path_of_subindicators_json_file == "") {
			this.path_of_subindicators_json_file = "subindicators_jsonString.json";
		} else {
			this.path_of_subindicators_json_file = path_of_subindicators_json_file;
		}
	}

	public String getPath_of_indicators_json_file() {
		return path_of_indicators_json_file;
	}

	public void setPath_of_indicators_json_file(
			String path_of_indicators_json_file) {
		if (path_of_indicators_json_file == "") {
			this.path_of_indicators_json_file = "indicators_jsonString.json";
		} else {

			this.path_of_indicators_json_file = path_of_indicators_json_file;
		}
	}

	public EditSubIndcatorsOrIndicatorsJsonStrings(
			String subindicators_json_file_path,
			String indicators_json_file_path) {
		setPath_of_indicators_json_file(indicators_json_file_path);
		setPath_of_subindicators_json_file(subindicators_json_file_path);
	}

}
