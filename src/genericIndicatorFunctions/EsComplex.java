package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import yahoo_finance_historical_scraper.StockConstants;

public class EsComplex {
	
	private String complex_name;
	private String stockName;
	private String stockFullName;
	private String indice;
	private String indicator_name;
	private String subindicator_name;
	private List<Integer> day_ids;
	private List<Integer> opposite_subgroups_day_ids;
	private List<String> business_types;
	private Integer period;
	private List<LinkedHashMap<Integer,HashMap<String,Double>>> complex_values;
		

	public String getComplex_name() {
		return complex_name;
	}
	public void setComplex_name(String complex_name) {
		this.complex_name = complex_name;
	}
	public List<String> getBusinessTypes() {
		return business_types;
	}
	public void setBusinessTypes(List<String> business_types) {
		this.business_types = business_types;
	}
	
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getStockFullName() {
		return stockFullName;
	}
	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}
	public String getIndice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public String getIndicator_name() {
		return indicator_name;
	}
	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}
	public String getSubindicator_name() {
		return subindicator_name;
	}
	public void setSubindicator_name(String subindicator_name) {
		this.subindicator_name = subindicator_name;
	}
	public List<Integer> getDay_ids() {
		return day_ids;
	}
	public void setDay_ids(List<Integer> day_ids) {
		this.day_ids = day_ids;
	}
	public List<Integer> getOpposite_subgroups_day_ids() {
		return opposite_subgroups_day_ids;
	}
	public void setOpposite_subgroups_day_ids(
			List<Integer> opposite_subgroups_day_ids) {
		this.opposite_subgroups_day_ids = opposite_subgroups_day_ids;
	}
	
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(String complex_name_to_derive_from) {
		
		for(Map.Entry<Integer, String> entry : StockConstants.period_in_integer_period_in_words_map.entrySet()){
			if(complex_name_to_derive_from.contains(entry.getValue())){
				this.period = entry.getKey();
			}
		}
		
		
	}
	
	/**
	 * @param complex_name
	 * @param stockName
	 * @param stockFullName
	 * @param indice
	 * @param indicator_name
	 * @param subindicator_name
	 * @param business_types
	 * @param industries
	 */
	public EsComplex(String complex_name, String stockName,
			String stockFullName, String indice, String indicator_name,
			String subindicator_name, 
			List<String> business_types) {
		super();
		this.complex_name = complex_name;
	
		this.stockName = stockName;
		this.stockFullName = stockFullName;
		this.indice = indice;
		this.indicator_name = indicator_name;
		this.subindicator_name = subindicator_name;
		this.business_types = business_types;
		this.day_ids = new ArrayList<Integer>();
		this.opposite_subgroups_day_ids = new ArrayList<Integer>();
		if(complex_name.contains("subindicator_value") || complex_name.contains("subindicator_n_day_stddev_of_change")){
			this.complex_values = new ArrayList<LinkedHashMap<Integer,HashMap<String,Double>>>();
		}
		
	}
	public List<LinkedHashMap<Integer,HashMap<String,Double>>> getComplex_values() {
		return complex_values;
	}
	public void setComplex_values(List<LinkedHashMap<Integer,HashMap<String,Double>>> complex_values) {
		this.complex_values = complex_values;
	}
	
	
	
	

}
