package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class EsGroup {

	private HashMap<String,Object> group_representation;
	private Integer group_numeric_id;
	private ArrayList<Integer> opposite_group_numeric_ids;
	private ArrayList<Integer> self_component_numeric_ids;
	private ArrayList<EsSubGroup> subgroups;
		
	public EsGroup(Integer group_numeric_id){
		this.group_representation = new HashMap<String, Object>();
		this.group_numeric_id = group_numeric_id;
		this.opposite_group_numeric_ids = new ArrayList<Integer>();
		this.self_component_numeric_ids = new ArrayList<Integer>();
		this.subgroups = new ArrayList<EsSubGroup>();
	}
	
	public HashMap<String,Object> build_group_representation(){
		getGroup_representation().put("group_numeric_id", this.group_numeric_id);
		getGroup_representation().put("self_components_numeric_ids", self_component_numeric_ids);
		getGroup_representation().put("opposite_groups_numeric_ids", opposite_group_numeric_ids);
		getGroup_representation().put("subgroups", build_and_get_subgroups());
		return getGroup_representation();
	}
	
	public ArrayList<HashMap<String,Object>> build_and_get_subgroups(){
		ArrayList<HashMap<String,Object>> subgroups_arraylist = new ArrayList<HashMap<String,Object>>();
		for(EsSubGroup esSubGroup : getSubgroups()){
			subgroups_arraylist.add(esSubGroup.build_and_return_subgroup());
		}
		return subgroups_arraylist;
	}

	public HashMap<String, Object> getGroup_representation() {
		return group_representation;
	}

	public void setGroup_representation(HashMap<String, Object> group_representation) {
		this.group_representation = group_representation;
	}

	public ArrayList<Integer> getOpposite_group_numeric_ids() {
		return opposite_group_numeric_ids;
	}

	public void setOpposite_group_numeric_ids(
			ArrayList<Integer> opposite_group_numeric_ids) {
		this.opposite_group_numeric_ids = opposite_group_numeric_ids;
	}

	public ArrayList<Integer> getSelf_component_numeric_ids() {
		return self_component_numeric_ids;
	}

	public void setSelf_component_numeric_ids(
			ArrayList<Integer> self_component_numeric_ids) {
		this.self_component_numeric_ids = self_component_numeric_ids;
	}

	public ArrayList<EsSubGroup> getSubgroups() {
		return subgroups;
	}

	public void setSubgroups(ArrayList<EsSubGroup> subgroups) {
		this.subgroups = subgroups;
	}
	
	
}
