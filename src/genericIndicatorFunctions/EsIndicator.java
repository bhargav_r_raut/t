package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;

import ExchangeBuilder.CentralSingleton;
import Titan.GroovyStatements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EsIndicator {

	private String indicator_name;
	private Integer indicator_numeric_id;
	private String indicator_hex_id;
	private ArrayList<String> ohlcv_groups;
	/**
	 * jsonified string.
	 */
	private ArrayList<HashMap<String,Object>> periods;
	private ArrayList<HashMap<String, Object>> monitor_with;
	private ArrayList<HashMap<String, Object>> cross_with;
	private ArrayList<Double> critical_line_limits;
	private transient String indicator_string_representation;
	private ArrayList<HashMap<String, Object>> applicable_subindicators;

	public ArrayList<HashMap<String, Object>> getMonitor_with() {
		return monitor_with;
	}

	public void setMonitor_with(ArrayList<HashMap<String, Object>> monitor_with) {
		this.monitor_with = monitor_with;
	}

	public ArrayList<HashMap<String, Object>> getCross_with() {
		return cross_with;
	}

	public void setCross_with(ArrayList<HashMap<String, Object>> cross_with) {
		this.cross_with = cross_with;
	}

	public ArrayList<HashMap<String, Object>> getApplicable_subindicators() {
		return applicable_subindicators;
	}

	public void setApplicable_subindicators(
			ArrayList<HashMap<String, Object>> applicable_subindicators) {
		this.applicable_subindicators = applicable_subindicators;
	}
	
	public void build_periods(ArrayList<ArrayList<Integer>> periods){
		this.periods = new ArrayList<HashMap<String,Object>>();
		Integer index_no = 0;
		for(final ArrayList<Integer> index_components: periods){
			final Integer inx = index_no;
			this.periods.add(new HashMap<String,Object>(){{
				put("index_components",index_components);
				put("index_no",inx);
			}});	
			index_no++;
		}
		
		
	}

	public void add_applicable_subindicator(String subindicator_name) {

		// get counter or index it.
		Integer counter_of_element = CentralSingleton
				.getInstance()
				.get_counter_for_element_or_index_it(
						new ArrayList<String>(Arrays.asList(subindicator_name)));
		HashMap<String,Object> applicable_subindicator = new HashMap<String, Object>();
		applicable_subindicator.put("applicable_subindicator_name", subindicator_name);
		applicable_subindicator.put("applicable_subindicator_numeric_id", counter_of_element);
		applicable_subindicators.add(applicable_subindicator);

	}

	public void add_monitor_with_indicator(String monitor_with_indicator_name) {

		Integer counter_of_element = CentralSingleton
				.getInstance()
				.get_counter_for_element_or_index_it(
						new ArrayList<String>(Arrays.asList(monitor_with_indicator_name)));
		HashMap<String,Object> monitor_with_indicator = new HashMap<String, Object>();
		monitor_with_indicator.put("monitor_with_indicator_name", monitor_with_indicator_name);
		monitor_with_indicator.put("monitor_with_indicator_numeric_id", counter_of_element);
		monitor_with.add(monitor_with_indicator);
	}

	public void add_cross_with_indicator(String cross_with_indicator_name) {

		Integer counter_of_element = CentralSingleton
				.getInstance()
				.get_counter_for_element_or_index_it(
						new ArrayList<String>(Arrays.asList(cross_with_indicator_name)));
		HashMap<String,Object> cross_with_indicator = new HashMap<String, Object>();
		cross_with_indicator.put("cross_with_indicator_name", cross_with_indicator_name);
		cross_with_indicator.put("cross_with_indicator_numeric_id", counter_of_element);
		cross_with.add(cross_with_indicator);
	}

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
	}

	public Integer getIndicator_numeric_id() {
		return indicator_numeric_id;
	}

	public void setIndicator_numeric_id(Integer indicator_numeric_id) {
		this.indicator_numeric_id = indicator_numeric_id;
	}

	public String getIndicator_hex_id() {
		return indicator_hex_id;
	}

	public void setIndicator_hex_id(String indicator_hex_id) {
		this.indicator_hex_id = indicator_hex_id;
	}

	public ArrayList<String> getOhlcv_groups() {
		return ohlcv_groups;
	}

	public void setOhlcv_groups(ArrayList<String> ohlcv_groups) {
		this.ohlcv_groups = ohlcv_groups;
	}

	public ArrayList<HashMap<String,Object>> getPeriods() {
		return periods;
	}

	public void setPeriods(ArrayList<HashMap<String,Object>> periods) {
		this.periods = periods;
	}

	public ArrayList<Double> getCritical_line_limits() {
		return critical_line_limits;
	}

	public void setCritical_line_limits(ArrayList<Double> critical_line_limits) {
		this.critical_line_limits = critical_line_limits;
	}

	public String getIndicator_string_representation() {
		return indicator_string_representation;
	}

	public void setIndicator_string_representation(
			String indicator_string_representation) {
		this.indicator_string_representation = indicator_string_representation;
	}

	public EsIndicator(String indicator_name) {
		this.indicator_name = indicator_name;
		this.indicator_numeric_id = CentralSingleton.getInstance()
				.get_counter_for_element_or_index_it(
						new ArrayList<String>(Arrays.asList(indicator_name)));
		// System.out.println("is it before this.");
		this.indicator_hex_id = DigestUtils.sha256Hex(indicator_name);
		this.monitor_with = new ArrayList<HashMap<String, Object>>();
		this.cross_with = new ArrayList<HashMap<String, Object>>();
		this.applicable_subindicators = new ArrayList<HashMap<String, Object>>();
		this.critical_line_limits = new ArrayList<Double>();
		this.ohlcv_groups = new ArrayList<String>();
	}

	public void build_string_representation() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		this.indicator_string_representation = gson.toJson(this);
	}

}
