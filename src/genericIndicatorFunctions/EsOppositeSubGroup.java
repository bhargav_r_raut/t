package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class EsOppositeSubGroup {

	private Integer opposite_subgroup_numeric_id;
	private Integer opposite_specific_subindicator_id;
	private ArrayList<String> opposite_subgroup_components;
	
	public ArrayList<String> getOpposite_subgroup_components() {
		return opposite_subgroup_components;
	}

	public void setOpposite_subgroup_components(
			ArrayList<String> opposite_subgroup_components) {
		this.opposite_subgroup_components = opposite_subgroup_components;
	}

	public Integer getOpposite_subgroup_numeric_id() {
		return opposite_subgroup_numeric_id;
	}

	public void setOpposite_subgroup_numeric_id(
			Integer opposite_subgroup_numeric_id) {
		this.opposite_subgroup_numeric_id = opposite_subgroup_numeric_id;
	}

	public Integer getOpposite_specific_subindicator_id() {
		return opposite_specific_subindicator_id;
	}

	public void setOpposite_specific_subindicator_id(
			Integer opposite_specific_subindicator_id) {
		this.opposite_specific_subindicator_id = opposite_specific_subindicator_id;
	}
	
	public EsOppositeSubGroup(Integer es_opposite_subgroup_id){
		this.opposite_subgroup_numeric_id = es_opposite_subgroup_id;
		this.opposite_subgroup_components = new ArrayList<String>();
	}
	
	public HashMap<String, Object> build_and_return_opposite_subgroup() {

		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("opposite_subgroup_numeric_id",
				getOpposite_subgroup_numeric_id());
		hm.put("opposite_specific_subindicator_id",
				getOpposite_specific_subindicator_id());
		hm.put("opposite_subgroup_components", getOpposite_subgroup_components());
		return hm;
	}

}
