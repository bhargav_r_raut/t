package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class EsSubGroup {

	private Integer subgroup_numeric_id;
	private ArrayList<EsOppositeSubGroup> opposite_subgroups;

	public Integer getSubgroup_numeric_id() {
		return subgroup_numeric_id;
	}

	public void setSubgroup_numeric_id(Integer subgroup_numeric_id) {
		this.subgroup_numeric_id = subgroup_numeric_id;
	}

	public ArrayList<EsOppositeSubGroup> getOpposite_subgroups() {
		return opposite_subgroups;
	}

	public void setOpposite_subgroups(
			ArrayList<EsOppositeSubGroup> opposite_subgroups) {
		this.opposite_subgroups = opposite_subgroups;
	}

	public EsSubGroup(Integer subgroup_numeric_id) {
		this.subgroup_numeric_id = subgroup_numeric_id;
		this.opposite_subgroups = new ArrayList<EsOppositeSubGroup>();
	}

	public HashMap<String, Object> build_and_return_subgroup() {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("subgroup_numeric_id", getSubgroup_numeric_id());
		ArrayList<HashMap<String, Object>> arm = new ArrayList<HashMap<String, Object>>();

		for (EsOppositeSubGroup esopp : getOpposite_subgroups()) {
			arm.add(esopp.build_and_return_opposite_subgroup());
		}

		hm.put("opposite_subgroups", arm);

		return hm;

	}

}
