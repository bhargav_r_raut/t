package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;

import ExchangeBuilder.CentralSingleton;
import Titan.GroovyStatements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EsSubIndicator {

	private String subindicator_name;
	private String subindicator_hex_id;
	private Integer subindicator_numeric_id;
	private ArrayList<HashMap<String, Object>> groups;
	
	
	
	public String getHex_id() {
		return subindicator_hex_id;
	}

	public void setHex_id(String hex_id) {
		this.subindicator_hex_id = hex_id;
	}

	public Integer getNumeric_id() {
		return subindicator_numeric_id;
	}

	public void setNumeric_id(Integer numeric_id) {
		this.subindicator_numeric_id = numeric_id;
	}

	public ArrayList<HashMap<String, Object>> getGroups() {
		return groups;
	}

	public void setGroups(ArrayList<HashMap<String, Object>> groups) {
		this.groups = groups;
	}
	
	public EsSubIndicator(){
		
	}
	
	public EsSubIndicator(String subindicator_name) {
		this.subindicator_name = subindicator_name;
		//System.out.println("is it after this.");
		//System.out.println("came to get the subindicator numeric id.");
		this.subindicator_numeric_id = CentralSingleton
				.getInstance()
				.get_counter_for_element_or_index_it(
						new ArrayList<String>(Arrays.asList(subindicator_name)));
		//System.out.println("is it before this.");
		//System.out.println("numeric id is:" + this.subindicator_numeric_id);
		this.subindicator_hex_id = DigestUtils.sha256Hex(subindicator_name);
		this.groups = new ArrayList<HashMap<String,Object>>();
	}

	public String get_json_representation() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

}
