package genericIndicatorFunctions;

public class FailedToCommitMd5HashToDb extends Exception{

	public FailedToCommitMd5HashToDb(String exception){
		super(exception);
	}
}
