package genericIndicatorFunctions;

import com.sun.tools.internal.xjc.generator.bean.field.GenericFieldRenderer;

public class GenerateFieldNamesForIndicator {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		String indicator_name = "Aroon_up";
		String stub = "public ArrayList<Double> ";
		Integer counter = 0;
		for(String s: GenericIndicatorsConstants.generic_indicator_parameters){
			System.out.println(stub + "" + indicator_name + "_" + s + ";");
			counter++;
		}
		System.out.println(counter);
		*/
		GenerateFieldNamesForIndicator.generate_titan_schema_for_indicators_array("Aroon_up");
	}
	
	public static void generate_titan_schema_for_indicators_array(String basic_indicator_name){
		
		for(String s: GenericIndicatorsConstants.generic_indicator_parameters){
			System.out.println("schema.create_key(" + GenericIndicatorsConstants.DOUBLE_QUOTES
					+ basic_indicator_name + "_" + s + GenericIndicatorsConstants.DOUBLE_QUOTES +
					"," + GenericIndicatorsConstants.DOUBLE_QUOTES + "Edge" + GenericIndicatorsConstants.DOUBLE_QUOTES 
					+ ",false, false, false, Float.class, true, graph, mgmt, this.cpk);");
		}
		
	}

}
