package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Map;

import DailyOperationsLogging.IndicatorOperations;
import DailyOperationsLogging.LoggingDailyOperations;
import DailyOperationsLogging.SubIndicatorOperations;

public class GenerateSummaryOfNewlyAddedIndicators {

	/***
	 * they are only added to the arraylist if they have been successfully added
	 * to the graph.
	 */
	private ArrayList<String> subindicators_which_changed;
	private ArrayList<String> indicators_which_changed;
	private ArrayList<String> subindicators_which_stayed_the_same;
	private ArrayList<String> indicators_which_stayed_the_same;

	public ArrayList<String> getSubindicators_which_changed() {
		return subindicators_which_changed;
	}

	public void setSubindicators_which_changed(
			ArrayList<String> subindicators_which_changed) {
		this.subindicators_which_changed = subindicators_which_changed;
	}

	public ArrayList<String> getIndicators_which_changed() {
		return indicators_which_changed;
	}

	public void setIndicators_which_changed(
			ArrayList<String> indicators_which_changed) {
		this.indicators_which_changed = indicators_which_changed;
	}

	public ArrayList<String> getSubindicators_which_stayed_the_same() {
		return subindicators_which_stayed_the_same;
	}

	public void setSubindicators_which_stayed_the_same(
			ArrayList<String> subindicators_which_stayed_the_same) {
		this.subindicators_which_stayed_the_same = subindicators_which_stayed_the_same;
	}

	public ArrayList<String> getIndicators_which_stayed_the_same() {
		return indicators_which_stayed_the_same;
	}

	public void setIndicators_which_stayed_the_same(
			ArrayList<String> indicators_which_stayed_the_same) {
		this.indicators_which_stayed_the_same = indicators_which_stayed_the_same;
	}

	public GenerateSummaryOfNewlyAddedIndicators() {
		setSubindicators_which_changed(new ArrayList<String>());
		setSubindicators_which_stayed_the_same(new ArrayList<String>());
		setIndicators_which_changed(new ArrayList<String>());
		setIndicators_which_stayed_the_same(new ArrayList<String>());
		System.out.println("came inside generate summary constrcutor.");
		generate_summary();
	}

	public void generate_summary() {

		for (Map.Entry<String, SubIndicatorOperations> entry : LoggingDailyOperations
				.get_instance().getSubindicator_operations().entrySet()) {

			SubIndicatorOperations sops = entry.getValue();
			if (sops.getAlready_present_in_graph_and_same()) {
				getSubindicators_which_stayed_the_same().add(entry.getKey());
			} else {
				if (sops.getdifferent() && sops.getAdded_vertex_to_graph()
						&& sops.getAdded_parameters_to_graph()) {
					getSubindicators_which_changed().add(entry.getKey());
				}
			}

		}

		for (Map.Entry<String, IndicatorOperations> entry : LoggingDailyOperations
				.get_instance().getIndicator_operations().entrySet()) {
			IndicatorOperations iops = entry.getValue();
			if (iops.getAlready_present_in_graph_and_same()) {
				getIndicators_which_stayed_the_same().add(entry.getKey());
			} else {
				if (iops.getdifferent_because_either_absent_or_changed()
						&& iops.getSuccessfully_added_vertex_and_vertex_properties_to_graph()
						&& iops.getSuccessfully_added_monitor_with_and_cross_with_edges()) {
					getIndicators_which_changed().add(entry.getKey());
				}
			}
		}

		LoggingDailyOperations.get_instance().indicators_which_changed = getIndicators_which_changed();
		LoggingDailyOperations.get_instance().indicators_which_stayed_same = getIndicators_which_stayed_the_same();
		LoggingDailyOperations.get_instance().subindicators_which_changed = getSubindicators_which_changed();
		LoggingDailyOperations.get_instance().subindicators_which_stayed_same = getSubindicators_which_stayed_the_same();
		
		System.out.println("came out of the generate summary code.");
	}
}
