/**
 * 
 */
package genericIndicatorFunctions;

/**
 * @author aditya
 *
 */
public interface GenericIndicatorsConstants {
	
	
	
	/**
	 * 
	 * generic indicator constants - paramters__
	 * 
	 */
	
	public static final String[] generic_indicator_parameters = 
		{"indicator_rising","indicator_rising_for_1_days",
		"indicator_rising_for_2_days","indicator_rising_for_3_days",
		"indicator_rising_for_4_days","indicator_rising_for_5_days",
		"indicator_rising_for_6_days","indicator_rising_for_7_days",
		"indicator_falling","indicator_falling_for_1_days",
		"indicator_falling_for_2_days","indicator_falling_for_3_days",
		"indicator_falling_for_4_days","indicator_falling_for_5_days",
		"indicator_falling_for_6_days","indicator_falling_for_7_days",
		"indicator_rises_by_upto_0__5_percent","indicator_falls_by_upto_0__5_percent",
		"indicator_rises_by_upto_1__0_percent","indicator_falls_by_upto_1__0_percent",
		"indicator_rises_by_upto_1__5_percent","indicator_falls_by_upto_1__5_percent",
		"indicator_rises_by_upto_2__0_percent","indicator_falls_by_upto_2__0_percent",
		"indicator_rises_by_upto_2__5_percent","indicator_falls_by_upto_2__5_percent",
		"indicator_rises_by_upto_3__0_percent","indicator_falls_by_upto_3__0_percent",
		"indicator_rises_by_upto_3__5_percent","indicator_falls_by_upto_3__5_percent",
		"indicator_rises_by_upto_4__0_percent","indicator_falls_by_upto_4__0_percent",
		"indicator_rises_by_upto_4__5_percent","indicator_falls_by_upto_4__5_percent",
		"indicator_rises_by_upto_5__0_percent","indicator_falls_by_upto_5__0_percent",
		"indicator_range","indicator_range_for_how_many_days",
		"indicator_goes_above_critical_level","indicator_goes_below_critical_level",
		"indicator_crosses_above_signal_line","indicator_crosses_below_signal_line",
		"indicator_crosses_above_zero_line","indicator_crosses_below_zero_line",
		"indicator_above_signal_line_for_how_many_days","indicator_below_signal_line_for_how_many_days"
		,"indicator_above_zero_line_for_how_many_days","indicator_below_zero_line_for_how_many_days"
		,"indicator_rate_of_change_of","indicator_range_of_rate_of_change","indicator_range_of_rate_of_change_for_how_many_days",
		"indicator_sma","indicator_sma_percentage",
		"indicator_high_3_days","indicator_low_3_days",
		"indicator_high_7_days","indicator_low_7_days",
		"indicator_high_10_days","indicator_low_10_days",
		"indicator_high_14_days","indicator_low_14_days",
		"indicator_high_21_days","indicator_low_21_days",
		"indicator_high_31_days","indicator_low_31_days",
		"indicator_high_60_days","indicator_low_60_days",
		"indicator_high_90_days","indicator_low_90_days",
		"indicator_high_120_days","indicator_low_120_days",
		"indicator_high_150_days","indicator_low_150_days",
		"price_new_low_but_indicator_not_3_days","price_new_high_but_indicator_not_3_days",
		"price_new_low_but_indicator_not_7_days","price_new_high_but_indicator_not_7_days",
		"price_new_low_but_indicator_not_10_days","price_new_high_but_indicator_not_10_days",
		"price_new_low_but_indicator_not_14_days","price_new_high_but_indicator_not_14_days",
		"price_new_low_but_indicator_not_21_days","price_new_high_but_indicator_not_21_days",
		"price_new_low_but_indicator_not_31_days","price_new_high_but_indicator_not_31_days",
		"price_new_low_but_indicator_not_60_days","price_new_high_but_indicator_not_60_days",
		"price_new_low_but_indicator_not_90_days","price_new_high_but_indicator_not_90_days",
		"price_new_low_but_indicator_not_120_days","price_new_high_but_indicator_not_120_days",
		"price_new_low_but_indicator_not_150_days","price_new_high_but_indicator_not_150_days",
		"price_new_low_but_indicator_not_score","price_new_high_but_indicator_not_score"};
	
	public static final Double default_parameter_for_not_calculable_due_to_lack_of_data = -7777.0;
	public static final Double defualt_parameter_for_yes = -9999.0;
	public static final Double default_parameter_for_no = -8888.0;
	
	public static final String [] aroon_indicator_parameters = {"aroon_up_crosses_above_aroon_down",
		"aroon_down_crosses_above_aroon_up","aroon_up_falls_below_aroon_down","aroon_down_falls_below_aroon_up"
		};
	public static final String DOT = ".";
	public static final String OPEN_BRACKET = "(";
	public static final String CLOSE_BRACKET = ")";
	public static final String COMMA = ",";
	public static final String DOUBLE_QUOTES = "\"";
	
		
	
}