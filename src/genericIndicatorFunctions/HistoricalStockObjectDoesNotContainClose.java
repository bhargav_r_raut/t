package genericIndicatorFunctions;

public class HistoricalStockObjectDoesNotContainClose extends Exception {
	public HistoricalStockObjectDoesNotContainClose(String message){
		super(message);
	}
}
