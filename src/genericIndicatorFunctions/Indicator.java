package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.cassandra.utils.OutputHandler.SystemOutput;
import org.apache.commons.codec.digest.DigestUtils;
import org.elasticsearch.action.index.IndexResponse;

import DailyOperationsLogging.LoggingDailyOperations;
import Titan.GroovyStatements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.util.structures.Row;

import elasticSearchNative.LocalEs;

public class Indicator {
	private static final String identifier_for_log = "indicator";
	
	@JsonIgnore
	private transient Gson gson;

	public Gson getGson() {
		if (this.gson == null) {
			setGson(new GsonBuilder().setPrettyPrinting().create());
		}
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	/**
	 * 
	 * name of new indicator to add.
	 * 
	 */
	private String indicator_name;
	private EsIndicator es_indicator;

	public EsIndicator getEs_indicator() {
		return es_indicator;
	}

	public void setEs_indicator(EsIndicator es_indicator) {
		this.es_indicator = es_indicator;
	}

	private transient ArrayList<SubIndicator> subindicators_applicable_to_this_indicator;

	/**
	 * ohlcv groups: of this indicator.
	 */
	private ArrayList<String> ohlcv_group;

	/**
	 * periods is an arraylist of interger arraylists. for example a given
	 * indicator may use in its calculation two indicators
	 * 
	 * another may use three.
	 * 
	 * so the first element of the array contains the possibilities of the first
	 * element in the calcualtion.
	 * 
	 * the second element in the array contains the possibliities for the second
	 * element etc.
	 * 
	 * if there is only one element in the calculation, then there is only one
	 * element in the main array
	 * 
	 */
	private ArrayList<ArrayList<Integer>> periods;

	/**
	 * arraylist string, cross this indicator with which indicators.
	 * 
	 * 
	 */
	private ArrayList<String> cross_with;

	/**
	 * arraylist string, monitor with.
	 * 
	 * 
	 */
	private ArrayList<String> monitor_with;

	/**
	 * the limits of the critical line wherever applicable.
	 * 
	 */
	private ArrayList<Double> critical_line_limits;

	public String getIndicator_name() {
		return indicator_name;
	}

	public void setIndicator_name(String indicator_name) {
		this.indicator_name = indicator_name;
		this.es_indicator = new EsIndicator(indicator_name);
	}

	public ArrayList<SubIndicator> getSubindicators_applicable_to_this_indicator() {
		return subindicators_applicable_to_this_indicator;
	}

	public void setSubindicators_applicable_to_this_indicator(
			ArrayList<SubIndicator> subindicators_applicable_to_this_indicator) {
		this.subindicators_applicable_to_this_indicator = subindicators_applicable_to_this_indicator;
	}

	public void add_subindicator(SubIndicator subindicator) {
		if (subindicators_applicable_to_this_indicator == null) {
			subindicators_applicable_to_this_indicator = new ArrayList<SubIndicator>();
		}
		this.subindicators_applicable_to_this_indicator.add(subindicator);
	}

	public void remove_subindicator(SubIndicator subindicator) {
		this.subindicators_applicable_to_this_indicator.remove(subindicator);
	}

	public void remove_subindicators(
			ArrayList<String> subindicator_names_to_remove)
			throws SpellingErrorWhileLookingForThisSubIndicator {

		ArrayList<SubIndicator> subindicators_to_remove = new ArrayList<SubIndicator>();
		/**
		 * here subindicators applicable to this indicator means all
		 * subindicators.
		 * 
		 */

		for (SubIndicator subindicator : this.subindicators_applicable_to_this_indicator) {

			if (subindicator_names_to_remove.contains(subindicator
					.getSubIndicator_name())) {
				subindicators_to_remove.add(subindicator);
			} else {

			}
		}

		if (subindicators_to_remove.size() != subindicator_names_to_remove
				.size()) {
			throw new SpellingErrorWhileLookingForThisSubIndicator(
					"some subindicator which we were "
							+ "trying to remove was not found anywhere"
							+ " in the available subindicators."
							+ "name of indicator being processed"
							+ this.indicator_name);
		}

		for (SubIndicator subindicator : subindicators_to_remove) {
			remove_subindicator(subindicator);
		}
	}

	public ArrayList<String> getOhlcv_group() {
		return ohlcv_group;
	}

	public void setOhlcv_group(ArrayList<String> ohlcv_group) {
		this.ohlcv_group = ohlcv_group;
	}

	public void addOhlcv_group(String ohlcv_group) {
		if (getOhlcv_group() == null) {
			setOhlcv_group(new ArrayList<String>());

		}
		getOhlcv_group().add(ohlcv_group);
	}

	public ArrayList<ArrayList<Integer>> getPeriods() {
		return periods;
	}

	public void setPeriods(ArrayList<ArrayList<Integer>> periods) {
		this.periods = periods;
	}

	public ArrayList<String> getCross_with() {
		return cross_with;
	}

	public void setCross_with(ArrayList<String> cross_with) {
		this.cross_with = cross_with;
	}

	public void addCross_with(String cross_with_indicator) {
		if (this.cross_with == null) {
			this.cross_with = new ArrayList<String>();
		}
		this.cross_with.add(cross_with_indicator);
	}

	public ArrayList<String> getMonitor_with() {
		return monitor_with;
	}

	public void setMonitor_with(ArrayList<String> monitor_with) {
		this.monitor_with = monitor_with;
	}

	public void addMonitor_with(String monitor_with_indicator) {
		if (this.monitor_with == null) {
			this.monitor_with = new ArrayList<String>();
		}
		this.monitor_with.add(monitor_with_indicator);
	}

	public ArrayList<Double> getCritical_line_limits() {
		return critical_line_limits;
	}

	public void setCritical_line_limits(ArrayList<Double> critical_line_limits) {
		this.critical_line_limits = critical_line_limits;
	}

	public void addCritical_line_limits(Double critical_line_limit) {
		this.critical_line_limits.add(critical_line_limit);
	}

	public Indicator(String indicator_name,
			ArrayList<SubIndicator> subindicators_applicable_to_this_indicator,
			ArrayList<String> ohlcv_group, ArrayList<ArrayList<Integer>> periods) {

		this.indicator_name = indicator_name;
		this.subindicators_applicable_to_this_indicator = subindicators_applicable_to_this_indicator;
		this.ohlcv_group = ohlcv_group;
		this.periods = periods;
		

	}

	/*
	 * public boolean compare_two_string_arrays(ArrayList<String> arr1,
	 * ArrayList<String>arr2){ if(arr1.size() != arr2.size()){ return false; }
	 * 
	 * for(Object obj : arr1){ if(!arr2.contains(obj)){ return false; } }
	 * 
	 * return true; }
	 * 
	 * public boolean compare_two_Integer_arrays(ArrayList<Integer> arr1,
	 * ArrayList<Integer>arr2){ if(arr1.size() != arr2.size()){ return false; }
	 * 
	 * for(Integer obj : arr1){ if(!arr2.contains(obj)){ return false; } }
	 * 
	 * return true; }
	 * 
	 * public boolean compare_two_double_arrays(ArrayList<Double> arr1,
	 * ArrayList<Double>arr2){ if(arr1.size() != arr2.size()){
	 * System.out.println("size was different."); return false; }
	 * 
	 * if(!(arr1.containsAll(arr2)) || !(arr2.containsAll(arr1))){ return false;
	 * }
	 * 
	 * return true; }
	 * 
	 * public boolean compare_two_arraylist_arrays(ArrayList<ArrayList<Integer>>
	 * arr1, ArrayList<ArrayList<Integer>> arr2){ if(arr1.size() !=
	 * arr2.size()){ return false; }
	 * 
	 * for(ArrayList<Integer> obj : arr1){ if(!arr2.contains(obj)){ return
	 * false; } }
	 * 
	 * return true; }
	 * 
	 * 
	 * 
	 * public Boolean compare_to_existing_indicator(Indicator json_indicator){
	 * ArrayList<String> json_subindicators = new ArrayList<String>();
	 * for(SubIndicator subindicator:
	 * json_indicator.getSubindicators_applicable_to_this_indicator()){
	 * json_subindicators.add(subindicator.getSubIndicator_name()); }
	 * ArrayList<String> existing_subindicators_names = new ArrayList<String>();
	 * 
	 * if(getSubindicators_applicable_to_this_indicator() == null &&
	 * json_subindicators.size()>0){
	 * System.out.println("subindicators applicable are null"); return false; }
	 * 
	 * for(SubIndicator s: getSubindicators_applicable_to_this_indicator()){
	 * existing_subindicators_names.add(s.getSubIndicator_name()); }
	 * 
	 * if(!(compare_two_string_arrays(json_subindicators,
	 * existing_subindicators_names))) {
	 * System.out.println("the subindicators were different."); return false; }
	 * if
	 * (!(compare_two_string_arrays(existing_subindicators_names,json_subindicators
	 * ))) { System.out.println("the subindicators were different."); return
	 * false; }
	 * 
	 * Collections.sort(getCritical_line_limits());
	 * Collections.sort(json_indicator.getCritical_line_limits());
	 * 
	 * 
	 * return true; }
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub

		StringBuilder sb = new StringBuilder();
		sb.append(this.indicator_name);
		sb.append("\n the subindicators are");
		if (this.subindicators_applicable_to_this_indicator != null) {

			ArrayList<String> subindicators_names = new ArrayList<String>();
			//System.out.println(getSubindicators_applicable_to_this_indicator()
			//		.toString());
			for (SubIndicator subindicator : getSubindicators_applicable_to_this_indicator()) {

				subindicators_names.add(subindicator.getSubIndicator_name());
			}

			Collections.sort(subindicators_names);

			sb.append(subindicators_names.toString());

		}
		sb.append("\n");

		if (this.ohlcv_group != null) {
			sb.append("the ohlcv group is-->" + getOhlcv_group().toString());
		} else {
			sb.append("the ohlcv group is-->"
					+ new ArrayList<String>().toString());
			// System.out.println("the ohlcv group was null");
		}

		if (this.periods != null) {
			sb.append("the periods are --->"
					+ getPeriods().toString().replace(".0", ""));
		} else {
			ArrayList<Integer> ar_n = new ArrayList<Integer>();
			ArrayList<ArrayList<Integer>> outer = new ArrayList<ArrayList<Integer>>();
			outer.add(ar_n);
			sb.append("the periods are --->" + outer.toString());
			// System.out.println("the periods were null");
		}

		if (this.monitor_with != null) {
			sb.append("the monitor_with indicators are--->"
					+ getMonitor_with().toString().replace("self",
							this.getIndicator_name()));
		} else {
			sb.append("the monitor_with indicators are--->"
					+ new ArrayList<String>().toString());
			// System.out.println("the monitor with was null");
		}

		if (this.cross_with != null) {
			sb.append("the cross with indicators are--->"
					+ getCross_with().toString().replace("self",
							this.getIndicator_name()));
		} else {
			sb.append("the cross with indicators are--->"
					+ new ArrayList<String>().toString());
			// System.out.println("the cross with is null");
		}

		if (this.critical_line_limits != null) {
			ArrayList<Double> reflection = getCritical_line_limits();
			Collections.sort(reflection);
			sb.append("the critical line limits are--->"
					+ reflection.toString());
		} else {
			sb.append("the critical line limits are--->"
					+ new ArrayList<Double>().toString());
			// System.out.println("critical line limits were null");
		}

		return sb.toString();
	}

	/**
	 * constructor for creating a default universally applicable indicator
	 * object.
	 * 
	 */
	public Indicator() {

	}

	/**
	 * appply all the subindicators that exist to this indicator.
	 * 
	 * 
	 */

	public void apply_all_subindicators_to_indicator(
			HashMap<String, SubIndicator> all_avaiable_subindicators) {
		for (Map.Entry<String, SubIndicator> entry : all_avaiable_subindicators
				.entrySet()) {
			this.add_subindicator(entry.getValue());
		}
	}

	public void draw_edge_to_cross_monitor_with_indicators(String label,
			HashMap<String, Long> indicator_vertex_ids, TitanGraph g,
			ArrayList<Indicator> failed_cross_with_edges,
			ArrayList<Indicator> failed_monitor_with_edges,
			ArrayList<String> intersect_with) {
		// System.out.println("inside here is its cross with.");

		if (!intersect_with.isEmpty()) {
			for (String cross_with : intersect_with) {
				// get the relevant vertex id in the map and draw the edge to
				// it.
				try {
					// System.out.println("the current indicator name is--->" +
					// this.getIndicator_name());
					// System.out.println("the indicator vertex ids are--->" +
					// indicator_vertex_ids.toString());
					Long vertex_id_of_crosser = null;
					if (cross_with.equals("self")) {

						vertex_id_of_crosser = indicator_vertex_ids.get(this
								.getIndicator_name());
						// System.out.println("the self vertex id of crosses is-->");
						// System.out.println(vertex_id_of_crosser);
					} else {
						vertex_id_of_crosser = indicator_vertex_ids
								.get(cross_with);
					}

					Long vertex_id_of_current_indicator = indicator_vertex_ids
							.get(this.getIndicator_name());

					Edge e = g.addEdge(null,
							g.getVertex(vertex_id_of_current_indicator),
							g.getVertex(vertex_id_of_crosser), label);
					// System.out.println("adding cross with or monitor with result");
					// System.out.println(e.getId());
					// Thread.sleep(10000);
					g.commit();
				} catch (Exception e) {
					System.out.println("exception in adding cross with");
					e.printStackTrace();
					if (label == "cross_indicator_with") {
						failed_cross_with_edges.add(this);
					} else {
						failed_monitor_with_edges.add(this);
					}
				}
			}
		}
	}

	public void remove_all_outgoing_edges(TitanGraph g, Vertex v) {

		ArrayList<Edge> outgoing_edges = GroovyStatements.getInstance()
				.get_outgoing_edges(v);
		for (Edge e : outgoing_edges) {
			System.out.println("now removing the outgoing edges only.");

			g.removeEdge(e);
			System.out.println("now removed" + e.getId());
		}
		g.commit();
	}

	public Long add_indicator_to_database(TitanGraph g, Gson gson)
			throws Exception {

		// TitanTransaction tx = g.newTransaction();

		Iterator<Vertex> v = g.getVertices("indicator_name",
				this.getIndicator_name()).iterator();
		while (v.hasNext()) {
			System.out.println("found that the indicator already exists.");
			Vertex existing_indicator_vertex = v.next();
			remove_all_outgoing_edges(g, existing_indicator_vertex);
			add_indicator_parameters_to_database(
					(Long) existing_indicator_vertex.getId(), g);
			g.commit();
			// tx.commit();
			return (Long) existing_indicator_vertex.getId();
		}
		// first search for an existing vertex with this indicator name, and
		// only otherwise create a new
		// one,
		// remove all outgoing edges of an existing such indicator.

		Vertex indicator = g.addVertexWithLabel("indicator");
		indicator.setProperty("indicator_name", this.indicator_name);
		// System.out.println("after creating a new vertex");
		// System.out.println(this.periods);
		add_indicator_parameters_to_database((Long) indicator.getId(), g);
		g.commit();
		LoggingDailyOperations.get_instance().add_to_log("info",
				null,
				identifier_for_log,
				"successfully added this indicator:" + this.indicator_name);
		
		LoggingDailyOperations.get_instance()
		.getIndicator_operations()
		.get(this.indicator_name)
		.setSuccessfully_added_vertex_and_vertex_properties_to_graph(true);
		
		System.out.println("finished adding indicatro---<"
				+ this.indicator_name);
		
		return (Long) indicator.getId();

		/*
		 * ArrayList<ArrayList<Integer>> periods =
		 * gson.fromJson((String)(g.getVertex
		 * (indicator.getId()).getProperty("periods")),ArrayList.class);
		 * System.out.println(periods);
		 */

	}
	
	public void build_es_indicator(){
		
		//ohlcv group.
		for (String ohlcv_group : this.ohlcv_group) {
				es_indicator.getOhlcv_groups().add(ohlcv_group);
		}
		//periods
		es_indicator.build_periods(this.periods);
		
		//critical line limits.
		for(Double critical_line_limit: this.critical_line_limits){
			es_indicator.getCritical_line_limits().add(critical_line_limit);
		}
		
		//subindicator and applicable subindicators.
		for (SubIndicator subindicator : this
				.getSubindicators_applicable_to_this_indicator()) {
			es_indicator.add_applicable_subindicator(subindicator.getSubIndicator_name());
		}
		
		//cross with and monitor with.
		for(String monitor_with: this.monitor_with){
			es_indicator.add_monitor_with_indicator(monitor_with);
		}
		
		for(String cross_with: this.cross_with){
			es_indicator.add_cross_with_indicator(cross_with);
		}
		
		es_indicator.build_string_representation();
	
	}
	
	public Boolean index_es_indicator(){
		String hm = getEs_indicator().getIndicator_string_representation();
		System.out.println(hm);
		try {
			IndexResponse res = LocalEs.getInstance()
					.prepareIndex("tradegenie_titan", "indicator")
					.setSource(hm).execute().actionGet();
			if (res.getId() == null) {
				LoggingDailyOperations.get_instance()
						.getIndicator_operations().get(getIndicator_name())
						.setSuccessfully_added_vertex_and_vertex_properties_to_graph(false);
				LoggingDailyOperations.get_instance()
				.getIndicator_operations().get(getIndicator_name())
				.setFailed_in_any_respect(true);
				LoggingDailyOperations.get_instance()
				.getIndicator_operations().get(getIndicator_name())
				.addFailed_to_add_what("failed to index to es");
				return false;
			} else {
				
				System.out.println("successfully put map.");
				LoggingDailyOperations.get_instance()
				.getIndicator_operations().get(getIndicator_name())
				.setSuccessfully_added_vertex_and_vertex_properties_to_graph(true);
				return true;
			}
		} catch (Exception e) {
			LoggingDailyOperations.get_instance()
					.getSubindicator_operations().
					get(getIndicator_name()).
					setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance()
			.getSubindicator_operations()
			.get(getIndicator_name())
			.addFailed_to_add_what("failed to add to es.");
			return false;
		}
		
	}
	
	public void add_indicator_parameters_to_database(Long vertex_id,
			TitanGraph g)
			 {
		// TitanTransaction tx = g.newTransaction();
		// System.out.println("by calling method, just before adding the parameters--->");
		// System.out.println(getPeriods());
		// System.out.println("by directly calling.");
		// System.out.println(this.periods);
		// System.out.println("gson is--->" + getGson());

		TitanVertex indicator = (TitanVertex) g.getVertex(vertex_id);
		// System.out.println("here is the periods--->" +
		// getGson().toJson(this.periods));

		/**
		 * 
		 * the exception happens when for instance you change th name of a
		 * subindicator, that triggers a reinsertion of the inidcator, and at
		 * that time, given that the property already exists, this
		 * illegalargument exception is hit.
		 * 
		 * 
		 * also we dont need to reset this property , because it is a set.
		 * but 
		 * 
		 * 
		 */
		indicator.removeProperty("ohlcv_groups");
		for (String ohlcv_group : this.ohlcv_group) {
			try {
				indicator.addProperty("ohlcv_groups", ohlcv_group);
			} catch (IllegalArgumentException e) {
				if (e.getLocalizedMessage().contains("set-valued")) {

				} else {
					throw e;
				}
			}
		}

		indicator.setProperty("periods", getGson().toJson(this.periods));
		
		indicator.removeProperty("critical_line_limits");
		for (Double critical_line_limit : this.critical_line_limits) {
			try {
				indicator.addProperty("critical_line_limits",
						critical_line_limit);
			} catch (IllegalArgumentException e) {
				if (e.getLocalizedMessage().contains("set-valued")) {

				} else {
					throw e;
				}
			}
		}
		
		try{
		add_applicable_subindicators_edges(g, indicator);
		}
		catch(SubIndicatorAppliacableForIndicatorNotFoundInDatabase se){
			LoggingDailyOperations.get_instance().add_to_log("exception",
					se, identifier_for_log, null);
			LoggingDailyOperations.get_instance()
			.getIndicator_operations().get((String)indicator.getProperty("indicator_name")).setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance()
			.getIndicator_operations().get((String)indicator.getProperty("indicator_name"))
			.addFailed_to_add_what("failed while trying to add applicable subindicators," +
					" and this is the exception:" + se.getLocalizedMessage());
			
		}
		
		// add the monitor with and cross with edges.

		// tx.commit();
	}

	public void add_applicable_subindicators_edges(TitanGraph g,
			Vertex indicator_vertex)
			throws SubIndicatorAppliacableForIndicatorNotFoundInDatabase {

		for (SubIndicator subindicator : this
				.getSubindicators_applicable_to_this_indicator()) {
			Iterator<Vertex> itv = g
					.query()
					.has("subindicator_name",
							subindicator.getSubIndicator_name()).vertices()
					.iterator();
			Integer vertices_found = 0;
			while (itv.hasNext()) {
				// System.out.println("found the subindicator.");
				Vertex subindicator_vertex = itv.next();
				// now draw an edge between this vertex and the vertex of the
				// indicator.
				Edge e = g.addEdge(null, indicator_vertex, subindicator_vertex,
						"applicable_to_subindicator");
				e.setProperty("when_was_indicator_applied_to_subindicator",
						new GregorianCalendar().getTimeInMillis());

				vertices_found++;
			}
			if (vertices_found == 0) {
				throw new SubIndicatorAppliacableForIndicatorNotFoundInDatabase(
						"the subindicator-->"
								+ subindicator.getSubIndicator_name()
								+ " was not found in database, while trying to apply it to this indicator, while adding the new json file.-->"
								+ this.getIndicator_name());
			}
		}
	}

}
