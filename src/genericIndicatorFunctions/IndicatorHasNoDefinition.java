package genericIndicatorFunctions;

public class IndicatorHasNoDefinition extends Exception{
	public IndicatorHasNoDefinition(String message){
		super(message);
	}
}
