package genericIndicatorFunctions;

public class IndicatorsJsonStringIsFaulty extends Exception {

	public IndicatorsJsonStringIsFaulty(String message){
		super(message);
	}

}
