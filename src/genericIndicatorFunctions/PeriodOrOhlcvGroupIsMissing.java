package genericIndicatorFunctions;

public class PeriodOrOhlcvGroupIsMissing extends Exception {
	public PeriodOrOhlcvGroupIsMissing(String message){
		super(message);
	}
}
