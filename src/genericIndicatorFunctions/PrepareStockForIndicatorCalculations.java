package genericIndicatorFunctions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTime;

import Titan.GroovyStatements;
import singletonClass.CalendarFunctions;
import eu.verdelhan.ta4j.Indicator;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import yahoo_finance_historical_scraper.HistoricalStock;

public class PrepareStockForIndicatorCalculations {
	
	/**
	 * 
	 * timezone
	 * 
	 * 
	 */
	
	private TimeZone tz;
	


	/**
	 * 
	 * 
	 * entity type
	 * 
	 */
	
	private String entityType;
	
	
	/**
	 * 
	 * name of stock
	 */
	private String stockName;
	
	/**
	 * 
	 * name of indice
	 */
	private String indice;
	
	/**
	 * fullname of stock
	 * 
	 */
	private String stockFullName;
	
	
	/**
	 * 
	 * start date- calendar object for timeseries
	 * 
	 */
	
	private Calendar start_date;
	
	
	/**
	 * 
	 * end date - calendar object for timeseries.
	 * 
	 */
	
	private Calendar end_date;
	
	/**
	 *
	 * 
	 * simple historical stock object containing all the ohlc data for the last 300 days.
	 * 
	 */
	 private HistoricalStock historical_stock_object;
	
	
	/**
	 * a hashmap containing the previous values for all parameters of the indicators that we need to calculate
	 * for this stock.
	 * 
	 * name of indicator -> hashmap containing 
	 * => name of subcalculation =>  arraylist of values over entire 300 day period. 
	 *  
	 * 
	 */
	 private HashMap<String,HashMap<String,ArrayList<Double>>>indicator_previous_300_parameters;
	 
	 
	/**
	 * 
	 * the timeseries upto 300 days before now, plus inclusive of now. 
	 * 
	 */
	 private TimeSeries ts;
	 
	 /**
	  * 
	  * a hashmap of the structure, for the indicators that we need to calculate for this stock.
	  * name of indicator -> [is it rising, how many days, .....]
	  * 
	  */
	 private HashMap<Long,ArrayList<String>> required_indicators_and_their_sub_calculations;
	
	 
	 
	 
	 public String getStockName() {
		return stockName;
	}



	public void setStockName(String stockName) {
		this.stockName = stockName;
	}



	public String getIndice() {
		return indice;
	}



	public void setIndice(String indice) {
		this.indice = indice;
	}



	public String getStockFullName() {
		return stockFullName;
	}



	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}



	public Calendar getStart_date() {
		return start_date;
	}



	public void setStart_date(Calendar start_date) {
		this.start_date = start_date;
	}



	public Calendar getEnd_date() {
		return end_date;
	}



	public void setEnd_date(Calendar end_date) {
		this.end_date = end_date;
	}



	public HashMap<String, HashMap<String, ArrayList<Double>>> getIndicator_previous_300_parameters() {
		return indicator_previous_300_parameters;
	}



	public void setIndicator_previous_300_parameters(
			HashMap<String, HashMap<String, ArrayList<Double>>> indicator_previous_300_parameters) {
		this.indicator_previous_300_parameters = indicator_previous_300_parameters;
	}



	public TimeSeries getTs() {
		return ts;
	}



	public void setTs(TimeSeries ts) {
		this.ts = ts;
	}
	
	public TimeZone getTz() {
		return tz;
	}



	public void setTz(TimeZone tz) {
		this.tz = tz;
	}



	public HashMap<Long, ArrayList<String>> getRequired_indicators_and_their_sub_calculations() {
		return required_indicators_and_their_sub_calculations;
	}



	public void setRequired_indicators_and_their_sub_calculations(
			HashMap<Long, ArrayList<String>> required_indicators_and_their_sub_calculations) {
		this.required_indicators_and_their_sub_calculations = required_indicators_and_their_sub_calculations;
	}



	public PrepareStockForIndicatorCalculations(String stockName, String stockFullName, String indice, HashMap<Long,ArrayList<String>> 
	 required_indicators_and_their_sub_calculations, TimeZone tz, Calendar start_date, Calendar end_date, String entityType) 
			 throws HistoricalStockObjectDoesNotContainClose{
		 
		 setStockFullName(stockFullName);
		 setStockName(stockName);
		 setIndice(indice);
		 setRequired_indicators_and_their_sub_calculations(required_indicators_and_their_sub_calculations);
		 setStart_date(start_date);
		 setEnd_date(end_date);
		 setEntityType(entityType);
		 setTz(tz);
		 setHistorical_stock_object(generate_historical_stock_object());
		 setTs(create_timeseries_from_historical_stock_arraylist());
		 
	}
	 
	

	public HistoricalStock getHistorical_stock_object() {
		return historical_stock_object;
	}



	public void setHistorical_stock_object(HistoricalStock historical_stock_object) {
		this.historical_stock_object = historical_stock_object;
	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}



	public HistoricalStock generate_historical_stock_object(){
		
		GroovyStatements gs = GroovyStatements.getInstance();
		HistoricalStock hs = gs.get_historical_stock_object_for_dates(getStart_date(),
				getEnd_date(), getStockName(), getStockFullName(), getIndice() ,getTz(), getEntityType());
		return hs;
		
	}
	

	public TimeSeries create_timeseries_from_historical_stock_arraylist() throws HistoricalStockObjectDoesNotContainClose{
		 ArrayList<Tick> tick_holder = new ArrayList<Tick>();
		 //go over open high, low close.
		 Integer counter = 0;
		 for(Long epoch:getHistorical_stock_object().getEpoches()){
			 Double open = getHistorical_stock_object().getOpen().get(counter);
			 if(open == null){
				 open = 0.0;
			 }
			 
			 Double high = getHistorical_stock_object().getHigh().get(counter);
			 if(high == null){
				 high = 0.0;
			 }
			 
			 Double low = getHistorical_stock_object().getLow().get(counter);
			 if(low == null){
				 low = 0.0;
			 }
			 
			 Double close = getHistorical_stock_object().getclose().get(counter);
			 if(close == null){
				 throw new HistoricalStockObjectDoesNotContainClose("the following stock-->" + getHistorical_stock_object().getStockFullName() + " does not contain a close on ---> " + new CalendarFunctions(getHistorical_stock_object().getDates().get(counter)).getFullDate());
			 }
			 
			 Long volume = getHistorical_stock_object().getVolume().get(counter);
			 if(volume == null){
				 volume = 0l;
			 }
			 
			 Tick tick = new Tick(new DateTime(epoch), open, high, low, close, volume.doubleValue());
			 tick_holder.add(tick);
			 
			 counter ++;
		 }
		 
		 return new TimeSeries(tick_holder);
		 
	 }
	
	 //create the querystrings for the previous three hundred indicators.
	 
}
