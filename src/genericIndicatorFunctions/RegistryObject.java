package genericIndicatorFunctions;

import Titan.MultiThreadable;

public class RegistryObject extends MultiThreadable {

	private String registry_component_md5_hash;
	private String registry_component_jsonified;
	private Integer registry_counter;

	public String getRegistry_component_md5_hash() {
		return registry_component_md5_hash;
	}

	public void setRegistry_component_md5_hash(
			String registry_component_md5_hash) {
		this.registry_component_md5_hash = registry_component_md5_hash;
	}

	public String getRegistry_component_jsonified() {
		return registry_component_jsonified;
	}

	public void setRegistry_component_jsonified(
			String registry_component_jsonified) {
		this.registry_component_jsonified = registry_component_jsonified;
	}

	public Integer getRegistry_counter() {
		return registry_counter;
	}

	public void setRegistry_counter(Integer registry_counter) {
		this.registry_counter = registry_counter;
	}

	public RegistryObject(String registry_hash, String jsonified,
			Integer registry_counter) {
		this.registry_component_md5_hash = registry_hash;
		this.registry_component_jsonified = jsonified;
		this.registry_counter = registry_counter;
	}

}
