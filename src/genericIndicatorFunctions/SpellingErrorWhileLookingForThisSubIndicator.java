package genericIndicatorFunctions;

public class SpellingErrorWhileLookingForThisSubIndicator extends Exception {
	public SpellingErrorWhileLookingForThisSubIndicator(String message){
		super(message);
	}
}
