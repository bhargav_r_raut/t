package genericIndicatorFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.codec.digest.DigestUtils;
import org.elasticsearch.action.index.IndexResponse;

import DailyOperationsLogging.LoggingDailyOperations;
import ExchangeBuilder.CentralSingleton;
import Titan.GroovyStatements;

import com.google.gson.Gson;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.util.structures.Row;

import elasticSearchNative.LocalEs;

public class SubIndicator {
	/*
	 * { "indicator_rising_111": { "indicator_name": "indicator_rising",
	 * "self_groups": [], "self_subgroups": [], "self_specific_groups": [],
	 * "opposite_groups": [], "opposite_subgroups": [],
	 * "opposite_specific_groups": [], "ids_of_applicable_indicators": [] } }
	 */

	private EsSubIndicator es_subindicator;

	public EsSubIndicator getEs_subindicator() {
		return es_subindicator;
	}

	public void setEs_subindicator(EsSubIndicator es_subindicator) {
		this.es_subindicator = es_subindicator;
	}

	private ArrayList<SubIndicator> failed_to_add_groups;

	public ArrayList<SubIndicator> getFailed_to_add_groups() {
		return failed_to_add_groups;
	}

	public void setFailed_to_add_groups(
			ArrayList<SubIndicator> failed_to_add_groups) {
		this.failed_to_add_groups = failed_to_add_groups;
	}

	public ArrayList<SubIndicator> getFailed_to_add_subgroups() {
		return failed_to_add_subgroups;
	}

	public void setFailed_to_add_subgroups(
			ArrayList<SubIndicator> failed_to_add_subgroups) {
		this.failed_to_add_subgroups = failed_to_add_subgroups;
	}

	public ArrayList<SubIndicator> getFailed_to_add_opposite_groups() {
		return failed_to_add_opposite_groups;
	}

	public void setFailed_to_add_opposite_groups(
			ArrayList<SubIndicator> failed_to_add_opposite_groups) {
		this.failed_to_add_opposite_groups = failed_to_add_opposite_groups;
	}

	public ArrayList<SubIndicator> getFailed_to_add_opposite_subgroups() {
		return failed_to_add_opposite_subgroups;
	}

	public void setFailed_to_add_opposite_subgroups(
			ArrayList<SubIndicator> failed_to_add_opposite_subgroups) {
		this.failed_to_add_opposite_subgroups = failed_to_add_opposite_subgroups;
	}

	public ArrayList<SubIndicator> getFailed_to_add_opposite_specific_subindicators() {
		return failed_to_add_opposite_specific_subindicators;
	}

	public void setFailed_to_add_opposite_specific_subindicators(
			ArrayList<SubIndicator> failed_to_add_opposite_specific_subindicators) {
		this.failed_to_add_opposite_specific_subindicators = failed_to_add_opposite_specific_subindicators;
	}

	private ArrayList<SubIndicator> failed_to_add_subgroups;
	private ArrayList<SubIndicator> failed_to_add_opposite_groups;
	private ArrayList<SubIndicator> failed_to_add_opposite_subgroups;
	private ArrayList<SubIndicator> failed_to_add_opposite_specific_subindicators;

	private String name;
	private ArrayList<ArrayList<String>> self_groups;

	public ArrayList<ArrayList<String>> getSelf_groups() {
		return self_groups;
	}

	public void setSelf_groups(ArrayList<ArrayList<String>> self_groups) {
		this.self_groups = self_groups;
	}

	public String getSubIndicator_name() {
		return name;
	}

	public void setSubIndicator_name(String subindicator_name) {
		this.name = subindicator_name;
	}

	public ArrayList<ArrayList<ArrayList<String>>> getSelf_sub_groups() {
		return self_sub_groups;
	}

	public void setSelf_sub_groups(
			ArrayList<ArrayList<ArrayList<String>>> self_sub_groups) {
		this.self_sub_groups = self_sub_groups;
	}

	public ArrayList<ArrayList<ArrayList<String>>> getSelf_specific_groups() {
		return self_specific_groups;
	}

	public void setSelf_specific_groups(
			ArrayList<ArrayList<ArrayList<String>>> self_specific_groups) {
		this.self_specific_groups = self_specific_groups;
	}

	public ArrayList<ArrayList<ArrayList<String>>> getOpposite_groups() {
		return opposite_groups;
	}

	public void setOpposite_groups(
			ArrayList<ArrayList<ArrayList<String>>> opposite_groups) {
		this.opposite_groups = opposite_groups;
	}

	public ArrayList<String> getNames_of_applicable_indicators() {
		return names_of_applicable_indicators;
	}

	public void setNames_of_applicable_indicators(
			ArrayList<String> names_of_applicable_indicators) {
		this.names_of_applicable_indicators = names_of_applicable_indicators;
	}

	public String getUnique_identifier() {
		return unique_identifier;
	}

	public void setUnique_identifier(String unique_identifier) {
		this.unique_identifier = unique_identifier;
	}

	public String getSubindicator() {
		return subindicator;
	}

	public void setSubindicator(String subindicator) {
		this.subindicator = subindicator;
	}

	/**
	 * main_group -> subgroup -> component 1 component 2
	 * 
	 * 
	 * 
	 */
	private ArrayList<ArrayList<ArrayList<String>>> self_sub_groups;

	private ArrayList<ArrayList<ArrayList<String>>> self_specific_groups;

	/**
	 * group -> opposite group one -> component 1 component 2 opposite group two
	 * -> ...
	 * 
	 */
	private ArrayList<ArrayList<ArrayList<String>>> opposite_groups;

	/**
	 * main_group -> subgroup 1 -> opposite subgroup 1 -> component 1 component
	 * 2
	 * 
	 * opposite subgroup 2
	 * 
	 * subgroup 2 -> opposite subgroup 1 opposite subgroup 2 subgroup 3 ->
	 * 
	 * 
	 * 
	 */
	private ArrayList<ArrayList<ArrayList<ArrayList<String>>>> opposite_subgroups;

	/**
	 * main_group -> subgroup 1 -> opposite subgroup 1(from first opposite group
	 * of main group) --> opposite component.
	 * 
	 * 
	 * opposite subgroup 2(from second opposite group of main group) -->
	 * opposite component.
	 * 
	 * subgroup 2 -> opposite subgroup 1 opposite subgroup 2 subgroup 3 ->
	 * 
	 * 
	 * 
	 */
	private ArrayList<ArrayList<ArrayList<String>>> opposite_specific_groups;
	private ArrayList<String> names_of_applicable_indicators;
	private String unique_identifier;
	private String subindicator;

	public void generate_unique_identifier() {
		this.unique_identifier = this.name
				+ String.valueOf(new GregorianCalendar().getTimeInMillis());
		this.subindicator = "subindicator";
	}

	/**
	 * @param subindicator_name
	 */
	public SubIndicator(String subindicator_name) {
		this.name = subindicator_name;
		this.self_groups = new ArrayList<ArrayList<String>>();
		this.self_sub_groups = new ArrayList<ArrayList<ArrayList<String>>>();
		this.self_specific_groups = new ArrayList<ArrayList<ArrayList<String>>>();
		this.opposite_groups = new ArrayList<ArrayList<ArrayList<String>>>();
		this.opposite_subgroups = new ArrayList<ArrayList<ArrayList<ArrayList<String>>>>();
		this.opposite_specific_groups = new ArrayList<ArrayList<ArrayList<String>>>();
		this.es_subindicator = new EsSubIndicator(subindicator_name);

	}

	public String hashit() {
		return DigestUtils.md5Hex(this.toString());
	}

	public ArrayList<ArrayList<ArrayList<ArrayList<String>>>> getOpposite_subgroups() {
		return opposite_subgroups;
	}

	public void setOpposite_subgroups(
			ArrayList<ArrayList<ArrayList<ArrayList<String>>>> opposite_subgroups) {
		this.opposite_subgroups = opposite_subgroups;
	}

	public ArrayList<ArrayList<ArrayList<String>>> getOpposite_specific_groups() {
		return opposite_specific_groups;
	}

	public void setOpposite_specific_groups(
			ArrayList<ArrayList<ArrayList<String>>> opposite_specific_groups) {
		this.opposite_specific_groups = opposite_specific_groups;
	}

	public void add_self_group(ArrayList<String> self_group) {
		this.self_groups.add(self_group);
	}

	public void add_self_subgroup(ArrayList<ArrayList<String>> self_subgroup) {
		this.self_sub_groups.add(self_subgroup);
	}

	public void add_self_specific_group(
			ArrayList<ArrayList<String>> self_specific_group) {
		this.self_specific_groups.add(self_specific_group);
	}

	public void add_opposite_group(ArrayList<String> opposite_group) {
		this.self_groups.add(opposite_group);
	}

	public void add_opposite_subgroup(
			ArrayList<ArrayList<ArrayList<String>>> opposite_subgroup) {
		this.opposite_subgroups.add(opposite_subgroup);
	}

	public void add_opposite_specific_group(
			ArrayList<ArrayList<String>> opposite_specific_group) {
		this.opposite_specific_groups.add(opposite_specific_group);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		StringBuilder sb = new StringBuilder();
		sb.append("name of indicator is --->" + getSubIndicator_name());
		sb.append("\n");
		sb.append("the self_groups are--->" + getSelf_groups().toString());
		sb.append("\n");
		sb.append("the self_sub_groups_are ---> "
				+ getSelf_sub_groups().toString());
		sb.append("\n");
		sb.append("teh self specific groups are-->"
				+ getSelf_specific_groups().toString());
		sb.append("\n");
		sb.append("the opposite groups are--->"
				+ getOpposite_groups().toString());
		sb.append("\n");
		sb.append("the opposite specific groups are--->"
				+ getOpposite_specific_groups().toString());
		sb.append("\n");
		sb.append("the opposite ssub groups are--->"
				+ getOpposite_subgroups().toString());
		return sb.toString();
	}

	public void remove_all_outgoing_edges(TitanGraph g, Vertex v) {
		// TitanTransaction tx = g.newTransaction();
		ArrayList<Edge> outgoing_edges = GroovyStatements.getInstance()
				.get_outgoing_edges(v);
		for (Edge e : outgoing_edges) {
			g.removeEdge(e);

		}
		g.commit();
		// tx.commit();
	}

	public Long add_subindicator_vertex_to_database(TitanGraph g, Gson gson)
			throws Exception {
		// if a subindicator with this name already exists then we just return
		// its id, otherwise
		// we create a new one.
		Iterator<Vertex> v = g.getVertices("subindicator_name",
				this.getSubIndicator_name()).iterator();
		while (v.hasNext()) {
			Vertex existing_subindicator_vertex = v.next();
			remove_all_outgoing_edges(g, existing_subindicator_vertex);

			return (Long) existing_subindicator_vertex.getId();
		}

		Vertex new_subindicator_vertex = g.addVertexWithLabel("subindicator");
		new_subindicator_vertex.setProperty("subindicator_name",
				this.getSubIndicator_name());
		g.commit();
		return (Long) new_subindicator_vertex.getId();

	}

	public void add_opposite_groups_to_graph(Integer group_id,
			HashMap<String, Long> vertex_ids_of_subindicators,
			Long vertex_id_of_current_subindicator, TitanGraph g,
			EsGroup es_group) {
		Integer opposite_group_index = 0;
		// System.out.println(getOpposite_groups().toString());

		if (this.getOpposite_groups().size() > 0) {

			for (ArrayList<String> opposite_group : this.getOpposite_groups()
					.get(group_id)) {

				/***
				 * additions to add to es group representation.
				 */
				es_group.getOpposite_group_numeric_ids().add(
						CentralSingleton.getInstance()
								.get_counter_for_element_or_index_it(
										opposite_group));
				/**
				 * changes end here.
				 * 
				 */

				/*
				 * String hex_id_of_opposite_group = DigestUtils
				 * .sha256Hex(opposite_group.toString()); Integer component_id =
				 * 0; for (String subindicator_of_opposite_group :
				 * opposite_group) { Long vertex_id_of_component =
				 * vertex_ids_of_subindicators
				 * .get(subindicator_of_opposite_group); Edge e =
				 * g.addEdge(null,
				 * g.getVertex(vertex_id_of_current_subindicator),
				 * g.getVertex(vertex_id_of_component),
				 * "subindicator_opposite_group");
				 * e.setProperty("subindicator_opposite_group_id",
				 * opposite_group_index); e.setProperty("subindicator_group_id",
				 * group_id); e.setProperty("component_id", component_id);
				 * g.commit(); LoggingDailyOperations .get_instance()
				 * .getSubindicator_operations() .get(getSubIndicator_name())
				 * .put_edge_type_id("opposite_groups", e.getId().toString());
				 * component_id++; }
				 * 
				 * opposite_group_index++;
				 */
			}
		}
	}

	public void add_self_sub_groups_to_graph(Integer group_id,
			HashMap<String, Long> vertex_ids_of_subindicators,
			Long vertex_id_of_current_subindicator, TitanGraph g,
			EsGroup es_group) throws InterruptedException {
		Integer sub_group_id = 0;
		// System.out.println("all the subgroups are--->");
		// System.out.println(getSelf_sub_groups().toString());
		// System.out.println("of the first group they are---->");
		// System.out.println(getSelf_sub_groups().get(group_id));

		for (ArrayList<String> sub_group : getSelf_sub_groups().get(group_id)) {
			// /System.out.println("the " + sub_group_id + " is -->  " +
			// sub_group);
			EsSubGroup es_subgroup = new EsSubGroup(CentralSingleton
					.getInstance().get_counter_for_element_or_index_it(
							sub_group));
			/*
			 * Integer component_id = 0; for (String
			 * subindicator_of_self_sub_group : sub_group) {
			 * 
			 * Long vertex_id_of_component = vertex_ids_of_subindicators
			 * .get(subindicator_of_self_sub_group); Edge e = g.addEdge(null,
			 * g.getVertex(vertex_id_of_current_subindicator),
			 * g.getVertex(vertex_id_of_component), "subindicator_subgroup");
			 * e.setProperty("subindicator_subgroup_id", sub_group_id);
			 * e.setProperty("subindicator_group_id", group_id);
			 * e.setProperty("component_id", component_id);
			 * 
			 * g.commit(); LoggingDailyOperations .get_instance()
			 * .getSubindicator_operations() .get(getSubIndicator_name())
			 * .put_edge_type_id("self_sub_group", e.getId().toString());
			 * 
			 * System.out.println("commited the edge");
			 * 
			 * component_id++; }
			 */
			// here add the opposite subgroups.
			try {
				//System.out.println("adding opposite sub groups.");
				add_opposite_sub_groups(group_id, sub_group_id,
						vertex_ids_of_subindicators,
						vertex_id_of_current_subindicator, g, es_subgroup);
				//System.out.println("finished adding opposite sub groups.");
			} catch (Exception e) {
				LoggingDailyOperations.get_instance()
						.getSubindicator_operations()
						.get(getSubIndicator_name()).addExceptions(e);
				failed_to_add_opposite_subgroups.add(this);
			}
			// here add opposite specific subindicator names
			try {
				//System.out.println("adding opposite specific groups.");
				add_opposite_specific_groups(group_id, sub_group_id,
						vertex_ids_of_subindicators,
						vertex_id_of_current_subindicator, g, es_subgroup);
				//System.out.println("finished adding opposite specific groups.");

			} catch (Exception e) {
				LoggingDailyOperations.get_instance()
						.getSubindicator_operations()
						.get(getSubIndicator_name()).addExceptions(e);
				failed_to_add_opposite_specific_subindicators.add(this);
			}
			es_group.getSubgroups().add(es_subgroup);
			sub_group_id++;
		}
	}

	public void add_opposite_specific_groups(Integer group_id,
			Integer sub_group_id,
			HashMap<String, Long> vertex_ids_of_subindicators,
			Long vertex_id_of_current_subindicator, TitanGraph g,
			EsSubGroup es_subgroup) {
		if (getOpposite_groups().size() > 0) {
			ArrayList<String> opposite_specific_subindicators = getOpposite_specific_groups()
					.get(group_id).get(sub_group_id);
			/**
			 * the opposite group id is the same as the component here.
			 * 
			 */
			Integer component = 0;
			for (String opposite_subindicator : opposite_specific_subindicators) {

				es_subgroup
						.getOpposite_subgroups()
						.get(component)
						.setOpposite_specific_subindicator_id(
								CentralSingleton
										.getInstance()
										.get_counter_for_element_or_index_it(
												new ArrayList<String>(
														Arrays.asList(opposite_subindicator))));
				/*
				 * Long vertex_id_of_component = vertex_ids_of_subindicators
				 * .get(opposite_subindicator); Edge e = g.addEdge(null,
				 * g.getVertex(vertex_id_of_current_subindicator),
				 * g.getVertex(vertex_id_of_component),
				 * "subindicator_opposite_specific_subindicator");
				 * e.setProperty("subindicator_subgroup_id", sub_group_id);
				 * e.setProperty("subindicator_group_id", group_id);
				 * e.setProperty("component_id", component); g.commit();
				 * LoggingDailyOperations .get_instance()
				 * .getSubindicator_operations() .get(getSubIndicator_name())
				 * .put_edge_type_id("opposite_specific_group",
				 * e.getId().toString());
				 */
				component++;
			}
		}
	}

	public void add_opposite_sub_groups(Integer group_id, Integer sub_group_id,
			HashMap<String, Long> vertex_ids_of_subindicators,
			Long vertex_id_of_current_subindicator, TitanGraph g,
			EsSubGroup es_subgroup) {
		if (getOpposite_groups().size() > 0) {
			ArrayList<ArrayList<String>> opposite_subgroups = getOpposite_subgroups()
					.get(group_id).get(sub_group_id);
			// this is correct because the opposite subgroup number in the array
			// is
			// the
			// same as the opposite group to which that subgroup belongs.
			Integer opposite_group_number = 0;
			for (ArrayList<String> opposite_subgroup : opposite_subgroups) {
				EsOppositeSubGroup es_opposite_subgroup = new EsOppositeSubGroup(
						CentralSingleton.getInstance()
								.get_counter_for_element_or_index_it(
										opposite_subgroup));
				es_opposite_subgroup.setOpposite_subgroup_components(opposite_subgroup);
				es_subgroup.getOpposite_subgroups().add(es_opposite_subgroup);
				/*
				 * Integer component_id = 0; for (String
				 * opposite_subindicator_in_subgroup : opposite_subgroup) { Long
				 * vertex_id_of_component = vertex_ids_of_subindicators
				 * .get(opposite_subindicator_in_subgroup); Edge e =
				 * g.addEdge(null,
				 * g.getVertex(vertex_id_of_current_subindicator),
				 * g.getVertex(vertex_id_of_component),
				 * "subindicator_opposite_subgroup");
				 * e.setProperty("component_id", component_id);
				 * e.setProperty("subindicator_opposite_group_id",
				 * opposite_group_number);
				 * e.setProperty("subindicator_subgroup_id", sub_group_id);
				 * e.setProperty("subindicator_group_id", group_id); g.commit();
				 * LoggingDailyOperations .get_instance()
				 * .getSubindicator_operations() .get(getSubIndicator_name())
				 * .put_edge_type_id("opposite_subgroups",
				 * e.getId().toString()); component_id++; }
				 */
				opposite_group_number++;
			}
		}
	}

	public void generate_es_subindicator(
			ArrayList<SubIndicator> failed_subindicators,
			ArrayList<SubIndicator> failed_to_add_groups,
			ArrayList<SubIndicator> failed_to_add_subgroups,
			ArrayList<SubIndicator> failed_to_add_opposite_groups,
			ArrayList<SubIndicator> failed_to_add_opposite_subgroups,
			ArrayList<SubIndicator> failed_to_add_opposite_specific_subindicators) {

		this.failed_to_add_groups = failed_to_add_groups;
		this.failed_to_add_subgroups = failed_to_add_subgroups;
		this.failed_to_add_opposite_groups = failed_to_add_opposite_groups;
		this.failed_to_add_opposite_subgroups = failed_to_add_opposite_subgroups;
		this.failed_to_add_opposite_specific_subindicators = failed_to_add_opposite_specific_subindicators;

		try {
			Integer group_id = 0;
			for (ArrayList<String> self_group : getSelf_groups()) {
				Integer component_id = 0;
				
				EsGroup esgroup = new EsGroup(CentralSingleton.getInstance()
						.get_counter_for_element_or_index_it(self_group));

				for (String component_of_group : self_group) {
					// draw edge between it and the group.
					//System.out.println("indexing component of group");
					esgroup.getSelf_component_numeric_ids()
							.add(CentralSingleton
									.getInstance()
									.get_counter_for_element_or_index_it(
											new ArrayList<String>(Arrays
													.asList(component_of_group))));

					component_id++;
				}
				try {
					//System.out.println("adding opposite groups to graph.");
					add_opposite_groups_to_graph(group_id, null, null, null,
							esgroup);
					//System.out.println("adding opposite groups to graph ends.");
				} catch (Exception e1) {
					e1.printStackTrace();
					try {
						System.in.read();
					} catch (IOException e11) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					LoggingDailyOperations.get_instance()
							.getSubindicator_operations()
							.get(getSubIndicator_name()).addExceptions(e1);
					failed_to_add_opposite_groups.add(this);
				}
				try {
					// System.out.println("came to add self sub groups.");
					//System.out.println("adding self sub groups to graph.");
					add_self_sub_groups_to_graph(group_id, null, null, null,
							esgroup);
					//System.out.println("finished adding self sub groups to graph.");
				} catch (Exception e2) {
					e2.printStackTrace();
					try {
						System.in.read();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					LoggingDailyOperations.get_instance()
							.getSubindicator_operations()
							.get(getSubIndicator_name()).addExceptions(e2);
					failed_to_add_opposite_specific_subindicators.add(this);
					failed_to_add_opposite_subgroups.add(this);
					failed_to_add_opposite_subgroups.add(this);
				}
				getEs_subindicator().getGroups().add(
						esgroup.build_group_representation());
				group_id++;
			}

			//System.out.println(getEs_subindicator().get_json_representation());
			//System.in.read();

		} catch (Exception e) {
			e.printStackTrace();
			try {
				System.in.read();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LoggingDailyOperations.get_instance().getSubindicator_operations()
					.get(getSubIndicator_name()).addExceptions(e);
			failed_to_add_groups.add(this);
			failed_to_add_opposite_groups.add(this);
			failed_to_add_opposite_specific_subindicators.add(this);
			failed_to_add_opposite_subgroups.add(this);
			failed_to_add_opposite_subgroups.add(this);
		}
	}

	/**
	 * 
	 * 
	 * 
	 "opposite_subgroups": [
	 * 
	 * =>first group [ =>first sub group in this group. [ =>first opposite
	 * subggroup to this subgroup - corresponds to the first opposite group to
	 * this group [ "indicator_falling_for_4_days",
	 * "indicator_falling_for_5_days" ] =>maybe there is number two -
	 * corresponds to the second opposite group to this group. ], =>second sub
	 * group in this group. [ =>first opposite subgroup to this subgroup. [
	 * "indicator_falling_for_5_days", "indicator_falling_for_6_days" ] ] ]
	 * =>second group .... ],
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	/*
	 * public void add_subindicator_params_to_database( TitanGraph g, Gson gson,
	 * HashMap<String, Long> vertex_ids_of_subindicators,
	 * ArrayList<SubIndicator> successfull_subindicators,
	 * ArrayList<SubIndicator> failed_subindicators, ArrayList<SubIndicator>
	 * failed_to_add_groups, ArrayList<SubIndicator> failed_to_add_subgroups,
	 * ArrayList<SubIndicator> failed_to_add_opposite_groups,
	 * ArrayList<SubIndicator> failed_to_add_opposite_subgroups,
	 * ArrayList<SubIndicator> failed_to_add_opposite_specific_subindicators
	 * 
	 * ) throws Exception {
	 * 
	 * 
	 * 
	 * this.failed_to_add_groups = failed_to_add_groups;
	 * this.failed_to_add_subgroups = failed_to_add_subgroups;
	 * this.failed_to_add_opposite_groups = failed_to_add_opposite_groups;
	 * this.failed_to_add_opposite_subgroups = failed_to_add_opposite_subgroups;
	 * this.failed_to_add_opposite_specific_subindicators =
	 * failed_to_add_opposite_specific_subindicators;
	 * 
	 * 
	 * 
	 * 
	 * Long vertex_id_of_current_subindicator = vertex_ids_of_subindicators
	 * .get(this.getSubIndicator_name());
	 * 
	 * try { Integer group_id = 0; for (ArrayList<String> self_group :
	 * getSelf_groups()) { Integer component_id = 0;
	 * 
	 * EsGroup esgroup = new EsGroup(GroovyStatements.getInstance()
	 * .get_counter_for_element_or_index_it(self_group));
	 * 
	 * 
	 * for (String component_of_group : self_group) {
	 * 
	 * esgroup.getSelf_component_numeric_ids() .add(GroovyStatements
	 * .getInstance() .get_counter_for_element_or_index_it( new
	 * ArrayList<String>(Arrays .asList(component_of_group))));
	 * 
	 * Long vertex_id_of_component = vertex_ids_of_subindicators
	 * .get(component_of_group); Edge e = g.addEdge(null,
	 * g.getVertex(vertex_id_of_current_subindicator),
	 * g.getVertex(vertex_id_of_component), "subindicator_group");
	 * e.setProperty("subindicator_group_id", group_id);
	 * e.setProperty("component_id", component_id); g.commit();
	 * 
	 * LoggingDailyOperations .get_instance() .getSubindicator_operations()
	 * .get(getSubIndicator_name()) .put_edge_type_id("self_group",
	 * e.getId().toString()); // now add the opposite groups.
	 * 
	 * component_id++; } try { add_opposite_groups_to_graph(group_id,
	 * vertex_ids_of_subindicators, vertex_id_of_current_subindicator, g,
	 * esgroup); } catch (Exception e1) { LoggingDailyOperations.get_instance()
	 * .getSubindicator_operations()
	 * .get(getSubIndicator_name()).addExceptions(e1);
	 * failed_to_add_opposite_groups.add(this); } try { //
	 * System.out.println("came to add self sub groups.");
	 * add_self_sub_groups_to_graph(group_id, vertex_ids_of_subindicators,
	 * vertex_id_of_current_subindicator, g, esgroup); } catch (Exception e2) {
	 * LoggingDailyOperations.get_instance() .getSubindicator_operations()
	 * .get(getSubIndicator_name()).addExceptions(e2);
	 * failed_to_add_opposite_specific_subindicators.add(this);
	 * failed_to_add_opposite_subgroups.add(this);
	 * failed_to_add_opposite_subgroups.add(this); }
	 * getEs_subindicator().getGroups
	 * ().add(esgroup.build_group_representation()); group_id++; }
	 * 
	 * } catch (Exception e) {
	 * LoggingDailyOperations.get_instance().getSubindicator_operations()
	 * .get(getSubIndicator_name()).addExceptions(e);
	 * failed_to_add_groups.add(this); failed_to_add_opposite_groups.add(this);
	 * failed_to_add_opposite_specific_subindicators.add(this);
	 * failed_to_add_opposite_subgroups.add(this);
	 * failed_to_add_opposite_subgroups.add(this); }
	 * 
	 * //now index it to elasticsearch. //index_subindicator_to_es(); }
	 */

	public Boolean index_subindicator_to_es() {
		String hm = getEs_subindicator().get_json_representation();
		//System.out.println(hm);
		try {
			IndexResponse res = LocalEs.getInstance()
					.prepareIndex("tradegenie_titan", "subindicator")
					.setSource(hm).execute().actionGet();
			if (res.getId() == null) {
				LoggingDailyOperations.get_instance()
				.add_to_log("exception", new Exception("failed to add an subindicator"),
						"subindicator.java", null);
				LoggingDailyOperations.get_instance()
						.getSubindicator_operations().get(getSubIndicator_name())
						.setAdded_vertex_to_graph(false);
				LoggingDailyOperations.get_instance()
				.getSubindicator_operations().get(getSubIndicator_name()).setFailed_in_any_respect(true);
				LoggingDailyOperations.get_instance()
				.getSubindicator_operations().get(getSubIndicator_name())
				.addFailed_to_add_what("failed to index into es");
				return false;
			} else {
				
				//System.out.println("successfully put map.");
				LoggingDailyOperations.get_instance()
				.getSubindicator_operations().get(getSubIndicator_name())
				.setAdded_vertex_to_graph(true);
				return true;
			}
		} catch (Exception e) {
			LoggingDailyOperations.get_instance()
			.add_to_log("exception", e,
				"subindicator.java", null);
			LoggingDailyOperations.get_instance()
			.getSubindicator_operations().get(getSubIndicator_name())
			.setAdded_vertex_to_graph(false);
			LoggingDailyOperations.get_instance()
			.getSubindicator_operations().get(getSubIndicator_name()).setFailed_in_any_respect(true);
			LoggingDailyOperations.get_instance()
			.getSubindicator_operations().get(getSubIndicator_name())
			.addFailed_to_add_what("failed with exception in adding to es, but not es failure.");
			return false;
			
		}
	}

	public void assemble_group(String label, TitanGraph g,
			SubIndicator existing_subindicator_in_database) {
		// System.out.println("came to assemble the group");
		ArrayList<Edge> ordered_group_edges = GroovyStatements.getInstance()
				.assemble_group(existing_subindicator_in_database);

		HashSet<Integer> group_ids = new HashSet<Integer>();

		for (Edge e : ordered_group_edges) {

			Vertex related_subindicator = e.getVertex(Direction.IN);

			Integer subindicator_group_id = (Integer) e
					.getProperty("subindicator_group_id");
			if (group_ids.contains(subindicator_group_id)) {

			} else {
				existing_subindicator_in_database.getSelf_groups().add(
						new ArrayList<String>());
				group_ids.add(subindicator_group_id);
			}
			existing_subindicator_in_database
					.getSelf_groups()
					.get(existing_subindicator_in_database.getSelf_groups()
							.size() - 1)
					.add((String) related_subindicator
							.getProperty("subindicator_name"));
		}

	}

	public void assemble_opposite_group(String label, TitanGraph g,
			SubIndicator existing_subindicator_in_database) {
		ArrayList<Edge> ordered_opposite_group_edges = GroovyStatements
				.getInstance().assemble_opposite_group(
						existing_subindicator_in_database);
		HashSet<Integer> group_ids_1 = new HashSet<Integer>();
		HashSet<Integer> opposite_group_ids_1 = new HashSet<Integer>();

		for (Edge e : ordered_opposite_group_edges) {
			Vertex related_subindicator = e.getVertex(Direction.IN);

			Integer subindicator_group_id = (Integer) e
					.getProperty("subindicator_group_id");
			// System.out.println(e.getProperty("subindicator_opposite_group_id"));
			Integer subindicator_opposite_group_id = (Integer) e
					.getProperty("subindicator_opposite_group_id");

			// first check if the group is okay.
			if (group_ids_1.contains(subindicator_group_id)) {

			} else {
				existing_subindicator_in_database.getOpposite_groups().add(
						new ArrayList<ArrayList<String>>());
				group_ids_1.add(subindicator_group_id);

			}

			// now check if the opposite group is okay.

			if (opposite_group_ids_1.contains(subindicator_opposite_group_id)) {

			} else {
				// get the last group, and get its last opposite group and add
				// this subindicator to it.
				existing_subindicator_in_database
						.getOpposite_groups()
						.get(existing_subindicator_in_database
								.getOpposite_groups().size() - 1)
						.add(new ArrayList<String>());
				opposite_group_ids_1.add(subindicator_opposite_group_id);
			}
			existing_subindicator_in_database
					.getOpposite_groups()
					.get(existing_subindicator_in_database.getOpposite_groups()
							.size() - 1)
					.get(existing_subindicator_in_database
							.getOpposite_groups()
							.get(existing_subindicator_in_database
									.getOpposite_groups().size() - 1).size() - 1)
					.add((String) related_subindicator
							.getProperty("subindicator_name"));

		}

	}

	public void assemble_opposite_subgroups(String label, TitanGraph g,
			SubIndicator existing_subindicator_in_database) {
		// System.out.println(existing_subindicator_in_database.getSubIndicator_name());
		ArrayList<Edge> ordered_opposite_subgroup_edges = GroovyStatements
				.getInstance().assemble_opposite_subgroup(
						existing_subindicator_in_database);
		HashSet<Integer> group_ids = new HashSet<Integer>();
		HashSet<Integer> opposite_group_ids = new HashSet<Integer>();
		HashSet<Integer> subgroup_ids = new HashSet<Integer>();

		Integer index_of_existing_last_group = -1;
		Integer index_of_existing_last_subgroup = -1;
		Integer index_of_existing_last_opposite_group = -1;

		for (Edge e : ordered_opposite_subgroup_edges) {
			Vertex related_subindicator = e.getVertex(Direction.IN);
			Integer group_id = (Integer) e.getProperty("subindicator_group_id");
			Integer subgroup_id = (Integer) e
					.getProperty("subindicator_subgroup_id");
			Integer opposite_group_id = (Integer) e
					.getProperty("subindicator_opposite_group_id");

			// System.out.println("\nstart new edge here");
			// System.out.println("group id is-->" + group_id +
			// " ||subgroup id is--->" + subgroup_id
			// + "||opposite group id is--->" + opposite_group_id);

			// System.out.println("this is subgroup_ids_1");
			// System.out.println(subgroup_ids_1.toString());

			if (group_ids.contains(group_id)) {

			} else {
				existing_subindicator_in_database.getOpposite_subgroups().add(
						new ArrayList<ArrayList<ArrayList<String>>>());
				group_ids.add(group_id);

				index_of_existing_last_group++;
				subgroup_ids.clear();
				index_of_existing_last_subgroup = -1;

			}

			if (subgroup_ids.contains(subgroup_id)) {

			} else {
				existing_subindicator_in_database.getOpposite_subgroups()
						.get(index_of_existing_last_group)
						.add(new ArrayList<ArrayList<String>>());
				subgroup_ids.add(subgroup_id);
				index_of_existing_last_subgroup++;

				opposite_group_ids.clear();
				index_of_existing_last_opposite_group = -1;
			}

			// System.out.println("opposite group ids 2 contains--->" +
			// opposite_group_ids_2.toString());
			// System.out.println("opposite group id is--->" +
			// opposite_group_id);

			if (opposite_group_ids.contains(opposite_group_id)) {
				// System.out.println("found ");
				// System.out.println("without additon we expect to find a list of something here.");
				// System.out.println(existing_subindicator_in_database
				// .getOpposite_subgroups()
				// .get(index_of_existing_last_group)
				// .get(index_of_existing_last_subgroup).toString());

			} else {
				// System.out.println("not found");
				existing_subindicator_in_database.getOpposite_subgroups()
						.get(index_of_existing_last_group)
						.get(index_of_existing_last_subgroup)
						.add(new ArrayList<String>());
				opposite_group_ids.add(opposite_group_id);
				index_of_existing_last_opposite_group++;
				// System.out.println("after addition we expect there to be an empty arraylist here.");
				// System.out.println(existing_subindicator_in_database
				// .getOpposite_subgroups()
				// .get(index_of_existing_last_group)
				// .get(index_of_existing_last_subgroup).toString());

			}

			// System.out.println("before adding the arraylist looksl ike--->");
			// System.out.println(existing_subindicator_in_database
			// .getOpposite_subgroups()
			// .get(index_of_existing_last_group)
			// .get(index_of_existing_last_subgroup).toString());

			existing_subindicator_in_database
					.getOpposite_subgroups()
					.get(index_of_existing_last_group)
					.get(index_of_existing_last_subgroup)
					.get(index_of_existing_last_opposite_group)
					.add((String) related_subindicator
							.getProperty("subindicator_name"));

			// System.out.println("after adding the arraylist looks like--->");
			// System.out.println(existing_subindicator_in_database
			// .getOpposite_subgroups()
			// .get(index_of_existing_last_group)
			// .get(index_of_existing_last_subgroup).toString());
		}
	}

	public void assemble_subgroup(String label, TitanGraph g,
			SubIndicator existing_subindicator_in_database) {
		ArrayList<Edge> ordered_subgroup_edges = GroovyStatements.getInstance()
				.assemble_subgroup(existing_subindicator_in_database);
		HashSet<Integer> group_ids = new HashSet<Integer>();

		HashSet<Integer> subgroup_ids = new HashSet<Integer>();

		Integer index_of_existing_last_group = -1;
		Integer index_of_existing_last_subgroup = -1;

		for (Edge e : ordered_subgroup_edges) {
			Vertex related_subindicator = e.getVertex(Direction.IN);
			Integer group_id = e.getProperty("subindicator_group_id");
			Integer subgroup_id = e.getProperty("subindicator_subgroup_id");
			if (group_ids.contains(group_id)) {

			} else {
				existing_subindicator_in_database.getSelf_sub_groups().add(
						new ArrayList<ArrayList<String>>());
				group_ids.add(group_id);
				index_of_existing_last_group++;
				subgroup_ids.clear();
				index_of_existing_last_subgroup = -1;
			}

			if (subgroup_ids.contains(subgroup_id)) {

			} else {
				existing_subindicator_in_database.getSelf_sub_groups()
						.get(index_of_existing_last_group)
						.add(new ArrayList<String>());
				subgroup_ids.add(subgroup_id);
				index_of_existing_last_subgroup++;
			}
			existing_subindicator_in_database
					.getSelf_sub_groups()
					.get(index_of_existing_last_group)
					.get(index_of_existing_last_subgroup)
					.add((String) related_subindicator
							.getProperty("subindicator_name"));
		}
	}

	public void assemble_opposite_specific_subindicator(String label,
			TitanGraph g, SubIndicator existing_subindicator_in_database) {
		ArrayList<Edge> ordered_opposite_specific_subgroup_edges = GroovyStatements
				.getInstance().assemble_opposite_specific_group(
						existing_subindicator_in_database);
		HashSet<Integer> group_ids = new HashSet<Integer>();

		HashSet<Integer> subgroup_ids = new HashSet<Integer>();

		Integer index_of_existing_last_group = -1;
		Integer index_of_existing_last_subgroup = -1;

		for (Edge e : ordered_opposite_specific_subgroup_edges) {
			Vertex related_subindicator = e.getVertex(Direction.IN);
			Integer group_id = e.getProperty("subindicator_group_id");
			Integer subgroup_id = e.getProperty("subindicator_subgroup_id");
			if (group_ids.contains(group_id)) {

			} else {
				existing_subindicator_in_database.getOpposite_specific_groups()
						.add(new ArrayList<ArrayList<String>>());
				group_ids.add(group_id);
				index_of_existing_last_group++;
				subgroup_ids.clear();
				index_of_existing_last_subgroup = -1;
			}

			if (subgroup_ids.contains(subgroup_id)) {

			} else {
				existing_subindicator_in_database.getOpposite_specific_groups()
						.get(index_of_existing_last_group)
						.add(new ArrayList<String>());
				subgroup_ids.add(subgroup_id);
				index_of_existing_last_subgroup++;
			}

			existing_subindicator_in_database
					.getOpposite_specific_groups()
					.get(index_of_existing_last_group)
					.get(index_of_existing_last_subgroup)
					.add((String) related_subindicator
							.getProperty("subindicator_name"));
		}

	}

	public SubIndicator assemble_existing_information(TitanGraph g) {

		// System.out.println("now doing assemble group");
		assemble_group(null, g, this);
		// System.out.println("now opposite");
		assemble_opposite_group(null, g, this);
		// System.out.println("now opp specific");
		assemble_opposite_specific_subindicator(null, g, this);
		// System.out.println("now assemble opposite subgroup");
		assemble_opposite_subgroups(null, g, this);
		// System.out.println("assemble subgroup");
		assemble_subgroup(null, g, this);
		// System.out.println("this is its string rep");
		// System.out.println(existing_subindicator_in_database.toString());
		return this;
	}

	public Integer last_element_of_array(ArrayList<Object> ar) {
		return ar.size() - 1;
	}

}
