package genericIndicatorFunctions;

public class SubIndicatorAppliacableForIndicatorNotFoundInDatabase extends
		Exception {

	public SubIndicatorAppliacableForIndicatorNotFoundInDatabase(String message){
		super(message);
	}

}
