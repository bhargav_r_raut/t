package genericIndicatorFunctions;

public class SubIndicatorDoesNotHaveAnyIncidentApplicableIndicators extends Exception{
	public SubIndicatorDoesNotHaveAnyIncidentApplicableIndicators(String message){
		super(message);
	}

}
