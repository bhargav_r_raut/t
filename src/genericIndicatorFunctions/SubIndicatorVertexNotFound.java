package genericIndicatorFunctions;

public class SubIndicatorVertexNotFound extends Exception{

	public SubIndicatorVertexNotFound(String message){
		super(message);
	}

}
