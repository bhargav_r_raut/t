package genericIndicatorFunctions;

public class SubIndicatorsJsonStringIsFaulty extends Exception {

	public SubIndicatorsJsonStringIsFaulty(String message){
		super(message);
	}

}
