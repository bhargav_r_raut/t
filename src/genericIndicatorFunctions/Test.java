package genericIndicatorFunctions;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;
import yahoo_finance_historical_scraper.StockConstants;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// iterate all the indicators
		HashMap<String, String> expected_blocked_subindicators = new LinkedHashMap<String, String>();
		HashMap<String, String> expected_subindicators = new LinkedHashMap<String, String>();

		try {
			JSONObject subindicators = new JSONObject(ReadWriteTextToFile
					.getInstance().readFile(
							StockConstants.subindicators_jsontextfile,
							TitanConstants.ENCODING));

			JSONArray group = subindicators.getJSONArray("group");

			for (int k = 0; k < group.length(); k++) {

				JSONObject existing_indicator_object = group.getJSONObject(k);
				Integer id = existing_indicator_object
						.getInt(StockConstants.name_of_id_field_in_subindicator_group_in_subindicator_jsonfile);

				JSONArray groups = existing_indicator_object
						.getJSONArray("names");
				if ((id >= 20 && id <= 109) || id == 119 || id == 120
						|| id == 4 || id == 5) {

					for (int i = 0; i < groups.length(); i++) {
						if (id == 4 || id == 5) {
							// only take the last thing.
							if (i == groups.length() - 1) {
								expected_blocked_subindicators.put(
										groups.getString(i),
										groups.getString(i));
							} else {
								expected_subindicators.put(groups.getString(i),
										groups.getString(i));
							}
						} else {
							expected_blocked_subindicators.put(
									groups.getString(i), groups.getString(i));
						}
					}

				} else {
					for (int i = 0; i < groups.length(); i++) {
						expected_subindicators.put(groups.getString(i),
								groups.getString(i));
					}
				}
			}
			System.out.println("total expected:" + (expected_subindicators.size())*3);
			System.out.println(expected_blocked_subindicators.size());
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//now time to iterate the jedis keys and see what we got.
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			Set<String> jedis_keys = jedis.keys("AAL_American Airlines Group*");
			System.out.println(jedis_keys.size());
		}

	}

}
