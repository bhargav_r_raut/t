package indicators_helpers;

public class IndicatorTimeSeriesLengthMismatch extends Exception {
	public IndicatorTimeSeriesLengthMismatch(String message){
		super(message);
	}
}
