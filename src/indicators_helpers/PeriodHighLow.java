package indicators_helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import eu.verdelhan.ta4j.Indicator;
import eu.verdelhan.ta4j.TADecimal;
import eu.verdelhan.ta4j.TimeSeries;

/**
 * first instantiate the class
 * then call calculate on whichever indices of the indicator you want. = better to do all the indices you need 
 * at once, so that the hashmaps will have everything you need.
 * then call gethighs_hasmap or getlows_hashmap.
 * @author aditya
 *
 */
public class PeriodHighLow {
	
	private ArrayList<Integer> indices;
	private Indicator<? extends TADecimal> indicator;
	private TimeSeries ts;
	private ArrayList<TADecimal> indicator_values;
	/**
	 * an hashmap which has the following structure:
	 * key => index queried.
	 * value => arraylist -> 
	 * 
	 * 0.3 day high
	 * 1.5 day high
	 * 2.8 day high - 1 week high
	 * 3.13 day high - 2 week high
	 * 4.21 day high - 3 week high
	 * 5.34 day high - 1 month high
	 * 6.55 day high - 2 month high
	 * 7.89 day high - 3 month high
	 * 8.144 day high - 5 month high
	 *  
	 * values for the elements in the arraylist include:
	 * 1 -> highest / lowest value
	 * -1 -> not highest or lowest value
	 * 0 -> no value possible here , because the period is too large for the indicator. 
	 *  
	 * 
	 */
	
	
	private HashMap<Integer,ArrayList<Integer>> highs_hashamp;
	private HashMap<Integer,ArrayList<Integer>> lows_hashmap; 
	/**
	 * @param indices: the index numbers of the indicator / time series for which to calculate the high and low
	 * @param indicator
	 * @param ts
	 */
	public PeriodHighLow(ArrayList<Integer> indices,
			Indicator<? extends TADecimal> indicator, TimeSeries ts, ArrayList<TADecimal> indicator_values) {
		this.indices = indices;
		this.indicator = indicator;
		this.ts = ts;
		this.setHighs_hashamp(new HashMap<Integer, ArrayList<Integer>>());
		this.setLows_hashmap(new HashMap<Integer, ArrayList<Integer>>());
		this.indicator_values = indicator_values;
	}
	
	
	public ArrayList<TADecimal> getIndicator_values() {
		return indicator_values;
	}


	public void setIndicator_values(ArrayList<TADecimal> indicator_values) {
		this.indicator_values = indicator_values;
	}


	public ArrayList<Integer> getIndices() {
		return indices;
	}
	public void setIndices(ArrayList<Integer> indices) {
		this.indices = indices;
	}
	public Indicator<? extends TADecimal> getIndicator() {
		return indicator;
	}
	public void setIndicator(Indicator<? extends TADecimal> indicator) {
		this.indicator = indicator;
	}
	public TimeSeries getTs() {
		return ts;
	}
	public void setTs(TimeSeries ts) {
		this.ts = ts;
	}
	
	
	public void calculate(ArrayList<Integer> indices_for_getting_highs_and_lows, Integer lookwhere) throws IndicatorTimeSeriesLengthMismatch{
	
		Integer indicator_counter = 0;
		Integer[] periods = new Integer[]{3,5,8,13,21,34,55,89,144};
		
		for(Integer i=0;i<ts.getSize(); i++){
			if(indicator.getValue(i)!=null){
				indicator_counter++;
			}
		}
		if(indicator_counter == ts.getSize()){
			RollingPeriodAnalyzer r_analyzer = new RollingPeriodAnalyzer(periods, getTs(), getIndicator(),getIndicator_values());
			
			for(Integer i=0;i<indices_for_getting_highs_and_lows.size(); i++){
				
				//get the slices for each period at this index number.
				ArrayList<ArrayList<TADecimal>> slices = r_analyzer.return_slices(true, lookwhere,
						indices_for_getting_highs_and_lows.get(i));
				
				//create an empty arraylist in the hashmap of highs and lows .
				getHighs_hashamp().put(i, new ArrayList<Integer>());
				getLows_hashmap().put(i, new ArrayList<Integer>());
				
				//
				for(ArrayList<TADecimal> ar : slices){
					TADecimal last_value_in_unsorted_list = ar.get(ar.size()-1);
					TADecimal first_value_in_unsorted_list = ar.get(0);
					Collections.sort(ar);
					ArrayList<Double> holder = new ArrayList<Double>();
					if(ar.size() == 0){
						//for this period, there is no available high or low indicator.
						getHighs_hashamp().get(i).add(0);
						getLows_hashmap().get(i).add(0);
					}
					else{
						if(lookwhere == -1){
							//this means that the last value in the unsorted list is the one
							//that we are interested in.
							if(ar.get(0) == last_value_in_unsorted_list){
								//this means that the last value in the unsorted list is the lowest value.
								getHighs_hashamp().get(i).add(-1);
								getLows_hashmap().get(i).add(1);
							}
							else if(ar.get(ar.size()-1) == last_value_in_unsorted_list){
								//last value in the unsorted list is the highest value.
								getHighs_hashamp().get(i).add(1);
								getLows_hashmap().get(i).add(-1);
							}
						}
						else{
							//this means that the first value in the unsorted list is the one that we are 
							//interested in , so we compare everything to the first value in the unsorted list.
							if(ar.get(0) == first_value_in_unsorted_list){
								//this means that the last value in the unsorted list is the lowest value.
								getHighs_hashamp().get(i).add(-1);
								getLows_hashmap().get(i).add(1);
							}
							else if(ar.get(ar.size()-1) == first_value_in_unsorted_list){
								//last value in the unsorted list is the highest value.
								getHighs_hashamp().get(i).add(1);
								getLows_hashmap().get(i).add(-1);
							}
						}
					}
				}
			}
		}
		else{
			throw new IndicatorTimeSeriesLengthMismatch(this.indicator.getClass().toString()
					+ "does not match its timeseries, indicator length is -->" + indicator_counter.toString() + "timeseries legnth is ===>"+ this.ts.getSize());
		}
	
		
		
	}


	/**
	 * @return the highs_hashamp
	 */
	public HashMap<Integer,ArrayList<Integer>> getHighs_hashamp() {
		return highs_hashamp;
	}


	/**
	 * @param highs_hashamp the highs_hashamp to set
	 */
	public void setHighs_hashamp(HashMap<Integer,ArrayList<Integer>> highs_hashamp) {
		this.highs_hashamp = highs_hashamp;
	}


	/**
	 * @return the lows_hashmap
	 */
	public HashMap<Integer,ArrayList<Integer>> getLows_hashmap() {
		return lows_hashmap;
	}


	/**
	 * @param lows_hashmap the lows_hashmap to set
	 */
	public void setLows_hashmap(HashMap<Integer,ArrayList<Integer>> lows_hashmap) {
		this.lows_hashmap = lows_hashmap;
	}
	
}
