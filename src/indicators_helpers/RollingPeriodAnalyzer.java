package indicators_helpers;

import java.util.ArrayList;

import eu.verdelhan.ta4j.Indicator;
import eu.verdelhan.ta4j.TADecimal;
import eu.verdelhan.ta4j.TimeSeries;

/**
 * Given an index it will return an arraylist of arraylists containing the
 * indicator sliced into pieces equal to those periods, by calling get_pieces
 * You have to also provide a lookback or a lookforward. You have to also
 * provide whether limits are to be included or not. i.e if you say index 4,
 * then does index four have to be included in the returned slice. example :
 * Periods => [3,5,8,13] index => 4 indicator series => 1.1, 1.2, 1.4, 1.6, 1.5,
 * 1.5 ,1.6 so the pieces it will return are (for lookback): (for period 3, with
 * limits : true): 1.4, 1.6, 1.5
 * 
 * 
 * 
 * @author aditya
 *
 */
public class RollingPeriodAnalyzer {

	private Integer[] periods;
	private TimeSeries series;
	private Indicator<? extends TADecimal> indicator;
	private ArrayList<TADecimal> indicator_values;

	public Integer[] getPeriods() {
		return periods;
	}

	public void setPeriods(Integer[] periods) {
		this.periods = periods;
	}

	public TimeSeries getSeries() {
		return series;
	}

	public void setSeries(TimeSeries series) {
		this.series = series;
	}

	public Indicator<? extends TADecimal> getIndicator() {
		return indicator;
	}

	public void setIndicator(Indicator<? extends TADecimal> indicator) {
		this.indicator = indicator;
	}

	public ArrayList<TADecimal> getIndicator_values() {
		return indicator_values;
	}

	public void setIndicator_values(ArrayList<TADecimal> indicator_values) {
		this.indicator_values = indicator_values;
	}

	/**
	 * @param periods
	 * @param series
	 * @param indicator
	 */
	public RollingPeriodAnalyzer(Integer[] periods, TimeSeries series,
			Indicator<? extends TADecimal> indicator, ArrayList<TADecimal> indicator_values) {
		this.periods = periods;
		this.series = series;
		this.indicator = indicator;
		this.indicator_values = indicator_values;
		
	}

	/**
	 * 
	 * 
	 * @param include_limits
	 *            => true means, it will return the slice with the index
	 *            included.
	 * @param lookwhere
	 *            => (1)lookforward means, index number + (slice_length - 1),
	 *            (-1)lookbackwards => index_number - (slice_length -1)
	 * @param index
	 *            => which point of indicator do you want to return from.
	 * @return
	 */
	public ArrayList<ArrayList<TADecimal>> return_slices(
			Boolean include_limits, Integer lookwhere, Integer index) {
		ArrayList<ArrayList<TADecimal>> slice_holder = new ArrayList<ArrayList<TADecimal>>();

		if (lookwhere == -1) {
			for (Integer period : periods) {
				ArrayList<TADecimal> period_slice_holder = new ArrayList<TADecimal>();
				if (include_limits == true) {
					// in this case if the index should be equal to the period -
					if (index >= period - 1) {
						//add the actual sublist.
						for(TADecimal dc: this.indicator_values.subList(index + (period - 1),index)){
							period_slice_holder.add(dc);
						}
					}
					else {
						
					}
				} else {
					// in this case the index has to be equal to the period.
					if (index >= period) {
						//add the actual sublist.
						for(TADecimal dc: this.indicator_values.subList(index - (period - 1),index)){
							period_slice_holder.add(dc);
						}
					}
					else {
						//add an empty list.
						
					}
				}
				slice_holder.add(period_slice_holder);
			}
		} else {
			for (Integer period : periods) {
				ArrayList<TADecimal> period_slice_holder = new ArrayList<TADecimal>();
				if (include_limits == true) {
					//if index is 4, and period is 3 count, count cannot be less than 
					if(this.indicator_values.size() >= (index + 1) + (period - 1)){
						for(TADecimal dc: this.indicator_values.subList(index, index + (period -1))){
							period_slice_holder.add(dc);
						}
					}
					else{
						
					}
				} else {
					if(this.indicator_values.size() > (index + 1) + (period -1)){
						for(TADecimal dc: this.indicator_values.subList(index, index + (period -1))){
							period_slice_holder.add(dc);
						}
					}
					else{
						
					}
				}
				slice_holder.add(period_slice_holder);
			}
		}

		return slice_holder;
	}

}
