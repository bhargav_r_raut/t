package metals;

import java.util.ArrayList;
import java.util.Calendar;

public class LocalMetalHolder {

	private ArrayList<Calendar> dates;
	private ArrayList<Double> close;
	private ArrayList<Integer> volume;
	
	public LocalMetalHolder(){
		this.dates = new ArrayList<Calendar>();
		this.close = new ArrayList<Double>();
		this.volume = new ArrayList<Integer>();
	}
	
	public void addDate(Calendar cal){
		this.dates.add(cal);
	}
	
	public void addClose(Double close){
		this.close.add(close);
	}
	
	public void addVolume(Integer volume){
		this.volume.add(volume);
	}

	public ArrayList<Calendar> getDates() {
		return dates;
	}

	public void setDates(ArrayList<Calendar> dates) {
		this.dates = dates;
	}

	public ArrayList<Double> getClose() {
		return close;
	}

	public void setClose(ArrayList<Double> close) {
		this.close = close;
	}

	public ArrayList<Integer> getVolume() {
		return volume;
	}

	public void setVolume(ArrayList<Integer> volume) {
		this.volume = volume;
	}
	
}
