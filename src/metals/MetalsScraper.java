package metals;


import static jodd.jerry.Jerry.jerry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.Collator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import singletonClass.CalendarFunctions;
import yahoo_finance_historical_scraper.HistoricalStock;
import yahoo_finance_historical_scraper.InputListSizeInsufficient;
import Titan.ReadWriteTextToFile;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import dailyDownload.DateOutOfRange;
import dailyDownload.QueryResult;
import forex.ForexTest;
import forex.HistoricalQuery;
/*
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
*/

public class MetalsScraper extends ForexTest implements MetalConstants{
	
	private Map<GregorianCalendar,Double>gold_dates_and_prices;
	private Map<GregorianCalendar,Double>silver_dates_and_prices;
	
	private Metal gold;
	public Metal getGold() {
		return gold;
	}


	public void setGold(Metal gold) {
		this.gold = gold;
	}


	public Metal getSilver() {
		return silver;
	}


	public void setSilver(Metal silver) {
		this.silver = silver;
	}


	private Metal silver;
	
	public MetalsScraper(){
		this.gold = new Metal("gold", TimeZone.getTimeZone("Europe/London"));
		this.silver = new Metal("silver",TimeZone.getTimeZone("Europe/London"));
		this.gold_dates_and_prices = new TreeMap<GregorianCalendar, Double>(new Comparator<GregorianCalendar>() {
		    public int compare(GregorianCalendar date1, GregorianCalendar date2) {
		        return date2.compareTo(date1);
		    }
		});
		this.silver_dates_and_prices = new TreeMap<GregorianCalendar, Double>(new Comparator<GregorianCalendar>() {
		    public int compare(GregorianCalendar date1, GregorianCalendar date2) {
		        return date2.compareTo(date1);
		    }
		});
	}
	
	
	public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);
        	response.append("\n");
        in.close();

        return response.toString();
    }
	
	
	public void scrapePage(String u) throws Exception{
		
		//String u = "http://www.321gold.com/archives/fix.html";
		System.out.println("now parsing page " + u);
		String pageSource = ReadWriteTextToFile.getInstance().getTextFromUrl(u);
		//ReadWriteTextToFile.getInstance().writeFile("gold.txt", pageSource);
		//String pageSource = ReadWriteTextToFile.getInstance().readFile("gold.txt", UTF_8);
		Jerry doc = jerry(pageSource);
		final Pattern gold_pattern = Pattern.compile("pm:\\$"
				+ "(\\d+.\\d+)");
		final Pattern silver_pattern = Pattern.compile("Silver\\s+\\$(\\d+.\\d+)");
		final Pattern date_pattern = Pattern.compile("(([A-Za-z]{3})\\s(\\d+)\\s(\\d+))");
		final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy HH:mm:ss");
		sdf.setTimeZone(getGold().tz);
		
		doc.$("li").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $list_element, int list_index) {
				// TODO Auto-generated method stub
				String line = $list_element.text();
				Matcher date_matcher = date_pattern.matcher(line); 
				Matcher gold_matcher = gold_pattern.matcher(line);
				Matcher silver_matcher = silver_pattern.matcher(line);
				while(date_matcher.find()){
					GregorianCalendar cal = new GregorianCalendar();
					try {
						cal.setTime(sdf.parse(date_matcher.group(0)  + " 15:00:00"));
						cal.setTimeZone(sdf.getTimeZone());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					while(gold_matcher.find()){
						//getGold().addDate(cal);
						//getGold().close.add(Double.parseDouble(gold_matcher.group(1)));
						gold_dates_and_prices.put(cal, Double.parseDouble(gold_matcher.group(1)));
					}
					while(silver_matcher.find()){
						//getSilver().addDate(cal);
						//getSilver().close.add(Double.parseDouble(silver_matcher.group(1)));
						silver_dates_and_prices.put(cal, Double.parseDouble(silver_matcher.group(1)));
					}
				}
				return true;
			}
			
		});
		
	}
	
	
	//FROM THIS POINT ONWARDS IS THE HANDLING OF THE QUERY AND THE GETTING OF THE FINAL
	//HM AND QUERYRESULT.
	
	@Override
	public HistoricalQuery generateHistoricalQuery(ArrayList<Object> queryString) {
		// TODO Auto-generated method stub
		return super.generateHistoricalQuery(queryString);
	}

	public QueryResult scrapeMetal(ArrayList<Object> queryString) throws 
	IllegalArgumentException, IllegalAccessException, InputListSizeInsufficient{
		
		QueryResult qr = new QueryResult();
		HistoricalQuery query = generateHistoricalQuery(queryString);
		
		
		HashMap<String,HistoricalStock> hm = new HashMap<String, HistoricalStock>();
		
		ArrayList<GregorianCalendar> all_dates = new ArrayList<GregorianCalendar>();
		for(Calendar date: query.getStart_dates()){
			GregorianCalendar newcal = new GregorianCalendar();
			newcal.setTimeInMillis(date.getTimeInMillis());
			all_dates.add(newcal);
		}
		for(Calendar date: query.getEnd_dates()){
			GregorianCalendar newcal = new GregorianCalendar();
			newcal.setTimeInMillis(date.getTimeInMillis());
			all_dates.add(newcal);
		}
		
	
		Collections.sort(all_dates);
		GregorianCalendar endDate = all_dates.get(all_dates.size()-1);
		GregorianCalendar startDate = all_dates.get(0);
		
	
		scrapePeriod(startDate,endDate);
		
		getGold().setDates(new ArrayList<Calendar>(gold_dates_and_prices.keySet()));
		getGold().setclose(new ArrayList<Double>(gold_dates_and_prices.values()));
		Collections.reverse(getGold().getDates());
		Collections.reverse(getGold().getclose());
		getSilver().setDates(new ArrayList<Calendar>(silver_dates_and_prices.keySet()));
		getSilver().setclose(new ArrayList<Double>(silver_dates_and_prices.values()));
		Collections.reverse(getSilver().getDates());
		Collections.reverse(getSilver().getclose());
		
		/*
		System.out.println("after reversing collections this is what it is inside.");
		for(Calendar cal : getGold().getDates()){
			CalendarFunctions calfunc = 
					new CalendarFunctions(cal);
			System.out.println(calfunc.getFullDateWithoutTimeZoneOnlyDate());
		}
		*/
		//System.exit(0);
		//adjust the dates for gold and silver seperately depending upon each.
		int m=0;
		for(String s:query.getForex_names()){
			if(s == "gold"){
				//it is gold.
				this.gold.adjustStockObject(query.getStart_dates().get(m), query.getEnd_dates().get(m));
				hm.put(s, this.gold);
			}
			else{
				//it is silver,
				this.silver.adjustStockObject(query.getStart_dates().get(m), query.getEnd_dates().get(m));
				hm.put(s, this.silver);
			}
			m++;
		}
		
		qr.setHm(hm);
		
		return qr;
	}
	
	

	public void scrapePeriod(
			Calendar start_date, Calendar end_date) throws IllegalArgumentException, IllegalAccessException, InputListSizeInsufficient{
		Calendar cal = new GregorianCalendar(end_date.getTimeZone());
		int currentYear = cal.get(Calendar.YEAR);
		int startYear = start_date.get(Calendar.YEAR);
		int endYear = end_date.get(Calendar.YEAR);
		ArrayList<String> pageUrls = new ArrayList<String>();
		if(startYear < 2001 || endYear > currentYear){
			try {
				throw new DateOutOfRange("the start date ---> " + 
			startYear + " or the end date " + endYear + " could not be reached");
			} catch (DateOutOfRange e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		else{
			//for each thing from the start till the end we have to add pages to an array
			//list of pages.
			if(startYear == currentYear){
				System.out.println("the years are equal.");
				pageUrls.add(metalbaseUrlstart + Integer.toString(currentYear) + metalbaseUrlend);
				pageUrls.add(metalbaseUrlstart + metalbaseUrlend);
			}
			else{
				for(int i = startYear; i<endYear+1;i++){
					pageUrls.add(metalbaseUrlstart + i + metalbaseUrlend);
				}
				if(endYear == currentYear){
					pageUrls.add(metalbaseUrlstart+metalbaseUrlend);
				}
			}
			//get unique list.
			
			
			
			for(String s : pageUrls){
				try {
					scrapePage(s);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	

}
