package oil;

import java.util.TimeZone;

import yahoo_finance_historical_scraper.HistoricalStock;
import yahoo_finance_historical_scraper.StockConstants;

public class Oil extends HistoricalStock{
	public static final String ENTITY_TYPE = StockConstants.OIL_STOCK_NAME;
	public Oil(String stockName, TimeZone tz) {
		super(stockName, tz,ENTITY_TYPE,StockConstants.indices_main[5],StockConstants.OIL_STOCK_NAME);
		// TODO Auto-generated constructor stub
	}
	
}
