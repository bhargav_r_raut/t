package oil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.plaf.metal.MetalScrollBarUI;

import org.javatuples.Pair;

import singletonClass.CalendarFunctions;
import Titan.ReadWriteTextToFile;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import dailyDownload.DateOutOfRange;
import dailyDownload.QueryResult;
import forex.HistoricalQuery;
import metals.MetalsScraper;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import static jodd.jerry.Jerry.jerry;

/*
 import org.jsoup.Jsoup;
 import org.jsoup.nodes.Document;
 import org.jsoup.nodes.Element;
 import org.jsoup.select.Elements;
 */

import yahoo_finance_historical_scraper.DailyPriceHolderObject;
import yahoo_finance_historical_scraper.HistoricalStock;
import yahoo_finance_historical_scraper.InputListSizeInsufficient;

public class OilScraper extends MetalsScraper implements OilConstants {

	private Oil oil;
	private Calendar start_date;
	private Calendar end_date;
	private String scraper_url;
	private final String USER_AGENT = "Mozilla/5.0";
	private TreeMap<Calendar, Pair<Double, Integer>> oil_date_with_close_and_volume;

	public TreeMap<Calendar, Pair<Double, Integer>> getOil_date_with_close_and_volume() {
		return oil_date_with_close_and_volume;
	}


	public void setOil_date_with_close_and_volume(
			TreeMap<Calendar, Pair<Double, Integer>> oil_date_with_close_and_volume) {
		this.oil_date_with_close_and_volume = oil_date_with_close_and_volume;
	}


	public OilScraper() {
		this.oil = new Oil("oil", TimeZone.getTimeZone("CST6CDT"));
		this.oil_date_with_close_and_volume = new TreeMap<Calendar, Pair<Double,Integer>>(new Comparator<Calendar>() {
		    public int compare(Calendar date1, Calendar date2) {
		        //this is ascending comaprison.
		    	return date1.compareTo(date2);
		    }
		});
	}
	

	public Oil getOil() {
		return oil;
	}



	public static String getText(String url) throws Exception {
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));

		StringBuilder response = new StringBuilder();
		String inputLine;

		while ((inputLine = in.readLine()) != null)
			response.append(inputLine);
		response.append("\n");
		in.close();

		return response.toString();
	}
	
	@Override
	public void scrapePage(String url) throws Exception {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
		sdf.setTimeZone(this.oil.getTz());
		
		final DailyPriceHolderObject dp = new DailyPriceHolderObject(null);
		
		String response = ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		//System.out.println("the url is --->" + url);
		//System.out.println(response);
		
		Jerry doc = jerry(response);
		//System.out.println(doc.text());
		doc.$("table").each(new JerryFunction() {
			public boolean onNode(Jerry $this, int index) {
				//System.out.println($this.text());
				//System.exit(0);
				if(index == 0){
				$this.$("tr").each(new JerryFunction() {	
					@Override
					public boolean onNode(Jerry $this_inner, int inner_index) {
						//System.out.println($this_inner.text());
						// TODO Auto-generated method stub
						if (inner_index%2 == 0 && inner_index!=0) {
							//System.out
							//		.println("this are the indiividual things from this row.");
							$this_inner.$("td").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $td, int td_index) {
									//System.out.println("the index is --->" + td_index);
									//System.out.println($td.text());
									//System.out.println("ends");
									switch (td_index) {
									case 0:
										// parse date
										Calendar cal = new GregorianCalendar();
										try {
											cal.setTime(sdf.parse($td.text() + " 13:30:00"));
											cal.setTimeZone(sdf.getTimeZone());
											dp.setCal(cal);
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											dp.setAbort(true);
											//e.printStackTrace();
										}

										break;
									case 4:
										// parse close price
										if (!dp.isAbort()) {
											try{
											dp.setClose(Double.parseDouble($td
													.text()));
											}
											catch(NullPointerException | NumberFormatException ex){
												dp.setAbort(true);
											}
										}
										break;
									case 6:
										// parse volume
										//System.out.println($td.text());
										if (!dp.isAbort()) {
											try{
											dp.setVolume(Integer.parseInt($td
													.text().replace(",", "")));
											}
											catch(NumberFormatException ex){
												dp.setAbort(true);
											}
										}
										break;
									default:

										break;
									}

									return true;
								}
							});
							// okay so now we check abort
							if (dp.isAbort()) {
								//System.out.println("this row initially"
								//		+ " contained, so we do "
								//		+ "not admit.");
								//System.out.println("date-->" + new CalendarFunctions(dp.getCal()));
								//System.out.println("close--->" + dp.getClose());
								//System.out.println("volume---->" + Integer.toString(dp.getVolume()));
							} else {
								
								//System.out.println("the date is --->" + new CalendarFunctions(dp.getCal()).
								//		getFullDate() + " the close price---->" + dp.getClose() + " volume: " + dp.getVolume());
								//getOil().addDate(dp.getCal());
								
							//	System.out.println(new CalendarFunctions(OilScraper.
							//			this.oil.getDates()
							//			.get(OilScraper.
							//					this.oil.getDates()
							//					.size()-1)).getFullDate());
								//getOil().addClose(dp.getClose());
								//getOil().addVolume(dp.getVolume());
								//dp.add_to_treemap(dp.getCal(), dp.getClose(), dp.getVolume());
								getOil_date_with_close_and_volume().put(dp.getCal(), 
										new Pair<Double, Integer>(dp.getClose(), dp.getVolume()));
								
							}
							dp.clearAllFields();
							//final DailyPriceHolderObject dp = new DailyPriceHolderObject();
							//System.out.println("this row ends \n");
						}

						return true;
					}
				});
				}
				//System.out.println(index);
				return true;
			}
		});
	
		

	}

	@Override
	public HistoricalQuery generateHistoricalQuery(ArrayList<Object> queryString) {
		// TODO Auto-generated method stub
		return super.generateHistoricalQuery(queryString);
	}

	@Override
	public QueryResult scrapeMetal(ArrayList<Object> queryString)
			throws IllegalArgumentException, IllegalAccessException,
			InputListSizeInsufficient {
		// TODO Auto-generated method stub
		// return super.scrapeMetal(queryString);
		QueryResult qr = new QueryResult();
		HistoricalQuery query = generateHistoricalQuery(queryString);
		HashMap<String, HistoricalStock> hm = new HashMap<String, HistoricalStock>();

		//System.out.println("this the generated query.");
		//System.out.println(query.getForex_names() + "\n"
		//		+ query.getStart_dates() + "\n" + query.getEnd_dates());

		ArrayList<Long> all_dates = new ArrayList<Long>();

		BiMap<Calendar, Long> dateMap = HashBiMap.create();
		BiMap<Long, Calendar> inversedateMap = dateMap.inverse();

		for (Calendar cal : query.getStart_dates()) {
			dateMap.put(cal, cal.getTimeInMillis());
		}
		for (Calendar cal : query.getEnd_dates()) {
			dateMap.put(cal, cal.getTimeInMillis());
		}

		Collection<Long> c = dateMap.values();
		List myList = new ArrayList(c);
		Collections.sort(myList);

		int i = 0;
		Calendar startDate = null;
		Calendar endDate = null;
		for (Long l : dateMap.values()) {

			if (l == myList.get(0)) {
				startDate = inversedateMap.get(l);
			}

			else if (l == myList.get(c.size() - 1)) {
				endDate = inversedateMap.get(l);
			}
			i++;
		}

		//System.out.println("this is the start and end dates.");
		//System.out.println(new CalendarFunctions(startDate).getFullDateWithoutTimeZoneOnlyDate());
		//System.out.println(new CalendarFunctions(endDate).getFullDateWithoutTimeZoneOnlyDate());

		// now entering scrape period.
	//	System.out.println("now entering scrape period.");

		scrapePeriod(startDate, endDate);
		for(Map.Entry<Calendar, Pair<Double,Integer>> entry : getOil_date_with_close_and_volume().entrySet()){
			getOil().addDate(entry.getKey());
			getOil().addClose(entry.getValue().getValue0());
			getOil().addVolume(entry.getValue().getValue1().longValue());
		}
		/*
		Collections.reverse(this.oil.close);
		Collections.reverse(this.oil.dates);
		Collections.reverse(this.oil.epoches);
		Collections.reverse(this.oil.volume);
		Collections.reverse(this.oil.day_of_month);
		Collections.reverse(this.oil.day_of_week);
		Collections.reverse(this.oil.month);
		Collections.reverse(this.oil.week_of_month);
		Collections.reverse(this.oil.week_of_year);
		Collections.reverse(this.oil.month_of_year);
		Collections.reverse(this.oil.quarter_of_year);
		Collections.reverse(this.oil.half_of_year);
		Collections.reverse(this.oil.day_id);
		Collections.reverse(this.oil.unique_identifier);
		*/
		for(Calendar cal: this.oil.dates){
			System.out.println(new CalendarFunctions(cal).getFullDateWithoutTimeZoneOnlyDate().toString());
		}
		
		
		
		//for(Calendar cal:getOil().dates){
		//	System.out.println(new CalendarFunctions(cal).getFullDate());
		//}
		//adjust the dates as per what was ordered.
	
		//System.out.println(new CalendarFunctions(this.oil.getDates().get(2)).getFullDate());
		//System.exit(0);
		for(String s:query.getForex_names()){
			if(s == "oil"){
				//it is oil
				
				this.oil.adjustStockObject(query.getStart_dates().get(0),
						query.getEnd_dates().get(0));
				hm.put(s, this.oil);
			}
			
		}
		
		qr.setHm(hm);
		
		return qr;

	}

	@Override
	public void scrapePeriod(Calendar start_date, Calendar end_date)
			throws IllegalArgumentException, IllegalAccessException,
			InputListSizeInsufficient {
		// TODO Auto-generated method stub
		Calendar cal = new GregorianCalendar(end_date.getTimeZone());
		int currentYear = cal.get(Calendar.YEAR);
		int startYear = start_date.get(Calendar.YEAR);
		int endYear = end_date.get(Calendar.YEAR);
		ArrayList<String> pageUrls = new ArrayList<String>();
		if (startYear < 2007 || endYear > currentYear) {
			try {
				throw new DateOutOfRange("the start date ---> " + startYear
						+ " or the end date " + endYear
						+ " could not be reached");
			} catch (DateOutOfRange e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (start_date.get(Calendar.MONTH) < 7 && start_date.get(Calendar.YEAR) == 2007)  {
			try {
				throw new DateOutOfRange("the month is less than 7 and it is "
						+ start_date.get(Calendar.MONTH));
			} catch (DateOutOfRange e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// for each thing from the start till the end we have to add pages
			// to an array
			// list of pages.

			// get unique list.
			// get index of the month of the start date.
			// create array of years

			ArrayList<Integer> years_to_parse = new ArrayList<Integer>();
			if (startYear == endYear) {
				years_to_parse.add(startYear);
			} else {
				for (int i = startYear; i < endYear + 1; i++) {
					years_to_parse.add(i);
				}
			}
			int counter = 0;

			if (years_to_parse.size() == 1) {
				
				if(start_date.get(Calendar.MONTH) == end_date.get(Calendar.MONTH)){
					pageUrls.add(oilBaseUrl + Integer.toString(start_date.get(Calendar.MONTH)+1) +
						oilConnector + Integer.toString(years_to_parse.get(0)));
				}
				else
				{
					for (int l = start_date.get(Calendar.MONTH)+1; l < end_date.get(Calendar.MONTH) + 2; l++) 
					{
						pageUrls.add(oilBaseUrl + Integer.toString(l) + oilConnector
							+ Integer.toString(years_to_parse.get(0)));
					}
				}
				
			}
			 else {
				for (int year : years_to_parse) {

					if (counter == 0) {
						for (int z = start_date.get(Calendar.MONTH) + 1; z < 13; z++) {
							pageUrls.add(oilBaseUrl + Integer.toString(z) + oilConnector
									+ Integer.toString(year));
						}
					} else if (counter == years_to_parse.size() - 1) {
						for (int l = 1; l < end_date.get(Calendar.MONTH) + 2; l++) {
							pageUrls.add(oilBaseUrl + Integer.toString(l) + oilConnector
									+ Integer.toString(year));
						}
					} else {
						for (int n = 1; n < 13; n++) {
							pageUrls.add(oilBaseUrl + Integer.toString(n) + oilConnector
									+ Integer.toString(year));
						}
					}

					counter++;

				}
			}

			
		

			//Set<String> uniqueHash = new HashSet<String>(pageUrls);
			//System.out.println(pageUrls);

			for (String s : pageUrls) {
				try {
					scrapePage(s);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
