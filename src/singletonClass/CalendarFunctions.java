package singletonClass;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Titan.TitanConstants;

public class CalendarFunctions {
	private Calendar cal;

	public CalendarFunctions(Calendar cal){
		this.cal = cal;
	}
	
	public String getFullDateWithoutTimeZone(){
		Date d = this.cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
		sdf.setTimeZone(this.cal.getTimeZone());
		return this.cal.get(Calendar.YEAR) + 
				"-" + String.format("%02d",this.cal.get(Calendar.MONTH) + 1)
				+ "-" + String.format("%02d",this.cal.get(Calendar.DAY_OF_MONTH)) + " " + sdf.format(d); 
	}
	/**
	 * returns the date in the same format as the dates field in the database.
	 * format is
	 * yyyy-MM-dd HH:mm:ss
	 * 
	 * @return
	 */
	public String getFullDate(){
		Date d = this.cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		sdf.setTimeZone(this.cal.getTimeZone());
		return this.cal.get(Calendar.YEAR) + 
				"-" + (this.cal.get(Calendar.MONTH) + 1)
				+ "-" + this.cal.get(Calendar.DAY_OF_MONTH) + " " + sdf.format(d) + this.cal.getTimeZone(); 
	}
	
	public String getFullDateWithoutTimeZoneOnlyDate(){
		Date d = this.cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		sdf.setTimeZone(this.cal.getTimeZone());
		return this.cal.get(Calendar.YEAR) + 
				"-" + String.format("%02d",this.cal.get(Calendar.MONTH) + 1)
				+ "-" + String.format("%02d",this.cal.get(Calendar.DAY_OF_MONTH)); 
	}
	
	public String gettime_for_logger(){
		Date d = this.cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		sdf.setTimeZone(this.cal.getTimeZone());
		String sans_time = this.cal.get(Calendar.YEAR) + 
				"-" + String.format("%02d",this.cal.get(Calendar.MONTH) + 1)
				+ "-" + String.format("%02d",this.cal.get(Calendar.DAY_OF_MONTH)); 
		//System.out.println("sans time is :" + sans_time );
		String time_string = sdf.format(d);
		//System.out.println("time string is:" + time_string);
		return sans_time + "  " + time_string;
	}
}
