package singletonClass;


public class MySingleton {
	
	private static volatile MySingleton instance = null;
	
	// private constructor
	private MySingleton() {
	}

	public static MySingleton getInstance() {
		if (instance == null) {
			synchronized (MySingleton.class) {
				// Double check
				if (instance == null) {
					instance = new MySingleton();
				}
			}
		}
		return instance;
	}
	
}
