package subindicators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import ExchangeBuilder.Entity;
import ExchangeBuilder.EventLogger;


public class CalculationTask implements Callable<HashMap<Integer,List<Complex>>> {
	
	


	private Entity e;
	
	private ISubIndicatorCalculation isub;
	
		
	public Entity getE() {
		return e;
	}



	public void setE(Entity e) {
		this.e = e;
	}



	public ISubIndicatorCalculation getIsub() {
		return isub;
	}



	public void setIsub(ISubIndicatorCalculation isub) {
		this.isub = isub;
	}

	

	public CalculationTask(Entity e, ISubIndicatorCalculation isub) {
		super();
		this.e = e;
		this.isub = isub;
	}



	/****
	 * logic of working:
	 * 
	 * 1. entity and subindicator calculation interface are the attributes passed in
	 * 2. this method will just call #calculate on the interface, passing in
	 * the entity.
	 * 3. it will get back the results hashmap.
	 * that's it.
	 * 
	 * so now we have to see if this will work or not.
	 * 
	 * now this has to be called from the entityindex.
	 * 
	 */
	
	@Override
	public HashMap<Integer,List<Complex>> call() throws Exception {
		// TODO Auto-generated method stub
		new EventLogger(EventLogger.ENTITY_TASK_STARTED, getE().getIndice(),
				getE().getUnique_name(), new HashMap<String,String>(){{
					put("calculation_task",getIsub().getClass().getName());
				}}).commit();
		getIsub().calculate(getE());
		new EventLogger(EventLogger.ENTITY_TASK_FINISHED, getE().getIndice(),
				getE().getUnique_name(), new HashMap<String,String>(){{
					put("calculation_task",getIsub().getClass().getName());
					put("task_generated_complexes_on_total_day_ids",
							String.valueOf(getIsub().getResults().keySet().size()));
				}}).commit();
		return getIsub().getResults();
	}

}
