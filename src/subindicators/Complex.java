package subindicators;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class Complex {
	String indicator_cum_period_name;
	String subindicator_name;
	String complex_id;
	String subindicator_id;
	String entity_id;
	Integer day_id;
	
	public String getIndicator_cum_period_name() {
		return indicator_cum_period_name;
	}

	public void setIndicator_cum_period_name(String indicator_cum_period_name) {
		this.indicator_cum_period_name = indicator_cum_period_name;
	}

	public String getSubindicator_name() {
		return subindicator_name;
	}

	public void setSubindicator_name(String subindicator_name) {
		this.subindicator_name = subindicator_name;
	}

	public String getComplex_id() {
		return complex_id;
	}

	public void setComplex_id(String complex_id) {
		this.complex_id = complex_id;
	}

	public String getSubindicator_id() {
		return subindicator_id;
	}

	public void setSubindicator_id(String subindicator_id) {
		this.subindicator_id = subindicator_id;
	}

	public String getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}

	public Integer getDay_id() {
		return day_id;
	}

	public void setDay_id(Integer day_id) {
		this.day_id = day_id;
	}

	public Complex(){
		
	}

	public Complex(String indicator_cum_period_name, String complex_id, String subindicator_id, String entity_id,
			Integer day_id, String subindicator_name) {
		super();
		this.indicator_cum_period_name = indicator_cum_period_name;
		this.complex_id = complex_id;
		this.subindicator_id = subindicator_id;
		this.entity_id = entity_id;
		this.day_id = day_id;
		this.subindicator_name = subindicator_name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		ObjectMapper o = new ObjectMapper();
		try {
			return o.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	
}
