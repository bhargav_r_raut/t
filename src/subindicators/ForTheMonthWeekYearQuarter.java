package subindicators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import java.math.BigDecimal;


/****
 * GENERATES THE FOLLOWING SUBINDICATORS:
 * 
 * 1. first/last trading day of the week/month/quarter/year
 * 2. rises/falls for the week/month/quarter/year.
 * 
 * 
 * 
 * 
 * 
 * @author bhargav
 *
 */
public class ForTheMonthWeekYearQuarter extends SubIndicatorCalculation {

	@Override
	public HashMap<Integer, List<Complex>> getResults() {
		// TODO Auto-generated method stub
		return super.getResults();
	}

	@Override
	public void setResults(HashMap<Integer, List<Complex>> results) {
		// TODO Auto-generated method stub
		super.setResults(results);
	}

	public static final HashMap<String, Integer> time_unit_ids = new HashMap<String, Integer>() {
		{
			put("week", 0);
			put("month", 1);
			put("quarter", 2);
			put("year", 3);
		}
	};

	/****
	 * 
	 * how subindicator ids are set
	 * 
	 * time units ============ if week : 0 if month : 1 if quarter : 2 if year :
	 * 3
	 * 
	 * first trading day of = 0 + (time_unit)
	 * 
	 * last trading day of = 4 + (time_unit)
	 * 
	 * rises for the = 8 + (time_unit)
	 * 
	 * falls for the = 12 + (time_unit)
	 */

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub

		/***
		 * like week, month, year, quarter
		 * 
		 */
		String time_unit = (String) arguments.get("time_unit");

		/***
		 * 
		 * the id for the time unit derived from the map above.
		 * 
		 */
		Integer time_unit_id = time_unit_ids.get(time_unit);

		/***
		 * 
		 * the final subindicator id assigned.
		 * 
		 */
		String subindicator_id = null;

		if (arguments.containsKey("rises_or_falls")) {
			/***
			 * rises or falls 1 for rises -1 for falls
			 * 
			 * 
			 */
			Integer rises_or_falls = (Integer) arguments.get("rises_or_falls");

			if (rises_or_falls == 1) {
				subindicator_id = String.valueOf(8 + time_unit_id);
			} else {
				subindicator_id = String.valueOf(12 + time_unit_id);
			}

		}

		if (arguments.containsKey("first_or_last")) {

			/**
			 * 
			 * first or last
			 * 
			 */
			String first_or_last = (String) arguments.get("first_or_last");

			if (first_or_last == "first") {
				subindicator_id = String.valueOf(0 + time_unit_id);
			} else {
				subindicator_id = String.valueOf(4 + time_unit_id);
			}

		}

		if (subindicator_id != null) {
			subindicator_id = subindicator_prefix_map.get("ForTheMonthWeekYearQuarter") + subindicator_id;
		}

		return subindicator_id;
	}

	/****
	 * the names we have are: 1. first trading day of (week/month/quarter/year)
	 * 2. last trading day of (week/month/quarter/year) 3. rises for the
	 * (week/month/quarter/year) 4. falls for the (week/month/quarter/year)
	 * 
	 * what about individual dates, also should be added.
	 * 
	 * 
	 */
	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {

		String time_unit = (String) arguments.get("time_unit");
		// how to calculate the subindicator name?
		String first_or_last = null;
		Integer rises_or_falls = null;
		String rf = null;

		Integer day_id = (Integer) arguments.get("day_id");

		if (arguments.containsKey("first_or_last")) {
			first_or_last = (String) arguments.get("first_or_last");
			return first_or_last + " trading day of the " + time_unit;
		} else if (arguments.containsKey("rises_or_falls")) {
			rises_or_falls = (Integer) arguments.get("rises_or_falls");
			if (rises_or_falls == 1) {
				rf = "rises";
			} else {
				rf = "falls";
			}

			return rf + " for the " + time_unit;
		}

		return null;
		// TODO Auto-generated method stub
		// return super.get_subindicator_name(arguments);
	}

	/***
	 * 
	 * 
	 * @param e
	 * @param day_id
	 * @param indicator_cum_period_name
	 * @return
	 */
	private Boolean day_has_indicator(Entity e, Integer day_id, String indicator_cum_period_name) {

		if (e.getPrice_map_internal().get(day_id).getIndicator_values_on_datapoint()
				.containsKey(indicator_cum_period_name)) {
			return true;
		}
		return false;
	}

	private Boolean datapoint_has_indicator(DataPoint dp, String indicator_cum_period_name) {
		if (dp.getIndicator_values_on_datapoint().containsKey(indicator_cum_period_name)) {
			return true;
		}
		return false;
	}

	private void process_time_unit(final Integer day_id, final Integer prev_day_id, final Entity e,
			final String indicator_cum_period_name, DataPoint prev_dp, final String indicator_name,
			final String time_unit) {

		// PRESENT DP SETTING COMPLEX AS FIRST DAY OF YEAR
		HashMap<String, Object> args_first = new HashMap<String, Object>() {
			{
				put("time_unit", time_unit);
				put("first_or_last", "first");
				put("day_id", day_id);

				put("indicator_cum_period_name", indicator_cum_period_name);
				put("entity", e);
				put("indicator_name", indicator_name);
			}
		};
		set_complex(args_first);

		// PREV DP SETTING COMPLEX AS LAST DAY OF YEAR
		HashMap<String, Object> args_last = new HashMap<String, Object>() {
			{
				put("time_unit", time_unit);
				put("first_or_last", "last");
				put("day_id", prev_day_id);

				put("indicator_cum_period_name", indicator_cum_period_name);
				put("entity", e);
				put("indicator_name", indicator_name);
			}
		};
		set_complex(args_last);

		/****
		 * 
		 * now check what is the price change for the year.
		 * 
		 * 
		 */
		/***
		 * 
		 * check if the previous day id has the indicator or not.
		 * 
		 * and then check if the first day of that time_unit, also has the
		 * indicator, and that theat first day also exists.
		 * 
		 * so basically first we got the first day of the current time_unit
		 * 
		 * then we got the last day of previous time_unit
		 * 
		 * now we got the first day of previous time_unit. cuz we wanna
		 * calculate for the time_unit
		 * 
		 */
		if (day_has_indicator(e, prev_day_id, indicator_cum_period_name)) {

			// now get the first day of the previous year.
			DataPoint first_day_of_previous_time_unit = prev_dp.get_first_of(e, time_unit, prev_day_id);

			if (first_day_of_previous_time_unit != null
					&& datapoint_has_indicator(first_day_of_previous_time_unit, indicator_cum_period_name)) {

				BigDecimal first_day_of_prev_time_unit_indicator_value = first_day_of_previous_time_unit
						.getIndicator_values_on_datapoint().get(indicator_cum_period_name)
						.getIndicator_big_decimal_value();

				BigDecimal last_day_of_prev_time_unit_indicator_value = prev_dp.getIndicator_values_on_datapoint()
						.get(indicator_cum_period_name).getIndicator_big_decimal_value();

				Integer rises_or_falls = null;

				if (last_day_of_prev_time_unit_indicator_value
						.compareTo(first_day_of_prev_time_unit_indicator_value) >= 0) {
					rises_or_falls = 1;
				} else {
					rises_or_falls = -1;
				}

				/**
				if (prev_dp.getDateString().equals("2010-06-04")) {
					if (indicator_cum_period_name.equals("close_period_start_1_period_end")) {
						if (time_unit.equals("week")) {
							System.out.println("rises or falls:" + rises_or_falls);
							System.out.println("first day of prev time unit");
							System.out.println(first_day_of_previous_time_unit.getDateString());
							System.out.println("indicator value:");
							System.out.println(first_day_of_prev_time_unit_indicator_value);
							//System.exit(1);
						}
					}
				}
				**/

				HashMap<String, Object> args = new HashMap<String, Object>();
				args.put("day_id", prev_day_id);
				args.put("indicator_cum_period_name", indicator_cum_period_name);
				args.put("entity", e);
				args.put("rises_or_falls", rises_or_falls);
				args.put("time_unit", time_unit);
				args.put("indicator_name", indicator_name);

				set_complex(args);

			}
		}
	}

	@Override
	public void calculate(Entity e) throws Exception {
		System.out.println("came to calculate");
		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {

			final Integer day_id = entry.getKey();
			final Integer prev_day_id = e.getPrice_map_internal().lowerKey(day_id);

			// System.out.println("the day id is: " + day_id);
			// System.out.println("prev day id is:" + prev_day_id);
			// System.out.println("these are the indicator names");
			// System.out.println(e.getEntity_initial_stub_names().keySet());
			for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {
				// System.out.println("indicator name is:" + indicator_name);
				ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);
				// System.out.println("indicator cum period names are:");
				// System.out.println(indicator_cum_period_names);
				for (final String indicator_cum_period_name : indicator_cum_period_names) {

					if (prev_day_id != null) {
						DataPoint current_dp = entry.getValue();
						DataPoint prev_dp = e.getPrice_map_internal().get(prev_day_id);

						String time_unit = null;
						if (current_dp.new_year(prev_dp)) {

							time_unit = "year";
							process_time_unit(day_id, prev_day_id, e, indicator_cum_period_name, prev_dp,
									indicator_name, time_unit);
						}
						if (current_dp.new_month(prev_dp)) {
							time_unit = "month";
							process_time_unit(day_id, prev_day_id, e, indicator_cum_period_name, prev_dp,
									indicator_name, time_unit);
						}
						if (current_dp.new_quarter(prev_dp)) {
							time_unit = "quarter";
							process_time_unit(day_id, prev_day_id, e, indicator_cum_period_name, prev_dp,
									indicator_name, time_unit);
						}
						if (current_dp.new_week(prev_dp)) {
							time_unit = "week";
							process_time_unit(day_id, prev_day_id, e, indicator_cum_period_name, prev_dp,
									indicator_name, time_unit);
						}

					}

				}

			}
		}
	}

}
