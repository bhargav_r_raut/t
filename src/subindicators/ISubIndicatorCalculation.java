package subindicators;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import com.tinkerpop.gremlin.Tokens.T;

import DayIdMapGenerator.DayIdBuilder;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.CachedIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;

public interface ISubIndicatorCalculation {
	
	public abstract HashMap<Integer, List<Complex>> getResults();
		
	
	
	public abstract String get_subindicator_id(HashMap<String,Object> arguments);
	public abstract String get_subindicator_name(HashMap<String,Object> arguments);
	public abstract void set_complex(HashMap<String,Object> arguments);
	
	/***
	 * To be called directly on the entity
	 * 
	 * @param e
	 * @throws Exception
	 */
	public abstract void calculate(Entity e) throws Exception;
	
	public abstract ClosePriceIndicator get_close_price_indicator_from_given_indicator(
			TreeMap<Integer, dataPointIndicator> dpi_tm);
	
	/***
	 * converts the Decimal 
	 * from TADecimal to 
	 * a bigdecimal value.
	 * 
	 * Does this by first converting the decimal to a string
	 * then converts the string to a bigdecimal using the 
	 * bd_rounding_scale.
	 * 
	 * @param d
	 * @return
	 */
	public abstract BigDecimal decimal_to_bigDecimal(Decimal d);
	
	
	
	
}
