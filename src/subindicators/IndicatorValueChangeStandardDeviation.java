package subindicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import IdAndShardResolver.IdAndShardResolver;
import Indicators.Indicator;
import Indicators.TickMapIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.statistics.StandardDeviationIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class IndicatorValueChangeStandardDeviation extends SubIndicatorCalculation {

	public static final Integer only_consider_periods_greater_than = 1;

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.tailMap(only_consider_periods_greater_than, false).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	/***
	 * FOLLOWING ATTRIBUTES ARE FOR TEST PURPOSES ONLY. HAVE NO FUNCTION TO PLAY
	 * IN THE ACTUAL CODE.
	 * 
	 * populated with the changes in the indicator values over all the periods.
	 * \n
	 * 
	 * key : indicator_cum_period_name-period \n value : treemap of indicator
	 * changes \n
	 * 
	 */
	private HashMap<String, TreeMap<Integer, dataPointIndicator>> indicator_changes_hashmap_for_test;

	public HashMap<String, TreeMap<Integer, dataPointIndicator>> getIndicator_changes_hashmap_for_test() {
		return indicator_changes_hashmap_for_test;
	}

	public void setIndicator_changes_hashmap_for_test(
			HashMap<String, TreeMap<Integer, dataPointIndicator>> indicator_changes_hashmap_for_test) {
		this.indicator_changes_hashmap_for_test = indicator_changes_hashmap_for_test;
	}

	/****
	 * key -> indicator_cum_period_name - period value -> {key =>
	 * "standard_deviation/mean", value -> tickmapindicator}
	 * 
	 */
	private HashMap<String, Object> standard_deviation_and_mean_indicators = new HashMap<String, Object>();

	public HashMap<String, Object> getStandard_deviation_and_mean_indicators() {
		return standard_deviation_and_mean_indicators;
	}

	public void setStandard_deviation_and_mean_indicators(
			HashMap<String, Object> standard_deviation_and_mean_indicators) {
		this.standard_deviation_and_mean_indicators = standard_deviation_and_mean_indicators;
	}

	/*****
	 * 
	 * attributes for test ends.
	 * 
	 */

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub

		// so basically for every period, there are all the standard deviations.
		// so we have to correctly get the ideas for that.
		// period counter.
		//

		Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) arguments
				.get("standard_deviation_amount_and_name");

		Integer period = (Integer) arguments.get("period");

		Integer period_counter = 1;
		for (Map.Entry<Integer, String> entry : custom_periods_map.entrySet()) {

			if (entry.getKey().equals(period)) {

				break;
			} else {
				period_counter++;
			}

		}

		// what would be the total standard deviations per period
		// and how many periods have elapsed
		// the good old shit.
		// so if it was the first period we would have from 0 - 37
		// for the second period it would be
		// (total_stddev - 1) + (total_stddev)
		// for the first period (total - 1)
		// from second period onwards.

		Integer rises_or_falls = (Integer) arguments.get("rises_or_falls");

		/***
		 * what is the position of this standard deviation in the map
		 * 
		 */
		Integer std_dev_position = StandardDeviation.standard_deviations.headMap(nearest_standard_deviation.getKey())
				.size();

		/***
		 * 
		 * if its rise then it remains this position. if its fall then add
		 * 19(total standard deviations considered) to the id.
		 * 
		 * 
		 */
		std_dev_position = rises_or_falls == -1 ? std_dev_position + StandardDeviation.standard_deviations.size()
				: std_dev_position;

		/***
		 * 
		 * 
		 * 
		 */

		if (period_counter > 1) {

			std_dev_position = std_dev_position + (StandardDeviation.standard_deviations.size() * 2 * (period_counter - 1));
		}

		return subindicator_prefix_map.get("IndicatorValueChangeStandardDeviation") + String.valueOf(std_dev_position);
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) arguments
				.get("standard_deviation_amount_and_name");

		

		Integer rises_or_falls = (Integer) arguments.get("rises_or_falls");

		Integer period = (Integer) arguments.get("period");

		String standard_deviation = nearest_standard_deviation.getValue();

		String subindicator_name = "";

		if (rises_or_falls == 1) {
			subindicator_name = period.toString() + " day change rises by " + standard_deviation;
		} else if (rises_or_falls == -1) {
			subindicator_name = period.toString() + " day change falls by " + standard_deviation;
		}

		return subindicator_name;
	}

	public HashMap<String, Object> get_nearest_standard_deviation_entry(SMAIndicator sma, TickMapIndicator sma_tickmap,
			StandardDeviationIndicator stddev_indicator, TickMapIndicator stddev_tickmap,
			TreeMap<Integer, dataPointIndicator> indicator_change_values_over_period, Integer day_id) {

		final BigDecimal indicator_value_change_over_period = indicator_change_values_over_period.get(day_id)
				.getIndicator_big_decimal_value();

		/***
		 * 
		 * now get mean value for this day.
		 * 
		 */

		// the rite way to do this is to get the day id of
		// the first tick.
		// then to get how many day ids are there in between
		// ticks.
		// given a day id.
		// get it from the timeseries.
		// pass in the indicator.

		// System.out.println("day id is:" + day_id);
		// System.out.println("sma tickmap first key:" +
		// sma_tickmap.firstKey());
		// Decimal mean_value_decimal = sma
		// .getValue(sma_tickmap.getTick_to_timeseries_index_map().get(day_id));

		final BigDecimal mean_value = decimal_to_bigDecimal(
				sma.getValue(sma_tickmap.getTick_to_timeseries_index_map().get(day_id)));

		/***
		 * 
		 * now get the standard deviation at this day
		 * 
		 * 
		 */
				final BigDecimal std_dev = decimal_to_bigDecimal(
						stddev_indicator.getValue(stddev_tickmap.getTick_to_timeseries_index_map().get(day_id)));

		/***
		 * 
		 * now subtract current from mean.
		 * 
		 * 
		 */
						final BigDecimal difference = indicator_value_change_over_period.subtract(mean_value);

		// System.out.println("day_id:" + day_id);
		// System.out.println("indicator value:" +
		// indicator_value);
		// System.out.println("std dev:" +
		// std_dev.toPlainString());
		// System.out.println("sma:" +
		// mean_value.toString());

		// what to do if the standard deviation is 0
		// in that case we cannot do anything.
		if (std_dev.compareTo(new BigDecimal("0.00")) != 0) {

			/***
			 * 
			 * now express as a ratio of the standard deviation.
			 * 
			 * 19.1231/120.212 => 19123
			 * 
			 * 0.1 * 100
			 * 
			 */
			BigDecimal ratio_as_percentage = difference
					.divide(std_dev, StockConstants.DataPoint_bd_scale, StockConstants.DataPoint_bd_rounding_mode)
					.multiply(new BigDecimal(StockConstants.DataPoint_multiplicand_for_deriving_percentage_correlate));

			/***
			 * 
			 * now need to convert this ratio to the necessary nearest standard
			 * deviation.
			 * 
			 * 
			 * c
			 */

			Integer ratio_as_integer = ratio_as_percentage.toBigInteger().intValueExact();

			// find nearest standard deviation.

			Entry<Integer, String> nearest_standard_deviation = find_nearest(Math.abs(ratio_as_integer),
					StandardDeviation.standard_deviations);

			HashMap<String, Object> return_values = new HashMap<String, Object>();
			return_values.put("ratio_as_percentage", ratio_as_percentage);
			return_values.put("nearest_standard_deviation", nearest_standard_deviation);

			return return_values;

		}

		return null;
	}

	/****
	 * 
	 * this is a measure of the change in the values of the different
	 * indicators, over all the periods that we have
	 * 
	 * eg: if period is 2.
	 * 
	 * 1. make a series of the changes in teh values over 2 days for the
	 * indicator. 2. now calculate a 300 day standard deviation 3. now calculate
	 * the mean. 4. now calculate the difference between the value on any day
	 * and that mean. 5. now calculate that difference as a percentage of the
	 * standard deviation.
	 * 
	 * do this for all the periods.
	 * 
	 * 
	 */
	public void process_period_for_indicator(Indicator o, Integer period,
			TreeMap<Integer, dataPointIndicator> indicator_values, Entity e, String indicator_cum_period_name,
			String indicator_name) {

		TreeMap<Integer, dataPointIndicator> indicator_change_values_over_period = o
				.get_period_change_indicator_values(indicator_values, period);

		/***
		 * used for the test. this hashmap will be automatically cleared if the
		 * main object is nullified.
		 */
		getIndicator_changes_hashmap_for_test().put(indicator_cum_period_name + "-" + period.toString(),
				indicator_change_values_over_period);

		ClosePriceIndicator indicator_changes_as_close_price = get_close_price_indicator_from_given_indicator(
				indicator_change_values_over_period);

		StandardDeviationIndicator stddev_indicator = new StandardDeviationIndicator(indicator_changes_as_close_price,
				StockConstants.total_pairs_for_any_given_days);

		TickMapIndicator stddev_tickmap = new TickMapIndicator(stddev_indicator);

		SMAIndicator sma = new SMAIndicator(indicator_changes_as_close_price,
				StockConstants.total_pairs_for_any_given_days);

		TickMapIndicator sma_tickmap = new TickMapIndicator(sma);

		// TickMapIndicator close_price_tickmap = new
		// TickMapIndicator(indicator_changes_as_close_price);

		getStandard_deviation_and_mean_indicators()
				.put(indicator_cum_period_name + "-" + period.toString() + "-standard-deviation", stddev_tickmap);

		getStandard_deviation_and_mean_indicators().put(indicator_cum_period_name + "-" + period.toString() + "-mean",
				sma_tickmap);

		getStandard_deviation_and_mean_indicators()
				.put(indicator_cum_period_name + "-" + period.toString() + "-mean-indicator", sma);

		getStandard_deviation_and_mean_indicators().put(
				indicator_cum_period_name + "-" + period.toString() + "-standard-deviation-indicator",
				stddev_indicator);

		// getStandard_deviation_and_mean_indicators().put(indicator_cum_period_name
		// + "-" + period.toString() + "-price-change", close_price_tickmap);

		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {
			Integer day_id = entry.getKey();
			DataPoint dp = entry.getValue();

			// if day id is greater than or equal to latest newly downloaded day
			// id.
			// then check its difference from the mean.
			if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {

				/****
				 * 
				 * get the indicator value on this day id.
				 * 
				 * 
				 */
				/// check if the indicator value even exists.
				if (indicator_change_values_over_period.get(day_id) != null) {

					// find nearest standard deviation.
					// ratio as percentage and nearest standard deviation are
					// both returned.

					HashMap<String, Object> calc_values = get_nearest_standard_deviation_entry(sma, sma_tickmap,
							stddev_indicator, stddev_tickmap, indicator_change_values_over_period, day_id);

					if (calc_values != null) {

						Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) calc_values
								.get("nearest_standard_deviation");

						BigDecimal ratio_as_percentage = (BigDecimal) calc_values.get("ratio_as_percentage");

						/***
						 * now pass it into the get_subindicator_name and
						 * get_subindicator_id functions respectively.
						 * 
						 */
						HashMap<String, Object> arguments = new HashMap<String, Object>();
						arguments.put("standard_deviation_amount_and_name", nearest_standard_deviation);
						arguments.put("day_id", day_id);
						arguments.put("rises_or_falls", ratio_as_percentage.compareTo(new BigDecimal("0.00")));
						arguments.put("indicator_cum_period_name", indicator_cum_period_name);
						arguments.put("entity", e);
						arguments.put("indicator_name", indicator_name);
						arguments.put("period", period);
						set_complex(arguments);

					}

				}
			}
		}

	}

	public void calculate(Entity e) {

		/**
		 * for testing purposes this hashmap is created. it stores the indicator
		 * value change treemaps for each indicator_name-period.
		 * 
		 */
		setIndicator_changes_hashmap_for_test(new HashMap<String, TreeMap<Integer, dataPointIndicator>>());

		for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {

			ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

			/****
			 * iterate each applicable indicator cum period name.
			 * 
			 * 
			 */
			for (final String indicator_cum_period_name : indicator_cum_period_names) {
				System.out.println("this is the indicator cum period name");
				System.out.println(indicator_cum_period_name);
				/****
				 * 
				 * 
				 * create an indicator object.
				 * 
				 */
				Indicator o = new Indicator();

				o.setIndicator_and_period_name(indicator_cum_period_name);

				/****
				 * get a treemap of the type
				 * 
				 * if today its 89 and yesterday it was 77 then actually we have
				 * calculated it is the mean price
				 * 
				 * key -> day id value -> datapointIndicator
				 */
				TreeMap<Integer, dataPointIndicator> indicator_values = o.get_indicator_values(e);

				// now for each period.
				// let it return a hashmap?
				for (Integer period : custom_periods_map.keySet()) {
					process_period_for_indicator(o, period, indicator_values, e, indicator_cum_period_name,
							indicator_name);
				}

			}
		}
	}

	/****
	 * 
	 * for testing the correct id generation. will generate a hashmap of names
	 * -> ids for all the possible period and standard deviation combinations.
	 * you can then test and see if the function is calculating it correctly.
	 * 
	 */
	public HashMap<String,String> get_subindicator_names_to_ids() {
			
		HashMap<String,String> subindicator_names_and_ids = new HashMap<String,String>();
		
		Integer id_counter = 0;
		
		for (Integer period : custom_periods_map.keySet()) {
			
			ArrayList<Integer> rises_or_falls = new ArrayList<Integer>(Arrays.asList(1, -1));
			for (Integer rf : rises_or_falls) {

				for (Map.Entry<Integer, String> standard_deviations : StandardDeviation.standard_deviations
						.entrySet()) {

					Integer standard_deviation = standard_deviations.getKey();
					String standard_deviation_name = standard_deviations.getValue();
						
					// what will be the name?
					// so we have to calculate both.
					String subindicator_name = "";
					
					
					
					if(rf.equals(1)){
						subindicator_name = period.toString() + " day change rises by " + standard_deviation_name;
						
					}
					else{
						subindicator_name = period.toString() + " day change falls by " + standard_deviation_name;
					}
						
					String subindicator_id = SubIndicatorCalculation
							.subindicator_prefix_map.
							get
							("IndicatorValueChangeStandardDeviation")
							+ id_counter.toString();
					
					
					subindicator_names_and_ids.put(subindicator_name, subindicator_id);
					
					id_counter++;
				}
			}
		}
		
		//System.exit(1);
		
		return subindicator_names_and_ids;
	}

}
