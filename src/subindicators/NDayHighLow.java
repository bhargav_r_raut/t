package subindicators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ComplexProcessor.indicatorNDayHighLow;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import IdAndShardResolver.IdAndShardResolver;

/****
 * 
 * @author bhargav
 *
 */
public class NDayHighLow extends SubIndicatorCalculation {

	public static final Integer only_consider_periods_greater_than = 2;

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.tailMap(only_consider_periods_greater_than, false).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	/***
	 * to call something a ten day high. we need at least 10 datapoints before
	 * it.
	 * 
	 * so here we want actual "period" datapoints before any given day.
	 * 
	 * 
	 * @param e
	 * @throws Exception
	 */
	public void calculate(Entity e) throws Exception {

		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {
			Integer day_id = entry.getKey();

			for (ArrayList<String> indicator_cum_period_names : e.getEntity_initial_stub_names().values()) {
				for (String indicator_cum_period_name : indicator_cum_period_names) {

					HashMap<String, dataPointIndicator> indicator_values_at_datapoint = e.getPrice_map_internal()
							.get(day_id).getIndicator_values_on_datapoint();

					if (indicator_values_at_datapoint.containsKey(indicator_cum_period_name)) {

						Integer indicator_value_at_day_id = indicator_values_at_datapoint.get(indicator_cum_period_name)
								.getIndicator_integer_value();

						if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {
							HashSet<Integer> comparisons = new HashSet<Integer>();
							// iterate backwards from this day id.
							for (int i = (day_id - 1); i >= e.getPrice_map_internal().firstKey(); i--) {
								// the day id exists.
								if (e.getPrice_map_internal().containsKey(i)) {
									// the indicator value exists on that day
									// id.
									if (e.getPrice_map_internal().get(i).getIndicator_values_on_datapoint()
											.containsKey(indicator_cum_period_name)) {
										Integer indicator_value_on_compared_day_id = e.getPrice_map_internal().get(i)
												.getIndicator_values_on_datapoint().get(indicator_cum_period_name)
												.getIndicator_integer_value();
										comparisons.add(indicator_value_at_day_id
												.compareTo(indicator_value_on_compared_day_id));
									}
								}
								// is the difference a period?
								// this is numerical days
								// not datapoints.
								Integer period = day_id - i + 1;
								if (custom_periods_map.containsKey(period)) {
									// if the sum of the comparisons is either
									// 1, or -1
									// then we have a high/low.
									// if not then

									Integer sum = 0;
									for (Integer c : comparisons) {
										sum += c;
									}

									if (sum == 1 || sum == -1) {
										// its a n day high.
										// can we continue processing -> yes.

										HashMap<String, Object> args = new HashMap<String, Object>();
										args.put("period", period);
										args.put("sum", sum);
										args.put("day_id", day_id);
										args.put("indicator_cum_period_name", indicator_cum_period_name);
										args.put("entity", e);
										set_complex(args);
									} else {
										// its neither high nor low.
										// they can all be equals.
										// if they are all equal, then we cant
										// break, because
										// this is incorrect.
										// breaking is done when it is confirmed
										// that this is neither
										// the highest or lowest point in the
										// given period.
										// but all being equals, does not
										// satisfy this conditions.
										// more periods can be investigated to
										// see if this is the
										// highest or lowest value for them.
										if (comparisons.size() == 1 && comparisons.contains(0)) {
											// for the event where they are all
											// equals, we do not break.
										} else {
											break;
										}
									}
								}
							}
						}
					}
				}
			}

		}
	}

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		Integer period = (Integer) arguments.get("period");
		Integer high_or_low = (Integer) arguments.get("high_or_low");
		
		/***
		 * simplify the calculation of the subindicator id.
		 * 
		 */
		if(high_or_low.equals(1)){
			high_or_low = 0;
		}
		else{
			high_or_low = 1;
		}
		
		
		return subindicator_prefix_map.get("NDayHighLow") +
				((high_or_low * NDayHighLow.custom_periods_map.size())
				+ (NDayHighLow.custom_periods_map.headMap(period).size()));
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		// return super.get_subindicator_name(arguments);
		Integer period = (Integer) arguments.get("period");
		Integer high_or_low = (Integer) arguments.get("high_or_low");
		String name = custom_periods_map.get(period);
		Pattern p = Pattern.compile("s$");
		Matcher m = p.matcher(name);
		while (m.find()) {
			name = m.replaceFirst("");
		}
		if (high_or_low == 1) {
			name = name + " high";
		} else if (high_or_low == -1) {
			name = name + " low";
		}
		return name;
	}

	@Override
	public void set_complex(HashMap<String, Object> arguments) {
		final Integer p = (Integer) arguments.get("period");
		final Integer sum = (Integer) arguments.get("sum");
		Integer day_id = (Integer) arguments.get("day_id");
		String indicator_cum_period_name = (String) arguments.get("indicator_cum_period_name");
		Entity e = (Entity) arguments.get("entity");

		String subindicator_id = get_subindicator_id(new HashMap<String, Object>() {
			{
				put("period", p);
				put("high_or_low", sum);
			}
		});
		String subindicator_name = get_subindicator_name(new HashMap<String, Object>() {
			{
				put("period", p);
				put("high_or_low", sum);
			}
		});
		String indicator_id = CentralSingleton.stub_hashmap.get(indicator_cum_period_name).getId();
		String entity_id = e.getEntity_es_id();
		String complex_id = entity_id + indicator_id + subindicator_id;
		Complex c = new Complex(indicator_cum_period_name, complex_id, subindicator_id, entity_id, day_id,
				subindicator_name);

		// System.out.println(getResults());
		if (getResults().get(day_id) == null) {
			getResults().put(day_id, new ArrayList<Complex>());
		}
		getResults().get(day_id).add(c);
		// TODO Auto-generated method stub
		// super.set_complex(arguments);
	}

	public HashMap<String, String> get_subindicator_names_to_ids() {

		HashMap<String, String> names_to_ids = new HashMap<String, String>();

		ArrayList<Integer> high_or_low = new ArrayList<Integer>(Arrays.asList(0, 1));

		Integer id_counter = 0;
		for (Integer hl : high_or_low) {
		for (Integer period : custom_periods_map.keySet()) {
			

				String subindicator_name = null;
				//String subindicator_id = null;

				String period_name = custom_periods_map.get(period);
				if (period_name.endsWith("s")) {
					subindicator_name = period_name.substring(0, period_name.length() - 1);
				}
				else{
					subindicator_name = period_name;
				}

				if (hl.equals(0)) {
					subindicator_name = subindicator_name + " high";
				} else {
					subindicator_name = subindicator_name + " low";
				}

				names_to_ids.put(subindicator_name, subindicator_prefix_map.get("NDayHighLow") + id_counter.toString());

				id_counter++;
			}
		}

		return names_to_ids;
	}

}
