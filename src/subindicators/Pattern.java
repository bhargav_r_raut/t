package subindicators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import Indicators.Indicator;

public class Pattern extends SubIndicatorCalculation {

	public static final String RISE_REPRESENTATION = "1";
	public static final String FALL_REPRESENTATION = "2";

	public static final HashMap<String, String[]> permutations_map = new LinkedHashMap<String, String[]>() {
		{

			put("11", new String[] { "0", "shows up_up pattern", "rises for 2 sessions" });
			put("21", new String[] { "1", "shows down_up pattern" });
			put("12", new String[] { "2", "shows up_down pattern" });
			put("22", new String[] { "3", "shows down_down pattern", "falls for 2 sessions" });
			put("111", new String[] { "4", "shows up_up_up pattern", "rises for 3 sessions" });
			put("211", new String[] { "5", "shows down_up_up pattern" });
			put("121", new String[] { "6", "shows up_down_up pattern" });
			put("221", new String[] { "7", "shows down_down_up pattern" });
			put("112", new String[] { "8", "shows up_up_down pattern" });
			put("212", new String[] { "9", "shows down_up_down pattern" });
			put("122", new String[] { "10", "shows up_down_down pattern" });
			put("222", new String[] { "11", "shows down_down_down pattern", "falls for 3 sessions" });
			put("1111", new String[] { "12", "shows up_up_up_up pattern", "rises for 4 sessions" });
			put("2111", new String[] { "13", "shows down_up_up_up pattern" });
			put("1211", new String[] { "14", "shows up_down_up_up pattern" });
			put("2211", new String[] { "15", "shows down_down_up_up pattern" });
			put("1121", new String[] { "16", "shows up_up_down_up pattern" });
			put("2121", new String[] { "17", "shows down_up_down_up pattern" });
			put("1221", new String[] { "18", "shows up_down_down_up pattern" });
			put("2221", new String[] { "19", "shows down_down_down_up pattern" });
			put("1112", new String[] { "20", "shows up_up_up_down pattern" });
			put("2112", new String[] { "21", "shows down_up_up_down pattern" });
			put("1212", new String[] { "22", "shows up_down_up_down pattern" });
			put("2212", new String[] { "23", "shows down_down_up_down pattern" });
			put("1122", new String[] { "24", "shows up_up_down_down pattern" });
			put("2122", new String[] { "25", "shows down_up_down_down pattern" });
			put("1222", new String[] { "26", "shows up_down_down_down pattern" });
			put("2222", new String[] { "27", "shows down_down_down_down pattern", "falls for 4 sessions" });
			put("11111", new String[] { "28", "shows up_up_up_up_up pattern", "rises for 5 sessions" });
			put("21111", new String[] { "29", "shows down_up_up_up_up pattern" });
			put("12111", new String[] { "30", "shows up_down_up_up_up pattern" });
			put("22111", new String[] { "31", "shows down_down_up_up_up pattern" });
			put("11211", new String[] { "32", "shows up_up_down_up_up pattern" });
			put("21211", new String[] { "33", "shows down_up_down_up_up pattern" });
			put("12211", new String[] { "34", "shows up_down_down_up_up pattern" });
			put("22211", new String[] { "35", "shows down_down_down_up_up pattern" });
			put("11121", new String[] { "36", "shows up_up_up_down_up pattern" });
			put("21121", new String[] { "37", "shows down_up_up_down_up pattern" });
			put("12121", new String[] { "38", "shows up_down_up_down_up pattern" });
			put("22121", new String[] { "39", "shows down_down_up_down_up pattern" });
			put("11221", new String[] { "40", "shows up_up_down_down_up pattern" });
			put("21221", new String[] { "41", "shows down_up_down_down_up pattern" });
			put("12221", new String[] { "42", "shows up_down_down_down_up pattern" });
			put("22221", new String[] { "43", "shows down_down_down_down_up pattern" });
			put("11112", new String[] { "44", "shows up_up_up_up_down pattern" });
			put("21112", new String[] { "45", "shows down_up_up_up_down pattern" });
			put("12112", new String[] { "46", "shows up_down_up_up_down pattern" });
			put("22112", new String[] { "47", "shows down_down_up_up_down pattern" });
			put("11212", new String[] { "48", "shows up_up_down_up_down pattern" });
			put("21212", new String[] { "49", "shows down_up_down_up_down pattern" });
			put("12212", new String[] { "50", "shows up_down_down_up_down pattern" });
			put("22212", new String[] { "51", "shows down_down_down_up_down pattern" });
			put("11122", new String[] { "52", "shows up_up_up_down_down pattern" });
			put("21122", new String[] { "53", "shows down_up_up_down_down pattern" });
			put("12122", new String[] { "54", "shows up_down_up_down_down pattern" });
			put("22122", new String[] { "55", "shows down_down_up_down_down pattern" });
			put("11222", new String[] { "56", "shows up_up_down_down_down pattern" });
			put("21222", new String[] { "57", "shows down_up_down_down_down pattern" });
			put("12222", new String[] { "58", "shows up_down_down_down_down pattern" });
			put("22222", new String[] { "59", "shows down_down_down_down_down pattern", "falls for 5 sessions" });
			put("111111", new String[] { "60", "shows up_up_up_up_up_up pattern", "rises for 6 sessions" });
			put("222222", new String[] { "61", "shows down_down_down_down_down_down pattern", "falls for 6 sessions" });
			put("1111111", new String[] { "62", "shows up_up_up_up_up_up_up pattern", "rises for 7 sessions" });
			put("2222222",
					new String[] { "63", "shows down_down_down_down_down_down_down pattern", "falls for 7 sessions" });
			put("11111111", new String[] { "64", "shows up_up_up_up_up_up_up_up pattern", "rises for 8 sessions" });
			put("22222222", new String[] { "65", "shows down_down_down_down_down_down_down_down pattern",
					"falls for 8 sessions" });
			put("111111111", new String[] { "66", "shows up_up_up_up_up_up_up_up_up pattern", "rises for 9 sessions" });
			put("222222222", new String[] { "67", "shows down_down_down_down_down_down_down_down_down pattern",
					"falls for 9 sessions" });
			put("1111111111",
					new String[] { "68", "shows up_up_up_up_up_up_up_up_up_up pattern", "rises for 10 sessions" });
			put("2222222222", new String[] { "69", "shows down_down_down_down_down_down_down_down_down_down pattern",
					"falls for 10 sessions" });

		}
	};

	public static final TreeMap<Integer, String> periods_map = new TreeMap<Integer, String>() {
		{
			put(2, "2 sessions");
			put(3, "3 sessions");
			put(4, "4 sessions");
			put(5, "5 sessions");
			put(6, "6 sessions");
			put(7, "7 sessions");
			put(8, "8 sessions");
			put(9, "9 sessions");
			put(10, "10 sessions");
		}
	};

	private HashMap<String, TreeMap<Integer, String>> patterns_map;

	public HashMap<String, TreeMap<Integer, String>> getPatterns_map() {
		return patterns_map;
	}

	public void setPatterns_map(HashMap<String, TreeMap<Integer, String>> patterns_map) {
		this.patterns_map = patterns_map;
	}

	@Override
	public void setResults(HashMap<Integer, List<Complex>> results) {
		// TODO Auto-generated method stub
		super.setResults(results);
	}

	@Override
	public void set_complex(HashMap<String, Object> arguments) {
		Integer period = (Integer) arguments.get("period");
		final String pattern = (String) arguments.get("pattern");
		Integer day_id = (Integer) arguments.get("day_id");
		Entity e = (Entity) arguments.get("entity");
		String indicator_cum_period_name = (String) arguments.get("indicator_cum_period_name");
		String indicator_name = (String) arguments.get("indicator_name");
		String indicator_id = CentralSingleton.stub_hashmap.get(indicator_cum_period_name).getId();
		String subindicator_name = get_subindicator_name(new HashMap<String, Object>() {
			{
				put("pattern", pattern);
			}
		});
		String subindicator_id = get_subindicator_id(new HashMap<String, Object>() {
			{
				put("pattern", pattern);
			}
		});

		String entity_id = e.getEntity_es_id();
		String complex_id = entity_id + indicator_id + subindicator_id;
		Complex c = new Complex(indicator_cum_period_name, complex_id, subindicator_id, entity_id, day_id,
				subindicator_name);

		// System.out.println(getResults());
		if (getResults().get(day_id) == null) {
			getResults().put(day_id, new ArrayList<Complex>());
		}

		/**
		 * if(entity_id.equals("E-0")){ if(indicator_id.equals("I16_0")){
		 * if(subindicator_id.equals("sb6")){ System.out.println(
		 * "detected on day_id:" + day_id); } } }
		 **/

		getResults().get(day_id).add(c);
	}

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		// return super.get_subindicator_id(arguments);
		String pattern = (String) arguments.get("pattern");
		String[] names = permutations_map.get(pattern);
		String subindicator_id = subindicator_prefix_map.get("Pattern") + names[0];
		return subindicator_id;

	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		String pattern = (String) arguments.get("pattern");
		String[] names = permutations_map.get(pattern);

		// if there is an alias i.e rises/falls for n sessions, then use
		// that(names.length == 3)
		// otherwise use the shows up_down_blah_blah pattern thing.
		if (names.length == 3) {
			return names[2];
		} else {
			return names[1];
		}

	}

	/****
	 * each pattern is assembled for every newly downloaded datapoint. the
	 * pattern is built using the datapoints, and not the blank day ids. for eg:
	 * Assume the following sequence 1* 2 3 4 5* 6 7 8 Here day id 1 and 5 does
	 * not have data. In order to generate the 3 day pattern for day id 6 the
	 * following day ids are used: 3_4_6 So we take 3 actual datapoints, and
	 * ignore the blank day ids. This is unlike what is done in RiseFallAmount,
	 * and NDayHighLow, where absolute day ids are considered.
	 */
	@Override
	public void calculate(Entity e) throws Exception {
		
		for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {

			ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

			for (final String indicator_cum_period_name : indicator_cum_period_names) {

				Indicator o = new Indicator();
				o.setIndicator_and_period_name(indicator_cum_period_name);
				if (!getPatterns_map().containsKey(o.getIndicator_and_period_name())) {
					getPatterns_map().put(o.getIndicator_and_period_name(),
							o.get_indicator_values_as_pattern(e, RISE_REPRESENTATION, FALL_REPRESENTATION));
				}

				TreeMap<Integer, String> indicator_rise_fall_patterns = getPatterns_map()
						.get(o.getIndicator_and_period_name());

				if (indicator_name.equals("close")) {
					System.out.println("indicator cum period name:" + o.getIndicator_and_period_name());
					for (Integer dd : e.getPrice_map_internal().keySet()) {
						if (dd < 30) {
							System.out
									.println("day id:" + dd + ": close :"
											+ e.getPrice_map_internal().get(dd).getIndicator_values_on_datapoint()
													.get(o.getIndicator_and_period_name())
													.getIndicator_big_decimal_value());

							System.out.println("pattern:");
							if (indicator_rise_fall_patterns.containsKey(dd)) {
								System.out.println(indicator_rise_fall_patterns.get(dd));
							}
						}
					}
				}

				for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {

					Integer day_id = entry.getKey();

					DataPoint d = entry.getValue();

					if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {

						NavigableMap<Integer, String> nv = indicator_rise_fall_patterns.headMap(day_id, true);
						ArrayList<Integer> day_ids_in_ascending_order = new ArrayList<Integer>(nv.keySet());
						for (String pattern : permutations_map.keySet()) {
					
							Integer pattern_length = pattern.length();
							
							Integer from_index = day_ids_in_ascending_order.size() - 1 - pattern_length + 1;
							if(from_index < 0 ){
								from_index = 0;
							}
							Integer to_index = day_ids_in_ascending_order.size();
							if(to_index < 0){
								to_index = 0;
							}
							
							List<Integer> days_sublist = day_ids_in_ascending_order.subList(
									from_index,
									to_index);
							String pattern_sublist = "";
							for(Integer i : days_sublist){
								pattern_sublist += nv.get(i);
							}
							
							
							
							
							
							
							if (pattern_sublist.toString().equals(pattern)) {
								

								if(indicator_name.equals("close") && 
										pattern.equals("121") && day_id < 50){
									
									System.out.println("day id is:" + day_id + 
											" pattern sublist:" + pattern_sublist + "pattern searched:"
											+ pattern);
									
								}
								
								HashMap<String, Object> args = new HashMap<String, Object>();
								args.put("pattern", pattern);
								args.put("period", pattern_length);
								args.put("day_id", day_id);
								args.put("indicator_cum_period_name", indicator_cum_period_name);
								args.put("entity", e);
								set_complex(args);
							}

						}
						
					}
				}
			}
		}
	}

	public Pattern() {
		super();
		setPatterns_map(new HashMap<String, TreeMap<Integer, String>>());
	}

}
