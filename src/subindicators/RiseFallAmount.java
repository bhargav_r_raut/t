package subindicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import IdAndShardResolver.IdAndShardResolver;
import Indicators.Indicator;

public class RiseFallAmount extends SubIndicatorCalculation {

	public static final TreeMap<String, Integer> indicators_applicable = new TreeMap<String, Integer>() {
		{
			put("close", 1);
			put("high", 1);
			put("open", 1);
			put("low", 1);
		}
	};

	// first define the rise or fall amounts
	// and also for which indicators they apply.
	// rise fall amounts 450
	// standard deviation total 129(7 standard deviations, 9 periods * 2)
	// patterns : 40
	// sma_cross : 2 month, 1 month, 3 weeks, 2 weeks, 1 weeks, 5 day(10 + 8 + 6
	// + 4 + 2): 30
	// high_low : 36
	// total works out to around 692.
	public static final TreeMap<Integer, String> periods_map = new TreeMap<Integer, String>() {
		{
			put(1, "1 day");
			put(2, "2 days");
			put(3, "3 days");
			put(7, "1 week");
			put(14, "2 weeks");
			put(21, "3 weeks");
			put(28, "1 month");
			put(60, "2 months");
			put(90, "3 months");
			put(180, "6 months");
			put(270, "9 months");
			put(365, "1 year");
			put(540, "18 months");
			put(630, "21 months");
			put(730, "2 years");
		}
	};

	// total subindicators become:
	// periods(15)*rise_fall_amounts(15)*(falls or rises[2]) = 450
	// get (percentage index in map)*15 -> total percentages* periods_before_it
	// + (period index in map) => this_percentage -> periods_before it.
	// if rise -> this
	// if fall -> total rise + this.
	// lets rise : 0, fall 1

	public static final TreeMap<Integer, String> rise_fall_amounts = new TreeMap<Integer, String>() {
		{
			put(5, "half percent");
			put(10, "one percent");
			put(15, "one and a half percent");
			put(20, "two percent");
			put(25, "two and a half percent");
			put(30, "three percent");
			put(35, "three and a half percent");
			put(40, "four percent");
			put(50, "five percent");
			put(100, "ten percent");
			put(200, "twenty percent");
			put(400, "forty percent");
			put(600, "sixty percent");
			put(1000, "hundred percent");
			put(2000, "two hundred percent");
			put(3000, "three hundred percent or more");
		}
	};

	// need to add just plain old rises
	// in the time period.
	// so rises in 2 days.

	public HashMap<Integer, List<Complex>> results;

	public HashMap<Integer, List<Complex>> getResults() {
		return results;
	}

	public void setResults(HashMap<Integer, List<Complex>> results) {
		this.results = results;
	}

	public void calculate(final Entity e) {

		for (Map.Entry<Integer, DataPoint> entry : e.getPrice_map_internal().entrySet()) {

			final Integer day_id = entry.getKey();

			for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {

				if (indicators_applicable.containsKey(indicator_name)) {
					ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

					for (final String indicator_cum_period_name : indicator_cum_period_names) {

						HashMap<String, dataPointIndicator> indicator_values_at_datapoint = e.getPrice_map_internal()
								.get(day_id).getIndicator_values_on_datapoint();

						if (indicator_values_at_datapoint.containsKey(indicator_cum_period_name)) {

							// here we do the first_day_last_day_calculation.

							BigDecimal end_day_indicator = indicator_values_at_datapoint.get(indicator_cum_period_name)
									.getIndicator_big_decimal_value();

							if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {
								for (int i = (day_id - 1); i >= e.getPrice_map_internal().firstKey(); i--) {
									// the day id exists.
									final Integer period = day_id - i;

									if (e.getPrice_map_internal().containsKey(i)) {
										// the indicator value exists on that
										// day
										// id.
										if (e.getPrice_map_internal().get(i).getIndicator_values_on_datapoint()
												.containsKey(indicator_cum_period_name)) {

											// definition as per this page
											// www.investopedia.com/ask/answers/03/100303.asp
											// Integer diff =
											// indicator_value_at_day_id -
											// indicator_value_on_compared_day_id;
											// Integer percentage_change =
											// Double.valueOf(diff)/Double.valueOf(indicator_value_on_compared_day_id;
											BigDecimal start_day_indicator = e.getPrice_map_internal().get(i)
													.getIndicator_values_on_datapoint().get(indicator_cum_period_name)
													.getIndicator_big_decimal_value();

											final Integer percentage_change = Indicator
													.get_percentage_change(start_day_indicator, end_day_indicator);
											
											if(day_id.equals(1)){
												System.out.println("percentage change is:");
												System.out.println(percentage_change);
											}
											// so here can add this.

											// here we have the situation where
											// the percentage change if negative
											// will return shit in the
											// find_nearest
											// so we must first multiply it by
											final Map.Entry<Integer, String> nearest_change = find_nearest(
													percentage_change < 0 ? percentage_change * -1 : percentage_change,
													rise_fall_amounts);
											if(day_id.equals(1)){
												System.out.println("nearest change is: " + nearest_change);
											}
											
											
											final Integer approx_percentage_change = percentage_change < 0 ? nearest_change.getKey()*-1 : nearest_change.getKey();
											
											
											if (periods_map.containsKey(period)) {

												set_complex(new HashMap<String, Object>() {
													{
														put("percentage_change",
																approx_percentage_change);
														put("day_id", day_id);
														put("period", period);
														put("entity", e);
														put("indicator_cum_period_name", indicator_cum_period_name);

													}
												});
											}

										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Integer period = (Integer) arguments.get("period");
		Integer percentage_change = (Integer) arguments.get("percentage_change");
		Integer rise_or_fall = percentage_change >= 0 ? 0 : 1;

		// total periods in the periods map.
		Integer total_periods = periods_map.size();

		// total rise fall amounts
		Integer total_percentage_changes = rise_fall_amounts.size();

		// how many percentage changes are less than this one in the
		// rise_fall_amouts map
		// eg: if percentage change is: 20, then 3 are there before it, so index
		// is 3.
		Integer percentage_change_index = rise_fall_amounts.headMap(percentage_change).size();

		// for every percentage before the current one, we multiply by the
		// periods per
		// percentage.
		Integer percentage_period_combinations_elapsed = percentage_change_index * total_periods;

		// finally for the current percentage, how many periods are there before
		// this period
		Integer periods_elapsed_on_current_percentage = periods_map.headMap(period).size();

		// sum of above two.
		Integer combinations_elapsed = percentage_period_combinations_elapsed + periods_elapsed_on_current_percentage;

		// begin counting with 0 for rise, but if fall, then all the rise
		// combinations have to e added
		// so if its rise, the second part of multiplication will be 0,
		// otherwise it will be total_periods*total percentages.
		combinations_elapsed = combinations_elapsed + (total_percentage_changes * total_periods * rise_or_fall);

		// finally just prepend the prefix for rise or fall to the above number
		// since we start from 0, we can directly use the above number, as the
		// first
		// subindicator of this group will be si0
		// so if we get a number of 10, then it means si9 is taken, so we
		// directly
		// assign this one as si10.
		return subindicator_prefix_map.get("RiseFallAmount") + String.valueOf(combinations_elapsed);
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		
		Integer period = (Integer) arguments.get("period");
		Integer percentage_change = (Integer) arguments.get("percentage_change");
		String rise_or_fall = percentage_change >= 0 ? "rises by" : "falls by";
		String name = rise_or_fall + " " + rise_fall_amounts.get(Math.abs(percentage_change)) + " in " + periods_map.get(period);
		return name;
	}

	/***
	 * for test purposes.
	 * 
	 * @return
	 */
	public HashMap<String, String> get_subindicator_names_to_ids() {

		HashMap<String, String> names_to_ids = new HashMap<String, String>();

		Integer id_counter = 0;
		ArrayList<Integer> rise_fall_list = new ArrayList<Integer>(Arrays.asList(0, 1));
		for (Integer period : periods_map.keySet()) {
			for (Integer rise_fall : rise_fall_amounts.keySet()) {
				for (Integer rf : rise_fall_list) {

					String subindicator_name = "";
					if (rf.equals(0)) {
						subindicator_name = "rises by " + rise_fall_amounts.get(rise_fall) + " in "
								+ periods_map.get(period);
					} else {
						subindicator_name = "falls by " + rise_fall_amounts.get(rise_fall) + " in "
								+ periods_map.get(period);
					}
					String subindicator_id = SubIndicatorCalculation.subindicator_prefix_map.get("RiseFallAmount")
							+ id_counter.toString();

					names_to_ids.put(subindicator_name, subindicator_id);

					id_counter++;
				}
			}

		}

		return names_to_ids;
	}

}
