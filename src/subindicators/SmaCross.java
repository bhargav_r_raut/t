package subindicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import IdAndShardResolver.IdAndShardResolver;
import Indicators.Indicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;

public class SmaCross extends SubIndicatorCalculation {

	public static final Integer only_consider_periods_greater_than = 2;

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.tailMap(only_consider_periods_greater_than, false).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	/***
	 * HashMap<String,HashMap<Integer,TreeMap<Integer,BigDecimal>>>
	 * 
	 * key => String : indicator_cum_period_name value =>
	 * HashMap<Integer,TreeMap<Integer,BigDecimal>>
	 * 
	 * ======key => day_id ======value => TreeMap<Integer,BigDecimal>
	 * 
	 * 
	 * ==============key => period ==============value => sma value.
	 * 
	 */
	private HashMap<String, HashMap<Integer, TreeMap<Integer, BigDecimal>>> sma_map;

	public HashMap<String, HashMap<Integer, TreeMap<Integer, BigDecimal>>> getSma_map() {
		return sma_map;
	}

	public void setSma_map(HashMap<String, HashMap<Integer, TreeMap<Integer, BigDecimal>>> sma_map) {
		this.sma_map = sma_map;
	}

	@Override
	public HashMap<Integer, List<Complex>> getResults() {
		// TODO Auto-generated method stub
		return super.getResults();
	}

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Integer crosser = (Integer) arguments.get("crosser");
		Integer crossed = (Integer) arguments.get("crossed");
		
		Integer crosser_rank = custom_periods_map.headMap(crosser).size();
		System.out.println("crosser rank:" + crosser_rank);
		// now we know the rank.
		// so suppose we multiply by custom_periods.size - 1 
		// we get 60
		// now we minus one from that
		// till now the periods 
		Integer complexes_upto_now = crosser_rank*(custom_periods_map.size() - 1);
		
		
		System.out.println("complexes upto now:" + complexes_upto_now);
		// now we need to know the crossed rank inclusive
		
		Integer crossed_rank = 0;
		// count all the elements in custom_periods_map
		// and then ignore 
		for(Integer key : custom_periods_map.keySet()){
			if(key.equals(crosser)){
				//break;
			}
			else{
		
				if(key.equals(crossed)){
					break;
				}
				crossed_rank++;
			}
		}
		
		
		
		System.out.println("crossed rank: " + crossed_rank);
		
		String id = subindicator_prefix_map.get("SmaCross") + 
				(complexes_upto_now + crossed_rank);

		return id;
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Integer crosser = (Integer) arguments.get("crosser");
		Integer crossed = (Integer) arguments.get("crossed");
		String subindicator_name = crosser + " day sma crosses above " + crossed + "  day sma";
		return subindicator_name;
	}

	/****
	 * @Override public void set_complex(HashMap<String, Object> arguments) { //
	 *           TODO Auto-generated method stub
	 * 
	 *           Integer day_id = (Integer) arguments.get("day_id"); Entity e =
	 *           (Entity) arguments.get("entity"); String
	 *           indicator_cum_period_name = (String)
	 *           arguments.get("indicator_cum_period_name"); String
	 *           indicator_name = (String) arguments.get("indicator_name");
	 *           String indicator_id =
	 *           CentralSingleton.stub_hashmap.get(indicator_cum_period_name).
	 *           getId(); String subindicator_name =
	 *           get_subindicator_name(arguments); String subindicator_id =
	 *           get_subindicator_id(arguments); String entity_id =
	 *           e.getEntity_es_id(); String complex_id = entity_id +
	 *           indicator_id + subindicator_id; Complex c = new
	 *           Complex(indicator_cum_period_name, complex_id, subindicator_id,
	 *           entity_id, day_id, subindicator_name); if
	 *           (getResults().get(day_id) == null) { getResults().put(day_id,
	 *           new ArrayList<Complex>()); } getResults().get(day_id).add(c); }
	 ***/

	/*****
	 * 
	 * 1. the basic problem is that
	 * 
	 */

	@Override
	public void calculate(Entity e) throws Exception {
		setSma_map(new HashMap<String, HashMap<Integer, TreeMap<Integer, BigDecimal>>>());

		/***
		 * iterate each indicator
		 * 
		 * 
		 * 
		 */

		for (String indicator_name : e.getEntity_initial_stub_names().keySet()) {

			ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

			/****
			 * iterate each applicable indicator cum period name.
			 * 
			 * 
			 */
			for (final String indicator_cum_period_name : indicator_cum_period_names) {

				/****
				 * 
				 * 
				 * create an indicator object.
				 * 
				 */
				Indicator o = new Indicator();

				o.setIndicator_and_period_name(indicator_cum_period_name);

				/****
				 * get a treemap of the type
				 * 
				 * key -> day id value -> datapointIndicator
				 */
				TreeMap<Integer, dataPointIndicator> indicator_values = o.get_indicator_values(e);

				/***
				 * convert this treemap to a closepriceindicator by making a
				 * timeseries out of it, and then populating the close price in
				 * the timeseries with the value of the indicator.
				 * 
				 */
				ClosePriceIndicator indicator_as_close_price = get_close_price_indicator_from_given_indicator(
						indicator_values);

				/***
				 * check if the sma map has the indicator_cum_period_name and if
				 * not then add it.
				 * 
				 */
				if (!sma_map_has_indicator_period_name(indicator_cum_period_name)) {
					getSma_map().put(indicator_cum_period_name, new HashMap<Integer, TreeMap<Integer, BigDecimal>>());

				}

				/*****
				 * create a map to hold the sma indicator for each period
				 * defined in the custom_periods_map for this
				 * indicator_cum_period_name
				 * 
				 * 
				 */
				TreeMap<Integer, SMAIndicator> sma_indicators_map = new TreeMap<Integer, SMAIndicator>();

				/******
				 * populate the above map with an sma indicator built for each
				 * of the periods in the custom periods map.
				 * 
				 * 
				 */
				for (Integer period : custom_periods_map.keySet()) {
					SMAIndicator sma = new SMAIndicator(indicator_as_close_price, period);
					sma_indicators_map.put(period, sma);
				}

				/****
				 * 
				 * create a treemap to hold comparisons. this map is structured
				 * as follows:
				 * 
				 * key -> period (one of the custom periods above) value ->
				 * TreeMap<another_period(one of the custom periods above),
				 * comparison result>
				 * 
				 * the comparison result can be either 1 or -1.
				 * 
				 * The way this comparison map is populated and used is as
				 * follows:
				 * 
				 * 
				 * 
				 */
				TreeMap<Integer, TreeMap<Integer, Integer>> comparison_map = new TreeMap<Integer, TreeMap<Integer, Integer>>();

				// now iterate each day id.
				Integer time_series_index = 0;
				for (Integer day_id : e.getPrice_map_internal().keySet()) {
					// if(day_id <= 20){
					// System.out.println("day id is:" + day_id);
					// }
					if (e.getPrice_map_internal().get(day_id).getIndicator_values_on_datapoint()
							.containsKey(indicator_cum_period_name)) {
						// System.out.println("indicator has value for
						// it.");
						/****
						 * for each day_id in the price_map_internal =>
						 * 
						 * If the main_sma map does not have the day id for this
						 * indicator cum period_name then add it.
						 * 
						 * 
						 */

						if (!sma_map_has_day_id(indicator_cum_period_name, day_id)) {
							getSma_map().get(indicator_cum_period_name).put(day_id, new TreeMap<Integer, BigDecimal>());
						}

						/****
						 * 
						 * the sma is calculated by deverdelhan , just like for
						 * any other indicator.
						 * 
						 * if you neeed a period of 10, it will take last 10
						 * datapoints in teh timeseries
						 * 
						 * the thing is that even if you have only 7 datapoints
						 * then it will calculate using 7 datapoints.
						 * 
						 * in calculate indicators an indicator is calculated at
						 * a day id if numerical days from beggining of price
						 * map are equal to the maximum period needed for
						 * datapoint calculation.
						 * 
						 * 
						 * here also we consider an sma as calculable for
						 * inclusion in the sma_naviagable map using same
						 * function as calculate indicator.
						 * 
						 * it means however that only those many numerical days
						 * will have elapsed, and not those many datapoints
						 * 
						 * this will produce some weird sma readings at the
						 * beginning of the timesereis
						 * 
						 * like at dayid 5 suppose only 3 datapoints are
						 * actually there the sma will be calculated using a
						 * divisor of 3.
						 * 
						 */
						Integer current_period = e.period_for_indicator_calculation(day_id);

						// current period has to be the total number of
						// datapoints
						// available before.

						/*****
						 * 
						 * now we want to get those sma_indicators which can be
						 * thus computed.
						 * 
						 */
						NavigableMap<Integer, SMAIndicator> sma_navigable = sma_indicators_map.headMap(current_period,
								true);
						/****
						 * 
						 * for each of the periods in the sma_navigable map
						 * above,
						 * 
						 * populate the sma_map with it. use the same logic of
						 * creating bigdecimals as in datapoint.
						 * 
						 */
						for (Integer period_n : sma_navigable.keySet()) {

							BigDecimal bd = decimal_to_bigDecimal(
									sma_navigable.get(period_n).getValue(time_series_index));
							getSma_map().get(indicator_cum_period_name).get(day_id).put(period_n, bd);

						}

						/***
						 * so upto the above point we just populated the sma_map
						 * with the relevant sma values.
						 * 
						 * 
						 * now from hereon perform the comparison of this days
						 * sma's(for each period) versus the previous day's
						 * sma's
						 */

						for (Integer period_n : sma_navigable.keySet()) {

							/***
							 * if the comparison map doesnt have this period,
							 * then add it.
							 * 
							 */
							if (!comparison_map.containsKey(period_n)) {
								comparison_map.put(period_n, new TreeMap<Integer, Integer>());
							}

							/***
							 * now iterate every other period. as long as its
							 * not the current period.
							 */
							for (Integer period_other : sma_navigable.keySet()) {

								if (period_n != period_other) {

									/***
									 * compare the smaverage for the two periods
									 * for the current day id.
									 */
									Integer current_comparison = get_comparison(indicator_cum_period_name, day_id,
											period_n, period_other);
											/***
											 * if(day_id >= 8 && day_id <= 15 &&
											 * period_n == 3 && period_other ==
											 * 7){ System.out.println(
											 * "on day id:" + day_id +
											 * " and comparison :" +
											 * current_comparison); }
											 ****/

									// System.out.println("day id
									// is:" + day_id + " period n
									// is: " + period_n +
									// " period other is:" +
									// period_other +
									// " current comparison is: " +
									// current_comparison);

									/****
									 * 
									 * check if the comparison map contains a
									 * comparison for the outer iteratee for the
									 * inner iteratee. ie. period_n =>
									 * period_other
									 */
									if (!comparison_map.get(period_n).containsKey(period_other)) {

										/****
										 * 
										 * add the current comparison as long as
										 * its not null or 0. why?
										 * 
										 * null -> means sma could not be
										 * calculated for the greater or both
										 * periods, so no point in adding
										 * 
										 * 0 -> the comparison should either
										 * hold 1 or -1 assume the following
										 * situation:
										 * 
										 * day -> comparison 0 => 1 1 => 0 2 =>
										 * 0 3 => -1
										 * 
										 * in the above case, there is no point
										 * in adding the zeros, bcecause on the
										 * day 3 , when the comparison map is
										 * consulted, it will find the existing
										 * comparison to be 0 so there is no way
										 * to know if there has been a cross or
										 * not instead comparisons are only
										 * added if they are either 1 or -1 in
										 * the above scenario, thus there will
										 * be just 1 from the day 0 stored in
										 * the comparison map enabling a cross
										 * to be detected at 3.
										 */
										if (current_comparison != null && current_comparison != 0) {
											comparison_map.get(period_n).put(period_other, current_comparison);
										}

									} else {

										/***
										 * get the existing comparison i.e
										 * comparison for two sma's in the
										 * comparison_map. this cannot be null,
										 * because the if condition above has
										 * evaluated that the period_n has a
										 * comparison existing for period_other.
										 */
										Integer existing_comparison = comparison_map.get(period_n).get(period_other);

										/***
										 * 
										 * 
										 * here to as long as
										 * existing_comparison not null or zero,
										 * then proceed.
										 * 
										 */
										if (current_comparison != null && current_comparison != 0) {

											/****
											 * 
											 * only add to the comparison map if
											 * the comparison is not equal to
											 * the existing comparison.
											 * 
											 */
											if (current_comparison != existing_comparison) {

												comparison_map.get(period_n).put(period_other, current_comparison);

												Integer crosser = null;
												Integer crossed = null;

												/***
												 * 
												 * why consider only if the
												 * current comparison is 1?
												 * 
												 * why not -1.
												 * 
												 * Reason:
												 * 
												 * suppose you are processing
												 * period 5. now suppose 5
												 * crosses 9.
												 * 
												 * then its comparison will be 1
												 * 
												 * , but then later on when
												 * processing 9 : it will be
												 * crossed by 5, there the
												 * comparison will be -1
												 * 
												 * so it will create the
												 * subindicator twice.
												 * 
												 * To avoid that, we only
												 * process where comparison is
												 * 1.
												 * 
												 * 
												 */
												if (current_comparison == 1) {
													crosser = period_n;
													crossed = period_other;
													/***
													 * 
													 * only process for those
													 * day ids where the greater
													 * than or equal to the
													 * newest added day id.
													 * 
													 */
													if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {
														HashMap<String, Object> args = new HashMap<String, Object>();
														args.put("crosser", crosser);
														args.put("crossed", crossed);
														args.put("day_id", day_id);
														args.put("indicator_cum_period_name",
																indicator_cum_period_name);
														args.put("entity", e);

														set_complex(args);
													}
												}

												/***
												 * 
												 * add cross complex.
												 * 
												 */

												/**
												 * if(day_id == 15 && period_n
												 * == 3 && period_other == 7){
												 * System.out.println(
												 * "INNER on day id:" + day_id +
												 * " and comparisons are different"
												 * );
												 * System.out.println("crosser:"
												 * + crosser);
												 * System.out.println("crossed:"
												 * + crossed); }
												 ***/

											}

										}
									}
								}
							}
						}
						time_series_index++;
					} else {
						// System.out.println("the indicator is not present for
						// day id:");
						// System.out.println(day_id);
						// System.exit(1);
					}
				}

			}
		}

	}

	public Boolean sma_map_has_indicator_period_name(String indicator_cum_period_name) {
		return getSma_map().containsKey(indicator_cum_period_name);
	}

	public Boolean sma_map_has_day_id(String indicator_cum_period_name, Integer day_id) {
		return sma_map_has_indicator_period_name(indicator_cum_period_name)
				&& getSma_map().get(indicator_cum_period_name).containsKey(day_id);
	}

	/***
	 * compares the sma for the two periods on the day_id.
	 *
	 * for eg: we populated the sma map with the sma indicators for each period
	 * for each day id.
	 * 
	 * then we want to see if a particular sma is greater than that of another
	 * period for a day id.
	 * 
	 * this function returns the result.
	 *
	 * @param indicator_cum_period_name
	 * @param day_id
	 * @param period_n
	 * @param period_other
	 * @return : Integer if both periods are present on the sma_map, and null
	 *         otherwise.
	 */
	public Integer get_comparison(String indicator_cum_period_name, Integer day_id, Integer period_n,
			Integer period_other) {
		if (getSma_map().get(indicator_cum_period_name).get(day_id).containsKey(period_n)
				&& getSma_map().get(indicator_cum_period_name).get(day_id).containsKey(period_other)) {
			return getSma_map().get(indicator_cum_period_name).get(day_id).get(period_n)
					.compareTo(getSma_map().get(indicator_cum_period_name).get(day_id).get(period_other));
		} else {
			return null;
		}
	}

	/***
	 * this was built for the test purposes.
	 * now we have to experiment. with different crossers and crossed
	 * and see that they all work out.
	 * 
	 * @return
	 */
	public static HashMap<String, String> get_subindicator_names_to_ids() {
		HashMap<String, String> subindicator_names_to_ids = new HashMap<String, String>();

		Integer subindicator_id_counter = 0;

		for (Entry<Integer, String> period_entry : custom_periods_map.entrySet()) {

			Integer crosser_period = period_entry.getKey();
			String crosser_period_name = period_entry.getValue();
			
			for (Entry<Integer, String> other_period_entry : 
				custom_periods_map.entrySet()) {
				
				Integer crossed_period = other_period_entry.getKey();
				String crossed_period_name = other_period_entry.getValue();
				
				if(!crosser_period.equals(crossed_period)){
					subindicator_names_to_ids.put(crosser_period + " day sma crosses above " + crossed_period + " day sma",
							subindicator_prefix_map.get("SmaCross") + subindicator_id_counter.toString());
					subindicator_id_counter++;
				}
			}

			
		}
		
		
		System.out.println(subindicator_names_to_ids);
	
		System.out.println("------------------------");
		
		return subindicator_names_to_ids;
	}

}
