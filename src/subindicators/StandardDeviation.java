package subindicators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import DayIdMapGenerator.DayIdBuilder;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import IdAndShardResolver.IdAndShardResolver;
import Indicators.Indicator;
import Indicators.TickMapIndicator;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.statistics.StandardDeviationIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class StandardDeviation extends SubIndicatorCalculation {

	public static final Integer only_consider_periods_greater_than = 2;

	public static final ArrayList<Integer> custom_periods = new ArrayList<Integer>(
			IdAndShardResolver.periods_map.tailMap(only_consider_periods_greater_than, false).keySet());

	public static final TreeMap<Integer, String> custom_periods_map = new TreeMap<Integer, String>() {
		{
			for (Integer period : custom_periods) {
				put(period, IdAndShardResolver.periods_map.get(period));
			}
		}
	};

	public static final TreeMap<Integer, String> standard_deviations =

	new TreeMap<Integer, String>() {
		{

			put(100, "10 percent of standard deviation");

			put(200, "20 percent of standard deviation");

			put(300, "30 percent of standard deviation");

			put(400, "40 percent of standard deviation");

			put(500, "50 percent of standard deviation");

			put(600, "60 percent of standard deviation");

			put(700, "70 percent of standard deviation");

			put(800, "80 percent of standard deviation");

			put(900, "90 percent of standard deviation");

			put(1000, "1 standard deviation");

			put(1250, "1.25x standard deviation");

			put(1500, "1.5x standard deviation");

			put(1750, "1.75x standard deviation");

			put(2000, "2.0x standard deviation");

			put(2500, "2.5x standard deviation");

			put(3000, "3.0x standard deviation");

			put(4000, "4.0x standard deviation");

			put(5000, "5.0x standard deviation");

			put(6000, "6.0x standard deviation");

		}
	};

	/****
	 * 
	 * so there are a total of 19 standard deviations. it can either rise or
	 * fall.
	 * 
	 * 
	 * first see how many standard_deviation entries in the map are less than
	 * this one.
	 * 
	 * then see if its rise or fall.
	 * 
	 * if its rise add nothing.
	 * 
	 * if its fall add (19), basically all the rise subindicators(0 + 18), since
	 * we start from zero, so just add 19. its actually like for rise: s0 , s1,
	 * s2.... s18 -> total 19.
	 * 
	 * so fall will start from s19...
	 * 
	 * this indicator just talks about itself.
	 * it is the difference of the indicator from its 300 day mean on any given day
	 * as compared to the standard deviation.
	 * 
	 * suppose you want to incorporate periods into it, 
	 * then you have to consider the n day changes
	 * 
	 * eg 4 day change
	 * 2 day change
	 * 1 day change => x
	 * 
	 * buy apple when rsi indicator falls and 
	 * rsi indicator rises by 20% of standard deviation in 20 days
	 * we have rises,
	 * but not about standard deviation.
	 * rises 
	 * 
	 * for each period we can have all these changes in standard devaition.
	 * 
	 * the process is just 
	 * 1. create a series with the n day price change, i.e change of price in 4 days
	 * 2. then using that as the close price, do the rest of the steps the same
	 * 3. how to create n day price change without screwing up
	 * 4. how to create 
	 * 5.
	 * 
	 * earlier it was this, then i changed in favor of this.
	 * 4 day change.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub

		Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) arguments
				.get("standard_deviation_amount_and_name");

		Integer day_id = (Integer) arguments.get("day_id");

		Integer rises_or_falls = (Integer) arguments.get("rises_or_falls");

		/***
		 * what is the position of this standard deviation in the map
		 * 
		 */
		Integer std_dev_position = standard_deviations.headMap(nearest_standard_deviation.getKey()).size();

		/***
		 * 
		 * if its rise then it remains this position. if its fall then add
		 * 19(total standard deviations considered) to the id.
		 * 
		 * 
		 */
		std_dev_position = rises_or_falls == -1 ? std_dev_position + standard_deviations.size() : std_dev_position;

		/***
		 * 
		 * now simply add the
		 * 
		 */

		return subindicator_prefix_map.get("StandardDeviation")
				+ String.valueOf(std_dev_position);
	}

	/****
	 * @Override public void set_complex(HashMap<String, Object> arguments) {
	 * 
	 * 
	 *           Integer day_id = (Integer) arguments.get("day_id"); Entity e =
	 *           (Entity) arguments.get("entity"); String
	 *           indicator_cum_period_name = (String)
	 *           arguments.get("indicator_cum_period_name"); String
	 *           indicator_name = (String) arguments.get("indicator_name");
	 *           String indicator_id =
	 *           CentralSingleton.stub_hashmap.get(indicator_cum_period_name).
	 *           getId(); String subindicator_name =
	 *           get_subindicator_name(arguments); String subindicator_id =
	 *           get_subindicator_id(arguments); String entity_id =
	 *           e.getEntity_es_id(); String complex_id = entity_id +
	 *           indicator_id + subindicator_id; Complex c = new
	 *           Complex(indicator_cum_period_name, complex_id, subindicator_id,
	 *           entity_id, day_id, subindicator_name); if
	 *           (getResults().get(day_id) == null) { getResults().put(day_id,
	 *           new ArrayList<Complex>()); } getResults().get(day_id).add(c);
	 * 
	 *           }
	 ****/

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Entry<Integer, String> nearest_standard_deviation = (Entry<Integer, String>) arguments
				.get("standard_deviation_amount_and_name");

		Integer day_id = (Integer) arguments.get("day_id");

		Integer rises_or_falls = (Integer) arguments.get("rises_or_falls");

		String standard_deviation = nearest_standard_deviation.getValue();

		String subindicator_name = "";

		if (rises_or_falls == 1) {
			subindicator_name = "rises by " + standard_deviation;
		} else if (rises_or_falls == -1) {
			subindicator_name = "falls by " + standard_deviation;
		}
		
		
		return subindicator_name;
	}

	@Override
	public void calculate(Entity e) throws Exception {
		/***
		 * iterate each indicator
		 * 
		 * 
		 * 
		 */

		for (final String indicator_name : e.getEntity_initial_stub_names().keySet()) {

			ArrayList<String> indicator_cum_period_names = e.getEntity_initial_stub_names().get(indicator_name);

			/****
			 * iterate each applicable indicator cum period name.
			 * 
			 * 
			 */
			for (final String indicator_cum_period_name : indicator_cum_period_names) {
				System.out.println("this is the indicator cum period name");
				System.out.println(indicator_cum_period_name);
				/****
				 * 
				 * 
				 * create an indicator object.
				 * 
				 */
				Indicator o = new Indicator();

				o.setIndicator_and_period_name(indicator_cum_period_name);

				/****
				 * get a treemap of the type
				 * 
				 * if today its 89 and yesterday it was 77
				 * then actually we have calculated 
				 * it is the mean price 
				 * 
				 * key -> day id 
				 * value -> datapointIndicator
				 */
				TreeMap<Integer, dataPointIndicator> indicator_values = 
						o.get_indicator_values(e);

				/***
				 * convert this treemap to a closepriceindicator by making a
				 * timeseries out of it, and then populating the close price in
				 * the timeseries with the value of the indicator.
				 * 
				 */
				ClosePriceIndicator indicator_as_close_price = get_close_price_indicator_from_given_indicator(
						indicator_values);

				// System.out.println("first close price is day id is:");
				// System.out.println(CentralSingleton.getInstance().gmt_datestring_to_day_id
				// .get(DayIdBuilder.zonedatetime_to_datestring(
				// indicator_as_close_price.getTimeSeries().getFirstTick().getEndTime()))

				// );

				/***
				 * now convert this close price indicator to a standard
				 * deviation indicator.
				 * 
				 * 
				 */

				StandardDeviationIndicator stddev_indicator = 
						new StandardDeviationIndicator(indicator_as_close_price,
						StockConstants.total_pairs_for_any_given_days);

				TickMapIndicator stddev_tickmap = new TickMapIndicator(stddev_indicator);

				/***
				 * 
				 * convert the close price indicator to a sma indicator using
				 * the last 300 days as a period. this is basically a mean
				 * price.
				 * 
				 * 
				 */
				SMAIndicator sma = new SMAIndicator(indicator_as_close_price,
						StockConstants.total_pairs_for_any_given_days);

				TickMapIndicator sma_tickmap = new TickMapIndicator(sma);

				/***
				 * 
				 * now create the new modified indicators here, using those as
				 * constructors.
				 * 
				 */

				/****
				 * 
				 * now we iterate each day id from the first newest downloaded
				 * day id.
				 * 
				 * 
				 */

				for (final Integer day_id : e.getPrice_map_internal().keySet()) {
					if (day_id >= e.generate_newly_downloaded_datapoints().firstKey()) {

						/****
						 * 
						 * get the indicator value on this day id.
						 * 
						 * 
						 */
						/// check if the indicator value even exists.
						if (indicator_values.get(day_id) != null) {
							final BigDecimal indicator_value = indicator_values.get(day_id)
									.getIndicator_big_decimal_value();

							/***
							 * 
							 * now get mean value for this day.
							 * 
							 */

							// the rite way to do this is to get the day id of
							// the first tick.
							// then to get how many day ids are there in between
							// ticks.
							// given a day id.
							// get it from the timeseries.
							// pass in the indicator.

							// System.out.println("day id is:" + day_id);
							// System.out.println("sma tickmap first key:" +
							// sma_tickmap.firstKey());
							//Decimal mean_value_decimal = sma
							//		.getValue(sma_tickmap.getTick_to_timeseries_index_map().get(day_id));

							final BigDecimal mean_value = decimal_to_bigDecimal(
									sma.getValue(sma_tickmap.getTick_to_timeseries_index_map().get(day_id)));

							/***
							 * 
							 * now get the standard deviation at this day
							 * 
							 * 
							 */
							final BigDecimal std_dev = decimal_to_bigDecimal(stddev_indicator
											.getValue(stddev_tickmap.getTick_to_timeseries_index_map().get(day_id)));

							/***
							 * 
							 * now subtract current from mean.
							 * 
							 * 
							 */
							final BigDecimal difference = indicator_value.subtract(mean_value);

							// System.out.println("day_id:" + day_id);
							// System.out.println("indicator value:" +
							// indicator_value);
							// System.out.println("std dev:" +
							// std_dev.toPlainString());
							// System.out.println("sma:" +
							// mean_value.toString());

							// what to do if the standard deviation is 0
							// in that case we cannot do anything.
							if (std_dev.compareTo(new BigDecimal("0.00")) != 0) {

								/***
								 * 
								 * now express as a ratio of the standard
								 * deviation.
								 * 
								 * 19.1231/120.212 => 19123
								 * 
								 * 0.1 * 100
								 * 
								 */
								BigDecimal ratio_as_percentage = difference
										.divide(std_dev, StockConstants.DataPoint_bd_scale,
												StockConstants.DataPoint_bd_rounding_mode)
										.multiply(new BigDecimal(StockConstants.DataPoint_multiplicand_for_deriving_percentage_correlate));
								
								
								/***
								 * 
								 * now need to convert this ratio to the
								 * necessary nearest standard deviation.
								 * 
								 * 
								 * c
								 */

								Integer ratio_as_integer = ratio_as_percentage.toBigInteger().intValueExact();
								/***
								 * 
								 * 
								 * printing out some values, for test purposes.
								 * 
								 */
								if (indicator_name.equals("close") && day_id > 500 && day_id < 510) {
									HashMap<String, Object> args_for_print = new HashMap<String, Object>();

									args_for_print.put("day_id", day_id);
									args_for_print.put("indicator", indicator_name);
									args_for_print.put("mean", mean_value);
									args_for_print.put("std_dev", std_dev);
									args_for_print.put("difference", difference);
									args_for_print.put("ratio", ratio_as_integer);
									args_for_print.put("indicator_value", indicator_value);
									
									
									
									print_info(args_for_print);
									
								}

								/***
								 * 
								 * now use find_nearest to find the nearest
								 * standard deviation.
								 * 
								 * 
								 */

								Entry<Integer, String> nearest_standard_deviation = find_nearest(Math.abs(ratio_as_integer),
										standard_deviations);

								/***
								 * now pass it into the get_subindicator_name
								 * and get_subindicator_id functions
								 * respectively.
								 * 
								 */
								HashMap<String, Object> arguments = new HashMap<String, Object>();
								arguments.put("standard_deviation_amount_and_name", nearest_standard_deviation);
								arguments.put("day_id", day_id);
								arguments.put("rises_or_falls", ratio_as_percentage.compareTo(new BigDecimal("0.00")));
								arguments.put("indicator_cum_period_name", indicator_cum_period_name);
								arguments.put("entity", e);
								arguments.put("indicator_name", indicator_name);
								set_complex(arguments);
							}
						}

					}

				}

			}
		}

	}

	/***
	 * 
	 * given a set of arguments as a hashmap will print them out as key : value
	 * .
	 * 
	 * @param arguments
	 */
	public void print_info(HashMap<String, Object> arguments) {
		System.out.println("----------------");
		for (String s : arguments.keySet()) {
			System.out.println(s + ":" + arguments.get(s));
		}
	}
	
	public static HashMap<String,String> subindicator_names_to_ids(){
		HashMap<String,String> subindicator_names_to_ids =
				new HashMap<String,String>();
		// for each standard deviation
		// generate the name and the id.
		Integer subindicator_id = 0;
		ArrayList<Integer> rises_or_falls = new ArrayList<Integer>(Arrays.asList(1,-1));
		for(Integer rf : rises_or_falls){
			for(Map.Entry<Integer, String> entry : standard_deviations.entrySet()){
				Integer standard_deviation_amt = entry.getKey();
				String standard_deviation_lit = entry.getValue();
				
				String standard_deviation_name = "";
				
				if(rf.equals(1)){
					standard_deviation_name = "rises by " + entry.getValue();
				}
				else{
					standard_deviation_name = "falls by " + entry.getValue();
				}
				
				subindicator_names_to_ids.put(standard_deviation_name, (subindicator_prefix_map.get("StandardDeviation") + subindicator_id.toString()));
				subindicator_id++;
			}
		}
		System.out.println(subindicator_names_to_ids);
		return subindicator_names_to_ids;
	}

}
