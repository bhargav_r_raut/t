package subindicators;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tinkerpop.gremlin.Tokens.T;

import DayIdMapGenerator.DayIdBuilder;
import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.Entity;
import ExchangeBuilder.dataPointIndicator;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.CachedIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import yahoo_finance_historical_scraper.StockConstants;

public class SubIndicatorCalculation implements ISubIndicatorCalculation {
	
	public static final HashMap<String,String> subindicator_prefix_map = 
			new HashMap<String,String>(){{
				put("NDayHighLow","sa");
				put("Pattern","sb");
				put("RiseFallAmount","sc");
				put("SmaCross","sd");
				put("StandardDeviation","se");
				put("TimeBasedSubindicators","sf");
				put("ForTheMonthWeekYearQuarter","sg");
				put("IndicatorValueChangeStandardDeviation","si");
			}};
	
	public HashMap<Integer, List<Complex>> results;

	public HashMap<Integer, List<Complex>> getResults() {
		return results;
	}

	public void setResults(HashMap<Integer, List<Complex>> results) {
		this.results = results;
	}
	
	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void set_complex(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		Integer day_id = (Integer) arguments.get("day_id");
		Entity e = (Entity) arguments.get("entity");
		String indicator_cum_period_name = (String) arguments.get("indicator_cum_period_name");
		String indicator_name = (String) arguments.get("indicator_name");
		//System.out.println("indicator cum period name is:" + indicator_cum_period_name);
		//System.out.println(CentralSingleton.stub_hashmap);
		//System.out.println(CentralSingleton.stub_hashmap.get(indicator_cum_period_name));
		
		String indicator_id = CentralSingleton.stub_hashmap.get(indicator_cum_period_name).getId();
		String subindicator_name = get_subindicator_name(arguments);
		String subindicator_id = get_subindicator_id(arguments);
		String entity_id = e.getEntity_es_id();
		String complex_id = entity_id + indicator_id + subindicator_id;
		Complex c = new Complex(indicator_cum_period_name, complex_id, subindicator_id, entity_id, day_id,
				subindicator_name);
		if (getResults().get(day_id) == null) {
			getResults().put(day_id, new ArrayList<Complex>());
		}
		getResults().get(day_id).add(c);
	}
	
	@Override
	public void calculate(Entity e) throws Exception {
		// TODO Auto-generated method stub

	}
	
	public static Map.Entry<Integer, String> find_nearest(Integer input, TreeMap<Integer, String> compare_map) {
		//System.out.println("input is:" + input);
		
		Integer higher_key = compare_map.higherKey(input);
		Integer lower_key = compare_map.lowerKey(input);
		if(compare_map.containsKey(input)){
			// all keys less than input + 1, then the last entry, which will be the 
			// input itself.
			return compare_map.headMap(input + 1, false).lastEntry();
		}
		if (higher_key == null) {
			return compare_map.lastEntry();
		} else if (lower_key == null) {
			return compare_map.firstEntry();
		} else {
			// closer to lower key
			if (higher_key - input > lower_key - input) {
				// all keys less than input, and then the last entry(so we get
				// the lower key)
				return compare_map.headMap(input, false).lastEntry();
			}
			// closer to higher key or equal -> will consider higher key.
			else if (higher_key - input <= lower_key - input) {
				// all keys more than input, and then the first entry.
				return compare_map.tailMap(input, false).firstEntry();
			} else {
				return null;
			}
		}
	}
	
	public SubIndicatorCalculation(){	
		setResults(new HashMap<Integer,List<Complex>>());
	}

	
	public static Boolean GREATER_THAN(BigDecimal a, BigDecimal b) throws Exception {
		// TODO Auto-generated method stub
		return a.compareTo(b) >= 0;
	}

	/***
	 * issue arises out of the following requirement:
	 * 
	 * given an indicator_cum_period_name
	 * 
	 * we want to get the SMA values(for all the periods in custom_periods_map)
	 * for each day id for this indicator.
	 * 
	 * so what we do is, we create a new timeseries, with the close price as the
	 * indicator value. then return the close price indicator derived from that
	 * timeseries
	 * 
	 * for the open, high, low, volumen => we add NaN as the value for the close
	 * => if the indicator has a value for that day id, then we set it as the
	 * bigdecimal.toplainstring, otherwise that also NaN
	 * 
	 * finally return the ClosePriceIndicator got from the timeseries.
	 * 
	 * add a test to see that sma, and a close price indicator can be calculated
	 * if there are intervening values which are NaN, as there inevitably will
	 * be.
	 * 
	 * 
	 * @param dpi
	 * @return
	 */
	@Override
	public ClosePriceIndicator get_close_price_indicator_from_given_indicator(
			TreeMap<Integer, dataPointIndicator> dpi_tm) {
		ArrayList<Tick> ts_holder = new ArrayList<Tick>();

		// System.out.println("datapoint indicator is:" +
		// dpi_tm.lastEntry().getValue().getIndicator_name());

		// System.out.println("first day id begin added is:" +
		// dpi_tm.firstKey());

		for (Integer day_id : dpi_tm.keySet()) {
			// System.out.println("day id is:" + day_id);

			// System.out.println("index is:" + index);
			Tick tick = new Tick(CentralSingleton.getInstance().gmt_day_id_to_dataPoint.get(day_id).dateString_to_zdt(),
					"NaN", "NaN", "NaN", dpi_tm.get(day_id).getIndicator_big_decimal_value().toString(), "NaN");
			ts_holder.add(tick);
		}

		// System.out.println("total datapoints is:" + dpi_tm.size());

		TimeSeries ts = new TimeSeries(ts_holder);

		// System.out.println("datapoint indicator is:" +
		// dpi_tm.lastEntry().getValue().getIndicator_name());
		// System.out.println("the tick value is at 1913:");
		// System.out.println(ts.getTick(1913));
		// System.out.println("the tick value is:");
		// System.out.println(ts.getTick(1914));

		ClosePriceIndicator cpi = new ClosePriceIndicator(ts);

		return cpi;

	}

	/***
	 * see interface for description.
	 * 
	 */
	@Override
	public BigDecimal decimal_to_bigDecimal(Decimal d) {
		// TODO Auto-generated method stub
		String val  = d.toString();
		BigDecimal bd = new BigDecimal(val).
				setScale(StockConstants.DataPoint_bd_scale,
				StockConstants.DataPoint_bd_rounding_mode);
		return bd;
	}
	

	

}
