package subindicators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;

import ExchangeBuilder.CentralSingleton;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;

/***
 * 
 * 
 * autogenerates the subindicators based on time.
 * 
 * eg: entity falls on monday(first-fifth:month,1-50 of year,1-12
 * quarter)/tue/wed/thu/fri/sat/sun => 7 subindicators eg: entity falls in
 * first,last,january/feb.../dec => plus numerics,12 +12 subindicators eg:
 * entity falls in first,3rd week of month/1,2,3,4,5,last => 5 subindicators eg:
 * entity falls in first,last,2nd quarter of year/1,2,3 => 6 subindicators eg:
 * entity falls on the 23rd of November/1..31 => 31 subindicators
 * 
 * 1)monday...sunday 2)
 * 
 * it doesn't have anything to do with each entity / indicator.
 * 
 * @author bhargav
 *
 */
public class TImeBasedSubindicators extends SubIndicatorCalculation {

	@Override
	public HashMap<Integer, List<Complex>> getResults() {
		// TODO Auto-generated method stub
		return super.getResults();
	}

	@Override
	public void setResults(HashMap<Integer, List<Complex>> results) {
		// TODO Auto-generated method stub
		super.setResults(results);
	}

	@Override
	public String get_subindicator_id(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		return (String) arguments.get("subindicator_id");
	}

	@Override
	public String get_subindicator_name(HashMap<String, Object> arguments) {
		// TODO Auto-generated method stub
		return (String) arguments.get("subindicator_name");
	}

	@Override
	public void set_complex(HashMap<String, Object> arguments) {

		Integer day_id = (Integer) arguments.get("day_id");
		String subindicator_name = get_subindicator_name(arguments);
		String subindicator_id = get_subindicator_id(arguments);

		/***
		 * since it has no applicability to entity or indiceator we just use :
		 * e- and i-
		 * 
		 * 
		 * 
		 */
		String complex_id = "e-" + "i-" + subindicator_id;

		Complex c = new Complex("", complex_id, subindicator_id, "", day_id, subindicator_name);

		// System.out.println("the complex subindicator name is:");
		// System.out.println(c.getSubindicator_name());

		if (!getResults().containsKey(day_id)) {

			getResults().put(day_id, new ArrayList<Complex>(Arrays.asList(c)));
		} else {
			getResults().get(day_id).add(c);
		}

		// System.out.println(getResults().get(day_id).get(0).getSubindicator_name());

	}

	/***
	 * 
	 * this method basically is called only ever once. Don't pass in any entity
	 * because the entity is not used. It just uses the CentralSingleton
	 * day_id_map.
	 * 
	 */
	@Override
	public void calculate(Entity e) throws Exception {
		// TODO Auto-generated method stub
		// super.calculate(e);
		for (Map.Entry<Integer, DataPoint> entry : CentralSingleton.getInstance().gmt_day_id_to_dataPoint.entrySet()) {

			Integer day_id = entry.getKey();
			DataPoint dp = entry.getValue();

			set_weekday_subindicators(day_id, dp);
			set_last_nday_of_month_subindicator(day_id, dp);
			set_last_nday_of_quarter(day_id, dp);
			set_last_nday_of_year(day_id, dp);
			set_month(day_id, dp);
			set_nday_of_quarter(day_id, dp);
			set_nday_of_year(day_id, dp);
			set_quarter(day_id, dp);
			set_weekday_of_month_subindicator(day_id, dp);
			set_year(day_id, dp);

		}

	}

	private static final HashMap<String, Integer> subindicator_id_numbers = new HashMap<String, Integer>() {
		{
			put("Monday", 0);
			put("Tuesday", 1);
			put("Wednesday", 2);
			put("Thursday", 3);
			put("Friday", 4);
			put("Saturday", 5);
			put("Sunday", 6);
		}
	};
	/**
	 * there are actually 82, but we add 1 to that for the day itself. so that
	 * it becomes 83. i.e monday itself is a subindicator, then all the
	 * subindicators based on monday are 82. so we get a total of 83
	 * 
	 */
	private static final Integer subindicators_per_weekday = 83;

	/****
	 * sets the subindicators as follows:
	 * 
	 * Monday(0), Tuesday(1), Wednesday(2), Thursday(3), Friday(4), Saturday(5),
	 * Sunday(6) for each of the above: -first to sixth for month + last(7)
	 * -first to fifteith-fifth for year(56) -first to twelth for quarter(18) --
	 * there will never be 18, just allowing 6 extra so that nothing fucks up
	 * -last for year(1) -last for quarter(1)
	 * 
	 * 
	 * -all the months -all the years that we have -all the quarters eg:
	 * 1st,2nd,3rd
	 * 
	 * so total subindicators here are: 7*(7 + 55 + 18 + 2 + 1(the day itself))
	 * = 575(total) and 83(per day) total per indicator:
	 * 
	 * CALL THIS FUNCTION TO CALL ALL THE OTHERS.
	 * 
	 * @param day_id
	 * @param dp
	 */

	/***
	 * so how is this id assigned. basically the first subindicator is monday
	 * 
	 * 
	 * @param day_id
	 * @param dp
	 */
	private void set_weekday_subindicators(Integer day_id, DataPoint dp) {
		HashMap<String, Object> arguments = new HashMap<String, Object>();
		arguments.put("subindicator_name", dp.getDay_of_week());
		arguments.put("subindicator_id",
				subindicator_prefix_map.get("TimeBasedSubindicators") + (get_weekday_subindicator_id(dp) + subindicator_id_numbers.get(dp.getDay_of_week())));
		arguments.put("day_id", day_id);
		// System.out.println("arguments are:");
		// System.out.println(arguments);
		set_complex(arguments);
	}

	/***
	 * how subindicator is calculated:
	 * 
	 * 83*(weekday index) + nth_day_integer
	 * 
	 * @param day_id
	 * @param dp
	 * 
	 * @return : will return a name like "first...sixth monday", it can be for
	 *         any month.
	 * 
	 * 
	 * 
	 */
	private void set_weekday_of_month_subindicator(Integer day_id, DataPoint dp) {
		/**
		 * returns first,second...sixth
		 */
		String nth_day = dp.getNth_day();
		/**
		 * ref idandshardresolver : 1,2,3,4,5,6
		 * 
		 */
		Integer nth_day_integer = dp.getNth_day_integer();

		HashMap<String, Object> arguments = new HashMap<String, Object>();

		arguments.put("subindicator_name", nth_day + " " + dp.getDay_of_week() + " of the month");
		arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
				+ (get_weekday_subindicator_id(dp)
						+ subindicator_id_numbers.get(dp.getDay_of_week())
						+ nth_day_integer));
		arguments.put("day_id", day_id);
		set_complex(arguments);
	}

	/****
	 * 
	 * how subindicator id is calculated: basically while calculating the
	 * subindicator id for the first to sixth day we have assigned teh ids like
	 * this:
	 * 
	 * for eg for monday monday : 0 first : 1 second : 2 third : 3 fourht : 4
	 * fifth : 5 sixth : 6 last : 7
	 * 
	 * 
	 * so basically last gets an id of 7 for any given day. so basically the
	 * combined subindicator id becomes:
	 * 
	 * get base id i.e id for monday/tuesday/wednesday/thursday/friday/sat/sun:
	 * use function {@link #get_weekday_subindicator_id(DataPoint)}, and then to
	 * that just add 7.
	 * 
	 * 
	 * @param day_id
	 * @param dp
	 * 
	 * @return : will return a name like "last monday", can be of any month.
	 */
	private void set_last_nday_of_month_subindicator(Integer day_id, DataPoint dp) {
		DateTime dt = dp.datestring_to_datetime();
		if (dp.is_last_nday_of_month(dt, dp.getNth_day_integer())) {
			HashMap<String, Object> arguments = new HashMap<String, Object>();
			arguments.put("day_id", day_id);
			arguments.put("subindicator_name", "last" + " " + dp.getDay_of_week() + " of the month");
			arguments.put("subindicator_id",
					subindicator_prefix_map.get("TimeBasedSubindicators") + 
					
					
					(get_weekday_subindicator_id(dp)
							+ subindicator_id_numbers.get(dp.getDay_of_week())
							+  7));
			set_complex(arguments);
		}
	}

	/****
	 * how to calculate the subindicator id:
	 * 
	 * base weekday_id + 7 + (n -> which day of the year.)
	 * 
	 * 
	 * @param day_id
	 * @param dp
	 * 
	 *            how many weeks since start of year. today is day id 221
	 * 
	 *            221/7 = 31 weeks. so 31 weeks have passed -> in that there are
	 *            31 of whatever days we are getting and then plus one if there
	 *            is a modulus.
	 * 
	 *            how subindicator id is calculated:
	 * 
	 *            basic_weekday + + 7(for last) + nth_weekday_of_year
	 * 
	 * 
	 * @return : will return something like 23rd monday of the year.
	 */
	private void set_nday_of_year(Integer day_id, DataPoint dp) {
		DateTime dt = dp.dateString_to_dateTime();
		// Integer day_of_year = dt.getDayOfYear();
		DateTime first_day_of_year = new DateTime(dp.getYear(), 1, 1, 0, 0);
		Integer days_since_beginning_of_year = dt.getDayOfYear() - first_day_of_year.getDayOfYear();

		Integer weeks_since_start_of_year = days_since_beginning_of_year / 7;
		Integer modulus = days_since_beginning_of_year % 7;
		HashMap<String, Object> arguments = new HashMap<String, Object>();

		Integer nth_weekday_of_year = weeks_since_start_of_year;
		if (modulus >= 0) {
			nth_weekday_of_year++;
		}

		arguments.put("day_id", day_id);
		arguments.put("subindicator_name", ordinal(nth_weekday_of_year) + " " + dp.getDay_of_week() + " of the year");
		
		
		
		
		arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
				+ (get_weekday_subindicator_id(dp) + subindicator_id_numbers.get(dp.getDay_of_week())  + 7 + nth_weekday_of_year));
		set_complex(arguments);

	}

	/****
	 * 
	 * 
	 * returns something like: 5th monday of the quarter
	 * 
	 * so first it should go to the beginning of the current quarter.
	 * 
	 * then it should do the same system as for the year.
	 * 
	 * 
	 * subindicator id is calculated as being just after the nday of year.
	 * 
	 * basic_weekday + + 7(for last) + nth_weekday_of_year(56) + nday_of_quarter
	 * 
	 * eg : will return something like: 5th monday of quarter(all quarters
	 * combined.)
	 * 
	 * @param day_id
	 * @param dp
	 */
	private void set_nday_of_quarter(Integer day_id, DataPoint dp) {
		DateTime dt = dp.dateString_to_dateTime();
		Integer days_since_beginning_of_quarter = 0;
		if (dp.getQuarter_of_year_integer().equals(1)) {
			DateTime first_day_of_first_quarter = new DateTime(dt.getYear(), 1, 1, 0, 0);
			days_since_beginning_of_quarter = dt.getDayOfYear() - first_day_of_first_quarter.getDayOfYear();
		} else if (dp.getQuarter_of_year_integer().equals(2)) {
			DateTime first_day_of_second_quarter = new DateTime(dt.getYear(), 4, 1, 0, 0);
			days_since_beginning_of_quarter = dt.getDayOfYear() - first_day_of_second_quarter.getDayOfYear();
		} else if (dp.getQuarter_of_year_integer().equals(3)) {
			DateTime first_day_of_third_quarter = new DateTime(dt.getYear(), 7, 1, 0, 0);
			days_since_beginning_of_quarter = dt.getDayOfYear() - first_day_of_third_quarter.getDayOfYear();
		} else {
			DateTime first_day_of_fourth_quarter = new DateTime(dt.getYear(), 10, 1, 0, 0);
			days_since_beginning_of_quarter = dt.getDayOfYear() - first_day_of_fourth_quarter.getDayOfYear();
		}

		Integer weeks_since_start_of_quarter = days_since_beginning_of_quarter / 7;

		Integer modulus = days_since_beginning_of_quarter % 7;
		HashMap<String, Object> arguments = new HashMap<String, Object>();

		Integer nth_weekday_of_quarter = weeks_since_start_of_quarter;
		if (modulus >= 0) {
			nth_weekday_of_quarter++;
		}

		arguments.put("day_id", day_id);
		arguments.put("subindicator_name",
				ordinal(nth_weekday_of_quarter) + " " + dp.getDay_of_week() + " of the quarter");
		arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
				+ (get_weekday_subindicator_id(dp) + subindicator_id_numbers.get(dp.getDay_of_week()) + 7 + 56 + nth_weekday_of_quarter));
		set_complex(arguments);
	}

	/****
	 * 
	 * how is the subindicator id assigned?
	 * 
	 * weekday_subindicator_id + 7 + 56 + 18 +
	 * subindicator_id_numbers.get(dp.getDay_of_week()) + 1 so how does this
	 * work?
	 * 
	 * we will have one "last" of year for every weekday. so basically uptil 18
	 * -> its for {@link #set_nday_of_quarter(Integer, DataPoint)}
	 * 
	 * after that suppose we are on a wednesday, and it comes out to be the last
	 * one of the year
	 * 
	 * so leave monday and tuesday : then from subindicator id numbers get the
	 * id of wednesday (2) now add one to that so its 3
	 * 
	 * basically monday -> 1 tuesday -> 2 wednesday -> 3
	 * 
	 * @return : something like : "last monday of year"
	 * @param day_id
	 * @param dp
	 */
	private void set_last_nday_of_year(Integer day_id, DataPoint dp) {

		// so how do you do this?
		// we need to go to the end of the year
		// take this days
		DateTime dt = dp.dateString_to_dateTime();
		Integer current_day_of_year = dt.getDayOfYear();
		DateTime end_of_year = new DateTime(dt.getYear(), 12, 31, 0, 0);
		Integer day_of_end_of_year = end_of_year.getDayOfYear();
		Integer difference = day_of_end_of_year - current_day_of_year;

		// if the difference is less than 7, then this is the last day of the
		// year of
		// this day type.

		if (difference < 7) {
			HashMap<String, Object> arguments = new HashMap<String, Object>();
			arguments.put("day_id", day_id);
			arguments.put("subindicator_name", "last " + dp.getDay_of_week() + " of the year");
			//if(dp.getDay_of_week().equals("Sunday")){
			//System.out.println("calculating for last sunday of year");
			
			//System.out.println("dp datestring is:" + dp.getDateString());
			//System.out.println("day id is:" + dp.getDay_of_week_integer());
			///System.out.println("day of week in words: " + dp.getDay_of_week());
			//System.out.println("weekday subindicator id is");
			
			//System.out.println(get_weekday_subindicator_id(dp));
			
			
			arguments.put("subindicator_id",
					subindicator_prefix_map.get("TimeBasedSubindicators") +
					(get_weekday_subindicator_id(dp) + subindicator_id_numbers.get(dp.getDay_of_week()) + 7 + 56
							+ 18  + 1));
			//System.out.println("subindicator id works out to:" + (get_weekday_subindicator_id(dp) + 7 + 56
			//				+ 18  + 1));
			//System.exit(1);
			//}
			
			set_complex(arguments);
		}

	}

	/***
	 * subindicator id calculation : same as for last_of_year, just added again
	 * like that. refer last_of_year, here we add 7 more for the last of year
	 * ones, and then do the same thing as the last of year.
	 * 
	 * @return: returns something like : "last monday of the quarter"
	 * @param day_id
	 * @param dp
	 */
	private void set_last_nday_of_quarter(Integer day_id, DataPoint dp) {

		DateTime dt = dp.dateString_to_dateTime();
		Integer current_day_of_year = dt.getDayOfYear();
		DateTime end_of_quarter = null;
		Integer quarter_of_year = dp.getQuarter_of_year_integer();
		if (quarter_of_year == 1) {
			end_of_quarter = new DateTime(dp.getYear(), 1, 1, 0, 0);
		} else if (quarter_of_year == 2) {
			end_of_quarter = new DateTime(dp.getYear(), 4, 1, 0, 0);
		} else if (quarter_of_year == 3) {
			end_of_quarter = new DateTime(dp.getYear(), 7, 1, 0, 0);
		} else {
			end_of_quarter = new DateTime(dp.getYear(), 10, 1, 0, 0);
		}
		Integer day_of_end_of_quarter = end_of_quarter.getDayOfYear();
		Integer difference = day_of_end_of_quarter - current_day_of_year;

		// same thing, if the difference is less than 7, then its the last.
		if (difference < 7) {
			HashMap<String, Object> arguments = new HashMap<String, Object>();
			arguments.put("day_id", day_id);
			arguments.put("subindicator_name", "last " + dp.getDay_of_week() + " of the quarter");
			arguments.put("subindicator_id",
					subindicator_prefix_map.get("TimeBasedSubindicators") + 
					(get_weekday_subindicator_id(dp) + subindicator_id_numbers.get(dp.getDay_of_week()) + 7 + 56
							+ 18 + 2));
			set_complex(arguments);
		}
	}

	/****
	 * how subindicator id is calculated
	 * 
	 * get_weekday_subindicator_id(dp) + 7 + 56 + 18 + 7 + 7 + (whatever month
	 * starts with + 1 for jan and goes till +12)
	 * 
	 * 
	 * 
	 * @param day_id
	 * @param dp
	 * @return: returns something like "January"
	 */
	private void set_month(Integer day_id, DataPoint dp) {
		HashMap<String, Object> arguments = new HashMap<String, Object>();
		arguments.put("day_id", day_id);
		arguments.put("subindicator_name", dp.getMonth_of_year());
		arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
				+ (6*subindicators_per_weekday + 6 + 7 + 56 + 18 + 2 + dp.getMonth_of_year_integer()));
		set_complex(arguments);
	}

	/****
	 * how subindicator id is calculated
	 * 
	 * get_weekday_subindicator_id(dp) + 7 + 56 + 18 + 7 + 7 + 12 + (+1 for
	 * first quarter till +4)
	 * 
	 * 
	 * 
	 * @param day_id
	 * @param dp
	 * @return: returns something like "first quarter"
	 */
	private void set_quarter(Integer day_id, DataPoint dp) {
		HashMap<String, Object> arguments = new HashMap<String, Object>();
		arguments.put("day_id", day_id);
		arguments.put("subindicator_name", dp.getQuarter_of_year() + " quarter");
		arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
				+ (6*subindicators_per_weekday + 6 + 7 + 56 + 18 + 2 + 12 + dp.getQuarter_of_year_integer()));
		set_complex(arguments);
	}

	/***
	 * how subindicator is calculated: get_weekday_subindicator_id(dp) + 7 + 56
	 * + 18 + 7 + 7 + 12 + 4 + (+1 for 2005 and upto + 20 for 2025)
	 * 
	 * allow for everything from 2005 -> 2025
	 * 
	 * 
	 * @return: retursn something like "2005"
	 * @param day_id
	 * @param dp
	 */
	private void set_year(Integer day_id, DataPoint dp) {
		if (dp.getYear() >= 2005 && dp.getYear() <= 2025) {
			HashMap<String, Object> arguments = new HashMap<String, Object>();
			arguments.put("day_id", day_id);
			arguments.put("subindicator_name", dp.getYear().toString());
			arguments.put("subindicator_id", subindicator_prefix_map.get("TimeBasedSubindicators")
					+ (6*subindicators_per_weekday + 6 + 7 + 56 + 18 + 2 + 12 + 4 + (dp.getYear() - 2005 + 1)));
			set_complex(arguments);
		}
	}

	/***
	 * 
	 * then there are all the subindicators of monday.(83 + monday itself)
	 * 
	 * just multiply by 84(total subindicators) the day_of_week ids defined
	 * above.
	 * 
	 * @param dp
	 * @return
	 */
	private Integer get_weekday_subindicator_id(DataPoint dp) {
		return subindicator_id_numbers.get(dp.getDay_of_week()) * subindicators_per_weekday;
	}

	/***
	 * 
	 * given any integer, will return its suffix ordinal eg: given 1 will return
	 * "st" and so on
	 * 
	 * @param i
	 * @return
	 */
	public static String ordinal(int i) {
		String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
		switch (i % 100) {
		case 11:
		case 12:
		case 13:
			return i + "th";
		default:
			return i + sufixes[i % 10];

		}
	}

	

	public static HashMap<String,Integer> generate_expected_subindicators() {
		HashMap<String, Integer> subindicator_names_to_ids = new HashMap<String, Integer>();
		TreeMap<Integer,String> id_sorted_map = new TreeMap<Integer,String>();
		
		Integer current_id = 0;
		
		ArrayList<String> weekday_names = 
				new ArrayList<String>(Arrays.asList("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"));
		
		for (String s : weekday_names) {
			
			// weekday is zero.
			
			String weekday = s;
			//System.out.println("now doing weekday:" + weekday);
			if(current_id != 0){
				current_id++;
			}
			// for each weekday -> there are all the things, so go on assigning
			// ids to the.
			// first for this weekday make the id as current_id + weekday_id.
			subindicator_names_to_ids.put(weekday, current_id);			
			subindicator_names_to_ids.put("first " + weekday + " of the month", current_id + 1);
			subindicator_names_to_ids.put("second " + weekday + " of the month", current_id + 2);
			subindicator_names_to_ids.put("third " + weekday + " of the month", current_id + 3);
			subindicator_names_to_ids.put("fourth " + weekday + " of the month", current_id + 4);
			subindicator_names_to_ids.put("fifth " + weekday + " of the month", current_id + 5);
			subindicator_names_to_ids.put("sixth " + weekday + " of the month", current_id + 6);
			subindicator_names_to_ids.put("last " + weekday + " of the month", current_id + 7);
			current_id = current_id + 7;
			

			// after this there are 76
			// then continue.
			// day of year.
			for (int i = 0; i < 56; i++) {
				subindicator_names_to_ids.put(ordinal(i + 1) + " " + weekday + " of the year", current_id +  i + 1);
				
			}
			
			
			current_id = current_id + 56;
			
			
			
			// now of the quarter
			for (int i = 0; i < 18; i++) {
				subindicator_names_to_ids.put(ordinal(i + 1) + " " + weekday + " of the quarter",
						current_id + i + 1);
			}
			
			current_id = current_id + 18;
			
			
			// last of the year
			// what is the id for this that is needed?
			subindicator_names_to_ids.put("last " + weekday + " of the year", current_id + 1);

			// last of the quarter
			// what is the id for this that is needed?
			subindicator_names_to_ids.put("last " + weekday + " of the quarter", current_id + 2);
			
			
			
			current_id = current_id + 2;
			
			System.out.println("current_id is:" + current_id);
			
		}
		
		
		
		
		// 83*7 = 
		// now month
		//current_id = id_sorted_map.lastKey();
		//System.out.println("the last entry is: " + current_id);
		
		
		subindicator_names_to_ids.put("January", current_id + 1);
		subindicator_names_to_ids.put("February", current_id + 2);
		subindicator_names_to_ids.put("March", current_id + 3);
		subindicator_names_to_ids.put("April", current_id + 4);
		subindicator_names_to_ids.put("May", current_id + 5);
		subindicator_names_to_ids.put("June", current_id + 6);
		subindicator_names_to_ids.put("July", current_id + 7);
		subindicator_names_to_ids.put("August", current_id + 8);
		subindicator_names_to_ids.put("September", current_id + 9);
		subindicator_names_to_ids.put("October", current_id + 10);
		subindicator_names_to_ids.put("November", current_id + 11);
		subindicator_names_to_ids.put("December", current_id + 12);

		subindicator_names_to_ids.put("first quarter", current_id + 13);

		subindicator_names_to_ids.put("second quarter", current_id + 14);

		subindicator_names_to_ids.put("third quarter", current_id + 15);

		subindicator_names_to_ids.put("fourth quarter", current_id + 16);

		// year
		Integer year_counter = 0;
		for (int i = 2005; i <= 2025; i++) {
			subindicator_names_to_ids.put(String.valueOf(i), current_id + 16 + year_counter + 1);
			year_counter++;
		}
		
		
		for(Map.Entry<String, Integer> ee : subindicator_names_to_ids.entrySet()){
			id_sorted_map.put(ee.getValue(), ee.getKey());
		}
		
	
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.defaultPrettyPrintingWriter().writeValueAsString(id_sorted_map));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.exit(1);
		return subindicator_names_to_ids;

	}
}
