package yahoo_finance_historical_scraper;

import java.util.ArrayList;

import org.javatuples.Triplet;

import singletonClass.CalendarFunctions;

import DailyOperationsLogging.LoggingDailyOperations;

public class CheckPriceAberrations {

	private HistoricalStock stock;

	public HistoricalStock getStock() {
		return stock;
	}

	public void setStock(HistoricalStock stock) {
		this.stock = stock;
	}

	public CheckPriceAberrations(HistoricalStock stock) {
		setStock(stock);
		detect_price_aberrations();
	}

	public Boolean detect_price_aberrations() {
		// if any of the prices have changed by more than 7 percent, we abort,
		// and log.

		ArrayList<Boolean> results = new ArrayList<Boolean>();

		results.add(iterate_price_and_check_for_aberration(stock.getclose(),
				"close"));

		if (getStock().getOpen().size() != 0) {
			results.add(iterate_price_and_check_for_aberration(stock.getOpen(),
					"open"));
		}

		if (getStock().getHigh().size() != 0) {
			results.add(iterate_price_and_check_for_aberration(stock.getHigh(),
					"high"));

		}

		if (getStock().getLow().size() != 0) {
			results.add(iterate_price_and_check_for_aberration(stock.getLow(),
					"low"));
		}

		if (results.contains(true)) {
			return true;
		}

		return false;
	}

	public Boolean iterate_price_and_check_for_aberration(
			ArrayList<Double> price, String type) {

		for (int i = 0; i < price.size() - 1; i++) {
			// if(getStock().getclose().get(i+1)!=null){
			Double difference = price.get(i + 1) - price.get(i);
			if ((difference / getStock().getclose().get(i)) * 100 > StockConstants.PRICE_ABERRATION_THRESHOLD_IN_PERCENTAGE) {
				Triplet<Integer, Double, Double> index_and_prices = new Triplet<Integer, Double, Double>(
						i, price.get(i), price.get(i + 1));
				LoggingDailyOperations
						.get_instance()
						.getIndividual_stock_operations()
						.get(stock.getStockName() + "_"
								+ stock.getStockFullName() + "_"
								+ stock.getIndice())
						.getAberration_type_and_index()
						.put(type, index_and_prices);
				if (stock.getIndice().equals("DME")
						|| stock.getIndice().equals("LME")) {
					LoggingDailyOperations
							.get_instance()
							.add_to_log(
									"exception",null,
									
											
									"checkPriceAberrations", "price aberration detected with the following stock-"
											+ stock.getStockName()
											+ "_"
											+ stock.getStockFullName()
											+ "_"
											+ stock.getIndice()
											+ " at this indeice---"
											+ String.valueOf(i)
											+ "and this date"
											+ new CalendarFunctions(
													stock.getDates()
															.get(i))
													.getFullDateWithoutTimeZoneOnlyDate()
											+ "and the next day date was: "
											+ new CalendarFunctions(
													stock.getDates()
															.get(i + 1))
													.getFullDateWithoutTimeZoneOnlyDate()
											+ "\n the next day price was:"
											+ price.get(i + 1)
											+ " todays" + "price was :"
											+ price.get(i));
					;
					return true;
				}
				else
				{
					LoggingDailyOperations
					.get_instance()
					.add_to_log(
							"info",
							new Exception(
									"IGNORING price aberration detected with the following stock-"
											+ stock.getStockName()
											+ "_"
											+ stock.getStockFullName()
											+ "_"
											+ stock.getIndice()
											+ " at this indeice---"
											+ String.valueOf(i)
											+ "and this date"
											+ new CalendarFunctions(
													stock.getDates()
															.get(i))
													.getFullDateWithoutTimeZoneOnlyDate()
											+ "and the next day date was: "
											+ new CalendarFunctions(
													stock.getDates()
															.get(i + 1))
													.getFullDateWithoutTimeZoneOnlyDate()
											+ "\n the next day price was:"
											+ price.get(i + 1)
											+ " todays" + "price was :"
											+ price.get(i)),
							"checkPriceAberrations", null);
			;
				}
			}
			// }
		}

		return false;
	}

}
