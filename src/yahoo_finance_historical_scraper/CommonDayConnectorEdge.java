package yahoo_finance_historical_scraper;

import Titan.MultiThreadable;

import com.tinkerpop.blueprints.Vertex;

public class CommonDayConnectorEdge extends MultiThreadable{

	private Vertex root_vertex;
	private Vertex older_destination_vertex;
	private String edgeLabelName;
	
	public CommonDayConnectorEdge(Vertex root_vertex, Vertex older_destination_vertex){
		this.root_vertex = root_vertex;
		this.older_destination_vertex = older_destination_vertex;
		this.edgeLabelName = "day_connector";
	}

}
