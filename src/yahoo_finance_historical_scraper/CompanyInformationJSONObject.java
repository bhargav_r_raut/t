package yahoo_finance_historical_scraper;

import static jodd.jerry.Jerry.jerry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

import org.json.JSONException;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;

public class CompanyInformationJSONObject {
	private JSONObject company_information_object;
	private String stockName;
	private String indice;
	private Integer indice_number;
	
	public CompanyInformationJSONObject(String stockName,String indice, Integer indice_number) {
		setCompany_information_object(new JSONObject());
		setIndice(indice);
		setIndice_number(indice_number);
		setStockName(stockName);
	}

	/**
	 * @return the company_information_object
	 */
	public JSONObject getCompany_information_object() {
		return company_information_object;
	}

	/**
	 * @param company_information_object
	 *            the company_information_object to set
	 */
	public void setCompany_information_object(
			JSONObject company_information_object) {
		this.company_information_object = company_information_object;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public Integer getIndice_number() {
		return indice_number;
	}

	public void setIndice_number(Integer indice_number) {
		this.indice_number = indice_number;
	}

	public void add_details_from_y() throws Exception{
		
		
		String url_for_company_info = StockConstants.base_url_for_company_details
				+ getStockName();
		/***
		 * start changing from here.
		 * 
		 * 
		 */
		String company_info_page = null;
		try(Jedis jedis = RedisManager.getInstance().getJedis()){
			company_info_page = jedis.get(url_for_company_info);
			if(company_info_page == null){
				company_info_page = ReadWriteTextToFile.getInstance()
				.getTextFromUrl(url_for_company_info);
				jedis.set(url_for_company_info, company_info_page);
			}
		}
		
		System.out.println("stockName is:" + getStockName());
		

		Jerry doc_company_info_page = jerry(company_info_page);

		doc_company_info_page.$("td.yfnc_modtitlew1 p").each(
				new JerryFunction() {
					@Override
					public boolean onNode(Jerry $this, int index) {
						// TODO Auto-generated method stub

						if (index == 0) {
							try {
								getCompany_information_object()
										.put("company_description",
												$this.text());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						return true;
					}
				});

		doc_company_info_page.$(
				"table.yfnc_datamodoutline1 td.yfnc_tabledata1").each(
				new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						// TODO Auto-generated method stub
						//System.out.println("this is the text " + $this.text());
						switch (index) {

						case 1:
							
							if ($this.text().trim() != "") {
								try {
									getCompany_information_object().put(
											"sector",
											get_arraylist_of_sectors($this.text().trim())
											);
								} catch (JSONException e) {
									// TODO Auto-generated catch
									// block
									e.printStackTrace();
								}
							}
							else{
								System.out.println("couldnt scrape successfully it was:" + $this.text());
								try {
									System.in.read();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							break;

						case 2:
							if ($this.text().trim() != "") {
								try {
									getCompany_information_object()
											.put("industry",
													get_arraylist_of_sectors
													($this.text().trim())
													);
								} catch (JSONException e) {
									// TODO Auto-generated catch
									// block
									e.printStackTrace();
								}
							}
							break;

						//

						case 3:
							if ($this.text().trim() != "") {
								try {
									//System.out.println("this is what you get-->" + $this.text().trim().replace(",", ""));
									if($this.text().trim().replace(",", "").contains("N/A") ){
										
									}
									else{
										getCompany_information_object().put(
												"full_time_employees",
												Integer.parseInt($this.text().trim().replace(",", "")));
									}
									
								} catch (JSONException e) {
									// TODO Auto-generated catch
									// block
									e.printStackTrace();
								}
							}
							break;

						default:
							break;
						}

						return true;
					}
				});

	}
	
	public void add_nifty_company_information(List<String[]> csv_lines,String stockName) throws Exception{
		for(String [] line : csv_lines){
			if(line[2].equals(stockName)){
				//System.out.println("found the industry");
				getCompany_information_object().put("industry", get_arraylist_of_sectors(line[1]));
			}
		}
	}
	
	public void create_json_object() throws Exception {
		if (getIndice_number() < 4) {
			add_details_from_y();
		}
	}
	
	public ArrayList<String> get_arraylist_of_sectors(String input){
		input = input.toLowerCase();
		//return input.substring(0, 1).toUpperCase() + input.substring(1);
		return new ArrayList<String>(Arrays.asList(input.split(",")));
	}

}
