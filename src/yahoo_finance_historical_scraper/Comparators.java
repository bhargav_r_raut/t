package yahoo_finance_historical_scraper;

public class Comparators {

	public static Boolean is_double_positive(Double d){
		if(d >= 0){
			return true;
		}
		return false;
	}
	
	
	public static Boolean is_integer_positive(Integer i){
		if( i >= 0){
			return true;
		}
		return false;
	}
	
	public static Boolean is_double_greater(Double d, Double compare_against){
		if(d >= compare_against){
			return true;
		}
		return false;
	}
	
	public static Boolean is_integer_greater(Integer i, Integer compare_against){
		if(i > compare_against){
			return true;
		}
		return false;
	}
	
}
