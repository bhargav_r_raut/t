package yahoo_finance_historical_scraper;

import static jodd.jerry.Jerry.jerry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

import org.json.JSONException;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;
import ComponentsProvider.WikipediaScraper;
import DailyOperationsLogging.LoggingDailyOperations;
import ExchangeBuilder.Entity;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;
import au.com.bytecode.opencsv.CSVReader;
import forex.forexConstants;

public class ComponentScraper {

	public static final String identifier_for_log = "component_scraper";
	public static ComponentScraper instance;
	public static HashMap<String, ArrayList<String>> index_names_and_stock_names;
	public static HashMap<String, JSONObject> company_information;

	public static HashMap<String, JSONObject> getCompany_information() {
		return company_information;
	}

	public static void setCompany_information(
			HashMap<String, JSONObject> company_information) {
		ComponentScraper.company_information = company_information;
	}

	public static String getText(String url) throws Exception {
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));

		StringBuilder response = new StringBuilder();
		String inputLine;

		while ((inputLine = in.readLine()) != null)
			response.append(inputLine);
		response.append("\n");
		in.close();

		return response.toString();
	}

	public static HashMap<String, ArrayList<String>> getIndex_names_and_stock_names() {
		return index_names_and_stock_names;
	}

	public static void setIndex_names_and_stock_names(
			HashMap<String, ArrayList<String>> index_names_and_stock_names) {
		ComponentScraper.index_names_and_stock_names = index_names_and_stock_names;
	}

	public static ComponentScraper getInstance(Integer index_id)
			throws Exception {

		instance = new ComponentScraper();
		// configure hashmap
		configureHashMap(index_id);

		return instance;
	}

	public static void setInstance(ComponentScraper instance) {
		ComponentScraper.instance = instance;
	}

	public void parseComponents(String response) {

	}

	public static void wikipedia_stock_page_and_sector_extraction(
			Integer index_counter) throws IOException {

		String url = StockConstants.wikipedia_components_pages[index_counter];
		String response = ReadWriteTextToFile.getInstance().getTextFromUrl(url);
		Jerry jerry_doc = jerry(response);
		jerry_doc.$("#constituents.tr").each(new JerryFunction() {
			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				$this.$("td").each(new JerryFunction() {
					@Override
					public boolean onNode(Jerry $this, int index) {
						// TODO Auto-generated method stub
						return false;
					}
				});
				return false;
			}
		});

	}

	/**
	 * 
	 * 
	 * @param wso
	 *            - the wikipedia stock object.
	 * @param stockName
	 *            - the stock symbol as saved in company information
	 * @throws JSONException
	 */
	public static void merge_wiki_information_with_company_information(
			Entity wso, String stockName) throws JSONException {
		JSONObject company_information = getCompany_information()
				.get(stockName);

		Set<String> zones = new HashSet<String>();
		
		System.out.println("company information is:" + company_information);
		
		org.json.JSONArray sectors = new org.json.JSONArray();
		if(company_information.has("sector")){
			sectors = (org.json.JSONArray) company_information
				.get("sector");
			for(int i=0; i < sectors.length(); i++){
				zones.add(sectors.getString(i));
			}
		}
		else{
			//System.out.println("the sector was not there for this stock:" + stockName);
			//try {
			//	System.in.read();
			//} catch (IOException e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
		}
		
		
		org.json.JSONArray industry = new org.json.JSONArray();
		if(company_information.has("industry")){
			industry = (org.json.JSONArray) company_information
				.get("industry");
			for(int i=0; i < industry.length(); i++){
				zones.add(industry.getString(i));
			}
		}
		else{
			//System.out.println("the industry was not there for this stock:" + stockName);
			//try {
			//	System.in.read();
			//} catch (IOException e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
		}

	
		zones.addAll(wso.getIndustry());
		zones.addAll(wso.getProducts());
		
		String[] zl = new String[zones.size()];
		zones.toArray(zl);
	
		company_information.put("zones", zl);
		getCompany_information().put(stockName, company_information);

	}

	public static void configureHashMap(Integer index_id) throws Exception {
		// here we scrape all the pages and get all the components.
		// System.out.println("entered the components scraper function.");
		setIndex_names_and_stock_names(new HashMap<String, ArrayList<String>>());
		setCompany_information(new HashMap<String, JSONObject>());
		// get indice from the string array.
		String indice = StockConstants.indices[index_id];
		if (index_id < 4) {
			WikipediaScraper scraper = new WikipediaScraper(index_id);
			final ArrayList<String> indice_stocks = new ArrayList<String>();

			boolean go_on = true;

			for (int ii = 0; ii < 10; ii++) {

				if (go_on) {

					String url = StockConstants.base_url + indice
							+ StockConstants.alphabet_piece
							+ Integer.toString(ii);

					/***
					 * THE TEST STUFF STARTS HERE. REMOVE THIS LATER CAUTION.
					 * 
					 * only leave this line; String response =
					 * ReadWriteTextToFile.getInstance() .getTextFromUrl(url);
					 * 
					 */

					String response = null;
					try (Jedis jedis = RedisManager.getInstance().getJedis()) {
						response = jedis.get(url);
						if (response == null) {
							System.out.println("no components got.");
							response = ReadWriteTextToFile.getInstance()
									.getTextFromUrl(url);
							jedis.set(url, response);

						}
					}

					Jerry doc = jerry(response);

					int indice_size_before = indice_stocks.size();
					// System.out.println("siz before--->" +
					// indice_size_before);
					doc.$("td.yfnc_tabledata1").each(new JerryFunction() {

						int internal_counter = 0;

						@Override
						public boolean onNode(Jerry $this, int index) {
							// TODO Auto-generated method stub
							if (internal_counter == 0) {
								// System.out.println("this si the symbol"
								// + $this.text());
								internal_counter++;
								indice_stocks.add($this.text().trim());

							} else if (internal_counter == 1) {
								// System.out.println("this is the full name"
								// + $this.text());
								internal_counter++;
								indice_stocks.add($this.text().trim());
							} else if (internal_counter == 4) {

								internal_counter = 0;
							} else {

								internal_counter++;
							}

							// System.out.println(index);
							// System.out.println($this.text());

							return true;
						}

					});

					// now we get url for the details of the stock.

					if (indice_stocks.size() > indice_size_before) {
						// System.out.println("left true");
					} else {
						// System.out.println("made false");
						go_on = false;
					}

				}

			}
			
			getIndex_names_and_stock_names().put(indice, indice_stocks);
			HashMap<String, Entity> wikicomponents = scraper
					.getWikiobjectsmap();
			ArrayList<String> wikiobjectsmap_stockNames = scraper
					.getStockNames_in_wikiobjest_map();

			Integer alternate = 0;

			String stockSymbol = null;

			for (String s : indice_stocks) {
				if (alternate == 0) {
					CompanyInformationJSONObject cj = new CompanyInformationJSONObject(
							s, indice, index_id);
					cj.create_json_object();
					getCompany_information().put(cj.getStockName(),
							cj.getCompany_information_object());
					stockSymbol = s;
					alternate++;
				} else {
					alternate = 0;
					// now we have to get the matching name from wikicomponents.
					// and merge the companyinformation objects.

					String normalized_string = WikipediaScraper
							.prepare_string(stockSymbol);
				//	System.out.println("stocksymbol is:" + stockSymbol
				//			+ " and normalized string is:" + normalized_string);
					Boolean found = false;
					for (String wiki_stockName : wikiobjectsmap_stockNames) {
						if (normalized_string.equals(wiki_stockName)) {
							merge_wiki_information_with_company_information(
									wikicomponents.get(wiki_stockName),
									stockSymbol);
							found = true;
							break;
						} else if (normalized_string.contains(wiki_stockName)) {
							merge_wiki_information_with_company_information(
									wikicomponents.get(wiki_stockName),
									stockSymbol);
							found = true;
							break;
						} else if (wiki_stockName.contains(normalized_string)) {
							merge_wiki_information_with_company_information(
									wikicomponents.get(wiki_stockName),
									stockSymbol);
							found = true;

							break;
						} else {

						}
					}
					if (!found) {

						LoggingDailyOperations.get_instance().add_to_log(
								"info",
								new Exception("could not match "
										+ "this root stock symbol : "
										+ normalized_string),
								identifier_for_log, null);
						System.in.read();
					}
				}
			}

		} else {
			switch (index_id) {
			case 4:
				// String niftyComponents =
				// ReadWriteTextToFile.getInstance().getTextFromUrl
				// ("http://www.nseindia.com/content/indices/ind_niftylist.csv");
				// BufferedReader br =
				// ReadWriteTextToFile.getInstance().stringToBufferedReader(niftyComponents);
				URL stockURL = new URL(
						StockConstants.url_for_nifty_component_csv);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						stockURL.openStream()));
				CSVReader nifty_components_csv = new CSVReader(br);
				List<String[]> myEntries = nifty_components_csv.readAll();
				ArrayList<String> indice_stocks = new ArrayList<String>();

				Integer counter = 0;
				List<String[]> entries_with_correct_names = new ArrayList<String[]>();
				for (String[] line : myEntries) {
					if (counter != 0) {

						String correct_symbol_name = find_correct_name_for_nifty_component(line[2]
								+ ".NS");
						if (correct_symbol_name != null) {
							// System.out.println("this sybmol name is correct:"
							// + correct_symbol_name);
							indice_stocks.add(correct_symbol_name);
							indice_stocks.add(line[0]);
							String[] k = line;
							k[2] = correct_symbol_name;
							entries_with_correct_names.add(k);
						} else {
							LoggingDailyOperations.get_instance().add_to_log(
									"info",
									null,
									"Component_scraper",
									"NIFTY SYMBOL COULD NOT BE RESOLVED:"
											+ line[2]);
						}
					}
					counter++;
				}

				getIndex_names_and_stock_names().put(indice, indice_stocks);

				Integer alternate = 0;

				for (String s : indice_stocks) {
					if (alternate == 0) {
						CompanyInformationJSONObject cj = new CompanyInformationJSONObject(
								s, indice, index_id);
						cj.add_nifty_company_information(
								entries_with_correct_names, s);
						getCompany_information().put(cj.getStockName(),
								cj.getCompany_information_object());
						alternate++;
					} else {
						alternate = 0;
					}
				}

				break;
			case 5:
				// oil
				CompanyInformationJSONObject cj = new CompanyInformationJSONObject(
						StockConstants.OIL_STOCK_NAME, indice, index_id);
				cj.create_json_object();
				getCompany_information().put(cj.getStockName(),
						cj.getCompany_information_object());

				ArrayList<String> oil_index = new ArrayList<String>();
				oil_index.add(StockConstants.OIL_STOCK_NAME);
				oil_index.add(StockConstants.OIL_STOCK_NAME);
				getIndex_names_and_stock_names().put(indice, oil_index);

				break;

			case 6:
				// metals
				CompanyInformationJSONObject cjgold = new CompanyInformationJSONObject(
						StockConstants.GOLD_STOCK_NAME, indice, index_id);
				cjgold.create_json_object();
				getCompany_information().put(cjgold.getStockName(),
						cjgold.getCompany_information_object());

				cjgold = new CompanyInformationJSONObject(
						StockConstants.SILVER_STOCK_NAME, indice, index_id);
				cjgold.create_json_object();
				getCompany_information().put(cjgold.getStockName(),
						cjgold.getCompany_information_object());

				ArrayList<String> metals_index = new ArrayList<String>();
				metals_index.add(StockConstants.GOLD_STOCK_NAME);
				metals_index.add(StockConstants.GOLD_STOCK_NAME);
				metals_index.add(StockConstants.SILVER_STOCK_NAME);
				metals_index.add(StockConstants.SILVER_STOCK_NAME);
				getIndex_names_and_stock_names().put(indice, metals_index);
				break;

			case 7:
				// forex
				ArrayList<String> forex_index = new ArrayList<String>();
				int so = 0;
				for (String shortForm : forexConstants.shortforms) {
					CompanyInformationJSONObject cjf = new CompanyInformationJSONObject(
							shortForm, indice, index_id);
					cjf.create_json_object();
					getCompany_information().put(cjf.getStockName(),
							cjf.getCompany_information_object());
					forex_index.add(shortForm);
					forex_index.add(forexConstants.fullforms[so]);
					so++;
				}
				getIndex_names_and_stock_names().put(indice, forex_index);
				break;

			default:
				break;
			}
		}

		add_indice_and_components_to_daily_log(indice);

	}

	public static void add_indice_and_components_to_daily_log(String indice) {
		Boolean added = false;
		for (Map.Entry<String, ArrayList<String>> entry : getIndex_names_and_stock_names()
				.entrySet()) {
			if (entry.getKey().equals(indice)) {
				if (entry.getValue().isEmpty()) {
					LoggingDailyOperations.get_instance().add_to_log(
							"exception",
							new Exception(
									"no components could be scraped for this index:"
											+ indice), "component_scraper",
							null);
					break;
				} else {
					LoggingDailyOperations.get_instance()
							.add_indice_components(entry.getKey(),
									entry.getValue());
					added = true;
				}
			}

		}

		if (!added) {
			LoggingDailyOperations
					.get_instance()
					.add_to_log(
							"exception",
							new Exception(
									"the indice being scraped,"
											+ "was not present in the indice and components hash, probably nothing was"
											+ "found it it: " + indice),
							"component_scraper", null);
		}
	}

	/***
	 * in the starting we say that proceed is false, and found correct name is
	 * true. this is because if no errors are encountered, and we dont loop.
	 * however the first run of the loop proceeds because counter is 0
	 * 
	 * inside the loop, if the error is encounterd, first we check if the size
	 * of the name being tried will not become zero.
	 * 
	 * in that case if its becoming zero, we set that correct name was never
	 * found. and we set proceed to false, so the loop will stop and before
	 * returning from the entire function we check if the found correct name was
	 * true or false and , if it was false, we return null from this function,
	 * and that means this component should not be added.
	 * 
	 * if the correct name was found, we return it.
	 * 
	 * 
	 * 
	 * @param current_name
	 * @return
	 * @throws IOException
	 */
	public static String find_correct_name_for_nifty_component(
			String current_name) throws IOException {

		final ArrayList<Boolean> proceed = new ArrayList<Boolean>();
		proceed.add(false);
		final ArrayList<Boolean> found_correct_name = new ArrayList<Boolean>();
		found_correct_name.add(true);
		final ArrayList<String> names_being_tried = new ArrayList<String>();
		names_being_tried.add(current_name);
		Integer counter = 0;
		while (proceed.get(0) || counter == 0) {
			String url = StockConstants.url_for_verifying_nifty_components_start
					+ names_being_tried.get(0)
					+ StockConstants.url_for_veriftying_nifty_components_ends;
			String page_response = ReadWriteTextToFile.getInstance()
					.getTextFromUrl(url);
			Jerry doc = jerry(page_response);
			if (doc.$("div[class=\"error\"]").length() == 0) {
				// check if the historical data is available or not.
				if (doc.text()
						.contains(
								"Historical quote data is unavailable for the specified date range")) {
					proceed.clear();
					proceed.add(false);
					found_correct_name.clear();
					found_correct_name.add(false);
					System.out.println("no historical data");
				} else {
					proceed.clear();
					proceed.add(false);
					System.out.println("no problems");
				}
			} else {
				doc.$("div[class=\"error\"]").each(new JerryFunction() {

					@Override
					public boolean onNode(Jerry $this, int index) {
						// TODO Auto-generated method stub

						if ($this
								.text()
								.trim()
								.contains(
										"There are no results for the given search term.")) {
							// System.out.println("error found for this name:"
							// + names_being_tried.get(0));
							String new_name_without_ns = names_being_tried.get(
									0).replace(".NS", "");
							String new_name = new_name_without_ns.substring(0,
									new_name_without_ns.length() - 1) + ".NS";
							// System.out.println("new name created was:"
							// + new_name);

							if (new_name.length() != 3) {
								// System.out
								// .println("name lenght is not yet three.");
								names_being_tried.clear();
								names_being_tried.add(new_name);
								proceed.clear();
								proceed.add(true);
							} else {
								// System.out
								// .println("name lenght has become three(.NS).");
								proceed.clear();
								proceed.add(false);
								found_correct_name.clear();
								found_correct_name.add(false);
							}

							// System.out
							// .println("names being tried now look like:"
							// + names_being_tried.toString());
							// System.out.println("proceed now looks like:"
							// + proceed.toString());
							// System.out
							// .println("found correct name now looks like "
							// + found_correct_name.toString());

							// System.out
							// .println("now returning false and coming out of error detection block");
							// try {
							// System.in.read();
							// } catch (IOException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							return false;
						}
						return true;
					}
				});
			}

			counter++;
		}

		if (found_correct_name.get(0)) {
			return names_being_tried.get(0);
		} else {

			return null;
		}
	}

}
