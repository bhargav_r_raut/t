package yahoo_finance_historical_scraper;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TreeMap;

import org.javatuples.Pair;

public class DailyPriceHolderObject {

	private Calendar cal;
	private Double open;
	private Double high;
	private Double low;
	private Double actual_close;
	private Double close;
	private Integer volume;
	private boolean abort = false;
	private TreeMap<Calendar, Pair<Double, Integer>> oil_date_with_close_and_volume;

	
	public DailyPriceHolderObject(TreeMap<Calendar, Pair<Double, Integer>> tr){
		this.oil_date_with_close_and_volume = tr;
	}
	
	public void clearAllFields(){
		this.cal = null;
		this.abort = false;
		this.close = null;
		this.open = null;
		this.high = null;
		this.low = null;
		this.volume = null;
		this.actual_close = null;
	}
	
	public Calendar getCal() {
		return cal;
	}
	public void setCal(Calendar cal) {
		this.cal = cal;
	}
	public Double getOpen() {
		return open;
	}
	public void setOpen(Double open) {
		this.open = open;
	}
	public Double getHigh() {
		return high;
	}
	public void setHigh(Double high) {
		this.high = high;
	}
	public Double getLow() {
		return low;
	}
	public void setLow(Double low) {
		this.low = low;
	}
	public Double getActual_close() {
		return actual_close;
	}
	public void setActual_close(Double actual_close) {
		this.actual_close = actual_close;
	}
	public Double getClose() {
		return close;
	}
	public void setClose(Double close) {
		if(close == 0.0){
			setAbort(true);
		}
		this.close = close;
	}
	public Integer getVolume() {
		return volume;
	}
	public void setVolume(Integer volume) {
		if(volume == 0){
			setAbort(true);
		}
		this.volume = volume;
	}
	
	public boolean isAbort() {
		return abort;
	}
	
	public void setAbort(boolean abort) {
		this.abort = abort;
	}
	
	public void add_to_treemap(Calendar cal,Double close, Integer volume){
		this.oil_date_with_close_and_volume.put(cal, new Pair<>(close, volume));
	}
	
	public TreeMap<Calendar, Pair<Double, Integer>>  get_treemap(){
		return this.oil_date_with_close_and_volume;
	}

}
