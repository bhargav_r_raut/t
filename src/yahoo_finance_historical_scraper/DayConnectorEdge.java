package yahoo_finance_historical_scraper;

import Titan.MultiThreadable;

import com.thinkaurelius.titan.graphdb.vertices.CacheVertex;
import com.tinkerpop.blueprints.Vertex;

public class DayConnectorEdge extends MultiThreadable{
	
	
	public Float price_change_percentage;
	public Integer days_back;
	public Integer profit_or_loss;
	//public String unique_identifier;
	///private fields, accessible via additional params
	private String edge_id;
	public String getEdge_id() {
		return edge_id;
	}
	public void setEdge_id(String edge_id) {
		this.edge_id = edge_id;
	}
	private String edgeLabelName;
	private String stock_combination;
	private String stockName;
	private String stockFullName;
	private String indice;
	private Integer day_id_from;
	private Integer day_id_to;
	public String getStock_combination() {
		return stock_combination;
	}
	public void setStock_combination(String stock_combination) {
		this.stock_combination = stock_combination;
	}
	/*
	public Vertex getRoot_vertex() {
		return root_vertex;
	}
	public void setRoot_vertex(Vertex root_vertex) {
		this.root_vertex = root_vertex;
	}
	
	public Vertex getOlder_destination_vertex() {
		return older_destination_vertex;
	}
	public void setOlder_destination_vertex(Vertex older_destination_vertex) {
		this.older_destination_vertex = older_destination_vertex;
	}
	*/
	public Float getPrice_change_percentage() {
		return price_change_percentage;
	}
	public void setPrice_change_percentage(Float price_change_percentage) {
		this.price_change_percentage = price_change_percentage;
	}
	public Integer getDays_back() {
		return days_back;
	}
	public void setDays_back(Integer days_back) {
		this.days_back = days_back;
	}
	public Integer getProfit_or_loss() {
		return profit_or_loss;
	}
	public void setProfit_or_loss(Integer profit_or_loss) {
		this.profit_or_loss = profit_or_loss;
	}
	/**
	 * @param price_change_percentage
	 * @param days_back
	 * @param profit_or_loss
	 * @param root_vertex
	 * @param older_destination_vertex
	 */
	public DayConnectorEdge(Float price_change_percentage, Integer days_back,
			Integer profit_or_loss, String stock_combination, String edge_id,
			String stockName, String stockFullName, String indice, Integer day_id_from, Integer day_id_to) {
		this.price_change_percentage = price_change_percentage;
		this.days_back = days_back;
		this.profit_or_loss = profit_or_loss;
		
		//this.unique_identifier = "day_connector_edge_" + stockuniqueidentifier;
		this.stock_combination = stock_combination;
		this.edgeLabelName = "day_connector";
		this.edge_id = edge_id;
		this.stockName = stockName;
		this.stockFullName = stockFullName;
		this.indice = indice;
		this.day_id_from = day_id_from;
		this.day_id_to = day_id_to;
	}
	
	
	
	
	
}
