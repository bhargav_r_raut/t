package yahoo_finance_historical_scraper;

import static jodd.jerry.Jerry.jerry;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import metals.MetalsScraper;

import org.joda.time.DateTime;

import redis.clients.jedis.Jedis;
import Indicators.RedisManager;
import Titan.ReadWriteTextToFile;

public class HistoricalQouteGetter extends MetalsScraper {
	private Calendar start_date;
	private Calendar end_date;
	private Calendar real_start_date;
	private Calendar real_end_date;
	private HistoricalStock historicalStock;
	// private static final String baseUrl =
	// "http://ichart.finance.yahoo.com/table.csv?s=";
	// private static final String middleUrl = "";
	private String close_time;
	private URL finalUrl;

	private ArrayList<String> params = new ArrayList<String>();

	public static String getBaseurl() {
		return StockConstants.base_url_for_historical_data_download;
	}

	public void create_start_dates_and_end_dates(Calendar sd, Calendar ed) {
		DateTime real_start_date_one = new DateTime(sd);
		DateTime real_end_date_one = new DateTime(ed);

		this.start_date = real_start_date_one.minusDays(10).toCalendar(null);

		// add 10 days to the end day so that we will definitely get something.
		//this.end_date = real_end_date_one.plusDays(10).toCalendar(null);
		/**
		 * MADE MODIFICATION HERE.
		 * USE THE LINE ABOVE THIS INSTEAD.
		 * 
		 */
		this.end_date = real_end_date_one.toCalendar(null);

	}

	public HistoricalQouteGetter(Calendar start_date_main,
			Calendar end_date_main, String stock_name, TimeZone timeZone,
			String entity_type, String close_time, String indice,
			String stockFullName) {

		this.real_start_date = start_date_main;
		this.real_end_date = end_date_main;
		setClose_time(close_time);

		create_start_dates_and_end_dates(start_date_main, end_date_main);

		// System.out.println("these are the start and the end after");
		// System.out.println(new
		// CalendarFunctions(getReal_start_date()).getFullDate());
		// System.out.println(new
		// CalendarFunctions(getReal_end_date()).getFullDate());

		// this.start_date = start_date;
		// this.end_date = end_date;
		/*
		 * String real_index_name = ""; switch (indice) {
		 * 
		 * case "NDX": real_index_name = "NASDAQ"; break;
		 * 
		 * case "GDAXI": real_index_name = "DAX"; break;
		 * 
		 * 
		 * case "FCHI": real_index_name = "PARIS"; break;
		 * 
		 * 
		 * case "FTSE": real_index_name = "LSE"; break;
		 * 
		 * 
		 * case "NSEI": real_index_name = "NIFTY"; break;
		 * 
		 * default:
		 * 
		 * break;
		 * 
		 * }
		 */

		setHistoricalStock(new HistoricalStock(stock_name, timeZone,
				entity_type, indice, stockFullName));

		params.add("&a=");
		params.add("&b=");
		params.add("&c=");
		params.add("&d=");
		params.add("&e=");
		params.add("&f=");
	}

	public HistoricalStock getHistoricalStock() {
		return historicalStock;
	}

	public void setHistoricalStock(HistoricalStock historicalStock) {
		this.historicalStock = historicalStock;
	}

	// now we have to write a method to get the data and read it into a
	// historical stock object.
	@SuppressWarnings("deprecation")
	public void populateHistoricalStock() throws IOException, ParseException,
			IllegalArgumentException, IllegalAccessException,
			InputListSizeInsufficient {

		// String params_string = baseUrl + this.historicalStock.getStockName()
		// + middleUrl;

		String params_string = StockConstants.base_url_for_historical_data_download
				+ StockConstants.middle_url_for_historical_data_download
				+ this.historicalStock.getStockName();

		for (String param : params) {
			switch (param) {
			case "&a=":
				params_string = params_string + param
						+ Integer.toString(start_date.get(Calendar.MONTH));
				break;

			case "&b=":
				params_string = params_string
						+ param
						+ Integer.toString(start_date
								.get(Calendar.DAY_OF_MONTH));
				break;

			case "&c=":
				params_string = params_string + param
						+ Integer.toString(start_date.get(Calendar.YEAR));
				break;

			case "&d=":
				params_string = params_string + param
						+ Integer.toString(end_date.get(Calendar.MONTH));

				break;

			case "&e=":
				params_string = params_string + param
						+ Integer.toString(end_date.get(Calendar.DAY_OF_MONTH));

				break;

			case "&f=":
				params_string = params_string + param
						+ Integer.toString(end_date.get(Calendar.YEAR));

				break;

			default:
				break;
			}
		}
		params_string = params_string
				+ StockConstants.end_url_for_historical_data_download;

		get_historical_data(params_string);

		/*
		 * OLD PART this.finalUrl = new URL(params_string); String response =
		 * ReadWriteTextToFile.getInstance().getTextFromUrl( params_string);
		 * InputStreamReader is = new InputStreamReader(new
		 * ByteArrayInputStream( response.getBytes(StandardCharsets.UTF_8)));
		 * BufferedReader br = new BufferedReader(is);
		 * 
		 * String line; int counter = 0;
		 * 
		 * while ((line = br.readLine()) != null) { if (counter == 0) {
		 * 
		 * } else { String[] price_array = line.split(","); int
		 * internalParamCounter = 0; // here we dont need a tremmap, like in
		 * oil, because yahoo gives // the data fortunately in order.
		 * DailyPriceHolderObject DPHolderObject = new DailyPriceHolderObject(
		 * null);
		 * 
		 * for (String s : price_array) { // System.out.println(s); if
		 * (Pattern.matches("N/A", s)) { DPHolderObject.setAbort(true); } switch
		 * (internalParamCounter) { case 0: //
		 * System.out.println("this is the date returned"); //
		 * System.out.println(s); Calendar cal = parseDateToCalendar(s); //
		 * this.historicalStock.addDate(cal); DPHolderObject.setCal(cal); //
		 * System.out.println("this is the cal entry."); //
		 * System.out.println(DPHolderObject.getCal()); break;
		 * 
		 * case 1:
		 * 
		 * DPHolderObject.setOpen(Double.parseDouble(s)); //
		 * System.out.println("this is the open entry."); //
		 * System.out.println(DPHolderObject.getOpen()); break;
		 * 
		 * case 2: DPHolderObject.setHigh(Double.parseDouble(s)); break;
		 * 
		 * case 3: DPHolderObject.setLow(Double.parseDouble(s)); break;
		 * 
		 * case 4: DPHolderObject.setActual_close(Double.parseDouble(s)); break;
		 * 
		 * case 5: DPHolderObject.setVolume(Integer.parseInt(s)); break;
		 * 
		 * case 6: DPHolderObject.setClose(Double.parseDouble(s)); break;
		 * 
		 * default: break; } internalParamCounter++; }
		 * 
		 * if (!DPHolderObject.isAbort()) { // now we will add all the
		 * parameters to the historical // stock object. //
		 * System.out.println(DPHolderObject.getOpen());
		 * this.historicalStock.addDate(DPHolderObject.getCal());
		 * this.historicalStock.addopen(DPHolderObject.getOpen());
		 * this.historicalStock.addHigh(DPHolderObject.getHigh());
		 * this.historicalStock.addLow(DPHolderObject.getLow());
		 * this.historicalStock.addActual_close(DPHolderObject
		 * .getActual_close());
		 * this.historicalStock.addVolume(DPHolderObject.getVolume());
		 * this.historicalStock.addClose(DPHolderObject.getClose()); }
		 * 
		 * }
		 * 
		 * counter++; }
		 */
		// now we will return historical stock, which has all the data, that
		// needs to sent into titan.

		Collections.reverse(this.historicalStock.actual_close);
		Collections.reverse(this.historicalStock.close);
		Collections.reverse(this.historicalStock.dates);
		Collections.reverse(this.historicalStock.epoches);
		Collections.reverse(this.historicalStock.high);
		Collections.reverse(this.historicalStock.low);
		Collections.reverse(this.historicalStock.open);
		Collections.reverse(this.historicalStock.volume);
		Collections.reverse(this.historicalStock.day_of_month);
		Collections.reverse(this.historicalStock.day_of_week);
		Collections.reverse(this.historicalStock.month);
		Collections.reverse(this.historicalStock.week_of_month);
		Collections.reverse(this.historicalStock.week_of_year);
		Collections.reverse(this.historicalStock.month_of_year);
		Collections.reverse(this.historicalStock.quarter_of_year);
		Collections.reverse(this.historicalStock.half_of_year);
		Collections.reverse(this.historicalStock.day_id);
		Collections.reverse(this.historicalStock.unique_identifier);

		// now adjust
		// System.out.println("now adjusting the stock for the dates.");
		this.historicalStock.adjustStockObject(getReal_start_date(),
				getReal_end_date());

	}

	/*
	 * public Calendar parseDateToCalendar(String dateString) throws
	 * ParseException { Calendar calendar = new GregorianCalendar(); TimeZone tz
	 * = this.historicalStock.getTz(); //
	 * System.out.println("it has to parse this date---->" + dateString);
	 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 * sdf.setTimeZone(this.historicalStock.getTz());
	 * calendar.setTime(sdf.parse(dateString + " " + getClose_time()));
	 * calendar.setTimeZone(tz); // System.out.println(new
	 * CalendarFunctions(calendar).getFullDate()); return calendar; }
	 */
	public URL getFinalUrl() {
		return finalUrl;
	}

	public void setFinalUrl(URL finalUrl) {
		this.finalUrl = finalUrl;
	}

	public Calendar getReal_start_date() {
		return real_start_date;
	}

	public void setReal_start_date(Calendar real_start_date) {
		this.real_start_date = real_start_date;
	}

	public Calendar getReal_end_date() {
		return real_end_date;
	}

	public void setReal_end_date(Calendar real_end_date) {
		this.real_end_date = real_end_date;
	}

	public String getClose_time() {
		return close_time;
	}

	public void setClose_time(String close_time) {
		this.close_time = close_time;
	}

	// ***************************************************
	// CHANGED FUNCTIONS START HERE
	// ***************************************************

	public ArrayList<String> check_if_more_pages_available(Jerry doc,
			final ArrayList<String> next_page) {

		doc.$("a[rel=\"next\"]").each(new JerryFunction() {

			@Override
			public boolean onNode(Jerry $this, int index) {
				// TODO Auto-generated method stub
				next_page.add(getBaseurl() + $this.attr("href"));

				return false;
			}
		});

		return next_page;

	}

	public Jerry scrape_page(String url) throws IOException {
		try {
			
			String response = null;
			/***
			 * this is for test purposes, only , we are storing the page response into redis.
			 * REMOVE THIS AFTERWARDS.
			 * 
			 * 
			 */
			try(Jedis jedis = RedisManager.getInstance().getJedis()){
				response = jedis.get(url);
				if(response == null){
					
			
					response = ReadWriteTextToFile.getInstance().getTextFromUrl(
									url); 
					jedis.set(url, response);
					
					
				}
				//System.out.println("the url response was already stored in redis.");
				//System.in.read();
				
			}
			
			/***
			 * END PART TO REMOVE.
			 * 
			 * 
			 * 
			 */
			
			//response = ReadWriteTextToFile.getInstance().getTextFromUrl(
			//		url);
			
			// UserAgent userAgent = new UserAgent(); // create new userAgent
			// (headless browser).
			// userAgent.visit(url); // visit a url
			// String response = userAgent.doc.innerHTML();
			Jerry doc = jerry(response);
			final String uu = url;

			doc.$("table[class=\"yfnc_datamodoutline1\"]").each(
					new JerryFunction() {
						@Override
						public boolean onNode(Jerry $this, int index) {
							// TODO Auto-generated method stub
							final Integer total_rows = $this.$("table tr")
									.length();
							System.out.println("this was the url:" + uu);
							System.out.println("this was the number of rows:"
									+ total_rows);

							$this.$("table tr").each(new JerryFunction() {
								@Override
								public boolean onNode(Jerry $this, int index) {
									// TODO Auto-generated method stub
									final DailyPriceHolderObject DPHolderObject = new DailyPriceHolderObject(
											null);
									if (index == 0 || index == total_rows - 1) {

									} else {
										if ($this.$("td").size() == StockConstants.total_number_of_cells_expected_in_stock_scraper_row) {
											$this.$("td").each(
													new JerryFunction() {
														@Override
														public boolean onNode(
																Jerry $this,
																int index) {

															if (index == 0) {

																try {
																	Calendar cal = parse_calendar($this
																			.text());
																	DPHolderObject
																			.setCal(cal);
																} catch (ParseException e) {
																	DPHolderObject
																			.setAbort(true);
																}

															} else if (index == 1) {
																try {
																	DPHolderObject
																			.setOpen(Double
																					.parseDouble($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															} else if (index == 2) {
																try {
																	DPHolderObject
																			.setHigh(Double
																					.parseDouble($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															} else if (index == 3) {
																try {
																	DPHolderObject
																			.setLow(Double
																					.parseDouble($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															} else if (index == 4) {
																try {
																	DPHolderObject
																			.setActual_close(Double
																					.parseDouble($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															} else if (index == 5) {
																try {
																	DPHolderObject
																			.setVolume(Integer
																					.parseInt($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															} else if (index == 6) {
																try {
																	DPHolderObject
																			.setClose(Double
																					.parseDouble($this
																							.text()
																							.replace(
																									",",
																									"")));
																} catch (Exception e) {
																	DPHolderObject
																			.setAbort(true);
																}
															}

															return true;
														}

													});
										}
										/**
										 * there were not seven cells in the row for this date.
										 * probably a dividend or something.
										 */
										else{
											//System.out.println("the size was not correct");
											//System.out.println("this was its text:" + $this.text());
											//try {
											//	System.in.read();
											//} catch (IOException e) {
												// TODO Auto-generated catch block
											//	e.printStackTrace();
											//}
											DPHolderObject.setAbort(true);
										}
										// System.out.println(sb.toString());
										if (!DPHolderObject.isAbort()) {
											getHistoricalStock().addDate(
													DPHolderObject.getCal());
											getHistoricalStock().addopen(
													DPHolderObject.getOpen());
											getHistoricalStock().addHigh(
													DPHolderObject.getHigh());
											getHistoricalStock().addLow(
													DPHolderObject.getLow());
											getHistoricalStock()
													.addActual_close(
															DPHolderObject
																	.getActual_close());
											getHistoricalStock().addVolume(
													DPHolderObject.getVolume()
															.longValue());
											getHistoricalStock().addClose(
													DPHolderObject.getClose());
										}
									}
									return true;
								}
							});
							return false;
						}
					});

			return doc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Calendar parse_calendar(String dateString) throws ParseException {
		Calendar calendar = new GregorianCalendar();
		TimeZone tz = this.historicalStock.getTz();
		// System.out.println("it has to parse this date---->" + dateString);
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
		sdf.setTimeZone(this.historicalStock.getTz());
		calendar.setTime(sdf.parse(dateString + " " + getClose_time()));
		calendar.setTimeZone(tz);
		// System.out.println(new CalendarFunctions(calendar).getFullDate());
		return calendar;
	}

	public void get_historical_data(String url) throws IOException {
		Boolean proceed = true;
		while (proceed) {
			Jerry doc = scrape_page(url);
			ArrayList<String> next_page_urls = check_if_more_pages_available(
					doc, new ArrayList<String>());
			if (!next_page_urls.isEmpty()) {
				url = next_page_urls.get(0);
			} else {
				proceed = false;
			}
		}
	}
}
