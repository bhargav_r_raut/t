package yahoo_finance_historical_scraper;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.hadoop.hdfs.protocol.proto.ClientDatanodeProtocolProtos.GetHdfsBlockLocationsRequestProto;
import org.joda.time.DateTime;

import CommonUtils.FloatGlitch;
import DailyOperationsLogging.LoggingDailyOperations;

import dailyDownload.AddHistoricalStockQueryResultsToDatabase;
import singletonClass.CalendarFunctions;

public class HistoricalStock {
	
	public static final String identifier_for_log = "historical_stock";
	
	public ArrayList<Double> open;
	public ArrayList<Double> high;
	public ArrayList<Double> low;
	public ArrayList<Double> actual_close;
	public ArrayList<Double> close;
	public ArrayList<Double> volume;
	public ArrayList<Calendar> dates;
	public ArrayList<Long> epoches;
	public String stockName;
	public TimeZone tz;
	public String entityType;
	public String timezoneString;
	public String indice;
	public String stockFullName;
	public ArrayList<String> unique_identifier;
	// new ones//
	public ArrayList<Integer> day_of_month;
	public ArrayList<String> day_of_week;
	public ArrayList<String> month;
	public ArrayList<Integer> week_of_month;
	public ArrayList<Integer> week_of_year;
	public ArrayList<Integer> month_of_year;
	public ArrayList<Integer> quarter_of_year;
	public ArrayList<String> half_of_year;
	/**
	 * this string is not set here, but in the addhistoricalstockquery result,
	 * in the specific function
	 * {@link AddHistoricalStockQueryResultsToDatabase#add_ohlcv_type_details(java.util.Map)}
	 * 
	 */
	public String ohlcv_type;
	public ArrayList<Integer> day_id;

	// for all of them we have to have numeric and textual.
	// we have to add day of month , week of month, month of year
	// we have to add sector for stock.
	// we have to add arraylist of possible industries in which it is involved.
	
	private ArrayList<Double> generate_float_glitch_adjusted_arraylist
	(ArrayList<Double> incoming_list){
		
		ArrayList<Double> processed_list = new ArrayList<Double>();
		
		for(Double d : incoming_list){
			Double readjusted = 
					FloatGlitch.process_indicator(d, StockConstants.name_for_historical_stock_price_adjustment);
			processed_list.add(readjusted);
		}
		
		System.out.println("unadjusted float glitched list is:" + incoming_list);
		System.out.println("now showing the adjusted float glitched list:");
		System.out.println("adjusted float glitched list is:" + processed_list);
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return processed_list;
		
	}
	
	public void fix_float_glitch(){
		
		//close
		if(getclose()!=null & !getclose().isEmpty()){
			setclose(generate_float_glitch_adjusted_arraylist(getclose()));
		}
		
		//open
		if(getOpen()!=null & !getOpen().isEmpty()){
			setOpen(generate_float_glitch_adjusted_arraylist(getOpen()));
		}
		
		//high
		if(getHigh()!=null & !getHigh().isEmpty()){
			setHigh(generate_float_glitch_adjusted_arraylist(getHigh()));
		}
		
		
		//low
		if(getLow()!=null & !getLow().isEmpty()){
			setLow(generate_float_glitch_adjusted_arraylist(getLow()));
		}
		
		
		//actual close
		if(getActual_close()!=null & !getActual_close().isEmpty()){
			setActual_close(generate_float_glitch_adjusted_arraylist(getActual_close()));
		}
		
		
	}
	
	public void adjustStockObject(Calendar start_date, Calendar end_date)
			throws IllegalArgumentException, IllegalAccessException,
			InputListSizeInsufficient {

		if (this.dates.size() > 0) {
			int i = 0;
			Integer start_index = null;
			Integer end_index = null;

			// System.out.println(this.dates.size());
			// System.out.println(new
			// CalendarFunctions(this.dates.get(0)).getFullDate());
			// System.out.println(new
			// CalendarFunctions(this.dates.get(this.dates.size()-1)).getFullDate());

			// System.out.println("this is the start date required.");
			// System.out.println(new
			// CalendarFunctions(start_date).getFullDate());
			// System.out.println("this is the end date required");
			// System.out.println(new
			// CalendarFunctions(end_date).getFullDate());

			for (Calendar cal : this.dates) {
				// System.out.println("now doing date:" + new
				// CalendarFunctions(cal).getFullDateWithoutTimeZoneOnlyDate());

				if (cal.getTimeInMillis() >= start_date.getTimeInMillis()) {
					// System.out.println("the date is greater than the start date,");

					if (start_index == null) {
						// System.out.println("start index is null, so we equate it to i:"
						// + i);
						start_index = i;
					}
				}
				if (start_index != null) {
					// means that we have already got a start index
					// so now if the
					// System.out.println("by this point, start index is not null;");
					if (cal.getTimeInMillis() >= end_date.getTimeInMillis()) {

						if (end_index == null) {
							end_index = i;
						}
					}
				}
				i++;
			}
			//this is the only case when we have a start index and we dont have an end index
			//this happens when the last date got from the scrape is less than the required last date
			
			//we ensure that the start index is not null, because in that case, it means that 
			//everything contained in the scrape was less than the start date, in that case,
			//we cannot apply this on the end date.
			if (end_index == null && start_index!=null) {
				// this means that if the last date that we have is less than
				// the
				// required date, then it becomes,
				// the last date.
				end_index = this.dates.size() - 1;
			}

			LoggingDailyOperations.get_instance().add_to_log(
					"info",
					null,
					"Historical_Stock",
					"the start and end indices of dates for scraping this stock: "
							+ this.stockName + " " + start_index
							+ " and end index " + end_index);

			if (start_index == null && end_index == null) {
				//this is the case where there was something in the scrape but 
				//it was all less than the start date itself.
				LoggingDailyOperations.get_instance()
				.add_to_log("info", null,identifier_for_log ,"everything in scrape was older than " +
						"the required start date");
				LoggingDailyOperations
				.get_instance()
				.getIndividual_stock_operations()
				.get(this.stockName + "_" + this.stockFullName + "_"
						+ this.indice)
				.setAll_things_in_scrape_less_than_start_date(true);
			}
			else{
				
				if (start_date.getTimeInMillis() == end_date.getTimeInMillis()) {
					int index = getIndexOfCalendar(start_date, this.dates);
					// System.out.println("the index is --->");
					// System.out.println(index);
					Class cls = HistoricalStock.class;
					Field fieldlist[] = cls.getDeclaredFields();

					for (Field field : fieldlist) {
						// System.out.println(field.getName());
						if (field.get(this) instanceof ArrayList<?>) {
							ArrayList<?> l = getArrayListWithSingleElement(
									(ArrayList<?>) field.get(this), index,
									field.getName());
							field.set(this, l);
						}
					}
					// System.out.println(new
					// CalendarFunctions(dates.get(this.dates.size()-1)).getFullDate());
				} else {
					// System.out.println(new
					// CalendarFunctions(this.getDates().get(2)).getFullDate());
					// System.out.println(this.dates.size());
					// System.out.println(start_index);
					// System.out.println(end_index);
					// System.out.println(this.stockFullName);
					// for(Calendar cal: this.dates){
					// System.out.println(new
					// CalendarFunctions(cal).getFullDateWithoutTimeZone());
					// }
					// System.exit(0);

					// System.out.println("this is the unique identifier");
					// System.out.println(this.stockName + "_" +
					// this.stockFullName
					// + "_" + this.indice);

					// System.out.println("this is the daily logging operations hashmap before the error.");
					// System.out.println(LoggingDailyOperations.get_instance().getIndividual_stock_operations().toString());

					System.out
							.println("this is the stock whose dates are now to be adjusted");

					System.out.println(this.stockName + "_"
							+ this.stockFullName + "_" + this.indice);

					System.out.println("this is the start and end indices:"
							+ start_index);
					System.out.println("this is the end index: " + end_index);

					LoggingDailyOperations
							.get_instance()
							.getIndividual_stock_operations()
							.get(this.stockName + "_" + this.stockFullName
									+ "_" + this.indice)
							.setDates_before_adjusting(this.dates);

					for (Calendar cal : this.dates) {
						System.out.println(new CalendarFunctions(cal)
								.getFullDateWithoutTimeZoneOnlyDate());
					}

					// System.out.println("last date before adjustment is");
					// System.out.println(new
					// CalendarFunctions(this.dates.get(this.dates.size()-1)).getFullDateWithoutTimeZoneOnlyDate());

					this.dates = (new ArrayList(this.dates.subList(start_index,
							end_index + 1)));

					// System.out.println("last date after adjustment is---");
					// System.out.println(new
					// CalendarFunctions(this.dates.get(this.dates.size()-1)).getFullDateWithoutTimeZoneOnlyDate());

					// System.out.println(new
					// CalendarFunctions(this.dates.get(this.dates.size()-1)).getFullDate());
					this.open = this.open.size() == 0 ? this.open
							: new ArrayList(this.open.subList(start_index,
									end_index + 1));
					this.high = this.high.size() == 0 ? this.high
							: new ArrayList(this.high.subList(start_index,
									end_index + 1));
					this.low = this.low.size() == 0 ? this.low : new ArrayList(
							this.low.subList(start_index, end_index + 1));
					this.close = this.close.size() == 0 ? this.close
							: new ArrayList(this.close.subList(start_index,
									end_index + 1));
					this.actual_close = this.actual_close.size() == 0 ? this.actual_close
							: new ArrayList(this.actual_close.subList(
									start_index, end_index + 1));
					this.volume = this.volume.size() == 0 ? this.volume
							: new ArrayList(this.volume.subList(start_index,
									end_index + 1));
					this.epoches = this.epoches.size() == 0 ? this.epoches
							: new ArrayList(this.epoches.subList(start_index,
									end_index + 1));
					// System.out.println("unique identifier before adjustment.");
					// System.out.println(this.unique_identifier.
					// get(this.unique_identifier.size()-1));

					this.unique_identifier = this.unique_identifier.size() == 0 ? this.unique_identifier
							: new ArrayList(this.unique_identifier.subList(
									start_index, end_index + 1));
					// System.out.println("unique identifier after adjustment.");
					// System.out.println(this.unique_identifier.
					// get(this.unique_identifier.size()-1));

					// try {
					// System.in.read();
					// } catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					// }

					this.day_of_month = this.day_of_month.size() == 0 ? this.day_of_month
							: new ArrayList(this.day_of_month.subList(
									start_index, end_index + 1));
					this.day_of_week = this.day_of_week.size() == 0 ? this.day_of_week
							: new ArrayList(this.day_of_week.subList(
									start_index, end_index + 1));
					this.month = this.month.size() == 0 ? this.month
							: new ArrayList(this.month.subList(start_index,
									end_index + 1));
					this.week_of_month = this.week_of_month.size() == 0 ? this.week_of_month
							: new ArrayList(this.week_of_month.subList(
									start_index, end_index + 1));
					this.week_of_year = this.week_of_year.size() == 0 ? this.week_of_year
							: new ArrayList(this.week_of_year.subList(
									start_index, end_index + 1));
					this.month_of_year = this.month_of_year.size() == 0 ? this.month_of_year
							: new ArrayList(this.month_of_year.subList(
									start_index, end_index + 1));
					this.quarter_of_year = this.quarter_of_year.size() == 0 ? this.quarter_of_year
							: new ArrayList(this.quarter_of_year.subList(
									start_index, end_index + 1));
					this.half_of_year = this.half_of_year.size() == 0 ? this.half_of_year
							: new ArrayList(this.half_of_year.subList(
									start_index, end_index + 1));
					this.day_id = this.day_id.size() == 0 ? this.day_id
							: new ArrayList(this.day_id.subList(start_index,
									end_index + 1));

					LoggingDailyOperations
							.get_instance()
							.getIndividual_stock_operations()
							.get(this.stockName + "_" + this.stockFullName
									+ "_" + this.indice)
							.setStock_object_got_from_api_before_insertion(this);
				}
			}
		} else {
			// TODO: LOG HERE.
			LoggingDailyOperations
					.get_instance()
					.getIndividual_stock_operations()
					.get(this.stockName + "_" + this.stockFullName + "_"
							+ this.indice)
					.setNo_data_got_from_api_for_dates(true);

		}

	}

	public ArrayList<?> getArrayListWithSingleElement(ArrayList<?> inputList,
			int index, String field) throws InputListSizeInsufficient {
		ArrayList<Object> outputList = new ArrayList<Object>();
		System.out.println("now doing field." + field);
		if (inputList.size() == 0) {
			return inputList;
		} else if (inputList.size() + 1 < index) {
			// throw error.
			String message = "this field " + field + " had a size of "
					+ inputList.size() + " belongs to ---->" + this.stockName
					+ " " + this.entityType;

			throw new InputListSizeInsufficient(message);

		} else {

			outputList.add(inputList.get(index));
			return outputList;
		}

	}

	public int getIndexOfCalendar(Calendar start_date, ArrayList<Calendar> dates) {
		Integer index = null;
		int i = 0;
		for (Calendar cal : dates) {
			if (index == null) {
				if (cal.getTimeInMillis() >= start_date.getTimeInMillis()) {
					index = i;
				}
			} else {
				break;
			}
			i++;
		}
		return index;
	}

	public String getTimezoneString() {
		return timezoneString;
	}

	public void setTimezoneString(String timezoneString) {
		this.timezoneString = timezoneString;
	}

	public ArrayList<Long> getEpoches() {
		return epoches;
	}

	public void setEpoches(ArrayList<Long> epoches) {
		this.epoches = epoches;
	}

	

	public HistoricalStock(String stockName, TimeZone tz, String entity_type,
			String indice, String stockFullName) {
		this.setStockName(stockName);
		this.setTz(tz);
		this.setTimezoneString(tz.getID());
		this.dates = new ArrayList<Calendar>();
		this.open = new ArrayList<Double>();
		this.high = new ArrayList<Double>();
		this.low = new ArrayList<Double>();
		this.actual_close = new ArrayList<Double>();
		this.volume = new ArrayList<Double>();
		this.close = new ArrayList<Double>();
		this.entityType = entity_type;
		this.epoches = new ArrayList<Long>();
		this.indice = indice;
		this.stockFullName = stockFullName;
		this.unique_identifier = new ArrayList<String>();
		this.day_of_month = new ArrayList<Integer>();
		this.day_of_week = new ArrayList<String>();
		this.month = new ArrayList<String>();
		this.week_of_month = new ArrayList<Integer>();
		this.week_of_year = new ArrayList<Integer>();
		this.month_of_year = new ArrayList<Integer>();
		this.quarter_of_year = new ArrayList<Integer>();
		this.half_of_year = new ArrayList<String>();
		this.day_id = new ArrayList<Integer>();
	}

	public ArrayList<Double> getOpen() {
		return open;
	}

	public void setOpen(ArrayList<Double> open) {
		this.open = open;
	}

	public ArrayList<Double> getHigh() {
		return high;
	}

	public void setHigh(ArrayList<Double> high) {
		this.high = high;
	}

	public ArrayList<Double> getLow() {
		return low;
	}

	public void setLow(ArrayList<Double> low) {
		this.low = low;
	}

	public ArrayList<Double> getclose() {
		return close;
	}

	public void setclose(ArrayList<Double> close) {
		this.close = close;
	}

	public ArrayList<Double> getVolume() {
		return volume;
	}

	public void setVolume(ArrayList<Double> volume) {
		this.volume = volume;
	}

	// NOW SET METHODS TO ADD AND REMOVE ELEMENTS TO THE ARRAYS.

	public void addUniqueIdentifier(String unique_identifier) {
		this.unique_identifier.add(unique_identifier);
	}

	public void addepoch(long epoch) {
		this.epoches.add(epoch);
	}

	public void addopen(Double open) {
		this.open.add(open);
	}

	public void addHigh(Double high) {
		this.high.add(high);
	}

	public void addLow(Double low) {
		this.low.add(low);
	}

	public void addVolume(Long Volume) {
		this.volume.add(Volume.doubleValue());
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public ArrayList<Calendar> getDates() {
		return dates;
	}

	public void setDates(ArrayList<Calendar> dates) {
		this.dates = dates;
	}

	/**
	 * minus the relevant time adjustment from the epoch before calculating the
	 * day id.
	 * 
	 * {"NASDAQ","DAX","CAC","FTSE","NIFTY","DME","LME","forex"};
	 * 
	 * addition done here, because they are stored as negative or positive
	 * directly so no issue regarding whether to minus or plus.
	 * 
	 * @param epoch
	 * @return
	 */
	public Integer get_number_of_days_since_epoch(Long epoch) {

		switch (getIndice()) {
		case "NASDAQ":
			epoch = epoch + StockConstants.nasdaq_time_equalisation;
			break;

		case "DAX":
		case "CAC":
			epoch = epoch + StockConstants.dax_cac_time_equalisation;
			break;

		case "FTSE":
			epoch = epoch + StockConstants.ftse_time_equalisation;
			break;

		case "NIFTY":
			epoch = epoch + StockConstants.nifty_time_equalisation;
			break;

		case "DME":
			epoch = epoch + StockConstants.oil_time_equalisation;
			break;

		case "LME":
			epoch = epoch + StockConstants.gold_silver_time_equalisation;
			break;

		case "forex":
			epoch = epoch + StockConstants.forex_time_equalisation;
			break;
		default:
			break;
		}

		double number_of_days = epoch / (86400000.0);
		return (int) number_of_days;
	}

	public void addDate(Calendar date) {
		this.dates.add(date);
		this.epoches.add(date.getTimeInMillis());
		this.day_id.add(get_number_of_days_since_epoch(date.getTimeInMillis()));
		// here enforcing unique constraint for price for each day for a given
		// stock.
		addUniqueIdentifier(this.stockFullName
				+ "_"
				+ this.stockName
				+ "_"
				+ this.indice
				+ "_"
				+ String.valueOf(new CalendarFunctions(date)
						.getFullDateWithoutTimeZone()));

		// System.out.println("added date--" +
		// this.dates.get(this.dates.size()-1));
		// System.out.println("added unique identifier--" +
		// this.unique_identifier.get(this.unique_identifier.size()-1));

		// date.setMinimalDaysInFirstWeek(4);
		// date.setFirstDayOfWeek(GregorianCalendar.MONDAY);

		this.week_of_month.add(date.get(Calendar.WEEK_OF_MONTH));
		this.week_of_year.add(date.get(Calendar.WEEK_OF_YEAR));

		DateTime d = new DateTime(date);
		this.day_of_month.add(d.getDayOfMonth());
		this.day_of_week.add(d.dayOfWeek().getAsText());
		this.month.add(d.monthOfYear().getAsText());
		if (d.getMonthOfYear() < 6) {
			this.half_of_year.add("first");
		} else {
			this.half_of_year.add("second");
		}
		this.month_of_year.add(d.getMonthOfYear());

		// quarters.
		if (d.getMonthOfYear() <= 3) {
			this.quarter_of_year.add(4);
		} else if (d.getMonthOfYear() > 3 && d.getMonthOfYear() <= 6) {
			this.quarter_of_year.add(1);
		} else if (d.getMonthOfYear() > 6 && d.getMonthOfYear() <= 9) {
			this.quarter_of_year.add(2);
		} else {
			this.quarter_of_year.add(3);
		}

	}

	public ArrayList<Double> getActual_close() {
		return actual_close;
	}

	public void setActual_close(ArrayList<Double> actual_close) {
		this.actual_close = actual_close;
	}

	public void addActual_close(Double actual_close) {
		this.actual_close.add(actual_close);
	}

	public void addClose(Double close) {
		this.close.add(close);
	}

	public TimeZone getTz() {
		return tz;
	}

	public void setTz(TimeZone tz) {
		this.tz = tz;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public String getStockFullName() {
		return stockFullName;
	}

	public void setStockFullName(String stockFullName) {
		this.stockFullName = stockFullName;
	}

	public ArrayList<String> getUnique_identifier() {
		return unique_identifier;
	}

	public void setUnique_identifier(ArrayList<String> unique_identifier) {
		this.unique_identifier = unique_identifier;
	}

	public ArrayList<Integer> getDay_of_month() {
		return day_of_month;
	}

	public void setDay_of_month(ArrayList<Integer> day_of_month) {
		this.day_of_month = day_of_month;
	}

	public ArrayList<String> getDay_of_week() {
		return day_of_week;
	}

	public void setDay_of_week(ArrayList<String> day_of_week) {
		this.day_of_week = day_of_week;
	}

	public ArrayList<String> getMonth() {
		return month;
	}

	public void setMonth(ArrayList<String> month) {
		this.month = month;
	}

	public ArrayList<Integer> getWeek_of_month() {
		return week_of_month;
	}

	public void setWeek_of_month(ArrayList<Integer> week_of_month) {
		this.week_of_month = week_of_month;
	}

	public ArrayList<Integer> getWeek_of_year() {
		return week_of_year;
	}

	public void setWeek_of_year(ArrayList<Integer> week_of_year) {
		this.week_of_year = week_of_year;
	}

	public ArrayList<Integer> getMonth_of_year() {
		return month_of_year;
	}

	public void setMonth_of_year(ArrayList<Integer> month_of_year) {
		this.month_of_year = month_of_year;
	}

	public ArrayList<Integer> getQuarter_of_year() {
		return quarter_of_year;
	}

	public void setQuarter_of_year(ArrayList<Integer> quarter_of_year) {
		this.quarter_of_year = quarter_of_year;
	}

	public ArrayList<String> getHalf_of_year() {
		return half_of_year;
	}

	public void setHalf_of_year(ArrayList<String> half_of_year) {
		this.half_of_year = half_of_year;
	}

	public String getOhlcv_type() {
		return ohlcv_type;
	}

	public void setOhlcv_type(String ohlcv_type) {
		this.ohlcv_type = ohlcv_type;
	}

	public ArrayList<Integer> getDay_id() {
		return day_id;
	}

	public void setDay_id(ArrayList<Integer> day_id) {
		this.day_id = day_id;
	}

	public String print_last_date_and_unique_identifier() {

		System.out.println(new CalendarFunctions(this.dates.get(this.dates
				.size() - 1)).getFullDateWithoutTimeZoneOnlyDate());

		System.out.println(this.unique_identifier.get(this.unique_identifier
				.size() - 1));

		return "";
	}

}
