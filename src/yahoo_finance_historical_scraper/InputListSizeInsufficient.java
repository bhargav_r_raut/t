package yahoo_finance_historical_scraper;

public class InputListSizeInsufficient extends Exception {
	public InputListSizeInsufficient(String message){
		super(message);
	}
}
