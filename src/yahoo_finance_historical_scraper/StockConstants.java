package yahoo_finance_historical_scraper;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javatuples.Triplet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import CommonUtils.CompressUncompress;
import DataProvider.RequestBase;
import ExchangeBuilder.DataPoint;
import ExchangeBuilder.Entity;
import ExchangeBuilder.TestMethod;
import Titan.ReadWriteTextToFile;
import Titan.TitanConstants;
import eu.verdelhan.ta4j.Decimal;

public abstract class StockConstants {

	/***
	 * 
	 * constants for the test cases
	 * 
	 */
	public static final String[] TEST_all_indices = { StockConstants.name_for_nasdaq, StockConstants.name_for_dax,
			StockConstants.name_for_cac, StockConstants.name_for_ftse, StockConstants.name_for_nifty,
			StockConstants.name_for_forex, StockConstants.name_for_oil };
	public static final HashMap<String, TestMethod> test_methods = new HashMap<String, TestMethod>();

	/****
	 * url to simulate connection timeouts.
	 * 
	 * 
	 * 
	 */
	public static final String timeout_simulate_url = "http://10.255.255.1/";

	/****
	 * forex and quandl request exception constants.
	 * 
	 */
	public static final String request_failed = "request_failed";
	public static final String response_code_not_200 = "response_not_ok";
	public static final String response_json_parse_exception = "response_json_parse_exception";
	public static final String forex_date_not_found_in_response = "date_not_found_in_response";
	public static final String forex_rates_not_found_in_response = "forex_rates_not_found_in_response";
	public static final String forex_symbol_not_found_in_response = "forex_symbol_not_found_in_response";
	public static final String quandl_empty_response = "quandl_empty_response";
	public static final String quandl_dataset_key_not_found_in_response = "dataset_key_not_found_in_response";
	public static final String quandl_data_key_not_found_in_dataset_key = "data_key_not_found_in_dataset_key";
	// numberformat exceptions.
	public static final String open_numberformat_exception = "open_numberformat_exception";
	public static final String high_numberformat_exception = "high_numberformat_exception";
	public static final String low_numberformat_exception = "low_numberformat_exception";
	public static final String close_numberformat_exception = "close_numberformat_exception";
	public static final String volume_numberformat_exception = "volume_numberformat_exception";
	/***
	 * 
	 * end
	 * 
	 */
	public static final String rauth_t = "aditya";
	public static final String url_http = "http";
	public static final String colon = ":";
	public static final String url_seperator = colon + "//";

	public static final String elasticsearch_local_host_name = "localhost";
	public static final String redis_local_host_name = elasticsearch_local_host_name;

	/*******
	 * 
	 * CHANGEABLE THIS TO THE REMOTE HOST NAME/IP ADDRESS.
	 * 
	 */
	// 192.168.10.5
	public static final String elasticsearch_remote_host_name = "localhost";
	public static final String redis_remote_host_name = elasticsearch_remote_host_name;

	public static final String remote_host_web_app_host_name = "localhost";
	public static final String remote_host_web_app_port = "8080";
	public static final String remote_host_web_app_root_path = "/SpringApp";
	public static final String remote_host_price_change_and_stop_loss_path = "/pc_arrays";
	public static final String remote_host_job_status_path = "/get_job_status";
	public static final String remote_host_cancel_job_path = "/cancel_job";
	public static final String remote_host_cancel_all_jobs_path = "/cancel_all_jobs";
	public static final String remote_host_semaphore_count = "/get_semaphore_count";
	public static final String remote_host_pending_jobs = "/get_pending_jobs_count";
	public static final String remote_dechunk_url = "/dechunk";
	public static final String remote_clear_singleton_url = "/clear_singleton";

	/****
	 * 
	 * ok_http_timeouts = for the local data requests. applicalbe to method
	 * 
	 * #ReadWriteTextToFile.getTextFromUrl #ReadWriteTextToFile.forexRequest
	 * 
	 */

	public static Integer ok_http_connect_timeout = 5;
	public static TimeUnit ok_http_tu = TimeUnit.SECONDS;
	public static Integer ok_http_read_timeout = 5;

	/***********
	 * CONSTANTS FOR TESTS ONLY. THESE FOLLOWING TWO VARIABLES ARE SET IN THE
	 * SETUPBEFORECLASS METHOD OF BASE TEST.
	 * 
	 * 
	 * 
	 */
	public static Boolean test = false;

	public static Boolean single_machine_tests = false;
	/**
	 * basically for forex we can request only one date at a time for any given
	 * entity. so while testing we take only the first n requests for forex.
	 * this is only done while testing. you can refer to
	 * {@link #ForexRequest#make_request} == here we take the sublist
	 */
	public static final Integer number_of_requests_to_send_per_forex_entity = 5;

	/**********
	 * 
	 * CONSTANTS FOR TESTS ENDS.
	 * 
	 * 
	 * 
	 */

	/****
	 * constants for jsch. shscript.java
	 * 
	 */

	/***
	 * 
	 * these are passed in as command line argument for start.sh for the local
	 * es server
	 * 
	 */
	public static final String es_path_local = "/home/bhargav/Desktop/elasticsearch-1.4.4";

	/***
	 * local es transport port.
	 * 
	 */
	public static final Integer es_local_transport_port = 9300;

	/***
	 * 
	 * these are passed in as command line argument for start.sh of the remote
	 * es server "/home/aditya/Downloads/elasticsearch-1.4.4"
	 * 
	 * 
	 */
	public static final String es_path_remote = "/home/bhargav/Desktop/es_for_xeon";

	/***
	 * 
	 * 
	 * 
	 * 
	 */
	public static final Integer es_remote_transport_port = 9301;

	/**
	 * the location of the start.sh , stop.sh scripts for the local server these
	 * scripts start and stop the local elasticsearch server.
	 * 
	 */
	public static final String configuration_and_scripts_path_local = "/home/bhargav/Desktop/elasticsearch-1.4.4/config/";

	/**
	 * the location of the start.sh, stop.sh, and start_r.sh script for remote
	 * server these scripts start and stop the remote es and remote redis
	 * servers. "/home/aditya/Downloads/elasticsearch-1.4.4/config/"
	 * 
	 */
	public static final String configuration_and_scripts_path_remote = "/home/bhargav/Desktop/es_for_xeon/config/";

	/**
	 * 
	 * this is passed in as a command line argument for the remote redis start
	 * script.
	 * 
	 */
	public static final String redis_path_remote = configuration_and_scripts_path_remote + "redis.conf";

	/***
	 * the location of the mapping for the index that is created both local and
	 * remote.
	 * 
	 */
	public static final String es_mapping_json_file = configuration_and_scripts_path_local
			+ "tradegenie_stats_mapping.json";

	/**
	 * the location of the private key file that has to be maintained locally to
	 * communicate with the remote server
	 */
	public static final String private_key_file = configuration_and_scripts_path_local + "id_rsa";

	/***
	 * the location of the known hosts file that has to be maintained locally to
	 * communicate with the remote server
	 * 
	 */
	public static final String known_hosts_file = configuration_and_scripts_path_local + "known_hosts";

	/**
	 * the timeout for the ssh connection to the remote server.
	 */
	public static final Integer ssh_timeout = 10000;

	/***
	 * here you can see it has three parts script_location +
	 * script_name(start.sh) + arg(remote es path)
	 * 
	 * --remote_es_path is passed in as a command line argument, so that we dont
	 * have to hard code the location of the es in the script that starts the
	 * es.
	 * 
	 */
	public static final String remote_es_start_script = configuration_and_scripts_path_remote + "start.sh "
			+ es_path_remote;

	/***
	 * here no command line args are passed because we just do pkill.
	 * 
	 */
	public static final String remote_es_stop_script = configuration_and_scripts_path_remote + "stop.sh";

	/****
	 * same as for the remote es start script. we pass in the
	 * (redis_path_remote) + redis.conf as the command line argument, this is so
	 * that we dont have to hard code the location of the conf file in the
	 * script itself.
	 */
	public static final String remote_redis_start_script = configuration_and_scripts_path_remote + "start_r.sh "
			+ redis_path_remote;

	/***
	 * we dont need any args here.
	 * 
	 */
	public static final String remote_redis_stop_script = configuration_and_scripts_path_remote + "stop_r.sh";

	/***
	 * username of the remote server.
	 */
	public static final String remote_user = "root";

	/**
	 * 
	 * remote ssh port.
	 */
	public static final Integer remote_ssh_port = 22;

	/**
	 * elasticsearch start success messages.
	 * 
	 */
	public static final String es_process_running = "process is running.";
	public static final String es_starting_process = "starting process";
	public static final String es_no_process = "no process";

	/***
	 * remote redis start and stop success messages.
	 * 
	 */
	public static final String redis_process_running = "PONG";
	public static final String redis_no_process = "successfully stopped redis";

	/***
	 * here just like for remote redis and remote es, we pass in the command
	 * line argument of the location of the es, so that we dont have to hard
	 * code it inside the starter script.
	 * 
	 * here however we dont pass in the argument together with the script,
	 * because we are not using JSCH, we are using direct java cmd way, so there
	 * in the function {@link #ShScript.do_local}, we pass in the variable
	 * #es_path_local as the third element in the array.
	 * 
	 */
	public static final String local_es_start_script = configuration_and_scripts_path_local + "start.sh";

	public static final String local_es_stop_script = configuration_and_scripts_path_local + "stop.sh";
	public static final Long local_es_bash_script_timeout = 10l;

	/****
	 * start and stop wifi scripts.
	 * 
	 */

	public static final String local_wifi_start_script = configuration_and_scripts_path_local + "start_wifi.sh";

	public static final String local_wifi_stop_script = configuration_and_scripts_path_local + "stop_wifi.sh";

	/***
	 * jsch constants end.
	 * 
	 */

	/***
	 * all the document mappings defined in the es index.
	 * 
	 */

	public static final String ESTypes_test = "test_document";
	public static final String ESTypes_test_session_id = "session_id";
	public static final String ESTypes_test_obj_stringified = "stringified_object";

	public static final String ESTypes_arrays = "arrays";
	public static final String ESTypes_arrays_start_index = "start_index";
	public static final String ESTypes_arrays_arr = "arr";
	public static final String ESTypes_arrays_arr_name = "arr_name";

	public static final String ESTypes_bs = "bs";
	public static final String ESTypes_ask = "ask";
	public static final String ESTypes_price_buckets = "price_buckets";
	public static final String ESTypes_entity = "entity";

	public static final String ESTypes_error_log = "error_log";
	public static final String ESTypes_error_log_session_id = "session_id";
	public static final String ESTypes_error_log_info_packet = "info_packet";
	public static final String ESTypes_error_log_info_packet_key = "key";
	public static final String ESTypes_error_log_info_packet_value = "value";
	public static final String ESTypes_error_log_entity_unique_name = "entity_unique_name";
	public static final String ESTypes_error_log_status = "status";
	public static final String ESTypes_error_log_exception = "exception";
	public static final String EsTypes_error_log_exception_type = "exception_type";
	public static final String EsTypes_error_log_exception_classification = "exception_classification";

	public static final HashMap<String, Integer> EsTypes_error_log_exception_levels = new HashMap<String, Integer>() {
		{
			put(StockConstants.EsTypes_error_log_exception_type_error, 4);
			put(StockConstants.EsTypes_error_log_exception_type_info, 2);
		}
	};

	public static final String EsTypes_error_log_exception_type_error = "error";
	public static final String EsTypes_error_log_exception_type_info = "info";

	/****
	 * 
	 * 
	 * this will cause a dummy request response to be generated from random
	 * data. it will only build a response that mimics the response from quandl.
	 * so not applicable to forex. {@link RequestBase#run()} so it bypasses the
	 * call to the normal dataprovider and generates a dummy response.
	 */
	public static Boolean make_dummy_request = false;

	/***
	 * if you want to run program with a custom end date. then provide how many
	 * days minus the current day, it should use as the end date. for this first
	 * test the below param to true.
	 * 
	 * this is used in {@link Entity#calculate_end_date_for_request()} it checks
	 * for this variable to be true, and if true, then minuses the said days in
	 * {@link #minus_days_from_current_datetime} from the the current date.
	 * 
	 */
	public static Boolean define_custom_end_date = false;
	public static Integer minus_days_from_current_datetime = 100;
	/***
	 * 
	 * 
	 * 
	 */
	public static final String Result_results_for_indicator = "result_for_indicator";
	public static final String Result_subindicator_name = "result_subindicator_name";
	public static final String Result_Hyphen = "-";
	/***
	 * 
	 * total shards in the es cluster.
	 * 
	 */

	public static final Integer IdAndShardResolver_total_shards = 2;

	/***
	 * day id builder constants.
	 * 
	 */
	public static final String DayIdBuilder_close_times_map_redis_key = "close_times_map";
	public static final String DayIdBuilder_query_time_day_id_lookup_map_redis_key = "query_time_day_id_lookup_map";
	public static final String DayIdBuilder_gmt_datestring_to_day_id_redis_key = "gmt_datestring_to_day_id";
	public static final String DayIdBuilder_gmt_day_id_to_data_point_redis_key = "gmt_day_id_to_data_point";
	public static final String DayIdBuilder_exchange_day_id_gmt_id = "exchange_day_id_gmt_id";

	/**
	 * central singleton constants
	 * 
	 * 
	 */
	public static final String CentralSingleton_indicators_jsonfile_redis_key_name = "indicators_jsonfile";
	public static final String CentralSingleton_exchange_constants_redis_key_name = "exchange_constants";

	/***
	 * 
	 * constants used in EntityIndex class
	 * 
	 * 
	 */
	public static final String EntityIndex_failed_to_shut_bulk_processors = "failed_to_shut_bulk_processors";
	public static final String EntityIndex_Entity_id_prefix = "E-";
	// 10 minutes.
	public static final Long EntityIndex_bulk_processor_timeout = 5l;
	public static final String EntityIndex_bulk_processor_doc_id = "doc_id";
	public static final String EntityIndex_name = "entity_index_name";
	public static final String EntityIndex_bulk_failure = "bulk_failure";

	/****
	 * 
	 * if true will build the price_change_buckets in the
	 * after_download_entity_hook
	 * 
	 * 
	 * 
	 */
	public static final Boolean EntityIndex_build_price_buckets = false;

	/****
	 * constants used in entity class.
	 * 
	 * 
	 */
	public static final Integer Entity_days_ago = 10;
	public static final String Entity_update_script_name = "update_entity";
	public static final Integer[] Entity_stop_loss_amounts = { -5, -3, -2, 2, 3, 5 };
	
	/***
	 * corresponds to 0.1%
	 */
	public static final Integer Entity_trailing_stop_loss_trigger_threshold = 1;
	
	
	public static final Integer Entity_total_pairs_for_a_day = 300;
	public static final String Entity_open_open_map_suffix = "open_open";
	public static final String Entity_close_close_map_suffix = "close_close";
	public static final String Entity_daily_close_map_suffix = "daily_close";
	public static final String Entity_daily_open_map_suffix = "daily_open";
	public static final String Entity_unique_name_placeholder = "unique_name";
	public static final String Entity_open_open_or_close_close_placeholder = "oo_cc";

	public static final String Entity_template_for_sorted_stat_stop_loss_map_name = Entity_unique_name_placeholder + "_"
			+ "nb_percent_stop_loss_" + Entity_open_open_or_close_close_placeholder;

	public static final String Entity_template_for_sorted_trailing_stop_loss_map_name = Entity_unique_name_placeholder
			+ "_" + "nb_percent_stop_loss_" + "_TSL_" + Entity_open_open_or_close_close_placeholder;

	public static final Integer PairArray_default_filler_value_for_open_open_close_close_price_change_arrays 
	= 0;

	public static final String Entity_data_point = "data_point";
	public static final String Entity_day_id = "day_id";
	public static final String Entity_forex_request_dates = "request_dates";
	
	/******
	 * 
	 * THESE CONSTANTS ARE SHARED WITH THE SPRINGAPP, CHANGING THEIR
	 * NAMES WILL HAVE IMPLICATIONS ON HOME -> REMOTE COMMUNICATION AND 
	 * THINGS MAY GET MESSED UP.
	 * 
	 */
	public static final String Entity_arr = "arr";
	public static final String Entity_arr_name = "arr_name";

	public static final String Entity_chunk_number = "chunk_number";
	public static final String Entity_unique_name = ESTypes_error_log_entity_unique_name;
	public static final String Entity_chunk_prefix = "C";
	public static final Integer Entity_chunk_size_for_close_close_and_open_open_price_change_arrays = 10000;

	public static final String Entity_first_newly_added_pair_index = "first";
	public static final String Entity_last_newly_added_pair_index = "last";
	public static final Integer CentralSingleton_total_datapoints = 3650;
	/****
	 * 
	 * ENDS SHARED CONSTANTS.
	 * 
	 * 
	 */
	
	
	public static final String Entity_last_day_of_prev_week = "last_day_of_prev_week";
	public static final String Entity_last_day_of_prev_month = "last_day_of_prev_month";

	// ///////ENTITY INDEX CONSTANTS
	public static final String EntityIndex_price_change_and_stop_loss_update_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_price_change_and_stop_loss_path;

	public static final String EntityIndex_job_status_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_job_status_path;

	public static final String EntityIndex_cancle_job_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_cancel_job_path;

	public static final String EntityIndex_cancel_all_jobs_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_cancel_all_jobs_path;

	public static final String EntityIndex_get_semaphore_count_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_semaphore_count;

	public static final String EntityIndex_get_pending_jobs_remote_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_host_pending_jobs;
	
	public static final String ChunkTest_dechunk_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_dechunk_url ;
	
	public static final String Clear_singleton_url = url_http + url_seperator
			+ remote_host_web_app_host_name + colon + remote_host_web_app_port + remote_host_web_app_root_path
			+ remote_clear_singleton_url ;

	/***
	 * constants used in datapointIndicator
	 * 
	 * 
	 */
	public static final Integer dataPointIndicator_mutliplier_for_big_decimal_conversion = 10000;
	public static final String datapointIndicator_dataPointIndicator = "dataPointIndicator";
	public static final String datapointIndicator_indicator_name = "dataPointIndicator_indicator_name";
	public static final String datapoint_price_less_than_or_equal_to_zero_template = "the PRICE : VALUE was less than or equal to zero";
	public static final String datapoint_PRICE = "PRICE";
	public static final String datapoint_VALUE = "VALUE";
	/***
	 * 
	 * constants used in datapoint.
	 * 
	 * 
	 */
	public static final RoundingMode DataPoint_bd_rounding_mode = RoundingMode.HALF_UP;
	public static final Integer DataPoint_bd_scale = 4;
	public static final String DataPoint_name = "datapoint";
	public static final Integer DataPoint_multiplicand_for_deriving_percentage_correlate = 1000;
	public static final Integer DataPoint_percentage_correlate_scale = 0;
	public static final RoundingMode DataPoint_percentage_correlate_rounding_mode = RoundingMode.DOWN;
	/***
	 * constants used in the dataprovider package classes.
	 * 
	 * 
	 * 
	 */
	public static final String dataProvider_start_date = "start_date";
	public static final String dataProvider_end_date = "end_date";
	public static final String dataProvider_dataset_code = "dataset_code";

	/***
	 * constants used in logobject.
	 * 
	 * 
	 * 
	 */
	public static final String LogObject_EntityName = "entity_name";
	public static final String LogObject_EntityIndexName = "entity_index_name";

	/***
	 * constants used in calculate indicators
	 * 
	 */

	public static final BigDecimal bd_0 = new BigDecimal("0.0000").setScale(StockConstants.DataPoint_bd_scale,
			StockConstants.DataPoint_bd_rounding_mode);

	public static final BigDecimal bd_1 = new BigDecimal(1).setScale(StockConstants.DataPoint_bd_scale,
			StockConstants.DataPoint_bd_rounding_mode);

	public static final Decimal td_0 = Decimal.valueOf(bd_0.toString());
	/***
	 * key -> exchange_name value -> download url
	 * 
	 * 
	 */
	public static LinkedHashMap<String, String> exchange_builder_constants;

	/***
	 * names for well know exchanges.
	 * 
	 * 
	 */
	public static String name_for_nasdaq;
	public static String name_for_dax;
	public static String name_for_cac;
	public static String name_for_ftse;
	public static String name_for_nifty;
	public static String name_for_forex;
	public static String name_for_metals;
	public static String name_for_oil;
	public static String default_start_date;

	/**
	 * key name for indicator_ohlcv_groups in indicator jsonstring
	 * 
	 */
	public static final String indicator_ohlcv_groups_key_name = "ohlcv_group";
	public static final String CalculateIndicators_indicator_jsonfile_periods_key_name = "time_frames";

	// /////////////////////////////////////////////////////////////////////////

	/****
	 * 
	 * forex api template.
	 * 
	 */

	public static final String date_placeholder = "***date***";
	public static final String currency_base_placeholder = "***base***";
	public static final String symbol_placeholder = "***symbol***";
	public static String forex_root_url = "http://api.fixer.io/";
	public static String forex_request_template = forex_root_url + date_placeholder + "?base="
			+ currency_base_placeholder + "&symbols=" + symbol_placeholder;

	// //////////////////////////////////////////////////////////////////////////
	/**
	 * quandl api key
	 * 
	 */
	public static final String quandl_api_key = "xEuVsY8DwkcT6mfzMHAa";

	/**
	 * 
	 * string to replace with dataset code
	 * 
	 */
	public static final String dataset_code_placeholder = "**dataset_code**";

	// date format is : YYYY-MM-DD

	/**
	 * replace this string with the start date.
	 * 
	 */
	public static final String start_date_placeholder = "**start_date**";
	/**
	 * 
	 * replace this string with the end date.
	 * 
	 */
	public static final String end_date_placeholder = "**end_date**";

	/***
	 * root url for quandl.
	 * 
	 */
	public static String quandl_root_url = "https://www.quandl.com/api/v3/datasets/";

	/***
	 * 
	 * replace the quandl dataset code
	 * 
	 */
	public static String quandl_request_template = quandl_root_url + dataset_code_placeholder + ".json?" + "start_date="
			+ start_date_placeholder + "&" + "end_date=" + end_date_placeholder + "&" + "api_key=" + quandl_api_key;

	/***
	 * 
	 * jsonobject indicator keys.
	 * 
	 * 
	 */
	public static final String not_applicable_to_object = "not_applicable_to_object";

	/***********************************************************************************************
	 * 
	 * 
	 * 
	 * 
	 * 
	 * NEW PROGRAM CONSTANTS END HERE.
	 * 
	 * 
	 * 
	 * 
	 */

	/***
	 * 
	 * constants for abnormally_large n session rise/fall
	 * 
	 * 
	 */
	public static LinkedHashMap<Integer, String> range_correlation_for_n_session_rise_fall = new LinkedHashMap<Integer, String>();

	/***
	 * 
	 * here replace range_correlate => with the actual range correlate : eg:
	 * a_massive replace n session -> sessions. rise_or_fall -> either rise or
	 * fall.
	 * 
	 */
	public static final String template_for_n_session_rise_or_fall = "subindicator_shows_range_correlate_n_session_rise_or_fall";

	public static final Integer max_map_size = 5;

	public static final Double epsilon_for_rollling_map = 0.005;

	public static final String representative_subindicator_for_price_change_percentage = "subindicator_rises_by_0.25_percent_in_1_session";

	public static final String representative_subindicator_for_massive_rise_fall = "subindicator_shows_a_massive_5_session_rise";

	/***
	 * constants for float-glitch
	 * 
	 * 
	 */
	public static final String multiplication_factor_key_in_indicators_jsonstring = "multiplication_factor_for_adjustment";
	public static final Integer price_multiple = 10000;
	/**
	 * this is built by going over all the indicators in indicators pre
	 * jsonstring and also for things like mean and standard deviation and
	 * others. it defines how much the thing should be multiplied by and whether
	 * or not it should be multiplied at all or not.
	 * 
	 */
	public static final LinkedHashMap<String, Integer> value_adjustment_rules = new LinkedHashMap<String, Integer>();

	public static final String name_for_historical_stock_price_adjustment = "raw_adjust";
	/***
	 * for the three fixed types.
	 * 
	 * 
	 */

	public static final String subindicator_value = "subindicator_value";
	public static final String subindicator_n_day_stddev = "subindicator_n_day_stddev";
	public static final String subindicator_sma_values = "subindicator_sma_values";
	public static final String subindicator_n_day_rise_fall_amounts = "subindicator_n_day_rise_fall_amounts";

	/***
	 * 
	 * 
	 * 
	 */

	public static final String above = "above";
	public static final String below = "below";
	public static final String signal_line = "signal_line";
	public static final String zero_line = "zero_line";
	public static final String goes = "goes";
	public static final String for_string = "for";
	public static final String sessions = "sessions";

	/**
	 * key holding "names" in the subindicators/preparatory_subindicators
	 * jsonstrings.
	 * 
	 */
	public static final String names = "names";

	/**
	 * 
	 * name of redis key which contains the complex_values in the redis hash for
	 * each complex.
	 * 
	 */
	public static final String complex_values = "complex_values";

	/***
	 * 
	 * redis ok response
	 * 
	 */
	public static final String redis_ok_response = "OK";

	/**
	 * 
	 * ok response translated to long
	 * 
	 * in our jedis popper, we need it to be 0l for a correct response,since the
	 * keys are updated.
	 * 
	 * so even for the hmset, we translate ok -> 0l and not ok to -> -1l
	 * 
	 */

	public static final Long jedis_popper_redis_ok_translated_to_long = 0l;
	public static final Long jedis_popper_redis_not_ok_translated_to_long = -1l;

	/***
	 * 
	 * in jedis popper -> there is an arraylist of hashmap of success / failure
	 * ojects that enter teh process resposne. these come from the initial
	 * attempt to send it into es.
	 * 
	 * each hashmap is processed. it has an item object which contains the
	 * complex name and calculated complex values.
	 * 
	 * so inside process response there are three things:
	 * 
	 * 1.subindicator_n_day_standard_Dev - 2.subindicator_sma_values
	 * 
	 * 3.simple complex changes
	 * 
	 * 
	 * after we try to write these into redis, we put back into the item a
	 * further processed thing, just for the sake of debugging in case there is
	 * an error in redis.
	 * 
	 * so for subindicator n day standard dev -> we put the stddev_and_mean
	 * maps.
	 */
	public static final String stddev_and_mean_name_in_jedis_popper_item = "stddev_and_mean";

	/**
	 * so for subindicator_sma_values -> we put just the calculated complex
	 * values
	 * 
	 * 
	 */
	public static final String sma_values_map_in_jedis_popper_item = "sma_values_map";

	/**
	 * so for subindicator n day rise of fall amounts.(for the massive small
	 * etc)
	 * 
	 * 
	 */
	public static final String n_day_rise_fall_map_in_jedis_popper_item = "n_day_rise_fall_amounts";

	/**
	 * so for simplex_complex_changes -> we put the compressed byte arrays, that
	 * are generated at the very end. to this is appended the buy/sell key name.
	 * so there are two things put into the item for this case.
	 */
	public static final String compressed_new_complex_values_in_jedis_popper_item = "compressed_new_complex_values_";

	/**
	 * is testing
	 * 
	 * set true only if we are testing the program, it is used in two places:
	 * 
	 * a.createindicatorsubindicatorpairsforeach - here it enables break, so
	 * that only the first stock that is encountered, its pairs are created.
	 * b.prepare complexes: - here it ensures that only the stockname found by
	 * parsing a redis key, and indicator referred to below are used for
	 * preparecomplexes.
	 * 
	 * this redis key parsing is done so that it picks the correct stock pair
	 * from those created in point a.
	 * 
	 * always set to false in the main program.
	 */

	/**
	 * this is the boolean that is set when generating complexes - and adding
	 * them to redis. for the first time. this should always be true in the
	 * final program.
	 */
	public static Boolean prepare_complexes = true;

	/**
	 * this boolean controls whether the update stock pair changes to es will be
	 * done or not. it is disabled while testing, so that we dont repeatedly
	 * update stock pair changes, since we always run it on the same date.
	 * 
	 * this should always be true in the final program.
	 */
	public static Boolean update_stock_pair_changes_to_es = true;

	/***
	 * this is stockName, of the stock that you want to test. this name is used
	 * only in prepare complexes
	 * 
	 * 
	 * 
	 * 
	 */

	public static final ArrayList<String> company_synonyms = new ArrayList<String>(
			Arrays.asList("companies", "stocks", "scrips"));
	public static final String all_companies_name = "all";
	/**
	 * all the sectors are added with the same score.
	 * 
	 */
	public static final String path_for_component_info = "resources/";
	public static final String path_for_component_lists = path_for_component_info + "component_lists/";
	public static final String path_for_component_pages = path_for_component_info + "component_pages/";
	public static Boolean force_download_and_store_wikipedia_company_info_pages = false;

	public static final String redis_sorted_set_containing_sector_names = "zones_sorted_set";
	public static final String wikipedia_ftse_components_page = "https://en.wikipedia.org/wiki/FTSE_100_Index";
	public static final String wikipedia_cac_components_page = "https://en.wikipedia.org/wiki/CAC_40";
	public static final String wikipedia_dax_components_page = "https://en.wikipedia.org/wiki/DAX";
	public static final String wikipedia_nasdaq_components_page = "https://en.wikipedia.org/wiki/NASDAQ-100";
	public static final String wikipedia_nifty_components_page = "https://en.wikipedia.org/wiki/CNX_Nifty";

	public static final String[] wikipedia_components_pages = { wikipedia_nasdaq_components_page,
			wikipedia_dax_components_page, wikipedia_cac_components_page, wikipedia_ftse_components_page,
			wikipedia_nifty_components_page };

	public static final String complex_values_buy_key_name = "complex_values_buy";
	public static final String complex_values_sell_key_name = "complex_values_sell";

	public static final String unchanged = "unchanged";

	public static final String name_of_script_for_updating_session_info = "update_session_info";
	public static final String name_of_script_for_updating_complex_value_self = "update_complex_value_self";
	public static final String name_of_script_for_updating_complex_value_opposite = "update_complex_value_opposite";

	public static final String path_to_elasticsearch_config = "/home/aditya/Downloads/"
			+ "elasticsearch_in_use/elasticsearch-1.4.4/config/";

	public static final String path_to_saved_queries = path_to_elasticsearch_config + "saved_queries/";

	public static final String name_of_file_holding_saved_query_for_last_successfully_session = "get_last_successfully_session.json";

	public static final String name_of_file_holding_saved_query_for_latest_error = "get_latest_error.json";

	public static final String name_of_elasticsearch_index_maker_sh_script = "make_index.sh";

	public static final String name_of_elasticsearch_index = "tradegenie_titan";

	public static final String mean_name_in_inner_map = "mean";
	public static final String standard_deviation_name_in_inner_map = "std_dev";
	public static final String subindicator_value_name_in_inner_map = "subindicator_value";

	public static final Integer[] indices_of_subindicators_which_have_to_bypass_iterate_step = { 15 };

	public static Boolean recalculate_all_indicators = false;

	public static final Integer days_considered_for_signal_line_calculation = 9;

	public static final String sorted_set_holding_the_indicator_values_history = "session_indicator_values";

	public static final Long initial_delay_for_jedis_popper_schedule_executor = 1L;
	public static final Long schedule_for_jedis_popper_schedule_executor = 1L;
	public static final TimeUnit timeunit_for_jedis_popper_schedule_executor = TimeUnit.SECONDS;
	public static final Integer total_threads_to_use_for_jedis_popper = 3;
	public static final String redis_key_name_for_jedis_poller_controller = "redis_jedis_poller_controller";
	public static final String stop_signal = "stop";
	public static final String proceed_signal = "proceed";
	public static final Long timeout_for_jedis_poller_shutdown = 10L;
	public static final TimeUnit timeunit_for_jedis_poller_shutdown = TimeUnit.SECONDS;

	public static final String redis_key_name_for_main_program_wait_controller = "redis_main_program_wait_controleler";

	public static final Integer number_of_threads_for_complex_calculation_thread_pool = Runtime.getRuntime()
			.availableProcessors() - total_threads_to_use_for_jedis_popper;

	public static final Long number_of_seconds_to_wait_for_thread_pool_executor_to_shutdown_after_complex_processing = 10l;
	public static final Integer maximum_queue_size_for_complex_calculation_thread_pool_executor = 10;

	public static final String name_to_use_when_no_periods = "period_start_null_period_end";
	public static final String name_of_file_holding_first_subindicator_from_each_group = "first_subindicator_from_each_group.json";

	public static final String name_of_field_holding_jsonarray_of_first_subindicator_in_each_group = "jsonarray";

	public static final String name_of_main_jsonarray_in_subindicator_jsonfile = "group";
	public static final String name_of_id_field_in_subindicator_group_in_subindicator_jsonfile = "id";
	public static final String name_of_jsonarray_holding_names_in_subindicator_group_in_subindicator_jsonfile = "names";
	public static final String name_of_opposite_group_field_in_subindicator_group_in_subindicator_jsonfile = "opposite_groups";

	public static final String preparatory_subindicators_jsonstring = "subindicators_preparatory_jsonString.json";

	public static final String prepartory_indicators_jsonstring = "indicators_preparatory_jsonString.json";

	public static final Integer total_number_of_cells_expected_in_stock_scraper_row = 7;
	public static final String of = "of";
	public static final String standard_deviation = "_standard_deviation";
	public static final String multiplier = "*";
	public static final String seperator = "_";
	public static final String the_average = "_the_average";
	public static final String one_standard_deviation = "_1_standard_deviation";
	public static final String subindicator = "_subindicator";
	public static final String[] rise_or_fall = { "_rises_by", "_falls_by" };
	public static final String more_than = "_more_than";
	public static final String percent = "_percent";
	public static final String in = "_in";
	public static final String session = "session";
	public static final String plural_s = "s";
	public static final String day = "day";
	public static final String week = "week";
	public static final String month = "month";
	public static final String year = "year";

	public static final String[] words_to_be_ignored_while_generating_inverted_index_for_indicators_not_applicable_to = {
			"day", "days", "week", "weeks", "month", "months", "year", "years", "session", "sessions", "quarter",
			"quarters", "rise", "fall", "rises", "falls", "period", "periods", "for", "in", "above", "below", "level",
			"high", "low", "more", "than", "less", "subindicator", "sma", "crosses", "line", "consecutive", "average",
			"standard", "deviation", "*", ".", ",", "value", "percent", "falling", "rising", "of", "shows", "w", "m",
			"closes", "opens", "up", "down", "the", "show", "goes", "W", "M" };

	public static final String[] indicators_for_which_we_offer_price_change_percentage = { "open", "high", "low",
			"close" };

	public static final Double[] rise_or_fall_amounts_for_price_change_percentage_diff = { 0.25, 0.5, 1.0, 1.5, 2.0,
			2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 25.0, 30.0, 35.0,
			40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 100.0, 200.0, 300.0, 400.0, 500.0,
			600.0 };

	public static final Double[] rise_or_fall_amounts_for_stddev_diff = { 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0,
			40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 95.0, 1.0, 1.05, 1.10, 1.15, 1.20, 1.25,
			1.30, 1.35, 1.40, 1.45, 1.50, 1.55, 1.60, 1.65, 1.70, 1.75, 1.80, 1.85, 1.90, 1.95, 2.00, 2.05, 2.10, 2.15,
			2.20, 2.25, 2.30, 2.35, 2.40, 2.45, 2.50, 2.55, 2.60, 2.65, 2.70, 2.75, 2.80, 2.85, 2.90, 2.95, 3.00, 3.50,
			4.00, 5.00, 6.00 };

	public static final String[] standard_deviation_subindicator_names = {
			"subindicator_n_day_stddev_of_change_in_1_session", "subindicator_n_day_stddev_of_change_in_2_sessions",
			"subindicator_n_day_stddev_of_change_in_3_sessions", "subindicator_n_day_stddev_of_change_in_4_sessions",
			"subindicator_n_day_stddev_of_change_in_5_sessions", "subindicator_n_day_stddev_of_change_in_6_sessions",
			"subindicator_n_day_stddev_of_change_in_1_week", "subindicator_n_day_stddev_of_change_in_10_days",
			"subindicator_n_day_stddev_of_change_in_2_weeks", "subindicator_n_day_stddev_of_change_in_3_weeks",
			"subindicator_n_day_stddev_of_change_in_1_month", "subindicator_n_day_stddev_of_change_in_6_weeks",
			"subindicator_n_day_stddev_of_change_in_2_months", "subindicator_n_day_stddev_of_change_in_3_months",
			"subindicator_n_day_stddev_of_change_in_4_months", "subindicator_n_day_stddev_of_change_in_5_months",
			"subindicator_n_day_stddev_of_change_in_6_months", "subindicator_n_day_stddev_of_change_in_7_months",
			"subindicator_n_day_stddev_of_change_in_8_months", "subindicator_n_day_stddev_of_change_in_9_months",
			"subindicator_n_day_stddev_of_change_in_10_months", "subindicator_n_day_stddev_of_change_in_11_months",
			"subindicator_n_day_stddev_of_change_in_1_year" };

	public static final Integer[] standard_deviation_subindicator_periods = { 1, 2, 3, 4, 5, 6, 7, 10, 15, 21, 31, 42,
			61, 92, 122, 152, 184, 214, 245, 275, 305, 335, 365 };

	public static final Map<String, Integer> standard_deviation_subindicator_names_and_periods = new LinkedHashMap<String, Integer>();

	public static final Map<Integer, Double> price_change_thresholds_based_on_periods = new LinkedHashMap<Integer, Double>();

	/**
	 * correlation of period in integer and words.
	 * 
	 */
	public static final Map<Integer, String> period_in_integer_period_in_words_map = new LinkedHashMap<Integer, String>();

	public static final LinkedHashMap<String, Integer> first_subindicator_from_each_group_with_index = new LinkedHashMap<String, Integer>();

	public static final LinkedHashMap<String, ArrayList<String>> indice_and_country = new LinkedHashMap<String, ArrayList<String>>();

	public static ArrayList<String> subindicators_which_have_to_bypass_the_iterate_step = new ArrayList<String>();

	public static final Double[] standard_deviation_rise_and_fall_amounts_greater_than_average = { 1.0, 1.05, 1.10,
			1.15, 1.20, 1.25, 1.30, 1.35, 1.40, 1.45, 1.50, 1.55, 1.60, 1.65, 1.70, 1.75, 1.80, 1.85, 1.90, 1.95, 2.00,
			2.05, 2.10, 2.15, 2.20, 2.25, 2.30, 2.35, 2.40, 2.45, 2.50, 2.55, 2.60, 2.65, 2.70, 2.75, 2.80, 2.85, 2.90,
			2.95, 3.00, 3.50, 4.00, 5.00, 6.00 };

	public static final Integer[] periods_for_sma_calculations = { 1, 5, 9, 12, 26, 50, 100, 150, 200, 250 };
	public static final Double[] periods_for_sma_calculations_as_doubles = { 1.0, 5.0, 9.0, 12.0, 26.0, 50.0, 100.0,
			150.0, 200.0, 250.0 };

	public static final Double[] standard_deviation_rise_and_fall_amounts_less_than_average = { 0.0, 5.0, 10.0, 15.0,
			20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 95.0 };

	public static final Double[] subindicator_above_or_below_signal_line_amounts = { 0.25, 0.5, 0.75, 1.25, 1.5, 1.75,
			2.00, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0 };

	protected static Double find_nearest(Double search, Double[] arr) {

		Double closest = null;
		for (Double item : arr) {
			if ((closest == null) || (Math.abs(search - closest) > Math.abs(item - search))) {
				closest = item;
			}
		}

		return closest;
	}

	public static final String[] indices_main = { "NASDAQ", "DAX", "CAC", "FTSE", "NIFTY", "DME", "LME", "forex" };

	public static ArrayList<Pattern> patterns_array = new ArrayList<Pattern>();

	public static final HashMap<String, String> up_or_down_and_corresponding_pattern = new HashMap<String, String>() {
		{
			put("up", "1");
			put("up_", "1");
			put("down", "2");
			put("down_", "2");
		}
	};

	/**
	 * for the n_day_high_low. periods.
	 */
	public static final ArrayList<Double> high_low_periods = new ArrayList<Double>();

	/**
	 * for the : subindicator_rises_for_2_sessions for the :
	 * subindicator_shows_up_up_pattern -> this map holds the subindicator_naem
	 * -> corresponding 112/111 etc patterns
	 * 
	 */
	public static LinkedHashMap<String, String> subindicator_name_and_corresponding_pattern = new LinkedHashMap<String, String>();

	public static String string_to_test_if_indicator_supports_zero_line;
	public static String string_to_test_if_indicator_supports_signal_line;

	/**
	 * template for subindicator_above/below_n_session_sma_for_n_sessions
	 * 
	 * 
	 */
	public static final String template_subindicator_above_n_session_sma_for_d_sessions = "subindicator_above_n_session_sma_for";
	public static final String template_subindicator_below_n_session_sma_for_d_sessions = "subindicator_below_n_session_sma_for";

	/**
	 * template for subindicator above/below zero line.
	 * 
	 * 
	 * 
	 */
	public static final String template_subindicator_above_zero_line_for_d_sessions = "subindicator_above_zero_line_for";
	public static final String template_subindicator_below_zero_line_for_d_sessions = "subindicator_below_zero_line_for";

	/**
	 * ns -> smaller period nl -> larger period
	 * 
	 */
	public static final String template_for_smaller_sma_cross_above_larger = "subindicator_ns_session_sma_crosses_above_subindicator_nl_session_sma";

	public static final String template_for_larger_sma_cross_above_smaller = "subindicator_nl_session_sma_crosses_above_subindicator_ns_session_sma";

	public static final String template_for_single_day_cross_above = "subindicator_crosses_above_nl_session_sma";

	public static final String template_for_single_day_crossed_by_higher_day = "subindicator_nl_session_sma_crosses_above_subindicator";

	/**
	 * now generating the line intersects basically in order to understand
	 * whether the 5 day crosses the 9 day and so on.
	 * 
	 * we create line slopes.
	 * 
	 * for rank : total count of sma_periods for rank : total count of sma
	 * periods combine and store in hashmap -> String, Pair
	 * <start_Integer,end_Integer> store in two hashmaps - which one is it
	 * above. end end
	 * 
	 * so we have to know whether a particular start_end pair has what all above
	 * or below it. so suppose we save the pair of the ranks, on each day id,
	 * for each period.
	 * 
	 * 
	 * 
	 * 
	 */

	public static HashMap<String, Triplet<Integer, Integer, Integer>> sma_cross_detection_string_pairs = new LinkedHashMap<String, Triplet<Integer, Integer, Integer>>();
	/***
	 * not used anywhere in the main program, this is an internal variable, used
	 * in the generation of the actual crosses above what rank combinations.
	 * 
	 */
	public static HashMap<String, ArrayList<Integer>> which_rank_pattern_crosses_above_which = new LinkedHashMap<String, ArrayList<Integer>>();

	/**
	 * also not used anywhere in the main program, just used for debugging.
	 * 
	 */
	public static HashMap<String, ArrayList<String>> which_rank_pattern_crosses_above_which_as_string = new LinkedHashMap<String, ArrayList<String>>();

	/***
	 * main thing used in the program.
	 * 
	 */
	public static HashMap<String, int[]> rank_combination_and_array_of_ranks_which_cross_above_it = new LinkedHashMap<String, int[]>();

	/***
	 * internal not used anywhere. in main progarm.
	 * 
	 */
	private static TreeMap<Integer, Integer> convenience_treemap = new TreeMap<Integer, Integer>();

	/**
	 * 
	 * 
	 * 
	 */
	public static HashMap<Integer, String> cross_detection_string_pairs_for_binary_search_integer_as_key = new LinkedHashMap<Integer, String>();

	/**
	 * 
	 * 
	 */
	public static HashMap<String, Integer> cross_detection_string_pairs_for_binary_search_string_as_key = new LinkedHashMap<String, Integer>();

	private static int cross_detection_string_pairs_start_int = 0;

	static {

		/***
		 * 
		 * building the range correlation map.
		 * 
		 * so for example:
		 * 
		 * greater than 85 percent of values -> massive greater than 70 percent
		 * of values -> very large greater than 55 percent of values -> large
		 * greater than 40 percent of values -> moderate greater than 25 percent
		 * of values -> mild greater than 10 percent of values -> meagre greater
		 * than or lowest value -> infinitesimally small
		 * 
		 */

		range_correlation_for_n_session_rise_fall.put(85, "a_massive");
		range_correlation_for_n_session_rise_fall.put(70, "a_very_large");
		range_correlation_for_n_session_rise_fall.put(55, "a_large");
		range_correlation_for_n_session_rise_fall.put(40, "a_moderate");
		range_correlation_for_n_session_rise_fall.put(25, "a_mild");
		range_correlation_for_n_session_rise_fall.put(10, "a_meagre");
		range_correlation_for_n_session_rise_fall.put(0, "an_infinitesimally_small");

		/***
		 * 
		 * building the map of how to modify the values.
		 * 
		 * 
		 */
		try {
			org.json.JSONObject indicators_jsonstring = new org.json.JSONObject(ReadWriteTextToFile.getInstance()
					.readFile(StockConstants.prepartory_indicators_jsonstring, TitanConstants.ENCODING));

			Iterator<String> indicator_names = indicators_jsonstring.keys();
			while (indicator_names.hasNext()) {
				String indicator_name = indicator_names.next();
				// now we have to decide what to do with each indicator.
				// best would be to put it into the jsonstring itself.
				// simply.
				JSONObject indicator_info = indicators_jsonstring.getJSONObject(indicator_name);
				Integer multiplication_factor_for_indicator = indicator_info
						.getInt(StockConstants.multiplication_factor_key_in_indicators_jsonstring);
				value_adjustment_rules.put(indicator_name, multiplication_factor_for_indicator);

			}
			/***
			 * basically for adjusting open,high,low,close,volume we multiply by
			 * 10000 this is done during the
			 * addhistoricalstockqueryresultotdatabase
			 * 
			 * 
			 * afterwards in calculateindicators all the indicators are again
			 * passed into floatglitch. at this time however,
			 * open,high,low,close,volume indicator are not modified, because
			 * obviously they have already been modified while downloading.
			 * 
			 */
			value_adjustment_rules.put(StockConstants.name_for_historical_stock_price_adjustment, 10000);

		} catch (JSONException | IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		/**
		 * populating sma cross detection map.
		 * 
		 * so this is the rank thing.
		 * 
		 */
		Integer outer_index = 0;
		for (Integer i : periods_for_sma_calculations) {
			Integer inner_index = 0;
			for (Integer q : periods_for_sma_calculations) {

				String combination = outer_index.toString() + "_" + inner_index.toString();
				sma_cross_detection_string_pairs.put(combination, new Triplet<Integer, Integer, Integer>(outer_index,
						inner_index, cross_detection_string_pairs_start_int));

				/***
				 * we keep two copies of this, one with integer as key and one
				 * with string as key. both are needed.
				 */
				cross_detection_string_pairs_for_binary_search_integer_as_key
						.put(cross_detection_string_pairs_start_int, combination);
				cross_detection_string_pairs_for_binary_search_string_as_key.put(combination,
						cross_detection_string_pairs_start_int);

				cross_detection_string_pairs_start_int++;
				inner_index++;
			}
			convenience_treemap.put(outer_index, outer_index);
			outer_index++;
		}

		for (Map.Entry<String, Triplet<Integer, Integer, Integer>> entry : sma_cross_detection_string_pairs
				.entrySet()) {

			String pair_combo = entry.getKey();
			Triplet<Integer, Integer, Integer> start_and_end = entry.getValue();

			// to get what passes above it, get everything from the array that
			// starts greater than start,
			// and ends
			// less than the end

			// so first get a navigable headmap, and tailmap, then we create
			// pairs out of them, and
			// we
			Integer start = start_and_end.getValue0();
			Integer end = start_and_end.getValue1();

			// start -> get tailmap -> inclusive
			// these become start keys.
			Set<Integer> start_keys = convenience_treemap.tailMap(start, false).keySet();

			// end -> get headmap -> inclusive
			// these become end keys.
			Set<Integer> end_keys = convenience_treemap.headMap(end, false).keySet();

			which_rank_pattern_crosses_above_which.put(pair_combo, new ArrayList<Integer>());
			which_rank_pattern_crosses_above_which_as_string.put(pair_combo, new ArrayList<String>());
			for (Integer start_key : start_keys) {
				for (Integer end_key : end_keys) {
					which_rank_pattern_crosses_above_which.get(pair_combo)
							.add(cross_detection_string_pairs_for_binary_search_string_as_key
									.get(start_key + "_" + end_key));

					which_rank_pattern_crosses_above_which_as_string.get(pair_combo).add(start_key + "_" + end_key);

				}
			}
		}

		/***
		 * just converting above thing from arraylist of integers to int array.
		 * 
		 */
		for (Map.Entry<String, ArrayList<Integer>> entry : which_rank_pattern_crosses_above_which.entrySet()) {
			rank_combination_and_array_of_ranks_which_cross_above_it.put(entry.getKey(),
					CompressUncompress.convert_integer_list_to_int_array(entry.getValue()));
		}

		/***
		 * creating the patterns for the list search eg: up up down ---> 1,1,-1
		 * eg: subindicator rises for 2 sessions --> 1,1 basically we read the
		 * subindicators_preparatory_json_string, and then we derive these
		 * patterns from it.
		 * 
		 */

		patterns_array.add(Pattern.compile("rises_for_(\\d+)_sessions"));
		patterns_array.add(Pattern.compile("falls_for_(\\d+)_sessions"));
		patterns_array.add(Pattern.compile("shows_([a-z]+)_([a-z]+)_([a-z]+_)?pattern"));
		patterns_array.add(Pattern.compile("_(\\d+)_sessions?_high"));
		patterns_array.add(Pattern.compile("(?<signal>signal)|(?<zero>zero)"));

		try {
			org.json.JSONObject subindicators_preparatory_jsonstring = new org.json.JSONObject(
					ReadWriteTextToFile.getInstance().readFile(StockConstants.preparatory_subindicators_jsonstring,
							TitanConstants.ENCODING));
			// now the subindicators preparatory jsonstring is to be iteratoed
			JSONArray jsonlist_of_preparatory_subindicator_groups = subindicators_preparatory_jsonstring
					.getJSONArray(StockConstants.name_of_main_jsonarray_in_subindicator_jsonfile);

			for (int i = 0; i < jsonlist_of_preparatory_subindicator_groups.length(); i++) {
				org.json.JSONObject group = jsonlist_of_preparatory_subindicator_groups.getJSONObject(i);
				org.json.JSONArray names = group.getJSONArray("names");
				// System.out.println("names were: " + names);

				Integer patterns_counter = 0;
				for (Pattern p : patterns_array) {
					// System.out.println("pattern was: " + p.toString());
					for (int k = 0; k < names.length(); k++) {
						Matcher m = p.matcher(names.getString(k));
						if (m.find()) {
							if (patterns_counter == 0) {
								Integer rises_for_days = Integer.parseInt(m.group(1));
								String search_pattern_rises = "";

								for (int i1 = 0; i1 < rises_for_days; i1++) {
									search_pattern_rises = search_pattern_rises + "1";

								}
								subindicator_name_and_corresponding_pattern.put(names.getString(k),
										search_pattern_rises);

							} else if (patterns_counter == 1) {
								Integer falls_for_days = Integer.parseInt(m.group(1));
								String search_pattern_falls = "";

								for (int i1 = 0; i1 < falls_for_days; i1++) {
									search_pattern_falls = search_pattern_falls + "2";

								}
								subindicator_name_and_corresponding_pattern.put(names.getString(k),
										search_pattern_falls);

							} else if (patterns_counter == 2) {
								// System.out.println("came to pattern counter
								// 2");
								// System.out.println("the name is:" +
								// names.getString(k));
								String pattern_for_subindicator_search = "";

								String first_pattern_match = m.group(1);
								pattern_for_subindicator_search = pattern_for_subindicator_search
										+ up_or_down_and_corresponding_pattern.get(first_pattern_match);

								String second_pattern_match = m.group(2);
								pattern_for_subindicator_search = pattern_for_subindicator_search
										+ up_or_down_and_corresponding_pattern.get(second_pattern_match);

								String third_pattern_match = m.group(3);
								if (third_pattern_match == null) {

								} else {
									pattern_for_subindicator_search = pattern_for_subindicator_search
											+ up_or_down_and_corresponding_pattern.get(third_pattern_match);
								}

								subindicator_name_and_corresponding_pattern.put(names.getString(k),
										pattern_for_subindicator_search);

							} else if (patterns_counter == 3) {
								String first_pattern_match = m.group(1);
								high_low_periods.add(Double.parseDouble(first_pattern_match));
							} else if (patterns_counter == 4) {
								// this is the for the signal line zero line
								// thing.
								// so it will always be the last available
								// variable.
								String signal_line_match = m.group("signal");
								String zero_line_match = m.group("zero");
								if (signal_line_match != null) {
									// so its a signal line.
									string_to_test_if_indicator_supports_signal_line = names.getString(k);
								} else {
									string_to_test_if_indicator_supports_zero_line = names.getString(k);
								}
							}

						}
					}
					patterns_counter++;
				}
			}

		} catch (JSONException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/**
		 * creating the indice and country names hashmap.
		 * 
		 * 
		 */
		for (String indice : indices_main) {
			if (indice.equals("NASDAQ")) {
				indice_and_country.put(indice, new ArrayList<String>(Arrays.asList("US", "American")));
			} else if (indice.equals("DAX")) {
				indice_and_country.put(indice, new ArrayList<String>(Arrays.asList("German",

						"Germany")));
			} else if (indice.equals("CAC")) {
				indice_and_country.put(indice, new ArrayList<String>(Arrays.asList("French", "France")));
			} else if (indice.equals("FTSE")) {
				indice_and_country.put(indice, new ArrayList<String>(Arrays.asList("British", "UK", "English")));
			} else if (indice.equals("NIFTY")) {
				indice_and_country.put(indice, new ArrayList<String>(Arrays.asList("India", "Indian")));
			}
		}

		/**
		 * creating the two seperate arrays for the finding of nearest for rise
		 * or fall amounts in standard deviation
		 * 
		 */

		/***
		 * reading in the first subindicator from each group. from the json file
		 * which contains them.
		 * 
		 * we also populate the subindicators which have to bypass the
		 * 
		 */
		try {
			org.json.JSONObject first_subindicator_from_each_group_read = new org.json.JSONObject(ReadWriteTextToFile
					.getInstance().readFile(StockConstants.name_of_file_holding_first_subindicator_from_each_group,
							TitanConstants.ENCODING));

			JSONArray list = first_subindicator_from_each_group_read
					.getJSONArray(StockConstants.name_of_field_holding_jsonarray_of_first_subindicator_in_each_group);

			for (int i = 0; i < list.length(); i++) {
				// if (Arrays
				// .binarySearch(
				// indices_of_subindicators_which_have_to_bypass_iterate_step,
				// i) >= 0) {
				// subindicators_which_have_to_bypass_the_iterate_step
				// .add(list.getString(i));
				// }
				first_subindicator_from_each_group_with_index.put(list.getString(i), i);
			}

		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * first create a hashmap of the standard deviation subindicator names
		 * and periods defined above. this is used in the ComplexCalculate
		 * everywhere, so that there are no mismatch issues
		 * 
		 */
		Arrays.sort(standard_deviation_subindicator_periods);

		if (standard_deviation_subindicator_periods.length != standard_deviation_subindicator_names.length) {
			try {
				throw new Exception("there is a mismatch between periods and names of standard deviations while"
						+ "createing static map");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Integer counter = 0;
			Pattern p = Pattern.compile("_\\d+_(sessions?|days|weeks?|months?|year)");
			for (String s : standard_deviation_subindicator_names) {
				standard_deviation_subindicator_names_and_periods.put(s,
						standard_deviation_subindicator_periods[counter]);

				Matcher m = p.matcher(s);

				if (m.find()) {
					period_in_integer_period_in_words_map.put(standard_deviation_subindicator_periods[counter],
							m.group(0));
				} else {
					try {
						throw new Exception("a match was not found while matching the regex for creating"
								+ "the map of the periods and their string equivalents.");
					} catch (Exception e) {

					}
				}

				/**
				 * we are basically now defining the threshold of what is the
				 * max percentage we allow for a given period, this is for the
				 * daily price change.
				 * 
				 */
				Integer stddev_sub_period = standard_deviation_subindicator_periods[counter];
				if (stddev_sub_period <= 2) {
					price_change_thresholds_based_on_periods.put(stddev_sub_period, 6.0);
				}
				/**
				 * in 30 days days max rise or fall assumed to be 60 percent.
				 */
				else if (stddev_sub_period <= 31) {

					price_change_thresholds_based_on_periods.put(stddev_sub_period,
							find_nearest(stddev_sub_period * 2.0,
									StockConstants.rise_or_fall_amounts_for_price_change_percentage_diff));
				}
				/**
				 * for everything else its 1.5 times.
				 * 
				 */
				else if (stddev_sub_period <= 365) {
					price_change_thresholds_based_on_periods.put(stddev_sub_period,
							find_nearest(stddev_sub_period * 1.5,
									StockConstants.rise_or_fall_amounts_for_price_change_percentage_diff));
				}

				counter++;
			}

		}

	}

	public static final String name_of_sh_file_to_run_at_end_of_setup = "at_end_of_setup.sh";
	public static final String name_of_sh_file_to_run_at_start_of_test_prepare_complexes = "at_start_of_test.sh";

	public static final Integer minimum_day_id_from_which_all_lists_are_made = 16503;
	public static final String elasticsearch_mapping_name_for_historical_data_point = "historical_data_point";
	public static final Integer days_before_actual_minus_amount = 730;
	public static final int total_days_to_consider_for_standard_deviation = 10;
	public static final int max_size_of_redis_complex_values_arrays = 3000;

	public static final String name_of_redis_list_for_complex_objects = "redis_list_for_complex_objects";
	public static final Long minimum_length_of_redis_list_for_pop = 100l;
	public static final Long total_pairs_that_should_exist_in_es = 1091948l;
	public static final String elasticsearch_mapping_name_for_stock_complex = "subindicator_indicator_stock_complex";
	public static final Integer total_records_to_retrieve_while_adding_ohlcv_values_to_redis_from_es = 2000;
	public static final Integer total_pairs_for_any_given_days = 300;

	public static final String name_of_redis_channel_for_historical_stock_es_docs = "historical_stock_es_docs";
	public static final Integer total_common_day_id_vertices_expected_in_graph = 4748;
	public static final Integer total_days_to_connect_by_day_connector = 60;
	public static final String name_of_redis_channel_for_es_documents = "es_documents";
	public static final String base_url = "https://au.finance.yahoo.com/q/cp?s=%5E";
	public static final String base_url_for_company_details = "https://au.finance.yahoo.com/q/pr?s=";
	public static final String url_for_nifty_component_csv = "http://www.nseindia.com/content/indices/ind_niftylist.csv";
	public static final String base_url_for_historical_data_download = "http://finance.yahoo.com";
	public static final String middle_url_for_historical_data_download = "/q/hp?s=";
	public static final String end_url_for_historical_data_download = "&g=d";

	public static final String url_for_verifying_nifty_components_start = "http://finance.yahoo.com/q/hp?s=";
	public static final String url_for_veriftying_nifty_components_ends = "+Historical+Prices";

	public static final String[] indices = { "NDX", "GDAXI", "FCHI", "FTSE", "NSEI", "DME", "LME", "forex" };
	public static final String[] alphabet_array = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	public static final String alphabet_piece = "&c=";

	public static final String[] entity_type = { "stock", "stock", "stock", "stock", "stock", "oil", "metal", "forex" };
	public static final String OIL_STOCK_NAME = "oil";
	public static final String GOLD_STOCK_NAME = "gold";
	public static final String SILVER_STOCK_NAME = "silver";
	public static final Double PRICE_ABERRATION_THRESHOLD_IN_PERCENTAGE = 15.00;
	/*
	 * public static final String[] default_start_dates = {"2001-1-1 15:00:00",
	 * "2000-1-1 15:00:00", "2000-1-1 15:00:00", "2000-1-1 15:00:00",
	 * "2000-1-1 15:00:00", "2007-1-8 13:30:00", "2001-9-19 15:00:00",
	 * "1999-1-4 15:00:00"};
	 */
	// pattern below is yyyy-mm-dd
	// if month is single digit then it is yyyy-m-dd
	// same if day is single digit: then it is: yyyy-m-d
	public static final String[] default_start_dates = { "2015-3-10 15:00:00", "2015-3-10 15:00:00",
			"2015-3-10 15:00:00", "2015-3-10 15:00:00", "2015-3-10 15:00:00", "2015-3-10 15:00:00",
			"2015-3-10 15:00:00", "2015-3-10 15:00:00" };
	public static final String[] default_timezoneStrings = { "America/New_York", "Europe/Berlin", "Europe/Paris",
			"Europe/London", "Asia/Calcutta", "CST6CDT", "Europe/London", "Europe/Berlin" };
	public static final boolean use_transactional_graph = false;
	public static final String daily_subindicators_json_string_tableName = "daily_subindicators_json_string";
	public static final String daily_indicators_json_string_tableName = "daily_indicators_json_string";
	public static final String subindicators_jsontextfile = "subindicators_jsonstring.json";
	public static final String indicators_jsontextfile = "indicators_jsonString.json";
	public static final String subindicators_jsonDatabasefile = "subindicators_jsonDatabaseFile.json";
	public static final String indicators_jsonDatabasefile = "indicators_jsonString.json";
	/**
	 * New York (U.S.A. - New York) Thursday, 1 January 1970, 16:00:00 EST UTC-5
	 * hours UTC (Time Zone) Thursday, 1 January 1970, 21:00:00 UTC
	 * 
	 * 
	 * so new york starts 21 hours after of the actual epoch. so we have to
	 * minus 21 hours from the total milliseconds since the unix epoch. 21 *
	 * 3600 * 1000
	 */
	public static Long nasdaq_time_equalisation = -75600000L;

	/**
	 * 
	 * Frankfurt (Germany - Hesse) Thursday, 1 January 1970, 17:30:00 CET UTC+1
	 * hour UTC (Time Zone) Thursday, 1 January 1970, 16:30:00 UTC
	 * 
	 * frankfurt starts 16 and a half hours after the actual epoch. so minus
	 * that amount.
	 * 
	 * Same for paris.
	 * 
	 * 16 * 3600 * 1000 + 1800*1000
	 */
	public static Long dax_cac_time_equalisation = -59400000L;

	/**
	 * 
	 * england London (United Kingdom - England) Thursday, 1 January 1970,
	 * 16:30:00 BST UTC+1 hour UTC (Time Zone) Thursday, 1 January 1970,
	 * 15:30:00 UTC
	 * 
	 * so 15 hours and 30 minutes are minused.
	 * 
	 */

	public static Long ftse_time_equalisation = -55800000L;

	/**
	 * nifty.
	 * 
	 * Pune (India - Maharashtra) Thursday, 1 January 1970, 16:00:00 IST
	 * UTC+5:30 hours UTC (Time Zone) Thursday, 1 January 1970, 10:30:00 UTC
	 * 
	 */

	public static Long nifty_time_equalisation = -37800000L;

	/**
	 * oil
	 * 
	 * Dallas (U.S.A. - Texas) Thursday, 1 January 1970, 13:30:00 CST UTC-6
	 * hours UTC (Time Zone) Thursday, 1 January 1970, 19:30:00 UTC
	 * 
	 */

	public static Long oil_time_equalisation = -70200000L;

	/**
	 * 
	 * gold silver time equalisation. London (United Kingdom - England)
	 * Thursday, 1 January 1970, 15:00:00 BST UTC+1 hour UTC (Time Zone)
	 * Thursday, 1 January 1970, 14:00:00 UTC
	 * 
	 * 
	 * 
	 * 
	 */
	public static Long gold_silver_time_equalisation = -50400000L;

	/***
	 * forex time
	 * 
	 * Berlin (Germany - Berlin) Thursday, 1 January 1970, 15:00:00 CET UTC+1
	 * hour UTC (Time Zone) Thursday, 1 January 1970, 14:00:00 UTC
	 * 
	 * 
	 */
	public static Long forex_time_equalisation = -50400000L;

}
