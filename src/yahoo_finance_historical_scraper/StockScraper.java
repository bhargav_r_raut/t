package yahoo_finance_historical_scraper;


import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import singletonClass.CalendarFunctions;

import metals.MetalsScraper;
import dailyDownload.QueryResult;
import forex.HistoricalQuery;

public class StockScraper extends MetalsScraper{

	@Override
	public HistoricalQuery generateHistoricalQuery(ArrayList<Object> queryString) {
		// TODO Auto-generated method stub
		return super.generateHistoricalQuery(queryString);
	}

	public String getCloseTime(String indice){
		String close_time = "";
		
		switch (indice) {
		
		case "NASDAQ":
				close_time = "16:00:00";
			break;
			
		case "DAX":
				close_time = "17:30:00";
			break;
			
			
		case "CAC":
				close_time = "17:30:00";
			break;
			
			
		case "FTSE":
				close_time = "16:30:00";
			break;
			
		
		case "NIFTY":
				close_time = "16:00:00";
			break;
			
		default:
			
			break;
		
		}
		
		return close_time;
	}
	
	
	@Override
	public QueryResult scrapeMetal(ArrayList<Object> queryString)
			throws IllegalArgumentException, IllegalAccessException,
			InputListSizeInsufficient {
		// TODO Auto-generated method stub
		
		QueryResult qr = new QueryResult();
		HistoricalQuery query = generateHistoricalQuery(queryString);
		HashMap<String, HistoricalStock> hm = new HashMap<String, HistoricalStock>();

		//System.out.println("this the generated query.");
		//System.out.println(query.getForex_names() + "\n"
		//		+ query.getStart_dates() + "\n" + query.getEnd_dates());
		Integer counter = 0;
		for(String forex_name : query.getForex_names()){
			System.out.println("stock name:"
		+ forex_name + " start date:"
					+ new CalendarFunctions
					(query.getStart_dates().get(counter)).getFullDateWithoutTimeZoneOnlyDate()
		+ " end date:" + new CalendarFunctions
		(query.getEnd_dates().get(counter)).getFullDateWithoutTimeZoneOnlyDate());
			
			counter++;
		}
		
		//try {
		//	System.in.read();
		//} catch (IOException e1) {
			// TODO Auto-generated catch block
		//	e1.printStackTrace();
		//}
		
		int i = 0;
		for(String stockName:query.getForex_names()){
			
			Calendar startDateMain = query.getStart_dates().get(i);
			Calendar endDateMain = query.getEnd_dates().get(i);
			
			HistoricalQouteGetter qg = new HistoricalQouteGetter(startDateMain,
					endDateMain, 
					stockName,
					startDateMain.getTimeZone(), "STOCK",getCloseTime
					(query.getIndice_names().get(i)), query.getIndice_names().get(i),
					query.getStockFullName().get(i));
			
			try {
				qg.populateHistoricalStock();
				hm.put(qg.getHistoricalStock().getStockName(),qg.getHistoricalStock());
			} catch (IOException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//in this case we have to say nothing came back.	
			}
			
			
			
			i++;
		}
		
		qr.setHm(hm);
		return qr;
		//return super.scrapeMetal(queryString);
	}
	
	

	
}
