package Titan

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder
import static org.elasticsearch.index.query.FilterBuilders.*
import static org.elasticsearch.index.query.QueryBuilders.*
import genericIndicatorFunctions.SubIndicator

import java.util.concurrent.atomic.AtomicInteger

import org.apache.commons.codec.digest.DigestUtils
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.SerializationConfig.Feature
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.common.xcontent.XContentBuilder
import org.elasticsearch.search.SearchHit
import org.javatuples.Pair
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import redis.clients.jedis.Jedis
import yahoo_finance_historical_scraper.CommonDayConnectorEdge
import yahoo_finance_historical_scraper.DayConnectorEdge
import yahoo_finance_historical_scraper.HistoricalStock
import yahoo_finance_historical_scraper.StockConstants
import DailyOperationsLogging.LoggingDailyOperations
import DailyOperationsLogging.StockOperations
import ExchangeBuilder.DataPoint
import Indicators.ComplexCalculationRequest
import Indicators.RedisManager

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.thinkaurelius.titan.core.Titan
import com.thinkaurelius.titan.core.schema.TitanManagement
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.TransactionalGraph
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.gremlin.Tokens.*
import com.tinkerpop.gremlin.groovy.Gremlin

import dailyDownload.CreateQueryStringFromDbFields
import elasticSearchNative.ElasticClientHolder

public class GroovyStatements {

	public static GroovyStatements instance;
	private static Graph graph;
	private static TitanManagement mgmt;
	
	public static HashMap<String, int[]> cmap;
	
	public static HashMap<String,Pair<Integer,String>> registry_hash;
	private static AtomicInteger central_counter;
	
	/***
	 * only these four things are used.
	 * 
	 */
	private static final String identifier_for_log = "GroovyStatements";
	public static  Gson gson;
	public static ObjectMapper objmapper;
	public static org.json.JSONObject indicators_jsonfile;
	public static Long session_start_time;
	
	public static TreeMap<Integer, DataPoint> gmt_day_id_to_dataPoint;
	
	public static LinkedHashMap<String, Integer> gmt_datestring_to_day_id;
	
	public static HashMap<String, Integer> query_time_lookup_map;
	
	
	
	
	/**
	 * 
	 * ends.
	 */
	
	
	static {
		Gremlin.load();
	}
	
	public static void set_indicators_jsonfile(){
		try {
			org.json.JSONObject indicators_jsonfile_read = new org.json.JSONObject(
					ReadWriteTextToFile.getInstance().readFile(
							StockConstants.indicators_jsontextfile,
							TitanConstants.ENCODING));
			
			Iterator<String> indicators = indicators_jsonfile_read.keys();
			while(indicators.hasNext()){
				String indicator_name = indicators.next();
				JSONObject indicator_object = indicators_jsonfile_read.getJSONObject(indicator_name);
				//make a jsonobject out of the not applicable to .
				//and restore that into the groovystatements
				JSONArray subindicators_not_applicable_to_indicator_array =
						indicator_object.getJSONArray("not_applicable_to");
				
				JSONObject subindicators_not_applicable_to_indicator_jsonobject = new JSONObject();
				
				for(int i=0; i < subindicators_not_applicable_to_indicator_array.length(); i++){
					//subindicators_not_applicable_to_indicator_jsonobject.put
					//(, arg1)
					subindicators_not_applicable_to_indicator_jsonobject
					.put(subindicators_not_applicable_to_indicator_array.getString(i), true);
				}
				
				indicator_object.put(StockConstants.not_applicable_to_object,
						subindicators_not_applicable_to_indicator_jsonobject);
				
				indicators_jsonfile_read.put(indicator_name, indicator_object);
			}
			
			
			//now put
			instance.indicators_jsonfile = indicators_jsonfile_read;
			
						
						
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static synchronized void initializeGroovyStatements(Graph g){
		if(instance == null){
			instance = new GroovyStatements();
			instance.graph = g;
			instance.registry_hash = new HashMap<String,Pair<Integer,String>>();
			instance.central_counter = new AtomicInteger();
			instance.gson = new GsonBuilder().setPrettyPrinting().create();
			instance.objmapper = new ObjectMapper();
			instance.objmapper.configure(Feature.USE_ANNOTATIONS, true);
			instance.session_start_time = new GregorianCalendar().getTimeInMillis();
			instance.cmap = new HashMap<String,int[]>();
			
			//used in 
			set_indicators_jsonfile();
		}
	}

	public void check_in(List<Integer> tt){
		def b = [];
		graph.V.has("day_id",com.tinkerpop.gremlin.Tokens.T.in,tt);
		System.out.println(b);
	}

	public void check_bitsy_speed(ArrayList<Integer> random_elements){
		System.out.println("this is the bitsy speed");
		GregorianCalendar start_calendar = new GregorianCalendar();
		graph.V.filter{
			random_elements.contains(it.day_id);
		}

		GregorianCalendar end_calendar = new GregorianCalendar();
		System.out.println(end_calendar.getTimeInMillis() - start_calendar.getTimeInMillis());
	}

	/**
	 * **********************************************************************************
	 * 
	 * ELASTICSEARCH FUNCTIONS.
	 * 
	 * **********************************************************************************
	 */

	public Integer index_new_component(ArrayList<String> component_constituents,String hex_id){
		Integer component_counter_of_this_element = null;
		Jedis jedis = null;
		try {
			jedis = RedisManager.getInstance().getJedis();
			Long current_component_counter = jedis.incr("component_counter");
			jedis.hset(hex_id, "component_counter",String.valueOf(current_component_counter
					.intValue()));
			component_counter_of_this_element =  current_component_counter.intValue();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		
		Integer component_count = component_constituents.size();
		HashMap<String,Object> hm =
				new HashMap<String,Object>(){ {
						put("component_numeric_id",component_counter_of_this_element);
						put("component_hex_id",hex_id);
						put("component_constituents",component_constituents.toArray(new String[component_constituents.size()]));
						put("component_count",component_count);
					}
				};
		IndexResponse res = ElasticClientHolder.getInstance().prepareIndex("tradegenie_titan",
				"component").setSource(hm).execute().actionGet();

		if(res.getId() == null){
			//System.out.println("not created");
			return null;
		}
		else{


			return component_counter_of_this_element;
		}

	}



	public Integer get_counter_for_element_or_index_it(ArrayList<String> component_constituents){

		String hex_id = DigestUtils
				.sha256Hex(component_constituents.toString());
		Integer component_count = component_constituents.size();

		Jedis jedis = null;
		try {
			jedis = RedisManager.getInstance().getJedis();
			String existing_component_counter = jedis.hget(hex_id, "component_counter");
			if(existing_component_counter==null){
				Integer j = index_new_component(component_constituents, hex_id);
				if(j!=null){
					//System.out.println("the result of indexing the new component:true");
					return j;
				}
				else{
					//System.out.println("failed to index new component.");
					//now log failure and we exit and log on exiting.
					LoggingDailyOperations.get_instance().add_to_log("exception",
							new Exception("elastisearch failed to index this component:" + component_constituents.toString()),
							identifier_for_log, null);

					LoggingDailyOperations.get_instance().exit_without_graph_and_log_before_exiting();
					return null;
				}
			}
			else{
				//System.out.println("the component was foudn in redis:" + existing_component_counter);
				return Integer.parseInt(existing_component_counter);
			}
		}
		catch(Exception e){

		}

	}


	/**
	 * check if subindicator already exists?
	 * 
	 * 
	 * 
	 */


	/**
	 * check if indicator already exists or is same as before.
	 * 
	 *
	 * 
	 */


	/**
	 * add subindicator to the graph.
	 * 
	 * 
	 */

	/**
	 * 
	 * add indicator to the graph.
	 * 
	 *
	 */


	/**
	 * searches the elasticsearch index for all stocks of this exchange.
	 * 
	 * 
	 * @param indiceName
	 * @return hashmap<symbol,fullname>
	 */
	public static List<HashMap<String,String>> searchElasticExchange(String indiceName){
		ArrayList<HashMap<String,String>> existing_stocks_in_exchange = new ArrayList<HashMap<String,String>>();

		try {
			XContentBuilder builder =
					jsonBuilder()
					.startObject()
					.startObject("query")
					.startObject("filtered")
					.startObject("query")
					.field("match_all")
					.startObject().endObject()
					.endObject()
					.startObject("filter")
					.startObject("bool")
					.startArray("must")
					.startObject()
					.startObject("term")
					.field("tableName").value(indiceName)
					.endObject()
					.endObject()
					.endArray()
					.endObject()
					.endObject()
					.endObject()
					.endObject()
					.endObject()
			;



			SearchRequestBuilder requestBuilder = ElasticClientHolder.
					getInstance().prepareSearch("tradegenie_titan")
					.setTypes("stock_details")
					.setSource(builder).addField("stockName").addField("stockFullName").setSize(110)
			;

			SearchResponse response = requestBuilder.execute().actionGet();

			for(SearchHit hit : response.getHits().hits()){
				HashMap<String,String> hm = new HashMap<String, String>();
				hm.put("symbol", hit.field("stockName").getValue().toString());
				hm.put("fullName", hit.field("stockFullName").getValue().toString());
				existing_stocks_in_exchange.add(hm);
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}





		return existing_stocks_in_exchange;

	}




	

	/**
	 * ****************************************************************************************
	 * 
	 * END ELASTICSEARCH FUNCTIONS.
	 * 
	 * ***************************************************************************************
	 */







	public static synchronized GroovyStatements getInstance(){
		return instance;
	}


	public Pair<ArrayList<Integer>,ArrayList<Integer>> get_two_lists(String stockName,
			String stockFullName, String indice, Integer day_id_from, Integer day_id_to){



		Integer days_since_epoch = new GregorianCalendar().getTimeInMillis()/86400000;

		Integer day_id_inclusive = day_id_from - ComplexCalculationRequest.days_before_actual_minus_amount;

		def b = [];

		//System.out.println("before request.");

		graph.V.has('stockName',stockName).has('stockFullName',stockFullName).has('indice',indice)
				.interval('day_id',day_id_inclusive,day_id_to + 1)
				.order({it.a.getProperty('day_id') <=> it.b.getProperty('day_id')})
				.day_id.fill(b);

		//System.out.println("came after the request.");


		def inclusive_list = [];
		def required_list = [];

		b.eachWithIndex{ c,i ->

			if(c >= day_id_inclusive && c <= day_id_to){

				inclusive_list.add(c)

				if(c >= day_id_from){

					required_list.add(c)

				}
			}



		}



		Pair<ArrayList<Integer>, ArrayList<Integer>> p = new Pair<ArrayList<Integer>,ArrayList<Integer>>
				(inclusive_list, required_list);

		return p;

	}

	public ArrayList<Edge> assemble_group(SubIndicator existing_subindicator_in_database){
		graph.commit();
		ArrayList<Edge> ordered_edges = new ArrayList<Edge>();
		graph.V.has("subindicator_name",existing_subindicator_in_database.getSubIndicator_name())
				.outE("subindicator_group")
				.order({it.a.getProperty('component_id') <=> it.b.getProperty('component_id')})
				.order({it.a.getProperty('subindicator_group_id') <=> it.b.getProperty('subindicator_group_id')})
				.fill(ordered_edges);
		return ordered_edges;
	}

	public ArrayList<Edge> assemble_opposite_group(SubIndicator existing_subindicator_in_database){
		graph.commit();
		ArrayList<Edge> ordered_edges = new ArrayList<Edge>();
		graph.V.has("subindicator_name",existing_subindicator_in_database.getSubIndicator_name())
				.outE("subindicator_opposite_group")
				.order({it.a.getProperty('component_id') <=> it.b.getProperty('component_id')})
				.order({it.a.getProperty('subindicator_opposite_group_id') <=> it.b.getProperty('subindicator_opposite_group_id')})
				.order({it.a.getProperty('subindicator_group_id') <=> it.b.getProperty('subindicator_group_id')})
				.fill(ordered_edges);
		return ordered_edges;
	}

	public ArrayList<Edge> assemble_opposite_subgroup(SubIndicator existing_subindicator_in_database){
		graph.commit();
		ArrayList<Edge> ordered_edges = new ArrayList<Edge>();
		graph.V.has("subindicator_name",existing_subindicator_in_database.getSubIndicator_name())
				.outE("subindicator_opposite_subgroup")
				.order({it.a.getProperty('component_id') <=> it.b.getProperty('component_id')})
				.order({it.a.getProperty('subindicator_opposite_group_id') <=> it.b.getProperty('subindicator_opposite_group_id')})
				.order({it.a.getProperty('subindicator_subgroup_id') <=> it.b.getProperty('subindicator_subgroup_id')})
				.order({it.a.getProperty('subindicator_group_id') <=> it.b.getProperty('subindicator_group_id')})
				.fill(ordered_edges);
		return ordered_edges;
	}

	public ArrayList<Edge> assemble_subgroup(SubIndicator existing_subindicator_in_database){
		graph.commit();
		ArrayList<Edge> ordered_edges = new ArrayList<Edge>();
		graph.V.has("subindicator_name",existing_subindicator_in_database.getSubIndicator_name())
				.outE("subindicator_subgroup")
				.order({it.a.getProperty('component_id') <=> it.b.getProperty('component_id')})
				.order({it.a.getProperty('subindicator_subgroup_id') <=> it.b.getProperty('subindicator_subgroup_id')})
				.order({it.a.getProperty('subindicator_group_id') <=> it.b.getProperty('subindicator_group_id')})
				.fill(ordered_edges);
		return ordered_edges;
	}

	public ArrayList<Edge> assemble_opposite_specific_group(SubIndicator existing_subindicator_in_database){
		graph.commit();
		ArrayList<Edge> ordered_edges = new ArrayList<Edge>();
		graph.V.has("subindicator_name",existing_subindicator_in_database.getSubIndicator_name())
				.outE("subindicator_opposite_specific_subindicator")
				.order({it.a.getProperty('component_id') <=> it.b.getProperty('component_id')})
				.order({it.a.getProperty('subindicator_subgroup_id') <=> it.b.getProperty('subindicator_subgroup_id')})
				.order({it.a.getProperty('subindicator_group_id') <=> it.b.getProperty('subindicator_group_id')})
				.fill(ordered_edges);
		return ordered_edges;
	}

	/**
	 * functions for 
	 * validating status of database
	 * 
	 * 
	 */
	public Integer total_stocks_of_particular_index(String index){
		def b = []
		graph.V.has("indice",index).stockName.dedup.fill(b);
		return b.size();
	}



	public void getCompanyInformation(){
		def b = []
		graph.V.has("tableName","LME").has("stockName","silver").fill(b);
		println b.size();
	}

	public void getTotalVertexCount(){
		def b = []
		graph.V.map().fill(b);
		println "the total vertex count is";
		println b.size();
	}



	/**
	 * you have to give this function a hashmap, for each vertex that is to be created
	 * the keys are the propertynames, and the values are the values for those properties.
	 * @param g : TitanGraph.
	 * @param m : HashMap of Property Names and their values.
	 */

	public Integer addSectorIndustryInformation(JSONObject additional_params,
			Map<String,Object> m,
			TItanWorkerThreadResultObject result_object,
			String job_type){

		//first check if we already have this property.
		//if yes then we have to update it.
		def a = [];
		String stockName = (String)additional_params.get("stockName");
		String stockFullName = (String)additional_params.get("stockFullName");
		String indice = (String)additional_params.get("tableName");


		//graph.V.has("stockName",stockName).has("stockFullName",stockFullName).has("tableName",indice).map().fill(a);
		//if(a.size() == 0){
		addProperty(m,false,job_type);
		return 1;
		//}
		//else if(a.size == 1){
		//	graph.V.has("stockName",stockName).has("stockFullName",stockFullName).has("tableName",indice).sideEffect({tn=it;m.each{tn.setProperty(it.key,it.value)}});
		//	System.out.println("this is not added because already there in graph , company information");
		//	return 1;
		//}
		//else{
		//	return 0;
		//}

	}



	public void addPropertyTransactional(TransactionalGraph tg,Map<String,Object> m) throws VertexMapDoesNotHaveUniqueIdentifier{
		//def b = []
		//g.V("unique_identifier",m.get("unique_identifier")).unique_identifier.fill(b);
		//def uniqueidentifier = m.get("unique_identifier");
		//if(b.size == 0){
		//println "valid unique identifier"
		//println uniqueidentifier;

		Vertex v = graph.addVertex(["unique_identifier":m.get("unique_identifier")]);
		if(m.get("unique_identifier") ==  null){
			throw new VertexMapDoesNotHaveUniqueIdentifier(m);
		}
		else{
			m.each{ property,value ->
				if(property == "unique_identifier"){

				}
				else{


					v.setProperty(property, value);
					//println "added ${property}, ${value}";
				}

			}
			//}
			//else{
			//  println "failed on this unique identifier"
			//	println uniqueidentifier;
			//}
		}
	}

	/***
	 * draws edges betweeen common day id vertices, already generated by the function.
	 * generate_common_day_connector_edge.
	 * 
	 * @param root_vertex
	 * @param destination_vertex
	 * @param edgeLabelName
	 */
	public void draw_common_day_connector_edge(Vertex root_vertex, Vertex destination_vertex,
			String edgeLabelName){
		Vertex v1 = graph.getVertex(root_vertex.id);
		Vertex v2 = graph.getVertex(destination_vertex.id);

		try{
			Edge e = graph.addEdge(null,v1,
					v2, edgeLabelName);
			graph.commit();
		}
		catch(Exception e){

			e.printStackTrace();
			LoggingDailyOperations.get_instance()
					.add_to_log("exception", e, identifier_for_log, null);
		}
	}


	public void draw_edge(Map<String,Object>m, Vertex root_vertex,
			Vertex destination_vertex, String edgeLabelName){

		//if(m.get("unique_identifier") == null || m.get("unique_identifier") == ""){

		//	LoggingDailyOperations.get_instance()
		//			.getIndividual_stock_operations().
		//			get(root_vertex.getProperty("stockName") + "_" + root_vertex.getProperty("stockFullName")
		//			+ "_" + root_vertex.getProperty("indice")).add_date_edge_failed_to_draw(root_vertex.getProperty("dates"),
		//			destination_vertex.getProperty("dates"));
		//}
		//else{
		try{
			Edge e = graph.addEdge(graph.v(root_vertex.id),graph.v(destination_vertex.id),
					edgeLabelName);
			m.each{ property,value ->
				//if(property == "unique_identifier"){

				//}
				//else{

				//e = graph.getEdge(e);
				e.setProperty(property, value);
				//println "added ${property}, ${value} for edge";
				//}

			}

			graph.commit();
			//add to the
			LoggingDailyOperations.get_instance()
					.getIndividual_stock_operations().
					get(root_vertex.getProperty("stockName") + "_"
					+ root_vertex.getProperty("stockFullName")
					+ "_" + root_vertex.getProperty("indice"))
					.add_date_edge(root_vertex.getProperty("dates"),
					destination_vertex.getProperty("dates"));
			;
		}
		catch(Exception e){
			//before doing this log the error.

			set_error_pair_exception(e, make_map_from_vertex_pair_for_connector_edge_loggin(root_vertex, destination_vertex));
			LoggingDailyOperations.get_instance()
					.getIndividual_stock_operations().
					get(root_vertex.getProperty("stockName") + "_" + root_vertex.getProperty("stockFullName")
					+ "_" + root_vertex.getProperty("indice")).add_date_edge_failed_to_draw(root_vertex.getProperty("dates"),
					destination_vertex.getProperty("dates"));

			throw e;
		}
		//commit.
		//}


	}


	public void add_combined_vertex_and_edges(String vertexLabel ,
			HashMap<String,Object> vertex_properties_map,
			Long indicator_vertex_id, Long subindicator_vertex_id,
			String entity_parent_indicator_label,
			String entity_parent_subindicator_label) throws Exception{

		try{
			Vertex v = graph.addVertexWithLabel(vertexLabel);
			vertex_properties_map.each {property,value ->
				v.setProperty(property,value);
			}

			Edge parent_indicator = graph.addEdge(null, v, graph.v(indicator_vertex_id),
					entity_parent_indicator_label);
			Edge parent_subindicator = graph.addEdge(null, v, graph.v(subindicator_vertex_id),
					entity_parent_subindicator_label);

			graph.commit();
		}
		catch(Exception e){
			throw e;
		}
	}


	public Map<String,String> make_map_from_vertex_pair_for_connector_edge_loggin(Vertex root_vertex, Vertex destination_vertex){
		Map<String,String> m = new HashMap<String,String>();
		m.put("stockName", root_vertex.getProperty("stockName"));
		m.put("stockFullName", root_vertex.getProperty("stockFullName"));
		m.put("indice", root_vertex.getProperty("indice"));
		m.put("dates", root_vertex.getProperty("dates") + "_" + destination_vertex.getProperty("dates"));
	}

	public void set_error_pair_exception(
			Exception e,Map<String,String> m){

		Pair<String,JSONObject> error_pair =
				new Pair<String,JSONObject>(null,null);

		StockOperations ops = LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations()
				.get(m.get("stockName") + "_" + m.get("stockFullName") + "_"
				+ m.get("indice"));
		//error_pair.setAt0(m.get("dates"));
		JSONObject object =  new JSONObject();
		object.put("exception",e);
		//	error_pair.setAt1(object);
		ops.getDates_failed_in_database_adding().putIfAbsent(m.get("dates"), object);
		LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations().put(m.get("stockName") + "_" + m.get("stockFullName") + "_"
				+ m.get("indice"), ops);
		LoggingDailyOperations.get_instance().add_to_log("exception", new Exception("failed" +
				"to add a stock historical data vertex/ or draw the edge ftom stock vertex to common day id.."  +
				" , check the stockoperations, error reports. "), "Groovy_Statements:" +
				" set_error_pair_exception", null);
	}


	public void set_exception_in_updating_common_day_id_edge(String stockName, String stockFullName,
			String indice, Integer day_id_from, Exception e){
		StockOperations ops = LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations()
				.get(stockName + "_" + stockFullName + "_"
				+ indice);

		JSONObject object =  new JSONObject();
		object.put("exception",e);
		//	error_pair.setAt1(object);
		ops.getDates_failed_in_database_adding().putIfAbsent(String.valueOf(day_id_from), object);
		LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations().put(stockName + "_" + stockFullName + "_"
				+ indice, ops);
		LoggingDailyOperations.get_instance().add_to_log("exception", new Exception("failed" +
				"to add a stock historical data vertex/ or draw the edge ftom stock vertex to common day id.."  +
				" , check the stockoperations, error reports. "), "Groovy_Statements:" +
				" set_error_pair_exception", null);

	}


	/**
	 *
	 * draws edge between stock vertex and 
	 * common day id vertex of same day id.
	 * 
	 * @param m
	 * @param v
	 * @return
	 * @throws Exception
	 */
	public draw_common_day_id_edge(Map<String,String> m,Vertex v)
	throws Exception{

		try{
			GregorianCalendar start_draw_edge_id = new GregorianCalendar();

			//println "came to draw the edge to day id"
			Integer day_id = m.get("day_id");
			Vertex common_day_id_vertex =
					graph.V.has("common_day_id",day_id).next();
			Edge e = graph.addEdge(null,v,common_day_id_vertex,'common_day_id_edge_label'
					,[common_day_id_stockName:m.get("stockName"),common_day_id_stockFullName:m.get("stockFullName")
						,common_day_id_indice:m.get("indice")]);
			//println "this is the edge id " + e.getId().toString();


			graph.commit();

			set_logging_operations_success(m);

			return;
		}
		catch(Exception e){


			if(e.getLocalizedMessage().contains("An edge with the given label already")){
				LoggingDailyOperations.get_instance()
						.add_to_log("info", null,"groovy_statements" , "IGNORED EDGE ALREADY EXISTS WHILE DRAWING COMMON DAY ID EDGE day id in question : " + String.valueOf(m.get("day_id"))
						+ "stock unique identifier:" + m.get("stockName") + "_" + m.get("stockFullName") + "_"
						+ m.get("indice"));

				StockOperations ops = get_stock_operations_for_stock(m);
				ops.add_dates_of_stock_vertex_to_common_day_id_edges_already_present_in_db(m.get("dates"));
				update_stock_operations(m, ops);
			}
			else{
				e.printStackTrace();
				//LoggingDailyOperations.get_instance()
				//		.add_to_log("exception", e, "groovy_statements exception: day id in question : " + String.valueOf(m.get("day_id"))
				//		+ "stock unique identifier:" + m.get("stockName") + "_" + m.get("stockFullName") + "_"
				//		+ m.get("indice"), null);
				set_error_pair_exception(e, m);
			}

		}
	}

	public StockOperations get_stock_operations_for_stock(Map<String,String> m){
		StockOperations ops = LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations()
				.get(m.get("stockName") + "_" + m.get("stockFullName") + "_"
				+ m.get("indice"));

		return ops;
	}

	public void update_stock_operations(Map<String,String> m, StockOperations ops){
		LoggingDailyOperations.get_instance()
				.getIndividual_stock_operations().put(m.get("stockName") + "_" + m.get("stockFullName") + "_"
				+ m.get("indice"), ops);
	}

	public set_logging_operations_success(Map<String,String> m){
		StockOperations ops = get_stock_operations_for_stock(m);
		ops.dates_successfully_added_to_db.add(m.get("dates"));
		update_stock_operations(m, ops);
	}

	public void addProperty(Map<String,Object> m,
			Boolean draw_edge_to_day_id,
			String job_type) throws Exception{

		if(job_type.equals("add_daily_stock_price")){
			if(m.get("unique_identifier") == null || m.get("unique_identifier").equals("")){
				set_error_pair_exception(new Exception("no unique identifier, while adding" +
						"stock historical object"), m);
			}
			else{
				try{
					Vertex v = graph.addVertex(["unique_identifier":m.get("unique_identifier")]);
					m.each{ property,value ->
						if(property == "unique_identifier"){

						}
						else{


							v.setProperty(property, value);
							//println "added ${property}, ${value}";
						}

					}

					if(draw_edge_to_day_id){
						draw_common_day_id_edge(m,v);
					}

					graph.commit();



				}
				catch(Exception e){
					if(job_type.equals("add_daily_stock_price")){
						if(e.getLocalizedMessage().contains("violates")){
							System.out.println("caught the violates and tried to add the day id edge.");
							/**
							 * update in stock ops.
							 */
							StockOperations ops = get_stock_operations_for_stock(m);
							ops.add_dates_already_existing_in_database(m.get("dates"));
							update_stock_operations(m, ops);
							/**
							 * finish update.
							 *
							 */
							Vertex existing_vertex = graph.V().has("unique_identifier",m.get("unique_identifier")).next();
							if(draw_edge_to_day_id){
								draw_common_day_id_edge(m,existing_vertex);
							}
						}
						else{
							//graph.rollback();
							set_error_pair_exception(e, m);
							//e.printStackTrace();
							throw new Exception(e);
						}
					}
					else{
						System.out.println(job_type);
						throw new Exception(e);
					}
				}
			}

		}
		else if(job_type.equals("add_sector_industry_information")){
			try{
				def b = [];
				Vertex v = null;
				graph.V().has("tableName",m.get("tableName")).has("stockName",m.get("stockName"))
						.has("stockFullName",m.get("stockFullName")).fill(b);
				if(b.empty){
					v = graph.addVertex(null);
				}
				else{
					v = b.get(0);
				}
				m.each{ property,value ->
					if(property == "unique_identifier"){
						//we dont set any unique identifier on this thing.
					}
					else{


						v.setProperty(property, value);
						//println "added ${property}, ${value}";
					}

				}
				graph.commit();
			}
			catch(Exception e){
				LoggingDailyOperations.get_instance().add_to_log("exception",
						e,
						"groovy_statements: " + m.toMapString(), job_type)
			}
		}

	}

	public void checkIfPropertyKeyExists(){
		def b = []
		graph.V("unique_identifier","Avago Technologies Limited_AVGO__1406145600000").unique_identifier.fill(b);
		if(b.size == 0){
			println "its not fine";
		}
		else{
			println "its fine.";
		}
	}


	/**
	 * simple query on one parameter, gets whatever parameters are supplied in form of list of hashmaps
	 * @param g
	 * @param exchange
	 * @return list
	 */
	public List<HashMap<String,String>> searchExchange(String exchange){
		def b = []

		graph.V("tableName",exchange).transform({['symbol' : it.getProperty('stockName') , 'fullName' : it.getProperty('stockFullName')]}).fill(b);

		return b;
	}

	//g.V("stockName","ddr").order
	//({it.b.getProperty('epoches')
	// <=> it.a.getProperty('epoches')})[0].
	//transform({['dateString':it.getProperty('dates'),'timezone':it.getProperty('timezoneString')]})


	public ArrayList<String> getLastDateAndTimeZone(String stockSymbol, String stockFullName){
		//System.out.println(stockSymbol);
		//System.out.println(stockFullName);
		ArrayList<String> dateFindings = new ArrayList<String>();
		def b = []
		graph.V("stockName",stockSymbol).has("stockFullName",stockFullName).hasNot('tableName').map().fill(b);
		//graph.commit();
		if(b.size()==0){
			return dateFindings;
		}
		else{

			def c = []
			graph.V("stockName",stockSymbol).has("stockFullName",stockFullName)
					.order({it.b.getProperty("epoches") <=> it.a.getProperty("epoches")})[0].transform({['dateString' : it.getProperty("dates"),'timezone' : it.getProperty("timezoneString")]}).fill(c);
			graph.commit();
			if(c.size == 0){
				return dateFindings;
			}
			else{
				String a = c[0].dateString + " " + c[0].timezone;
				dateFindings.add(c[0].dateString);
				dateFindings.add(c[0].timezone);
			}

			return dateFindings;
		}
	}

	public Integer getTotalNumberOfPricesForStock(String stockName){
		def m = []
		graph.V("stockName",stockName).fill(m);
		return m.size;
	}

	public void deleteNullVertices(){
		/*
		 def b = []
		 int a = graph.V.hasNot("unique_identifier").map().fill(b).size;
		 println "total null before--->";
		 println a;
		 graph.V.hasNot("unique_identifier").remove();
		 graph.commit();
		 b = []
		 a = graph.V.hasNot("unique_identifier").map().fill(b).size;
		 println "total null after--->";
		 println a;
		 */
	}

	/**
	 * STOCK STATE QUERIES START HERE.
	 * 
	 * 
	 * 
	 * 
	 */

	public Integer get_all_stocks_for_a_index(String indice){
		def b = [];
		graph.V.has("indice",indice).stockName.dedup().fill(b);
		//now get the components
		def components = []
		graph.V.has("tableName",indice).stockName.fill(components);

		//now iterate over all the basic stock names and see that they are present in the components.

	}



	/**
	 * 
	 * get a list of dailyvalues for a given stock, between two given dates.
	 * 
	 * 
	 */

	public HistoricalStock get_historical_stock_object_for_dates(Calendar start_date, Calendar end_date,
			String stockName, String stockFullName,
			String indice, TimeZone tz,String entity_type)
	{

		def b = [];
		Long start_time = start_date.getTimeInMillis();
		Long end_time = end_date.getTimeInMillis();

		graph.V.has("stockName",stockName).has("stockFullName",stockFullName)
				.has("indice",indice).interval("epoches",start_time,end_time + 1).order({it.a.getProperty("epoches")<=>it.b.getProperty("epoches")}).map().fill(b);
		HistoricalStock hs = new HistoricalStock(stockName, tz, entity_type, indice, stockFullName);
		def params = ["open", "high", "low"];
		for(HashMap<String,Object> dailyvalue: b){
			hs.addClose((Double)dailyvalue.get("close"));
			for(String p:params){
				if(dailyvalue.get(p) == null)
				{

				}
				else{
					hs.addopen((Double)dailyvalue.get(p));
				}
			}
			if(dailyvalue.get("volume") == null){

			}
			else{
				hs.addVolume((Integer)dailyvalue.get("volume"));
			}
			CreateQueryStringFromDbFields cst = new CreateQueryStringFromDbFields();
			cst.appendDbDate(dailyvalue.get("dates"),dailyvalue.get("timezoneString"));
			hs.addDate(	cst.getDbdate());
			hs.addepoch((Long)dailyvalue.get("epoches"));
			hs.timezoneString = (String)dailyvalue.get("timezoneString");

		}

		return hs;

	}

	/**
	 * checks the graph for the existence of a vertex with a tablename of "daily_indicators_json_string"
	 * 
	 * 
	 * 
	 */
	public String get_daily_indicators_json_string(String type){
		if(type == "subindicators"){
			a = graph.V("tableName",StockConstants.daily_subindicators_json_string_tableName).daily_indicators_json_string;
			return a;
		}
		else{
			//for indicators update later.
			return "";
		}

	}

	public Object delete_daily_indicators_string_from_database(String type){
		if(type == "subindicators"){
			a = graph.E.has("tableName",StockConstants.daily_subindicators_json_string_tableName).remove();
			return a;
		}
		else{
			//for indicators
			return "";
		}
	}



	/**
	 * 
	 * indicator status -> 0 means that it is to be calculated.
	 * 					-> 1 means that it has been calculated.
	 * 					-> 2 means that it does not apply to this stock.
	 * 
	 * 
	 * @param stock_name
	 * @param stockFullName
	 * @param indiceName
	 * @param start_date
	 * @param end_date
	 * @return a hashmap containing epoch and which indicators need to be calculated for the epoch for this
	 * stock.
	 */
	public HashMap<Long,ArrayList<String>> indicators_to_be_calculated_for_stock(String stock_name,
			String stockFullName, String indiceName,Calendar start_date,Calendar end_date){
		Long start_date_millis = start_date.getTimeInMillis();
		Long end_date_millis = end_date.getTimeInMillis();
		def b = [];
		graph.V("stockName",stockName).has("stockFullName",stockFullName)
				.has("indice",indiceName).interval("epoches",start_date_millis,end_date_millis + 1)
				.outE("indicator_status_edge").has("indicator_status",0).inV.paths
				.transform(
				{
					["epoches" : graph.v(it[0]).getProperty("epoches"), "indicator": graph.v(it[2]).getProperty("indicator_name")]
				}
				)
				.fill(b);

		HashMap<Long,ArrayList<String>> indicators_holder = new HashMap<Long,ArrayList<String>>();

		b.each {key,value->
			if(indicators_holder.get(key) == null){
				indicators_holder.put(key,[value]);
			}
			else{
				indicators_holder.put(key,indicators_holder.get(key).add(value));
			}
		}

		return indicators_holder;
	}

	public Float calculate_profit_percentage_between_two_days(Float later_price, Float earlier_price){

		return ((later_price-earlier_price)/later_price)*100.0;
	}

	public Integer get_profit_or_loss_or_same(Float price_change_percentage){
		if(price_change_percentage > 0.00){
			return 1;
		}
		else if(price_change_percentage < 0.00){
			return -1;
		}
		else{
			return 0;
		}
	}

	/**
	 * generates the arraylist of edges to be drawn from a given common day id vertex to all 
	 * previous common day id vertices within the number of days defined in stockconstants.
	 * between the common day id vertices.
	 * 
	 * @param common_day_id
	 * @return
	 */
	public ArrayList<CommonDayConnectorEdge> generate_common_day_id_day_connector_edges(Integer common_day_id){

		def a = [];
		Integer k = common_day_id - StockConstants.total_days_to_connect_by_day_connector;
		Integer k1 = common_day_id - 1;
		Vertex root_vertex = graph.V.has('common_day_id',common_day_id).next();
		graph.V.interval("common_day_id",k,k1).fill(a);
		ArrayList<CommonDayConnectorEdge> dmain = new ArrayList<CommonDayConnectorEdge>();

		a.eachWithIndex {vertex,i ->
			CommonDayConnectorEdge cde =
					new CommonDayConnectorEdge(root_vertex, vertex);
			dmain.add(cde);
		}

		return dmain;
	}


	/**
	 * generates the arraylist of connector edges to be drawn between
	 * the common day id vertices, but for each stock change.
	 * 
	 * 
	 * @param vertex_map
	 * @return
	 */
	public ArrayList<DayConnectorEdge> generate_stock_wise_updates_for_common_day_connector_edges(Map<String,Object> vertex_map){

		def a = []
		//System.out.println(vertex_map.get("day_id"));

		Integer k = (Integer)vertex_map.get("day_id") - 365;
		//println(k);

		Integer k2 = (Integer)vertex_map.get("day_id") - 1;
		//println(k2);

		//System.out.println(k);
		//System.out.println(k2);

		//here the interval is intentionally non inclusive so as to not add the root vertex itself.
		graph.V.interval("common_day_id",k,k2).fill(a);

		//System.out.println(vertex_map);

		Vertex root_vertex = graph.V.has("common_day_id",vertex_map.get("day_id")).next();

		ArrayList<DayConnectorEdge> day_connector_edges = new ArrayList<DayConnectorEdge>();
		//println("size of the vertices got is  " + a.size());

		String stock_combination = (String)vertex_map.get("stockName") + "_" +
				vertex_map.get("stockFullName");
		String stockName = (String)vertex_map.get("stockName");
		String stockFullName = (String)vertex_map.get("stockFullName");
		String indice = (String)vertex_map.get("indice");


		a.eachWithIndex{ vertex,i ->
			//first calculate the profit.
			//then calculate the days
			//then calculate the binary profit.

			//println("this is the vertex close price");
			//println(vertex.close);
			String edge_id = root_vertex.outE().as('edge').inV().has('id',vertex.id).back('edge').next().id;

			Float price_change_percentage = calculate_profit_percentage_between_two_days((Float)vertex_map.
					get("close"),(Float)vertex.close);
			Integer days_back = vertex_map.get("day_id") - vertex.day_id;
			Integer profit_or_loss = get_profit_or_loss_or_same(price_change_percentage);
			Integer day_id_from = vertex_map.get("day_id");
			Integer day_id_to = vertex.day_id;
			//now draw the edge between this vertex and the root vertex and add all these properties to it.

			//DayConnectorEdge e = new DayConnectorEdge(price_change_percentage,
			//		days_back, profit_or_loss, root_vertex, vertex, stock_combination,edge_id);

			DayConnectorEdge e = new DayConnectorEdge(price_change_percentage, days_back,
					profit_or_loss, stock_combination, edge_id, stockName,
					stockFullName, indice, day_id_from, day_id_to);

			day_connector_edges.add(e);

		}

		return day_connector_edges;
		//now run the graph add job.

	}

	/**
	 * updates edge between two common day id vertices, with
	 * the profit percentage, profit or loss and days back for a particular stock.
	 * basically it is part two of 'generate_day_connector_edges'
	 *
	 * @return true if the operation was successfully false otherwise.
	 */
	public Boolean update_property_on_common_day_connector_edge(Float price_change_percentage, Integer days_back,
			String stock_combination, Integer profit_or_loss, String edge_id, String stockName,
			String stockFullName, String indice, Integer day_id_from, Integer day_id_to){

		try{
			Edge e = graph.getEdge(edge_id);
			e.setProperty("days_back_" + stock_combination, days_back);
			e.setProperty("price_change_percentage_" + stock_combination, price_change_percentage);
			e.setProperty("profit_or_loss_" + stock_combination, profit_or_loss);
			graph.commit();
			LoggingDailyOperations.get_instance()
					.getIndividual_stock_operations().
					get(stockName + "_"
					+ stockFullName
					+ "_" + indice)
					.add_date_edge(String.valueOf(day_id_from),
					String.valueOf(day_id_to));
			return true;
		}
		catch(Exception e){
			set_exception_in_updating_common_day_id_edge(stockName, stockFullName, indice, day_id_from, e);
			LoggingDailyOperations.get_instance()
					.getIndividual_stock_operations().
					get(stockName + "_" + stockFullName + "_" + indice).add_date_edge_failed_to_draw(String.valueOf(day_id_from),
					String.valueOf(day_id_to));
			return false;
		}
	}

	/**
	 * traverses out from newly added end of week and end of month edges for each stock,
	 * and updates this property on common day connector edges.
	 * 
	 * 
	 */



	public ArrayList<Edge> get_outgoing_edges(Vertex v){
		ArrayList<Edge> outgoing_edges = new ArrayList<Edge>();
		graph.v(v.getId()).outE().fill(outgoing_edges);
		return outgoing_edges;
	}

	public ArrayList<Edge> get_outgoing_edge_with_label(Vertex v, String label)
	{
		ArrayList<Edge> outgoing_edges = new ArrayList<Edge>();
		graph.v(v.getId()).outE(label).fill(outgoing_edges);

		return outgoing_edges;
	}

	public ArrayList<Vertex> get_vertices_with_these_names(ArrayList<String> elements,
			String property){
		ArrayList<Vertex> vertices =  new ArrayList<Vertex>();
		for(String element_name: elements){
			//println "now doing this property--->" + element_name
			try{
				Vertex v = graph.V.has(property,element_name).next()
				vertices.add(v);
			}
			catch(Exception e){

			}

		}

		return vertices;
	}

	/************************************************************
	 * 
	 * Functions to create the subindicator_indicator_stock_complexes
	 * 
	 * 
	 * 
	 */



	/**
	 * 
	 * 
	 * @param ohlcv_group
	 * @return
	 */
	public Set<Vertex> get_all_stocks_with_given_ohlcv_groups(ArrayList<String> ohlcv_group){
		//we need the unique names of the stocks.
		Set<Vertex> existing_stocks_with_this_ohlcv_group = new HashSet<HashMap<String,String>>();
		for(String ohlcv_g : ohlcv_group){
			def b = []


			graph.V.has("ohlcv_type",ohlcv_g).as("stock").stockName.dedup()
					.back("stock").fill(b);

			b.eachWithIndex {value,i ->
				existing_stocks_with_this_ohlcv_group.add(value);
			}
		}

		return existing_stocks_with_this_ohlcv_group;
	}

	/**
	 * remove all the subindicators
	 * which are no longer present in the subindicators hashmap.
	 * 
	 * 
	 * @param subindicator_names
	 * @return
	 */
	public remove_all_subindicators_except_these_from_graph(Set<String> subindicator_names){
		try{
			def existing_subindicators_in_graph = []

			graph.V.has("subindicator_name").as("subindicator").outE("subindicator_group")
					.back("subindicator").dedup().subindicator_name.fill(existing_subindicators_in_graph);


			existing_subindicators_in_graph.eachWithIndex {s,i->
				if(!subindicator_names.contains(s)){
					System.out.println("now removed--->" + s);
					graph.V.has("subindicator_name",s).outE().remove();
					graph.V.has("subindicator_name",s).inE().remove();
					graph.V.has("subindicator_name",s).remove();
					LoggingDailyOperations.get_instance().add_to_log("info",
							null, identifier_for_log, "removed this subindicator because it is not present in the json file.");
				}

			}

			graph.commit();
		}
		catch(Exception e){
			LoggingDailyOperations.get_instance().add_to_log("exception",
					e, identifier_for_log, null);
		}
	}


	/**
	 * remove all the indicators
	 * which are no longer present in the subindicators hashmap.
	 *
	 *
	 * @param subindicator_names
	 * @return
	 */
	public remove_all_indicators_except_these_from_graph(Set<String> indicator_names){
		def existing_indicators_in_graph = []

		graph.V.has("indicator_name").has("ohlcv_groups").dedup()
				.indicator_name.fill(existing_indicators_in_graph);

		System.out.println(existing_indicators_in_graph);

		existing_indicators_in_graph.eachWithIndex {s,i->
			if(!indicator_names.contains(s)){
				System.out.println("now removed indicator--->" + s);
				graph.V.has("indicator_name",s).has("ohlcv_groups").outE().remove();
				graph.V.has("subindicator_name",s).has("ohlcv_groups").inE().remove();
				graph.V.has("subindicator_name",s).has("ohlcv_groups").remove();
			}

		}

	}

	/***
	 * FUNCTIONS FOR CALCULATION OF ACTUAL SUBINDICATOR INDICATOR COMPLEXES.
	 * 
	 * the structure of the edge from each day is like:
	 * 					   name:complex_name, label: complex,
	 * 							day_id: day_id (sortkey)
	 * stock value vertex =======================================> complex_vertex
	 * 
	 * 
	 * 
	 * 
	 */

	public ArrayList<LinkedHashMap<String,ArrayList<String>>>
	get_all_applicable_complexes_to_this_stock(String stockName,
			String stockFullName, String indice){


		def b = [];

		graph.V.has("entity_indicator_subindicator_stockName_name",stockName)
				.as("complex").outE('entity_parent_indicator').
				inV().as('indicator').back("complex").
				outE('entity_parent_subindicator').
				inV().as('subindicator').back('complex').transform({v,m->
					[(v.entity_indicator_subindicator_unique_name) :
						[
							(m.indicator.indicator_name) ,
							(m.subindicator.subindicator_name)
						]]
				}).fill(b);



		return b;
	}

	public ArrayList<LinkedHashMap<String,ArrayList<String>>>
	get_indicators_and_subindicators_given_complex_name(ArrayList<String> complex_names){

		def b = [];
		for(String complex_name : complex_names){
			def c = [];
			graph.V.has("entity_indicator_subindicator_unique_name",complex_name).as('complex').
					outE('entity_parent_indicator').inV().as('indicator').back('complex')
					.outE('entity_parent_subindicator').inV().as('subindicator').back('complex')
					.transform({v,m->
						[(v.entity_indicator_subindicator_unique_name) :
							[
								(m.indicator.indicator_name) ,
								(m.subindicator.subindicator_name)
							]]
					}).fill(c);

			b.addAll(c);

		}

		return b;


	}


	public HashMap<String, LinkedHashMap<Integer, Double>>
	get_required_complex_values_for_n_days_before_given_day(Integer start_day,
			Integer end_day,String stockName,String stockFullName, String indiceName,
			ArrayList<String> complex_names){

		System.out.println(start_day);
		System.out.println(end_day);
		System.out.println(stockName);
		System.out.println(stockName);


		HashMap<String,
				LinkedHashMap<Integer,Double>> results = new HashMap<String,LinkedHashMap<Integer,Double>>();

		for(String complex_name : complex_names){
			System.out.println(complex_name);
			def b = [];
			graph.V.has("stockName",stockName).has("day_of_week")
					.interval('day_id',start_day,end_day + 1).
					order({it.a.getProperty('day_id') <=> it.b.getProperty('day_id')}).
					outE('complex').has("complex_name",complex_name).
					transform({
						['day_id':it.getProperty('complex_day_id'),
							'value':it.getProperty('complex_value')]}).fill(b)
			if(b.size() > 0){
				//first make an entry for b in the results hashmap.
				//the
				System.out.println("there are results");
				LinkedHashMap<Integer,Double> complex_results = new LinkedHashMap<Integer, Double>();


				//the results are in ascending order.
				b.eachWithIndex{c,i->

					complex_results.put(c.day_id, c.value);

				}

				results.put(complex_name, complex_results);

			}

		}

		return results;

	}

	public Object get_complex_vertex_id(String complex_name){
		Vertex v = graph.V.has("entity_indicator_subindicator_unique_name",complex_name).next();
		return v.getId();
	}

	public String add_complex_value_edge(Integer day_id, String stockName, String stockFullName, String indice
			, Long complex_vertex_id,String complex_name, Double complex_value){

		Vertex stock_day_id_vertex =
				graph.V.has("indice",indice)
				.has("day_id",day_id)
				.has("stockName",stockName)
				.has("stockFullName",stockFullName).next();

		Edge e = graph.addEdge(null, stock_day_id_vertex, graph.v(complex_vertex_id),
				"complex");
		e.setProperty("complex_name", complex_name);
		e.setProperty("complex_value", complex_value);
		e.setProperty("complex_day_id", day_id);
		graph.commit();
		return e.getId().toString();
	}


	public String get_complex_name_without_subindicator(String complex_name){
		String initial_stub = complex_name.substring(0, complex_name.lastIndexOf("_period_end") + 11);
		return initial_stub;
	}

	/**
	 * the name of the complex will be the stock details, _ indicator_name_with_period_start_end,
	 * so we need to get sub
	 * 
	 */
	public LinkedHashMap<Integer,Double> get_indicator_values_over_date_range(String complex_name,
			Integer day_id_start,
			Integer day_id_end){

		String initial_stub = get_complex_name_without_subindicator(complex_name);
		String name_of_complex_holding_indicator_value = initial_stub + "_subindicator_value";
		return get_complex_value_over_period(day_id_start, day_id_end,
				name_of_complex_holding_indicator_value);

	}




	public ArrayList<Vertex> get_all_indicator_vertices(){
		def b = [];
		graph.V.has("indicator_name").fill(b);
		return b;
	}

	public LinkedHashMap<Integer,Double> get_open_high_low_close_prices(String stockName, String stockFullName,
			String indice, Pair<Integer,Integer> day_id_range, String requirement){


		def b = [];
		graph.V.has("stockName",stockName).has('stockFullName',stockFullName).has('indice',indice).has("day_of_week")
				.interval('day_id',day_id_range.getValue0(),day_id_range.getValue1() + 1).
				order({it.a.getProperty('day_id') <=> it.b.getProperty('day_id')}).
				transform({
					['day_id':it.getProperty('day_id'),
						'value':it.getProperty(requirement)]}).fill(b)

		b.removeAll([null]);

		LinkedHashMap<Integer,Double> ln = new LinkedHashMap<Integer,Double>();



		b.eachWithIndex {c,i->

			ln.put(Integer.parseInt(c.day_id.toString()), Double.parseDouble(c.value.toString()));

		}

		return ln;
	}

	public LinkedHashMap<Integer,Long> get_volume(String stockName, String stockFullName,
			String indice, Pair<Integer,Integer> day_id_range, String requirement){

		LinkedHashMap<Integer,Long> ln = new LinkedHashMap<Integer,Long>();
		def b = [];
		graph.V.has("stockName",stockName).has('stockFullName',stockFullName).has('indice',indice).has("day_of_week")
				.interval('day_id',day_id_range.getValue0(),day_id_range.getValue1() + 1).
				order({it.a.getProperty('day_id') <=> it.b.getProperty('day_id')}).
				transform({
					['day_id':it.getProperty('day_id'),
						'value':it.getProperty('volume')]}).fill(b);
		b.removeAll([null]);
		b.eachWithIndex {c,i->

			ln.put(Integer.parseInt(c.day_id.toString()), Long.parseLong(c.value.toString()));

		}
		return ln;
	}

	public LinkedHashMap<Integer,Double> get_complex_value_over_period(int start_day, int end_day,
			String complex_name){

		def b = [];

		LinkedHashMap<Integer,Double> ln = new LinkedHashMap<Integer,Double>();

		graph.V.has("entity_indicator_subindicator_unique_name", complex_name)
				.inE('complex').interval('complex_day_id',start_day,end_day + 1)
				.order({it.a.getProperty('complex_day_id') <=> it.b.getProperty('complex_day_id')}).
				transform({
					['day_id':it.getProperty('complex_day_id'),
						'value':it.getProperty('complex_value')]}).fill(b);
		b.removeAll([null]);
		b.eachWithIndex {c,i->
			//System.out.println("class of the indicator value.");
			//System.out.println(c.value.class);
			//System.out.println("class of the indicator day id");
			//System.out.println(c.day_id.class);
			ln.put(Integer.parseInt(c.day_id.toString()), Double.parseDouble(c.value.toString()));

		}

		return ln;

	}

	/**
	 * FUNCTIONS TO GET THE NUMBER OF DAYS FOR WHICH THE INDICATOR HAS BEEN ABOVE
	 * OR BELOW THE SIGNAL LINE.
	 * 
	 * @return
	 */

	public Integer get_last_day_on_which_complex_is_positive(String complex_name){
		Integer req_day_id = null;
		def b = [];
		graph.V.has("entity_indicator_subindicator_unique_name",complex_name).inE('complex').
				order({it.a.getProperty('day_id') <=> it.b.getProperty('day_id')}).day_id.fill(b);
		System.out.println(b);
		if(b.size() > 0){
			return b.last();
		}
		else{
			return 0;
		}

	}

	public Integer get_day_ids_for_stock_between_two_day_ids(Integer from_day_id,
			Integer to_day_id,String stockName, String stockFullName, String indice){
		def b = [];
		graph.V.has('stockName',stockName).has('stockFullName',stockFullName).has('indice',indice)
				.interval('day_id',from_day_id, to_day_id + 1).day_id.fill(b);
		return b.size();
	}


	/**
	 * 
	 * Ends.
	 * 
	 * 
	 */


	/**
	 * FUNCTIONS FOR THE CONSECUTIVE COMPLEX CALCULATIONS.
	 * 
	 * 
	 * 
	 */

	public Boolean is_last_day_of_month(String stockName, String stockFullName, String indice,
			Integer day_id){


		graph.V.has('stockName',stockName).has('stockFullName',stockFullName).has('day_id',day_id)
				.has('indice',indice).as('primary_day').outE('day_connector_edge')

	}



	/**
	 * ENDS
	 * 
	 * 
	 */


	public Boolean check_if_common_day_ids_exist(){

		//graph.V.interval("common_day_id",0,)
		def b = graph.V.has("common_day_id").count();

		if(b == 0 ){
			LoggingDailyOperations.get_instance().add_to_log("info", null, "groovy_statements",
					"size of common day vertices was zero, adding them now.");

			return true;
		}
		else if(b < 500){
			LoggingDailyOperations.get_instance()
					.add_to_log("exception", new Exception("the size of the common day id vertex ids was ess than 18262 "
					), "groovy_statements", null);
			LoggingDailyOperations.get_instance().exit_and_log_before_exiting(graph);
			return false;
		}
		LoggingDailyOperations.get_instance().add_to_log("info",
				null, identifier_for_log, "the common day id vertices were present in the graph.");
		return false;
	}

	public add_registry_objects(String registry_component_md5_hash,
			Integer registry_counter, String registry_component_jsonified){

		Vertex v = graph.addVertexWithLabel("registry");
		v.setProperty("registry_component_md5_hash",registry_component_md5_hash);
		v.setProperty("registry_counter", registry_counter);
		v.setProperty("registry_component_jsonified", registry_component_jsonified);
		graph.commit();
	}

	public String increment_and_get_registry_hash_counter(String hash,String jsonified_component){


		if(registry_hash.containsKey(hash)){
			return registry_hash.get(hash).getValue0();
		}
		else{
			registry_hash.put(hash, new Pair<Integer, String>(central_counter.incrementAndGet().intValue(), jsonified_component));
			return central_counter;
		}
		/*
		 def b = [];
		 def central_counter = [];
		 graph.V.has("registry_component_md5_hash","central_counter").fill(central_counter);
		 Vertex central_counter_vertex = null;
		 Integer current_counter = null;
		 if(central_counter.isEmpty()){
		 //System.out.println("central counter is empty/");
		 central_counter_vertex = (Vertex)graph.addVertexWithLabel("registry");
		 central_counter_vertex.setProperty("registry_component_md5_hash", "central_counter");
		 central_counter_vertex.setProperty("registry_counter", 0);
		 current_counter = 0;
		 }
		 else{
		 //System.out.println("central counter was found.");
		 central_counter_vertex = (Vertex)central_counter[0];
		 current_counter = central_counter_vertex.getProperty("registry_counter");
		 //System.out.println("current counter is:" + current_counter);
		 }
		 graph.V.has("registry_component_md5_hash",hash).fill(b);
		 try{
		 if(b.isEmpty()){
		 Integer new_current_counter = current_counter + 1;
		 //System.out.println("this component was not found:" + jsonified_component);
		 Vertex v = (Vertex)graph.addVertexWithLabel("registry");
		 v.setProperty("registry_component_md5_hash", hash);
		 v.setProperty("registry_component_jsonified", jsonified_component);
		 v.setProperty("registry_counter", new_current_counter);
		 central_counter_vertex.setProperty("registry_counter", new_current_counter);
		 //System.out.println("now returning counter:" + String.valueOf(new_current_counter));
		 graph.commit();
		 return String.valueOf(new_current_counter);
		 }
		 else{
		 //System.out.println("this component was found:" + jsonified_component);
		 Vertex v = b.get(0);
		 Integer counter_for_this_component = v.getProperty("registry_counter");
		 //System.out.println("now returning counter:" + counter_for_this_component);
		 graph.commit();
		 return String.valueOf(counter_for_this_component);
		 //System.in.read();
		 }
		 }
		 catch(Exception e){
		 LoggingDailyOperations.get_instance().add_to_log("exception",
		 e,
		 identifier_for_log,
		 null);
		 return null;
		 }
		 */

	}



}
